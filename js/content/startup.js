"use strict";
!(function () {
	try {
		!(function () {
			if ("f" !== localStorage.getItem("darkyState")) {
				var t = (function () {
						var t = e(document.location.href);
						"" === t && (t = e(document.referrer));
						if ("" === t) return null;
						var n = !1;
						try {
							n = window.self !== window.top;
						} catch (e) {
							n = !0;
						}
						var r = "";
						if (n && (r = e(document.referrer)) !== t) return r + "-" + t;
						return "";
					})(),
					n = localStorage.getItem(t + "darkyMode");
				switch ((n = parseInt(n))) {
					case 1:
						window.self === window.top &&
							(document.documentElement.style.setProperty(
								"background-image",
								"linear-gradient(#1a1a1a,#1a1a1a)",
								"important"
							),
							setTimeout(() => {
								document.documentElement.style.setProperty(
									"background-image",
									"none",
									"important"
								);
							}, 3e3)),
							document.documentElement.setAttribute("nighteye", "passive");
						break;
					case 2:
					case 4:
						document.documentElement.setAttribute("nighteye", "disabled");
						break;
					default:
						window.self !== window.top &&
							document.documentElement.setAttribute("nighteye", "disabled");
				}
			} else document.documentElement.setAttribute("nighteye", "disabled");
		})();
	} catch (e) {
		document.documentElement.setAttribute("nighteye", "disabled");
	}
	function e(e) {
		var t = (e = e.replace("www.", "")).indexOf("://"),
			n = 0;
		return (
			-1 !== t &&
				-1 !== (n = (e = e.substring(t + 3)).indexOf("/")) &&
				(e = e.substring(0, n)),
			-1 !== (n = e.indexOf(":", t)) && (e = e.substring(0, n)),
			e
		);
	}
})();
