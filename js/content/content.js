"use strict";
!(function () {
	try {
		!(function () {
			if ("f" !== localStorage.getItem("darkyState")) {
				var t = (function () {
						var t = e(document.location.href);
						"" === t && (t = e(document.referrer));
						if ("" === t) return null;
						var n = !1;
						try {
							n = window.self !== window.top;
						} catch (e) {
							n = !0;
						}
						var r = "";
						if (n && (r = e(document.referrer)) !== t) return r + "-" + t;
						return "";
					})(),
					n = localStorage.getItem(t + "darkyMode");
				switch ((n = parseInt(n))) {
					case 1:
						window.self === window.top &&
							(document.documentElement.style.setProperty(
								"background-image",
								"linear-gradient(#1a1a1a,#1a1a1a)",
								"important"
							),
							setTimeout(() => {
								document.documentElement.style.setProperty(
									"background-image",
									"none",
									"important"
								);
							}, 3e3)),
							document.documentElement.setAttribute("nighteye", "passive");
						break;
					case 2:
					case 4:
						document.documentElement.setAttribute("nighteye", "disabled");
						break;
					default:
						window.self !== window.top &&
							document.documentElement.setAttribute("nighteye", "disabled");
				}
			} else document.documentElement.setAttribute("nighteye", "disabled");
		})();
	} catch (e) {
		document.documentElement.setAttribute("nighteye", "disabled");
	}
	function e(e) {
		var t = (e = e.replace("www.", "")).indexOf("://"),
			n = 0;
		return (
			-1 !== t &&
				-1 !== (n = (e = e.substring(t + 3)).indexOf("/")) &&
				(e = e.substring(0, n)),
			-1 !== (n = e.indexOf(":", t)) && (e = e.substring(0, n)),
			e
		);
	}
})();

!(function (t) {
	var e = {};
	function r(n) {
		if (e[n]) return e[n].exports;
		var o = (e[n] = { i: n, l: !1, exports: {} });
		return t[n].call(o.exports, o, o.exports, r), (o.l = !0), o.exports;
	}
	(r.m = t),
		(r.c = e),
		(r.d = function (t, e, n) {
			r.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n });
		}),
		(r.r = function (t) {
			"undefined" != typeof Symbol &&
				Symbol.toStringTag &&
				Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }),
				Object.defineProperty(t, "__esModule", { value: !0 });
		}),
		(r.t = function (t, e) {
			if ((1 & e && (t = r(t)), 8 & e)) return t;
			if (4 & e && "object" == typeof t && t && t.__esModule) return t;
			var n = Object.create(null);
			if (
				(r.r(n),
				Object.defineProperty(n, "default", { enumerable: !0, value: t }),
				2 & e && "string" != typeof t)
			)
				for (var o in t)
					r.d(
						n,
						o,
						function (e) {
							return t[e];
						}.bind(null, o)
					);
			return n;
		}),
		(r.n = function (t) {
			var e =
				t && t.__esModule
					? function () {
							return t.default;
					  }
					: function () {
							return t;
					  };
			return r.d(e, "a", e), e;
		}),
		(r.o = function (t, e) {
			return Object.prototype.hasOwnProperty.call(t, e);
		}),
		(r.p = ""),
		r((r.s = 146));
})([
	function (t, e, r) {
		var n = r(2),
			o = r(15).f,
			i = r(14),
			s = r(16),
			a = r(86),
			c = r(109),
			l = r(57);
		t.exports = function (t, e) {
			var r,
				u,
				f,
				h,
				d,
				p = t.target,
				g = t.global,
				m = t.stat;
			if ((r = g ? n : m ? n[p] || a(p, {}) : (n[p] || {}).prototype))
				for (u in e) {
					if (
						((h = e[u]),
						(f = t.noTargetGet ? (d = o(r, u)) && d.value : r[u]),
						!l(g ? u : p + (m ? "." : "#") + u, t.forced) && void 0 !== f)
					) {
						if (typeof h == typeof f) continue;
						c(h, f);
					}
					(t.sham || (f && f.sham)) && i(h, "sham", !0), s(r, u, h, t);
				}
		};
	},
	function (t, e) {
		t.exports = function (t) {
			try {
				return !!t();
			} catch (t) {
				return !0;
			}
		};
	},
	function (t, e, r) {
		(function (e) {
			var r = "object",
				n = function (t) {
					return t && t.Math == Math && t;
				};
			t.exports =
				n(typeof globalThis == r && globalThis) ||
				n(typeof window == r && window) ||
				n(typeof self == r && self) ||
				n(typeof e == r && e) ||
				Function("return this")();
		}.call(this, r(149)));
	},
	function (t, e) {
		t.exports = function (t) {
			return "object" == typeof t ? null !== t : "function" == typeof t;
		};
	},
	function (t, e, r) {
		var n = r(3);
		t.exports = function (t) {
			if (!n(t)) throw TypeError(String(t) + " is not an object");
			return t;
		};
	},
	function (t, e, r) {
		"use strict";
		var n,
			o = r(6),
			i = r(2),
			s = r(3),
			a = r(12),
			c = r(62),
			l = r(14),
			u = r(16),
			f = r(9).f,
			h = r(30),
			d = r(47),
			p = r(7),
			g = r(54),
			m = i.DataView,
			v = m && m.prototype,
			y = i.Int8Array,
			b = y && y.prototype,
			S = i.Uint8ClampedArray,
			_ = S && S.prototype,
			E = y && h(y),
			T = b && h(b),
			A = Object.prototype,
			w = A.isPrototypeOf,
			x = p("toStringTag"),
			R = g("TYPED_ARRAY_TAG"),
			I = !(!i.ArrayBuffer || !i.DataView),
			C = I && !!d,
			O = !1,
			M = {
				Int8Array: 1,
				Uint8Array: 1,
				Uint8ClampedArray: 1,
				Int16Array: 2,
				Uint16Array: 2,
				Int32Array: 4,
				Uint32Array: 4,
				Float32Array: 4,
				Float64Array: 8,
			},
			L = function (t) {
				return s(t) && a(M, c(t));
			};
		for (n in M) i[n] || (C = !1);
		if (
			(!C || "function" != typeof E || E === Function.prototype) &&
			((E = function () {
				throw TypeError("Incorrect invocation");
			}),
			C)
		)
			for (n in M) i[n] && d(i[n], E);
		if ((!C || !T || T === A) && ((T = E.prototype), C))
			for (n in M) i[n] && d(i[n].prototype, T);
		if ((C && h(_) !== T && d(_, T), o && !a(T, x)))
			for (n in ((O = !0),
			f(T, x, {
				get: function () {
					return s(this) ? this[R] : void 0;
				},
			}),
			M))
				i[n] && l(i[n], R, n);
		I && d && h(v) !== A && d(v, A),
			(t.exports = {
				NATIVE_ARRAY_BUFFER: I,
				NATIVE_ARRAY_BUFFER_VIEWS: C,
				TYPED_ARRAY_TAG: O && R,
				aTypedArray: function (t) {
					if (L(t)) return t;
					throw TypeError("Target is not a typed array");
				},
				aTypedArrayConstructor: function (t) {
					if (d) {
						if (w.call(E, t)) return t;
					} else
						for (var e in M)
							if (a(M, n)) {
								var r = i[e];
								if (r && (t === r || w.call(r, t))) return t;
							}
					throw TypeError("Target is not a typed array constructor");
				},
				exportProto: function (t, e, r) {
					if (o) {
						if (r)
							for (var n in M) {
								var s = i[n];
								s && a(s.prototype, t) && delete s.prototype[t];
							}
						(T[t] && !r) || u(T, t, r ? e : (C && b[t]) || e);
					}
				},
				exportStatic: function (t, e, r) {
					var n, s;
					if (o) {
						if (d) {
							if (r) for (n in M) (s = i[n]) && a(s, t) && delete s[t];
							if (E[t] && !r) return;
							try {
								return u(E, t, r ? e : (C && y[t]) || e);
							} catch (t) {}
						}
						for (n in M) !(s = i[n]) || (s[t] && !r) || u(s, t, e);
					}
				},
				isView: function (t) {
					var e = c(t);
					return "DataView" === e || a(M, e);
				},
				isTypedArray: L,
				TypedArray: E,
				TypedArrayPrototype: T,
			});
	},
	function (t, e, r) {
		var n = r(1);
		t.exports = !n(function () {
			return (
				7 !=
				Object.defineProperty({}, "a", {
					get: function () {
						return 7;
					},
				}).a
			);
		});
	},
	function (t, e, r) {
		var n = r(2),
			o = r(53),
			i = r(54),
			s = r(111),
			a = n.Symbol,
			c = o("wks");
		t.exports = function (t) {
			return c[t] || (c[t] = (s && a[t]) || (s ? a : i)("Symbol." + t));
		};
	},
	function (t, e, r) {
		var n = r(25),
			o = Math.min;
		t.exports = function (t) {
			return t > 0 ? o(n(t), 9007199254740991) : 0;
		};
	},
	function (t, e, r) {
		var n = r(6),
			o = r(106),
			i = r(4),
			s = r(27),
			a = Object.defineProperty;
		e.f = n
			? a
			: function (t, e, r) {
					if ((i(t), (e = s(e, !0)), i(r), o))
						try {
							return a(t, e, r);
						} catch (t) {}
					if ("get" in r || "set" in r)
						throw TypeError("Accessors not supported");
					return "value" in r && (t[e] = r.value), t;
			  };
	},
	function (t, e, r) {
		"use strict";
		r(22), r(66), r(34);
		class n {
			constructor(t) {
				(this.communicatorID = t), (this.promiseID = ""), (this.info = {});
			}
		}
		class o {
			static shared() {
				return (
					(void 0 !== o.instance && null !== o.instance) ||
						(o.instance = new o()),
					o.instance
				);
			}
			constructor() {
				(this.communicatorID = o.generateUUID()),
					(this.promises = new Map()),
					(this.onConnectListeners = { tabs: [], runtime: [] }),
					(this.onPortMessageListeners = []),
					(this.messageRuntimeListeners = []),
					(this.tabActionListeners = []),
					void 0 !== window.safari &&
						window.safari.self.addEventListener(
							"message",
							this.processResponse.bind(this)
						);
			}
			getFile(t) {
				return new Promise((e, r) => {
					var i = o.generateUUID();
					this.promises.set(i, { resolve: e, reject: r });
					var s = new n(this.communicatorID);
					(s.promiseID = i), (s.info.filename = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.files.postMessage(s)
							: window.safari.extension.dispatchMessage("files", s);
					} catch (t) {
						console.error(t);
					}
				});
			}
			downlaod(t) {
				return new Promise((e, r) => {
					var i = o.generateUUID();
					this.promises.set(i, { resolve: e, reject: r });
					var s = new n(this.communicatorID);
					(s.promiseID = i), (s.info.url = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.downlaod.postMessage(s)
							: window.safari.extension.dispatchMessage("download", s);
					} catch (t) {
						console.error(t);
					}
				});
			}
			tabs(t, e) {
				return new Promise((r, i) => {
					var s = o.generateUUID();
					this.promises.set(s, { resolve: r, reject: i });
					var a = new n(this.communicatorID);
					(a.promiseID = s), (a.info.action = e), (a.info.queryInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.tabs.postMessage(a)
							: window.safari.extension.dispatchMessage("tabs", a);
					} catch (t) {
						console.error(t);
					}
				});
			}
			sendMessage(t) {
				return new Promise((e, r) => {
					var i = o.generateUUID();
					this.promises.set(i, { resolve: e, reject: r });
					var s = new n(this.communicatorID);
					(s.promiseID = i), (s.info.userInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.runtime.postMessage(s)
							: window.safari.extension.dispatchMessage("runtime", s);
					} catch (t) {
						console.error(t);
					}
				});
			}
			browserAction(t) {
				return new Promise((e, r) => {
					var i = o.generateUUID();
					this.promises.set(i, { resolve: e, reject: r });
					var s = new n(this.communicatorID);
					(s.promiseID = i), (s.info.userInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.browser_action.postMessage(s)
							: window.safari.extension.dispatchMessage("browser_action", s);
					} catch (t) {
						console.error(t);
					}
				});
			}
			port(t) {
				return new Promise((e, r) => {
					var i = o.generateUUID();
					this.promises.set(i, { resolve: e, reject: r });
					var s = new n(this.communicatorID);
					(s.promiseID = i), (s.info.portInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.port.postMessage(s)
							: window.safari.extension.dispatchMessage("port", s);
					} catch (t) {
						console.error(t);
					}
				});
			}
			static generateUUID() {
				var t = new Date().getTime();
				return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
					/[xy]/g,
					function (e) {
						var r = (t + 16 * Math.random()) % 16 | 0;
						return (
							(t = Math.floor(t / 16)),
							("x" == e ? r : (3 & r) | 8).toString(16)
						);
					}
				);
			}
			processResponse(t) {
				let e = t.message.data;
				if (
					-1 !==
					[
						"files_resolve",
						"tabs_resolve",
						"runtime_resolve",
						"port_resolve",
					].indexOf(t.name)
				) {
					if (t.message.communicatorID !== this.communicatorID) return;
					let r = t.message.promiseID;
					!1 === this.resolvePromise(r, e, null) &&
						console.warn(
							`Response with promiseID: ${r} cannot not found in map`
						);
				} else
					"runtime" === t.name
						? this.onMessageRuntimeRecived(e)
						: "onConnectTab" === t.name
						? this.onConnectTabRecieved(e)
						: "onConnectRuntime" === t.name
						? this.onConnectRuntimeRecieved(e)
						: "portMessage" === t.name
						? this.onPortMessageListenersRecieved(e)
						: "tabActions" === t.name
						? this.onTabActionRecieved(e)
						: console.warn("Recieved message with name:", t.name);
			}
			resolvePromise(t, e, r) {
				var n = this.promises.get(t);
				return (
					void 0 !== n &&
					(r ? n.reject(e) : n.resolve(e), this.promises.delete(t), !0)
				);
			}
			addListenerOnConnectTab(t) {
				this.onConnectListeners.tabs.push(t);
			}
			addListenerOnConnectRuntime(t) {
				this.onConnectListeners.runtime.push(t);
			}
			onConnectTabRecieved(t) {
				for (const e of this.onConnectListeners.tabs) e(t);
			}
			onConnectRuntimeRecieved(t) {
				for (const e of this.onConnectListeners.runtime) e(t);
			}
			addListenerOnPortMessageListener(t) {
				this.onPortMessageListeners.push(t);
			}
			onPortMessageListenersRecieved(t) {
				for (const e of this.onPortMessageListeners) e(t);
			}
			addListenerOnMessageRuntime(t) {
				this.messageRuntimeListeners.push(t);
			}
			onMessageRuntimeRecived(t) {
				for (const e of this.messageRuntimeListeners) e(t);
			}
			onTabActionRecieved(t) {
				for (const e of this.tabActionListeners) e(t);
			}
			addListenerTabAction(t) {
				this.tabActionListeners.push(t);
			}
		}
		(o.instance = null), (window.communicator = o.shared());
		e.a = o;
	},
	function (t, e, r) {
		var n = r(19);
		t.exports = function (t) {
			return Object(n(t));
		};
	},
	function (t, e) {
		var r = {}.hasOwnProperty;
		t.exports = function (t, e) {
			return r.call(t, e);
		};
	},
	function (t, e, r) {
		var n = r(38),
			o = r(52),
			i = r(11),
			s = r(8),
			a = r(63);
		t.exports = function (t, e) {
			var r = 1 == t,
				c = 2 == t,
				l = 3 == t,
				u = 4 == t,
				f = 6 == t,
				h = 5 == t || f,
				d = e || a;
			return function (e, a, p) {
				for (
					var g,
						m,
						v = i(e),
						y = o(v),
						b = n(a, p, 3),
						S = s(y.length),
						_ = 0,
						E = r ? d(e, S) : c ? d(e, 0) : void 0;
					S > _;
					_++
				)
					if ((h || _ in y) && ((m = b((g = y[_]), _, v)), t))
						if (r) E[_] = m;
						else if (m)
							switch (t) {
								case 3:
									return !0;
								case 5:
									return g;
								case 6:
									return _;
								case 2:
									E.push(g);
							}
						else if (u) return !1;
				return f ? -1 : l || u ? u : E;
			};
		};
	},
	function (t, e, r) {
		var n = r(6),
			o = r(9),
			i = r(41);
		t.exports = n
			? function (t, e, r) {
					return o.f(t, e, i(1, r));
			  }
			: function (t, e, r) {
					return (t[e] = r), t;
			  };
	},
	function (t, e, r) {
		var n = r(6),
			o = r(51),
			i = r(41),
			s = r(18),
			a = r(27),
			c = r(12),
			l = r(106),
			u = Object.getOwnPropertyDescriptor;
		e.f = n
			? u
			: function (t, e) {
					if (((t = s(t)), (e = a(e, !0)), l))
						try {
							return u(t, e);
						} catch (t) {}
					if (c(t, e)) return i(!o.f.call(t, e), t[e]);
			  };
	},
	function (t, e, r) {
		var n = r(2),
			o = r(53),
			i = r(14),
			s = r(12),
			a = r(86),
			c = r(107),
			l = r(20),
			u = l.get,
			f = l.enforce,
			h = String(c).split("toString");
		o("inspectSource", function (t) {
			return c.call(t);
		}),
			(t.exports = function (t, e, r, o) {
				var c = !!o && !!o.unsafe,
					l = !!o && !!o.enumerable,
					u = !!o && !!o.noTargetGet;
				"function" == typeof r &&
					("string" != typeof e || s(r, "name") || i(r, "name", e),
					(f(r).source = h.join("string" == typeof e ? e : ""))),
					t !== n
						? (c ? !u && t[e] && (l = !0) : delete t[e],
						  l ? (t[e] = r) : i(t, e, r))
						: l
						? (t[e] = r)
						: a(e, r);
			})(Function.prototype, "toString", function () {
				return ("function" == typeof this && u(this).source) || c.call(this);
			});
	},
	function (t, e, r) {
		var n = r(71),
			o = r(12),
			i = r(112),
			s = r(9).f;
		t.exports = function (t) {
			var e = n.Symbol || (n.Symbol = {});
			o(e, t) || s(e, t, { value: i.f(t) });
		};
	},
	function (t, e, r) {
		var n = r(52),
			o = r(19);
		t.exports = function (t) {
			return n(o(t));
		};
	},
	function (t, e) {
		t.exports = function (t) {
			if (null == t) throw TypeError("Can't call method on " + t);
			return t;
		};
	},
	function (t, e, r) {
		var n,
			o,
			i,
			s = r(108),
			a = r(2),
			c = r(3),
			l = r(14),
			u = r(12),
			f = r(69),
			h = r(55),
			d = a.WeakMap;
		if (s) {
			var p = new d(),
				g = p.get,
				m = p.has,
				v = p.set;
			(n = function (t, e) {
				return v.call(p, t, e), e;
			}),
				(o = function (t) {
					return g.call(p, t) || {};
				}),
				(i = function (t) {
					return m.call(p, t);
				});
		} else {
			var y = f("state");
			(h[y] = !0),
				(n = function (t, e) {
					return l(t, y, e), e;
				}),
				(o = function (t) {
					return u(t, y) ? t[y] : {};
				}),
				(i = function (t) {
					return u(t, y);
				});
		}
		t.exports = {
			set: n,
			get: o,
			has: i,
			enforce: function (t) {
				return i(t) ? o(t) : n(t, {});
			},
			getterFor: function (t) {
				return function (e) {
					var r;
					if (!c(e) || (r = o(e)).type !== t)
						throw TypeError("Incompatible receiver, " + t + " required");
					return r;
				};
			},
		};
	},
	function (t, e) {
		t.exports = function (t) {
			if ("function" != typeof t)
				throw TypeError(String(t) + " is not a function");
			return t;
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(18),
			o = r(39),
			i = r(60),
			s = r(20),
			a = r(94),
			c = s.set,
			l = s.getterFor("Array Iterator");
		(t.exports = a(
			Array,
			"Array",
			function (t, e) {
				c(this, { type: "Array Iterator", target: n(t), index: 0, kind: e });
			},
			function () {
				var t = l(this),
					e = t.target,
					r = t.kind,
					n = t.index++;
				return !e || n >= e.length
					? ((t.target = void 0), { value: void 0, done: !0 })
					: "keys" == r
					? { value: n, done: !1 }
					: "values" == r
					? { value: e[n], done: !1 }
					: { value: [n, e[n]], done: !1 };
			},
			"values"
		)),
			(i.Arguments = i.Array),
			o("keys"),
			o("values"),
			o("entries");
	},
	function (t, e, r) {
		var n = r(19),
			o = /"/g;
		t.exports = function (t, e, r, i) {
			var s = String(n(t)),
				a = "<" + e;
			return (
				"" !== r &&
					(a += " " + r + '="' + String(i).replace(o, "&quot;") + '"'),
				a + ">" + s + "</" + e + ">"
			);
		};
	},
	function (t, e, r) {
		var n = r(1);
		t.exports = function (t) {
			return n(function () {
				var e = ""[t]('"');
				return e !== e.toLowerCase() || e.split('"').length > 3;
			});
		};
	},
	function (t, e) {
		var r = Math.ceil,
			n = Math.floor;
		t.exports = function (t) {
			return isNaN((t = +t)) ? 0 : (t > 0 ? n : r)(t);
		};
	},
	function (t, e, r) {
		"use strict";
		r.d(e, "a", function () {
			return i;
		});
		var n = r(10),
			o = r(67);
		class i {
			constructor(t, e) {
				(this.uniqueID = void 0 === e ? "" : e),
					(this.type = t),
					(this.uuid = n.a.generateUUID()),
					(this.name = ""),
					(this.sender = null),
					(this.onMessage = new o.a()),
					(this.onDisconnect = new o.a()),
					(this.ready = !1),
					(this.postMessageQueue = []);
			}
			executeWaitingMessages() {
				var t = this.postMessageQueue.pop();
				this.sendMessage(t),
					0 !== this.postMessageQueue.length && this.executeWaitingMessages();
			}
			postMessage(t) {
				this.postMessageQueue.unshift(t),
					this.ready && this.executeWaitingMessages();
			}
			sendMessage(t) {
				var e = {
					action: i.ACTION.SEND,
					portUUID: this.uuid,
					type: this.type,
					uniqueID: this.uniqueID,
					data: t,
				};
				n.a
					.shared()
					.port(e)
					.then((t) => {})
					.catch((t) => {
						console.error(`Error runtime ${t}`);
					});
			}
			disconnect() {}
		}
		(i.TYPES = { TAB: 1, RUNTIME: 2 }),
			(i.ACTION = { CREATE: 1, SEND: 2, DISCONECT: 3 }),
			(i.RECIEVER = { CONTENT: 1, POPUP: 2 });
	},
	function (t, e, r) {
		var n = r(3);
		t.exports = function (t, e) {
			if (!n(t)) return t;
			var r, o;
			if (e && "function" == typeof (r = t.toString) && !n((o = r.call(t))))
				return o;
			if ("function" == typeof (r = t.valueOf) && !n((o = r.call(t)))) return o;
			if (!e && "function" == typeof (r = t.toString) && !n((o = r.call(t))))
				return o;
			throw TypeError("Can't convert object to primitive value");
		};
	},
	function (t, e) {
		var r = {}.toString;
		t.exports = function (t) {
			return r.call(t).slice(8, -1);
		};
	},
	function (t, e, r) {
		var n = r(9).f,
			o = r(12),
			i = r(7)("toStringTag");
		t.exports = function (t, e, r) {
			t &&
				!o((t = r ? t : t.prototype), i) &&
				n(t, i, { configurable: !0, value: e });
		};
	},
	function (t, e, r) {
		var n = r(12),
			o = r(11),
			i = r(69),
			s = r(91),
			a = i("IE_PROTO"),
			c = Object.prototype;
		t.exports = s
			? Object.getPrototypeOf
			: function (t) {
					return (
						(t = o(t)),
						n(t, a)
							? t[a]
							: "function" == typeof t.constructor && t instanceof t.constructor
							? t.constructor.prototype
							: t instanceof Object
							? c
							: null
					);
			  };
	},
	function (t, e, r) {
		"use strict";
		var n = r(1);
		t.exports = function (t, e) {
			var r = [][t];
			return (
				!r ||
				!n(function () {
					r.call(
						null,
						e ||
							function () {
								throw 1;
							},
						1
					);
				})
			);
		};
	},
	function (t, e, r) {
		var n = r(4),
			o = r(21),
			i = r(7)("species");
		t.exports = function (t, e) {
			var r,
				s = n(t).constructor;
			return void 0 === s || null == (r = n(s)[i]) ? e : o(r);
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(2),
			i = r(6),
			s = r(104),
			a = r(5),
			c = r(83),
			l = r(40),
			u = r(41),
			f = r(14),
			h = r(8),
			d = r(139),
			p = r(140),
			g = r(27),
			m = r(12),
			v = r(62),
			y = r(3),
			b = r(37),
			S = r(47),
			_ = r(42).f,
			E = r(141),
			T = r(13),
			A = r(48),
			w = r(9),
			x = r(15),
			R = r(20),
			I = R.get,
			C = R.set,
			O = w.f,
			M = x.f,
			L = T(0),
			N = o.RangeError,
			k = c.ArrayBuffer,
			P = c.DataView,
			D = a.NATIVE_ARRAY_BUFFER_VIEWS,
			U = a.TYPED_ARRAY_TAG,
			B = a.TypedArray,
			F = a.TypedArrayPrototype,
			G = a.aTypedArrayConstructor,
			j = a.isTypedArray,
			H = function (t, e) {
				for (var r = 0, n = e.length, o = new (G(t))(n); n > r; ) o[r] = e[r++];
				return o;
			},
			W = function (t, e) {
				O(t, e, {
					get: function () {
						return I(this)[e];
					},
				});
			},
			z = function (t) {
				var e;
				return (
					t instanceof k ||
					"ArrayBuffer" == (e = v(t)) ||
					"SharedArrayBuffer" == e
				);
			},
			Y = function (t, e) {
				return (
					j(t) && "symbol" != typeof e && e in t && String(+e) == String(e)
				);
			},
			V = function (t, e) {
				return Y(t, (e = g(e, !0))) ? u(2, t[e]) : M(t, e);
			},
			q = function (t, e, r) {
				return !(Y(t, (e = g(e, !0))) && y(r) && m(r, "value")) ||
					m(r, "get") ||
					m(r, "set") ||
					r.configurable ||
					(m(r, "writable") && !r.writable) ||
					(m(r, "enumerable") && !r.enumerable)
					? O(t, e, r)
					: ((t[e] = r.value), t);
			};
		i
			? (D ||
					((x.f = V),
					(w.f = q),
					W(F, "buffer"),
					W(F, "byteOffset"),
					W(F, "byteLength"),
					W(F, "length")),
			  n(
					{ target: "Object", stat: !0, forced: !D },
					{ getOwnPropertyDescriptor: V, defineProperty: q }
			  ),
			  (t.exports = function (t, e, r, i) {
					var a = t + (i ? "Clamped" : "") + "Array",
						c = "get" + t,
						u = "set" + t,
						g = o[a],
						m = g,
						v = m && m.prototype,
						T = {},
						w = function (t, r) {
							O(t, r, {
								get: function () {
									return (function (t, r) {
										var n = I(t);
										return n.view[c](r * e + n.byteOffset, !0);
									})(this, r);
								},
								set: function (t) {
									return (function (t, r, n) {
										var o = I(t);
										i &&
											(n =
												(n = Math.round(n)) < 0 ? 0 : n > 255 ? 255 : 255 & n),
											o.view[u](r * e + o.byteOffset, n, !0);
									})(this, r, t);
								},
								enumerable: !0,
							});
						};
					D
						? s &&
						  ((m = r(function (t, r, n, o) {
								return (
									l(t, m, a),
									y(r)
										? z(r)
											? void 0 !== o
												? new g(r, p(n, e), o)
												: void 0 !== n
												? new g(r, p(n, e))
												: new g(r)
											: j(r)
											? H(m, r)
											: E.call(m, r)
										: new g(d(r))
								);
						  })),
						  S && S(m, B),
						  L(_(g), function (t) {
								t in m || f(m, t, g[t]);
						  }),
						  (m.prototype = v))
						: ((m = r(function (t, r, n, o) {
								l(t, m, a);
								var i,
									s,
									c,
									u = 0,
									f = 0;
								if (y(r)) {
									if (!z(r)) return j(r) ? H(m, r) : E.call(m, r);
									(i = r), (f = p(n, e));
									var g = r.byteLength;
									if (void 0 === o) {
										if (g % e) throw N("Wrong length");
										if ((s = g - f) < 0) throw N("Wrong length");
									} else if ((s = h(o) * e) + f > g) throw N("Wrong length");
									c = s / e;
								} else (c = d(r)), (i = new k((s = c * e)));
								for (
									C(t, {
										buffer: i,
										byteOffset: f,
										byteLength: s,
										length: c,
										view: new P(i),
									});
									u < c;

								)
									w(t, u++);
						  })),
						  S && S(m, B),
						  (v = m.prototype = b(F))),
						v.constructor !== m && f(v, "constructor", m),
						U && f(v, U, a),
						(T[a] = m),
						n({ global: !0, forced: m != g, sham: !D }, T),
						"BYTES_PER_ELEMENT" in m || f(m, "BYTES_PER_ELEMENT", e),
						"BYTES_PER_ELEMENT" in v || f(v, "BYTES_PER_ELEMENT", e),
						A(a);
			  }))
			: (t.exports = function () {});
	},
	function (t, e, r) {
		var n = r(2),
			o = r(359),
			i = r(22),
			s = r(14),
			a = r(7),
			c = a("iterator"),
			l = a("toStringTag"),
			u = i.values;
		for (var f in o) {
			var h = n[f],
				d = h && h.prototype;
			if (d) {
				if (d[c] !== u)
					try {
						s(d, c, u);
					} catch (t) {
						d[c] = u;
					}
				if ((d[l] || s(d, l, f), o[f]))
					for (var p in i)
						if (d[p] !== i[p])
							try {
								s(d, p, i[p]);
							} catch (t) {
								d[p] = i[p];
							}
			}
		}
	},
	function (t, e) {
		t.exports = !1;
	},
	function (t, e, r) {
		var n = r(25),
			o = Math.max,
			i = Math.min;
		t.exports = function (t, e) {
			var r = n(t);
			return r < 0 ? o(r + e, 0) : i(r, e);
		};
	},
	function (t, e, r) {
		var n = r(4),
			o = r(89),
			i = r(88),
			s = r(55),
			a = r(113),
			c = r(85),
			l = r(69)("IE_PROTO"),
			u = function () {},
			f = function () {
				var t,
					e = c("iframe"),
					r = i.length;
				for (
					e.style.display = "none",
						a.appendChild(e),
						e.src = String("javascript:"),
						(t = e.contentWindow.document).open(),
						t.write("<script>document.F=Object</script>"),
						t.close(),
						f = t.F;
					r--;

				)
					delete f.prototype[i[r]];
				return f();
			};
		(t.exports =
			Object.create ||
			function (t, e) {
				var r;
				return (
					null !== t
						? ((u.prototype = n(t)),
						  (r = new u()),
						  (u.prototype = null),
						  (r[l] = t))
						: (r = f()),
					void 0 === e ? r : o(r, e)
				);
			}),
			(s[l] = !0);
	},
	function (t, e, r) {
		var n = r(21);
		t.exports = function (t, e, r) {
			if ((n(t), void 0 === e)) return t;
			switch (r) {
				case 0:
					return function () {
						return t.call(e);
					};
				case 1:
					return function (r) {
						return t.call(e, r);
					};
				case 2:
					return function (r, n) {
						return t.call(e, r, n);
					};
				case 3:
					return function (r, n, o) {
						return t.call(e, r, n, o);
					};
			}
			return function () {
				return t.apply(e, arguments);
			};
		};
	},
	function (t, e, r) {
		var n = r(7),
			o = r(37),
			i = r(14),
			s = n("unscopables"),
			a = Array.prototype;
		null == a[s] && i(a, s, o(null)),
			(t.exports = function (t) {
				a[s][t] = !0;
			});
	},
	function (t, e) {
		t.exports = function (t, e, r) {
			if (!(t instanceof e))
				throw TypeError("Incorrect " + (r ? r + " " : "") + "invocation");
			return t;
		};
	},
	function (t, e) {
		t.exports = function (t, e) {
			return {
				enumerable: !(1 & t),
				configurable: !(2 & t),
				writable: !(4 & t),
				value: e,
			};
		};
	},
	function (t, e, r) {
		var n = r(110),
			o = r(88).concat("length", "prototype");
		e.f =
			Object.getOwnPropertyNames ||
			function (t) {
				return n(t, o);
			};
	},
	function (t, e, r) {
		var n = r(28);
		t.exports =
			Array.isArray ||
			function (t) {
				return "Array" == n(t);
			};
	},
	function (t, e, r) {
		var n = r(55),
			o = r(3),
			i = r(12),
			s = r(9).f,
			a = r(54),
			c = r(58),
			l = a("meta"),
			u = 0,
			f =
				Object.isExtensible ||
				function () {
					return !0;
				},
			h = function (t) {
				s(t, l, { value: { objectID: "O" + ++u, weakData: {} } });
			},
			d = (t.exports = {
				REQUIRED: !1,
				fastKey: function (t, e) {
					if (!o(t))
						return "symbol" == typeof t
							? t
							: ("string" == typeof t ? "S" : "P") + t;
					if (!i(t, l)) {
						if (!f(t)) return "F";
						if (!e) return "E";
						h(t);
					}
					return t[l].objectID;
				},
				getWeakData: function (t, e) {
					if (!i(t, l)) {
						if (!f(t)) return !0;
						if (!e) return !1;
						h(t);
					}
					return t[l].weakData;
				},
				onFreeze: function (t) {
					return c && d.REQUIRED && f(t) && !i(t, l) && h(t), t;
				},
			});
		n[l] = !0;
	},
	function (t, e, r) {
		"use strict";
		var n = r(27),
			o = r(9),
			i = r(41);
		t.exports = function (t, e, r) {
			var s = n(e);
			s in t ? o.f(t, s, i(0, r)) : (t[s] = r);
		};
	},
	function (t, e, r) {
		var n = r(110),
			o = r(88);
		t.exports =
			Object.keys ||
			function (t) {
				return n(t, o);
			};
	},
	function (t, e, r) {
		var n = r(119);
		t.exports =
			Object.setPrototypeOf ||
			("__proto__" in {}
				? (function () {
						var t,
							e = !1,
							r = {};
						try {
							(t = Object.getOwnPropertyDescriptor(
								Object.prototype,
								"__proto__"
							).set).call(r, []),
								(e = r instanceof Array);
						} catch (t) {}
						return function (r, o) {
							return n(r, o), e ? t.call(r, o) : (r.__proto__ = o), r;
						};
				  })()
				: void 0);
	},
	function (t, e, r) {
		"use strict";
		var n = r(93),
			o = r(9),
			i = r(7),
			s = r(6),
			a = i("species");
		t.exports = function (t) {
			var e = n(t),
				r = o.f;
			s &&
				e &&
				!e[a] &&
				r(e, a, {
					configurable: !0,
					get: function () {
						return this;
					},
				});
		};
	},
	function (t, e, r) {
		var n = r(19),
			o = "[" + r(80) + "]",
			i = RegExp("^" + o + o + "*"),
			s = RegExp(o + o + "*$");
		t.exports = function (t, e) {
			return (
				(t = String(n(t))),
				1 & e && (t = t.replace(i, "")),
				2 & e && (t = t.replace(s, "")),
				t
			);
		};
	},
	function (t, e, r) {
		var n = r(16);
		t.exports = function (t, e, r) {
			for (var o in e) n(t, o, e[o], r);
			return t;
		};
	},
	function (t, e, r) {
		"use strict";
		var n = {}.propertyIsEnumerable,
			o = Object.getOwnPropertyDescriptor,
			i = o && !n.call({ 1: 2 }, 1);
		e.f = i
			? function (t) {
					var e = o(this, t);
					return !!e && e.enumerable;
			  }
			: n;
	},
	function (t, e, r) {
		var n = r(1),
			o = r(28),
			i = "".split;
		t.exports = n(function () {
			return !Object("z").propertyIsEnumerable(0);
		})
			? function (t) {
					return "String" == o(t) ? i.call(t, "") : Object(t);
			  }
			: Object;
	},
	function (t, e, r) {
		var n = r(2),
			o = r(86),
			i = r(35),
			s = n["__core-js_shared__"] || o("__core-js_shared__", {});
		(t.exports = function (t, e) {
			return s[t] || (s[t] = void 0 !== e ? e : {});
		})("versions", []).push({
			version: "3.1.3",
			mode: i ? "pure" : "global",
			copyright: "© 2019 Denis Pushkarev (zloirock.ru)",
		});
	},
	function (t, e) {
		var r = 0,
			n = Math.random();
		t.exports = function (t) {
			return "Symbol(".concat(
				void 0 === t ? "" : t,
				")_",
				(++r + n).toString(36)
			);
		};
	},
	function (t, e) {
		t.exports = {};
	},
	function (t, e, r) {
		var n = r(18),
			o = r(8),
			i = r(36);
		t.exports = function (t) {
			return function (e, r, s) {
				var a,
					c = n(e),
					l = o(c.length),
					u = i(s, l);
				if (t && r != r) {
					for (; l > u; ) if ((a = c[u++]) != a) return !0;
				} else
					for (; l > u; u++)
						if ((t || u in c) && c[u] === r) return t || u || 0;
				return !t && -1;
			};
		};
	},
	function (t, e, r) {
		var n = r(1),
			o = /#|\.prototype\./,
			i = function (t, e) {
				var r = a[s(t)];
				return r == l || (r != c && ("function" == typeof e ? n(e) : !!e));
			},
			s = (i.normalize = function (t) {
				return String(t).replace(o, ".").toLowerCase();
			}),
			a = (i.data = {}),
			c = (i.NATIVE = "N"),
			l = (i.POLYFILL = "P");
		t.exports = i;
	},
	function (t, e, r) {
		var n = r(1);
		t.exports = !n(function () {
			return Object.isExtensible(Object.preventExtensions({}));
		});
	},
	function (t, e, r) {
		var n = r(4),
			o = r(90),
			i = r(8),
			s = r(38),
			a = r(61),
			c = r(117),
			l = {};
		(t.exports = function (t, e, r, u, f) {
			var h,
				d,
				p,
				g,
				m,
				v = s(e, r, u ? 2 : 1);
			if (f) h = t;
			else {
				if ("function" != typeof (d = a(t)))
					throw TypeError("Target is not iterable");
				if (o(d)) {
					for (p = 0, g = i(t.length); g > p; p++)
						if ((u ? v(n((m = t[p]))[0], m[1]) : v(t[p])) === l) return l;
					return;
				}
				h = d.call(t);
			}
			for (; !(m = h.next()).done; ) if (c(h, v, m.value, u) === l) return l;
		}).BREAK = l;
	},
	function (t, e) {
		t.exports = {};
	},
	function (t, e, r) {
		var n = r(62),
			o = r(60),
			i = r(7)("iterator");
		t.exports = function (t) {
			if (null != t) return t[i] || t["@@iterator"] || o[n(t)];
		};
	},
	function (t, e, r) {
		var n = r(28),
			o = r(7)("toStringTag"),
			i =
				"Arguments" ==
				n(
					(function () {
						return arguments;
					})()
				);
		t.exports = function (t) {
			var e, r, s;
			return void 0 === t
				? "Undefined"
				: null === t
				? "Null"
				: "string" ==
				  typeof (r = (function (t, e) {
						try {
							return t[e];
						} catch (t) {}
				  })((e = Object(t)), o))
				? r
				: i
				? n(e)
				: "Object" == (s = n(e)) && "function" == typeof e.callee
				? "Arguments"
				: s;
		};
	},
	function (t, e, r) {
		var n = r(3),
			o = r(43),
			i = r(7)("species");
		t.exports = function (t, e) {
			var r;
			return (
				o(t) &&
					("function" != typeof (r = t.constructor) ||
					(r !== Array && !o(r.prototype))
						? n(r) && null === (r = r[i]) && (r = void 0)
						: (r = void 0)),
				new (void 0 === r ? Array : r)(0 === e ? 0 : e)
			);
		};
	},
	function (t, e, r) {
		var n = r(1),
			o = r(7)("species");
		t.exports = function (t) {
			return !n(function () {
				var e = [];
				return (
					((e.constructor = {})[o] = function () {
						return { foo: 1 };
					}),
					1 !== e[t](Boolean).foo
				);
			});
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(4);
		t.exports = function () {
			var t = n(this),
				e = "";
			return (
				t.global && (e += "g"),
				t.ignoreCase && (e += "i"),
				t.multiline && (e += "m"),
				t.unicode && (e += "u"),
				t.sticky && (e += "y"),
				e
			);
		};
	},
	function (t, e, r) {
		"use strict";
		var n,
			o,
			i,
			s = r(0),
			a = r(35),
			c = r(2),
			l = r(71),
			u = r(50),
			f = r(29),
			h = r(48),
			d = r(3),
			p = r(21),
			g = r(40),
			m = r(28),
			v = r(59),
			y = r(73),
			b = r(32),
			S = r(134).set,
			_ = r(299),
			E = r(135),
			T = r(300),
			A = r(136),
			w = r(301),
			x = r(100),
			R = r(20),
			I = r(57),
			C = r(7)("species"),
			O = "Promise",
			M = R.get,
			L = R.set,
			N = R.getterFor(O),
			k = c.Promise,
			P = c.TypeError,
			D = c.document,
			U = c.process,
			B = c.fetch,
			F = U && U.versions,
			G = (F && F.v8) || "",
			j = A.f,
			H = j,
			W = "process" == m(U),
			z = !!(D && D.createEvent && c.dispatchEvent),
			Y = I(O, function () {
				var t = k.resolve(1),
					e = function () {},
					r = ((t.constructor = {})[C] = function (t) {
						t(e, e);
					});
				return !(
					(W || "function" == typeof PromiseRejectionEvent) &&
					(!a || t.finally) &&
					t.then(e) instanceof r &&
					0 !== G.indexOf("6.6") &&
					-1 === x.indexOf("Chrome/66")
				);
			}),
			V =
				Y ||
				!y(function (t) {
					k.all(t).catch(function () {});
				}),
			q = function (t) {
				var e;
				return !(!d(t) || "function" != typeof (e = t.then)) && e;
			},
			K = function (t, e, r) {
				if (!e.notified) {
					e.notified = !0;
					var n = e.reactions;
					_(function () {
						for (
							var o = e.value,
								i = 1 == e.state,
								s = 0,
								a = function (r) {
									var n,
										s,
										a,
										c = i ? r.ok : r.fail,
										l = r.resolve,
										u = r.reject,
										f = r.domain;
									try {
										c
											? (i || (2 === e.rejection && Q(t, e), (e.rejection = 1)),
											  !0 === c
													? (n = o)
													: (f && f.enter(),
													  (n = c(o)),
													  f && (f.exit(), (a = !0))),
											  n === r.promise
													? u(P("Promise-chain cycle"))
													: (s = q(n))
													? s.call(n, l, u)
													: l(n))
											: u(o);
									} catch (t) {
										f && !a && f.exit(), u(t);
									}
								};
							n.length > s;

						)
							a(n[s++]);
						(e.reactions = []), (e.notified = !1), r && !e.rejection && Z(t, e);
					});
				}
			},
			X = function (t, e, r) {
				var n, o;
				z
					? (((n = D.createEvent("Event")).promise = e),
					  (n.reason = r),
					  n.initEvent(t, !1, !0),
					  c.dispatchEvent(n))
					: (n = { promise: e, reason: r }),
					(o = c["on" + t])
						? o(n)
						: "unhandledrejection" === t && T("Unhandled promise rejection", r);
			},
			Z = function (t, e) {
				S.call(c, function () {
					var r,
						n = e.value;
					if (
						J(e) &&
						((r = w(function () {
							W
								? U.emit("unhandledRejection", n, t)
								: X("unhandledrejection", t, n);
						})),
						(e.rejection = W || J(e) ? 2 : 1),
						r.error)
					)
						throw r.value;
				});
			},
			J = function (t) {
				return 1 !== t.rejection && !t.parent;
			},
			Q = function (t, e) {
				S.call(c, function () {
					W ? U.emit("rejectionHandled", t) : X("rejectionhandled", t, e.value);
				});
			},
			$ = function (t, e, r, n) {
				return function (o) {
					t(e, r, o, n);
				};
			},
			tt = function (t, e, r, n) {
				e.done ||
					((e.done = !0),
					n && (e = n),
					(e.value = r),
					(e.state = 2),
					K(t, e, !0));
			},
			et = function (t, e, r, n) {
				if (!e.done) {
					(e.done = !0), n && (e = n);
					try {
						if (t === r) throw P("Promise can't be resolved itself");
						var o = q(r);
						o
							? _(function () {
									var n = { done: !1 };
									try {
										o.call(r, $(et, t, n, e), $(tt, t, n, e));
									} catch (r) {
										tt(t, n, r, e);
									}
							  })
							: ((e.value = r), (e.state = 1), K(t, e, !1));
					} catch (r) {
						tt(t, { done: !1 }, r, e);
					}
				}
			};
		Y &&
			((k = function (t) {
				g(this, k, O), p(t), n.call(this);
				var e = M(this);
				try {
					t($(et, this, e), $(tt, this, e));
				} catch (t) {
					tt(this, e, t);
				}
			}),
			((n = function (t) {
				L(this, {
					type: O,
					done: !1,
					notified: !1,
					parent: !1,
					reactions: [],
					rejection: !1,
					state: 0,
					value: void 0,
				});
			}).prototype = u(k.prototype, {
				then: function (t, e) {
					var r = N(this),
						n = j(b(this, k));
					return (
						(n.ok = "function" != typeof t || t),
						(n.fail = "function" == typeof e && e),
						(n.domain = W ? U.domain : void 0),
						(r.parent = !0),
						r.reactions.push(n),
						0 != r.state && K(this, r, !1),
						n.promise
					);
				},
				catch: function (t) {
					return this.then(void 0, t);
				},
			})),
			(o = function () {
				var t = new n(),
					e = M(t);
				(this.promise = t),
					(this.resolve = $(et, t, e)),
					(this.reject = $(tt, t, e));
			}),
			(A.f = j = function (t) {
				return t === k || t === i ? new o(t) : H(t);
			}),
			a ||
				"function" != typeof B ||
				s(
					{ global: !0, enumerable: !0, forced: !0 },
					{
						fetch: function (t) {
							return E(k, B.apply(c, arguments));
						},
					}
				)),
			s({ global: !0, wrap: !0, forced: Y }, { Promise: k }),
			f(k, O, !1, !0),
			h(O),
			(i = l.Promise),
			s(
				{ target: O, stat: !0, forced: Y },
				{
					reject: function (t) {
						var e = j(this);
						return e.reject.call(void 0, t), e.promise;
					},
				}
			),
			s(
				{ target: O, stat: !0, forced: a || Y },
				{
					resolve: function (t) {
						return E(a && this === i ? k : this, t);
					},
				}
			),
			s(
				{ target: O, stat: !0, forced: V },
				{
					all: function (t) {
						var e = this,
							r = j(e),
							n = r.resolve,
							o = r.reject,
							i = w(function () {
								var r = p(e.resolve),
									i = [],
									s = 0,
									a = 1;
								v(t, function (t) {
									var c = s++,
										l = !1;
									i.push(void 0),
										a++,
										r.call(e, t).then(function (t) {
											l || ((l = !0), (i[c] = t), --a || n(i));
										}, o);
								}),
									--a || n(i);
							});
						return i.error && o(i.value), r.promise;
					},
					race: function (t) {
						var e = this,
							r = j(e),
							n = r.reject,
							o = w(function () {
								var o = p(e.resolve);
								v(t, function (t) {
									o.call(e, t).then(r.resolve, n);
								});
							});
						return o.error && n(o.value), r.promise;
					},
				}
			);
	},
	function (t, e, r) {
		"use strict";
		r.d(e, "a", function () {
			return n;
		});
		r(22), r(34);
		class n {
			constructor() {
				this.listeners = [];
			}
			addListener(t) {
				this.listeners.push(t);
			}
			removeListener(t) {
				var e = this.listeners.indexOf(t);
				e > -1 && this.listeners.splice(e, 1);
			}
			hasListener(t) {
				return this.listeners.indexOf(t) > -1;
			}
			executeListeners(t) {
				for (const e of this.listeners) e(t);
			}
		}
	},
	function (t, e, r) {
		var n,
			o = (function () {
				var t = String.fromCharCode,
					e =
						"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
					r =
						"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$",
					n = {};
				function o(t, e) {
					if (!n[t]) {
						n[t] = {};
						for (var r = 0; r < t.length; r++) n[t][t.charAt(r)] = r;
					}
					return n[t][e];
				}
				var i = {
					compressToBase64: function (t) {
						if (null == t) return "";
						var r = i._compress(t, 6, function (t) {
							return e.charAt(t);
						});
						switch (r.length % 4) {
							default:
							case 0:
								return r;
							case 1:
								return r + "===";
							case 2:
								return r + "==";
							case 3:
								return r + "=";
						}
					},
					decompressFromBase64: function (t) {
						return null == t
							? ""
							: "" == t
							? null
							: i._decompress(t.length, 32, function (r) {
									return o(e, t.charAt(r));
							  });
					},
					compressToUTF16: function (e) {
						return null == e
							? ""
							: i._compress(e, 15, function (e) {
									return t(e + 32);
							  }) + " ";
					},
					decompressFromUTF16: function (t) {
						return null == t
							? ""
							: "" == t
							? null
							: i._decompress(t.length, 16384, function (e) {
									return t.charCodeAt(e) - 32;
							  });
					},
					compressToUint8Array: function (t) {
						for (
							var e = i.compress(t),
								r = new Uint8Array(2 * e.length),
								n = 0,
								o = e.length;
							n < o;
							n++
						) {
							var s = e.charCodeAt(n);
							(r[2 * n] = s >>> 8), (r[2 * n + 1] = s % 256);
						}
						return r;
					},
					decompressFromUint8Array: function (e) {
						if (null == e) return i.decompress(e);
						for (
							var r = new Array(e.length / 2), n = 0, o = r.length;
							n < o;
							n++
						)
							r[n] = 256 * e[2 * n] + e[2 * n + 1];
						var s = [];
						return (
							r.forEach(function (e) {
								s.push(t(e));
							}),
							i.decompress(s.join(""))
						);
					},
					compressToEncodedURIComponent: function (t) {
						return null == t
							? ""
							: i._compress(t, 6, function (t) {
									return r.charAt(t);
							  });
					},
					decompressFromEncodedURIComponent: function (t) {
						return null == t
							? ""
							: "" == t
							? null
							: ((t = t.replace(/ /g, "+")),
							  i._decompress(t.length, 32, function (e) {
									return o(r, t.charAt(e));
							  }));
					},
					compress: function (e) {
						return i._compress(e, 16, function (e) {
							return t(e);
						});
					},
					_compress: function (t, e, r) {
						if (null == t) return "";
						var n,
							o,
							i,
							s = {},
							a = {},
							c = "",
							l = "",
							u = "",
							f = 2,
							h = 3,
							d = 2,
							p = [],
							g = 0,
							m = 0;
						for (i = 0; i < t.length; i += 1)
							if (
								((c = t.charAt(i)),
								Object.prototype.hasOwnProperty.call(s, c) ||
									((s[c] = h++), (a[c] = !0)),
								(l = u + c),
								Object.prototype.hasOwnProperty.call(s, l))
							)
								u = l;
							else {
								if (Object.prototype.hasOwnProperty.call(a, u)) {
									if (u.charCodeAt(0) < 256) {
										for (n = 0; n < d; n++)
											(g <<= 1),
												m == e - 1 ? ((m = 0), p.push(r(g)), (g = 0)) : m++;
										for (o = u.charCodeAt(0), n = 0; n < 8; n++)
											(g = (g << 1) | (1 & o)),
												m == e - 1 ? ((m = 0), p.push(r(g)), (g = 0)) : m++,
												(o >>= 1);
									} else {
										for (o = 1, n = 0; n < d; n++)
											(g = (g << 1) | o),
												m == e - 1 ? ((m = 0), p.push(r(g)), (g = 0)) : m++,
												(o = 0);
										for (o = u.charCodeAt(0), n = 0; n < 16; n++)
											(g = (g << 1) | (1 & o)),
												m == e - 1 ? ((m = 0), p.push(r(g)), (g = 0)) : m++,
												(o >>= 1);
									}
									0 == --f && ((f = Math.pow(2, d)), d++), delete a[u];
								} else
									for (o = s[u], n = 0; n < d; n++)
										(g = (g << 1) | (1 & o)),
											m == e - 1 ? ((m = 0), p.push(r(g)), (g = 0)) : m++,
											(o >>= 1);
								0 == --f && ((f = Math.pow(2, d)), d++),
									(s[l] = h++),
									(u = String(c));
							}
						if ("" !== u) {
							if (Object.prototype.hasOwnProperty.call(a, u)) {
								if (u.charCodeAt(0) < 256) {
									for (n = 0; n < d; n++)
										(g <<= 1),
											m == e - 1 ? ((m = 0), p.push(r(g)), (g = 0)) : m++;
									for (o = u.charCodeAt(0), n = 0; n < 8; n++)
										(g = (g << 1) | (1 & o)),
											m == e - 1 ? ((m = 0), p.push(r(g)), (g = 0)) : m++,
											(o >>= 1);
								} else {
									for (o = 1, n = 0; n < d; n++)
										(g = (g << 1) | o),
											m == e - 1 ? ((m = 0), p.push(r(g)), (g = 0)) : m++,
											(o = 0);
									for (o = u.charCodeAt(0), n = 0; n < 16; n++)
										(g = (g << 1) | (1 & o)),
											m == e - 1 ? ((m = 0), p.push(r(g)), (g = 0)) : m++,
											(o >>= 1);
								}
								0 == --f && ((f = Math.pow(2, d)), d++), delete a[u];
							} else
								for (o = s[u], n = 0; n < d; n++)
									(g = (g << 1) | (1 & o)),
										m == e - 1 ? ((m = 0), p.push(r(g)), (g = 0)) : m++,
										(o >>= 1);
							0 == --f && ((f = Math.pow(2, d)), d++);
						}
						for (o = 2, n = 0; n < d; n++)
							(g = (g << 1) | (1 & o)),
								m == e - 1 ? ((m = 0), p.push(r(g)), (g = 0)) : m++,
								(o >>= 1);
						for (;;) {
							if (((g <<= 1), m == e - 1)) {
								p.push(r(g));
								break;
							}
							m++;
						}
						return p.join("");
					},
					decompress: function (t) {
						return null == t
							? ""
							: "" == t
							? null
							: i._decompress(t.length, 32768, function (e) {
									return t.charCodeAt(e);
							  });
					},
					_decompress: function (e, r, n) {
						var o,
							i,
							s,
							a,
							c,
							l,
							u,
							f = [],
							h = 4,
							d = 4,
							p = 3,
							g = "",
							m = [],
							v = { val: n(0), position: r, index: 1 };
						for (o = 0; o < 3; o += 1) f[o] = o;
						for (s = 0, c = Math.pow(2, 2), l = 1; l != c; )
							(a = v.val & v.position),
								(v.position >>= 1),
								0 == v.position && ((v.position = r), (v.val = n(v.index++))),
								(s |= (a > 0 ? 1 : 0) * l),
								(l <<= 1);
						switch (s) {
							case 0:
								for (s = 0, c = Math.pow(2, 8), l = 1; l != c; )
									(a = v.val & v.position),
										(v.position >>= 1),
										0 == v.position &&
											((v.position = r), (v.val = n(v.index++))),
										(s |= (a > 0 ? 1 : 0) * l),
										(l <<= 1);
								u = t(s);
								break;
							case 1:
								for (s = 0, c = Math.pow(2, 16), l = 1; l != c; )
									(a = v.val & v.position),
										(v.position >>= 1),
										0 == v.position &&
											((v.position = r), (v.val = n(v.index++))),
										(s |= (a > 0 ? 1 : 0) * l),
										(l <<= 1);
								u = t(s);
								break;
							case 2:
								return "";
						}
						for (f[3] = u, i = u, m.push(u); ; ) {
							if (v.index > e) return "";
							for (s = 0, c = Math.pow(2, p), l = 1; l != c; )
								(a = v.val & v.position),
									(v.position >>= 1),
									0 == v.position && ((v.position = r), (v.val = n(v.index++))),
									(s |= (a > 0 ? 1 : 0) * l),
									(l <<= 1);
							switch ((u = s)) {
								case 0:
									for (s = 0, c = Math.pow(2, 8), l = 1; l != c; )
										(a = v.val & v.position),
											(v.position >>= 1),
											0 == v.position &&
												((v.position = r), (v.val = n(v.index++))),
											(s |= (a > 0 ? 1 : 0) * l),
											(l <<= 1);
									(f[d++] = t(s)), (u = d - 1), h--;
									break;
								case 1:
									for (s = 0, c = Math.pow(2, 16), l = 1; l != c; )
										(a = v.val & v.position),
											(v.position >>= 1),
											0 == v.position &&
												((v.position = r), (v.val = n(v.index++))),
											(s |= (a > 0 ? 1 : 0) * l),
											(l <<= 1);
									(f[d++] = t(s)), (u = d - 1), h--;
									break;
								case 2:
									return m.join("");
							}
							if ((0 == h && ((h = Math.pow(2, p)), p++), f[u])) g = f[u];
							else {
								if (u !== d) return null;
								g = i + i.charAt(0);
							}
							m.push(g),
								(f[d++] = i + g.charAt(0)),
								(i = g),
								0 == --h && ((h = Math.pow(2, p)), p++);
						}
					},
				};
				return i;
			})();
		void 0 ===
			(n = function () {
				return o;
			}.call(e, r, e, t)) || (t.exports = n);
	},
	function (t, e, r) {
		var n = r(53),
			o = r(54),
			i = n("keys");
		t.exports = function (t) {
			return i[t] || (i[t] = o(t));
		};
	},
	function (t, e) {
		e.f = Object.getOwnPropertySymbols;
	},
	function (t, e, r) {
		t.exports = r(2);
	},
	function (t, e, r) {
		"use strict";
		var n = r(35),
			o = r(2),
			i = r(1);
		t.exports =
			n ||
			!i(function () {
				var t = Math.random();
				__defineSetter__.call(null, t, function () {}), delete o[t];
			});
	},
	function (t, e, r) {
		var n = r(7)("iterator"),
			o = !1;
		try {
			var i = 0,
				s = {
					next: function () {
						return { done: !!i++ };
					},
					return: function () {
						o = !0;
					},
				};
			(s[n] = function () {
				return this;
			}),
				Array.from(s, function () {
					throw 2;
				});
		} catch (t) {}
		t.exports = function (t, e) {
			if (!e && !o) return !1;
			var r = !1;
			try {
				var i = {};
				(i[n] = function () {
					return {
						next: function () {
							return { done: (r = !0) };
						},
					};
				}),
					t(i);
			} catch (t) {}
			return r;
		};
	},
	function (t, e, r) {
		var n = r(21),
			o = r(11),
			i = r(52),
			s = r(8);
		t.exports = function (t, e, r, a, c) {
			n(e);
			var l = o(t),
				u = i(l),
				f = s(l.length),
				h = c ? f - 1 : 0,
				d = c ? -1 : 1;
			if (r < 2)
				for (;;) {
					if (h in u) {
						(a = u[h]), (h += d);
						break;
					}
					if (((h += d), c ? h < 0 : f <= h))
						throw TypeError("Reduce of empty array with no initial value");
				}
			for (; c ? h >= 0 : f > h; h += d) h in u && (a = e(a, u[h], h, l));
			return a;
		};
	},
	function (t, e, r) {
		var n = r(25),
			o = r(19);
		t.exports = function (t, e, r) {
			var i,
				s,
				a = String(o(t)),
				c = n(e),
				l = a.length;
			return c < 0 || c >= l
				? r
					? ""
					: void 0
				: (i = a.charCodeAt(c)) < 55296 ||
				  i > 56319 ||
				  c + 1 === l ||
				  (s = a.charCodeAt(c + 1)) < 56320 ||
				  s > 57343
				? r
					? a.charAt(c)
					: i
				: r
				? a.slice(c, c + 2)
				: s - 56320 + ((i - 55296) << 10) + 65536;
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(14),
			o = r(16),
			i = r(1),
			s = r(7),
			a = r(77),
			c = s("species"),
			l = !i(function () {
				var t = /./;
				return (
					(t.exec = function () {
						var t = [];
						return (t.groups = { a: "7" }), t;
					}),
					"7" !== "".replace(t, "$<a>")
				);
			}),
			u = !i(function () {
				var t = /(?:)/,
					e = t.exec;
				t.exec = function () {
					return e.apply(this, arguments);
				};
				var r = "ab".split(t);
				return 2 !== r.length || "a" !== r[0] || "b" !== r[1];
			});
		t.exports = function (t, e, r, f) {
			var h = s(t),
				d = !i(function () {
					var e = {};
					return (
						(e[h] = function () {
							return 7;
						}),
						7 != ""[t](e)
					);
				}),
				p =
					d &&
					!i(function () {
						var e = !1,
							r = /a/;
						return (
							(r.exec = function () {
								return (e = !0), null;
							}),
							"split" === t &&
								((r.constructor = {}),
								(r.constructor[c] = function () {
									return r;
								})),
							r[h](""),
							!e
						);
					});
			if (!d || !p || ("replace" === t && !l) || ("split" === t && !u)) {
				var g = /./[h],
					m = r(h, ""[t], function (t, e, r, n, o) {
						return e.exec === a
							? d && !o
								? { done: !0, value: g.call(e, r, n) }
								: { done: !0, value: t.call(r, e, n) }
							: { done: !1 };
					}),
					v = m[0],
					y = m[1];
				o(String.prototype, t, v),
					o(
						RegExp.prototype,
						h,
						2 == e
							? function (t, e) {
									return y.call(t, this, e);
							  }
							: function (t) {
									return y.call(t, this);
							  }
					),
					f && n(RegExp.prototype[h], "sham", !0);
			}
		};
	},
	function (t, e, r) {
		"use strict";
		var n,
			o,
			i = r(65),
			s = RegExp.prototype.exec,
			a = String.prototype.replace,
			c = s,
			l =
				((n = /a/),
				(o = /b*/g),
				s.call(n, "a"),
				s.call(o, "a"),
				0 !== n.lastIndex || 0 !== o.lastIndex),
			u = void 0 !== /()??/.exec("")[1];
		(l || u) &&
			(c = function (t) {
				var e,
					r,
					n,
					o,
					c = this;
				return (
					u && (r = new RegExp("^" + c.source + "$(?!\\s)", i.call(c))),
					l && (e = c.lastIndex),
					(n = s.call(c, t)),
					l && n && (c.lastIndex = c.global ? n.index + n[0].length : e),
					u &&
						n &&
						n.length > 1 &&
						a.call(n[0], r, function () {
							for (o = 1; o < arguments.length - 2; o++)
								void 0 === arguments[o] && (n[o] = void 0);
						}),
					n
				);
			}),
			(t.exports = c);
	},
	function (t, e, r) {
		"use strict";
		var n = r(75);
		t.exports = function (t, e, r) {
			return e + (r ? n(t, e, !0).length : 1);
		};
	},
	function (t, e, r) {
		var n = r(28),
			o = r(77);
		t.exports = function (t, e) {
			var r = t.exec;
			if ("function" == typeof r) {
				var i = r.call(t, e);
				if ("object" != typeof i)
					throw TypeError(
						"RegExp exec method returned something other than an Object or null"
					);
				return i;
			}
			if ("RegExp" !== n(t))
				throw TypeError("RegExp#exec called on incompatible receiver");
			return o.call(t, e);
		};
	},
	function (t, e) {
		t.exports = "\t\n\v\f\r                　\u2028\u2029\ufeff";
	},
	function (t, e) {
		var r = Math.expm1;
		t.exports =
			!r ||
			r(10) > 22025.465794806718 ||
			r(10) < 22025.465794806718 ||
			-2e-17 != r(-2e-17)
				? function (t) {
						return 0 == (t = +t)
							? t
							: t > -1e-6 && t < 1e-6
							? t + (t * t) / 2
							: Math.exp(t) - 1;
				  }
				: r;
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(2),
			i = r(57),
			s = r(16),
			a = r(44),
			c = r(59),
			l = r(40),
			u = r(3),
			f = r(1),
			h = r(73),
			d = r(29),
			p = r(102);
		t.exports = function (t, e, r, g, m) {
			var v = o[t],
				y = v && v.prototype,
				b = v,
				S = g ? "set" : "add",
				_ = {},
				E = function (t) {
					var e = y[t];
					s(
						y,
						t,
						"add" == t
							? function (t) {
									return e.call(this, 0 === t ? 0 : t), this;
							  }
							: "delete" == t
							? function (t) {
									return !(m && !u(t)) && e.call(this, 0 === t ? 0 : t);
							  }
							: "get" == t
							? function (t) {
									return m && !u(t) ? void 0 : e.call(this, 0 === t ? 0 : t);
							  }
							: "has" == t
							? function (t) {
									return !(m && !u(t)) && e.call(this, 0 === t ? 0 : t);
							  }
							: function (t, r) {
									return e.call(this, 0 === t ? 0 : t, r), this;
							  }
					);
				};
			if (
				i(
					t,
					"function" != typeof v ||
						!(
							m ||
							(y.forEach &&
								!f(function () {
									new v().entries().next();
								}))
						)
				)
			)
				(b = r.getConstructor(e, t, g, S)), (a.REQUIRED = !0);
			else if (i(t, !0)) {
				var T = new b(),
					A = T[S](m ? {} : -0, 1) != T,
					w = f(function () {
						T.has(1);
					}),
					x = h(function (t) {
						new v(t);
					}),
					R =
						!m &&
						f(function () {
							for (var t = new v(), e = 5; e--; ) t[S](e, e);
							return !t.has(-0);
						});
				x ||
					(((b = e(function (e, r) {
						l(e, b, t);
						var n = p(new v(), e, b);
						return null != r && c(r, n[S], n, g), n;
					})).prototype = y),
					(y.constructor = b)),
					(w || R) && (E("delete"), E("has"), g && E("get")),
					(R || A) && E(S),
					m && y.clear && delete y.clear;
			}
			return (
				(_[t] = b),
				n({ global: !0, forced: b != v }, _),
				d(b, t),
				m || r.setStrong(b, t, g),
				b
			);
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(2),
			o = r(6),
			i = r(5).NATIVE_ARRAY_BUFFER,
			s = r(14),
			a = r(50),
			c = r(1),
			l = r(40),
			u = r(25),
			f = r(8),
			h = r(139),
			d = r(42).f,
			p = r(9).f,
			g = r(92),
			m = r(29),
			v = r(20),
			y = v.get,
			b = v.set,
			S = n.ArrayBuffer,
			_ = S,
			E = n.DataView,
			T = n.Math,
			A = n.RangeError,
			w = T.abs,
			x = T.pow,
			R = T.floor,
			I = T.log,
			C = T.LN2,
			O = function (t, e, r) {
				var n,
					o,
					i,
					s = new Array(r),
					a = 8 * r - e - 1,
					c = (1 << a) - 1,
					l = c >> 1,
					u = 23 === e ? x(2, -24) - x(2, -77) : 0,
					f = t < 0 || (0 === t && 1 / t < 0) ? 1 : 0,
					h = 0;
				for (
					(t = w(t)) != t || t === 1 / 0
						? ((o = t != t ? 1 : 0), (n = c))
						: ((n = R(I(t) / C)),
						  t * (i = x(2, -n)) < 1 && (n--, (i *= 2)),
						  (t += n + l >= 1 ? u / i : u * x(2, 1 - l)) * i >= 2 &&
								(n++, (i /= 2)),
						  n + l >= c
								? ((o = 0), (n = c))
								: n + l >= 1
								? ((o = (t * i - 1) * x(2, e)), (n += l))
								: ((o = t * x(2, l - 1) * x(2, e)), (n = 0)));
					e >= 8;
					s[h++] = 255 & o, o /= 256, e -= 8
				);
				for (
					n = (n << e) | o, a += e;
					a > 0;
					s[h++] = 255 & n, n /= 256, a -= 8
				);
				return (s[--h] |= 128 * f), s;
			},
			M = function (t, e) {
				var r,
					n = t.length,
					o = 8 * n - e - 1,
					i = (1 << o) - 1,
					s = i >> 1,
					a = o - 7,
					c = n - 1,
					l = t[c--],
					u = 127 & l;
				for (l >>= 7; a > 0; u = 256 * u + t[c], c--, a -= 8);
				for (
					r = u & ((1 << -a) - 1), u >>= -a, a += e;
					a > 0;
					r = 256 * r + t[c], c--, a -= 8
				);
				if (0 === u) u = 1 - s;
				else {
					if (u === i) return r ? NaN : l ? -1 / 0 : 1 / 0;
					(r += x(2, e)), (u -= s);
				}
				return (l ? -1 : 1) * r * x(2, u - e);
			},
			L = function (t) {
				return (t[3] << 24) | (t[2] << 16) | (t[1] << 8) | t[0];
			},
			N = function (t) {
				return [255 & t];
			},
			k = function (t) {
				return [255 & t, (t >> 8) & 255];
			},
			P = function (t) {
				return [255 & t, (t >> 8) & 255, (t >> 16) & 255, (t >> 24) & 255];
			},
			D = function (t) {
				return O(t, 23, 4);
			},
			U = function (t) {
				return O(t, 52, 8);
			},
			B = function (t, e) {
				p(t.prototype, e, {
					get: function () {
						return y(this)[e];
					},
				});
			},
			F = function (t, e, r, n) {
				var o = h(+r),
					i = y(t);
				if (o + e > i.byteLength) throw A("Wrong index");
				var s = y(i.buffer).bytes,
					a = o + i.byteOffset,
					c = s.slice(a, a + e);
				return n ? c : c.reverse();
			},
			G = function (t, e, r, n, o, i) {
				var s = h(+r),
					a = y(t);
				if (s + e > a.byteLength) throw A("Wrong index");
				for (
					var c = y(a.buffer).bytes, l = s + a.byteOffset, u = n(+o), f = 0;
					f < e;
					f++
				)
					c[l + f] = u[i ? f : e - f - 1];
			};
		if (i) {
			if (
				!c(function () {
					S(1);
				}) ||
				!c(function () {
					new S(-1);
				}) ||
				c(function () {
					return new S(), new S(1.5), new S(NaN), "ArrayBuffer" != S.name;
				})
			) {
				for (
					var j,
						H = ((_ = function (t) {
							return l(this, _), new S(h(t));
						}).prototype = S.prototype),
						W = d(S),
						z = 0;
					W.length > z;

				)
					(j = W[z++]) in _ || s(_, j, S[j]);
				H.constructor = _;
			}
			var Y = new E(new _(2)),
				V = E.prototype.setInt8;
			Y.setInt8(0, 2147483648),
				Y.setInt8(1, 2147483649),
				(!Y.getInt8(0) && Y.getInt8(1)) ||
					a(
						E.prototype,
						{
							setInt8: function (t, e) {
								V.call(this, t, (e << 24) >> 24);
							},
							setUint8: function (t, e) {
								V.call(this, t, (e << 24) >> 24);
							},
						},
						{ unsafe: !0 }
					);
		} else
			(_ = function (t) {
				l(this, _, "ArrayBuffer");
				var e = h(t);
				b(this, { bytes: g.call(new Array(e), 0), byteLength: e }),
					o || (this.byteLength = e);
			}),
				(E = function (t, e, r) {
					l(this, E, "DataView"), l(t, _, "DataView");
					var n = y(t).byteLength,
						i = u(e);
					if (i < 0 || i > n) throw A("Wrong offset");
					if (i + (r = void 0 === r ? n - i : f(r)) > n)
						throw A("Wrong length");
					b(this, { buffer: t, byteLength: r, byteOffset: i }),
						o ||
							((this.buffer = t), (this.byteLength = r), (this.byteOffset = i));
				}),
				o &&
					(B(_, "byteLength"),
					B(E, "buffer"),
					B(E, "byteLength"),
					B(E, "byteOffset")),
				a(E.prototype, {
					getInt8: function (t) {
						return (F(this, 1, t)[0] << 24) >> 24;
					},
					getUint8: function (t) {
						return F(this, 1, t)[0];
					},
					getInt16: function (t) {
						var e = F(this, 2, t, arguments[1]);
						return (((e[1] << 8) | e[0]) << 16) >> 16;
					},
					getUint16: function (t) {
						var e = F(this, 2, t, arguments[1]);
						return (e[1] << 8) | e[0];
					},
					getInt32: function (t) {
						return L(F(this, 4, t, arguments[1]));
					},
					getUint32: function (t) {
						return L(F(this, 4, t, arguments[1])) >>> 0;
					},
					getFloat32: function (t) {
						return M(F(this, 4, t, arguments[1]), 23);
					},
					getFloat64: function (t) {
						return M(F(this, 8, t, arguments[1]), 52);
					},
					setInt8: function (t, e) {
						G(this, 1, t, N, e);
					},
					setUint8: function (t, e) {
						G(this, 1, t, N, e);
					},
					setInt16: function (t, e) {
						G(this, 2, t, k, e, arguments[2]);
					},
					setUint16: function (t, e) {
						G(this, 2, t, k, e, arguments[2]);
					},
					setInt32: function (t, e) {
						G(this, 4, t, P, e, arguments[2]);
					},
					setUint32: function (t, e) {
						G(this, 4, t, P, e, arguments[2]);
					},
					setFloat32: function (t, e) {
						G(this, 4, t, D, e, arguments[2]);
					},
					setFloat64: function (t, e) {
						G(this, 8, t, U, e, arguments[2]);
					},
				});
		m(_, "ArrayBuffer"),
			m(E, "DataView"),
			(e.ArrayBuffer = _),
			(e.DataView = E);
	},
	function (t, e, r) {
		"use strict";
		r.d(e, "a", function () {
			return n;
		});
		r(22), r(34);
		class n {
			constructor() {
				this.listeners = [];
			}
			addListener(t) {
				this.listeners.push(t);
			}
			removeListener(t) {
				var e = this.listeners.indexOf(t);
				e > -1 && this.listeners.splice(e, 1);
			}
			hasListener(t) {
				return this.listeners.indexOf(t) > -1;
			}
			executeListeners(t) {
				for (const e of this.listeners) e(t);
			}
		}
	},
	function (t, e, r) {
		var n = r(2),
			o = r(3),
			i = n.document,
			s = o(i) && o(i.createElement);
		t.exports = function (t) {
			return s ? i.createElement(t) : {};
		};
	},
	function (t, e, r) {
		var n = r(2),
			o = r(14);
		t.exports = function (t, e) {
			try {
				o(n, t, e);
			} catch (r) {
				n[t] = e;
			}
			return e;
		};
	},
	function (t, e, r) {
		var n = r(2),
			o = r(42),
			i = r(70),
			s = r(4),
			a = n.Reflect;
		t.exports =
			(a && a.ownKeys) ||
			function (t) {
				var e = o.f(s(t)),
					r = i.f;
				return r ? e.concat(r(t)) : e;
			};
	},
	function (t, e) {
		t.exports = [
			"constructor",
			"hasOwnProperty",
			"isPrototypeOf",
			"propertyIsEnumerable",
			"toLocaleString",
			"toString",
			"valueOf",
		];
	},
	function (t, e, r) {
		var n = r(6),
			o = r(9),
			i = r(4),
			s = r(46);
		t.exports = n
			? Object.defineProperties
			: function (t, e) {
					i(t);
					for (var r, n = s(e), a = n.length, c = 0; a > c; )
						o.f(t, (r = n[c++]), e[r]);
					return t;
			  };
	},
	function (t, e, r) {
		var n = r(7),
			o = r(60),
			i = n("iterator"),
			s = Array.prototype;
		t.exports = function (t) {
			return void 0 !== t && (o.Array === t || s[i] === t);
		};
	},
	function (t, e, r) {
		var n = r(1);
		t.exports = !n(function () {
			function t() {}
			return (
				(t.prototype.constructor = null),
				Object.getPrototypeOf(new t()) !== t.prototype
			);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(11),
			o = r(36),
			i = r(8);
		t.exports = function (t) {
			for (
				var e = n(this),
					r = i(e.length),
					s = arguments.length,
					a = o(s > 1 ? arguments[1] : void 0, r),
					c = s > 2 ? arguments[2] : void 0,
					l = void 0 === c ? r : o(c, r);
				l > a;

			)
				e[a++] = t;
			return e;
		};
	},
	function (t, e, r) {
		var n = r(71),
			o = r(2),
			i = function (t) {
				return "function" == typeof t ? t : void 0;
			};
		t.exports = function (t, e) {
			return arguments.length < 2
				? i(n[t]) || i(o[t])
				: (n[t] && n[t][e]) || (o[t] && o[t][e]);
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(95),
			i = r(30),
			s = r(47),
			a = r(29),
			c = r(14),
			l = r(16),
			u = r(7),
			f = r(35),
			h = r(60),
			d = r(125),
			p = d.IteratorPrototype,
			g = d.BUGGY_SAFARI_ITERATORS,
			m = u("iterator"),
			v = function () {
				return this;
			};
		t.exports = function (t, e, r, u, d, y, b) {
			o(r, e, u);
			var S,
				_,
				E,
				T = function (t) {
					if (t === d && I) return I;
					if (!g && t in x) return x[t];
					switch (t) {
						case "keys":
						case "values":
						case "entries":
							return function () {
								return new r(this, t);
							};
					}
					return function () {
						return new r(this);
					};
				},
				A = e + " Iterator",
				w = !1,
				x = t.prototype,
				R = x[m] || x["@@iterator"] || (d && x[d]),
				I = (!g && R) || T(d),
				C = ("Array" == e && x.entries) || R;
			if (
				(C &&
					((S = i(C.call(new t()))),
					p !== Object.prototype &&
						S.next &&
						(f ||
							i(S) === p ||
							(s ? s(S, p) : "function" != typeof S[m] && c(S, m, v)),
						a(S, A, !0, !0),
						f && (h[A] = v))),
				"values" == d &&
					R &&
					"values" !== R.name &&
					((w = !0),
					(I = function () {
						return R.call(this);
					})),
				(f && !b) || x[m] === I || c(x, m, I),
				(h[e] = I),
				d)
			)
				if (
					((_ = {
						values: T("values"),
						keys: y ? I : T("keys"),
						entries: T("entries"),
					}),
					b)
				)
					for (E in _) (!g && !w && E in x) || l(x, E, _[E]);
				else n({ target: e, proto: !0, forced: g || w }, _);
			return _;
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(125).IteratorPrototype,
			o = r(37),
			i = r(41),
			s = r(29),
			a = r(60),
			c = function () {
				return this;
			};
		t.exports = function (t, e, r) {
			var l = e + " Iterator";
			return (
				(t.prototype = o(n, { next: i(1, r) })), s(t, l, !1, !0), (a[l] = c), t
			);
		};
	},
	function (t, e, r) {
		var n = r(97),
			o = r(19);
		t.exports = function (t, e, r) {
			if (n(e))
				throw TypeError("String.prototype." + r + " doesn't accept regex");
			return String(o(t));
		};
	},
	function (t, e, r) {
		var n = r(3),
			o = r(28),
			i = r(7)("match");
		t.exports = function (t) {
			var e;
			return n(t) && (void 0 !== (e = t[i]) ? !!e : "RegExp" == o(t));
		};
	},
	function (t, e, r) {
		var n = r(7)("match");
		t.exports = function (t) {
			var e = /./;
			try {
				"/./"[t](e);
			} catch (r) {
				try {
					return (e[n] = !1), "/./"[t](e);
				} catch (t) {}
			}
			return !1;
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(25),
			o = r(19);
		t.exports =
			"".repeat ||
			function (t) {
				var e = String(o(this)),
					r = "",
					i = n(t);
				if (i < 0 || i == 1 / 0)
					throw RangeError("Wrong number of repetitions");
				for (; i > 0; (i >>>= 1) && (e += e)) 1 & i && (r += e);
				return r;
			};
	},
	function (t, e, r) {
		var n = r(2).navigator;
		t.exports = (n && n.userAgent) || "";
	},
	function (t, e, r) {
		var n = r(1),
			o = r(80);
		t.exports = function (t) {
			return n(function () {
				return !!o[t]() || "​᠎" != "​᠎"[t]() || o[t].name !== t;
			});
		};
	},
	function (t, e, r) {
		var n = r(3),
			o = r(47);
		t.exports = function (t, e, r) {
			var i,
				s = e.constructor;
			return (
				s !== r &&
					"function" == typeof s &&
					(i = s.prototype) !== r.prototype &&
					n(i) &&
					o &&
					o(t, i),
				t
			);
		};
	},
	function (t, e) {
		t.exports =
			Math.sign ||
			function (t) {
				return 0 == (t = +t) || t != t ? t : t < 0 ? -1 : 1;
			};
	},
	function (t, e, r) {
		var n = r(2),
			o = r(1),
			i = r(73),
			s = r(5).NATIVE_ARRAY_BUFFER_VIEWS,
			a = n.ArrayBuffer,
			c = n.Int8Array;
		t.exports =
			!s ||
			!o(function () {
				c(1);
			}) ||
			!o(function () {
				new c(-1);
			}) ||
			!i(function (t) {
				new c(), new c(null), new c(1.5), new c(t);
			}, !0) ||
			o(function () {
				return 1 !== new c(new a(2), 1, void 0).length;
			});
	},
	function (t, e, r) {
		"use strict";
		r(128);
		var n,
			o = r(0),
			i = r(6),
			s = r(142),
			a = r(2),
			c = r(89),
			l = r(16),
			u = r(40),
			f = r(12),
			h = r(115),
			d = r(121),
			p = r(75),
			g = r(360),
			m = r(29),
			v = r(361),
			y = r(20),
			b = a.URL,
			S = v.URLSearchParams,
			_ = v.getState,
			E = y.set,
			T = y.getterFor("URL"),
			A = Math.pow,
			w = /[A-Za-z]/,
			x = /[\d+\-.A-Za-z]/,
			R = /\d/,
			I = /^(0x|0X)/,
			C = /^[0-7]+$/,
			O = /^\d+$/,
			M = /^[\dA-Fa-f]+$/,
			L = /[\u0000\u0009\u000A\u000D #%/:?@[\\]]/,
			N = /[\u0000\u0009\u000A\u000D #/:?@[\\]]/,
			k = /^[\u0000-\u001F ]+|[\u0000-\u001F ]+$/g,
			P = /[\u0009\u000A\u000D]/g,
			D = function (t, e) {
				var r, n, o;
				if ("[" == e.charAt(0)) {
					if ("]" != e.charAt(e.length - 1)) return "Invalid host";
					if (!(r = B(e.slice(1, -1)))) return "Invalid host";
					t.host = r;
				} else if (V(t)) {
					if (((e = g(e)), L.test(e))) return "Invalid host";
					if (null === (r = U(e))) return "Invalid host";
					t.host = r;
				} else {
					if (N.test(e)) return "Invalid host";
					for (r = "", n = d(e), o = 0; o < n.length; o++) r += z(n[o], G);
					t.host = r;
				}
			},
			U = function (t) {
				var e,
					r,
					n,
					o,
					i,
					s,
					a,
					c = t.split(".");
				if (("" == c[c.length - 1] && c.length && c.pop(), (e = c.length) > 4))
					return t;
				for (r = [], n = 0; n < e; n++) {
					if ("" == (o = c[n])) return t;
					if (
						((i = 10),
						o.length > 1 &&
							"0" == o.charAt(0) &&
							((i = I.test(o) ? 16 : 8), (o = o.slice(8 == i ? 1 : 2))),
						"" === o)
					)
						s = 0;
					else {
						if (!(10 == i ? O : 8 == i ? C : M).test(o)) return t;
						s = parseInt(o, i);
					}
					r.push(s);
				}
				for (n = 0; n < e; n++)
					if (((s = r[n]), n == e - 1)) {
						if (s >= A(256, 5 - e)) return null;
					} else if (s > 255) return null;
				for (a = r.pop(), n = 0; n < r.length; n++) a += r[n] * A(256, 3 - n);
				return a;
			},
			B = function (t) {
				var e,
					r,
					n,
					o,
					i,
					s,
					a,
					c = [0, 0, 0, 0, 0, 0, 0, 0],
					l = 0,
					u = null,
					f = 0,
					h = function () {
						return t.charAt(f);
					};
				if (":" == h()) {
					if (":" != t.charAt(1)) return;
					(f += 2), (u = ++l);
				}
				for (; h(); ) {
					if (8 == l) return;
					if (":" != h()) {
						for (e = r = 0; r < 4 && M.test(h()); )
							(e = 16 * e + parseInt(h(), 16)), f++, r++;
						if ("." == h()) {
							if (0 == r) return;
							if (((f -= r), l > 6)) return;
							for (n = 0; h(); ) {
								if (((o = null), n > 0)) {
									if (!("." == h() && n < 4)) return;
									f++;
								}
								if (!R.test(h())) return;
								for (; R.test(h()); ) {
									if (((i = parseInt(h(), 10)), null === o)) o = i;
									else {
										if (0 == o) return;
										o = 10 * o + i;
									}
									if (o > 255) return;
									f++;
								}
								(c[l] = 256 * c[l] + o), (2 != ++n && 4 != n) || l++;
							}
							if (4 != n) return;
							break;
						}
						if (":" == h()) {
							if ((f++, !h())) return;
						} else if (h()) return;
						c[l++] = e;
					} else {
						if (null !== u) return;
						f++, (u = ++l);
					}
				}
				if (null !== u)
					for (s = l - u, l = 7; 0 != l && s > 0; )
						(a = c[l]), (c[l--] = c[u + s - 1]), (c[u + --s] = a);
				else if (8 != l) return;
				return c;
			},
			F = function (t) {
				var e, r, n, o;
				if ("number" == typeof t) {
					for (e = [], r = 0; r < 4; r++)
						e.unshift(t % 256), (t = Math.floor(t / 256));
					return e.join(".");
				}
				if ("object" == typeof t) {
					for (
						e = "",
							n = (function (t) {
								for (var e = null, r = 1, n = null, o = 0, i = 0; i < 8; i++)
									0 !== t[i]
										? (o > r && ((e = n), (r = o)), (n = null), (o = 0))
										: (null === n && (n = i), ++o);
								return o > r && ((e = n), (r = o)), e;
							})(t),
							r = 0;
						r < 8;
						r++
					)
						(o && 0 === t[r]) ||
							(o && (o = !1),
							n === r
								? ((e += r ? ":" : "::"), (o = !0))
								: ((e += t[r].toString(16)), r < 7 && (e += ":")));
					return "[" + e + "]";
				}
				return t;
			},
			G = {},
			j = h({}, G, { " ": 1, '"': 1, "<": 1, ">": 1, "`": 1 }),
			H = h({}, j, { "#": 1, "?": 1, "{": 1, "}": 1 }),
			W = h({}, H, {
				"/": 1,
				":": 1,
				";": 1,
				"=": 1,
				"@": 1,
				"[": 1,
				"\\": 1,
				"]": 1,
				"^": 1,
				"|": 1,
			}),
			z = function (t, e) {
				var r = p(t, 0);
				return r > 32 && r < 127 && !f(e, t) ? t : encodeURIComponent(t);
			},
			Y = {
				ftp: 21,
				file: null,
				gopher: 70,
				http: 80,
				https: 443,
				ws: 80,
				wss: 443,
			},
			V = function (t) {
				return f(Y, t.scheme);
			},
			q = function (t) {
				return "" != t.username || "" != t.password;
			},
			K = function (t) {
				return !t.host || t.cannotBeABaseURL || "file" == t.scheme;
			},
			X = function (t, e) {
				var r;
				return (
					2 == t.length &&
					w.test(t.charAt(0)) &&
					(":" == (r = t.charAt(1)) || (!e && "|" == r))
				);
			},
			Z = function (t) {
				var e;
				return (
					t.length > 1 &&
					X(t.slice(0, 2)) &&
					(2 == t.length ||
						"/" === (e = t.charAt(2)) ||
						"\\" === e ||
						"?" === e ||
						"#" === e)
				);
			},
			J = function (t) {
				var e = t.path,
					r = e.length;
				!r || ("file" == t.scheme && 1 == r && X(e[0], !0)) || e.pop();
			},
			Q = function (t) {
				return "." === t || "%2e" === t.toLowerCase();
			},
			$ = {},
			tt = {},
			et = {},
			rt = {},
			nt = {},
			ot = {},
			it = {},
			st = {},
			at = {},
			ct = {},
			lt = {},
			ut = {},
			ft = {},
			ht = {},
			dt = {},
			pt = {},
			gt = {},
			mt = {},
			vt = {},
			yt = {},
			bt = {},
			St = function (t, e, r, o) {
				var i,
					s,
					a,
					c,
					l,
					u = r || $,
					h = 0,
					p = "",
					g = !1,
					m = !1,
					v = !1;
				for (
					r ||
						((t.scheme = ""),
						(t.username = ""),
						(t.password = ""),
						(t.host = null),
						(t.port = null),
						(t.path = []),
						(t.query = null),
						(t.fragment = null),
						(t.cannotBeABaseURL = !1),
						(e = e.replace(k, ""))),
						e = e.replace(P, ""),
						i = d(e);
					h <= i.length;

				) {
					switch (((s = i[h]), u)) {
						case $:
							if (!s || !w.test(s)) {
								if (r) return "Invalid scheme";
								u = et;
								continue;
							}
							(p += s.toLowerCase()), (u = tt);
							break;
						case tt:
							if (s && (x.test(s) || "+" == s || "-" == s || "." == s))
								p += s.toLowerCase();
							else {
								if (":" != s) {
									if (r) return "Invalid scheme";
									(p = ""), (u = et), (h = 0);
									continue;
								}
								if (
									r &&
									(V(t) != f(Y, p) ||
										("file" == p && (q(t) || null !== t.port)) ||
										("file" == t.scheme && !t.host))
								)
									return;
								if (((t.scheme = p), r))
									return void (
										V(t) &&
										Y[t.scheme] == t.port &&
										(t.port = null)
									);
								(p = ""),
									"file" == t.scheme
										? (u = ht)
										: V(t) && o && o.scheme == t.scheme
										? (u = rt)
										: V(t)
										? (u = st)
										: "/" == i[h + 1]
										? ((u = nt), h++)
										: ((t.cannotBeABaseURL = !0), t.path.push(""), (u = vt));
							}
							break;
						case et:
							if (!o || (o.cannotBeABaseURL && "#" != s))
								return "Invalid scheme";
							if (o.cannotBeABaseURL && "#" == s) {
								(t.scheme = o.scheme),
									(t.path = o.path.slice()),
									(t.query = o.query),
									(t.fragment = ""),
									(t.cannotBeABaseURL = !0),
									(u = bt);
								break;
							}
							u = "file" == o.scheme ? ht : ot;
							continue;
						case rt:
							if ("/" != s || "/" != i[h + 1]) {
								u = ot;
								continue;
							}
							(u = at), h++;
							break;
						case nt:
							if ("/" == s) {
								u = ct;
								break;
							}
							u = mt;
							continue;
						case ot:
							if (((t.scheme = o.scheme), s == n))
								(t.username = o.username),
									(t.password = o.password),
									(t.host = o.host),
									(t.port = o.port),
									(t.path = o.path.slice()),
									(t.query = o.query);
							else if ("/" == s || ("\\" == s && V(t))) u = it;
							else if ("?" == s)
								(t.username = o.username),
									(t.password = o.password),
									(t.host = o.host),
									(t.port = o.port),
									(t.path = o.path.slice()),
									(t.query = ""),
									(u = yt);
							else {
								if ("#" != s) {
									(t.username = o.username),
										(t.password = o.password),
										(t.host = o.host),
										(t.port = o.port),
										(t.path = o.path.slice()),
										t.path.pop(),
										(u = mt);
									continue;
								}
								(t.username = o.username),
									(t.password = o.password),
									(t.host = o.host),
									(t.port = o.port),
									(t.path = o.path.slice()),
									(t.query = o.query),
									(t.fragment = ""),
									(u = bt);
							}
							break;
						case it:
							if (!V(t) || ("/" != s && "\\" != s)) {
								if ("/" != s) {
									(t.username = o.username),
										(t.password = o.password),
										(t.host = o.host),
										(t.port = o.port),
										(u = mt);
									continue;
								}
								u = ct;
							} else u = at;
							break;
						case st:
							if (((u = at), "/" != s || "/" != p.charAt(h + 1))) continue;
							h++;
							break;
						case at:
							if ("/" != s && "\\" != s) {
								u = ct;
								continue;
							}
							break;
						case ct:
							if ("@" == s) {
								g && (p = "%40" + p), (g = !0), (a = d(p));
								for (var y = 0; y < a.length; y++) {
									var b = a[y];
									if (":" != b || v) {
										var S = z(b, W);
										v ? (t.password += S) : (t.username += S);
									} else v = !0;
								}
								p = "";
							} else if (
								s == n ||
								"/" == s ||
								"?" == s ||
								"#" == s ||
								("\\" == s && V(t))
							) {
								if (g && "" == p) return "Invalid authority";
								(h -= d(p).length + 1), (p = ""), (u = lt);
							} else p += s;
							break;
						case lt:
						case ut:
							if (r && "file" == t.scheme) {
								u = pt;
								continue;
							}
							if (":" != s || m) {
								if (
									s == n ||
									"/" == s ||
									"?" == s ||
									"#" == s ||
									("\\" == s && V(t))
								) {
									if (V(t) && "" == p) return "Invalid host";
									if (r && "" == p && (q(t) || null !== t.port)) return;
									if ((c = D(t, p))) return c;
									if (((p = ""), (u = gt), r)) return;
									continue;
								}
								"[" == s ? (m = !0) : "]" == s && (m = !1), (p += s);
							} else {
								if ("" == p) return "Invalid host";
								if ((c = D(t, p))) return c;
								if (((p = ""), (u = ft), r == ut)) return;
							}
							break;
						case ft:
							if (!R.test(s)) {
								if (
									s == n ||
									"/" == s ||
									"?" == s ||
									"#" == s ||
									("\\" == s && V(t)) ||
									r
								) {
									if ("" != p) {
										var _ = parseInt(p, 10);
										if (_ > 65535) return "Invalid port";
										(t.port = V(t) && _ === Y[t.scheme] ? null : _), (p = "");
									}
									if (r) return;
									u = gt;
									continue;
								}
								return "Invalid port";
							}
							p += s;
							break;
						case ht:
							if (((t.scheme = "file"), "/" == s || "\\" == s)) u = dt;
							else {
								if (!o || "file" != o.scheme) {
									u = mt;
									continue;
								}
								if (s == n)
									(t.host = o.host),
										(t.path = o.path.slice()),
										(t.query = o.query);
								else if ("?" == s)
									(t.host = o.host),
										(t.path = o.path.slice()),
										(t.query = ""),
										(u = yt);
								else {
									if ("#" != s) {
										Z(i.slice(h).join("")) ||
											((t.host = o.host), (t.path = o.path.slice()), J(t)),
											(u = mt);
										continue;
									}
									(t.host = o.host),
										(t.path = o.path.slice()),
										(t.query = o.query),
										(t.fragment = ""),
										(u = bt);
								}
							}
							break;
						case dt:
							if ("/" == s || "\\" == s) {
								u = pt;
								break;
							}
							o &&
								"file" == o.scheme &&
								!Z(i.slice(h).join("")) &&
								(X(o.path[0], !0) ? t.path.push(o.path[0]) : (t.host = o.host)),
								(u = mt);
							continue;
						case pt:
							if (s == n || "/" == s || "\\" == s || "?" == s || "#" == s) {
								if (!r && X(p)) u = mt;
								else if ("" == p) {
									if (((t.host = ""), r)) return;
									u = gt;
								} else {
									if ((c = D(t, p))) return c;
									if (("localhost" == t.host && (t.host = ""), r)) return;
									(p = ""), (u = gt);
								}
								continue;
							}
							p += s;
							break;
						case gt:
							if (V(t)) {
								if (((u = mt), "/" != s && "\\" != s)) continue;
							} else if (r || "?" != s)
								if (r || "#" != s) {
									if (s != n && ((u = mt), "/" != s)) continue;
								} else (t.fragment = ""), (u = bt);
							else (t.query = ""), (u = yt);
							break;
						case mt:
							if (
								s == n ||
								"/" == s ||
								("\\" == s && V(t)) ||
								(!r && ("?" == s || "#" == s))
							) {
								if (
									(".." === (l = (l = p).toLowerCase()) ||
									"%2e." === l ||
									".%2e" === l ||
									"%2e%2e" === l
										? (J(t), "/" == s || ("\\" == s && V(t)) || t.path.push(""))
										: Q(p)
										? "/" == s || ("\\" == s && V(t)) || t.path.push("")
										: ("file" == t.scheme &&
												!t.path.length &&
												X(p) &&
												(t.host && (t.host = ""), (p = p.charAt(0) + ":")),
										  t.path.push(p)),
									(p = ""),
									"file" == t.scheme && (s == n || "?" == s || "#" == s))
								)
									for (; t.path.length > 1 && "" === t.path[0]; )
										t.path.shift();
								"?" == s
									? ((t.query = ""), (u = yt))
									: "#" == s && ((t.fragment = ""), (u = bt));
							} else p += z(s, H);
							break;
						case vt:
							"?" == s
								? ((t.query = ""), (u = yt))
								: "#" == s
								? ((t.fragment = ""), (u = bt))
								: s != n && (t.path[0] += z(s, G));
							break;
						case yt:
							r || "#" != s
								? s != n &&
								  ("'" == s && V(t)
										? (t.query += "%27")
										: (t.query += "#" == s ? "%23" : z(s, G)))
								: ((t.fragment = ""), (u = bt));
							break;
						case bt:
							s != n && (t.fragment += z(s, j));
					}
					h++;
				}
			},
			_t = function (t) {
				var e,
					r,
					n = u(this, _t, "URL"),
					o = arguments.length > 1 ? arguments[1] : void 0,
					s = String(t),
					a = E(n, { type: "URL" });
				if (void 0 !== o)
					if (o instanceof _t) e = T(o);
					else if ((r = St((e = {}), String(o)))) throw TypeError(r);
				if ((r = St(a, s, null, e))) throw TypeError(r);
				var c = (a.searchParams = new S()),
					l = _(c);
				l.updateSearchParams(a.query),
					(l.updateURL = function () {
						a.query = String(c) || null;
					}),
					i ||
						((n.href = Tt.call(n)),
						(n.origin = At.call(n)),
						(n.protocol = wt.call(n)),
						(n.username = xt.call(n)),
						(n.password = Rt.call(n)),
						(n.host = It.call(n)),
						(n.hostname = Ct.call(n)),
						(n.port = Ot.call(n)),
						(n.pathname = Mt.call(n)),
						(n.search = Lt.call(n)),
						(n.searchParams = Nt.call(n)),
						(n.hash = kt.call(n)));
			},
			Et = _t.prototype,
			Tt = function () {
				var t = T(this),
					e = t.scheme,
					r = t.username,
					n = t.password,
					o = t.host,
					i = t.port,
					s = t.path,
					a = t.query,
					c = t.fragment,
					l = e + ":";
				return (
					null !== o
						? ((l += "//"),
						  q(t) && (l += r + (n ? ":" + n : "") + "@"),
						  (l += F(o)),
						  null !== i && (l += ":" + i))
						: "file" == e && (l += "//"),
					(l += t.cannotBeABaseURL ? s[0] : s.length ? "/" + s.join("/") : ""),
					null !== a && (l += "?" + a),
					null !== c && (l += "#" + c),
					l
				);
			},
			At = function () {
				var t = T(this),
					e = t.scheme,
					r = t.port;
				if ("blob" == e)
					try {
						return new URL(e.path[0]).origin;
					} catch (t) {
						return "null";
					}
				return "file" != e && V(t)
					? e + "://" + F(t.host) + (null !== r ? ":" + r : "")
					: "null";
			},
			wt = function () {
				return T(this).scheme + ":";
			},
			xt = function () {
				return T(this).username;
			},
			Rt = function () {
				return T(this).password;
			},
			It = function () {
				var t = T(this),
					e = t.host,
					r = t.port;
				return null === e ? "" : null === r ? F(e) : F(e) + ":" + r;
			},
			Ct = function () {
				var t = T(this).host;
				return null === t ? "" : F(t);
			},
			Ot = function () {
				var t = T(this).port;
				return null === t ? "" : String(t);
			},
			Mt = function () {
				var t = T(this),
					e = t.path;
				return t.cannotBeABaseURL ? e[0] : e.length ? "/" + e.join("/") : "";
			},
			Lt = function () {
				var t = T(this).query;
				return t ? "?" + t : "";
			},
			Nt = function () {
				return T(this).searchParams;
			},
			kt = function () {
				var t = T(this).fragment;
				return t ? "#" + t : "";
			},
			Pt = function (t, e) {
				return { get: t, set: e, configurable: !0, enumerable: !0 };
			};
		if (
			(i &&
				c(Et, {
					href: Pt(Tt, function (t) {
						var e = T(this),
							r = String(t),
							n = St(e, r);
						if (n) throw TypeError(n);
						_(e.searchParams).updateSearchParams(e.query);
					}),
					origin: Pt(At),
					protocol: Pt(wt, function (t) {
						var e = T(this);
						St(e, String(t) + ":", $);
					}),
					username: Pt(xt, function (t) {
						var e = T(this),
							r = d(String(t));
						if (!K(e)) {
							e.username = "";
							for (var n = 0; n < r.length; n++) e.username += z(r[n], W);
						}
					}),
					password: Pt(Rt, function (t) {
						var e = T(this),
							r = d(String(t));
						if (!K(e)) {
							e.password = "";
							for (var n = 0; n < r.length; n++) e.password += z(r[n], W);
						}
					}),
					host: Pt(It, function (t) {
						var e = T(this);
						e.cannotBeABaseURL || St(e, String(t), lt);
					}),
					hostname: Pt(Ct, function (t) {
						var e = T(this);
						e.cannotBeABaseURL || St(e, String(t), ut);
					}),
					port: Pt(Ot, function (t) {
						var e = T(this);
						K(e) || ("" == (t = String(t)) ? (e.port = null) : St(e, t, ft));
					}),
					pathname: Pt(Mt, function (t) {
						var e = T(this);
						e.cannotBeABaseURL || ((e.path = []), St(e, t + "", gt));
					}),
					search: Pt(Lt, function (t) {
						var e = T(this);
						"" == (t = String(t))
							? (e.query = null)
							: ("?" == t.charAt(0) && (t = t.slice(1)),
							  (e.query = ""),
							  St(e, t, yt)),
							_(e.searchParams).updateSearchParams(e.query);
					}),
					searchParams: Pt(Nt),
					hash: Pt(kt, function (t) {
						var e = T(this);
						"" != (t = String(t))
							? ("#" == t.charAt(0) && (t = t.slice(1)),
							  (e.fragment = ""),
							  St(e, t, bt))
							: (e.fragment = null);
					}),
				}),
			l(
				Et,
				"toJSON",
				function () {
					return Tt.call(this);
				},
				{ enumerable: !0 }
			),
			l(
				Et,
				"toString",
				function () {
					return Tt.call(this);
				},
				{ enumerable: !0 }
			),
			b)
		) {
			var Dt = b.createObjectURL,
				Ut = b.revokeObjectURL;
			Dt &&
				l(_t, "createObjectURL", function (t) {
					return Dt.apply(b, arguments);
				}),
				Ut &&
					l(_t, "revokeObjectURL", function (t) {
						return Ut.apply(b, arguments);
					});
		}
		m(_t, "URL"), o({ global: !0, forced: !s, sham: !i }, { URL: _t });
	},
	function (t, e, r) {
		var n = r(6),
			o = r(1),
			i = r(85);
		t.exports =
			!n &&
			!o(function () {
				return (
					7 !=
					Object.defineProperty(i("div"), "a", {
						get: function () {
							return 7;
						},
					}).a
				);
			});
	},
	function (t, e, r) {
		var n = r(53);
		t.exports = n("native-function-to-string", Function.toString);
	},
	function (t, e, r) {
		var n = r(2),
			o = r(107),
			i = n.WeakMap;
		t.exports = "function" == typeof i && /native code/.test(o.call(i));
	},
	function (t, e, r) {
		var n = r(12),
			o = r(87),
			i = r(15),
			s = r(9);
		t.exports = function (t, e) {
			for (var r = o(e), a = s.f, c = i.f, l = 0; l < r.length; l++) {
				var u = r[l];
				n(t, u) || a(t, u, c(e, u));
			}
		};
	},
	function (t, e, r) {
		var n = r(12),
			o = r(18),
			i = r(56),
			s = r(55),
			a = i(!1);
		t.exports = function (t, e) {
			var r,
				i = o(t),
				c = 0,
				l = [];
			for (r in i) !n(s, r) && n(i, r) && l.push(r);
			for (; e.length > c; ) n(i, (r = e[c++])) && (~a(l, r) || l.push(r));
			return l;
		};
	},
	function (t, e, r) {
		var n = r(1);
		t.exports =
			!!Object.getOwnPropertySymbols &&
			!n(function () {
				return !String(Symbol());
			});
	},
	function (t, e, r) {
		e.f = r(7);
	},
	function (t, e, r) {
		var n = r(2).document;
		t.exports = n && n.documentElement;
	},
	function (t, e, r) {
		var n = r(18),
			o = r(42).f,
			i = {}.toString,
			s =
				"object" == typeof window && window && Object.getOwnPropertyNames
					? Object.getOwnPropertyNames(window)
					: [];
		t.exports.f = function (t) {
			return s && "[object Window]" == i.call(t)
				? (function (t) {
						try {
							return o(t);
						} catch (t) {
							return s.slice();
						}
				  })(t)
				: o(n(t));
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(6),
			o = r(1),
			i = r(46),
			s = r(70),
			a = r(51),
			c = r(11),
			l = r(52),
			u = Object.assign;
		t.exports =
			!u ||
			o(function () {
				var t = {},
					e = {},
					r = Symbol();
				return (
					(t[r] = 7),
					"abcdefghijklmnopqrst".split("").forEach(function (t) {
						e[t] = t;
					}),
					7 != u({}, t)[r] || "abcdefghijklmnopqrst" != i(u({}, e)).join("")
				);
			})
				? function (t, e) {
						for (
							var r = c(t), o = arguments.length, u = 1, f = s.f, h = a.f;
							o > u;

						)
							for (
								var d,
									p = l(arguments[u++]),
									g = f ? i(p).concat(f(p)) : i(p),
									m = g.length,
									v = 0;
								m > v;

							)
								(d = g[v++]), (n && !h.call(p, d)) || (r[d] = p[d]);
						return r;
				  }
				: u;
	},
	function (t, e, r) {
		var n = r(6),
			o = r(46),
			i = r(18),
			s = r(51).f;
		t.exports = function (t, e) {
			for (var r, a = i(t), c = o(a), l = c.length, u = 0, f = []; l > u; )
				(r = c[u++]), (n && !s.call(a, r)) || f.push(e ? [r, a[r]] : a[r]);
			return f;
		};
	},
	function (t, e, r) {
		var n = r(4);
		t.exports = function (t, e, r, o) {
			try {
				return o ? e(n(r)[0], r[1]) : e(r);
			} catch (e) {
				var i = t.return;
				throw (void 0 !== i && n(i.call(t)), e);
			}
		};
	},
	function (t, e) {
		t.exports =
			Object.is ||
			function (t, e) {
				return t === e ? 0 !== t || 1 / t == 1 / e : t != t && e != e;
			};
	},
	function (t, e, r) {
		var n = r(3),
			o = r(4);
		t.exports = function (t, e) {
			if ((o(t), !n(e) && null !== e))
				throw TypeError("Can't set " + String(e) + " as a prototype");
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(21),
			o = r(3),
			i = [].slice,
			s = {},
			a = function (t, e, r) {
				if (!(e in s)) {
					for (var n = [], o = 0; o < e; o++) n[o] = "a[" + o + "]";
					s[e] = Function("C,a", "return new C(" + n.join(",") + ")");
				}
				return s[e](t, r);
			};
		t.exports =
			Function.bind ||
			function (t) {
				var e = n(this),
					r = i.call(arguments, 1),
					s = function () {
						var n = r.concat(i.call(arguments));
						return this instanceof s ? a(e, n.length, n) : e.apply(t, n);
					};
				return o(e.prototype) && (s.prototype = e.prototype), s;
			};
	},
	function (t, e, r) {
		"use strict";
		var n = r(38),
			o = r(11),
			i = r(117),
			s = r(90),
			a = r(8),
			c = r(45),
			l = r(61);
		t.exports = function (t) {
			var e,
				r,
				u,
				f,
				h = o(t),
				d = "function" == typeof this ? this : Array,
				p = arguments.length,
				g = p > 1 ? arguments[1] : void 0,
				m = void 0 !== g,
				v = 0,
				y = l(h);
			if (
				(m && (g = n(g, p > 2 ? arguments[2] : void 0, 2)),
				null == y || (d == Array && s(y)))
			)
				for (r = new d((e = a(h.length))); e > v; v++)
					c(r, v, m ? g(h[v], v) : h[v]);
			else
				for (f = y.call(h), r = new d(); !(u = f.next()).done; v++)
					c(r, v, m ? i(f, g, [u.value, v], !0) : u.value);
			return (r.length = v), r;
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(11),
			o = r(36),
			i = r(8);
		t.exports =
			[].copyWithin ||
			function (t, e) {
				var r = n(this),
					s = i(r.length),
					a = o(t, s),
					c = o(e, s),
					l = arguments.length > 2 ? arguments[2] : void 0,
					u = Math.min((void 0 === l ? s : o(l, s)) - c, s - a),
					f = 1;
				for (
					c < a && a < c + u && ((f = -1), (c += u - 1), (a += u - 1));
					u-- > 0;

				)
					c in r ? (r[a] = r[c]) : delete r[a], (a += f), (c += f);
				return r;
			};
	},
	function (t, e, r) {
		"use strict";
		var n = r(43),
			o = r(8),
			i = r(38),
			s = function (t, e, r, a, c, l, u, f) {
				for (var h, d = c, p = 0, g = !!u && i(u, f, 3); p < a; ) {
					if (p in r) {
						if (((h = g ? g(r[p], p, e) : r[p]), l > 0 && n(h)))
							d = s(t, e, h, o(h.length), d, l - 1) - 1;
						else {
							if (d >= 9007199254740991)
								throw TypeError("Exceed the acceptable array length");
							t[d] = h;
						}
						d++;
					}
					p++;
				}
				return d;
			};
		t.exports = s;
	},
	function (t, e, r) {
		"use strict";
		var n = r(18),
			o = r(25),
			i = r(8),
			s = r(31),
			a = [].lastIndexOf,
			c = !!a && 1 / [1].lastIndexOf(1, -0) < 0,
			l = s("lastIndexOf");
		t.exports =
			c || l
				? function (t) {
						if (c) return a.apply(this, arguments) || 0;
						var e = n(this),
							r = i(e.length),
							s = r - 1;
						for (
							arguments.length > 1 && (s = Math.min(s, o(arguments[1]))),
								s < 0 && (s = r + s);
							s >= 0;
							s--
						)
							if (s in e && e[s] === t) return s || 0;
						return -1;
				  }
				: a;
	},
	function (t, e, r) {
		"use strict";
		var n,
			o,
			i,
			s = r(30),
			a = r(14),
			c = r(12),
			l = r(7),
			u = r(35),
			f = l("iterator"),
			h = !1;
		[].keys &&
			("next" in (i = [].keys())
				? (o = s(s(i))) !== Object.prototype && (n = o)
				: (h = !0)),
			null == n && (n = {}),
			u ||
				c(n, f) ||
				a(n, f, function () {
					return this;
				}),
			(t.exports = { IteratorPrototype: n, BUGGY_SAFARI_ITERATORS: h });
	},
	function (t, e, r) {
		var n = r(8),
			o = r(99),
			i = r(19);
		t.exports = function (t, e, r, s) {
			var a,
				c,
				l = String(i(t)),
				u = l.length,
				f = void 0 === r ? " " : String(r),
				h = n(e);
			return h <= u || "" == f
				? l
				: ((a = h - u),
				  (c = o.call(f, Math.ceil(a / f.length))).length > a &&
						(c = c.slice(0, a)),
				  s ? c + l : l + c);
		};
	},
	function (t, e, r) {
		var n = r(100);
		t.exports = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(n);
	},
	function (t, e, r) {
		"use strict";
		var n = r(75),
			o = r(20),
			i = r(94),
			s = o.set,
			a = o.getterFor("String Iterator");
		i(
			String,
			"String",
			function (t) {
				s(this, { type: "String Iterator", string: String(t), index: 0 });
			},
			function () {
				var t,
					e = a(this),
					r = e.string,
					o = e.index;
				return o >= r.length
					? { value: void 0, done: !0 }
					: ((t = n(r, o, !0)), (e.index += t.length), { value: t, done: !1 });
			}
		);
	},
	function (t, e, r) {
		var n = r(2),
			o = r(49),
			i = r(80),
			s = n.parseInt,
			a = /^[+-]?0[Xx]/,
			c = 8 !== s(i + "08") || 22 !== s(i + "0x16");
		t.exports = c
			? function (t, e) {
					var r = o(String(t), 3);
					return s(r, e >>> 0 || (a.test(r) ? 16 : 10));
			  }
			: s;
	},
	function (t, e, r) {
		var n = r(2),
			o = r(49),
			i = r(80),
			s = n.parseFloat,
			a = 1 / s(i + "-0") != -1 / 0;
		t.exports = a
			? function (t) {
					var e = o(String(t), 3),
						r = s(e);
					return 0 === r && "-" == e.charAt(0) ? -0 : r;
			  }
			: s;
	},
	function (t, e, r) {
		var n = r(3),
			o = Math.floor;
		t.exports = function (t) {
			return !n(t) && isFinite(t) && o(t) === t;
		};
	},
	function (t, e, r) {
		var n = r(28);
		t.exports = function (t) {
			if ("number" != typeof t && "Number" != n(t))
				throw TypeError("Incorrect invocation");
			return +t;
		};
	},
	function (t, e) {
		t.exports =
			Math.log1p ||
			function (t) {
				return (t = +t) > -1e-8 && t < 1e-8 ? t - (t * t) / 2 : Math.log(1 + t);
			};
	},
	function (t, e, r) {
		var n,
			o,
			i,
			s = r(2),
			a = r(1),
			c = r(28),
			l = r(38),
			u = r(113),
			f = r(85),
			h = s.location,
			d = s.setImmediate,
			p = s.clearImmediate,
			g = s.process,
			m = s.MessageChannel,
			v = s.Dispatch,
			y = 0,
			b = {},
			S = function (t) {
				if (b.hasOwnProperty(t)) {
					var e = b[t];
					delete b[t], e();
				}
			},
			_ = function (t) {
				return function () {
					S(t);
				};
			},
			E = function (t) {
				S(t.data);
			},
			T = function (t) {
				s.postMessage(t + "", h.protocol + "//" + h.host);
			};
		(d && p) ||
			((d = function (t) {
				for (var e = [], r = 1; arguments.length > r; ) e.push(arguments[r++]);
				return (
					(b[++y] = function () {
						("function" == typeof t ? t : Function(t)).apply(void 0, e);
					}),
					n(y),
					y
				);
			}),
			(p = function (t) {
				delete b[t];
			}),
			"process" == c(g)
				? (n = function (t) {
						g.nextTick(_(t));
				  })
				: v && v.now
				? (n = function (t) {
						v.now(_(t));
				  })
				: m
				? ((i = (o = new m()).port2),
				  (o.port1.onmessage = E),
				  (n = l(i.postMessage, i, 1)))
				: !s.addEventListener ||
				  "function" != typeof postMessage ||
				  s.importScripts ||
				  a(T)
				? (n =
						"onreadystatechange" in f("script")
							? function (t) {
									u.appendChild(f("script")).onreadystatechange = function () {
										u.removeChild(this), S(t);
									};
							  }
							: function (t) {
									setTimeout(_(t), 0);
							  })
				: ((n = T), s.addEventListener("message", E, !1))),
			(t.exports = { set: d, clear: p });
	},
	function (t, e, r) {
		var n = r(4),
			o = r(3),
			i = r(136);
		t.exports = function (t, e) {
			if ((n(t), o(e) && e.constructor === t)) return e;
			var r = i.f(t);
			return (0, r.resolve)(e), r.promise;
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(21),
			o = function (t) {
				var e, r;
				(this.promise = new t(function (t, n) {
					if (void 0 !== e || void 0 !== r)
						throw TypeError("Bad Promise constructor");
					(e = t), (r = n);
				})),
					(this.resolve = n(e)),
					(this.reject = n(r));
			};
		t.exports.f = function (t) {
			return new o(t);
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(9).f,
			o = r(37),
			i = r(50),
			s = r(38),
			a = r(40),
			c = r(59),
			l = r(94),
			u = r(48),
			f = r(6),
			h = r(44).fastKey,
			d = r(20),
			p = d.set,
			g = d.getterFor;
		t.exports = {
			getConstructor: function (t, e, r, l) {
				var u = t(function (t, n) {
						a(t, u, e),
							p(t, {
								type: e,
								index: o(null),
								first: void 0,
								last: void 0,
								size: 0,
							}),
							f || (t.size = 0),
							null != n && c(n, t[l], t, r);
					}),
					d = g(e),
					m = function (t, e, r) {
						var n,
							o,
							i = d(t),
							s = v(t, e);
						return (
							s
								? (s.value = r)
								: ((i.last = s = {
										index: (o = h(e, !0)),
										key: e,
										value: r,
										previous: (n = i.last),
										next: void 0,
										removed: !1,
								  }),
								  i.first || (i.first = s),
								  n && (n.next = s),
								  f ? i.size++ : t.size++,
								  "F" !== o && (i.index[o] = s)),
							t
						);
					},
					v = function (t, e) {
						var r,
							n = d(t),
							o = h(e);
						if ("F" !== o) return n.index[o];
						for (r = n.first; r; r = r.next) if (r.key == e) return r;
					};
				return (
					i(u.prototype, {
						clear: function () {
							for (var t = d(this), e = t.index, r = t.first; r; )
								(r.removed = !0),
									r.previous && (r.previous = r.previous.next = void 0),
									delete e[r.index],
									(r = r.next);
							(t.first = t.last = void 0), f ? (t.size = 0) : (this.size = 0);
						},
						delete: function (t) {
							var e = d(this),
								r = v(this, t);
							if (r) {
								var n = r.next,
									o = r.previous;
								delete e.index[r.index],
									(r.removed = !0),
									o && (o.next = n),
									n && (n.previous = o),
									e.first == r && (e.first = n),
									e.last == r && (e.last = o),
									f ? e.size-- : this.size--;
							}
							return !!r;
						},
						forEach: function (t) {
							for (
								var e,
									r = d(this),
									n = s(t, arguments.length > 1 ? arguments[1] : void 0, 3);
								(e = e ? e.next : r.first);

							)
								for (n(e.value, e.key, this); e && e.removed; ) e = e.previous;
						},
						has: function (t) {
							return !!v(this, t);
						},
					}),
					i(
						u.prototype,
						r
							? {
									get: function (t) {
										var e = v(this, t);
										return e && e.value;
									},
									set: function (t, e) {
										return m(this, 0 === t ? 0 : t, e);
									},
							  }
							: {
									add: function (t) {
										return m(this, (t = 0 === t ? 0 : t), t);
									},
							  }
					),
					f &&
						n(u.prototype, "size", {
							get: function () {
								return d(this).size;
							},
						}),
					u
				);
			},
			setStrong: function (t, e, r) {
				var n = e + " Iterator",
					o = g(e),
					i = g(n);
				l(
					t,
					e,
					function (t, e) {
						p(this, { type: n, target: t, state: o(t), kind: e, last: void 0 });
					},
					function () {
						for (var t = i(this), e = t.kind, r = t.last; r && r.removed; )
							r = r.previous;
						return t.target && (t.last = r = r ? r.next : t.state.first)
							? "keys" == e
								? { value: r.key, done: !1 }
								: "values" == e
								? { value: r.value, done: !1 }
								: { value: [r.key, r.value], done: !1 }
							: ((t.target = void 0), { value: void 0, done: !0 });
					},
					r ? "entries" : "values",
					!r,
					!0
				),
					u(e);
			},
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(50),
			o = r(44).getWeakData,
			i = r(4),
			s = r(3),
			a = r(40),
			c = r(59),
			l = r(13),
			u = r(12),
			f = r(20),
			h = f.set,
			d = f.getterFor,
			p = l(5),
			g = l(6),
			m = 0,
			v = function (t) {
				return t.frozen || (t.frozen = new y());
			},
			y = function () {
				this.entries = [];
			},
			b = function (t, e) {
				return p(t.entries, function (t) {
					return t[0] === e;
				});
			};
		(y.prototype = {
			get: function (t) {
				var e = b(this, t);
				if (e) return e[1];
			},
			has: function (t) {
				return !!b(this, t);
			},
			set: function (t, e) {
				var r = b(this, t);
				r ? (r[1] = e) : this.entries.push([t, e]);
			},
			delete: function (t) {
				var e = g(this.entries, function (e) {
					return e[0] === t;
				});
				return ~e && this.entries.splice(e, 1), !!~e;
			},
		}),
			(t.exports = {
				getConstructor: function (t, e, r, l) {
					var f = t(function (t, n) {
							a(t, f, e),
								h(t, { type: e, id: m++, frozen: void 0 }),
								null != n && c(n, t[l], t, r);
						}),
						p = d(e),
						g = function (t, e, r) {
							var n = p(t),
								s = o(i(e), !0);
							return !0 === s ? v(n).set(e, r) : (s[n.id] = r), t;
						};
					return (
						n(f.prototype, {
							delete: function (t) {
								var e = p(this);
								if (!s(t)) return !1;
								var r = o(t);
								return !0 === r
									? v(e).delete(t)
									: r && u(r, e.id) && delete r[e.id];
							},
							has: function (t) {
								var e = p(this);
								if (!s(t)) return !1;
								var r = o(t);
								return !0 === r ? v(e).has(t) : r && u(r, e.id);
							},
						}),
						n(
							f.prototype,
							r
								? {
										get: function (t) {
											var e = p(this);
											if (s(t)) {
												var r = o(t);
												return !0 === r ? v(e).get(t) : r ? r[e.id] : void 0;
											}
										},
										set: function (t, e) {
											return g(this, t, e);
										},
								  }
								: {
										add: function (t) {
											return g(this, t, !0);
										},
								  }
						),
						f
					);
				},
			});
	},
	function (t, e, r) {
		var n = r(25),
			o = r(8);
		t.exports = function (t) {
			if (void 0 === t) return 0;
			var e = n(t),
				r = o(e);
			if (e !== r) throw RangeError("Wrong length or index");
			return r;
		};
	},
	function (t, e, r) {
		var n = r(25);
		t.exports = function (t, e) {
			var r = n(t);
			if (r < 0 || r % e) throw RangeError("Wrong offset");
			return r;
		};
	},
	function (t, e, r) {
		var n = r(11),
			o = r(8),
			i = r(61),
			s = r(90),
			a = r(38),
			c = r(5).aTypedArrayConstructor;
		t.exports = function (t) {
			var e,
				r,
				l,
				u,
				f,
				h = n(t),
				d = arguments.length,
				p = d > 1 ? arguments[1] : void 0,
				g = void 0 !== p,
				m = i(h);
			if (null != m && !s(m))
				for (f = m.call(h), h = []; !(u = f.next()).done; ) h.push(u.value);
			for (
				g && d > 2 && (p = a(p, arguments[2], 2)),
					r = o(h.length),
					l = new (c(this))(r),
					e = 0;
				r > e;
				e++
			)
				l[e] = g ? p(h[e], e) : h[e];
			return l;
		};
	},
	function (t, e, r) {
		var n = r(1),
			o = r(7),
			i = r(35),
			s = o("iterator");
		t.exports = !n(function () {
			var t = new URL("b?e=1", "http://a"),
				e = t.searchParams;
			return (
				(t.pathname = "c%20d"),
				(i && !t.toJSON) ||
					!e.sort ||
					"http://a/c%20d?e=1" !== t.href ||
					"1" !== e.get("e") ||
					"a=1" !== String(new URLSearchParams("?a=1")) ||
					!e[s] ||
					"a" !== new URL("https://a@b").username ||
					"b" !== new URLSearchParams(new URLSearchParams("a=b")).get("a") ||
					"xn--e1aybc" !== new URL("http://тест").host ||
					"#%D0%B1" !== new URL("http://a#б").hash
			);
		});
	},
	function (module, __webpack_exports__, __webpack_require__) {
		"use strict";
		__webpack_require__.d(__webpack_exports__, "a", function () {
			return TabsPolyfill;
		});
		var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
				22
			),
			core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(
				core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__
			),
			core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
				34
			),
			core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1___default = __webpack_require__.n(
				core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__
			),
			_communicator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(10),
			_listener_on_connect_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
				84
			),
			_port_polyfill__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(26);
		class TabsPolyfill {
			constructor() {
				(this.portsMap = new Map()),
					(this.onConnect = new _listener_on_connect_helper__WEBPACK_IMPORTED_MODULE_3__.a()),
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.addListenerOnPortMessageListener(
							this.onPortMessageListener.bind(this)
						),
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.addListenerOnConnectTab(this.onConnectTabListener.bind(this)),
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.addListenerTabAction(this.onTabActionListener.bind(this));
			}
			get(t, e) {
				console.warn("Not implemented `get` in TabsPolyfill");
			}
			getCurrent(t) {
				console.warn("Not implemented `getCurrent` in TabsPolyfill");
			}
			getSelected(t) {
				console.warn("Not implemented `getCurrent` in TabsPolyfill");
			}
			connect(t, e) {
				var r = new _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a(
					_port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a.TYPES.TAB,
					t
				);
				this.portsMap.set(r.uuid, r);
				var n = {
					action: _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a.ACTION.CREATE,
					portUUID: r.uuid,
					type: _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a.TYPES.TAB,
					uniqueID: r.uniqueID,
				};
				return (
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.port(n)
						.then((t) => {
							(r.ready = !0), r.executeWaitingMessages();
						})
						.catch((t) => {
							console.error(`Error port ${t}`);
						}),
					r
				);
			}
			sendRequest(t, e, r) {
				console.warn("Not implemented `sendRequest` in TabsPolyfill");
			}
			sendMessage(t, e, r, n) {
				console.warn("Not implemented `sendMessage` in TabsPolyfill");
			}
			getAllInWindow(t, e) {
				console.warn("Not implemented `getAllInWindow` in TabsPolyfill");
			}
			create(t, e) {
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs(t, TabsPolyfill.tabsActions.create)
					.then((t) => {
						e(t);
					})
					.catch((t) => {
						console.error(`Error in getiing tab ${t}`);
					});
			}
			duplicate(t, e) {
				console.warn("Not implemented `duplicate` in TabsPolyfill");
			}
			query(t, e) {
				return void 0 === window.safari
					? this.queryFromBrowserAction(t, e)
					: this.queryFromContent(t, e);
			}
			queryFromBrowserAction(t, e) {
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs(t, TabsPolyfill.tabsActions.getTabWith)
					.then((t) => {
						e(t);
					})
					.catch((t) => {
						console.error(`Error in getiing tab ${t}`);
					});
			}
			queryFromContent(t, e) {
				console.warn(`Enters in quering extension with: ${t}`);
			}
			highlight(t, e) {
				console.warn("Not implemented `highlightz` in TabsPolyfill");
			}
			update(t, e, r) {
				console.warn("Not implemented UPDATE funciton in TabsPolyfill");
			}
			move(t, e, r) {
				console.warn("Not implemented MOVE function in TabsPolyfill");
			}
			reload(t, e, r) {
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs({ tabID: t }, TabsPolyfill.tabsActions.reload)
					.then((t) => {
						null != r && r(t);
					})
					.catch((t) => {
						console.error(`Error reload tab ${t}`);
					});
			}
			detectLanguage(t, e) {
				console.warn("Not implemented `detectLanguage` in TabsPolyfill");
			}
			captureVisibleTab(t, e, r) {
				console.warn("Not implemented `captureVisibleTab` in TabsPolyfill");
			}
			executeScript(t, e, r) {
				void 0 === window.safari
					? this.executeScriptFromBrowserAction(t, e, r)
					: console.warn("Not implemented `executeScript` in TabsPolyfill");
			}
			executeScriptFromBrowserAction(t, e, r) {
				const n = { tabID: t, details: e };
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs(n, TabsPolyfill.tabsActions.executeScript)
					.then((t) => {
						r([t]);
					})
					.catch((t) => {
						console.error(`Error in executeScriptFromBrowserAction ${t}`);
					});
			}
			insertCSS(t, e, r) {
				console.warn("Not implemented `insertCSS` in TabsPolyfill");
			}
			setZoom(t, e, r) {
				console.warn("Not implemented `setZoom` in TabsPolyfill");
			}
			setZoomSettings(t, e, r) {
				console.warn("Not implemented `setZoomSettings` in TabsPolyfill");
			}
			discard(t, e) {
				console.warn("Not implemented `discard` in TabsPolyfill");
			}
			goForward(t, e) {
				console.warn("Not implemented `tabID` in TabsPolyfill");
			}
			goBack(t, e) {
				console.warn("Not implemented `goBack` in TabsPolyfill");
			}
			onPortMessageListener(t) {
				let e = t.portUUID,
					r = t.data;
				if (this.portsMap.has(e)) {
					let t = this.portsMap.get(e);
					void 0 !== t && t.onMessage.executeListeners(r);
				}
			}
			onConnectTabListener(t) {
				var e = t.portUUID,
					r = t.type,
					n = t.uniqueID,
					o = new _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a(r, n);
				(o.uuid = e),
					(o.ready = !0),
					this.portsMap.set(o.uuid, o),
					this.onConnect.executeListeners(o);
			}
			onTabActionListener(data) {
				console.log("Enters in onTabActionListener", data);
				let action = data.action;
				switch (action) {
					case "executeScript": {
						let callbackID = data.callbackID,
							code = data.data,
							returnValue = eval(code),
							queryInfo = { callbackID: callbackID, value: returnValue };
						_communicator__WEBPACK_IMPORTED_MODULE_2__.a
							.shared()
							.tabs(queryInfo, TabsPolyfill.tabsActions.executeScript)
							.then((t) => {
								console.log("result: ", t);
							})
							.catch((t) => {
								console.error(`Error in getiing tab ${t}`);
							});
					}
				}
			}
		}
		(TabsPolyfill.tabsActions = {
			getTabWith: 1,
			create: 2,
			reload: 3,
			executeScript: 4,
		}),
			(TabsPolyfill.onUpdated = {
				addListener: function (t) {
					window.safari.application.addEventListener(
						"beforeNavigate",
						function (e) {
							t(
								e.target.id,
								{ status: "loading", url: e.target.url, pinned: null },
								{
									id: e.target.id,
									index: null,
									windowId: null,
									openerTabId: null,
									highlighted: null,
									active: e.target.id == e.target.browserWindow.activeTab.id,
									pinned: null,
									url: e.target.url,
									title: e.target.title,
									favIconUrl: null,
									status: "loading",
									incognito: null,
								}
							);
						},
						!0
					);
				},
			});
	},
	function (t, e, r) {
		t.exports = (function () {
			"use strict";
			var t =
					Object.freeze ||
					function (t) {
						return t;
					},
				e = t([
					"a",
					"abbr",
					"acronym",
					"address",
					"area",
					"article",
					"aside",
					"audio",
					"b",
					"bdi",
					"bdo",
					"big",
					"blink",
					"blockquote",
					"body",
					"br",
					"button",
					"canvas",
					"caption",
					"center",
					"cite",
					"code",
					"col",
					"colgroup",
					"content",
					"data",
					"datalist",
					"dd",
					"decorator",
					"del",
					"details",
					"dfn",
					"dir",
					"div",
					"dl",
					"dt",
					"element",
					"em",
					"fieldset",
					"figcaption",
					"figure",
					"font",
					"footer",
					"form",
					"h1",
					"h2",
					"h3",
					"h4",
					"h5",
					"h6",
					"head",
					"header",
					"hgroup",
					"hr",
					"html",
					"i",
					"img",
					"input",
					"ins",
					"kbd",
					"label",
					"legend",
					"li",
					"main",
					"map",
					"mark",
					"marquee",
					"menu",
					"menuitem",
					"meter",
					"nav",
					"nobr",
					"ol",
					"optgroup",
					"option",
					"output",
					"p",
					"pre",
					"progress",
					"q",
					"rp",
					"rt",
					"ruby",
					"s",
					"samp",
					"section",
					"select",
					"shadow",
					"small",
					"source",
					"spacer",
					"span",
					"strike",
					"strong",
					"style",
					"sub",
					"summary",
					"sup",
					"table",
					"tbody",
					"td",
					"template",
					"textarea",
					"tfoot",
					"th",
					"thead",
					"time",
					"tr",
					"track",
					"tt",
					"u",
					"ul",
					"var",
					"video",
					"wbr",
				]),
				r = t([
					"svg",
					"a",
					"altglyph",
					"altglyphdef",
					"altglyphitem",
					"animatecolor",
					"animatemotion",
					"animatetransform",
					"audio",
					"canvas",
					"circle",
					"clippath",
					"defs",
					"desc",
					"ellipse",
					"filter",
					"font",
					"g",
					"glyph",
					"glyphref",
					"hkern",
					"image",
					"line",
					"lineargradient",
					"marker",
					"mask",
					"metadata",
					"mpath",
					"path",
					"pattern",
					"polygon",
					"polyline",
					"radialgradient",
					"rect",
					"stop",
					"style",
					"switch",
					"symbol",
					"text",
					"textpath",
					"title",
					"tref",
					"tspan",
					"video",
					"view",
					"vkern",
				]),
				n = t([
					"feBlend",
					"feColorMatrix",
					"feComponentTransfer",
					"feComposite",
					"feConvolveMatrix",
					"feDiffuseLighting",
					"feDisplacementMap",
					"feDistantLight",
					"feFlood",
					"feFuncA",
					"feFuncB",
					"feFuncG",
					"feFuncR",
					"feGaussianBlur",
					"feMerge",
					"feMergeNode",
					"feMorphology",
					"feOffset",
					"fePointLight",
					"feSpecularLighting",
					"feSpotLight",
					"feTile",
					"feTurbulence",
				]),
				o = t([
					"math",
					"menclose",
					"merror",
					"mfenced",
					"mfrac",
					"mglyph",
					"mi",
					"mlabeledtr",
					"mmultiscripts",
					"mn",
					"mo",
					"mover",
					"mpadded",
					"mphantom",
					"mroot",
					"mrow",
					"ms",
					"mspace",
					"msqrt",
					"mstyle",
					"msub",
					"msup",
					"msubsup",
					"mtable",
					"mtd",
					"mtext",
					"mtr",
					"munder",
					"munderover",
				]),
				i = t(["#text"]),
				s =
					Object.freeze ||
					function (t) {
						return t;
					},
				a = s([
					"accept",
					"action",
					"align",
					"alt",
					"autocomplete",
					"background",
					"bgcolor",
					"border",
					"cellpadding",
					"cellspacing",
					"checked",
					"cite",
					"class",
					"clear",
					"color",
					"cols",
					"colspan",
					"controls",
					"coords",
					"crossorigin",
					"datetime",
					"default",
					"dir",
					"disabled",
					"download",
					"enctype",
					"face",
					"for",
					"headers",
					"height",
					"hidden",
					"high",
					"href",
					"hreflang",
					"id",
					"integrity",
					"ismap",
					"label",
					"lang",
					"list",
					"loop",
					"low",
					"max",
					"maxlength",
					"media",
					"method",
					"min",
					"minlength",
					"multiple",
					"name",
					"noshade",
					"novalidate",
					"nowrap",
					"open",
					"optimum",
					"pattern",
					"placeholder",
					"poster",
					"preload",
					"pubdate",
					"radiogroup",
					"readonly",
					"rel",
					"required",
					"rev",
					"reversed",
					"role",
					"rows",
					"rowspan",
					"spellcheck",
					"scope",
					"selected",
					"shape",
					"size",
					"sizes",
					"span",
					"srclang",
					"start",
					"src",
					"srcset",
					"step",
					"style",
					"summary",
					"tabindex",
					"title",
					"type",
					"usemap",
					"valign",
					"value",
					"width",
					"xmlns",
				]),
				c = s([
					"accent-height",
					"accumulate",
					"additive",
					"alignment-baseline",
					"ascent",
					"attributename",
					"attributetype",
					"azimuth",
					"basefrequency",
					"baseline-shift",
					"begin",
					"bias",
					"by",
					"class",
					"clip",
					"clip-path",
					"clip-rule",
					"color",
					"color-interpolation",
					"color-interpolation-filters",
					"color-profile",
					"color-rendering",
					"cx",
					"cy",
					"d",
					"dx",
					"dy",
					"diffuseconstant",
					"direction",
					"display",
					"divisor",
					"dur",
					"edgemode",
					"elevation",
					"end",
					"fill",
					"fill-opacity",
					"fill-rule",
					"filter",
					"filterunits",
					"flood-color",
					"flood-opacity",
					"font-family",
					"font-size",
					"font-size-adjust",
					"font-stretch",
					"font-style",
					"font-variant",
					"font-weight",
					"fx",
					"fy",
					"g1",
					"g2",
					"glyph-name",
					"glyphref",
					"gradientunits",
					"gradienttransform",
					"height",
					"href",
					"id",
					"image-rendering",
					"in",
					"in2",
					"k",
					"k1",
					"k2",
					"k3",
					"k4",
					"kerning",
					"keypoints",
					"keysplines",
					"keytimes",
					"lang",
					"lengthadjust",
					"letter-spacing",
					"kernelmatrix",
					"kernelunitlength",
					"lighting-color",
					"local",
					"marker-end",
					"marker-mid",
					"marker-start",
					"markerheight",
					"markerunits",
					"markerwidth",
					"maskcontentunits",
					"maskunits",
					"max",
					"mask",
					"media",
					"method",
					"mode",
					"min",
					"name",
					"numoctaves",
					"offset",
					"operator",
					"opacity",
					"order",
					"orient",
					"orientation",
					"origin",
					"overflow",
					"paint-order",
					"path",
					"pathlength",
					"patterncontentunits",
					"patterntransform",
					"patternunits",
					"points",
					"preservealpha",
					"preserveaspectratio",
					"primitiveunits",
					"r",
					"rx",
					"ry",
					"radius",
					"refx",
					"refy",
					"repeatcount",
					"repeatdur",
					"restart",
					"result",
					"rotate",
					"scale",
					"seed",
					"shape-rendering",
					"specularconstant",
					"specularexponent",
					"spreadmethod",
					"stddeviation",
					"stitchtiles",
					"stop-color",
					"stop-opacity",
					"stroke-dasharray",
					"stroke-dashoffset",
					"stroke-linecap",
					"stroke-linejoin",
					"stroke-miterlimit",
					"stroke-opacity",
					"stroke",
					"stroke-width",
					"style",
					"surfacescale",
					"tabindex",
					"targetx",
					"targety",
					"transform",
					"text-anchor",
					"text-decoration",
					"text-rendering",
					"textlength",
					"type",
					"u1",
					"u2",
					"unicode",
					"values",
					"viewbox",
					"visibility",
					"version",
					"vert-adv-y",
					"vert-origin-x",
					"vert-origin-y",
					"width",
					"word-spacing",
					"wrap",
					"writing-mode",
					"xchannelselector",
					"ychannelselector",
					"x",
					"x1",
					"x2",
					"xmlns",
					"y",
					"y1",
					"y2",
					"z",
					"zoomandpan",
				]),
				l = s([
					"accent",
					"accentunder",
					"align",
					"bevelled",
					"close",
					"columnsalign",
					"columnlines",
					"columnspan",
					"denomalign",
					"depth",
					"dir",
					"display",
					"displaystyle",
					"encoding",
					"fence",
					"frame",
					"height",
					"href",
					"id",
					"largeop",
					"length",
					"linethickness",
					"lspace",
					"lquote",
					"mathbackground",
					"mathcolor",
					"mathsize",
					"mathvariant",
					"maxsize",
					"minsize",
					"movablelimits",
					"notation",
					"numalign",
					"open",
					"rowalign",
					"rowlines",
					"rowspacing",
					"rowspan",
					"rspace",
					"rquote",
					"scriptlevel",
					"scriptminsize",
					"scriptsizemultiplier",
					"selection",
					"separator",
					"separators",
					"stretchy",
					"subscriptshift",
					"supscriptshift",
					"symmetric",
					"voffset",
					"width",
					"xmlns",
				]),
				u = s([
					"xlink:href",
					"xml:id",
					"xlink:title",
					"xml:space",
					"xmlns:xlink",
				]),
				f = Object.hasOwnProperty,
				h = Object.setPrototypeOf,
				d = ("undefined" != typeof Reflect && Reflect).apply;
			function p(t, e) {
				h && h(t, null);
				for (var r = e.length; r--; ) {
					var n = e[r];
					if ("string" == typeof n) {
						var o = n.toLowerCase();
						o !== n && (Object.isFrozen(e) || (e[r] = o), (n = o));
					}
					t[n] = !0;
				}
				return t;
			}
			function g(t) {
				var e = {},
					r = void 0;
				for (r in t) d(f, t, [r]) && (e[r] = t[r]);
				return e;
			}
			d ||
				(d = function (t, e, r) {
					return t.apply(e, r);
				});
			var m =
					Object.seal ||
					function (t) {
						return t;
					},
				v = m(/\{\{[\s\S]*|[\s\S]*\}\}/gm),
				y = m(/<%[\s\S]*|[\s\S]*%>/gm),
				b = m(/^data-[\-\w.\u00B7-\uFFFF]/),
				S = m(/^aria-[\-\w]+$/),
				_ = m(
					/^(?:(?:(?:f|ht)tps?|mailto|tel|callto|cid|xmpp):|[^a-z]|[a-z+.\-]+(?:[^a-z+.\-:]|$))/i
				),
				E = m(/^(?:\w+script|data):/i),
				T = m(/[\u0000-\u0020\u00A0\u1680\u180E\u2000-\u2029\u205f\u3000]/g),
				A =
					"function" == typeof Symbol && "symbol" == typeof Symbol.iterator
						? function (t) {
								return typeof t;
						  }
						: function (t) {
								return t &&
									"function" == typeof Symbol &&
									t.constructor === Symbol &&
									t !== Symbol.prototype
									? "symbol"
									: typeof t;
						  };
			function w(t) {
				if (Array.isArray(t)) {
					for (var e = 0, r = Array(t.length); e < t.length; e++) r[e] = t[e];
					return r;
				}
				return Array.from(t);
			}
			var x = ("undefined" != typeof Reflect && Reflect).apply,
				R = Array.prototype.slice,
				I = Object.freeze,
				C = function () {
					return "undefined" == typeof window ? null : window;
				};
			x ||
				(x = function (t, e, r) {
					return t.apply(e, r);
				});
			var O = function (t, e) {
				if (
					"object" !== (void 0 === t ? "undefined" : A(t)) ||
					"function" != typeof t.createPolicy
				)
					return null;
				var r = null;
				e.currentScript &&
					e.currentScript.hasAttribute("data-tt-policy-suffix") &&
					(r = e.currentScript.getAttribute("data-tt-policy-suffix"));
				var n = "dompurify" + (r ? "#" + r : "");
				try {
					return t.createPolicy(n, {
						createHTML: function (t) {
							return t;
						},
					});
				} catch (t) {
					return (
						console.warn("TrustedTypes policy " + n + " could not be created."),
						null
					);
				}
			};
			return (function t() {
				var s =
						arguments.length > 0 && void 0 !== arguments[0]
							? arguments[0]
							: C(),
					f = function (e) {
						return t(e);
					};
				if (
					((f.version = "2.0.7"),
					(f.removed = []),
					!s || !s.document || 9 !== s.document.nodeType)
				)
					return (f.isSupported = !1), f;
				var h = s.document,
					d = !1,
					m = !1,
					M = s.document,
					L = s.DocumentFragment,
					N = s.HTMLTemplateElement,
					k = s.Node,
					P = s.NodeFilter,
					D = s.NamedNodeMap,
					U = void 0 === D ? s.NamedNodeMap || s.MozNamedAttrMap : D,
					B = s.Text,
					F = s.Comment,
					G = s.DOMParser,
					j = s.TrustedTypes;
				if ("function" == typeof N) {
					var H = M.createElement("template");
					H.content && H.content.ownerDocument && (M = H.content.ownerDocument);
				}
				var W = O(j, h),
					z = W ? W.createHTML("") : "",
					Y = M,
					V = Y.implementation,
					q = Y.createNodeIterator,
					K = Y.getElementsByTagName,
					X = Y.createDocumentFragment,
					Z = h.importNode,
					J = {};
				f.isSupported =
					V && void 0 !== V.createHTMLDocument && 9 !== M.documentMode;
				var Q = v,
					$ = y,
					tt = b,
					et = S,
					rt = E,
					nt = T,
					ot = _,
					it = null,
					st = p({}, [].concat(w(e), w(r), w(n), w(o), w(i))),
					at = null,
					ct = p({}, [].concat(w(a), w(c), w(l), w(u))),
					lt = null,
					ut = null,
					ft = !0,
					ht = !0,
					dt = !1,
					pt = !1,
					gt = !1,
					mt = !1,
					vt = !1,
					yt = !1,
					bt = !1,
					St = !1,
					_t = !1,
					Et = !1,
					Tt = !0,
					At = !0,
					wt = !1,
					xt = {},
					Rt = p({}, [
						"annotation-xml",
						"audio",
						"colgroup",
						"desc",
						"foreignobject",
						"head",
						"iframe",
						"math",
						"mi",
						"mn",
						"mo",
						"ms",
						"mtext",
						"noembed",
						"noframes",
						"plaintext",
						"script",
						"style",
						"svg",
						"template",
						"thead",
						"title",
						"video",
						"xmp",
					]),
					It = p({}, ["audio", "video", "img", "source", "image"]),
					Ct = null,
					Ot = p({}, [
						"alt",
						"class",
						"for",
						"id",
						"label",
						"name",
						"pattern",
						"placeholder",
						"summary",
						"title",
						"value",
						"style",
						"xmlns",
					]),
					Mt = null,
					Lt = M.createElement("form"),
					Nt = function (t) {
						(Mt && Mt === t) ||
							((t && "object" === (void 0 === t ? "undefined" : A(t))) ||
								(t = {}),
							(it = "ALLOWED_TAGS" in t ? p({}, t.ALLOWED_TAGS) : st),
							(at = "ALLOWED_ATTR" in t ? p({}, t.ALLOWED_ATTR) : ct),
							(Ct =
								"ADD_URI_SAFE_ATTR" in t ? p(g(Ot), t.ADD_URI_SAFE_ATTR) : Ot),
							(lt = "FORBID_TAGS" in t ? p({}, t.FORBID_TAGS) : {}),
							(ut = "FORBID_ATTR" in t ? p({}, t.FORBID_ATTR) : {}),
							(xt = "USE_PROFILES" in t && t.USE_PROFILES),
							(ft = !1 !== t.ALLOW_ARIA_ATTR),
							(ht = !1 !== t.ALLOW_DATA_ATTR),
							(dt = t.ALLOW_UNKNOWN_PROTOCOLS || !1),
							(pt = t.SAFE_FOR_JQUERY || !1),
							(gt = t.SAFE_FOR_TEMPLATES || !1),
							(mt = t.WHOLE_DOCUMENT || !1),
							(bt = t.RETURN_DOM || !1),
							(St = t.RETURN_DOM_FRAGMENT || !1),
							(_t = t.RETURN_DOM_IMPORT || !1),
							(Et = t.RETURN_TRUSTED_TYPE || !1),
							(yt = t.FORCE_BODY || !1),
							(Tt = !1 !== t.SANITIZE_DOM),
							(At = !1 !== t.KEEP_CONTENT),
							(wt = t.IN_PLACE || !1),
							(ot = t.ALLOWED_URI_REGEXP || ot),
							gt && (ht = !1),
							St && (bt = !0),
							xt &&
								((it = p({}, [].concat(w(i)))),
								(at = []),
								!0 === xt.html && (p(it, e), p(at, a)),
								!0 === xt.svg && (p(it, r), p(at, c), p(at, u)),
								!0 === xt.svgFilters && (p(it, n), p(at, c), p(at, u)),
								!0 === xt.mathMl && (p(it, o), p(at, l), p(at, u))),
							t.ADD_TAGS && (it === st && (it = g(it)), p(it, t.ADD_TAGS)),
							t.ADD_ATTR && (at === ct && (at = g(at)), p(at, t.ADD_ATTR)),
							t.ADD_URI_SAFE_ATTR && p(Ct, t.ADD_URI_SAFE_ATTR),
							At && (it["#text"] = !0),
							mt && p(it, ["html", "head", "body"]),
							it.table && (p(it, ["tbody"]), delete lt.tbody),
							I && I(t),
							(Mt = t));
					},
					kt = function (t) {
						f.removed.push({ element: t });
						try {
							t.parentNode.removeChild(t);
						} catch (e) {
							t.outerHTML = z;
						}
					},
					Pt = function (t, e) {
						try {
							f.removed.push({ attribute: e.getAttributeNode(t), from: e });
						} catch (t) {
							f.removed.push({ attribute: null, from: e });
						}
						e.removeAttribute(t);
					},
					Dt = function (t) {
						var e = void 0,
							r = void 0;
						if (yt) t = "<remove></remove>" + t;
						else {
							var n = t.match(/^[\s]+/);
							(r = n && n[0]) && (t = t.slice(r.length));
						}
						if (d)
							try {
								e = new G().parseFromString(t, "text/html");
							} catch (t) {}
						if ((m && p(lt, ["title"]), !e || !e.documentElement)) {
							var o = (e = V.createHTMLDocument("")).body;
							o.parentNode.removeChild(o.parentNode.firstElementChild),
								(o.outerHTML = W ? W.createHTML(t) : t);
						}
						return (
							t &&
								r &&
								e.body.insertBefore(
									M.createTextNode(r),
									e.body.childNodes[0] || null
								),
							K.call(e, mt ? "html" : "body")[0]
						);
					};
				f.isSupported &&
					((function () {
						try {
							Dt(
								'<svg><p><textarea><img src="</textarea><img src=x abc=1//">'
							).querySelector("svg img") && (d = !0);
						} catch (t) {}
					})(),
					(function () {
						try {
							var t = Dt("<x/><title>&lt;/title&gt;&lt;img&gt;");
							/<\/title/.test(t.querySelector("title").innerHTML) && (m = !0);
						} catch (t) {}
					})());
				var Ut = function (t) {
						return q.call(
							t.ownerDocument || t,
							t,
							P.SHOW_ELEMENT | P.SHOW_COMMENT | P.SHOW_TEXT,
							function () {
								return P.FILTER_ACCEPT;
							},
							!1
						);
					},
					Bt = function (t) {
						return "object" === (void 0 === k ? "undefined" : A(k))
							? t instanceof k
							: t &&
									"object" === (void 0 === t ? "undefined" : A(t)) &&
									"number" == typeof t.nodeType &&
									"string" == typeof t.nodeName;
					},
					Ft = function (t, e, r) {
						J[t] &&
							J[t].forEach(function (t) {
								t.call(f, e, r, Mt);
							});
					},
					Gt = function (t) {
						var e,
							r = void 0;
						if (
							(Ft("beforeSanitizeElements", t, null),
							!(
								(e = t) instanceof B ||
								e instanceof F ||
								("string" == typeof e.nodeName &&
									"string" == typeof e.textContent &&
									"function" == typeof e.removeChild &&
									e.attributes instanceof U &&
									"function" == typeof e.removeAttribute &&
									"function" == typeof e.setAttribute &&
									"string" == typeof e.namespaceURI)
							))
						)
							return kt(t), !0;
						var n = t.nodeName.toLowerCase();
						if (
							(Ft("uponSanitizeElement", t, { tagName: n, allowedTags: it }),
							("svg" === n || "math" === n) &&
								0 !== t.querySelectorAll("p, br").length)
						)
							return kt(t), !0;
						if (!it[n] || lt[n]) {
							if (At && !Rt[n] && "function" == typeof t.insertAdjacentHTML)
								try {
									var o = t.innerHTML;
									t.insertAdjacentHTML("AfterEnd", W ? W.createHTML(o) : o);
								} catch (t) {}
							return kt(t), !0;
						}
						return "noscript" === n && /<\/noscript/i.test(t.innerHTML)
							? (kt(t), !0)
							: "noembed" === n && /<\/noembed/i.test(t.innerHTML)
							? (kt(t), !0)
							: (!pt ||
									t.firstElementChild ||
									(t.content && t.content.firstElementChild) ||
									!/</g.test(t.textContent) ||
									(f.removed.push({ element: t.cloneNode() }),
									t.innerHTML
										? (t.innerHTML = t.innerHTML.replace(/</g, "&lt;"))
										: (t.innerHTML = t.textContent.replace(/</g, "&lt;"))),
							  gt &&
									3 === t.nodeType &&
									((r = (r = (r = t.textContent).replace(Q, " ")).replace(
										$,
										" "
									)),
									t.textContent !== r &&
										(f.removed.push({ element: t.cloneNode() }),
										(t.textContent = r))),
							  Ft("afterSanitizeElements", t, null),
							  !1);
					},
					jt = function (t, e, r) {
						if (Tt && ("id" === e || "name" === e) && (r in M || r in Lt))
							return !1;
						if (ht && tt.test(e));
						else if (ft && et.test(e));
						else {
							if (!at[e] || ut[e]) return !1;
							if (Ct[e]);
							else if (ot.test(r.replace(nt, "")));
							else if (
								("src" !== e && "xlink:href" !== e && "href" !== e) ||
								"script" === t ||
								0 !== r.indexOf("data:") ||
								!It[t]
							)
								if (dt && !rt.test(r.replace(nt, "")));
								else if (r) return !1;
						}
						return !0;
					},
					Ht = function (t) {
						var e = void 0,
							r = void 0,
							n = void 0,
							o = void 0,
							i = void 0;
						Ft("beforeSanitizeAttributes", t, null);
						var s = t.attributes;
						if (s) {
							var a = {
								attrName: "",
								attrValue: "",
								keepAttr: !0,
								allowedAttributes: at,
							};
							for (i = s.length; i--; ) {
								var c = (e = s[i]),
									l = c.name,
									u = c.namespaceURI;
								if (
									((r = e.value.trim()),
									(n = l.toLowerCase()),
									(a.attrName = n),
									(a.attrValue = r),
									(a.keepAttr = !0),
									Ft("uponSanitizeAttribute", t, a),
									(r = a.attrValue),
									"name" === n && "IMG" === t.nodeName && s.id)
								)
									(o = s.id),
										(s = x(R, s, [])),
										Pt("id", t),
										Pt(l, t),
										s.indexOf(o) > i && t.setAttribute("id", o.value);
								else {
									if (
										"INPUT" === t.nodeName &&
										"type" === n &&
										"file" === r &&
										a.keepAttr &&
										(at[n] || !ut[n])
									)
										continue;
									"id" === l && t.setAttribute(l, ""), Pt(l, t);
								}
								if (a.keepAttr)
									if (
										/svg|math/i.test(t.namespaceURI) &&
										new RegExp(
											"</(" + Object.keys(Rt).join("|") + ")",
											"i"
										).test(r)
									)
										Pt(l, t);
									else {
										gt && (r = (r = r.replace(Q, " ")).replace($, " "));
										var h = t.nodeName.toLowerCase();
										if (jt(h, n, r))
											try {
												u ? t.setAttributeNS(u, l, r) : t.setAttribute(l, r),
													f.removed.pop();
											} catch (t) {}
									}
							}
							Ft("afterSanitizeAttributes", t, null);
						}
					},
					Wt = function t(e) {
						var r = void 0,
							n = Ut(e);
						for (Ft("beforeSanitizeShadowDOM", e, null); (r = n.nextNode()); )
							Ft("uponSanitizeShadowNode", r, null),
								Gt(r) || (r.content instanceof L && t(r.content), Ht(r));
						Ft("afterSanitizeShadowDOM", e, null);
					};
				return (
					(f.sanitize = function (t, e) {
						var r = void 0,
							n = void 0,
							o = void 0,
							i = void 0,
							a = void 0;
						if ((t || (t = "\x3c!--\x3e"), "string" != typeof t && !Bt(t))) {
							if ("function" != typeof t.toString)
								throw new TypeError("toString is not a function");
							if ("string" != typeof (t = t.toString()))
								throw new TypeError("dirty is not a string, aborting");
						}
						if (!f.isSupported) {
							if (
								"object" === A(s.toStaticHTML) ||
								"function" == typeof s.toStaticHTML
							) {
								if ("string" == typeof t) return s.toStaticHTML(t);
								if (Bt(t)) return s.toStaticHTML(t.outerHTML);
							}
							return t;
						}
						if ((vt || Nt(e), (f.removed = []), wt));
						else if (t instanceof k)
							1 ===
								(n = (r = Dt("\x3c!--\x3e")).ownerDocument.importNode(t, !0))
									.nodeType && "BODY" === n.nodeName
								? (r = n)
								: "HTML" === n.nodeName
								? (r = n)
								: r.appendChild(n);
						else {
							if (!bt && !gt && !mt && Et && -1 === t.indexOf("<"))
								return W ? W.createHTML(t) : t;
							if (!(r = Dt(t))) return bt ? null : z;
						}
						r && yt && kt(r.firstChild);
						for (var c = Ut(wt ? t : r); (o = c.nextNode()); )
							(3 === o.nodeType && o === i) ||
								Gt(o) ||
								(o.content instanceof L && Wt(o.content), Ht(o), (i = o));
						if (((i = null), wt)) return t;
						if (bt) {
							if (St)
								for (a = X.call(r.ownerDocument); r.firstChild; )
									a.appendChild(r.firstChild);
							else a = r;
							return _t && (a = Z.call(h, a, !0)), a;
						}
						var l = mt ? r.outerHTML : r.innerHTML;
						return (
							gt && (l = (l = l.replace(Q, " ")).replace($, " ")),
							W && Et ? W.createHTML(l) : l
						);
					}),
					(f.setConfig = function (t) {
						Nt(t), (vt = !0);
					}),
					(f.clearConfig = function () {
						(Mt = null), (vt = !1);
					}),
					(f.isValidAttribute = function (t, e, r) {
						Mt || Nt({});
						var n = t.toLowerCase(),
							o = e.toLowerCase();
						return jt(n, o, r);
					}),
					(f.addHook = function (t, e) {
						"function" == typeof e && ((J[t] = J[t] || []), J[t].push(e));
					}),
					(f.removeHook = function (t) {
						J[t] && J[t].pop();
					}),
					(f.removeHooks = function (t) {
						J[t] && (J[t] = []);
					}),
					(f.removeAllHooks = function () {
						J = {};
					}),
					f
				);
			})();
		})();
	},
	function (t, e) {
		t.exports =
			"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6REIzNTZGMzkxNTRBMTFFODlDRjhBODBGRTBBMzEyMjMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6REIzNTZGM0ExNTRBMTFFODlDRjhBODBGRTBBMzEyMjMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpEQjM1NkYzNzE1NEExMUU4OUNGOEE4MEZFMEEzMTIyMyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpEQjM1NkYzODE1NEExMUU4OUNGOEE4MEZFMEEzMTIyMyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pi4mKaIAAAg5SURBVHjalFcLcBXVGf7O2b13b+7NgySEhJAHeQrhZXiWkUKhqKUOlRatQy1OnVY7xU61nYFqwZnqSG0Zps7g2Kl1LKh0hGaKdrRQq4YaA2JSiOEVIeQFeZUkkOTmcR+7e/rtcoEkBKVn5p89e85/9jvnP9//WIG3zgACwDAlYgOJOh7JSkCilOgJW/AZwB8r/jEBll0MqDug7LnUnAKlkuCsFKIXkK2AfRxSO0ipXzhzxUBVzQF+7OZNv9mErYBdn7ybCtu8lyAPcWhZbOq/MRmkKM5NAKwi9tfDMtk1D1ad+vfrmJi3H91NF/8vYEXUFyr+dhc/upmvSyndlJ2U92GLMzBVJzzaAPweGyErAKEmwbbzud3l1PkOIsM70dNcDsP/PPy3l+Py4RtOL0aZOsz5JN1AQ/ljsK1nOOKn/B7QXr3HSKz/zbMtdqSnxQMYasG24ujYjzXKYpGvGnOhzIf5+kvKEDxxz8BX+jKCh0PjAw/StmbIj57qLTCjT3HkM97d491p8yvMtiBCE73o21yJaE+bkPBLKiuuUPO25avxjSkX0gIvsrOQJ98KNfN5RKoGr81esa1jdLKpu/pnMdCPID1rulMXVWjxZJcUNIAFfUEKPOlTlJ6Tac3dlm9LBHBsU5O8CUuqoLS17JQjPLQZ4vQGROcY10+873Pq0MQth1bDipZx7CTCYm3dXrNFS/ORtALG13MR8imIVB9UhLtMMlDccgy31Cw9C5q5j73b4E99EOHcA7COWboL2lo9haBPczIKUz5a9XT95SAsjTuyAl+bwQPb3ICEGiZrhyyh+NKQMUcZ0AUSDZBeKtQeFJHD7fasgp7RptfMVljyx9DsQwj3bYS39wz5VK+XpsTLmpbwt6mygPLUiV911Uag+wSkHfhqCYy7smBHTRTpzWkIqpXczCpcxkzqJlO86HQ/b7pEKsA/CfJrgvSNxD77gfFZ8d3DW+lqzwGXadnSl/Sakx8lc+CHnG+iq7wWdemtbAGvkpPjMFVvCkC3v0vAxzgx70sMO42gTXzuGDlIUJUakrt6fPaPEA7eB1//XyWsyO2cc+TNU3+IdFiIuHQLrClB0fzeQh7mNYL++RZAr7bHwS2PHXywXevgo4wBaTHiQqUSZmQFBxwfOxg+3+uCCsSJ4sX9pQTcy/e143zc8eFTvPg6h75j5vJJ2XuveUys7ciP2q63OPrR/gVkjHJi73my4Bydhmtsh8c+jr1EmXtjWFPNyyNmzRO2nVlScSS9sLWzmqMXxrjSapha6tilSaZ+jo9mumyJs6scCmOqdVHPSaRLS4VH8oYYBhePc9KhooYLvevyshc+9M0Vyd7QcErXhx8vmt112TFjZITeDOiqeOzi5V32RRfLik51gFMofb9Izwol7FyJuKUFti/eG+ZY5zin/Vw0NRe9UfYOnqzsRP+81chIngDz5NlpnK0foekwPm/s8renyLB7TcpOu3oPasPkbGWxk7iqkAFRd4LogXGAbSfmtF7swZF25g1/AJIBz7keJ1SM0PTQYuk3rhfXApcDHKQkFB+tNsxdpxGu6XI8kkFA7oplpRHhVxYHCvKaLctG9pF9MA6+icEwvb4437m7olE5QAlP14zlYuTyyUErjg+DObtbxoiRZiuVhnb6cH8EorYb5/fICo6/PGbPiTWF2XpgdsmJqMCgMLwD2oxptbWZGU5REDcyUJLxl0KfnB+1uCMgHCtkQurn9StZCE5IyxcRzwVSi9y26AtOR9sGaU13c+z1CuG2uqLcfuRnn3YDuRTTHbcfs8FBLm/LTmwYHT6FVegcHJp+SkL3lPPFSeZLfF4Jr9Cgx8VxUxpaGib1M8P8hPPvjj05NLmAoPPdtTe2RgLXjRyo3xWRsaJCg8f/qYQ0jjrhlHJ/zp3BVG/UYYbG29cdMqG9eypdQFvP+RdjMflWWjlPN8q3f7fMcNz2GzxRLSIJJyWSF17ki1PWzGFeuD9qWogGw7By4iEyeG2WQoco6r1HJv2c7LqPepWxeuVmbZghsywW3dx27C1L+0uutsb1b49vD4YntAnsISE7K/MQGdrvmlDJJW1VqU12IiucxROht4agWHCYmgbNI3GH3RBotoZX0hqrqL+IkhorkbSYRd6gbBzpXg/0aLP2plrvkc2d8GY/jFBzrcBe+r0gB1oOroeymBDwLwj9gbbe3D470wcZErAGo5B+mp/3Llj+ZnbXEERLh7Dn0W1ILjvdTZGuJZzT2tVXQbecQOZzc7RXmdpXwj9xA4I5r0M7FuaJz7h3CStkoPPT31LhCervJql+eiEwrU9j+rCGyXLdguXzMuOb8PoNTA4f/9KL3tThydw2xd7Ob65j3bUdg9OfhX40eL3mEo6f+yLwxj3JvhM4vk9y7MkOnc2Xpn0l4rDkpa/DHrZhFsV/IWBl7wT5lbBnJkFfcUE1z58QmLH1KihGpS7GmtWzl4eRkLKBd/ECR1ay9KjIkHWPZvvOTSLJhGMYd5Pe8eu7/1yaKBYrb9aSlIENR4woa3BrBQ+zPSN3/iZcqu4dXd46pnZjjcK3sgJI9khxKWLq71Tv/x6vYKPLRMC5s7e5z49JvkZheHrqS2aFg4MDKG2sN+4e0tLeM8xCWmkp16x2iwYhauCJ34GU2WXoPDR4Y0E/BjiFzO2nS81N9mLLB/sLYIbX8WM/oEYBZcAtka7E8L6YxZJi4swnUOposd1IyCjLnTStpaX+w8h41rkp8LxkA5VdYaQaGnYf+vtkCG0RWb/M9XcglxKP2K8AT9fI53H3F8dIqJ2Vt6jjRN37X/jT9j8BBgAjs1TVUBbeswAAAABJRU5ErkJggg==";
	},
	function (t, e, r) {
		r(147), r(358), (t.exports = r(363));
	},
	function (t, e, r) {
		r(148),
			r(151),
			r(152),
			r(153),
			r(154),
			r(155),
			r(156),
			r(157),
			r(158),
			r(159),
			r(160),
			r(161),
			r(162),
			r(163),
			r(164),
			r(165),
			r(166),
			r(167),
			r(168),
			r(169),
			r(170),
			r(171),
			r(172),
			r(173),
			r(174),
			r(175),
			r(176),
			r(177),
			r(178),
			r(179),
			r(180),
			r(181),
			r(182),
			r(183),
			r(184),
			r(185),
			r(187),
			r(188),
			r(189),
			r(190),
			r(191),
			r(192),
			r(193),
			r(194),
			r(195),
			r(196),
			r(197),
			r(198),
			r(199),
			r(200),
			r(201),
			r(202),
			r(203),
			r(204),
			r(205),
			r(206),
			r(208),
			r(209),
			r(210),
			r(211),
			r(212),
			r(213),
			r(214),
			r(215),
			r(216),
			r(217),
			r(218),
			r(219),
			r(220),
			r(221),
			r(222),
			r(22),
			r(223),
			r(224),
			r(225),
			r(226),
			r(227),
			r(228),
			r(229),
			r(230),
			r(231),
			r(232),
			r(233),
			r(234),
			r(235),
			r(236),
			r(237),
			r(238),
			r(239),
			r(128),
			r(240),
			r(241),
			r(242),
			r(243),
			r(244),
			r(245),
			r(246),
			r(247),
			r(248),
			r(249),
			r(250),
			r(251),
			r(252),
			r(253),
			r(254),
			r(255),
			r(256),
			r(257),
			r(258),
			r(259),
			r(260),
			r(261),
			r(263),
			r(264),
			r(265),
			r(266),
			r(267),
			r(268),
			r(269),
			r(270),
			r(271),
			r(272),
			r(273),
			r(274),
			r(275),
			r(276),
			r(277),
			r(278),
			r(279),
			r(281),
			r(282),
			r(283),
			r(284),
			r(285),
			r(286),
			r(287),
			r(288),
			r(289),
			r(290),
			r(291),
			r(292),
			r(293),
			r(295),
			r(296),
			r(298),
			r(66),
			r(302),
			r(303),
			r(304),
			r(305),
			r(306),
			r(307),
			r(308),
			r(309),
			r(310),
			r(311),
			r(312),
			r(313),
			r(314),
			r(315),
			r(316),
			r(317),
			r(318),
			r(319),
			r(320),
			r(321),
			r(322),
			r(323),
			r(324),
			r(325),
			r(326),
			r(327),
			r(328),
			r(329),
			r(330),
			r(331),
			r(332),
			r(333),
			r(334),
			r(335),
			r(336),
			r(337),
			r(338),
			r(339),
			r(340),
			r(341),
			r(342),
			r(343),
			r(344),
			r(345),
			r(346),
			r(347),
			r(348),
			r(349),
			r(350),
			r(351),
			r(352),
			r(353),
			r(354),
			r(355),
			r(356),
			r(357),
			(t.exports = r(71));
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(2),
			i = r(12),
			s = r(111),
			a = r(6),
			c = r(35),
			l = r(16),
			u = r(55),
			f = r(1),
			h = r(53),
			d = r(29),
			p = r(54),
			g = r(7),
			m = r(112),
			v = r(17),
			y = r(150),
			b = r(43),
			S = r(4),
			_ = r(3),
			E = r(11),
			T = r(18),
			A = r(27),
			w = r(41),
			x = r(37),
			R = r(42),
			I = r(114),
			C = r(15),
			O = r(9),
			M = r(51),
			L = r(14),
			N = r(46),
			k = r(70),
			P = r(69),
			D = r(20),
			U = P("hidden"),
			B = D.set,
			F = D.getterFor("Symbol"),
			G = C.f,
			j = O.f,
			H = I.f,
			W = o.Symbol,
			z = o.JSON,
			Y = z && z.stringify,
			V = g("toPrimitive"),
			q = M.f,
			K = h("symbol-registry"),
			X = h("symbols"),
			Z = h("op-symbols"),
			J = h("wks"),
			Q = Object.prototype,
			$ = o.QObject,
			tt = !$ || !$.prototype || !$.prototype.findChild,
			et =
				a &&
				f(function () {
					return (
						7 !=
						x(
							j({}, "a", {
								get: function () {
									return j(this, "a", { value: 7 }).a;
								},
							})
						).a
					);
				})
					? function (t, e, r) {
							var n = G(Q, e);
							n && delete Q[e], j(t, e, r), n && t !== Q && j(Q, e, n);
					  }
					: j,
			rt = function (t, e) {
				var r = (X[t] = x(W.prototype));
				return (
					B(r, { type: "Symbol", tag: t, description: e }),
					a || (r.description = e),
					r
				);
			},
			nt =
				s && "symbol" == typeof W.iterator
					? function (t) {
							return "symbol" == typeof t;
					  }
					: function (t) {
							return Object(t) instanceof W;
					  },
			ot = function (t, e, r) {
				return (
					t === Q && ot(Z, e, r),
					S(t),
					(e = A(e, !0)),
					S(r),
					i(X, e)
						? (r.enumerable
								? (i(t, U) && t[U][e] && (t[U][e] = !1),
								  (r = x(r, { enumerable: w(0, !1) })))
								: (i(t, U) || j(t, U, w(1, {})), (t[U][e] = !0)),
						  et(t, e, r))
						: j(t, e, r)
				);
			},
			it = function (t, e) {
				S(t);
				for (var r, n = y((e = T(e))), o = 0, i = n.length; i > o; )
					ot(t, (r = n[o++]), e[r]);
				return t;
			},
			st = function (t) {
				var e = q.call(this, (t = A(t, !0)));
				return (
					!(this === Q && i(X, t) && !i(Z, t)) &&
					(!(e || !i(this, t) || !i(X, t) || (i(this, U) && this[U][t])) || e)
				);
			},
			at = function (t, e) {
				if (((t = T(t)), (e = A(e, !0)), t !== Q || !i(X, e) || i(Z, e))) {
					var r = G(t, e);
					return (
						!r || !i(X, e) || (i(t, U) && t[U][e]) || (r.enumerable = !0), r
					);
				}
			},
			ct = function (t) {
				for (var e, r = H(T(t)), n = [], o = 0; r.length > o; )
					i(X, (e = r[o++])) || i(u, e) || n.push(e);
				return n;
			},
			lt = function (t) {
				for (
					var e, r = t === Q, n = H(r ? Z : T(t)), o = [], s = 0;
					n.length > s;

				)
					!i(X, (e = n[s++])) || (r && !i(Q, e)) || o.push(X[e]);
				return o;
			};
		s ||
			(l(
				(W = function () {
					if (this instanceof W) throw TypeError("Symbol is not a constructor");
					var t = void 0 === arguments[0] ? void 0 : String(arguments[0]),
						e = p(t),
						r = function (t) {
							this === Q && r.call(Z, t),
								i(this, U) && i(this[U], e) && (this[U][e] = !1),
								et(this, e, w(1, t));
						};
					return a && tt && et(Q, e, { configurable: !0, set: r }), rt(e, t);
				}).prototype,
				"toString",
				function () {
					return F(this).tag;
				}
			),
			(M.f = st),
			(O.f = ot),
			(C.f = at),
			(R.f = I.f = ct),
			(k.f = lt),
			a &&
				(j(W.prototype, "description", {
					configurable: !0,
					get: function () {
						return F(this).description;
					},
				}),
				c || l(Q, "propertyIsEnumerable", st, { unsafe: !0 })),
			(m.f = function (t) {
				return rt(g(t), t);
			})),
			n({ global: !0, wrap: !0, forced: !s, sham: !s }, { Symbol: W });
		for (var ut = N(J), ft = 0; ut.length > ft; ) v(ut[ft++]);
		n(
			{ target: "Symbol", stat: !0, forced: !s },
			{
				for: function (t) {
					return i(K, (t += "")) ? K[t] : (K[t] = W(t));
				},
				keyFor: function (t) {
					if (!nt(t)) throw TypeError(t + " is not a symbol");
					for (var e in K) if (K[e] === t) return e;
				},
				useSetter: function () {
					tt = !0;
				},
				useSimple: function () {
					tt = !1;
				},
			}
		),
			n(
				{ target: "Object", stat: !0, forced: !s, sham: !a },
				{
					create: function (t, e) {
						return void 0 === e ? x(t) : it(x(t), e);
					},
					defineProperty: ot,
					defineProperties: it,
					getOwnPropertyDescriptor: at,
				}
			),
			n(
				{ target: "Object", stat: !0, forced: !s },
				{ getOwnPropertyNames: ct, getOwnPropertySymbols: lt }
			),
			n(
				{
					target: "Object",
					stat: !0,
					forced: f(function () {
						k.f(1);
					}),
				},
				{
					getOwnPropertySymbols: function (t) {
						return k.f(E(t));
					},
				}
			),
			z &&
				n(
					{
						target: "JSON",
						stat: !0,
						forced:
							!s ||
							f(function () {
								var t = W();
								return (
									"[null]" != Y([t]) ||
									"{}" != Y({ a: t }) ||
									"{}" != Y(Object(t))
								);
							}),
					},
					{
						stringify: function (t) {
							for (var e, r, n = [t], o = 1; arguments.length > o; )
								n.push(arguments[o++]);
							if (((r = e = n[1]), (_(e) || void 0 !== t) && !nt(t)))
								return (
									b(e) ||
										(e = function (t, e) {
											if (
												("function" == typeof r && (e = r.call(this, t, e)),
												!nt(e))
											)
												return e;
										}),
									(n[1] = e),
									Y.apply(z, n)
								);
						},
					}
				),
			W.prototype[V] || L(W.prototype, V, W.prototype.valueOf),
			d(W, "Symbol"),
			(u[U] = !0);
	},
	function (t, e) {
		var r;
		r = (function () {
			return this;
		})();
		try {
			r = r || new Function("return this")();
		} catch (t) {
			"object" == typeof window && (r = window);
		}
		t.exports = r;
	},
	function (t, e, r) {
		var n = r(46),
			o = r(70),
			i = r(51);
		t.exports = function (t) {
			var e = n(t),
				r = o.f;
			if (r)
				for (var s, a = r(t), c = i.f, l = 0; a.length > l; )
					c.call(t, (s = a[l++])) && e.push(s);
			return e;
		};
	},
	function (t, e, r) {
		r(17)("asyncIterator");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(6),
			i = r(2),
			s = r(12),
			a = r(3),
			c = r(9).f,
			l = r(109),
			u = i.Symbol;
		if (
			o &&
			"function" == typeof u &&
			(!("description" in u.prototype) || void 0 !== u().description)
		) {
			var f = {},
				h = function () {
					var t =
							arguments.length < 1 || void 0 === arguments[0]
								? void 0
								: String(arguments[0]),
						e = this instanceof h ? new u(t) : void 0 === t ? u() : u(t);
					return "" === t && (f[e] = !0), e;
				};
			l(h, u);
			var d = (h.prototype = u.prototype);
			d.constructor = h;
			var p = d.toString,
				g = "Symbol(test)" == String(u("test")),
				m = /^Symbol\((.*)\)[^)]+$/;
			c(d, "description", {
				configurable: !0,
				get: function () {
					var t = a(this) ? this.valueOf() : this,
						e = p.call(t);
					if (s(f, t)) return "";
					var r = g ? e.slice(7, -1) : e.replace(m, "$1");
					return "" === r ? void 0 : r;
				},
			}),
				n({ global: !0, forced: !0 }, { Symbol: h });
		}
	},
	function (t, e, r) {
		r(17)("hasInstance");
	},
	function (t, e, r) {
		r(17)("isConcatSpreadable");
	},
	function (t, e, r) {
		r(17)("iterator");
	},
	function (t, e, r) {
		r(17)("match");
	},
	function (t, e, r) {
		r(17)("matchAll");
	},
	function (t, e, r) {
		r(17)("replace");
	},
	function (t, e, r) {
		r(17)("search");
	},
	function (t, e, r) {
		r(17)("species");
	},
	function (t, e, r) {
		r(17)("split");
	},
	function (t, e, r) {
		r(17)("toPrimitive");
	},
	function (t, e, r) {
		r(17)("toStringTag");
	},
	function (t, e, r) {
		r(17)("unscopables");
	},
	function (t, e, r) {
		var n = r(0),
			o = r(115);
		n(
			{ target: "Object", stat: !0, forced: Object.assign !== o },
			{ assign: o }
		);
	},
	function (t, e, r) {
		r(0)({ target: "Object", stat: !0, sham: !r(6) }, { create: r(37) });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(6);
		n(
			{ target: "Object", stat: !0, forced: !o, sham: !o },
			{ defineProperty: r(9).f }
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(6);
		n(
			{ target: "Object", stat: !0, forced: !o, sham: !o },
			{ defineProperties: r(89) }
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(116);
		n(
			{ target: "Object", stat: !0 },
			{
				entries: function (t) {
					return o(t, !0);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(58),
			i = r(1),
			s = r(3),
			a = r(44).onFreeze,
			c = Object.freeze;
		n(
			{
				target: "Object",
				stat: !0,
				forced: i(function () {
					c(1);
				}),
				sham: !o,
			},
			{
				freeze: function (t) {
					return c && s(t) ? c(a(t)) : t;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(59),
			i = r(45);
		n(
			{ target: "Object", stat: !0 },
			{
				fromEntries: function (t) {
					var e = {};
					return (
						o(
							t,
							function (t, r) {
								i(e, t, r);
							},
							void 0,
							!0
						),
						e
					);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(18),
			s = r(15).f,
			a = r(6),
			c = o(function () {
				s(1);
			});
		n(
			{ target: "Object", stat: !0, forced: !a || c, sham: !a },
			{
				getOwnPropertyDescriptor: function (t, e) {
					return s(i(t), e);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(6),
			i = r(87),
			s = r(18),
			a = r(15),
			c = r(45);
		n(
			{ target: "Object", stat: !0, sham: !o },
			{
				getOwnPropertyDescriptors: function (t) {
					for (
						var e, r, n = s(t), o = a.f, l = i(n), u = {}, f = 0;
						l.length > f;

					)
						void 0 !== (r = o(n, (e = l[f++]))) && c(u, e, r);
					return u;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(114).f;
		n(
			{
				target: "Object",
				stat: !0,
				forced: o(function () {
					return !Object.getOwnPropertyNames(1);
				}),
			},
			{ getOwnPropertyNames: i }
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(11),
			s = r(30),
			a = r(91);
		n(
			{
				target: "Object",
				stat: !0,
				forced: o(function () {
					s(1);
				}),
				sham: !a,
			},
			{
				getPrototypeOf: function (t) {
					return s(i(t));
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "Object", stat: !0 }, { is: r(118) });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(3),
			s = Object.isExtensible;
		n(
			{
				target: "Object",
				stat: !0,
				forced: o(function () {
					s(1);
				}),
			},
			{
				isExtensible: function (t) {
					return !!i(t) && (!s || s(t));
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(3),
			s = Object.isFrozen;
		n(
			{
				target: "Object",
				stat: !0,
				forced: o(function () {
					s(1);
				}),
			},
			{
				isFrozen: function (t) {
					return !i(t) || (!!s && s(t));
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(3),
			s = Object.isSealed;
		n(
			{
				target: "Object",
				stat: !0,
				forced: o(function () {
					s(1);
				}),
			},
			{
				isSealed: function (t) {
					return !i(t) || (!!s && s(t));
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(11),
			i = r(46);
		n(
			{
				target: "Object",
				stat: !0,
				forced: r(1)(function () {
					i(1);
				}),
			},
			{
				keys: function (t) {
					return i(o(t));
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(3),
			i = r(44).onFreeze,
			s = r(58),
			a = r(1),
			c = Object.preventExtensions;
		n(
			{
				target: "Object",
				stat: !0,
				forced: a(function () {
					c(1);
				}),
				sham: !s,
			},
			{
				preventExtensions: function (t) {
					return c && o(t) ? c(i(t)) : t;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(3),
			i = r(44).onFreeze,
			s = r(58),
			a = r(1),
			c = Object.seal;
		n(
			{
				target: "Object",
				stat: !0,
				forced: a(function () {
					c(1);
				}),
				sham: !s,
			},
			{
				seal: function (t) {
					return c && o(t) ? c(i(t)) : t;
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "Object", stat: !0 }, { setPrototypeOf: r(47) });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(116);
		n(
			{ target: "Object", stat: !0 },
			{
				values: function (t) {
					return o(t);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(16),
			o = r(186),
			i = Object.prototype;
		o !== i.toString && n(i, "toString", o, { unsafe: !0 });
	},
	function (t, e, r) {
		"use strict";
		var n = r(62),
			o = {};
		(o[r(7)("toStringTag")] = "z"),
			(t.exports =
				"[object z]" !== String(o)
					? function () {
							return "[object " + n(this) + "]";
					  }
					: o.toString);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(6),
			i = r(72),
			s = r(11),
			a = r(21),
			c = r(9);
		o &&
			n(
				{ target: "Object", proto: !0, forced: i },
				{
					__defineGetter__: function (t, e) {
						c.f(s(this), t, { get: a(e), enumerable: !0, configurable: !0 });
					},
				}
			);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(6),
			i = r(72),
			s = r(11),
			a = r(21),
			c = r(9);
		o &&
			n(
				{ target: "Object", proto: !0, forced: i },
				{
					__defineSetter__: function (t, e) {
						c.f(s(this), t, { set: a(e), enumerable: !0, configurable: !0 });
					},
				}
			);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(6),
			i = r(72),
			s = r(11),
			a = r(27),
			c = r(30),
			l = r(15).f;
		o &&
			n(
				{ target: "Object", proto: !0, forced: i },
				{
					__lookupGetter__: function (t) {
						var e,
							r = s(this),
							n = a(t, !0);
						do {
							if ((e = l(r, n))) return e.get;
						} while ((r = c(r)));
					},
				}
			);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(6),
			i = r(72),
			s = r(11),
			a = r(27),
			c = r(30),
			l = r(15).f;
		o &&
			n(
				{ target: "Object", proto: !0, forced: i },
				{
					__lookupSetter__: function (t) {
						var e,
							r = s(this),
							n = a(t, !0);
						do {
							if ((e = l(r, n))) return e.set;
						} while ((r = c(r)));
					},
				}
			);
	},
	function (t, e, r) {
		r(0)({ target: "Function", proto: !0 }, { bind: r(120) });
	},
	function (t, e, r) {
		var n = r(6),
			o = r(9).f,
			i = Function.prototype,
			s = i.toString,
			a = /^\s*function ([^ (]*)/;
		!n ||
			"name" in i ||
			o(i, "name", {
				configurable: !0,
				get: function () {
					try {
						return s.call(this).match(a)[1];
					} catch (t) {
						return "";
					}
				},
			});
	},
	function (t, e, r) {
		"use strict";
		var n = r(3),
			o = r(9),
			i = r(30),
			s = r(7)("hasInstance"),
			a = Function.prototype;
		s in a ||
			o.f(a, s, {
				value: function (t) {
					if ("function" != typeof this || !n(t)) return !1;
					if (!n(this.prototype)) return t instanceof this;
					for (; (t = i(t)); ) if (this.prototype === t) return !0;
					return !1;
				},
			});
	},
	function (t, e, r) {
		var n = r(0),
			o = r(121);
		n(
			{
				target: "Array",
				stat: !0,
				forced: !r(73)(function (t) {
					Array.from(t);
				}),
			},
			{ from: o }
		);
	},
	function (t, e, r) {
		r(0)({ target: "Array", stat: !0 }, { isArray: r(43) });
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(1),
			i = r(45);
		n(
			{
				target: "Array",
				stat: !0,
				forced: o(function () {
					function t() {}
					return !(Array.of.call(t) instanceof t);
				}),
			},
			{
				of: function () {
					for (
						var t = 0,
							e = arguments.length,
							r = new ("function" == typeof this ? this : Array)(e);
						e > t;

					)
						i(r, t, arguments[t++]);
					return (r.length = e), r;
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(1),
			i = r(43),
			s = r(3),
			a = r(11),
			c = r(8),
			l = r(45),
			u = r(63),
			f = r(64),
			h = r(7)("isConcatSpreadable"),
			d = !o(function () {
				var t = [];
				return (t[h] = !1), t.concat()[0] !== t;
			}),
			p = f("concat"),
			g = function (t) {
				if (!s(t)) return !1;
				var e = t[h];
				return void 0 !== e ? !!e : i(t);
			};
		n(
			{ target: "Array", proto: !0, forced: !d || !p },
			{
				concat: function (t) {
					var e,
						r,
						n,
						o,
						i,
						s = a(this),
						f = u(s, 0),
						h = 0;
					for (e = -1, n = arguments.length; e < n; e++)
						if (((i = -1 === e ? s : arguments[e]), g(i))) {
							if (h + (o = c(i.length)) > 9007199254740991)
								throw TypeError("Maximum allowed index exceeded");
							for (r = 0; r < o; r++, h++) r in i && l(f, h, i[r]);
						} else {
							if (h >= 9007199254740991)
								throw TypeError("Maximum allowed index exceeded");
							l(f, h++, i);
						}
					return (f.length = h), f;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(122),
			i = r(39);
		n({ target: "Array", proto: !0 }, { copyWithin: o }), i("copyWithin");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(31),
			s = o(4);
		n(
			{ target: "Array", proto: !0, forced: i("every") },
			{
				every: function (t) {
					return s(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(92),
			i = r(39);
		n({ target: "Array", proto: !0 }, { fill: o }), i("fill");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(64),
			s = o(2);
		n(
			{ target: "Array", proto: !0, forced: !i("filter") },
			{
				filter: function (t) {
					return s(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(39),
			s = o(5),
			a = !0;
		"find" in [] &&
			Array(1).find(function () {
				a = !1;
			}),
			n(
				{ target: "Array", proto: !0, forced: a },
				{
					find: function (t) {
						return s(this, t, arguments.length > 1 ? arguments[1] : void 0);
					},
				}
			),
			i("find");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(39),
			s = o(6),
			a = !0;
		"findIndex" in [] &&
			Array(1).findIndex(function () {
				a = !1;
			}),
			n(
				{ target: "Array", proto: !0, forced: a },
				{
					findIndex: function (t) {
						return s(this, t, arguments.length > 1 ? arguments[1] : void 0);
					},
				}
			),
			i("findIndex");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(123),
			i = r(11),
			s = r(8),
			a = r(25),
			c = r(63);
		n(
			{ target: "Array", proto: !0 },
			{
				flat: function () {
					var t = arguments[0],
						e = i(this),
						r = s(e.length),
						n = c(e, 0);
					return (n.length = o(n, e, e, r, 0, void 0 === t ? 1 : a(t))), n;
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(123),
			i = r(11),
			s = r(8),
			a = r(21),
			c = r(63);
		n(
			{ target: "Array", proto: !0 },
			{
				flatMap: function (t) {
					var e,
						r = i(this),
						n = s(r.length);
					return (
						a(t),
						((e = c(r, 0)).length = o(e, r, r, n, 0, 1, t, arguments[1])),
						e
					);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(207);
		n({ target: "Array", proto: !0, forced: [].forEach != o }, { forEach: o });
	},
	function (t, e, r) {
		"use strict";
		var n = r(13),
			o = r(31),
			i = n(0),
			s = o("forEach");
		t.exports = s
			? function (t) {
					return i(this, t, arguments[1]);
			  }
			: [].forEach;
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(56),
			i = r(39),
			s = o(!0);
		n(
			{ target: "Array", proto: !0 },
			{
				includes: function (t) {
					return s(this, t, arguments.length > 1 ? arguments[1] : void 0);
				},
			}
		),
			i("includes");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(31),
			i = r(56)(!1),
			s = [].indexOf,
			a = !!s && 1 / [1].indexOf(1, -0) < 0,
			c = o("indexOf");
		n(
			{ target: "Array", proto: !0, forced: a || c },
			{
				indexOf: function (t) {
					return a ? s.apply(this, arguments) || 0 : i(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(52),
			i = r(18),
			s = r(31),
			a = [].join,
			c = o != Object,
			l = s("join", ",");
		n(
			{ target: "Array", proto: !0, forced: c || l },
			{
				join: function (t) {
					return a.call(i(this), void 0 === t ? "," : t);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(124);
		n(
			{ target: "Array", proto: !0, forced: o !== [].lastIndexOf },
			{ lastIndexOf: o }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(64),
			s = o(1);
		n(
			{ target: "Array", proto: !0, forced: !i("map") },
			{
				map: function (t) {
					return s(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(74);
		n(
			{ target: "Array", proto: !0, forced: r(31)("reduce") },
			{
				reduce: function (t) {
					return o(this, t, arguments.length, arguments[1], !1);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(74);
		n(
			{ target: "Array", proto: !0, forced: r(31)("reduceRight") },
			{
				reduceRight: function (t) {
					return o(this, t, arguments.length, arguments[1], !0);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(43),
			i = [].reverse,
			s = [1, 2];
		n(
			{ target: "Array", proto: !0, forced: String(s) === String(s.reverse()) },
			{
				reverse: function () {
					return o(this) && (this.length = this.length), i.call(this);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(3),
			i = r(43),
			s = r(36),
			a = r(8),
			c = r(18),
			l = r(45),
			u = r(64),
			f = r(7)("species"),
			h = [].slice,
			d = Math.max;
		n(
			{ target: "Array", proto: !0, forced: !u("slice") },
			{
				slice: function (t, e) {
					var r,
						n,
						u,
						p = c(this),
						g = a(p.length),
						m = s(t, g),
						v = s(void 0 === e ? g : e, g);
					if (
						i(p) &&
						("function" != typeof (r = p.constructor) ||
						(r !== Array && !i(r.prototype))
							? o(r) && null === (r = r[f]) && (r = void 0)
							: (r = void 0),
						r === Array || void 0 === r)
					)
						return h.call(p, m, v);
					for (
						n = new (void 0 === r ? Array : r)(d(v - m, 0)), u = 0;
						m < v;
						m++, u++
					)
						m in p && l(n, u, p[m]);
					return (n.length = u), n;
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(31),
			s = o(3);
		n(
			{ target: "Array", proto: !0, forced: i("some") },
			{
				some: function (t) {
					return s(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(21),
			i = r(11),
			s = r(1),
			a = r(31),
			c = [].sort,
			l = [1, 2, 3],
			u = s(function () {
				l.sort(void 0);
			}),
			f = s(function () {
				l.sort(null);
			}),
			h = a("sort");
		n(
			{ target: "Array", proto: !0, forced: u || !f || h },
			{
				sort: function (t) {
					return void 0 === t ? c.call(i(this)) : c.call(i(this), o(t));
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(36),
			i = r(25),
			s = r(8),
			a = r(11),
			c = r(63),
			l = r(45),
			u = r(64),
			f = Math.max,
			h = Math.min;
		n(
			{ target: "Array", proto: !0, forced: !u("splice") },
			{
				splice: function (t, e) {
					var r,
						n,
						u,
						d,
						p,
						g,
						m = a(this),
						v = s(m.length),
						y = o(t, v),
						b = arguments.length;
					if (
						(0 === b
							? (r = n = 0)
							: 1 === b
							? ((r = 0), (n = v - y))
							: ((r = b - 2), (n = h(f(i(e), 0), v - y))),
						v + r - n > 9007199254740991)
					)
						throw TypeError("Maximum allowed length exceeded");
					for (u = c(m, n), d = 0; d < n; d++)
						(p = y + d) in m && l(u, d, m[p]);
					if (((u.length = n), r < n)) {
						for (d = y; d < v - n; d++)
							(g = d + r), (p = d + n) in m ? (m[g] = m[p]) : delete m[g];
						for (d = v; d > v - n + r; d--) delete m[d - 1];
					} else if (r > n)
						for (d = v - n; d > y; d--)
							(g = d + r - 1),
								(p = d + n - 1) in m ? (m[g] = m[p]) : delete m[g];
					for (d = 0; d < r; d++) m[d + y] = arguments[d + 2];
					return (m.length = v - n + r), u;
				},
			}
		);
	},
	function (t, e, r) {
		r(48)("Array");
	},
	function (t, e, r) {
		r(39)("flat");
	},
	function (t, e, r) {
		r(39)("flatMap");
	},
	function (t, e, r) {
		var n = r(0),
			o = r(36),
			i = String.fromCharCode,
			s = String.fromCodePoint;
		n(
			{ target: "String", stat: !0, forced: !!s && 1 != s.length },
			{
				fromCodePoint: function (t) {
					for (var e, r = [], n = arguments.length, s = 0; n > s; ) {
						if (((e = +arguments[s++]), o(e, 1114111) !== e))
							throw RangeError(e + " is not a valid code point");
						r.push(
							e < 65536
								? i(e)
								: i(55296 + ((e -= 65536) >> 10), (e % 1024) + 56320)
						);
					}
					return r.join("");
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(18),
			i = r(8);
		n(
			{ target: "String", stat: !0 },
			{
				raw: function (t) {
					for (
						var e = o(t.raw),
							r = i(e.length),
							n = arguments.length,
							s = [],
							a = 0;
						r > a;

					)
						s.push(String(e[a++])), a < n && s.push(String(arguments[a]));
					return s.join("");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(75);
		n(
			{ target: "String", proto: !0 },
			{
				codePointAt: function (t) {
					return o(this, t);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(8),
			i = r(96),
			s = r(98),
			a = "".endsWith,
			c = Math.min;
		n(
			{ target: "String", proto: !0, forced: !s("endsWith") },
			{
				endsWith: function (t) {
					var e = i(this, t, "endsWith"),
						r = arguments.length > 1 ? arguments[1] : void 0,
						n = o(e.length),
						s = void 0 === r ? n : c(o(r), n),
						l = String(t);
					return a ? a.call(e, l, s) : e.slice(s - l.length, s) === l;
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(96);
		n(
			{ target: "String", proto: !0, forced: !r(98)("includes") },
			{
				includes: function (t) {
					return !!~o(this, t, "includes").indexOf(
						t,
						arguments.length > 1 ? arguments[1] : void 0
					);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(76),
			o = r(4),
			i = r(8),
			s = r(19),
			a = r(78),
			c = r(79);
		n("match", 1, function (t, e, r) {
			return [
				function (e) {
					var r = s(this),
						n = null == e ? void 0 : e[t];
					return void 0 !== n ? n.call(e, r) : new RegExp(e)[t](String(r));
				},
				function (t) {
					var n = r(e, t, this);
					if (n.done) return n.value;
					var s = o(t),
						l = String(this);
					if (!s.global) return c(s, l);
					var u = s.unicode;
					s.lastIndex = 0;
					for (var f, h = [], d = 0; null !== (f = c(s, l)); ) {
						var p = String(f[0]);
						(h[d] = p),
							"" === p && (s.lastIndex = a(l, i(s.lastIndex), u)),
							d++;
					}
					return 0 === d ? null : h;
				},
			];
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(95),
			i = r(19),
			s = r(8),
			a = r(21),
			c = r(4),
			l = r(62),
			u = r(65),
			f = r(14),
			h = r(7),
			d = r(32),
			p = r(78),
			g = r(20),
			m = r(35),
			v = h("matchAll"),
			y = g.set,
			b = g.getterFor("RegExp String Iterator"),
			S = RegExp.prototype,
			_ = S.exec,
			E = o(
				function (t, e, r, n) {
					y(this, {
						type: "RegExp String Iterator",
						regexp: t,
						string: e,
						global: r,
						unicode: n,
						done: !1,
					});
				},
				"RegExp String",
				function () {
					var t = b(this);
					if (t.done) return { value: void 0, done: !0 };
					var e = t.regexp,
						r = t.string,
						n = (function (t, e) {
							var r,
								n = t.exec;
							if ("function" == typeof n) {
								if ("object" != typeof (r = n.call(t, e)))
									throw TypeError("Incorrect exec result");
								return r;
							}
							return _.call(t, e);
						})(e, r);
					return null === n
						? { value: void 0, done: (t.done = !0) }
						: t.global
						? ("" == String(n[0]) &&
								(e.lastIndex = p(r, s(e.lastIndex), t.unicode)),
						  { value: n, done: !1 })
						: ((t.done = !0), { value: n, done: !1 });
				}
			),
			T = function (t) {
				var e,
					r,
					n,
					o,
					i,
					a,
					l = c(this),
					f = String(t);
				return (
					(e = d(l, RegExp)),
					void 0 === (r = l.flags) &&
						l instanceof RegExp &&
						!("flags" in S) &&
						(r = u.call(l)),
					(n = void 0 === r ? "" : String(r)),
					(o = new e(e === RegExp ? l.source : l, n)),
					(i = !!~n.indexOf("g")),
					(a = !!~n.indexOf("u")),
					(o.lastIndex = s(l.lastIndex)),
					new E(o, f, i, a)
				);
			};
		n(
			{ target: "String", proto: !0 },
			{
				matchAll: function (t) {
					var e,
						r,
						n,
						o = i(this);
					return null != t &&
						(void 0 === (r = t[v]) && m && "RegExp" == l(t) && (r = T),
						null != r)
						? a(r).call(t, o)
						: ((e = String(o)),
						  (n = new RegExp(t, "g")),
						  m ? T.call(n, e) : n[v](e));
				},
			}
		),
			m || v in S || f(S, v, T);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(126);
		n(
			{ target: "String", proto: !0, forced: r(127) },
			{
				padEnd: function (t) {
					return o(this, t, arguments.length > 1 ? arguments[1] : void 0, !1);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(126);
		n(
			{ target: "String", proto: !0, forced: r(127) },
			{
				padStart: function (t) {
					return o(this, t, arguments.length > 1 ? arguments[1] : void 0, !0);
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "String", proto: !0 }, { repeat: r(99) });
	},
	function (t, e, r) {
		"use strict";
		var n = r(76),
			o = r(4),
			i = r(11),
			s = r(8),
			a = r(25),
			c = r(19),
			l = r(78),
			u = r(79),
			f = Math.max,
			h = Math.min,
			d = Math.floor,
			p = /\$([$&'`]|\d\d?|<[^>]*>)/g,
			g = /\$([$&'`]|\d\d?)/g;
		n("replace", 2, function (t, e, r) {
			return [
				function (r, n) {
					var o = c(this),
						i = null == r ? void 0 : r[t];
					return void 0 !== i ? i.call(r, o, n) : e.call(String(o), r, n);
				},
				function (t, i) {
					var c = r(e, t, this, i);
					if (c.done) return c.value;
					var d = o(t),
						p = String(this),
						g = "function" == typeof i;
					g || (i = String(i));
					var m = d.global;
					if (m) {
						var v = d.unicode;
						d.lastIndex = 0;
					}
					for (var y = []; ; ) {
						var b = u(d, p);
						if (null === b) break;
						if ((y.push(b), !m)) break;
						"" === String(b[0]) && (d.lastIndex = l(p, s(d.lastIndex), v));
					}
					for (var S, _ = "", E = 0, T = 0; T < y.length; T++) {
						b = y[T];
						for (
							var A = String(b[0]),
								w = f(h(a(b.index), p.length), 0),
								x = [],
								R = 1;
							R < b.length;
							R++
						)
							x.push(void 0 === (S = b[R]) ? S : String(S));
						var I = b.groups;
						if (g) {
							var C = [A].concat(x, w, p);
							void 0 !== I && C.push(I);
							var O = String(i.apply(void 0, C));
						} else O = n(A, p, w, x, I, i);
						w >= E && ((_ += p.slice(E, w) + O), (E = w + A.length));
					}
					return _ + p.slice(E);
				},
			];
			function n(t, r, n, o, s, a) {
				var c = n + t.length,
					l = o.length,
					u = g;
				return (
					void 0 !== s && ((s = i(s)), (u = p)),
					e.call(a, u, function (e, i) {
						var a;
						switch (i.charAt(0)) {
							case "$":
								return "$";
							case "&":
								return t;
							case "`":
								return r.slice(0, n);
							case "'":
								return r.slice(c);
							case "<":
								a = s[i.slice(1, -1)];
								break;
							default:
								var u = +i;
								if (0 === u) return e;
								if (u > l) {
									var f = d(u / 10);
									return 0 === f
										? e
										: f <= l
										? void 0 === o[f - 1]
											? i.charAt(1)
											: o[f - 1] + i.charAt(1)
										: e;
								}
								a = o[u - 1];
						}
						return void 0 === a ? "" : a;
					})
				);
			}
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(76),
			o = r(4),
			i = r(19),
			s = r(118),
			a = r(79);
		n("search", 1, function (t, e, r) {
			return [
				function (e) {
					var r = i(this),
						n = null == e ? void 0 : e[t];
					return void 0 !== n ? n.call(e, r) : new RegExp(e)[t](String(r));
				},
				function (t) {
					var n = r(e, t, this);
					if (n.done) return n.value;
					var i = o(t),
						c = String(this),
						l = i.lastIndex;
					s(l, 0) || (i.lastIndex = 0);
					var u = a(i, c);
					return (
						s(i.lastIndex, l) || (i.lastIndex = l), null === u ? -1 : u.index
					);
				},
			];
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(76),
			o = r(97),
			i = r(4),
			s = r(19),
			a = r(32),
			c = r(78),
			l = r(8),
			u = r(79),
			f = r(77),
			h = r(1),
			d = [].push,
			p = Math.min,
			g = !h(function () {
				return !RegExp(4294967295, "y");
			});
		n(
			"split",
			2,
			function (t, e, r) {
				var n;
				return (
					(n =
						"c" == "abbc".split(/(b)*/)[1] ||
						4 != "test".split(/(?:)/, -1).length ||
						2 != "ab".split(/(?:ab)*/).length ||
						4 != ".".split(/(.?)(.?)/).length ||
						".".split(/()()/).length > 1 ||
						"".split(/.?/).length
							? function (t, r) {
									var n = String(s(this)),
										i = void 0 === r ? 4294967295 : r >>> 0;
									if (0 === i) return [];
									if (void 0 === t) return [n];
									if (!o(t)) return e.call(n, t, i);
									for (
										var a,
											c,
											l,
											u = [],
											h =
												(t.ignoreCase ? "i" : "") +
												(t.multiline ? "m" : "") +
												(t.unicode ? "u" : "") +
												(t.sticky ? "y" : ""),
											p = 0,
											g = new RegExp(t.source, h + "g");
										(a = f.call(g, n)) &&
										!(
											(c = g.lastIndex) > p &&
											(u.push(n.slice(p, a.index)),
											a.length > 1 &&
												a.index < n.length &&
												d.apply(u, a.slice(1)),
											(l = a[0].length),
											(p = c),
											u.length >= i)
										);

									)
										g.lastIndex === a.index && g.lastIndex++;
									return (
										p === n.length
											? (!l && g.test("")) || u.push("")
											: u.push(n.slice(p)),
										u.length > i ? u.slice(0, i) : u
									);
							  }
							: "0".split(void 0, 0).length
							? function (t, r) {
									return void 0 === t && 0 === r ? [] : e.call(this, t, r);
							  }
							: e),
					[
						function (e, r) {
							var o = s(this),
								i = null == e ? void 0 : e[t];
							return void 0 !== i ? i.call(e, o, r) : n.call(String(o), e, r);
						},
						function (t, o) {
							var s = r(n, t, this, o, n !== e);
							if (s.done) return s.value;
							var f = i(t),
								h = String(this),
								d = a(f, RegExp),
								m = f.unicode,
								v =
									(f.ignoreCase ? "i" : "") +
									(f.multiline ? "m" : "") +
									(f.unicode ? "u" : "") +
									(g ? "y" : "g"),
								y = new d(g ? f : "^(?:" + f.source + ")", v),
								b = void 0 === o ? 4294967295 : o >>> 0;
							if (0 === b) return [];
							if (0 === h.length) return null === u(y, h) ? [h] : [];
							for (var S = 0, _ = 0, E = []; _ < h.length; ) {
								y.lastIndex = g ? _ : 0;
								var T,
									A = u(y, g ? h : h.slice(_));
								if (
									null === A ||
									(T = p(l(y.lastIndex + (g ? 0 : _)), h.length)) === S
								)
									_ = c(h, _, m);
								else {
									if ((E.push(h.slice(S, _)), E.length === b)) return E;
									for (var w = 1; w <= A.length - 1; w++)
										if ((E.push(A[w]), E.length === b)) return E;
									_ = S = T;
								}
							}
							return E.push(h.slice(S)), E;
						},
					]
				);
			},
			!g
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(8),
			i = r(96),
			s = r(98),
			a = "".startsWith;
		n(
			{ target: "String", proto: !0, forced: !s("startsWith") },
			{
				startsWith: function (t) {
					var e = i(this, t, "startsWith"),
						r = o(
							Math.min(arguments.length > 1 ? arguments[1] : void 0, e.length)
						),
						n = String(t);
					return a ? a.call(e, n, r) : e.slice(r, r + n.length) === n;
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(49);
		n(
			{ target: "String", proto: !0, forced: r(101)("trim") },
			{
				trim: function () {
					return o(this, 3);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(49),
			i = r(101)("trimStart"),
			s = i
				? function () {
						return o(this, 1);
				  }
				: "".trimStart;
		n(
			{ target: "String", proto: !0, forced: i },
			{ trimStart: s, trimLeft: s }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(49),
			i = r(101)("trimEnd"),
			s = i
				? function () {
						return o(this, 2);
				  }
				: "".trimEnd;
		n({ target: "String", proto: !0, forced: i }, { trimEnd: s, trimRight: s });
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("anchor") },
			{
				anchor: function (t) {
					return o(this, "a", "name", t);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("big") },
			{
				big: function () {
					return o(this, "big", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("blink") },
			{
				blink: function () {
					return o(this, "blink", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("bold") },
			{
				bold: function () {
					return o(this, "b", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("fixed") },
			{
				fixed: function () {
					return o(this, "tt", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("fontcolor") },
			{
				fontcolor: function (t) {
					return o(this, "font", "color", t);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("fontsize") },
			{
				fontsize: function (t) {
					return o(this, "font", "size", t);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("italics") },
			{
				italics: function () {
					return o(this, "i", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("link") },
			{
				link: function (t) {
					return o(this, "a", "href", t);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("small") },
			{
				small: function () {
					return o(this, "small", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("strike") },
			{
				strike: function () {
					return o(this, "strike", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("sub") },
			{
				sub: function () {
					return o(this, "sub", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23);
		n(
			{ target: "String", proto: !0, forced: r(24)("sup") },
			{
				sup: function () {
					return o(this, "sup", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(6),
			o = r(2),
			i = r(57),
			s = r(102),
			a = r(9).f,
			c = r(42).f,
			l = r(97),
			u = r(65),
			f = r(16),
			h = r(1),
			d = r(48),
			p = r(7)("match"),
			g = o.RegExp,
			m = g.prototype,
			v = /a/g,
			y = /a/g,
			b = new g(v) !== v;
		if (
			i(
				"RegExp",
				n &&
					(!b ||
						h(function () {
							return (y[p] = !1), g(v) != v || g(y) == y || "/a/i" != g(v, "i");
						}))
			)
		) {
			for (
				var S = function (t, e) {
						var r = this instanceof S,
							n = l(t),
							o = void 0 === e;
						return !r && n && t.constructor === S && o
							? t
							: s(
									b
										? new g(n && !o ? t.source : t, e)
										: g(
												(n = t instanceof S) ? t.source : t,
												n && o ? u.call(t) : e
										  ),
									r ? this : m,
									S
							  );
					},
					_ = function (t) {
						(t in S) ||
							a(S, t, {
								configurable: !0,
								get: function () {
									return g[t];
								},
								set: function (e) {
									g[t] = e;
								},
							});
					},
					E = c(g),
					T = 0;
				T < E.length;

			)
				_(E[T++]);
			(m.constructor = S), (S.prototype = m), f(o, "RegExp", S);
		}
		d("RegExp");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(77);
		n({ target: "RegExp", proto: !0, forced: /./.exec !== o }, { exec: o });
	},
	function (t, e, r) {
		var n = r(6),
			o = r(9),
			i = r(65);
		n &&
			"g" != /./g.flags &&
			o.f(RegExp.prototype, "flags", { configurable: !0, get: i });
	},
	function (t, e, r) {
		"use strict";
		var n = r(16),
			o = r(4),
			i = r(1),
			s = r(65),
			a = /./.toString,
			c = RegExp.prototype,
			l = i(function () {
				return "/a/b" != a.call({ source: "a", flags: "b" });
			}),
			u = "toString" != a.name;
		(l || u) &&
			n(
				RegExp.prototype,
				"toString",
				function () {
					var t = o(this),
						e = String(t.source),
						r = t.flags;
					return (
						"/" +
						e +
						"/" +
						String(
							void 0 === r && t instanceof RegExp && !("flags" in c)
								? s.call(t)
								: r
						)
					);
				},
				{ unsafe: !0 }
			);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(129);
		n({ global: !0, forced: parseInt != o }, { parseInt: o });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(130);
		n({ global: !0, forced: parseFloat != o }, { parseFloat: o });
	},
	function (t, e, r) {
		"use strict";
		var n = r(6),
			o = r(2),
			i = r(57),
			s = r(16),
			a = r(12),
			c = r(28),
			l = r(102),
			u = r(27),
			f = r(1),
			h = r(37),
			d = r(42).f,
			p = r(15).f,
			g = r(9).f,
			m = r(49),
			v = o.Number,
			y = v.prototype,
			b = "Number" == c(h(y)),
			S = "trim" in String.prototype,
			_ = function (t) {
				var e,
					r,
					n,
					o,
					i,
					s,
					a,
					c,
					l = u(t, !1);
				if ("string" == typeof l && l.length > 2)
					if (
						43 === (e = (l = S ? l.trim() : m(l, 3)).charCodeAt(0)) ||
						45 === e
					) {
						if (88 === (r = l.charCodeAt(2)) || 120 === r) return NaN;
					} else if (48 === e) {
						switch (l.charCodeAt(1)) {
							case 66:
							case 98:
								(n = 2), (o = 49);
								break;
							case 79:
							case 111:
								(n = 8), (o = 55);
								break;
							default:
								return +l;
						}
						for (s = (i = l.slice(2)).length, a = 0; a < s; a++)
							if ((c = i.charCodeAt(a)) < 48 || c > o) return NaN;
						return parseInt(i, n);
					}
				return +l;
			};
		if (i("Number", !v(" 0o1") || !v("0b1") || v("+0x1"))) {
			for (
				var E,
					T = function (t) {
						var e = arguments.length < 1 ? 0 : t,
							r = this;
						return r instanceof T &&
							(b
								? f(function () {
										y.valueOf.call(r);
								  })
								: "Number" != c(r))
							? l(new v(_(e)), r, T)
							: _(e);
					},
					A = n
						? d(v)
						: "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(
								","
						  ),
					w = 0;
				A.length > w;
				w++
			)
				a(v, (E = A[w])) && !a(T, E) && g(T, E, p(v, E));
			(T.prototype = y), (y.constructor = T), s(o, "Number", T);
		}
	},
	function (t, e, r) {
		r(0)({ target: "Number", stat: !0 }, { EPSILON: Math.pow(2, -52) });
	},
	function (t, e, r) {
		r(0)({ target: "Number", stat: !0 }, { isFinite: r(262) });
	},
	function (t, e, r) {
		var n = r(2).isFinite;
		t.exports =
			Number.isFinite ||
			function (t) {
				return "number" == typeof t && n(t);
			};
	},
	function (t, e, r) {
		r(0)({ target: "Number", stat: !0 }, { isInteger: r(131) });
	},
	function (t, e, r) {
		r(0)(
			{ target: "Number", stat: !0 },
			{
				isNaN: function (t) {
					return t != t;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(131),
			i = Math.abs;
		n(
			{ target: "Number", stat: !0 },
			{
				isSafeInteger: function (t) {
					return o(t) && i(t) <= 9007199254740991;
				},
			}
		);
	},
	function (t, e, r) {
		r(0)(
			{ target: "Number", stat: !0 },
			{ MAX_SAFE_INTEGER: 9007199254740991 }
		);
	},
	function (t, e, r) {
		r(0)(
			{ target: "Number", stat: !0 },
			{ MIN_SAFE_INTEGER: -9007199254740991 }
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(130);
		n(
			{ target: "Number", stat: !0, forced: Number.parseFloat != o },
			{ parseFloat: o }
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(129);
		n(
			{ target: "Number", stat: !0, forced: Number.parseInt != o },
			{ parseInt: o }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(25),
			i = r(132),
			s = r(99),
			a = r(1),
			c = (1).toFixed,
			l = Math.floor,
			u = [0, 0, 0, 0, 0, 0],
			f = function (t, e) {
				for (var r = -1, n = e; ++r < 6; )
					(n += t * u[r]), (u[r] = n % 1e7), (n = l(n / 1e7));
			},
			h = function (t) {
				for (var e = 6, r = 0; --e >= 0; )
					(r += u[e]), (u[e] = l(r / t)), (r = (r % t) * 1e7);
			},
			d = function () {
				for (var t = 6, e = ""; --t >= 0; )
					if ("" !== e || 0 === t || 0 !== u[t]) {
						var r = String(u[t]);
						e = "" === e ? r : e + s.call("0", 7 - r.length) + r;
					}
				return e;
			},
			p = function (t, e, r) {
				return 0 === e
					? r
					: e % 2 == 1
					? p(t, e - 1, r * t)
					: p(t * t, e / 2, r);
			};
		n(
			{
				target: "Number",
				proto: !0,
				forced:
					(c &&
						("0.000" !== (8e-5).toFixed(3) ||
							"1" !== (0.9).toFixed(0) ||
							"1.25" !== (1.255).toFixed(2) ||
							"1000000000000000128" !== (0xde0b6b3a7640080).toFixed(0))) ||
					!a(function () {
						c.call({});
					}),
			},
			{
				toFixed: function (t) {
					var e,
						r,
						n,
						a,
						c = i(this),
						l = o(t),
						u = "",
						g = "0";
					if (l < 0 || l > 20) throw RangeError("Incorrect fraction digits");
					if (c != c) return "NaN";
					if (c <= -1e21 || c >= 1e21) return String(c);
					if ((c < 0 && ((u = "-"), (c = -c)), c > 1e-21))
						if (
							((r =
								(e =
									(function (t) {
										for (var e = 0, r = t; r >= 4096; ) (e += 12), (r /= 4096);
										for (; r >= 2; ) (e += 1), (r /= 2);
										return e;
									})(c * p(2, 69, 1)) - 69) < 0
									? c * p(2, -e, 1)
									: c / p(2, e, 1)),
							(r *= 4503599627370496),
							(e = 52 - e) > 0)
						) {
							for (f(0, r), n = l; n >= 7; ) f(1e7, 0), (n -= 7);
							for (f(p(10, n, 1), 0), n = e - 1; n >= 23; )
								h(1 << 23), (n -= 23);
							h(1 << n), f(1, 1), h(2), (g = d());
						} else f(0, r), f(1 << -e, 0), (g = d() + s.call("0", l));
					return (g =
						l > 0
							? u +
							  ((a = g.length) <= l
									? "0." + s.call("0", l - a) + g
									: g.slice(0, a - l) + "." + g.slice(a - l))
							: u + g);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(1),
			i = r(132),
			s = (1).toPrecision;
		n(
			{
				target: "Number",
				proto: !0,
				forced:
					o(function () {
						return "1" !== s.call(1, void 0);
					}) ||
					!o(function () {
						s.call({});
					}),
			},
			{
				toPrecision: function (t) {
					return void 0 === t ? s.call(i(this)) : s.call(i(this), t);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(133),
			i = Math.acosh,
			s = Math.log,
			a = Math.sqrt,
			c = Math.LN2;
		n(
			{
				target: "Math",
				stat: !0,
				forced:
					!i || 710 != Math.floor(i(Number.MAX_VALUE)) || i(1 / 0) != 1 / 0,
			},
			{
				acosh: function (t) {
					return (t = +t) < 1
						? NaN
						: t > 94906265.62425156
						? s(t) + c
						: o(t - 1 + a(t - 1) * a(t + 1));
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.asinh,
			i = Math.log,
			s = Math.sqrt;
		n(
			{ target: "Math", stat: !0, forced: !(o && 1 / o(0) > 0) },
			{
				asinh: function t(e) {
					return isFinite((e = +e)) && 0 != e
						? e < 0
							? -t(-e)
							: i(e + s(e * e + 1))
						: e;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.atanh,
			i = Math.log;
		n(
			{ target: "Math", stat: !0, forced: !(o && 1 / o(-0) < 0) },
			{
				atanh: function (t) {
					return 0 == (t = +t) ? t : i((1 + t) / (1 - t)) / 2;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(103),
			i = Math.abs,
			s = Math.pow;
		n(
			{ target: "Math", stat: !0 },
			{
				cbrt: function (t) {
					return o((t = +t)) * s(i(t), 1 / 3);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.floor,
			i = Math.log,
			s = Math.LOG2E;
		n(
			{ target: "Math", stat: !0 },
			{
				clz32: function (t) {
					return (t >>>= 0) ? 31 - o(i(t + 0.5) * s) : 32;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(81),
			i = Math.cosh,
			s = Math.abs,
			a = Math.E;
		n(
			{ target: "Math", stat: !0, forced: !i || i(710) === 1 / 0 },
			{
				cosh: function (t) {
					var e = o(s(t) - 1) + 1;
					return (e + 1 / (e * a * a)) * (a / 2);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(81);
		n({ target: "Math", stat: !0, forced: o != Math.expm1 }, { expm1: o });
	},
	function (t, e, r) {
		r(0)({ target: "Math", stat: !0 }, { fround: r(280) });
	},
	function (t, e, r) {
		var n = r(103),
			o = Math.pow,
			i = o(2, -52),
			s = o(2, -23),
			a = o(2, 127) * (2 - s),
			c = o(2, -126);
		t.exports =
			Math.fround ||
			function (t) {
				var e,
					r,
					o = Math.abs(t),
					l = n(t);
				return o < c
					? l * (o / c / s + 1 / i - 1 / i) * c * s
					: (r = (e = (1 + s / i) * o) - (e - o)) > a || r != r
					? l * (1 / 0)
					: l * r;
			};
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.abs,
			i = Math.sqrt;
		n(
			{ target: "Math", stat: !0 },
			{
				hypot: function (t, e) {
					for (var r, n, s = 0, a = 0, c = arguments.length, l = 0; a < c; )
						l < (r = o(arguments[a++]))
							? ((s = s * (n = l / r) * n + 1), (l = r))
							: (s += r > 0 ? (n = r / l) * n : r);
					return l === 1 / 0 ? 1 / 0 : l * i(s);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = Math.imul;
		n(
			{
				target: "Math",
				stat: !0,
				forced: o(function () {
					return -5 != i(4294967295, 5) || 2 != i.length;
				}),
			},
			{
				imul: function (t, e) {
					var r = +t,
						n = +e,
						o = 65535 & r,
						i = 65535 & n;
					return (
						0 |
						(o * i +
							((((65535 & (r >>> 16)) * i + o * (65535 & (n >>> 16))) << 16) >>>
								0))
					);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.log,
			i = Math.LOG10E;
		n(
			{ target: "Math", stat: !0 },
			{
				log10: function (t) {
					return o(t) * i;
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "Math", stat: !0 }, { log1p: r(133) });
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.log,
			i = Math.LN2;
		n(
			{ target: "Math", stat: !0 },
			{
				log2: function (t) {
					return o(t) / i;
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "Math", stat: !0 }, { sign: r(103) });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(81),
			s = Math.abs,
			a = Math.exp,
			c = Math.E;
		n(
			{
				target: "Math",
				stat: !0,
				forced: o(function () {
					return -2e-17 != Math.sinh(-2e-17);
				}),
			},
			{
				sinh: function (t) {
					return s((t = +t)) < 1
						? (i(t) - i(-t)) / 2
						: (a(t - 1) - a(-t - 1)) * (c / 2);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(81),
			i = Math.exp;
		n(
			{ target: "Math", stat: !0 },
			{
				tanh: function (t) {
					var e = o((t = +t)),
						r = o(-t);
					return e == 1 / 0 ? 1 : r == 1 / 0 ? -1 : (e - r) / (i(t) + i(-t));
				},
			}
		);
	},
	function (t, e, r) {
		r(29)(Math, "Math", !0);
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.ceil,
			i = Math.floor;
		n(
			{ target: "Math", stat: !0 },
			{
				trunc: function (t) {
					return (t > 0 ? i : o)(t);
				},
			}
		);
	},
	function (t, e, r) {
		r(0)(
			{ target: "Date", stat: !0 },
			{
				now: function () {
					return new Date().getTime();
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(1),
			i = r(11),
			s = r(27);
		n(
			{
				target: "Date",
				proto: !0,
				forced: o(function () {
					return (
						null !== new Date(NaN).toJSON() ||
						1 !==
							Date.prototype.toJSON.call({
								toISOString: function () {
									return 1;
								},
							})
					);
				}),
			},
			{
				toJSON: function (t) {
					var e = i(this),
						r = s(e);
					return "number" != typeof r || isFinite(r) ? e.toISOString() : null;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(294);
		n(
			{ target: "Date", proto: !0, forced: Date.prototype.toISOString !== o },
			{ toISOString: o }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(1),
			o = Date.prototype,
			i = o.getTime,
			s = o.toISOString,
			a = function (t) {
				return t > 9 ? t : "0" + t;
			};
		t.exports =
			n(function () {
				return "0385-07-25T07:06:39.999Z" != s.call(new Date(-5e13 - 1));
			}) ||
			!n(function () {
				s.call(new Date(NaN));
			})
				? function () {
						if (!isFinite(i.call(this))) throw RangeError("Invalid time value");
						var t = this.getUTCFullYear(),
							e = this.getUTCMilliseconds(),
							r = t < 0 ? "-" : t > 9999 ? "+" : "";
						return (
							r +
							("00000" + Math.abs(t)).slice(r ? -6 : -4) +
							"-" +
							a(this.getUTCMonth() + 1) +
							"-" +
							a(this.getUTCDate()) +
							"T" +
							a(this.getUTCHours()) +
							":" +
							a(this.getUTCMinutes()) +
							":" +
							a(this.getUTCSeconds()) +
							"." +
							(e > 99 ? e : "0" + a(e)) +
							"Z"
						);
				  }
				: s;
	},
	function (t, e, r) {
		var n = r(16),
			o = Date.prototype,
			i = o.toString,
			s = o.getTime;
		new Date(NaN) + "" != "Invalid Date" &&
			n(o, "toString", function () {
				var t = s.call(this);
				return t == t ? i.call(this) : "Invalid Date";
			});
	},
	function (t, e, r) {
		var n = r(14),
			o = r(297),
			i = r(7)("toPrimitive"),
			s = Date.prototype;
		i in s || n(s, i, o);
	},
	function (t, e, r) {
		"use strict";
		var n = r(4),
			o = r(27);
		t.exports = function (t) {
			if ("string" !== t && "number" !== t && "default" !== t)
				throw TypeError("Incorrect hint");
			return o(n(this), "number" !== t);
		};
	},
	function (t, e, r) {
		var n = r(2);
		r(29)(n.JSON, "JSON", !0);
	},
	function (t, e, r) {
		var n,
			o,
			i,
			s,
			a,
			c,
			l,
			u = r(2),
			f = r(15).f,
			h = r(28),
			d = r(134).set,
			p = r(100),
			g = u.MutationObserver || u.WebKitMutationObserver,
			m = u.process,
			v = u.Promise,
			y = "process" == h(m),
			b = f(u, "queueMicrotask"),
			S = b && b.value;
		S ||
			((n = function () {
				var t, e;
				for (y && (t = m.domain) && t.exit(); o; ) {
					(e = o.fn), (o = o.next);
					try {
						e();
					} catch (t) {
						throw (o ? s() : (i = void 0), t);
					}
				}
				(i = void 0), t && t.enter();
			}),
			y
				? (s = function () {
						m.nextTick(n);
				  })
				: g && !/(iphone|ipod|ipad).*applewebkit/i.test(p)
				? ((a = !0),
				  (c = document.createTextNode("")),
				  new g(n).observe(c, { characterData: !0 }),
				  (s = function () {
						c.data = a = !a;
				  }))
				: v && v.resolve
				? ((l = v.resolve(void 0)),
				  (s = function () {
						l.then(n);
				  }))
				: (s = function () {
						d.call(u, n);
				  })),
			(t.exports =
				S ||
				function (t) {
					var e = { fn: t, next: void 0 };
					i && (i.next = e), o || ((o = e), s()), (i = e);
				});
	},
	function (t, e, r) {
		var n = r(2);
		t.exports = function (t, e) {
			var r = n.console;
			r && r.error && (1 === arguments.length ? r.error(t) : r.error(t, e));
		};
	},
	function (t, e) {
		t.exports = function (t) {
			try {
				return { error: !1, value: t() };
			} catch (t) {
				return { error: !0, value: t };
			}
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(93),
			i = r(32),
			s = r(135);
		n(
			{ target: "Promise", proto: !0, real: !0 },
			{
				finally: function (t) {
					var e = i(this, o("Promise")),
						r = "function" == typeof t;
					return this.then(
						r
							? function (r) {
									return s(e, t()).then(function () {
										return r;
									});
							  }
							: t,
						r
							? function (r) {
									return s(e, t()).then(function () {
										throw r;
									});
							  }
							: t
					);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(82),
			o = r(137);
		t.exports = n(
			"Map",
			function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			o,
			!0
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(82),
			o = r(137);
		t.exports = n(
			"Set",
			function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			o
		);
	},
	function (t, e, r) {
		"use strict";
		var n,
			o = r(2),
			i = r(50),
			s = r(44),
			a = r(82),
			c = r(138),
			l = r(3),
			u = r(20).enforce,
			f = r(108),
			h = !o.ActiveXObject && "ActiveXObject" in o,
			d = Object.isExtensible,
			p = function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			g = (t.exports = a("WeakMap", p, c, !0, !0));
		if (f && h) {
			(n = c.getConstructor(p, "WeakMap", !0)), (s.REQUIRED = !0);
			var m = g.prototype,
				v = m.delete,
				y = m.has,
				b = m.get,
				S = m.set;
			i(m, {
				delete: function (t) {
					if (l(t) && !d(t)) {
						var e = u(this);
						return (
							e.frozen || (e.frozen = new n()),
							v.call(this, t) || e.frozen.delete(t)
						);
					}
					return v.call(this, t);
				},
				has: function (t) {
					if (l(t) && !d(t)) {
						var e = u(this);
						return (
							e.frozen || (e.frozen = new n()),
							y.call(this, t) || e.frozen.has(t)
						);
					}
					return y.call(this, t);
				},
				get: function (t) {
					if (l(t) && !d(t)) {
						var e = u(this);
						return (
							e.frozen || (e.frozen = new n()),
							y.call(this, t) ? b.call(this, t) : e.frozen.get(t)
						);
					}
					return b.call(this, t);
				},
				set: function (t, e) {
					if (l(t) && !d(t)) {
						var r = u(this);
						r.frozen || (r.frozen = new n()),
							y.call(this, t) ? S.call(this, t, e) : r.frozen.set(t, e);
					} else S.call(this, t, e);
					return this;
				},
			});
		}
	},
	function (t, e, r) {
		"use strict";
		r(82)(
			"WeakSet",
			function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			r(138),
			!1,
			!0
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(2),
			i = r(83),
			s = r(48),
			a = i.ArrayBuffer;
		n({ global: !0, forced: o.ArrayBuffer !== a }, { ArrayBuffer: a }),
			s("ArrayBuffer");
	},
	function (t, e, r) {
		var n = r(0),
			o = r(5);
		n(
			{ target: "ArrayBuffer", stat: !0, forced: !o.NATIVE_ARRAY_BUFFER_VIEWS },
			{ isView: o.isView }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(1),
			i = r(83),
			s = r(4),
			a = r(36),
			c = r(8),
			l = r(32),
			u = i.ArrayBuffer,
			f = i.DataView,
			h = u.prototype.slice;
		n(
			{
				target: "ArrayBuffer",
				proto: !0,
				unsafe: !0,
				forced: o(function () {
					return !new u(2).slice(1, void 0).byteLength;
				}),
			},
			{
				slice: function (t, e) {
					if (void 0 !== h && void 0 === e) return h.call(s(this), t);
					for (
						var r = s(this).byteLength,
							n = a(t, r),
							o = a(void 0 === e ? r : e, r),
							i = new (l(this, u))(c(o - n)),
							d = new f(this),
							p = new f(i),
							g = 0;
						n < o;

					)
						p.setUint8(g++, d.getUint8(n++));
					return i;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(83);
		n(
			{ global: !0, forced: !r(5).NATIVE_ARRAY_BUFFER },
			{ DataView: o.DataView }
		);
	},
	function (t, e, r) {
		r(33)("Int8", 1, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(33)("Uint8", 1, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(33)(
			"Uint8",
			1,
			function (t) {
				return function (e, r, n) {
					return t(this, e, r, n);
				};
			},
			!0
		);
	},
	function (t, e, r) {
		r(33)("Int16", 2, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(33)("Uint16", 2, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(33)("Int32", 4, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(33)("Uint32", 4, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(33)("Float32", 4, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(33)("Float64", 8, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(104),
			o = r(5),
			i = r(141);
		o.exportStatic("from", i, n);
	},
	function (t, e, r) {
		"use strict";
		var n = r(104),
			o = r(5),
			i = o.aTypedArrayConstructor;
		o.exportStatic(
			"of",
			function () {
				for (var t = 0, e = arguments.length, r = new (i(this))(e); e > t; )
					r[t] = arguments[t++];
				return r;
			},
			n
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(122),
			o = r(5),
			i = o.aTypedArray;
		o.exportProto("copyWithin", function (t, e) {
			return n.call(
				i(this),
				t,
				e,
				arguments.length > 2 ? arguments[2] : void 0
			);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(13),
			o = r(5),
			i = n(4),
			s = o.aTypedArray;
		o.exportProto("every", function (t) {
			return i(s(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(5),
			o = r(92),
			i = n.aTypedArray;
		n.exportProto("fill", function (t) {
			return o.apply(i(this), arguments);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(32),
			o = r(5),
			i = r(13)(2),
			s = o.aTypedArray,
			a = o.aTypedArrayConstructor;
		o.exportProto("filter", function (t) {
			for (
				var e = i(s(this), t, arguments.length > 1 ? arguments[1] : void 0),
					r = n(this, this.constructor),
					o = 0,
					c = e.length,
					l = new (a(r))(c);
				c > o;

			)
				l[o] = e[o++];
			return l;
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(5),
			o = r(13)(5),
			i = n.aTypedArray;
		n.exportProto("find", function (t) {
			return o(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(5),
			o = r(13)(6),
			i = n.aTypedArray;
		n.exportProto("findIndex", function (t) {
			return o(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(5),
			o = r(13)(0),
			i = n.aTypedArray;
		n.exportProto("forEach", function (t) {
			o(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(56),
			o = r(5),
			i = o.aTypedArray,
			s = n(!0);
		o.exportProto("includes", function (t) {
			return s(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(56),
			o = r(5),
			i = o.aTypedArray,
			s = n(!1);
		o.exportProto("indexOf", function (t) {
			return s(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(2),
			o = r(22),
			i = r(5),
			s = r(7)("iterator"),
			a = n.Uint8Array,
			c = o.values,
			l = o.keys,
			u = o.entries,
			f = i.aTypedArray,
			h = i.exportProto,
			d = a && a.prototype[s],
			p = !!d && ("values" == d.name || null == d.name),
			g = function () {
				return c.call(f(this));
			};
		h("entries", function () {
			return u.call(f(this));
		}),
			h("keys", function () {
				return l.call(f(this));
			}),
			h("values", g, !p),
			h(s, g, !p);
	},
	function (t, e, r) {
		"use strict";
		var n = r(5),
			o = n.aTypedArray,
			i = [].join;
		n.exportProto("join", function (t) {
			return i.apply(o(this), arguments);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(124),
			o = r(5),
			i = o.aTypedArray;
		o.exportProto("lastIndexOf", function (t) {
			return n.apply(i(this), arguments);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(32),
			o = r(5),
			i = r(13),
			s = o.aTypedArray,
			a = o.aTypedArrayConstructor,
			c = i(1, function (t, e) {
				return new (a(n(t, t.constructor)))(e);
			});
		o.exportProto("map", function (t) {
			return c(s(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(5),
			o = r(74),
			i = n.aTypedArray;
		n.exportProto("reduce", function (t) {
			return o(i(this), t, arguments.length, arguments[1], !1);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(5),
			o = r(74),
			i = n.aTypedArray;
		n.exportProto("reduceRight", function (t) {
			return o(i(this), t, arguments.length, arguments[1], !0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(5),
			o = n.aTypedArray;
		n.exportProto("reverse", function () {
			for (var t, e = o(this).length, r = Math.floor(e / 2), n = 0; n < r; )
				(t = this[n]), (this[n++] = this[--e]), (this[e] = t);
			return this;
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(8),
			o = r(140),
			i = r(11),
			s = r(5),
			a = r(1),
			c = s.aTypedArray,
			l = a(function () {
				new Int8Array(1).set({});
			});
		s.exportProto(
			"set",
			function (t) {
				c(this);
				var e = o(arguments[1], 1),
					r = this.length,
					s = i(t),
					a = n(s.length),
					l = 0;
				if (a + e > r) throw RangeError("Wrong length");
				for (; l < a; ) this[e + l] = s[l++];
			},
			l
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(32),
			o = r(5),
			i = r(1),
			s = o.aTypedArray,
			a = o.aTypedArrayConstructor,
			c = [].slice,
			l = i(function () {
				new Int8Array(1).slice();
			});
		o.exportProto(
			"slice",
			function (t, e) {
				for (
					var r = c.call(s(this), t, e),
						o = n(this, this.constructor),
						i = 0,
						l = r.length,
						u = new (a(o))(l);
					l > i;

				)
					u[i] = r[i++];
				return u;
			},
			l
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(5),
			o = r(13)(3),
			i = n.aTypedArray;
		n.exportProto("some", function (t) {
			return o(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(5),
			o = n.aTypedArray,
			i = [].sort;
		n.exportProto("sort", function (t) {
			return i.call(o(this), t);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(8),
			o = r(36),
			i = r(32),
			s = r(5),
			a = s.aTypedArray;
		s.exportProto("subarray", function (t, e) {
			var r = a(this),
				s = r.length,
				c = o(t, s);
			return new (i(r, r.constructor))(
				r.buffer,
				r.byteOffset + c * r.BYTES_PER_ELEMENT,
				n((void 0 === e ? s : o(e, s)) - c)
			);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(2),
			o = r(1),
			i = r(5),
			s = n.Int8Array,
			a = i.aTypedArray,
			c = [].toLocaleString,
			l = [].slice,
			u =
				!!s &&
				o(function () {
					c.call(new s(1));
				}),
			f =
				o(function () {
					return [1, 2].toLocaleString() != new s([1, 2]).toLocaleString();
				}) ||
				!o(function () {
					s.prototype.toLocaleString.call([1, 2]);
				});
		i.exportProto(
			"toLocaleString",
			function () {
				return c.apply(u ? l.call(a(this)) : a(this), arguments);
			},
			f
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(2),
			o = r(5),
			i = r(1),
			s = n.Uint8Array,
			a = s && s.prototype,
			c = [].toString,
			l = [].join;
		i(function () {
			c.call({});
		}) &&
			(c = function () {
				return l.call(this);
			}),
			o.exportProto("toString", c, (a || {}).toString != c);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(2),
			i = r(21),
			s = r(4),
			a = r(1),
			c = (o.Reflect || {}).apply,
			l = Function.apply;
		n(
			{
				target: "Reflect",
				stat: !0,
				forced: !a(function () {
					c(function () {});
				}),
			},
			{
				apply: function (t, e, r) {
					return i(t), s(r), c ? c(t, e, r) : l.call(t, e, r);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(2),
			i = r(37),
			s = r(21),
			a = r(4),
			c = r(3),
			l = r(1),
			u = r(120),
			f = (o.Reflect || {}).construct,
			h = l(function () {
				function t() {}
				return !(f(function () {}, [], t) instanceof t);
			}),
			d = !l(function () {
				f(function () {});
			}),
			p = h || d;
		n(
			{ target: "Reflect", stat: !0, forced: p, sham: p },
			{
				construct: function (t, e) {
					s(t), a(e);
					var r = arguments.length < 3 ? t : s(arguments[2]);
					if (d && !h) return f(t, e, r);
					if (t == r) {
						switch (e.length) {
							case 0:
								return new t();
							case 1:
								return new t(e[0]);
							case 2:
								return new t(e[0], e[1]);
							case 3:
								return new t(e[0], e[1], e[2]);
							case 4:
								return new t(e[0], e[1], e[2], e[3]);
						}
						var n = [null];
						return n.push.apply(n, e), new (u.apply(t, n))();
					}
					var o = r.prototype,
						l = i(c(o) ? o : Object.prototype),
						p = Function.apply.call(t, l, e);
					return c(p) ? p : l;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(9),
			s = r(4),
			a = r(27),
			c = r(6);
		n(
			{
				target: "Reflect",
				stat: !0,
				forced: o(function () {
					Reflect.defineProperty(i.f({}, 1, { value: 1 }), 1, { value: 2 });
				}),
				sham: !c,
			},
			{
				defineProperty: function (t, e, r) {
					s(t), (e = a(e, !0)), s(r);
					try {
						return i.f(t, e, r), !0;
					} catch (t) {
						return !1;
					}
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(15).f,
			i = r(4);
		n(
			{ target: "Reflect", stat: !0 },
			{
				deleteProperty: function (t, e) {
					var r = o(i(t), e);
					return !(r && !r.configurable) && delete t[e];
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(15),
			i = r(30),
			s = r(12),
			a = r(3),
			c = r(4);
		n(
			{ target: "Reflect", stat: !0 },
			{
				get: function t(e, r) {
					var n,
						l,
						u = arguments.length < 3 ? e : arguments[2];
					return c(e) === u
						? e[r]
						: (n = o.f(e, r))
						? s(n, "value")
							? n.value
							: void 0 === n.get
							? void 0
							: n.get.call(u)
						: a((l = i(e)))
						? t(l, r, u)
						: void 0;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(15),
			i = r(4);
		n(
			{ target: "Reflect", stat: !0, sham: !r(6) },
			{
				getOwnPropertyDescriptor: function (t, e) {
					return o.f(i(t), e);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(30),
			i = r(4);
		n(
			{ target: "Reflect", stat: !0, sham: !r(91) },
			{
				getPrototypeOf: function (t) {
					return o(i(t));
				},
			}
		);
	},
	function (t, e, r) {
		r(0)(
			{ target: "Reflect", stat: !0 },
			{
				has: function (t, e) {
					return e in t;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(4),
			i = Object.isExtensible;
		n(
			{ target: "Reflect", stat: !0 },
			{
				isExtensible: function (t) {
					return o(t), !i || i(t);
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "Reflect", stat: !0 }, { ownKeys: r(87) });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(93),
			i = r(4);
		n(
			{ target: "Reflect", stat: !0, sham: !r(58) },
			{
				preventExtensions: function (t) {
					i(t);
					try {
						var e = o("Object", "preventExtensions");
						return e && e(t), !0;
					} catch (t) {
						return !1;
					}
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(9),
			i = r(15),
			s = r(30),
			a = r(12),
			c = r(41),
			l = r(4),
			u = r(3);
		n(
			{ target: "Reflect", stat: !0 },
			{
				set: function t(e, r, n) {
					var f,
						h,
						d = arguments.length < 4 ? e : arguments[3],
						p = i.f(l(e), r);
					if (!p) {
						if (u((h = s(e)))) return t(h, r, n, d);
						p = c(0);
					}
					if (a(p, "value")) {
						if (!1 === p.writable || !u(d)) return !1;
						if ((f = i.f(d, r))) {
							if (f.get || f.set || !1 === f.writable) return !1;
							(f.value = n), o.f(d, r, f);
						} else o.f(d, r, c(0, n));
						return !0;
					}
					return void 0 !== p.set && (p.set.call(d, n), !0);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(47),
			i = r(119);
		o &&
			n(
				{ target: "Reflect", stat: !0 },
				{
					setPrototypeOf: function (t, e) {
						i(t, e);
						try {
							return o(t, e), !0;
						} catch (t) {
							return !1;
						}
					},
				}
			);
	},
	function (t, e, r) {
		var n = (function (t) {
			"use strict";
			var e,
				r = Object.prototype,
				n = r.hasOwnProperty,
				o = "function" == typeof Symbol ? Symbol : {},
				i = o.iterator || "@@iterator",
				s = o.asyncIterator || "@@asyncIterator",
				a = o.toStringTag || "@@toStringTag";
			function c(t, e, r, n) {
				var o = e && e.prototype instanceof g ? e : g,
					i = Object.create(o.prototype),
					s = new R(n || []);
				return (
					(i._invoke = (function (t, e, r) {
						var n = u;
						return function (o, i) {
							if (n === h) throw new Error("Generator is already running");
							if (n === d) {
								if ("throw" === o) throw i;
								return C();
							}
							for (r.method = o, r.arg = i; ; ) {
								var s = r.delegate;
								if (s) {
									var a = A(s, r);
									if (a) {
										if (a === p) continue;
										return a;
									}
								}
								if ("next" === r.method) r.sent = r._sent = r.arg;
								else if ("throw" === r.method) {
									if (n === u) throw ((n = d), r.arg);
									r.dispatchException(r.arg);
								} else "return" === r.method && r.abrupt("return", r.arg);
								n = h;
								var c = l(t, e, r);
								if ("normal" === c.type) {
									if (((n = r.done ? d : f), c.arg === p)) continue;
									return { value: c.arg, done: r.done };
								}
								"throw" === c.type &&
									((n = d), (r.method = "throw"), (r.arg = c.arg));
							}
						};
					})(t, r, s)),
					i
				);
			}
			function l(t, e, r) {
				try {
					return { type: "normal", arg: t.call(e, r) };
				} catch (t) {
					return { type: "throw", arg: t };
				}
			}
			t.wrap = c;
			var u = "suspendedStart",
				f = "suspendedYield",
				h = "executing",
				d = "completed",
				p = {};
			function g() {}
			function m() {}
			function v() {}
			var y = {};
			y[i] = function () {
				return this;
			};
			var b = Object.getPrototypeOf,
				S = b && b(b(I([])));
			S && S !== r && n.call(S, i) && (y = S);
			var _ = (v.prototype = g.prototype = Object.create(y));
			function E(t) {
				["next", "throw", "return"].forEach(function (e) {
					t[e] = function (t) {
						return this._invoke(e, t);
					};
				});
			}
			function T(t) {
				var e;
				this._invoke = function (r, o) {
					function i() {
						return new Promise(function (e, i) {
							!(function e(r, o, i, s) {
								var a = l(t[r], t, o);
								if ("throw" !== a.type) {
									var c = a.arg,
										u = c.value;
									return u && "object" == typeof u && n.call(u, "__await")
										? Promise.resolve(u.__await).then(
												function (t) {
													e("next", t, i, s);
												},
												function (t) {
													e("throw", t, i, s);
												}
										  )
										: Promise.resolve(u).then(
												function (t) {
													(c.value = t), i(c);
												},
												function (t) {
													return e("throw", t, i, s);
												}
										  );
								}
								s(a.arg);
							})(r, o, e, i);
						});
					}
					return (e = e ? e.then(i, i) : i());
				};
			}
			function A(t, r) {
				var n = t.iterator[r.method];
				if (n === e) {
					if (((r.delegate = null), "throw" === r.method)) {
						if (
							t.iterator.return &&
							((r.method = "return"),
							(r.arg = e),
							A(t, r),
							"throw" === r.method)
						)
							return p;
						(r.method = "throw"),
							(r.arg = new TypeError(
								"The iterator does not provide a 'throw' method"
							));
					}
					return p;
				}
				var o = l(n, t.iterator, r.arg);
				if ("throw" === o.type)
					return (r.method = "throw"), (r.arg = o.arg), (r.delegate = null), p;
				var i = o.arg;
				return i
					? i.done
						? ((r[t.resultName] = i.value),
						  (r.next = t.nextLoc),
						  "return" !== r.method && ((r.method = "next"), (r.arg = e)),
						  (r.delegate = null),
						  p)
						: i
					: ((r.method = "throw"),
					  (r.arg = new TypeError("iterator result is not an object")),
					  (r.delegate = null),
					  p);
			}
			function w(t) {
				var e = { tryLoc: t[0] };
				1 in t && (e.catchLoc = t[1]),
					2 in t && ((e.finallyLoc = t[2]), (e.afterLoc = t[3])),
					this.tryEntries.push(e);
			}
			function x(t) {
				var e = t.completion || {};
				(e.type = "normal"), delete e.arg, (t.completion = e);
			}
			function R(t) {
				(this.tryEntries = [{ tryLoc: "root" }]),
					t.forEach(w, this),
					this.reset(!0);
			}
			function I(t) {
				if (t) {
					var r = t[i];
					if (r) return r.call(t);
					if ("function" == typeof t.next) return t;
					if (!isNaN(t.length)) {
						var o = -1,
							s = function r() {
								for (; ++o < t.length; )
									if (n.call(t, o)) return (r.value = t[o]), (r.done = !1), r;
								return (r.value = e), (r.done = !0), r;
							};
						return (s.next = s);
					}
				}
				return { next: C };
			}
			function C() {
				return { value: e, done: !0 };
			}
			return (
				(m.prototype = _.constructor = v),
				(v.constructor = m),
				(v[a] = m.displayName = "GeneratorFunction"),
				(t.isGeneratorFunction = function (t) {
					var e = "function" == typeof t && t.constructor;
					return (
						!!e &&
						(e === m || "GeneratorFunction" === (e.displayName || e.name))
					);
				}),
				(t.mark = function (t) {
					return (
						Object.setPrototypeOf
							? Object.setPrototypeOf(t, v)
							: ((t.__proto__ = v), a in t || (t[a] = "GeneratorFunction")),
						(t.prototype = Object.create(_)),
						t
					);
				}),
				(t.awrap = function (t) {
					return { __await: t };
				}),
				E(T.prototype),
				(T.prototype[s] = function () {
					return this;
				}),
				(t.AsyncIterator = T),
				(t.async = function (e, r, n, o) {
					var i = new T(c(e, r, n, o));
					return t.isGeneratorFunction(r)
						? i
						: i.next().then(function (t) {
								return t.done ? t.value : i.next();
						  });
				}),
				E(_),
				(_[a] = "Generator"),
				(_[i] = function () {
					return this;
				}),
				(_.toString = function () {
					return "[object Generator]";
				}),
				(t.keys = function (t) {
					var e = [];
					for (var r in t) e.push(r);
					return (
						e.reverse(),
						function r() {
							for (; e.length; ) {
								var n = e.pop();
								if (n in t) return (r.value = n), (r.done = !1), r;
							}
							return (r.done = !0), r;
						}
					);
				}),
				(t.values = I),
				(R.prototype = {
					constructor: R,
					reset: function (t) {
						if (
							((this.prev = 0),
							(this.next = 0),
							(this.sent = this._sent = e),
							(this.done = !1),
							(this.delegate = null),
							(this.method = "next"),
							(this.arg = e),
							this.tryEntries.forEach(x),
							!t)
						)
							for (var r in this)
								"t" === r.charAt(0) &&
									n.call(this, r) &&
									!isNaN(+r.slice(1)) &&
									(this[r] = e);
					},
					stop: function () {
						this.done = !0;
						var t = this.tryEntries[0].completion;
						if ("throw" === t.type) throw t.arg;
						return this.rval;
					},
					dispatchException: function (t) {
						if (this.done) throw t;
						var r = this;
						function o(n, o) {
							return (
								(a.type = "throw"),
								(a.arg = t),
								(r.next = n),
								o && ((r.method = "next"), (r.arg = e)),
								!!o
							);
						}
						for (var i = this.tryEntries.length - 1; i >= 0; --i) {
							var s = this.tryEntries[i],
								a = s.completion;
							if ("root" === s.tryLoc) return o("end");
							if (s.tryLoc <= this.prev) {
								var c = n.call(s, "catchLoc"),
									l = n.call(s, "finallyLoc");
								if (c && l) {
									if (this.prev < s.catchLoc) return o(s.catchLoc, !0);
									if (this.prev < s.finallyLoc) return o(s.finallyLoc);
								} else if (c) {
									if (this.prev < s.catchLoc) return o(s.catchLoc, !0);
								} else {
									if (!l)
										throw new Error("try statement without catch or finally");
									if (this.prev < s.finallyLoc) return o(s.finallyLoc);
								}
							}
						}
					},
					abrupt: function (t, e) {
						for (var r = this.tryEntries.length - 1; r >= 0; --r) {
							var o = this.tryEntries[r];
							if (
								o.tryLoc <= this.prev &&
								n.call(o, "finallyLoc") &&
								this.prev < o.finallyLoc
							) {
								var i = o;
								break;
							}
						}
						i &&
							("break" === t || "continue" === t) &&
							i.tryLoc <= e &&
							e <= i.finallyLoc &&
							(i = null);
						var s = i ? i.completion : {};
						return (
							(s.type = t),
							(s.arg = e),
							i
								? ((this.method = "next"), (this.next = i.finallyLoc), p)
								: this.complete(s)
						);
					},
					complete: function (t, e) {
						if ("throw" === t.type) throw t.arg;
						return (
							"break" === t.type || "continue" === t.type
								? (this.next = t.arg)
								: "return" === t.type
								? ((this.rval = this.arg = t.arg),
								  (this.method = "return"),
								  (this.next = "end"))
								: "normal" === t.type && e && (this.next = e),
							p
						);
					},
					finish: function (t) {
						for (var e = this.tryEntries.length - 1; e >= 0; --e) {
							var r = this.tryEntries[e];
							if (r.finallyLoc === t)
								return this.complete(r.completion, r.afterLoc), x(r), p;
						}
					},
					catch: function (t) {
						for (var e = this.tryEntries.length - 1; e >= 0; --e) {
							var r = this.tryEntries[e];
							if (r.tryLoc === t) {
								var n = r.completion;
								if ("throw" === n.type) {
									var o = n.arg;
									x(r);
								}
								return o;
							}
						}
						throw new Error("illegal catch attempt");
					},
					delegateYield: function (t, r, n) {
						return (
							(this.delegate = { iterator: I(t), resultName: r, nextLoc: n }),
							"next" === this.method && (this.arg = e),
							p
						);
					},
				}),
				t
			);
		})(t.exports);
		try {
			regeneratorRuntime = n;
		} catch (t) {
			Function("r", "regeneratorRuntime = r")(n);
		}
	},
	function (t, e) {
		t.exports = {
			CSSRuleList: 0,
			CSSStyleDeclaration: 0,
			CSSValueList: 0,
			ClientRectList: 0,
			DOMRectList: 0,
			DOMStringList: 0,
			DOMTokenList: 1,
			DataTransferItemList: 0,
			FileList: 0,
			HTMLAllCollection: 0,
			HTMLCollection: 0,
			HTMLFormElement: 0,
			HTMLSelectElement: 0,
			MediaList: 0,
			MimeTypeArray: 0,
			NamedNodeMap: 0,
			NodeList: 1,
			PaintRequestList: 0,
			Plugin: 0,
			PluginArray: 0,
			SVGLengthList: 0,
			SVGNumberList: 0,
			SVGPathSegList: 0,
			SVGPointList: 0,
			SVGStringList: 0,
			SVGTransformList: 0,
			SourceBufferList: 0,
			StyleSheetList: 0,
			TextTrackCueList: 0,
			TextTrackList: 0,
			TouchList: 0,
		};
	},
	function (t, e, r) {
		"use strict";
		var n = /[^\0-\u007E]/,
			o = /[.\u3002\uFF0E\uFF61]/g,
			i = "Overflow: input needs wider integers to process",
			s = Math.floor,
			a = String.fromCharCode,
			c = function (t) {
				return t + 22 + 75 * (t < 26);
			},
			l = function (t, e, r) {
				var n = 0;
				for (t = r ? s(t / 700) : t >> 1, t += s(t / e); t > 455; n += 36)
					t = s(t / 35);
				return s(n + (36 * t) / (t + 38));
			},
			u = function (t) {
				var e,
					r,
					n = [],
					o = (t = (function (t) {
						for (var e = [], r = 0, n = t.length; r < n; ) {
							var o = t.charCodeAt(r++);
							if (o >= 55296 && o <= 56319 && r < n) {
								var i = t.charCodeAt(r++);
								56320 == (64512 & i)
									? e.push(((1023 & o) << 10) + (1023 & i) + 65536)
									: (e.push(o), r--);
							} else e.push(o);
						}
						return e;
					})(t)).length,
					u = 128,
					f = 0,
					h = 72;
				for (e = 0; e < t.length; e++) (r = t[e]) < 128 && n.push(a(r));
				var d = n.length,
					p = d;
				for (d && n.push("-"); p < o; ) {
					var g = 2147483647;
					for (e = 0; e < t.length; e++) (r = t[e]) >= u && r < g && (g = r);
					var m = p + 1;
					if (g - u > s((2147483647 - f) / m)) throw RangeError(i);
					for (f += (g - u) * m, u = g, e = 0; e < t.length; e++) {
						if ((r = t[e]) < u && ++f > 2147483647) throw RangeError(i);
						if (r == u) {
							for (var v = f, y = 36; ; y += 36) {
								var b = y <= h ? 1 : y >= h + 26 ? 26 : y - h;
								if (v < b) break;
								var S = v - b,
									_ = 36 - b;
								n.push(a(c(b + (S % _)))), (v = s(S / _));
							}
							n.push(a(c(v))), (h = l(f, m, p == d)), (f = 0), ++p;
						}
					}
					++f, ++u;
				}
				return n.join("");
			};
		t.exports = function (t) {
			var e,
				r,
				i = [],
				s = t.toLowerCase().replace(o, ".").split(".");
			for (e = 0; e < s.length; e++)
				(r = s[e]), i.push(n.test(r) ? "xn--" + u(r) : r);
			return i.join(".");
		};
	},
	function (t, e, r) {
		"use strict";
		r(22);
		var n = r(0),
			o = r(142),
			i = r(16),
			s = r(50),
			a = r(29),
			c = r(95),
			l = r(20),
			u = r(40),
			f = r(12),
			h = r(38),
			d = r(4),
			p = r(3),
			g = r(362),
			m = r(61),
			v = r(7)("iterator"),
			y = l.set,
			b = l.getterFor("URLSearchParams"),
			S = l.getterFor("URLSearchParamsIterator"),
			_ = /\+/g,
			E = Array(4),
			T = function (t) {
				return (
					E[t - 1] || (E[t - 1] = RegExp("((?:%[\\da-f]{2}){" + t + "})", "gi"))
				);
			},
			A = function (t) {
				try {
					return decodeURIComponent(t);
				} catch (e) {
					return t;
				}
			},
			w = function (t) {
				var e = t.replace(_, " "),
					r = 4;
				try {
					return decodeURIComponent(e);
				} catch (t) {
					for (; r; ) e = e.replace(T(r--), A);
					return e;
				}
			},
			x = /[!'()~]|%20/g,
			R = {
				"!": "%21",
				"'": "%27",
				"(": "%28",
				")": "%29",
				"~": "%7E",
				"%20": "+",
			},
			I = function (t) {
				return R[t];
			},
			C = function (t) {
				return encodeURIComponent(t).replace(x, I);
			},
			O = function (t, e) {
				if (e)
					for (var r, n, o = e.split("&"), i = 0; i < o.length; )
						(r = o[i++]).length &&
							((n = r.split("=")),
							t.push({ key: w(n.shift()), value: w(n.join("=")) }));
				return t;
			},
			M = function (t) {
				(this.entries.length = 0), O(this.entries, t);
			},
			L = function (t, e) {
				if (t < e) throw TypeError("Not enough arguments");
			},
			N = c(
				function (t, e) {
					y(this, {
						type: "URLSearchParamsIterator",
						iterator: g(b(t).entries),
						kind: e,
					});
				},
				"Iterator",
				function () {
					var t = S(this),
						e = t.kind,
						r = t.iterator.next(),
						n = r.value;
					return (
						r.done ||
							(r.value =
								"keys" === e
									? n.key
									: "values" === e
									? n.value
									: [n.key, n.value]),
						r
					);
				}
			),
			k = function () {
				u(this, k, "URLSearchParams");
				var t,
					e,
					r,
					n,
					o,
					i,
					s,
					a = arguments.length > 0 ? arguments[0] : void 0,
					c = this,
					l = [];
				if (
					(y(c, {
						type: "URLSearchParams",
						entries: l,
						updateURL: null,
						updateSearchParams: M,
					}),
					void 0 !== a)
				)
					if (p(a))
						if ("function" == typeof (t = m(a)))
							for (e = t.call(a); !(r = e.next()).done; ) {
								if (
									(o = (n = g(d(r.value))).next()).done ||
									(i = n.next()).done ||
									!n.next().done
								)
									throw TypeError("Expected sequence with length 2");
								l.push({ key: o.value + "", value: i.value + "" });
							}
						else for (s in a) f(a, s) && l.push({ key: s, value: a[s] + "" });
					else
						O(
							l,
							"string" == typeof a
								? "?" === a.charAt(0)
									? a.slice(1)
									: a
								: a + ""
						);
			},
			P = k.prototype;
		s(
			P,
			{
				append: function (t, e) {
					L(arguments.length, 2);
					var r = b(this);
					r.entries.push({ key: t + "", value: e + "" }),
						r.updateURL && r.updateURL();
				},
				delete: function (t) {
					L(arguments.length, 1);
					for (
						var e = b(this), r = e.entries, n = t + "", o = 0;
						o < r.length;

					)
						r[o].key === n ? r.splice(o, 1) : o++;
					e.updateURL && e.updateURL();
				},
				get: function (t) {
					L(arguments.length, 1);
					for (var e = b(this).entries, r = t + "", n = 0; n < e.length; n++)
						if (e[n].key === r) return e[n].value;
					return null;
				},
				getAll: function (t) {
					L(arguments.length, 1);
					for (
						var e = b(this).entries, r = t + "", n = [], o = 0;
						o < e.length;
						o++
					)
						e[o].key === r && n.push(e[o].value);
					return n;
				},
				has: function (t) {
					L(arguments.length, 1);
					for (var e = b(this).entries, r = t + "", n = 0; n < e.length; )
						if (e[n++].key === r) return !0;
					return !1;
				},
				set: function (t, e) {
					L(arguments.length, 1);
					for (
						var r,
							n = b(this),
							o = n.entries,
							i = !1,
							s = t + "",
							a = e + "",
							c = 0;
						c < o.length;
						c++
					)
						(r = o[c]).key === s &&
							(i ? o.splice(c--, 1) : ((i = !0), (r.value = a)));
					i || o.push({ key: s, value: a }), n.updateURL && n.updateURL();
				},
				sort: function () {
					var t,
						e,
						r,
						n = b(this),
						o = n.entries,
						i = o.slice();
					for (o.length = 0, e = 0; e < i.length; e++) {
						for (t = i[e], r = 0; r < e; r++)
							if (o[r].key > t.key) {
								o.splice(r, 0, t);
								break;
							}
						r === e && o.push(t);
					}
					n.updateURL && n.updateURL();
				},
				forEach: function (t) {
					for (
						var e,
							r = b(this).entries,
							n = h(t, arguments.length > 1 ? arguments[1] : void 0, 3),
							o = 0;
						o < r.length;

					)
						n((e = r[o++]).value, e.key, this);
				},
				keys: function () {
					return new N(this, "keys");
				},
				values: function () {
					return new N(this, "values");
				},
				entries: function () {
					return new N(this, "entries");
				},
			},
			{ enumerable: !0 }
		),
			i(P, v, P.entries),
			i(
				P,
				"toString",
				function () {
					for (var t, e = b(this).entries, r = [], n = 0; n < e.length; )
						(t = e[n++]), r.push(C(t.key) + "=" + C(t.value));
					return r.join("&");
				},
				{ enumerable: !0 }
			),
			a(k, "URLSearchParams"),
			n({ global: !0, forced: !o }, { URLSearchParams: k }),
			(t.exports = { URLSearchParams: k, getState: b });
	},
	function (t, e, r) {
		var n = r(4),
			o = r(61);
		t.exports = function (t) {
			var e = o(t);
			if ("function" != typeof e)
				throw TypeError(String(t) + " is not iterable");
			return n(e.call(t));
		};
	},
	function (t, e, r) {
		"use strict";
		r.r(e);
		r(66);
		r(143), r(22), r(34), r(10), r(67), r(84), r(26);
		const n = {
				EMPTY: "",
				TRUE: "t",
				FALSE: "f",
				OK: "0",
				ERROR: "1",
				NOT_EXISTS: "-2147483648",
				UNDEF: "undefined",
			},
			o = {
				EDGE_LIMITATION_DATE: new Date(2019, 9, 16),
				NOT_EXISTS: -2147483648,
				EXISTS: -2147483647,
				page: null,
				page_cpanel: null,
				mobile: !1,
				browser: (function () {
					var t = window.msBrowser || window.browser || window.chrome;
					if (void 0 === t)
						try {
							t = chrome || browser;
						} catch (t) {
							console.error("ERRR: ", t);
						}
					return t;
				})(),
				PAGE_URL: "",
				PAGE_PROTOCOL: "",
				PAGE_HOSTNAME: "",
				IFRAME: "",
				IMAGE_PROCESSING_ENABLED: !1,
				BUILT_IN_DARK_THEME_MODE: 3,
				TURBO_CACHE_ENABLED: !1,
				URL: "",
				isInitialConvertedCounter: 0,
				IMPORT_CSS_INDEX_LAST_POSITION: 1e3,
				url_thankyou_page: "https://nighteye.app/thank-you/",
			};
		class i {
			static makeURL(t, e, r, n, i) {
				void 0 === e &&
					((e = o.PAGE_PROTOCOL),
					(r = o.PAGE_HOSTNAME),
					(n = o.PAGE_PORT),
					(i = o.PAGE_URL)),
					"" !== n && (n = ":" + n);
				var s = t.lastIndexOf("/%20/");
				return s > -1
					? e + "//" + r + n + t.substring(s + 4)
					: "//" === t.slice(0, 2)
					? e + t
					: "/" === t[0]
					? e + "//" + r + n + t
					: -1 !== t.slice(0, 8).lastIndexOf("://")
					? t
					: i + t;
			}
			static parseURL(t) {
				var e = (t = t.replace("www.", n.EMPTY)).indexOf("://"),
					r = 0;
				return (
					-1 !== e &&
						-1 !== (r = (t = t.substring(e + 3)).indexOf("/")) &&
						(t = t.substring(0, r)),
					-1 !== (r = t.indexOf(":", e)) && (t = t.substring(0, r)),
					t
				);
			}
			static isOSColorSchemeDark() {
				const t = window.matchMedia("(prefers-color-scheme: dark)").matches;
				window.matchMedia("(prefers-color-scheme: light)").matches,
					window.matchMedia("(prefers-color-scheme: no-preference)").matches;
				return t;
			}
			static convertBytesToHumanReadable(t, e) {
				var r = e ? 1e3 : 1024;
				if (Math.abs(t) < r) return t + " B";
				var n = e
						? ["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
						: ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"],
					o = -1;
				do {
					(t /= r), ++o;
				} while (Math.abs(t) >= r && o < n.length - 1);
				return t.toFixed(1) + " " + n[o];
			}
			static showInfoDialog(t, e) {
				null === t && (t = "Explanation");
				var r = document.createElement("div");
				r.className = "DialogDim";
				var n = document.createElement("div");
				n.className = "DialogWrapper";
				var o = document.createElement("div");
				n.appendChild(o), (o.className = "Dialog");
				var i = document.createElement("div");
				o.appendChild(i), (i.className = "Title"), (i.innerHTML = t);
				var s = document.createElement("div");
				o.appendChild(s), (s.className = "Content"), (s.innerHTML = e);
				var a = document.createElement("div");
				o.appendChild(a), (a.className = "ButtonWrapper");
				var c = document.createElement("a");
				a.appendChild(c),
					(c.innerHTML = "OK"),
					c.addEventListener("click", () => {
						document.body.removeChild(r), document.body.removeChild(n);
					}),
					document.body.appendChild(r),
					document.body.appendChild(n);
			}
			static isBuiltInDarkThemeExists(t) {
				var e = i.BuiltInWebsites;
				for (var r in e) {
					if (e[r] === t) return !0;
				}
				return !1;
			}
		}
		(i.Mode = { DARK: 1, FILTERED: 2, NORMAL: 4 }),
			(i.BuiltInBehaviourMode = { DISABLED: 1, CONVERT: 2, INTEGRATED: 3 }),
			(i.BuiltInWebsites = {
				YOUTUBE: "youtube.com",
				REDDIT: "reddit.com",
				TWITTER: "twitter.com",
				DUCK_DUCK_GO: "duckduckgo.com",
				COIN_MARKET_CAP: "coinmarketcap.com",
				TWITCH: "twitch.tv",
				NINE_GAG: "9gag.com",
				DOCS_MICROSOFT: "docs.microsoft.com",
				ARSTECHNICA: "arstechnica.com",
				MATERIAL_UI: "material-ui.com",
				INDIEHACKERS: "indiehackers.com",
			}),
			(i.unsupported = {
				newtab: null,
				extensions: null,
				"chrome.google.com": null,
				"accounts.google.com": null,
				"nighteye.app": null,
				"billing.nighteye.app": null,
			}),
			(i.LOCAL_STORAGE = {
				KEYS: {
					MODE: "darkyMode",
					STATE: "darkyState",
					SUPPORTED: "darkySupported",
				},
			});
		class s {
			static sendPromise(t, e, r) {
				return new Promise(function (n, i) {
					try {
						o.browser.runtime.sendMessage(
							{ key: t, action: e, data: r },
							(t) => {
								n(t);
							}
						);
					} catch (t) {
						i(t);
					}
				});
			}
			static send(t, e, r, n) {
				o.browser.runtime.sendMessage({ key: t, action: e, data: r }, n);
			}
		}
		s.Settings = {
			ID: 1,
			ACTION_CHANGE_MODE: 1,
			ACTION_DISABLE: 2,
			ACTION_ENABLE: 3,
			ACTION_ON_START_BROWSER_ACTION: 4,
			ACTION_ON_START_CONVERTER: 5,
			ACTION_UPDATE_IMAGES: 6,
			ACTION_UPDATE_BRIGHTNESS: 7,
			ACTION_UPDATE_CONTRAST: 8,
			ACTION_UPDATE_SATURATION: 9,
			ACTION_UPDATE_TEMPERATURE: 10,
			ACTION_UPDATE_COLOR: 11,
			ACTION_RESTORE_COLOR_DEFAULTS: 12,
			ACTION_UPDATE_DEFAULT_MODE: 13,
			ACTION_UPDATE_DIM: 14,
			ACTION_ON_CHAT_INFO: 15,
			ACTION_UPDATE_CHAT_INFO: 16,
			ACTION_AFTER_FIRST_RUN: 17,
			ACTION_UPDATE_DAY_NIGHT_TIMERS: 18,
			ACTION_UPDATE_TRIAL_EXPIRED: 19,
			ACTION_COLLECT_DATA_FOR_EXPORT: 20,
			ACTION_ACTIVATE_COLOR_SELECTOR: 21,
			ACTION_TOGGLE_MODE: 22,
			ACTION_CHANGE_LANG: 23,
			ACTION_GET_LANG: 24,
			ACTION_GET_CLIENT_INFO: 25,
			ACTION_OPEN_PRICING: 26,
			ACTION_UPDATE_INSTANCE_AND_DEVICE_IDS: 27,
			ACTION_ACTIVATE_EXT: 28,
			ACTION_DEACTIVATE_EXT: 29,
			ACTION_UPDATE_USER_AGENT: 30,
			ACTION_GET_ACCOUNT_MODEL: 31,
			ACTION_GET_TRIAL_EXPIRED_SCREEN_SHOWN_TS: 32,
			ACTION_UPDATE_TRIAL_EXPIRED_SCREEN_SHOWN_TS: 33,
			ACTION_TOGGLE_POWER: 34,
			ACTION_UPDATE_PROPERTY: 35,
			ACTION_GET_PROPERTY: 36,
			ACTION_CHECK_LICENSE: 37,
			ACTION_START_LICENCE_CHECKER: 38,
			ACTION_OPEN_SAFARI_PREFERENCES: 39,
			ACTION_UPDATE_ALLOWED_WEBSITES: 40,
			ACTION_GET_ALLOWED_WEBSITES: 41,
			ACTION_UPDATE_IS_LITE_VERSION: 42,
			ACTION_UPDATE_CHANGING_SCROLLS_ENABLED: 43,
			ACTION_UPDATE_BUILT_IN_THEME_BEHAVIOUR_MODE: 44,
			ACTION_UPDATE_OS_COLOR_SCHEME_CHECKER_ENABLED: 45,
			ACTION_UPDATE_TURBO_CACHE: 46,
		};
		var a = i;
		class c {
			static fromState(t, e) {
				if ("string" == typeof e)
					try {
						e = JSON.parse(e);
					} catch (t) {}
				var r = new t();
				return c.fillModel(r, e), r;
			}
			static fillModel(t, e) {
				for (const n in t)
					if (
						t.hasOwnProperty(n) &&
						null !== e &&
						"object" == typeof e &&
						n in e
					) {
						var r = t[n];
						"object" == typeof r && null !== r
							? c.fillModel(t[n], e[n])
							: (t[n] = e[n]);
					}
			}
			static fromJSON() {
				throw Error("Function not implemented");
			}
			static toState(t) {
				return JSON.stringify(t);
			}
		}
		class l {
			constructor() {
				(this.default_v = !1), (this.global_v = !1), (this.user_changed = !1);
			}
		}
		class u {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class f {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class h {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class d {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class p {
			constructor() {
				(this.default_v = 0), (this.global_v = 0), (this.user_changed = !1);
			}
		}
		class g {
			constructor() {
				(this.default_v = !1), (this.global_v = !1), (this.user_changed = !1);
			}
		}
		class m {
			constructor() {
				(this.default_v = 3), (this.global_v = 3), (this.user_changed = !1);
			}
		}
		var v = {
			IMAGES: "images",
			BUILT_IN_DARK_THEME: "builtInDarkTheme",
			TURBO_CACHE: "turboCache",
			BRIGHTNESS: "brightness",
			CONTRAST: "contrast",
			SATURATION: "saturation",
			TEMPERATURE: "temperature",
			DIM: "dim",
		};
		class y {
			constructor() {
				(this.images = new l()),
					(this.builtInDarkTheme = new m()),
					(this.turboCache = new g()),
					(this.brightness = new u()),
					(this.contrast = new f()),
					(this.saturation = new h()),
					(this.temperature = new d()),
					(this.dim = new p()),
					(this.palette = {});
			}
			parseAndFill(t) {
				if ("string" == typeof t)
					try {
						t = JSON.parse(t);
					} catch (t) {}
				(this.images = v.IMAGES in t ? t[v.IMAGES] : new l()),
					(this.builtInDarkTheme =
						v.BUILT_IN_DARK_THEME in t ? t[v.BUILT_IN_DARK_THEME] : new m()),
					(this.turboCache = v.TURBO_CACHE in t ? t[v.TURBO_CACHE] : new g()),
					(this.brightness = v.BRIGHTNESS in t ? t[v.BRIGHTNESS] : new u()),
					(this.contrast = v.CONTRAST in t ? t[v.CONTRAST] : new f()),
					(this.saturation = v.SATURATION in t ? t[v.SATURATION] : new h()),
					(this.temperature = v.TEMPERATURE in t ? t[v.TEMPERATURE] : new d()),
					(this.dim = v.DIM in t ? t[v.DIM] : new p());
			}
			static getInitialModel(t) {
				switch (t) {
					case v.IMAGES:
						return new l();
					case v.BUILT_IN_DARK_THEME:
						return new m();
					case v.TURBO_CACHE:
						return new g();
					case v.BRIGHTNESS:
						return new u();
					case v.CONTRAST:
						return new f();
					case v.SATURATION:
						return new h();
					case v.TEMPERATURE:
						return new d();
					case v.DIM:
						return new p();
				}
			}
		}
		class b {
			constructor() {
				(this.start = !1),
					(this.enabled = !1),
					(this.show_alert = !1),
					(this.mode = a.Mode.NORMAL),
					(this.local_settings = new y()),
					(this.local_state = ""),
					(this.local_mode = a.Mode.NORMAL),
					(this.local_supported = ""),
					(this.is_trial_expired = !1),
					(this.isLiteVersion = !1),
					(this.lastExtensionWindowOpenTS = -1),
					(this.isChangingScrollsEnabled = !0);
			}
		}
		var S = r(144),
			_ = r.n(S);
		var E = class {
			static insertAfter(t, e) {
				null !== e.nextSibling
					? e.parentNode.insertBefore(t, e.nextSibling)
					: (null === e.parentNode
							? document.documentElement
							: e.parentNode
					  ).appendChild(t);
			}
			static insertBefore(t, e) {
				e.parentNode.insertBefore(t, e);
			}
			static makeParsedStyleNode() {
				var t = document.createElement("style");
				return (t.tgParsed = !0), (t.tgIgnore = !0), t;
			}
			static makeParsedLinkNode(t) {
				var e = document.createElement("link");
				return (
					(e.tgParsed = !0),
					(e.tgIgnore = !0),
					(e.rel = "stylesheet"),
					(e.href = t),
					e
				);
			}
		};
		var T = class {
			constructor(t) {
				(this.observer = null),
					(this.config = {
						childList: !0,
						attributes: !0,
						subtree: !0,
						attributeFilter: ["style", "fill", "src", "bgcolor", "ne"],
					}),
					(this.repeatedElementsMap = []),
					(this.running = !1),
					(this.core = t),
					(this.styleConverter = t.styleConverter),
					(this.styleConverter.startObserver = this.start.bind(this)),
					(this.styleConverter.stopObserver = this.stop.bind(this));
			}
			init() {
				(this.observer = new MutationObserver((t) => {
					this.onMutations(t);
				})),
					this.start();
			}
			onMutations(t) {
				!1 !== this.running &&
					setTimeout(() => {
						++o.isInitialConvertedCounter,
							this.process(t),
							--o.isInitialConvertedCounter;
					}, 0);
			}
			start() {
				(this.running = !0),
					this.observer.observe(document.documentElement, this.config);
			}
			stop() {
				(this.running = !1), this.observer.disconnect();
			}
			process(t) {
				for (var e = t.length; e-- > 0; ) {
					var r = t[e];
					"HEAD" === r.target.nodeName && this.onNodesRemoved(r.removedNodes),
						"STYLE" !== r.target.tagName
							? (this.onAttributeChange(r), this.onNodesAdded(r.addedNodes))
							: this.onNodeAdded(r.target);
				}
			}
			onNodesRemoved(t) {
				for (var e = t.length; e-- > 0; ) {
					"nighteyedefaultcss" === t[e].id && this.reinitContentScripts();
				}
			}
			onNodesAdded(t) {
				for (var e = t.length; e-- > 0; ) {
					var r = t[e];
					if ((this.onNodeAdded(r), void 0 !== r.querySelectorAll))
						for (var n = r.querySelectorAll("*"), o = n.length; o-- > 0; )
							this.onNodeAdded(n[o]);
				}
			}
			onNodeAdded(t) {
				if (t.tgIgnore) t.tgIgnore = !1;
				else if (!this.styleConverter.isInitializationNotFinished)
					switch (t.tagName) {
						case "VIDEO":
						case "CANVAS":
						case "SCRIPT":
							break;
						case "IFRAME":
							try {
								this.core.state.addCustomStyles(t.contentWindow.document);
							} catch (t) {}
							this.styleConverter.convertIFrame(t);
							break;
						case "LINK":
							if ("import" === t.rel) {
								this.styleConverter.convertLinkImports();
								break;
							}
							if ("stylesheet" !== t.rel) break;
							var e = t.media;
							if (
								(t.addEventListener("load", () => {
									var r = e !== t.media;
									this.styleConverter.convertStyleSheetFromMutator(
										t.sheet,
										!1,
										r
									);
								}),
								null !== t.sheet)
							) {
								var r = e !== t.media;
								this.styleConverter.convertStyleSheetFromMutator(
									t.sheet,
									!1,
									r
								);
							}
							break;
						case "STYLE":
							null !== t.sheet &&
								this.styleConverter.convertStyleSheetFromMutator(
									t.sheet,
									!0,
									!1
								);
							break;
						case "IMG":
							this.styleConverter.convertImgNode(t);
						default:
							if (void 0 === t.getAttribute) return;
							if (void 0 !== t.fileUrl) return;
							this.styleConverter.convertInlineStyle(t);
					}
			}
			onAttributeChange(t) {
				if ("attributes" === t.type && void 0 !== t.target.getAttribute)
					if (t.target.tgIgnore) t.target.tgIgnore = !1;
					else {
						if ("ne" === t.attributeName)
							return (
								t.target.removeAttribute("ne"), void (t.target.tgIgnore = !1)
							);
						if (t.target.tgIgnoreVariableCounter > 0)
							--t.target.tgIgnoreVariableCounter;
						else if (
							(void 0 === t.target.invokeCounter &&
								(t.target.invokeCounter = 0),
							!(++t.target.invokeCounter > 50))
						)
							switch (t.target.tagName) {
								case "VIDEO":
								case "CANVAS":
								case "SCRIPT":
								case "LINK":
								case "STYLE":
									break;
								case "IFRAME":
									this.styleConverter.convertIFrame(t.target);
									break;
								case "IMG":
									this.styleConverter.convertImgNode(t.target);
								default:
									this.styleConverter.convertInlineStyle(t.target);
							}
					}
			}
			reinitContentScripts() {
				this.core.reinitDomElements(), this.core.state.addCustomStyles();
			}
		};
		var A = class {
			constructor(t, e) {
				(this.styleConverter = t),
					(this.isChangingScrollsEnabled = e),
					(this.loading_added = !1),
					(this.isReady = !1),
					(this.emergencyCheckCounter = 0);
			}
			initAndShowLoading(t) {
				if (!0 !== this.loading_added && !this.isReady) {
					if (((this.loading_added = !0), t))
						return (
							this.checkDocumentState(t),
							void setTimeout(() => {
								this.onReady(t);
							}, 6e3)
						);
					this.onReady(!1);
				}
			}
			onReady(t) {
				this.isReady ||
					(t
						? (document.documentElement.setAttribute("nighteye", "active"),
						  (window.self !== window.top &&
								"music.163.com" !== window.location.host &&
								"analytics.google.com" !== window.location.host) ||
								(this.addCustomStyles(document), this.modifyCustomSites()))
						: document.documentElement.setAttribute("nighteye", "disabled"),
					(this.isReady = !0));
			}
			checkDocumentState(t) {
				var e = setInterval(() => {
					if (null !== document.body) {
						clearInterval(e), this.checkCSSParsedFinished(t);
						var r = setInterval(() => {
							(this.checkCSSParsedFinished(t) ||
								++this.emergencyCheckCounter >= 70) &&
								clearInterval(r);
						}, 100);
					}
				}, 100);
			}
			checkCSSParsedFinished(t) {
				for (
					var e = document.documentElement.querySelectorAll(
							'link[rel="stylesheet"]'
						).length,
						r = 0,
						n = 0;
					n < document.styleSheets.length;
					++n
				)
					if (
						("LINK" ===
							document.styleSheets[n].ownerNode.nodeName.toUpperCase() && ++r,
						!document.styleSheets[n].ownerNode.tgParsed)
					)
						return !1;
				return (
					!(e > r) && 0 === o.isInitialConvertedCounter && (this.onReady(t), !0)
				);
			}
			addCustomStyles(t) {
				var e = o.URL;
				e.indexOf("google.") > -1
					? (this.addCustomCSS("google.css", !0, t),
					  "google.com" === e
							? this.addCustomCSS("google.com.css", !0, t)
							: "mail.google.com" === e
							? this.addCustomCSS("mail.google.com.css", !0, t)
							: "calendar.google.com" === e
							? this.addCustomCSS("calendar.google.com.css", !0, t)
							: "drive.google.com" === e
							? this.addCustomCSS("drive.google.com.css", !0, t)
							: "docs.google.com" === e
							? this.addCustomCSS("docs.google.com.css", !0, t)
							: "analytics.google.com" === e
							? this.addCustomCSS("analytics.google.com.css", !0, t)
							: "groups.google.com" === e
							? this.addCustomCSS("groups.google.com.css", !0, t)
							: e.indexOf("books.google.") > -1 &&
							  this.addCustomCSS("books.google.com.css", !0, t))
					: e.indexOf("github.") > -1
					? this.addCustomCSS("github.css", !0, t)
					: e.indexOf("facebook.com") > -1
					? this.addCustomCSS("facebook.com.css", !0, t)
					: e.indexOf("feedly.com") > -1
					? this.addCustomCSS("feedly.com.css", !0, t)
					: e.indexOf(".force.com") > -1
					? this.addCustomCSS("salesforce.com.css", !0, t)
					: "community.soundcloud.com" === e
					? this.addCustomCSS("community.soundcloud.com.css", !0, t)
					: "material.io" === e
					? this.addCustomCSS("material.io.css", !0, t)
					: e.indexOf("ign.com") > -1
					? this.addCustomCSS("ign.com.css", !0, t)
					: e.indexOf("bbc.com") > -1
					? this.addCustomCSS("bbc.com.css", !0, t)
					: e.indexOf("wikipedia.org") > -1
					? this.addCustomCSS("wikipedia.org.css", !0, t)
					: "pcoservices.zendesk.com" === e
					? this.addCustomCSS("pcoservices.zendesk.com.css", !0, t)
					: "gazetadopovo.com.br" === e
					? this.addCustomCSS("gazetadopovo.com.br.css", !0, t)
					: "protonvpn.com" === e
					? this.addCustomCSS("protonvpn.com.css", !0, t)
					: "messenger.com" === e
					? this.addCustomCSS("messenger.com.css", !0, t)
					: "app.asana.com" === e
					? this.addCustomCSS("app.asana.com.css", !0, t)
					: "instagram.com" === e
					? this.addCustomCSS("instagram.com.css", !0, t)
					: "alibaba.com" === e
					? this.addCustomCSS("alibaba.com.css", !0, t)
					: e.indexOf("4chan.org") > -1
					? this.addCustomCSS("4chan.org.css", !0, t)
					: "web.whatsapp.com" === e
					? this.addCustomCSS("web.whatsapp.com.css", !0, t)
					: "youtube.com" === e
					? this.addCustomCSS("youtube.com.css", !0, t)
					: "thesaurus.com" === e
					? this.addCustomCSS("thesaurus.com.css", !0, t)
					: "wordpress.com" === e
					? this.addCustomCSS("wordpress.com.css", !0, t)
					: "msn.com" === e
					? this.addCustomCSS("msn.com.css", !0, t)
					: "digitec.ch" === e
					? this.addCustomCSS("digitec.ch.css", !0, t)
					: "cinecalidad.to" === e
					? this.addCustomCSS("cinecalidad.to.css", !0, t)
					: "virustotal.com" === e
					? this.addCustomCSS("virustotal.com.css", !0, t)
					: "theprofstudent.com" === e
					? this.addCustomCSS("theprofstudent.com.css", !0, t)
					: "distrowatch.com" === e
					? this.addCustomCSS("distrowatch.com.css", !0, t)
					: "music.163.com" === e
					? this.addCustomCSS("music.163.com.css", !0, t)
					: "strava.com" === e
					? this.addCustomCSS("strava.com.css", !0, t)
					: "auth0.com" === e
					? this.addCustomCSS("auth0.com.css", !0, t)
					: "bing.com" === e
					? this.addCustomCSS("bing.com.css", !0, t)
					: "imdb.com" === e
					? this.addCustomCSS("imdb.com.css", !0, t)
					: e.indexOf("craigslist.org") > -1
					? this.addCustomCSS("craigslist.org.css", !0, t)
					: "washingtonpost.com" === e
					? this.addCustomCSS("washingtonpost.com.css", !0, t)
					: "regexr.com" === e
					? this.addCustomCSS("regexr.com.css", !0, t)
					: "overleaf.com" === e
					? this.addCustomCSS("overleaf.com.css", !0, t)
					: "mailgun.com" === e
					? this.addCustomCSS("mailgun.com.css", !0, t)
					: "asdf-vm.com" === e
					? this.addCustomCSS("asdf-vm.com.css", !0, t)
					: "stackoverflow.com" === e
					? this.addCustomCSS("stackoverflow.com.css", !0, t)
					: "english.stackexchange.com" === e
					? this.addCustomCSS("english.stackexchange.com.css", !0, t)
					: "bugs.chromium.org" === e
					? this.addCustomCSS("bugs.chromium.org.css", !0, t)
					: "spaceweather.com" === e
					? this.addCustomCSS("spaceweather.com.css", !0, t)
					: "pixivision.net" === e
					? this.addCustomCSS("pixivision.net.css", !0, t)
					: "paper.dropbox.com" === e
					? this.addCustomCSS("paper.dropbox.com.css", !0, t)
					: "tunein.com" === e
					? this.addCustomCSS("tunein.com.css", !0, t)
					: "readcube.com" === e
					? this.addCustomCSS("readcube.com.css", !0, t)
					: "home.gamer.com.tw" === e
					? this.addCustomCSS("home.gamer.com.tw.css", !0, t)
					: "mail.protonmail.com" === e
					? this.addCustomCSS("mail.protonmail.com.css", !0, t)
					: "mathworld.wolfram.com" === e
					? this.addCustomCSS("mathworld.wolfram.com.css", !0, t)
					: "ordbogen.com" === e
					? this.addCustomCSS("ordbogen.com.css", !0, t)
					: "mangasail.co" === e
					? this.addCustomCSS("mangasail.co.css", !0, t)
					: "clubmate.fi" === e
					? this.addCustomCSS("clubmate.fi.css", !0, t)
					: "to-do.microsoft.com" === e
					? this.addCustomCSS("to-do.microsoft.com.css", !0, t)
					: "docs.opencv.org" === e
					? this.addCustomCSS("docs.opencv.org.css", !0, t)
					: "evernote.com" === e
					? this.addCustomCSS("evernote.com.css", !0, t)
					: "schwab.com" === e
					? this.addCustomCSS("schwab.com.css", !0, t)
					: "tv.apple.com" === e
					? this.addCustomCSS("tv.apple.com.css", !0, t)
					: "studio.youtube.com" === e
					? this.addCustomCSS("studio.youtube.com.css", !0, t)
					: "bol.com" === e
					? this.addCustomCSS("bol.com.css", !0, t)
					: "dw.com" === e
					? this.addCustomCSS("dw.com.css", !0, t)
					: "app.diagrams.net" === e
					? this.addCustomCSS("app.diagrams.net.css", !0, t)
					: "rescuetime.com" === e
					? this.addCustomCSS("rescuetime.com.css", !0, t)
					: "lingualeo.com" === e
					? this.addCustomCSS("lingualeo.com.css", !0, t)
					: "ebay.com" === e && this.addCustomCSS("ebay.com.css", !0, t),
					this.addCustomCSS("custom-styles.css", !1, t),
					this.isChangingScrollsEnabled &&
						this.addCustomCSS("scroll.css", !1, t);
			}
			addCustomCSS(t, e, r) {
				t = e ? "css/websites/" + t : "css/" + t;
				var n = o.browser.extension.getURL(t),
					i = null === r.head ? r.documentElement : r.head,
					s = E.makeParsedLinkNode(n);
				i.appendChild(s), (s.customWebsiteCSS = !0);
			}
			modifyCustomSites() {
				try {
					var t = window.location.hostname.replace("www.", "");
					if (
						"google." === t.substring(0, 7) ||
						"images.google." === t.substring(0, 14)
					) {
						var e = document.querySelector("form input[type='text']");
						if (null !== e) {
							var r = e.parentNode.parentNode.parentNode;
							(r.style.backgroundColor = "#242424"),
								(r.querySelector("svg").parentNode.style.backgroundColor =
									"#242424");
						}
					} else if ("calendar.google.com" === t) {
						document.getElementById(
							"drawerMiniMonthNavigator"
						).parentNode.className += " NE_Hidden";
					} else if ("drudgereport.com" === t) {
						let t = setInterval(() => {
							var e = document.documentElement.getAttribute("style");
							void 0 !== e &&
								(document.documentElement.style.removeProperty(
									"background-image"
								),
								e.indexOf("background-image: none") > -1 && clearInterval(t));
						}, 500);
					}
				} catch (t) {}
			}
		};
		r(105);
		class w {
			static makeHSLColorString(t) {
				return "hsl(" + t[0] + "," + t[1] + "%," + t[2] + "%)";
			}
			static makeColorKeyFromArray(t) {
				return (t[0] << 16) | (t[1] << 8) | t[2];
			}
			static makeHSLColorArrayFromKey(t) {
				return [(t >> 16) & 255, (t >> 8) & 255, 255 & t];
			}
			static forceRGBGray(t) {
				const e = Math.min(t[0], Math.min(t[1], t[2])),
					r = Math.max(t[0], Math.max(t[1], t[2]));
				if (r - e > Math.round(8 + (4 * (255 - e)) / 255)) return;
				const n = Math.round(0.5 * (r - e));
				t[0] = t[1] = t[2] = e + n;
			}
			static RGBtoHSL(t) {
				w.forceRGBGray(t);
				var e,
					r,
					n,
					o = t[0] / 255,
					i = t[1] / 255,
					s = t[2] / 255,
					a = Math.max(o, Math.max(i, s)),
					c = Math.min(o, Math.min(i, s)),
					l = a - c;
				(n = 0.5 * (a + c)),
					0 === l
						? ((e = 0), (r = 0))
						: ((e =
								a === o
									? 60 * w.modFloat((i - s) / l, 6)
									: a === i
									? 60 * ((s - o) / l + 2)
									: 60 * ((o - i) / l + 4)),
						  (r = l / (1 - Math.abs(2 * n - 1)))),
					(t[0] = Math.round(e)),
					(t[1] = Math.round(100 * r)),
					(t[2] = Math.round(100 * n));
			}
			static HSLtoRGB(t) {
				t[0] %= 360;
				var e = 0.01 * t[1],
					r = 0.01 * t[2],
					n = (1 - Math.abs(2 * r - 1)) * e,
					o = n * (1 - Math.abs(w.modFloat(t[0] / 60, 2) - 1)),
					i = r - 0.5 * n;
				t[0] < 180
					? t[0] < 60
						? ((t[0] = n), (t[1] = o), (t[2] = 0))
						: t[0] < 120
						? ((t[0] = o), (t[1] = n), (t[2] = 0))
						: ((t[0] = 0), (t[1] = n), (t[2] = o))
					: t[0] < 240
					? ((t[0] = 0), (t[1] = o), (t[2] = n))
					: t[0] < 300
					? ((t[0] = o), (t[1] = 0), (t[2] = n))
					: ((t[0] = n), (t[1] = 0), (t[2] = o)),
					(t[0] = Math.round(255 * (t[0] + i))),
					(t[1] = Math.round(255 * (t[1] + i))),
					(t[2] = Math.round(255 * (t[2] + i)));
			}
			static modFloat(t, e) {
				var r = t / e;
				return (r -= Math.floor(r)) * e;
			}
		}
		var x = w;
		class R {
			constructor(t, e) {
				(this.key = t),
					(this.val = e),
					(this.newer = null),
					(this.older = null);
			}
		}
		class I {
			constructor(t) {
				(this.capacity = t),
					(this.length = 0),
					(this.map = new Map()),
					(this.head = null),
					(this.tail = null);
			}
			node(t, e) {
				return new R(t, e);
			}
			get(t) {
				return this.map.has(t) ? (this.updateKey(t), this.map.get(t).val) : -1;
			}
			updateKey(t) {
				var e = this.map.get(t);
				e.newer ? (e.newer.older = e.older) : (this.head = e.older),
					e.older ? (e.older.newer = e.newer) : (this.tail = e.newer),
					(e.older = this.head),
					(e.newer = null),
					this.head && (this.head.newer = e),
					(this.head = e),
					this.tail || (this.tail = e);
			}
			set(t, e) {
				var r = this.node(t, e);
				if (this.map.has(t))
					return (this.map.get(t).val = e), void this.updateKey(t);
				if (this.length >= this.capacity) {
					var n = this.tail.key;
					(this.tail = this.tail.newer), this.tail && (this.tail.older = null);
					var o = this.map.get(n);
					void 0 !== o && ((this.length -= o.val.length), this.map.delete(n));
				}
				(r.older = this.head),
					this.head && (this.head.newer = r),
					(this.head = r),
					this.tail || (this.tail = r),
					this.map.set(t, r),
					(this.length += r.val.length);
			}
		}
		var C = class {
			static processBackgroundHSLColorArray(t) {
				var e = !1;
				return 0 == t[0] && 0 == t[1] && 100 == t[2]
					? ((t[0] = 240), (t[1] = 8), (t[2] = 12), !0)
					: (t[1] > 60 && ((t[1] = 60), (e = !0)),
					  t[2] > 60 && ((t[2] = 100 - t[2] + 10), (e = !0)),
					  t[0] > 40 &&
							t[0] <= 60 &&
							t[1] > 40 &&
							t[2] < 70 &&
							((t[0] = 44), (t[1] = 100), (t[2] = 20), (e = !0)),
					  t[0] > 90 &&
							t[0] <= 100 &&
							t[1] > 50 &&
							t[2] <= 55 &&
							((t[0] = 139), (t[1] = 61), (t[2] = 20), (e = !0)),
					  e);
			}
			static processForegroundHSLColorArray(t) {
				var e = !1;
				return (
					t[1] > 60 && ((t[1] = 60), (e = !0)),
					t[2] < 75 && ((t[2] = 75), (e = !0)),
					e
				);
			}
			static makeHSLAString(t) {
				return "hsla(" + t[0] + "," + t[1] + "%," + t[2] + "%," + t[3] + ")";
			}
			static applyPaletteToHSLColors(t) {}
		};
		var O = class {
			constructor(t) {
				(this.initial = [0, 0, 0, 0]), (this.colorProcessor = t);
			}
			applyToHSLColorsAsString(t) {
				C.processBackgroundHSLColorArray(t);
				var e = x.makeColorKeyFromArray(t);
				return (
					(N.colorsWithKey[e] = t),
					(N.colors[e] = null),
					C.applyPaletteToHSLColors(t),
					C.makeHSLAString(t)
				);
			}
			applyToRGBColorsAsString(t) {
				return x.RGBtoHSL(t), this.applyToHSLColorsAsString(t);
			}
			applyToRGBColorsAsArray(t) {
				return (
					x.RGBtoHSL(t),
					!0 === C.processBackgroundHSLColorArray(t) && (x.HSLtoRGB(t), !0)
				);
			}
		};
		var M = class {
			constructor(t) {
				(this.initial = [0, 0, 0, 1]), (this.colorProcessor = t);
			}
			applyToHSLColorsAsString(t) {
				return (
					C.processForegroundHSLColorArray(t),
					(N.colors[x.makeColorKeyFromArray(t)] = null),
					C.applyPaletteToHSLColors(t),
					C.makeHSLAString(t)
				);
			}
			applyToRGBColorsAsString(t) {
				return x.RGBtoHSL(t), this.applyToHSLColorsAsString(t);
			}
		};
		class L {
			constructor() {
				(this.mode_background = new O(this)),
					(this.mode_foreground = new M(this)),
					(this.out = { style_string: "", jump_size: 0, colors: null });
			}
			convertBackgroundColorString(t) {
				return this.convertColorString(t, this.mode_background);
			}
			convertForegroundColorString(t) {
				return this.convertColorString(t, this.mode_foreground);
			}
			convertColorString(t, e) {
				var r = t.indexOf(":");
				-1 === r && (r = 0);
				for (var n = t.substring(0, r), o = r; o < t.length; )
					this.convertHEX(t, o, e),
						0 === this.out.jump_size
							? (this.convertRGB(t, o, e),
							  0 === this.out.jump_size
									? (this.convertName(t, o, e),
									  0 === this.out.jump_size
											? (n += t[o++])
											: ((n += this.out.style_string),
											  (o += this.out.jump_size + 1)))
									: ((n += this.out.style_string),
									  (o += this.out.jump_size + 1)))
							: ((n += this.out.style_string), (o += this.out.jump_size + 1));
				return n;
			}
			convertHEX(t, e, r) {
				var n, o, i;
				if (((this.out.jump_size = 0), "#" === t[e])) {
					if (e + 6 < t.length) {
						for (n = !0, i = 1; i <= 6; ++i)
							if (
								!(
									((o = t[e + i].toLowerCase()) >= "a" && o <= "f") ||
									(o >= "0" && o <= "9")
								)
							) {
								n = !1;
								break;
							}
						if (n) {
							var s = [
								parseInt(t[e + 1] + t[e + 2], 16),
								parseInt(t[e + 3] + t[e + 4], 16),
								parseInt(t[e + 5] + t[e + 6], 16),
								1,
							];
							return (
								(this.out.style_string = r.applyToRGBColorsAsString(s)),
								void (this.out.jump_size = 6)
							);
						}
						if (i >= 4) {
							let n = [
								parseInt(t[e + 1] + t[e + 1], 16),
								parseInt(t[e + 2] + t[e + 2], 16),
								parseInt(t[e + 3] + t[e + 3], 16),
								1,
							];
							return (
								(this.out.style_string = r.applyToRGBColorsAsString(n)),
								void (this.out.jump_size = 3)
							);
						}
					}
					if (e + 3 < t.length) {
						for (n = !0, i = 1; i <= 3; ++i)
							if (
								!(
									((o = t[e + i].toLowerCase()) >= "a" && o <= "f") ||
									(o >= "0" && o <= "9")
								)
							) {
								n = !1;
								break;
							}
						if (n) {
							let n = [
								parseInt(t[e + 1] + t[e + 1], 16),
								parseInt(t[e + 2] + t[e + 2], 16),
								parseInt(t[e + 3] + t[e + 3], 16),
								1,
							];
							return (
								(this.out.style_string = r.applyToRGBColorsAsString(n)),
								void (this.out.jump_size = 3)
							);
						}
					}
				}
			}
			convertRGB(t, e, r) {
				this.parseColorsToHSL(t, e),
					0 !== this.out.jump_size &&
						(this.out.style_string = r.applyToHSLColorsAsString(
							this.out.colors
						));
			}
			convertName(t, e, r) {
				var n,
					o = null,
					i = L.text_colors;
				for (this.out.jump_size = 0, n = e; n < t.length; ++n) {
					let e = i[t[n]];
					if (void 0 === e) break;
					if (void 0 !== e.colors)
						if (n + 1 === t.length) o = e.colors;
						else {
							var s = t[n + 1];
							(";" !== s && " " !== s && "!" !== s) || (o = e.colors);
						}
					i = e;
				}
				if (null !== o) {
					if (-1 === o[0]) return;
					(this.out.style_string = r.applyToRGBColorsAsString(o.slice())),
						(this.out.jump_size = n - e - 1);
				}
			}
			parseColorsToHSL(t, e) {
				if (((this.out.jump_size = 0), e + 2 >= t.length)) return null;
				var r = !1;
				if ("r" !== t[e] || "g" !== t[e + 1] || "b" !== t[e + 2]) {
					if ("h" !== t[e] || "s" !== t[e + 1] || "l" !== t[e + 2]) return null;
					r = !0;
				}
				var n,
					o,
					i = 0,
					s = -1,
					a = -1;
				for (o = e + 3; o < t.length; ++o)
					if ("(" !== (n = t[o])) {
						if (-1 !== s) {
							if (")" === n) {
								a = o;
								break;
							}
							if (
								!(n >= "0" && n <= "9") &&
								" " !== n &&
								"." !== n &&
								"%" !== n
							) {
								if ("," !== n) break;
								++i;
							}
						}
					} else s = o;
				if (-1 === s || -1 === a) return null;
				if (i < 2 || i > 3) return null;
				var c = [0, 0, 0, 1],
					l = 0,
					u = 1;
				for (o = s + 1; o < a; ++o)
					(n = t[o]) >= "0" && n <= "9"
						? 1 === u
							? ((c[l] *= 10), (c[l] += parseInt(n)))
							: ((c[l] += parseInt(n) / u), (u *= 10))
						: "." === n
						? (u = 10)
						: "," === n && ((u = 1), 3 === ++l && (c[l] = 0));
				return (
					!1 === r && x.RGBtoHSL(c),
					(this.out.jump_size = a - e),
					(this.out.colors = c)
				);
			}
		}
		(L.colors = {}),
			(L.colorsWithKey = {}),
			(L.text_colors = {
				i: {
					n: {
						i: { t: { i: { a: { l: { colors: [26, 26, 26, 1] } } } } },
						d: {
							i: {
								a: { n: { r: { e: { d: { colors: [205, 92, 92, 1] } } } } },
								g: { o: { colors: [75, 0, 130, 1] } },
							},
						},
					},
					v: { o: { r: { y: { colors: [255, 255, 240, 1] } } } },
				},
				"-": {
					w: {
						e: {
							b: {
								k: {
									i: {
										t: {
											"-": {
												l: { i: { n: { k: { colors: [0, 0, 238, 1] } } } },
											},
										},
									},
								},
							},
						},
					},
				},
				w: {
					i: {
						n: {
							d: {
								o: {
									w: {
										colors: [255, 255, 255, 1],
										t: { e: { x: { t: { colors: [0, 0, 0, 1] } } } },
									},
								},
							},
						},
					},
					h: {
						i: {
							t: {
								e: {
									colors: [255, 255, 255, 1],
									s: { m: { o: { k: { e: { colors: [245, 245, 245, 1] } } } } },
								},
							},
						},
						e: { a: { t: { colors: [245, 222, 179, 1] } } },
					},
				},
				l: {
					i: {
						g: {
							h: {
								t: {
									y: {
										e: {
											l: { l: { o: { w: { colors: [255, 255, 224, 1] } } } },
										},
									},
									p: { i: { n: { k: { colors: [255, 182, 193, 1] } } } },
									s: {
										a: {
											l: { m: { o: { n: { colors: [255, 160, 122, 1] } } } },
										},
										t: {
											e: {
												e: {
													l: {
														b: {
															l: { u: { e: { colors: [176, 196, 222, 1] } } },
														},
													},
												},
											},
										},
										k: {
											y: {
												b: { l: { u: { e: { colors: [135, 206, 250, 1] } } } },
											},
										},
										l: {
											a: {
												t: {
													e: {
														g: {
															r: {
																e: { y: { colors: [119, 136, 153, 1] } },
																a: { y: { colors: [119, 136, 153, 1] } },
															},
														},
													},
												},
											},
										},
										e: {
											a: {
												g: {
													r: { e: { e: { n: { colors: [32, 178, 170, 1] } } } },
												},
											},
										},
									},
									g: {
										o: {
											l: {
												d: {
													e: {
														n: {
															r: {
																o: {
																	d: {
																		y: {
																			e: {
																				l: {
																					l: {
																						o: {
																							w: { colors: [250, 250, 210, 1] },
																						},
																					},
																				},
																			},
																		},
																	},
																},
															},
														},
													},
												},
											},
										},
										r: {
											e: {
												y: { colors: [211, 211, 211, 1] },
												e: { n: { colors: [144, 238, 144, 1] } },
											},
											a: { y: { colors: [211, 211, 211, 1] } },
										},
									},
									c: {
										o: { r: { a: { l: { colors: [240, 128, 128, 1] } } } },
										y: { a: { n: { colors: [224, 255, 255, 1] } } },
									},
									b: { l: { u: { e: { colors: [173, 216, 230, 1] } } } },
								},
							},
						},
						n: { e: { n: { colors: [250, 240, 230, 1] } } },
						m: {
							e: {
								g: { r: { e: { e: { n: { colors: [50, 205, 50, 1] } } } } },
								colors: [0, 255, 0, 1],
							},
						},
					},
					e: {
						m: {
							o: {
								n: {
									c: {
										h: {
											i: {
												f: { f: { o: { n: { colors: [255, 250, 205, 1] } } } },
											},
										},
									},
								},
							},
						},
					},
					a: {
						v: {
							e: {
								n: {
									d: {
										e: {
											r: {
												b: {
													l: {
														u: { s: { h: { colors: [255, 240, 245, 1] } } },
													},
												},
												colors: [230, 230, 250, 1],
											},
										},
									},
								},
							},
						},
						w: {
							n: {
								g: { r: { e: { e: { n: { colors: [124, 252, 0, 1] } } } } },
							},
						},
					},
				},
				y: {
					e: {
						l: {
							l: {
								o: {
									w: {
										colors: [255, 255, 0, 1],
										g: {
											r: { e: { e: { n: { colors: [154, 205, 50, 1] } } } },
										},
									},
								},
							},
						},
					},
				},
				s: {
					n: { o: { w: { colors: [255, 250, 250, 1] } } },
					e: {
						a: {
							s: { h: { e: { l: { l: { colors: [255, 245, 238, 1] } } } } },
							g: { r: { e: { e: { n: { colors: [46, 139, 87, 1] } } } } },
						},
					},
					a: {
						l: { m: { o: { n: { colors: [250, 128, 114, 1] } } } },
						n: {
							d: {
								y: {
									b: { r: { o: { w: { n: { colors: [244, 164, 96, 1] } } } } },
								},
							},
						},
						d: {
							d: {
								l: {
									e: {
										b: { r: { o: { w: { n: { colors: [139, 69, 19, 1] } } } } },
									},
								},
							},
						},
					},
					i: {
						l: { v: { e: { r: { colors: [192, 192, 192, 1] } } } },
						e: { n: { n: { a: { colors: [160, 82, 45, 1] } } } },
					},
					k: { y: { b: { l: { u: { e: { colors: [135, 206, 235, 1] } } } } } },
					l: {
						a: {
							t: {
								e: {
									g: {
										r: {
											e: { y: { colors: [112, 128, 144, 1] } },
											a: { y: { colors: [112, 128, 144, 1] } },
										},
									},
									b: { l: { u: { e: { colors: [106, 90, 205, 1] } } } },
								},
							},
						},
					},
					t: {
						e: {
							e: {
								l: { b: { l: { u: { e: { colors: [70, 130, 180, 1] } } } } },
							},
						},
					},
					p: {
						r: {
							i: {
								n: {
									g: {
										g: { r: { e: { e: { n: { colors: [0, 255, 127, 1] } } } } },
									},
								},
							},
						},
					},
				},
				f: {
					l: {
						o: {
							r: {
								a: {
									l: {
										w: {
											h: { i: { t: { e: { colors: [255, 250, 240, 1] } } } },
										},
									},
								},
							},
						},
					},
					u: { c: { h: { s: { i: { a: { colors: [255, 0, 255, 1] } } } } } },
					i: {
						r: {
							e: {
								b: { r: { i: { c: { k: { colors: [178, 34, 34, 1] } } } } },
							},
						},
					},
					o: {
						r: {
							e: {
								s: {
									t: {
										g: { r: { e: { e: { n: { colors: [34, 139, 34, 1] } } } } },
									},
								},
							},
						},
					},
				},
				c: {
					o: {
						r: {
							n: {
								s: { i: { l: { k: { colors: [255, 248, 220, 1] } } } },
								f: {
									l: {
										o: {
											w: {
												e: {
													r: {
														b: {
															l: { u: { e: { colors: [100, 149, 237, 1] } } },
														},
													},
												},
											},
										},
									},
								},
							},
							a: { l: { colors: [255, 127, 80, 1] } },
						},
					},
					r: { i: { m: { s: { o: { n: { colors: [220, 20, 60, 1] } } } } } },
					h: {
						o: {
							c: {
								o: { l: { a: { t: { e: { colors: [210, 105, 30, 1] } } } } },
							},
						},
						a: {
							r: {
								t: {
									r: { e: { u: { s: { e: { colors: [127, 255, 0, 1] } } } } },
								},
							},
						},
					},
					a: {
						d: {
							e: {
								t: { b: { l: { u: { e: { colors: [95, 158, 160, 1] } } } } },
							},
						},
					},
					y: { a: { n: { colors: [0, 255, 255, 1] } } },
				},
				p: {
					a: {
						p: {
							a: {
								y: {
									a: { w: { h: { i: { p: { colors: [255, 239, 213, 1] } } } } },
								},
							},
						},
						l: {
							e: {
								g: {
									o: {
										l: {
											d: {
												e: {
													n: {
														r: { o: { d: { colors: [238, 232, 170, 1] } } },
													},
												},
											},
										},
									},
									r: { e: { e: { n: { colors: [152, 251, 152, 1] } } } },
								},
								v: {
									i: {
										o: {
											l: {
												e: {
													t: {
														r: { e: { d: { colors: [219, 112, 147, 1] } } },
													},
												},
											},
										},
									},
								},
								t: {
									u: {
										r: {
											q: {
												u: {
													o: {
														i: { s: { e: { colors: [175, 238, 238, 1] } } },
													},
												},
											},
										},
									},
								},
							},
						},
					},
					e: {
						a: {
							c: {
								h: { p: { u: { f: { f: { colors: [255, 218, 185, 1] } } } } },
							},
						},
						r: { u: { colors: [205, 133, 63, 1] } },
					},
					i: { n: { k: { colors: [255, 192, 203, 1] } } },
					l: { u: { m: { colors: [221, 160, 221, 1] } } },
					o: {
						w: {
							d: {
								e: {
									r: { b: { l: { u: { e: { colors: [176, 224, 230, 1] } } } } },
								},
							},
						},
					},
					u: { r: { p: { l: { e: { colors: [128, 0, 128, 1] } } } } },
				},
				b: {
					l: {
						a: {
							n: {
								c: {
									h: {
										e: {
											d: {
												a: {
													l: {
														m: {
															o: { n: { d: { colors: [255, 235, 205, 1] } } },
														},
													},
												},
											},
										},
									},
								},
							},
							c: { k: { colors: [0, 0, 0, 1] } },
						},
						u: {
							e: {
								v: {
									i: { o: { l: { e: { t: { colors: [138, 43, 226, 1] } } } } },
								},
								colors: [0, 0, 255, 1],
							},
						},
					},
					i: { s: { q: { u: { e: { colors: [255, 228, 196, 1] } } } } },
					e: { i: { g: { e: { colors: [245, 245, 220, 1] } } } },
					u: {
						r: {
							l: {
								y: { w: { o: { o: { d: { colors: [222, 184, 135, 1] } } } } },
							},
						},
					},
					r: { o: { w: { n: { colors: [165, 42, 42, 1] } } } },
				},
				m: {
					i: {
						s: {
							t: {
								y: { r: { o: { s: { e: { colors: [255, 228, 225, 1] } } } } },
							},
						},
						n: {
							t: {
								c: { r: { e: { a: { m: { colors: [245, 255, 250, 1] } } } } },
							},
						},
						d: {
							n: {
								i: {
									g: {
										h: {
											t: {
												b: { l: { u: { e: { colors: [25, 25, 112, 1] } } } },
											},
										},
									},
								},
							},
						},
					},
					o: {
						c: {
							c: { a: { s: { i: { n: { colors: [255, 228, 181, 1] } } } } },
						},
					},
					a: {
						g: { e: { n: { t: { a: { colors: [255, 0, 255, 1] } } } } },
						r: { o: { o: { n: { colors: [128, 0, 0, 1] } } } },
					},
					e: {
						d: {
							i: {
								u: {
									m: {
										v: {
											i: {
												o: {
													l: {
														e: {
															t: {
																r: { e: { d: { colors: [199, 21, 133, 1] } } },
															},
														},
													},
												},
											},
										},
										o: {
											r: {
												c: { h: { i: { d: { colors: [186, 85, 211, 1] } } } },
											},
										},
										p: {
											u: {
												r: { p: { l: { e: { colors: [147, 112, 219, 1] } } } },
											},
										},
										s: {
											l: {
												a: {
													t: {
														e: {
															b: {
																l: { u: { e: { colors: [123, 104, 238, 1] } } },
															},
														},
													},
												},
											},
											e: {
												a: {
													g: {
														r: {
															e: { e: { n: { colors: [60, 179, 113, 1] } } },
														},
													},
												},
											},
											p: {
												r: {
													i: {
														n: {
															g: {
																g: {
																	r: {
																		e: {
																			e: { n: { colors: [0, 250, 154, 1] } },
																		},
																	},
																},
															},
														},
													},
												},
											},
										},
										a: {
											q: {
												u: {
													a: {
														m: {
															a: {
																r: {
																	i: {
																		n: { e: { colors: [102, 205, 170, 1] } },
																	},
																},
															},
														},
													},
												},
											},
										},
										t: {
											u: {
												r: {
													q: {
														u: {
															o: {
																i: { s: { e: { colors: [72, 209, 204, 1] } } },
															},
														},
													},
												},
											},
										},
										b: { l: { u: { e: { colors: [0, 0, 205, 1] } } } },
									},
								},
							},
						},
					},
				},
				n: {
					a: {
						v: {
							a: {
								j: {
									o: {
										w: {
											h: { i: { t: { e: { colors: [255, 222, 173, 1] } } } },
										},
									},
								},
							},
							y: { colors: [0, 0, 128, 1] },
						},
					},
				},
				g: {
					o: {
						l: {
							d: {
								colors: [255, 215, 0, 1],
								e: { n: { r: { o: { d: { colors: [218, 165, 32, 1] } } } } },
							},
						},
					},
					h: {
						o: {
							s: {
								t: {
									w: { h: { i: { t: { e: { colors: [248, 248, 255, 1] } } } } },
								},
							},
						},
					},
					a: {
						i: {
							n: {
								s: { b: { o: { r: { o: { colors: [220, 220, 220, 1] } } } } },
							},
						},
					},
					r: {
						e: {
							e: {
								n: {
									y: {
										e: {
											l: { l: { o: { w: { colors: [173, 255, 47, 1] } } } },
										},
									},
									colors: [0, 128, 0, 1],
								},
							},
							y: { colors: [128, 128, 128, 1] },
						},
						a: { y: { colors: [128, 128, 128, 1] } },
					},
				},
				o: {
					r: {
						a: {
							n: {
								g: {
									e: {
										colors: [255, 165, 0, 1],
										r: { e: { d: { colors: [255, 69, 0, 1] } } },
									},
								},
							},
						},
						c: { h: { i: { d: { colors: [218, 112, 214, 1] } } } },
					},
					l: {
						d: { l: { a: { c: { e: { colors: [253, 245, 230, 1] } } } } },
						i: {
							v: {
								e: {
									colors: [128, 128, 0, 1],
									d: { r: { a: { b: { colors: [107, 142, 35, 1] } } } },
								},
							},
						},
					},
				},
				d: {
					a: {
						r: {
							k: {
								o: {
									r: {
										a: { n: { g: { e: { colors: [255, 140, 0, 1] } } } },
										c: { h: { i: { d: { colors: [153, 50, 204, 1] } } } },
									},
									l: {
										i: {
											v: {
												e: {
													g: {
														r: {
															e: { e: { n: { colors: [85, 107, 47, 1] } } },
														},
													},
												},
											},
										},
									},
								},
								s: {
									a: { l: { m: { o: { n: { colors: [233, 150, 122, 1] } } } } },
									e: {
										a: {
											g: {
												r: { e: { e: { n: { colors: [143, 188, 143, 1] } } } },
											},
										},
									},
									l: {
										a: {
											t: {
												e: {
													b: { l: { u: { e: { colors: [72, 61, 139, 1] } } } },
													g: {
														r: {
															e: { y: { colors: [47, 79, 79, 1] } },
															a: { y: { colors: [47, 79, 79, 1] } },
														},
													},
												},
											},
										},
									},
								},
								k: { h: { a: { k: { i: { colors: [189, 183, 107, 1] } } } } },
								g: {
									o: {
										l: {
											d: {
												e: {
													n: { r: { o: { d: { colors: [184, 134, 11, 1] } } } },
												},
											},
										},
									},
									r: {
										e: {
											y: { colors: [169, 169, 169, 1] },
											e: { n: { colors: [0, 100, 0, 1] } },
										},
										a: { y: { colors: [169, 169, 169, 1] } },
									},
								},
								v: {
									i: { o: { l: { e: { t: { colors: [148, 0, 211, 1] } } } } },
								},
								m: {
									a: {
										g: { e: { n: { t: { a: { colors: [139, 0, 139, 1] } } } } },
									},
								},
								r: { e: { d: { colors: [139, 0, 0, 1] } } },
								t: {
									u: {
										r: {
											q: {
												u: {
													o: { i: { s: { e: { colors: [0, 206, 209, 1] } } } },
												},
											},
										},
									},
								},
								c: { y: { a: { n: { colors: [0, 139, 139, 1] } } } },
								b: { l: { u: { e: { colors: [0, 0, 139, 1] } } } },
							},
						},
					},
					e: {
						e: {
							p: {
								p: { i: { n: { k: { colors: [255, 20, 147, 1] } } } },
								s: {
									k: {
										y: { b: { l: { u: { e: { colors: [0, 191, 255, 1] } } } } },
									},
								},
							},
						},
					},
					i: {
						m: {
							g: {
								r: {
									e: { y: { colors: [105, 105, 105, 1] } },
									a: { y: { colors: [105, 105, 105, 1] } },
								},
							},
						},
					},
					o: {
						d: {
							g: {
								e: {
									r: { b: { l: { u: { e: { colors: [30, 144, 255, 1] } } } } },
								},
							},
						},
					},
				},
				h: {
					o: {
						t: { p: { i: { n: { k: { colors: [255, 105, 180, 1] } } } } },
						n: {
							e: { y: { d: { e: { w: { colors: [240, 255, 240, 1] } } } } },
						},
					},
				},
				t: {
					o: { m: { a: { t: { o: { colors: [255, 99, 71, 1] } } } } },
					h: { i: { s: { t: { l: { e: { colors: [216, 191, 216, 1] } } } } } },
					a: { n: { colors: [210, 180, 140, 1] } },
					u: {
						r: {
							q: {
								u: { o: { i: { s: { e: { colors: [64, 224, 208, 1] } } } } },
							},
						},
					},
					e: { a: { l: { colors: [0, 128, 128, 1] } } },
				},
				r: {
					e: {
						d: { colors: [255, 0, 0, 1] },
						b: {
							e: {
								c: {
									c: {
										a: {
											p: {
												u: {
													r: { p: { l: { e: { colors: [102, 51, 153, 1] } } } },
												},
											},
										},
									},
								},
							},
						},
					},
					o: {
						s: {
							y: {
								b: { r: { o: { w: { n: { colors: [188, 143, 143, 1] } } } } },
							},
						},
						y: {
							a: {
								l: { b: { l: { u: { e: { colors: [65, 105, 225, 1] } } } } },
							},
						},
					},
				},
				a: {
					n: {
						t: {
							i: {
								q: {
									u: {
										e: {
											w: {
												h: { i: { t: { e: { colors: [250, 235, 215, 1] } } } },
											},
										},
									},
								},
							},
						},
					},
					z: { u: { r: { e: { colors: [240, 255, 255, 1] } } } },
					l: {
						i: {
							c: {
								e: { b: { l: { u: { e: { colors: [240, 248, 255, 1] } } } } },
							},
						},
					},
					q: {
						u: {
							a: {
								m: {
									a: { r: { i: { n: { e: { colors: [127, 255, 212, 1] } } } } },
								},
								colors: [0, 255, 255, 1],
							},
						},
					},
				},
				k: { h: { a: { k: { i: { colors: [240, 230, 140, 1] } } } } },
				v: { i: { o: { l: { e: { t: { colors: [238, 130, 238, 1] } } } } } },
			});
		var N = L;
		const k = 1,
			P = 2,
			D = 3;
		class U {
			constructor() {
				(this.timeout = null),
					this.timeout_interval,
					(this.cache = []),
					(this.apply = this.apply.bind(this));
			}
			onFinish() {
				(this.timeout = null), (this.timeout_interval = 1e3), (this.cache = []);
			}
			clearTimeout() {
				null !== this.timeout && clearTimeout(this.timeout);
			}
			addTransitionItem(t, e) {
				this.clearTimeout(), this.cache.push([k, t, e]), this.setTimeout(1e3);
			}
			addWebGlImageItem(t, e, r) {
				this.clearTimeout(),
					this.cache.push([P, t, e, r]),
					this.setTimeout(500);
			}
			addWebGlBackgroundSizeItem(t, e, r) {
				this.clearTimeout(),
					this.cache.push([D, t, e, r]),
					this.setTimeout(500);
			}
			setTimeout(t) {
				(this.timeout_interval = Math.min(this.timeout_interval, t)),
					(this.timeout = setTimeout(this.apply, this.timeout_interval));
			}
			apply() {
				for (var t = this.cache.length; t-- > 0; ) {
					const e = this.cache[t];
					switch (e[0]) {
						case k:
							"0s" === e[1].style.transitionDuration &&
								(e[1].style.transitionDuration = e[2]);
							break;
						case P:
							e[1].style.setProperty(e[2].property, e[2].css_text, e[3]);
							break;
						case D:
							e[1].style.setProperty("background-size", e[2], e[3]);
					}
				}
				this.onFinish();
			}
		}
		var B = !1;
		const F = "night-eye-images-cache-v1";
		class G {
			static processBackgroundCSSString(t, e, r) {
				for (var n = 0, i = t.value; -1 !== (n = i.indexOf("url(", n)); ) {
					var s = i[(n += 4)];
					'"' !== s && "'" !== s ? (s = "") : ++n;
					var c = i.indexOf(s + ")", n);
					if (-1 === c) return;
					var l = i.substring(n, c),
						u = l;
					if ("blob:" === l.slice(0, 5)) {
						e({ css_text: null, property: t.property });
						continue;
					}
					if ("data:image" !== l.slice(0, 10)) {
						const r = l.lastIndexOf("?");
						-1 !== r && (l = l.substring(0, r)),
							(l = a.makeURL(
								l,
								o.PAGE_PROTOCOL,
								o.PAGE_HOSTNAME,
								o.PAGE_PORT,
								o.PAGE_URL
							));
						const n = this.images_cache.get(l);
						if (void 0 !== n) {
							e({
								css_text: null !== n ? i.replace(u, n) : null,
								property: t.property,
							});
							continue;
						}
					}
					const f = new j(i, l, u, t.property, e);
					r(),
						!0 !== f.isCachableUrl()
							? (G.images_queue.push(f), G.signalImage())
							: caches.open(F).then((t) => {
									t.match(l).then((e) => {
										if (void 0 !== e) {
											const r = e.headers.get("expires");
											if (null !== r && new Date(r) < new Date().getTime())
												return (
													t.delete(l),
													G.images_queue.push(f),
													void G.signalImage()
												);
											e.blob().then((t) => {
												switch (t.type) {
													case "application/json":
														new Response(t).text().then((t) => {
															const e = JSON.parse(t);
															G.invokeCallback(f, e);
														});
														break;
													case "text":
														new Response(t).text().then((t) => {
															G.invokeCallback(f, t);
														});
														break;
													default:
														G.invokeCallback(f, URL.createObjectURL(t));
												}
											});
										} else G.images_queue.push(f), G.signalImage();
									});
							  });
				}
			}
			static invokeCallback(t, e) {
				const r = null !== e ? t.css.replace(t.replace_url, e) : null;
				t.callback({ css_text: r, property: t.property });
			}
			static signalImage() {
				if (
					0 !== G.images_queue.length &&
					(G.webgl_contexts_size < G.WEBGL_MAX_CONTEXT_SIZE &&
						(G.webgl_free_contexts_queue.push(new H()),
						++G.webgl_contexts_size),
					0 !== G.webgl_free_contexts_queue.length)
				)
					for (var t, e = G.images_queue.length; e-- > 0; )
						if (
							((t = G.images_queue[e]), !1 === G.images_processing.has(t.url))
						) {
							var r = G.webgl_free_contexts_queue.pop();
							G.images_queue.splice(e, 1), r.process(t);
							break;
						}
			}
		}
		(G.images_queue = []),
			(G.images_cache = new Map()),
			(G.webgl_contexts_size = 0),
			(G.webgl_free_contexts_queue = []),
			(G.images_processing = new Set()),
			(G.WEBGL_MAX_CONTEXT_SIZE = 12),
			(G.AVERAGE_TEXTURE_SIZE = 1),
			(G.URL_BLOB = !0),
			(G.EDGE = !1);
		class j {
			constructor(t, e, r, n, o) {
				(this.css = t),
					(this.url = e),
					(this.replace_url = r),
					(this.property = n),
					(this.callback = o);
				const i = new Date();
				i.setDate(i.getDate() + 7), (this.header_expires = i.toString());
			}
			isCachableUrl() {
				return (
					0 === document.URL.indexOf("https://") &&
					"http" === this.url.slice(0, 4)
				);
			}
			getCacheHeaders() {
				return { headers: new Headers({ expires: this.header_expires }) };
			}
		}
		class H {
			constructor() {
				(this.canvas = null),
					(this.avg_pixel = new Uint8Array(4)),
					(this.vertices = new Float32Array([
						-1,
						1,
						0,
						-1,
						-1,
						0,
						1,
						-1,
						0,
						1,
						1,
						0,
					])),
					(this.textures = new Float32Array([0, 0, 0, 1, 1, 1, 1, 0])),
					(this.indices = new Uint16Array([0, 1, 2, 0, 2, 3])),
					(this.gl = null),
					(this.gl_flags = 0),
					(this.gl_frame_buffer = 0),
					(this.gl_texture_real = 0),
					(this.gl_rendered_texture = 0),
					(this.gl_depth_buffer = 0),
					(this.state_struct = null),
					(this.state_url = ""),
					(this.working_timer = null),
					(this.onBlobReady = this.onBlobReady.bind(this)),
					(this.onError = this.onError.bind(this));
			}
			init() {
				(this.canvas = document.createElement("canvas")),
					(this.gl = this.canvas.getContext("webgl", {
						antialias: !0,
						depth: !1,
						alpha: !0,
						preserveDrawingBuffer: !0,
					})),
					this.gl.enable(this.gl.BLEND),
					this.gl.enable(this.gl.DITHER),
					this.gl.blendFunc(this.gl.SRC_ALPHA, this.gl.ONE_MINUS_SRC_ALPHA),
					this.gl.clearColor(0, 0, 0, 0);
				const t = this.gl.createShader(this.gl.VERTEX_SHADER);
				this.gl.shaderSource(t, W), this.gl.compileShader(t);
				const e = this.gl.createShader(this.gl.FRAGMENT_SHADER);
				this.gl.shaderSource(e, z), this.gl.compileShader(e);
				var r = this.gl.createProgram();
				this.gl.attachShader(r, t),
					this.gl.attachShader(r, e),
					this.gl.linkProgram(r),
					this.gl.useProgram(r);
				const n = this.gl.createBuffer();
				this.gl.bindBuffer(this.gl.ARRAY_BUFFER, n),
					this.gl.bufferData(
						this.gl.ARRAY_BUFFER,
						this.vertices,
						this.gl.STATIC_DRAW
					);
				const o = this.gl.getAttribLocation(r, "a_coordinates");
				this.gl.vertexAttribPointer(o, 3, this.gl.FLOAT, !1, 0, 0),
					this.gl.enableVertexAttribArray(o);
				const i = this.gl.createBuffer();
				this.gl.bindBuffer(this.gl.ARRAY_BUFFER, i),
					this.gl.bufferData(
						this.gl.ARRAY_BUFFER,
						this.textures,
						this.gl.STATIC_DRAW
					);
				const s = this.gl.getAttribLocation(r, "a_texcoord");
				this.gl.vertexAttribPointer(s, 2, this.gl.FLOAT, !1, 0, 0),
					this.gl.enableVertexAttribArray(s);
				const a = this.gl.getUniformLocation(r, "u_texture");
				this.gl.uniform1i(a, 0),
					(this.gl_texture_real = this.gl.createTexture()),
					this.gl.bindTexture(this.gl.TEXTURE_2D, this.gl_texture_real),
					this.gl.texParameteri(
						this.gl.TEXTURE_2D,
						this.gl.TEXTURE_MAG_FILTER,
						this.gl.LINEAR
					),
					this.gl.texParameteri(
						this.gl.TEXTURE_2D,
						this.gl.TEXTURE_MIN_FILTER,
						this.gl.LINEAR
					),
					this.gl.texParameteri(
						this.gl.TEXTURE_2D,
						this.gl.TEXTURE_WRAP_S,
						this.gl.CLAMP_TO_EDGE
					),
					this.gl.texParameteri(
						this.gl.TEXTURE_2D,
						this.gl.TEXTURE_WRAP_T,
						this.gl.CLAMP_TO_EDGE
					),
					(this.gl_rendered_texture = this.gl.createTexture()),
					this.gl.bindTexture(this.gl.TEXTURE_2D, this.gl_rendered_texture),
					this.gl.texParameteri(
						this.gl.TEXTURE_2D,
						this.gl.TEXTURE_MAG_FILTER,
						this.gl.NEAREST
					),
					this.gl.texParameteri(
						this.gl.TEXTURE_2D,
						this.gl.TEXTURE_MIN_FILTER,
						this.gl.NEAREST
					),
					(this.gl_flags = this.gl.getUniformLocation(r, "u_flags"));
				const c = this.gl.createBuffer();
				if (
					(this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, c),
					this.gl.bufferData(
						this.gl.ELEMENT_ARRAY_BUFFER,
						this.indices,
						this.gl.STATIC_DRAW
					),
					(this.gl_frame_buffer = this.gl.createFramebuffer()),
					(this.gl_depth_buffer = this.gl.createRenderbuffer()),
					!0 === G.EDGE)
				) {
					var l = document.createElement("canvas");
					l.setAttribute("width", G.AVERAGE_TEXTURE_SIZE),
						l.setAttribute("height", G.AVERAGE_TEXTURE_SIZE),
						(this.small_canvas_ctx = l.getContext("2d"));
				}
			}
			process(t) {
				G.images_processing.add(t.url),
					(this.state_struct = t),
					(this.state_url = t.url),
					this.onProcessCheckCache();
			}
			onProcessCheckCache() {
				const t = G.images_cache.get(this.state_url);
				void 0 === t
					? !0 === this.state_struct.isCachableUrl()
						? caches.open(F).then((t) => {
								t.match(this.state_url).then((e) => {
									if (void 0 !== e) {
										const r = e.headers.get("expires");
										if (null !== r && new Date(r) < new Date().getTime())
											return (
												t.delete(this.state_url), void this.onProcessCacheMiss()
											);
										e.blob().then((t) => {
											switch (t.type) {
												case "application/json":
													new Response(t).text().then((t) => {
														const e = JSON.parse(t);
														this.finish(e);
													});
													break;
												case "text":
													new Response(t).text().then((t) => {
														this.finish(t);
													});
													break;
												default:
													this.finish(URL.createObjectURL(t));
											}
										});
									} else this.onProcessCacheMiss();
								});
						  })
						: this.onProcessCacheMiss()
					: this.finish(t);
			}
			onProcessCacheMiss() {
				if (!0 === this.state_struct.isCachableUrl()) {
					const t = {
							method: "GET",
							headers: new Headers(),
							mode: "cors",
							cache: "default",
							credentials: "include",
						},
						e = new Request(this.state_url);
					fetch(e, t)
						.then((t) => {
							if (void 0 === t) return void this.makeImgNode(this.state_url);
							const e = t.headers.get("expires");
							null !== e && (this.state_struct.header_expires = e),
								t.blob().then((t) => {
									this.makeImgNode(URL.createObjectURL(t));
								});
						})
						.catch(this.onError);
				} else this.makeImgNode(this.state_url);
			}
			makeImgNode(t) {
				const e = new Image();
				(e.crossOrigin = "anonymous"),
					(e.onerror = this.onError),
					(e.onload = this.onLoad.bind(this, e)),
					(e.src = t),
					!0 === B &&
						console.log(
							"request",
							this.state_url,
							G.images_cache.get(this.state_url)
						);
			}
			onError() {
				!0 === B && console.log("on error", this.state_url),
					G.images_cache.set(this.state_url, null),
					this.finish(null);
			}
			onLoad(t) {
				!0 === B && console.log("on load", this.state_url),
					null !== this.working_timer && clearTimeout(this.working_timer),
					null === this.canvas && this.init();
				var e = Math.min(512, H.gbp(t.width)),
					r = Math.min(512, H.gbp(t.height)),
					n = Math.min(e, r);
				if (
					(this.canvas.setAttribute("width", n),
					this.canvas.setAttribute("height", n),
					this.gl.viewport(0, 0, this.canvas.width, this.canvas.height),
					this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT),
					this.gl.uniform3f(this.gl_flags, 1, 0, 0),
					this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.gl_frame_buffer),
					this.gl.bindTexture(this.gl.TEXTURE_2D, this.gl_rendered_texture),
					this.gl.texParameteri(
						this.gl.TEXTURE_2D,
						this.gl.TEXTURE_MAG_FILTER,
						this.gl.NEAREST
					),
					this.gl.texParameteri(
						this.gl.TEXTURE_2D,
						this.gl.TEXTURE_MIN_FILTER,
						this.gl.NEAREST
					),
					this.gl.texImage2D(
						this.gl.TEXTURE_2D,
						0,
						this.gl.RGBA,
						n,
						n,
						0,
						this.gl.RGBA,
						this.gl.UNSIGNED_BYTE,
						new Uint8Array(1048576)
					),
					this.gl.framebufferTexture2D(
						this.gl.FRAMEBUFFER,
						this.gl.COLOR_ATTACHMENT0,
						this.gl.TEXTURE_2D,
						this.gl_rendered_texture,
						0
					),
					this.gl.bindTexture(this.gl.TEXTURE_2D, this.gl_texture_real),
					this.gl.texImage2D(
						this.gl.TEXTURE_2D,
						0,
						this.gl.RGBA,
						this.gl.RGBA,
						this.gl.UNSIGNED_BYTE,
						t
					),
					this.gl.bindRenderbuffer(this.gl.RENDERBUFFER, this.gl_depth_buffer),
					this.gl.renderbufferStorage(
						this.gl.RENDERBUFFER,
						this.gl.DEPTH_COMPONENT16,
						n,
						n
					),
					this.gl.framebufferRenderbuffer(
						this.gl.FRAMEBUFFER,
						this.gl.DEPTH_ATTACHMENT,
						this.gl.RENDERBUFFER,
						this.gl_depth_buffer
					),
					this.gl.drawElements(
						this.gl.TRIANGLES,
						this.indices.length,
						this.gl.UNSIGNED_SHORT,
						0
					),
					this.gl.bindTexture(this.gl.TEXTURE_2D, this.gl_rendered_texture),
					this.gl.texParameteri(
						this.gl.TEXTURE_2D,
						this.gl.TEXTURE_MAG_FILTER,
						this.gl.LINEAR
					),
					this.gl.texParameteri(
						this.gl.TEXTURE_2D,
						this.gl.TEXTURE_MIN_FILTER,
						this.gl.LINEAR_MIPMAP_LINEAR
					),
					this.gl.generateMipmap(this.gl.TEXTURE_2D),
					this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null),
					this.canvas.setAttribute("width", G.AVERAGE_TEXTURE_SIZE),
					this.canvas.setAttribute("height", G.AVERAGE_TEXTURE_SIZE),
					this.gl.viewport(0, 0, this.canvas.width, this.canvas.height),
					this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT),
					this.gl.drawElements(
						this.gl.TRIANGLES,
						this.indices.length,
						this.gl.UNSIGNED_SHORT,
						0
					),
					!0 === G.EDGE)
				) {
					var o = new Image();
					(o.onload = () => {
						this.small_canvas_ctx.drawImage(o, 0, 0),
							(this.avg_pixel = this.small_canvas_ctx.getImageData(
								0,
								0,
								G.AVERAGE_TEXTURE_SIZE,
								G.AVERAGE_TEXTURE_SIZE
							).data),
							this.onAverageFound(t);
					}),
						(o.src = this.canvas.toDataURL());
				} else
					this.gl.readPixels(
						0,
						0,
						1,
						1,
						this.gl.RGBA,
						this.gl.UNSIGNED_BYTE,
						this.avg_pixel
					),
						this.onAverageFound(t);
			}
			onAverageFound(t) {
				if (
					(!0 === B &&
						console.log("average for", this.state_url, this.avg_pixel),
					this.avg_pixel[3] < 90)
				) {
					if (
						(G.images_cache.set(this.state_url, null),
						!0 === this.state_struct.isCachableUrl())
					) {
						const { state_url: t, state_struct: e } = this;
						caches.open(F).then((r) => {
							const n = new Blob([null], { type: "application/json" });
							r.put(t, new Response(n, e.getCacheHeaders()));
						});
					}
					this.finish(null);
				} else {
					this.canvas.setAttribute("width", t.width),
						this.canvas.setAttribute("height", t.height),
						this.gl.viewport(0, 0, this.canvas.width, this.canvas.height),
						this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT),
						this.gl.bindTexture(this.gl.TEXTURE_2D, this.gl_texture_real);
					const e =
							Math.abs(this.avg_pixel[0] - this.avg_pixel[1]) < 2 &&
							Math.abs(this.avg_pixel[0] - this.avg_pixel[2]) < 2 &&
							Math.abs(this.avg_pixel[1] - this.avg_pixel[2]) < 2,
						r =
							this.avg_pixel[0] > 235 &&
							this.avg_pixel[1] > 235 &&
							this.avg_pixel[2] > 235;
					if (!1 === e && !1 === r) {
						const t =
								(this.avg_pixel[0] + this.avg_pixel[1] + this.avg_pixel[2]) / 3,
							e =
								t < 210 ? 0.8 : 0.0004074074 * t * t - 0.2040556 * t + 25.63093;
						!0 === B &&
							(console.log("total average", this.state_url, t),
							console.log(
								"dim",
								this.state_url,
								0.0004074074 * t * t - 0.2040556 * t + 25.63093
							)),
							this.gl.uniform3f(this.gl_flags, 0, 0, e);
					} else this.gl.uniform3f(this.gl_flags, 0, 1, 0);
					this.gl.drawElements(
						this.gl.TRIANGLES,
						this.indices.length,
						this.gl.UNSIGNED_SHORT,
						0
					),
						!0 === G.URL_BLOB
							? this.canvas.toBlob(this.onBlobReady)
							: this.onBase64URL();
				}
				this.working_timer = setTimeout(this.cleanup.bind(this), 8e3);
			}
			onBlobReady(t) {
				if (!0 === this.state_struct.isCachableUrl()) {
					const { state_url: e, state_struct: r } = this;
					caches.open(F).then((n) => {
						n.put(e, new Response(t, r.getCacheHeaders()));
					});
				}
				const e = URL.createObjectURL(t);
				this.onResult(e);
			}
			onBase64URL() {
				const t = this.canvas.toDataURL();
				if (!0 === this.state_struct.isCachableUrl()) {
					const { state_url: e, state_struct: r } = this;
					caches.open(F).then((n) => {
						const o = new Blob([t], { type: "text" });
						n.put(e, new Response(o, r.getCacheHeaders()));
					});
				}
				this.onResult(t);
			}
			onResult(t) {
				!0 === B && console.log("setting cache", this.state_url),
					G.images_cache.set(this.state_url, t),
					this.finish(t);
			}
			finish(t) {
				const e =
					null !== t
						? this.state_struct.css.replace(this.state_struct.replace_url, t)
						: null;
				this.state_struct.callback({
					css_text: e,
					property: this.state_struct.property,
				}),
					G.webgl_free_contexts_queue.push(this),
					G.images_processing.delete(this.state_url),
					G.signalImage();
			}
			cleanup() {
				null !== this.canvas &&
					(this.gl.getExtension("WEBGL_lose_context").loseContext(),
					(this.canvas = null),
					(this.gl = null),
					(this.gl_flags = 0),
					(this.gl_frame_buffer = 0),
					(this.gl_texture_real = 0),
					(this.gl_rendered_texture = 0),
					(this.gl_depth_buffer = 0));
			}
		}
		(H.gbp = (t) => {
			var e = t;
			return (
				(e |= e >> 1),
				(e |= e >> 2),
				(e |= e >> 4),
				(e |= e >> 8),
				(e |= e >> 16),
				(e &= (~e >> 1) ^ 2147483648)
			);
		}),
			HTMLCanvasElement.prototype.toBlob ||
				Object.defineProperty(HTMLCanvasElement.prototype, "toBlob", {
					value: function (t) {
						const e = this.toDataURL(),
							r = atob(e.substring(e.indexOf(",") + 1)),
							n = r.length,
							o = new Uint8Array(n);
						for (var i = r.length; i-- > 0; ) o[i] = r.charCodeAt(i);
						t(new Blob([o]));
					},
				});
		var W =
				"\n    attribute vec3 a_coordinates;\n    attribute vec2 a_texcoord;\n\n    varying vec2 v_texcoord;\n\n    void main() {\n        v_texcoord = a_texcoord;\n        gl_Position = vec4(a_coordinates, 1.0);\n    }\n",
			z =
				"\n    precision mediump float;\n\n    uniform vec3 u_flags;\n    uniform sampler2D u_texture;\n\n    varying vec2 v_texcoord;\n\n    vec3 rgb2hsl(vec3 color) {\n        vec3 hsl; // init to 0 to avoid warnings ? (and reverse if + remove first part)\n\n        float fmin = min(min(color.r, color.g), color.b); //Min. value of RGB\n        float fmax = max(max(color.r, color.g), color.b); //Max. value of RGB\n        float delta = fmax - fmin; //Delta RGB value\n\n        hsl.z = (fmax + fmin) / 2.0; // Luminance\n\n        if (delta == 0.0) //This is a gray, no chroma...\n        {\n            hsl.x = 0.0; // Hue\n            hsl.y = 0.0; // Saturation\n        } else //Chromatic data...\n        {\n            if (hsl.z < 0.5)\n                hsl.y = delta / (fmax + fmin); // Saturation\n            else\n                hsl.y = delta / (2.0 - fmax - fmin); // Saturation\n\n            float deltaR = (((fmax - color.r) / 6.0) + (delta / 2.0)) / delta;\n            float deltaG = (((fmax - color.g) / 6.0) + (delta / 2.0)) / delta;\n            float deltaB = (((fmax - color.b) / 6.0) + (delta / 2.0)) / delta;\n\n            if (color.r == fmax)\n                hsl.x = deltaB - deltaG; // Hue\n            else if (color.g == fmax)\n                hsl.x = (1.0 / 3.0) + deltaR - deltaB; // Hue\n            else if (color.b == fmax)\n                hsl.x = (2.0 / 3.0) + deltaG - deltaR; // Hue\n\n            if (hsl.x < 0.0)\n                hsl.x += 1.0; // Hue\n            else if (hsl.x > 1.0)\n                hsl.x -= 1.0; // Hue\n        }\n\n        return hsl;\n    }\n\n\n    float hue2rgb(float f1, float f2, float hue) {\n        if (hue < 0.0)\n            hue += 1.0;\n        else if (hue > 1.0)\n            hue -= 1.0;\n        float res;\n        if ((6.0 * hue) < 1.0)\n            res = f1 + (f2 - f1) * 6.0 * hue;\n        else if ((2.0 * hue) < 1.0)\n            res = f2;\n        else if ((3.0 * hue) < 2.0)\n            res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;\n        else\n            res = f1;\n        return res;\n    }\n\n    vec3 hsl2rgb(vec3 hsl) {\n        vec3 rgb;\n\n        if (hsl.y == 0.0) {\n            rgb = vec3(hsl.z); // Luminance\n        } else {\n            float f2;\n\n            if (hsl.z < 0.5)\n                f2 = hsl.z * (1.0 + hsl.y);\n            else\n                f2 = hsl.z + hsl.y - hsl.y * hsl.z;\n\n            float f1 = 2.0 * hsl.z - f2;\n\n            rgb.r = hue2rgb(f1, f2, hsl.x + (1.0/3.0));\n            rgb.g = hue2rgb(f1, f2, hsl.x);\n            rgb.b = hue2rgb(f1, f2, hsl.x - (1.0/3.0));\n        }\n        return rgb;\n    }\n\n    vec3 darken(vec3 hsl) {\n        if (hsl.x > (30.0 / 360.0) && hsl.x < (90.0 / 360.0) && hsl.y > (40.0 / 100.0) && hsl.z < (70.0 / 100.0)) {\n            hsl.x = (219.0 / 360.0);\n            hsl.y = (63.0 / 100.0);\n            hsl.z = (41.0 / 100.0);\n        }\n\n        if (hsl.y > (60.0 / 100.0)) {\n            hsl.y = (60.0 / 100.0);\n        }\n\n        if (hsl.z > (60.0 / 100.0)) {\n            hsl.z = 0.1 + (1.0 - hsl.z);\n        }\n\n        return hsl;\n    }\n\n    void main() {\n        vec4 color_rgba = texture2D(u_texture, v_texcoord);\n        vec3 color_hsl = rgb2hsl(color_rgba.xyz);\n        vec3 color_rgb = hsl2rgb(darken(color_hsl));\n        gl_FragColor = u_flags.y * vec4(color_rgb, color_rgba.w) + u_flags.x * color_rgba + vec4(u_flags.z, u_flags.z, u_flags.z, 1) * color_rgba;\n    }\n",
			Y = r(68),
			V = r.n(Y);
		const q = "nighteyecss",
			K = "nighteyestr";
		class X {
			constructor(t) {
				(this.styleConverter = t),
					(this.isForbidden =
						window.self !== window.top ||
						"docs.google.com" === o.URL ||
						"dropbox.com" === o.URL),
					(this.storage = {}),
					(this.mapper = {}),
					(this.uniqueRulesStorage = {}),
					(this.savedSelectorsCount = 0),
					(this.storageContentText = ""),
					(this.counter = 0);
			}
			loadParsedCSS() {
				if (!this.isForbidden) {
					var t = localStorage.getItem(q),
						e = V.a.decompressFromUTF16(t);
					if (null !== e && e.length > 0) {
						var r = E.makeParsedStyleNode();
						(r.innerHTML = e),
							r.addEventListener(
								"load",
								() => {
									setTimeout(() => {
										document.documentElement.setAttribute("nighteye", "active");
									}, 0);
								},
								!1
							);
						var n = setInterval(() => {
								void 0 !== document.head &&
									null !== document.head &&
									(clearInterval(n),
									document.head.insertBefore(r, document.head.firstChild));
							}),
							o = E.makeParsedStyleNode();
						(o.innerHTML = r.innerHTML), document.head.appendChild(o);
						var i = E.makeParsedStyleNode();
						i.innerHTML = r.innerHTML;
						var s = setInterval(() => {
							void 0 !== document.body &&
								null !== document.body &&
								(clearInterval(s), document.body.appendChild(i));
						});
						"".length > 0 &&
							(r.setAttribute("", ""),
							o.setAttribute("", ""),
							i.setAttribute("", ""));
					}
					var a = localStorage.getItem(K),
						c = V.a.decompressFromUTF16(a);
					null !== c &&
						c.length > 0 &&
						(this.uniqueRulesStorage = JSON.parse(c)),
						this.startSavingChecker();
				}
			}
			addRule(t, e, r, o, i) {
				if (!(this.isForbidden || r.indexOf("url") > -1 || r === n.EMPTY)) {
					if ((++this.counter, i))
						if (t.id) (t.selectorText = "#" + t.id), (o = "important");
						else if (t.className) {
							for (var s = "", a = t; null != a; )
								if (a.className) {
									var c = "";
									if ("svg" === a.tagName) c = a.className.baseVal;
									else
										for (var l = 0; l < a.classList.length; ++l)
											c += "." + a.classList[l];
									(s = "" === s ? c : c + " " + s), (a = a.parentNode);
								} else "" === s && (s = a.tagNam), (a = a.parentNode);
							(t.selectorText = s), (o = "important");
						} else {
							if ("HTML" !== t.nodeName && "BODY" !== t.nodeName)
								return void t.nodeName;
							(t.selectorText = t.nodeName), (o = "important");
						}
					var u = e + ":" + r + (0 === o.length ? "" : " !important") + ";",
						f = Number.MAX_SAFE_INTEGER;
					i ||
						(f =
							null === t.parentStyleSheet.ownerNode
								? 1
								: t.parentStyleSheet.ownerNode.tgCSSOrderIndex),
						void 0 === this.mapper[f] && (this.mapper[f] = {});
					var h = "-";
					if (
						(null !== t.parentRule &&
							void 0 !== t.parentRule &&
							void 0 !== t.parentRule.media &&
							(h = t.parentRule.media.mediaText),
						void 0 === this.mapper[f][h] && (this.mapper[f][h] = {}),
						void 0 === this.storage[h] && (this.storage[h] = []),
						void 0 === this.mapper[f][h][t.selectorText])
					) {
						var d = { nm: t.selectorText, rls: [] };
						this.storage[h].push(d), (this.mapper[f][h][t.selectorText] = d);
					}
					if (i) {
						var p = this.mapper[f][h][t.selectorText].rls;
						for (var g in p) if (g === u) return;
					}
					this.mapper[f][h][t.selectorText].rls.push(u);
				}
			}
			removeMissingInlineStyleRules(t) {
				var e = t;
				if (e.id) e.selectorText = "#" + e.id;
				else if (e.className) e.selectorText = "." + e.className;
				else {
					if ("HTML" !== e.nodeName && "BODY" !== e.nodeName)
						return void e.nodeName;
					e.selectorText = e.nodeName;
				}
				var r = Number.MAX_SAFE_INTEGER;
				if (void 0 !== this.mapper[r]) {
					var n = "-";
					if (
						(null !== e.parentRule &&
							void 0 !== e.parentRule &&
							void 0 !== e.parentRule.media &&
							(n = e.parentRule.media.mediaText),
						void 0 !== this.mapper[r][n][e.selectorText])
					)
						for (
							var o = e.getAttribute("style").split(";"), i = 0;
							i < o.length;
							++i
						) {
							var s = o[i].trim();
							if ("" !== s) {
								for (
									var a = this.mapper[r][n][e.selectorText].rls, c = -1, l = 0;
									l < a.length;
									++l
								)
									if (((c = l), s === a[l])) {
										c = -1;
										break;
									}
								c > -1 && a.splice(c, 1);
							}
						}
				}
			}
			startSavingChecker() {
				this.isForbidden ||
					setInterval(() => {
						this.savedSelectorsCount !== Object.keys(this.mapper).length &&
							this.save();
					}, 2e3);
			}
			save() {
				if (!this.isForbidden) {
					var t, e, r, n, o, i, s, a;
					for (var c in this.mapper) {
						var l = this.mapper[c];
						for (t in l) {
							var u = l[t];
							void 0 === this.uniqueRulesStorage[t] &&
								(this.uniqueRulesStorage[t] = []);
							var f = this.uniqueRulesStorage[t];
							for (var h in u) {
								var d = null;
								for (i = 0; i < f.length; ++i)
									if (h === (r = f[i]).nm) {
										d = r;
										break;
									}
								null === d &&
									((d = { nm: h, rls: [] }),
									this.uniqueRulesStorage[t].push(d));
								var p = d.rls,
									g = u[h].rls;
								for (s = 0; s < g.length; ++s) {
									var m = g[s],
										v = !1;
									for (a = 0; a < p.length; ++a)
										if (m === p[a]) {
											v = !0;
											break;
										}
									v || p.push(m);
								}
							}
						}
					}
					for (t in this.uniqueRulesStorage) {
						e = this.uniqueRulesStorage[t];
						var y = "-" !== t;
						for (o in (y && (this.storageContentText += "@media " + t + "{"),
						e)) {
							for (
								n = (r = e[o]).rls,
									this.storageContentText += r.nm + "{",
									s = 0;
								s < n.length;
								++s
							)
								this.storageContentText += n[s];
							this.storageContentText += "}";
						}
						y && (this.storageContentText += "}");
					}
					this.checkAndGetCustomWebsiteCSS() ||
						this.compressAndStoreToLocalStorage();
				}
			}
			checkAndGetCustomWebsiteCSS() {
				for (var t = 0; t < document.styleSheets.length; ++t) {
					var e = document.styleSheets[t].ownerNode;
					if (
						e.customWebsiteCSS &&
						-1 === e.href.lastIndexOf("custom-styles.css") &&
						-1 === e.href.lastIndexOf("scroll.css")
					) {
						if (void 0 === e.href) return;
						return (
							this.styleConverter
								.executeDownloadCSS(e.href)
								.then((t) => {
									(this.storageContentText += t),
										this.compressAndStoreToLocalStorage();
								})
								.catch((t) => {
									0;
								}),
							!0
						);
					}
				}
				return !1;
			}
			compressAndStoreToLocalStorage() {
				var t = V.a.compressToUTF16(this.storageContentText),
					e = V.a.compressToUTF16(JSON.stringify(this.uniqueRulesStorage));
				if (t.length > 2e6 || e.length > 2e6) X.erase();
				else
					try {
						localStorage.setItem(q, t),
							localStorage.setItem(K, e),
							(this.savedSelectorsCount = Object.keys(this.mapper).length);
					} catch (t) {
						X.erase();
					}
			}
			static erase() {
				localStorage.removeItem(q), localStorage.removeItem(K);
			}
		}
		class Z {
			constructor(t) {
				(this.colorProcessor = new N()),
					(this.local_settings = t),
					(this.converted = !1),
					(this.cache_bg = new I(1 << 22)),
					(this.cache_fr = new I(1 << 22)),
					(this.startObserver = null),
					(this.stopObserver = null),
					(this.convertBackground = this.colorProcessor.convertBackgroundColorString.bind(
						this.colorProcessor
					)),
					(this.convertForeground = this.colorProcessor.convertForegroundColorString.bind(
						this.colorProcessor
					)),
					(this.style_apply_cache = new U()),
					(this.googleDocsNodeMaps = []),
					(this.isInitializationNotFinished = !0),
					(this.customStorage = new X(this)),
					(this.cssOrderCounter = 0);
			}
			init() {
				for (var t = Z.BACKGROUND_PROPERTIES.length; t-- > 0; )
					Z.BACKGROUND_PROPERTIES_SET.add(Z.BACKGROUND_PROPERTIES[t]);
				for (var e = Z.FOREGROUND_PROPERTIES.length; e-- > 0; )
					Z.FOREGROUND_PROPERTIES_SET.add(Z.FOREGROUND_PROPERTIES[e]);
			}
			convert() {
				if (!0 !== this.converted) {
					(this.converted = !0),
						o.TURBO_CACHE_ENABLED && window.self === window.top
							? this.customStorage.loadParsedCSS()
							: X.erase(),
						-1 === navigator.userAgent.indexOf("Firefox") &&
							this.convertProcedure(!1);
					var t = 0,
						e = setInterval(() => {
							"vnexpress.net" !== o.URL && this.convertProcedure(!1),
								"complete" === document.readyState &&
									(clearInterval(e),
									this.checkForDynamicChanges(),
									("bugs.chromium.org" !== o.URL &&
										"cloud.google.com" !== o.URL &&
										"bbc.com" !== o.URL) ||
										(this.checkAndConvertDomShadowElements(document.body),
										setTimeout(() => {
											this.checkAndConvertDomShadowElements(document.body);
										}, 2e3))),
								++t > 50 && (clearInterval(e), this.checkForDynamicChanges());
						}, 300);
				} else
					console.error(
						"StyleConverted.convert() must be invoked only once. All other invoked must go through MutationObserver for specific node"
					);
			}
			checkForDynamicChanges() {
				setInterval(() => {
					this.convertStyleNodes(!1), (this.isInitializationNotFinished = !1);
				}, 1e3);
			}
			convertProcedure(t) {
				this.convertStyleNodes(t),
					this.convertIFrames(),
					this.convertLinkImports(),
					this.convertInlineStyles(),
					this.convertImgNodes();
			}
			convertStyleNodes(t) {
				for (var e = document.styleSheets, r = 0; r < e.length; ++r) {
					var n = e[r];
					(null !== n.ownerNode &&
						(t && (n.ownerNode.tgParsed = !1),
						"alternate stylesheet" === n.ownerNode.getAttribute("rel"))) ||
						this.convertStyleSheet(n, !1);
				}
			}
			convertStyleSheets(t) {
				for (var e = 0; e < t.length; ++e) {
					var r = t[e];
					this.convertStyleSheet(r, !1);
				}
			}
			convertStyleSheetFromMutator(t, e, r) {
				this.convertStyleSheet(t, e);
				var n = t.ownerNode;
				r && this.downloadCSS(n.href, n, n.media, 0);
			}
			convertIFrames() {
				for (
					var t = document.querySelectorAll("iframe:not([src])"), e = t.length;
					e-- > 0;

				)
					this.convertIFrame(t[e]);
				for (
					var r = (t = document.querySelectorAll('iframe[src^="javascript"]'))
						.length;
					r-- > 0;

				)
					this.convertIFrame(t[r]);
			}
			convertLinkImports() {
				for (
					var t = document.querySelectorAll('link[rel="import"]'), e = 0;
					e < t.length;
					++e
				) {
					var r = t[e],
						n = r.import;
					null != n
						? "complete" === n.readyState
							? this.convertStyleSheets(n.styleSheets)
							: this.addEventListenerLinkImportNode(r)
						: this.addEventListenerLinkImportNode(r);
				}
			}
			addEventListenerLinkImportNode(t) {
				t.addEventListener("load", () => {
					this.convertStyleSheets(t.import.styleSheets);
				});
			}
			convertInlineStyles() {
				for (
					var t = document.querySelectorAll(
							"[style],[fill],[stroke],[bgcolor]"
						),
						e = 0;
					e < t.length;
					++e
				)
					this.convertInlineStyle(t[e]);
			}
			convertImgNodes() {
				for (var t = document.querySelectorAll("img"), e = 0; e < t.length; ++e)
					this.convertImgNode(t[e]);
			}
			convertStyleSheet(t, e) {
				var r = t.ownerNode;
				if (
					(t.tgIsImported && (r = { tgParsed: !1, hasAttribute: () => {} }),
					void 0 !== r && !r.tgIgnore && "print" !== r.media)
				) {
					try {
						var n = r.tgParsedRules;
						void 0 !== n &&
							parseInt(n) !== t.cssRules.length &&
							(r.tgParsed = !1);
					} catch (t) {}
					if (
						!(
							(!1 === e && r.tgParsed) ||
							("STYLE" === r.tagName &&
								0 === r.innerHTML.length &&
								null === r.cssRules)
						)
					) {
						void 0 === r.tgCSSOrderIndex &&
							(r.tgCSSOrderIndex = ++this.cssOrderCounter),
							(r.tgParsed = !0);
						try {
							r.tgParsedRules = t.cssRules.length;
						} catch (t) {}
						if ("LINK" === r.tagName) {
							if (
								"data:text/css" === r.href.substring(0, "data:text/css".length)
							)
								try {
									null !== t.cssRules && this.processCSSRules(t.cssRules, r);
								} catch (t) {}
							else {
								if (this.isHrefCssFontUrl(r.href)) return;
								try {
									if (r.hasAttribute("ng-href") || "main-style-css" === r.id)
										return void this.downloadCSS(r.href, r, r.media, 0);
									this.processCSSRules(t.cssRules, r);
								} catch (t) {
									this.downloadCSS(r.href, r, r.media, 0);
								}
							}
						} else {
							if (r.hasAttribute("data-styled"))
								return (
									this.duplicateAndParseStyleElement(r, r.media, t.cssRules),
									void r.removeAttribute("data-styled")
								);
							if (r.hasAttribute("is"))
								return void this.duplicateParseAndDeleteStyleElement(
									r,
									r.media,
									t.cssRules
								);
							if (r.hasAttribute("data-emotion-css"))
								return void this.duplicateParseAndDeleteStyleElement(
									r,
									r.media,
									t.cssRules
								);
							this.processCSSRules(t.cssRules, r);
						}
					}
				}
			}
			convertInlineStyle(t) {
				if (void 0 !== t.getAttribute) {
					var e = t.getAttribute("style");
					if (null !== e) {
						o.TURBO_CACHE_ENABLED &&
							this.customStorage.removeMissingInlineStyleRules(t);
						for (
							var r = [], n = [], i = e.split(";"), s = i.length;
							s-- > 0;

						) {
							var a = i[s].indexOf(":");
							if (-1 !== a) {
								var c = [i[s].substring(0, a), i[s].substring(a + 1)][0].trim();
								!0 === Z.BACKGROUND_PROPERTIES_SET.has(c)
									? r.push(c)
									: !0 === Z.FOREGROUND_PROPERTIES_SET.has(c) && n.push(c);
							}
						}
						"docs.google.com" === o.URL
							? "kix-wordhtmlgenerator-word-node" === t.className
								? (this.parseCSSRule(t, n, r),
								  null == this.googleDocsNodeMaps[e] &&
										((this.googleDocsNodeMaps[e] = 1),
										this.convertGoogleDocsNode(t, e)))
								: t.classList.contains("color") ||
								  ("SPAN" === t.nodeName &&
										null !== t.parentNode &&
										void 0 !== t.parentNode.className &&
										t.parentNode.className.indexOf("editable") > -1) ||
								  this.parseCSSRule(t, n, r)
							: this.parseCSSRule(t, n, r);
					}
					for (
						var l = ["text", "link", "vlink", "alink"], u = l.length;
						u-- > 0;

					)
						t.removeAttribute(l[u]);
					var f = t.getAttribute("fill");
					null !== f &&
						((t.tgIgnore = !0),
						"currentColor" !== f &&
							-1 === f.indexOf("url") &&
							t.setAttribute(
								"fill",
								this.colorProcessor.convertBackgroundColorString(f)
							));
					var h = t.getAttribute("stroke");
					null !== h &&
						((t.tgIgnore = !0),
						"none" !== h &&
							t.setAttribute(
								"stroke",
								this.colorProcessor.convertForegroundColorString(h)
							));
					var d = t.getAttribute("bgcolor");
					if (null !== d) {
						t.tgIgnore = !0;
						var p = this.colorProcessor.convertBackgroundColorString(d);
						d === p && (p = "#1a1a1a"), (t.style.backgroundColor = p);
					}
				}
			}
			convertImgNode(t) {}
			convertIFrame(t) {
				var e = t.getAttribute("src");
				if (null === e || 0 === e.indexOf("javascript"))
					try {
						var r = t.contentWindow.document;
						(r.body.style.backgroundColor = "#292929"),
							(r.body.style.color = "#cecece");
						var n = 0,
							i = setInterval(() => {
								if (
									null !== (r = t.contentWindow.document).body &&
									r.body.children.length > 0 &&
									r.styleSheets.length > 2 &&
									(clearInterval(i),
									this.convertStyleSheets(r.styleSheets),
									(t.style.filter = "none"),
									"dl.acm.org" === o.URL)
								) {
									var e = E.makeParsedStyleNode();
									(e.innerHTML =
										"#page_body img { mix-blend-mode: color-burn; }"),
										r.head.appendChild(e);
								}
								++n > 30 && ((t.style.filter = "none"), clearInterval(i));
							}, 100);
					} catch (t) {}
			}
			downloadCSS(t, e, r, n) {
				"" !== t &&
					(++o.isInitialConvertedCounter,
					(t = a.makeURL(t)),
					this.executeDownloadCSS(t)
						.then((o) => {
							this.processResponseDownloadCSS(o, e, r, t, n);
						})
						.catch((t) => {
							--o.isInitialConvertedCounter,
								console.warn("Implement fetch from background - ", t);
						}));
			}
			executeDownloadCSS(t) {
				return new Promise((e, r) => {
					var n = new XMLHttpRequest();
					n.open("get", t, !0),
						(n.onerror = (t, e) => {
							r();
						}),
						(n.onreadystatechange = () => {
							4 === n.readyState &&
								(200 === n.status ? e(n.responseText) : r());
						}),
						n.send();
				});
			}
			processResponseDownloadCSS(t, e, r, n, i) {
				var s = i === o.IMPORT_CSS_INDEX_LAST_POSITION,
					a = this.convertImportUrls(n, t, s);
				var c = this.addStyleNodeWithCSSText(n, a, e, r, i);
				this.convertStyleSheet(c.sheet, !1), --o.isInitialConvertedCounter;
			}
			duplicateAndParseStyleElement(t, e, r) {
				for (
					var n = this.addStyleNodeWithCSSText("", "", t, e, 0),
						o = n.sheet,
						i = 0;
					i < r.length;
					i++
				) {
					var s = r[i];
					o.insertRule(s.cssText, o.cssRules.length);
				}
				setTimeout(() => {
					this.convertStyleSheet(n.sheet, !1);
				}, 0);
			}
			duplicateParseAndDeleteStyleElement(t, e, r) {
				var n = this.addStyleNodeWithCSSText("", "", t, e, 0);
				(n.innerHTML = t.innerHTML),
					document.head.appendChild(n),
					setTimeout(() => {
						this.convertStyleSheet(n.sheet, !1);
					}, 0),
					t.parentNode.removeChild(t),
					(t.disabled = !0);
			}
			checkAndParseImportURL(t) {
				var e = t.parentStyleSheet.href,
					r = t.parentStyleSheet.ownerNode.href,
					n = t.href;
				return n.indexOf("://") > -1
					? n
					: "/" === n[0] && "/" === n[1]
					? n
					: "/" === n[0]
					? o.PAGE_PROTOCOL + "//" + o.PAGE_HOSTNAME + n
					: null !== e
					? e.substring(0, e.lastIndexOf("/") + 1) + n
					: null !== r
					? void 0 === r
						? new URL(n, window.location.href).href
						: r.substring(0, r.lastIndexOf("/") + 1) + n
					: o.PAGE_URL + "/" + n;
			}
			convertImportUrls(t, e, r) {
				for (var n = "", o = 0; ; ) {
					var i = e.indexOf("@import", o);
					if (-1 === i) {
						n += e.substring(o);
						break;
					}
					for (
						var s = !0, a = e.length > i + 10 ? i + 10 : e.length, c = i;
						c < a;
						++c
					) {
						var l = e[c];
						if ("u" === l) break;
						if (" " !== l) {
							s = !1;
							break;
						}
					}
					if (s) {
						var u = e.indexOf(";", i);
						if (-1 === u) {
							n += e.substring(o);
							break;
						}
						if ((++u, (n += e.substring(o, i)), (o = u), !r))
							if (i > -1) {
								for (
									var f = i + parseInt((u - i) / 2), h = null, d = f;
									d >= i;
									--d
								) {
									var p = e[d];
									if ("'" === p || '"' === p || "(" === p) {
										(h = "(" === p ? ")" : p), (i = d);
										break;
									}
								}
								for (var g = u; g > f; --g)
									if (e[g] === h) {
										u = g;
										break;
									}
								var m = e.substring(i + 1, u).trim();
								n += "@import url(" + new URL(m, t).href + ");";
							} else n += e.substring(i, u);
					} else {
						var v = i + "@import".length;
						(n += e.substring(o, v)), (o = v);
					}
				}
				return n;
			}
			convertRelativeUrlsToAbsolute(t, e) {
				var r = "",
					n = document.createElement("a");
				n.href = e;
				for (
					var o =
							n.protocol +
							"//" +
							n.host +
							n.pathname.split("/").slice(0, -1).join("/"),
						i = "url(".length,
						s = 0;
					;

				) {
					var a = t.indexOf("url(", s);
					if (-1 === a) {
						r += t.substring(s);
						break;
					}
					var c = t.indexOf(")", a + i);
					if (-1 === c) {
						r += t.substring(s);
						break;
					}
					if (
						(++c,
						(r += t.substring(s, a)),
						(s = c),
						"." !== t.substr(a + i + 1, 1))
					)
						r += t.substring(a, c);
					else {
						var l = a + i,
							u = t.substr(l, 1);
						("'" !== u && '"' !== u) || (l += 1);
						var f = c - 2,
							h = t.substr(f, 1);
						"'" !== h && '"' !== h && (f = c - 1),
							(r += 'url("' + o + "/" + t.substring(l, f) + '")');
					}
				}
				return r;
			}
			convertURLs(t, e, r) {
				null == e && (e = o.PAGE_URL);
				var n = e.substring(0, e.lastIndexOf("/") + 1),
					i = "";
				if (r) {
					for (var s = 0; ; ) {
						var a = t.indexOf("@import", s);
						if (-1 === a) {
							i += t.substring(s);
							break;
						}
						var c = t.indexOf(";", a);
						if (-1 === c) {
							i += t.substring(s);
							break;
						}
						if (
							(++c,
							(i += t.substring(s, a)),
							(s = c),
							"url(" !== t.substr(a + 8, 4))
						) {
							var l = a + 8,
								u = t[l],
								f = t.indexOf(u, l + 1);
							if (-1 !== f) {
								(i += "@import url(" + u + t.substring(l + 1, f) + u + ")"),
									(i += t.substring(f + 1, c));
								continue;
							}
						}
						i += t.substring(a, c);
					}
					t = i;
				}
				i = "";
				for (let e = 0; ; ) {
					var h = t.indexOf("url(", e);
					if (-1 === h) {
						i += t.substring(e);
						break;
					}
					var d = t.indexOf(")", h);
					if (-1 === d) {
						i += t.substring(e);
						break;
					}
					++d, (i += t.substring(e, h)), (e = d);
					var p = h + 4;
					("'" !== t[p] && '"' !== t[p]) || ++p;
					var g = d - 2;
					("'" !== t[g] && '"' !== t[g]) || --g;
					var m = t.substring(p, g + 1);
					if ("#" !== m[0])
						if (
							m.length > 5 &&
							"d" === m[0] &&
							"a" === m[1] &&
							"t" === m[2] &&
							"a" === m[3] &&
							":" === m[4]
						)
							i += t.substring(h, d);
						else if (-1 === m.indexOf("://")) {
							if (m.length <= 2 || "/" != m[0] || "/" != m[1])
								if ("/" === m[0]) {
									var v = n.indexOf("/", n.indexOf("://") + 3);
									m = (-1 === v ? n : n.substr(0, v)) + m;
								} else m = n + m;
							i += "url(" + (m = m.replace(/ /g, "%20")) + ")";
						} else i += t.substring(h, d);
					else i += t.substring(h, d);
				}
				return i;
			}
			addStyleNodeWithCSSText(t, e, r, n, o) {
				var i = document.createElement("style");
				(i.tgParsed = !1),
					(i.tgIgnore = !0),
					"" !== n && (i.media = n),
					window.navigator.userAgent.indexOf("Edge") > -1 &&
						"only x" === n &&
						(i.media = "all"),
					"" !== t && (i.href = t),
					(i.innerHTML = e),
					(i.tgImportCSSIndex = o),
					(i.tgCSSOrderIndex = r.tgCSSOrderIndex);
				for (
					var s = r, a = s;
					null !== (a = a.nextSibling) &&
					void 0 !== a.tgImportCSSIndex &&
					!(o < a.tgImportCSSIndex);

				)
					s = a;
				return E.insertAfter(i, s), i;
			}
			processCSSRules(t, e) {
				for (var r = !1, n = 0; n < t.length; ++n) {
					var i = t[n];
					switch (i.type) {
						case CSSRule.STYLE_RULE:
							this.parseCSSRule(i);
							break;
						case CSSRule.MEDIA_RULE:
						case CSSRule.SUPPORTS_RULE:
							this.processCSSRules(i.cssRules, e);
							break;
						case CSSRule.IMPORT_RULE:
							if (this.isHrefCssFontUrl(i.href)) continue;
							try {
								null !== i.styleSheet.cssRules &&
									((i.styleSheet.tgIsImported = !0),
									this.convertStyleSheet(i.styleSheet, !1));
							} catch (t) {
								r = !0;
								var s = this.checkAndParseImportURL(i);
								(e.tgImportCSSCounter =
									void 0 === e.tgImportCSSCounter ? 1 : ++e.tgImportCSSCounter),
									this.downloadCSS(
										s,
										e,
										"" === i.media.mediaText ? e.media : i.media.mediaText,
										e.tgImportCSSCounter
									);
							}
					}
				}
				r &&
					"LINK" === e.nodeName &&
					this.downloadCSS(
						e.href,
						e,
						e.media,
						o.IMPORT_CSS_INDEX_LAST_POSITION
					);
			}
			parseCSSRule(t, e, r) {
				if (
					void 0 !== t.parentRule &&
					null !== t.parentRule &&
					"print" === t.parentRule.conditionText
				)
					return;
				const n = t.style.transitionDuration,
					o = "" !== n;
				!0 === o && (t.style.transitionDuration = "0s"),
					void 0 === e && (e = Z.FOREGROUND_PROPERTIES),
					void 0 === r && (r = Z.BACKGROUND_PROPERTIES),
					(0 === e.length && 0 === r.length) ||
						(this.parseCSSVariables(t),
						this.parse(t, r, this.convertBackground, this.cache_bg),
						this.parse(t, e, this.convertForeground, this.cache_fr),
						!0 === o && this.style_apply_cache.addTransitionItem(t, n));
			}
			parseCSSVariables(t) {
				void 0 === t.tgIgnoreVariableCounter && (t.tgIgnoreVariableCounter = 0);
				for (var e, r = [], n = t.style.length; n-- > 0; )
					(e = t.style[n]).length > 2 &&
						"-" === e[0] &&
						"-" === e[1] &&
						r.push(n);
				for (var o = "", i = "", s = r.length; s-- > 0; )
					if (
						void 0 !== (i = t.style[r[s]]) &&
						-1 === i.indexOf("-night-eye")
					) {
						var a = (o = this.extractCSSVariable(t, i)).charAt(0),
							c = !1;
						a >= "0" &&
							a <= "9" &&
							o.indexOf(",") > -1 &&
							((o = "rgb(" + o + ")"), (c = !0));
						var l = this.colorProcessor.convertForegroundColorString(o),
							u = this.colorProcessor.convertBackgroundColorString(o);
						c &&
							((l = this.parseAndGetRGBFromHSLA(l)),
							(u = this.parseAndGetRGBFromHSLA(u)));
						try {
							++t.tgIgnoreVariableCounter,
								t.style.setProperty(i, l),
								++t.tgIgnoreVariableCounter,
								t.style.setProperty(i + "-night-eye", u);
						} catch (t) {
							console.warn(t);
						}
					}
			}
			extractCSSVariable(t, e) {
				var r = t.style.getPropertyValue(e);
				return (-1 !== r.indexOf("var(--")
					? this.parseCSSVariable(r, t)
					: r
				).trim();
			}
			parseCSSVariable(t, e) {
				for (var r = "", n = "var(".length, o = 0; ; ) {
					var i = t.indexOf("var(", o);
					if (-1 === i) {
						r += t.substring(o);
						break;
					}
					var s = t.indexOf(")", i + n);
					if (-1 === s) {
						r += t.substring(o);
						break;
					}
					var a = t.indexOf(",", i + n);
					-1 !== a && s > a && (s = a), (r += t.substring(o, i)), (o = s);
					var c = i + n,
						l = s,
						u = t.substring(c, l),
						f = e.style.getPropertyValue(u),
						h = t.substring(l, l + 1);
					if ("" === f && "," === h) return t;
					-1 !== f.indexOf("var(--")
						? (r += this.parseCSSVariable(f, e))
						: (r += f);
				}
				return r;
			}
			markCSSVariable(t, e, r, n) {
				var o = "",
					i = "var(".length;
				for (var s = !1, a = 0; ; ) {
					var c = t.indexOf("var(", a);
					if (-1 === c) {
						if (s) {
							(a = t.indexOf(")", a)), (s = !1);
							continue;
						}
						0, (o += t.substring(a));
						break;
					}
					var l = t.indexOf(")", c + i);
					if (-1 === l) {
						0, (o += t.substring(a));
						break;
					}
					let p = a;
					var u = t.indexOf(",", c + i);
					-1 !== u && l > u ? ((s = !0), (p = u)) : (p = l),
						(o += t.substring(a, c));
					let g = c + i,
						m = p;
					a = m;
					var f = t.substring(g, m);
					let v = "";
					if (
						((o +=
							"var(" +
							(v = n && -1 === f.indexOf("-night-eye") ? f + "-night-eye" : f)),
						-1 !== u && l > u)
					) {
						var h = t.substring(u + 1, l);
						if (-1 === h.indexOf("var(--")) {
							var d = [];
							-1 === h.indexOf("rgb(") &&
								3 === (d = h.split(",")).length &&
								(h = "rgb(" + h + ")");
							let t = r(h);
							3 === d.length
								? (o += "," + this.parseAndGetRGBFromHSLA(t))
								: (o += "," + t);
						} else this.markCSSVariable(h, e, r, n);
					}
				}
				return o;
			}
			extractStyles(t) {
				const e = new Map();
				if (void 0 !== t) {
					const n = t.indexOf("{"),
						o = t.indexOf("}");
					if (-1 !== n && -1 !== o) {
						const i = (t = t.substring(n + 1, o)).split(";");
						for (var r = 0; r < i.length; ++r) {
							const t = i[r].split(":");
							2 === t.length &&
								((t[0] = t[0].trim()), (t[1] = t[1].trim()), e.set(t[0], t[1]));
						}
					}
				}
				return e;
			}
			parse(t, e, r, n) {
				var i,
					s,
					a,
					c = this.convertBackground === r,
					l = this.isCSSRuleBodyOrHTML(t),
					u = new Map(),
					f = this.extractStyles(t.cssText);
				for (let r, n = e.length; n-- > 0; )
					("" === (r = t.style.getPropertyValue(e[n])) &&
						void 0 === (r = f.get(e[n]))) ||
						u.set(e[n], r);
				!0 === u.has("background") &&
					(u.delete("background-image"), u.delete("background-color")),
					!0 === u.has("border") &&
						(u.delete("border-color"),
						u.delete("border-left"),
						u.delete("border-left-color"),
						u.delete("border-right"),
						u.delete("border-right-color"),
						u.delete("border-top"),
						u.delete("border-top-color"),
						u.delete("border-bottom"),
						u.delete("border-bottom-color")),
					!0 === u.has("border-color") &&
						(u.delete("border-left-color"),
						u.delete("border-right-color"),
						u.delete("border-top-color"),
						u.delete("border-bottom-color")),
					!0 === u.has("border-left") && u.delete("border-left-color"),
					!0 === u.has("border-right") && u.delete("border-right-color"),
					!0 === u.has("border-top") && u.delete("border-top-color"),
					!0 === u.has("border-bottom") && u.delete("border-bottom-color"),
					u.forEach((e, u) => {
						if (((i = e), "" !== e))
							if (
								((a = t.style.getPropertyPriority(u)), -1 === (s = n.get(e)))
							) {
								if (-1 !== e.indexOf("var(--")) {
									var f = this.markCSSVariable(e, t, r, c);
									if (this.applyNewColor(t, u, f, a, !1))
										return void n.set(i, f);
								}
								if (c)
									if (-1 !== e.indexOf("url")) {
										if (
											window.location.href.indexOf("photos.google.com") > -1 ||
											e.indexOf("/cleardot.gif") > -1
										)
											return;
										if ("background" === u) {
											if (e.lastIndexOf("fancybox/blank.gif") > -1) return;
											if (l) {
												var h = e;
												(e = r(e)) === h && (e = "#1f1f1f");
											} else e = r(e);
										} else
											"background-image" === u &&
												e.indexOf("gstatic.com") > -1 &&
												((u = "filter"), (e = "invert(85%)"));
										var d = null;
										if (
											(void 0 !== t.parentStyleSheet &&
												(d =
													null === t.parentStyleSheet.href
														? t.parentStyleSheet.ownerNode.href
														: t.parentStyleSheet.href),
											(e = this.convertURLs(e, d, !1)),
											this.applyNewColor(t, u, e, a, !1),
											!1 === o.IFRAME && !0 === o.IMAGE_PROCESSING_ENABLED)
										)
											if (-1 === e.indexOf("svg")) {
												if (
													window.location.href.indexOf("app.asana.com") > -1 ||
													window.location.href.indexOf("feedly.com") > -1 ||
													window.location.href.indexOf("google.com") > -1 ||
													window.location.href.indexOf("wikidot.com") > -1
												)
													return;
												let r =
														t.style.background.indexOf("repeat") > -1 &&
														t.style.backgroundRepeat.indexOf("repeat") > -1,
													n = null,
													i = "";
												G.processBackgroundCSSString(
													{
														value: e,
														PAGE_PROTOCOL: o.PAGE_PROTOCOL,
														PAGE_HOSTNAME: o.PAGE_HOSTNAME,
														PAGE_URL: o.PAGE_URL,
														property: u,
													},
													(e) => {
														if (null !== e.css_text) {
															t.tagName;
															(t.tgIgnore = !0),
																this.style_apply_cache.addWebGlImageItem(
																	t,
																	e,
																	a
																);
														}
														null !== n &&
															((t.tgIgnore = !0),
															this.style_apply_cache.addWebGlBackgroundSizeItem(
																t,
																n,
																i
															));
													},
													() => {
														if (!1 !== r) {
															if (
																((n = t.style.backgroundSize),
																(i = t.style.getPropertyPriority(
																	"background-size"
																)),
																"0px 0px" === n && "important" === i)
															)
																return (n = null), void (i = "");
															(t.tgIgnore = !0),
																t.style.setProperty(
																	"background-size",
																	"0 0",
																	"important"
																);
														}
													}
												);
											} else
												!1 === l &&
													window.location.hostname.indexOf("google") > -1 &&
													((t.tgIgnore = !0),
													(t.style.filter = "invert(100%)"),
													(t.style.backgroundBlendMode = "luminosity"));
									} else
										(e = "initial" === e ? "transparent" : r(e)),
											this.applyNewColor(t, u, e, a, !1) && n.set(i, e);
								else
									(e = r(e)), this.applyNewColor(t, u, e, a, !1) && n.set(i, e);
							} else this.applyNewColor(t, u, s, a, !0);
					});
			}
			applyNewColor(t, e, r, n, i) {
				var s = void 0 !== t.setAttribute;
				return t["eye-" + e] !== r || i
					? ((t["eye-" + e] = r),
					  (t.tgIgnore = !0),
					  t.style.setProperty(e, r, n),
					  void 0 !== t.setAttribute &&
							(t.setAttribute("ne", Math.random()), (s = !0)),
					  o.TURBO_CACHE_ENABLED && this.customStorage.addRule(t, e, r, n, s),
					  !0)
					: (o.TURBO_CACHE_ENABLED &&
							s &&
							this.customStorage.addRule(t, e, r, n, s),
					  !1);
			}
			isCSSRuleBodyOrHTML(t) {
				if (t === document.body || t === document.documentElement) return !0;
				if (void 0 === t.selectorText) return !1;
				var e = t.selectorText.indexOf("html"),
					r = t.selectorText.indexOf("body");
				if (
					(0 === e
						? "html" !== t.selectorText.trim() && (e = -1)
						: e > 0 &&
						  t.selectorText.length > 5 &&
						  (t.selectorText.indexOf(".", 5) > 0
								? (e = -1)
								: t.selectorText.indexOf(" ", 5) > 0 && (e = -1)),
					0 === r)
				)
					"body" !== t.selectorText.trim() && (r = -1);
				else if (r > 0) {
					for (
						var n = !0, o = t.selectorText.split(" "), i = 0;
						i < o.length;
						++i
					) {
						if (0 === o[i].indexOf("body") && i + 1 === o.length) {
							n = !1;
							break;
						}
					}
					n && (r = -1);
				}
				return -1 !== e || -1 !== r;
			}
			convertGoogleDocsNode(t, e) {
				var r = t["eye-color"],
					n = document.createElement("style");
				(n.innerHTML =
					'                            span[style="' +
					e +
					'"] {                            color: ' +
					r +
					" !important;                            }        "),
					document.head.appendChild(n);
			}
			checkAndConvertDomShadowElements(t) {
				for (var e = t.children, r = 0; r < e.length; ++r) {
					var n = e[r];
					if ("SCRIPT" !== n.nodeName) {
						"STYLE" === n.nodeName && this.convertStyleSheet(n.sheet);
						var o = null;
						n.shadowRoot
							? (this.checkAndConvertDomShadowElements(n.shadowRoot),
							  (o = window.getComputedStyle(n, ":host")))
							: (o = window.getComputedStyle(n));
						var i = o.getPropertyValue("background-color"),
							s = o.getPropertyValue("color"),
							a = this.convertBackground(i),
							c = this.convertForeground(s);
						(n.style.backgroundColor = a),
							(n.style.color = c),
							this.checkAndConvertDomShadowElements(n);
					}
				}
			}
			parseAndGetRGBFromHSLA(t) {
				let e = t.substring(5, t.length - 1).split(","),
					r = [];
				for (let t = 0; t < e.length; ++t) r.push(parseFloat(e[t]));
				x.HSLtoRGB(r);
				let n = r;
				return n[0] + "," + n[1] + "," + n[2];
			}
			isHrefCssFontUrl(t) {
				for (var e = ["https://fonts.go", ".woff"], r = 0; r < e.length; ++r)
					if (-1 !== t.indexOf(e[r])) return !0;
				return !1;
			}
		}
		(Z.BACKGROUND_PROPERTIES = [
			"background",
			"background-image",
			"background-color",
			"border",
			"border-color",
			"border-left",
			"border-left-color",
			"border-right",
			"border-right-color",
			"border-top",
			"border-top-color",
			"border-bottom",
			"border-top-color",
			"text-shadow",
			"box-shadow",
		]),
			(Z.FOREGROUND_PROPERTIES = [
				"fill",
				"color",
				"text-decoration",
				"outline",
				"column-rule",
				"caret",
			]),
			(Z.BACKGROUND_PROPERTIES_SET = new Set()),
			(Z.FOREGROUND_PROPERTIES_SET = new Set());
		var J = Z;
		var Q = class {
				constructor(t, e, r) {
					(this.enabled = t),
						(this.local_settings = e),
						(this.mode = parseInt(e.mode)),
						(this.styleConverter = new J(e)),
						(this.state = new A(this.styleConverter, r)),
						(this.mutationManager = new T(this));
				}
				start(t) {
					if (document.documentElement.hasAttribute("data-reactroot"))
						var e = setInterval(() => {
							"complete" === document.readyState &&
								(clearInterval(e), this.startConverting(t));
						}, 100);
					else this.startConverting(t);
				}
				startConverting(t) {
					setTimeout(() => {
						this.addFilterCSS(),
							(o.BUILT_IN_DARK_THEME_MODE !== a.BuiltInBehaviourMode.CONVERT &&
								this.checkAndApplyWebsiteBuiltInTheme()) ||
								(this.mode === a.Mode.DARK
									? (this.addDefaultCSS(),
									  this.state.initAndShowLoading(!0),
									  this.styleConverter.init(),
									  this.styleConverter.convert(),
									  -1 ===
											window.location.href.indexOf(
												"docs.google.com/presentation"
											) &&
											"app.diagrams.net" !== window.location.host &&
											this.mutationManager.init())
									: this.state.initAndShowLoading(!1));
					}, 0);
				}
				checkAndApplyWebsiteBuiltInTheme() {
					if (window.self !== window.top) return !1;
					var t = window.location.hostname.replace("www.", ""),
						e = !0,
						r =
							o.BUILT_IN_DARK_THEME_MODE !== a.BuiltInBehaviourMode.INTEGRATED;
					switch (t) {
						case a.BuiltInWebsites.YOUTUBE:
							if (r) break;
							if (window.navigator.userAgent.indexOf("Edg/") > -1) return !1;
							this.updateWebsiteBuiltInTheme_Youtube();
							break;
						case a.BuiltInWebsites.REDDIT:
							if (r) break;
							if (
								this.isBraveBrowser() ||
								window.navigator.userAgent.indexOf("Edge/") > -1
							)
								return !1;
							this.updateWebsiteBuiltInTheme_Reddit();
							break;
						case a.BuiltInWebsites.TWITTER:
							if (r) break;
							this.updateWebsiteBuiltInTheme_Twitter();
							break;
						case a.BuiltInWebsites.DUCK_DUCK_GO:
							if (r) break;
							this.updateWebsiteBuiltInTheme_DuckDuckGo();
							break;
						case a.BuiltInWebsites.COIN_MARKET_CAP:
							if (r) break;
							this.updateWebsiteBuiltInTheme_CoinMarketCap();
							break;
						case a.BuiltInWebsites.TWITCH:
							if (r) break;
							if (this.isBraveBrowser()) return !1;
							this.updateWebsiteBuiltInTheme_Twitch();
							break;
						case a.BuiltInWebsites.NINE_GAG:
							if (r) break;
							if (this.isBraveBrowser()) return !1;
							this.updateWebsiteBuiltInTheme_9gag();
							break;
						case a.BuiltInWebsites.DOCS_MICROSOFT:
							if (r) break;
							if (
								this.isBraveBrowser() ||
								window.navigator.userAgent.indexOf("Edg/") > -1
							)
								return !1;
							this.updateWebsiteBuiltInTheme_DocsMicrosoft();
							break;
						case a.BuiltInWebsites.ARSTECHNICA:
							if (r) break;
							this.updateWebsiteBuiltInTheme_ArsTechnica();
							break;
						case a.BuiltInWebsites.MATERIAL_UI:
							if (r) break;
							this.updateWebsiteBuiltInTheme_MaterialUI();
							break;
						case a.BuiltInWebsites.INDIEHACKERS:
							break;
						default:
							e = !1;
					}
					return !!e && (this.state.onReady(this.mode === a.Mode.DARK), !0);
				}
				reinitDomElements() {
					(this.mode = parseInt(localStorage.getItem(a.LOCAL_STORAGE.MODE))),
						4 !== this.mode &&
							(this.addFilterCSS(),
							this.addDefaultCSS(),
							this.addColorFilters()),
						1 === this.mode && this.styleConverter.convertProcedure(!0);
				}
				addDefaultCSS() {
					var t =
							null === document.head ? document.documentElement : document.head,
						e = document.createElement("style");
					(e.id = "nighteyedefaultcss"),
						(e.tgIgnore = !0),
						(e.tgParsed = !0),
						(e.innerHTML =
							"html {                            color:#bfbfbf;                            background-image:none !important;                            background:#1f1f1f !important;                        }                        body {                            background-color:#1f1f1f;                            background-image:none !important;                        }                        input, select, textarea, button {                            color:#bfbfbf;                            background-color:#1f1f1f;                        }                        font {                            color:#bfbfbf;                        }"),
						t.insertBefore(e, t.childNodes[0]),
						(e = E.makeParsedStyleNode());
					var r =
						'a {                        color:rgb(140,140,250);                        }                        *::-webkit-scrollbar-track-piece {                            background-color:rgba(255, 255, 255, 0.2) !important;                        }                        *::-webkit-scrollbar-track {                            background-color:rgba(255, 255, 255, 0.3) !important;                        }                        *::-webkit-scrollbar-thumb {                            background-color:rgba(255, 255, 255, 0.5) !important;                        }                        embed[type="application/pdf"] {                            filter:invert(90%);                        }';
					"stackoverflow.com" === o.URL &&
						(r +=
							".answer, .question, .post-layout {                background-color: transparent !important;            }"),
						(e.innerHTML = r),
						t.insertBefore(e, t.childNodes[0]);
				}
				addFilterCSS() {
					var t = 2 * this.local_settings.brightness.global_v,
						e = 2 * this.local_settings.contrast.global_v,
						r = 2 * this.local_settings.saturation.global_v,
						n =
							this.local_settings.temperature.global_v < 50
								? 1 - 0.02 * this.local_settings.temperature.global_v
								: 0,
						o =
							this.local_settings.temperature.global_v > 50
								? 0.02 * (this.local_settings.temperature.global_v - 50)
								: 0,
						i = 0.01 * this.local_settings.dim.global_v,
						s =
							null === document.head ? document.documentElement : document.head,
						c = E.makeParsedStyleNode(),
						l = "";
					(l +=
						"html {filter: contrast(" +
						e +
						"%) brightness(" +
						t +
						"%) saturate(" +
						r +
						"%);"),
						this.mode === a.Mode.FILTERED &&
							(l += "background: url() !important;"),
						(l += "}"),
						(0 === o && 0 === n && 0 === i) || this.addColorFilters(),
						(l +=
							"                        .NIGHTEYE_Filter {                            width:100%; height:100%;                            position:fixed;                            left:0; top:0;                            pointer-events:none;                            z-index:4000000000;                        }                        .NIGHTEYE_YellowFilter {                            background:rgba(255, 255, 0, 0.15);                            opacity:" +
							o +
							";                        }                        .NIGHTEYE_BlueFilter {                            background:rgba(0, 0, 255, 0.15);                            opacity:" +
							n +
							";                        }                        .NIGHTEYE_DimFilter {                            background:rgba(0, 0, 0, 0.5);                            opacity:" +
							i +
							";                        }                        .NIGHTEYE_TransformZ {                            transform:translateZ(0);                        }"),
						window.navigator.userAgent.indexOf("Firefox") > -1 &&
							(l = _.a.sanitize(l)),
						(c.innerHTML = l),
						s.insertBefore(c, s.childNodes[0]);
				}
				addColorFilters() {
					var t = document.createDocumentFragment(),
						e = document.createElement("div");
					if (
						((e.className = "NIGHTEYE_BlueFilter NIGHTEYE_Filter"),
						t.appendChild(e),
						((e = document.createElement("div")).className =
							"NIGHTEYE_YellowFilter NIGHTEYE_Filter"),
						t.appendChild(e),
						((e = document.createElement("div")).className =
							"NIGHTEYE_DimFilter NIGHTEYE_Filter"),
						t.appendChild(e),
						null !== document.body)
					)
						document.body.appendChild(t);
					else
						var r = setInterval(() => {
							null !== document.body &&
								(document.body.appendChild(t), clearInterval(r));
						}, 300);
				}
				checkAndUpdateModeSimple(t) {
					return t
						? !!this.enabled && this.mode === a.Mode.DARK
						: !this.enabled ||
								this.mode === a.Mode.NORMAL ||
								this.mode === a.Mode.FILTER;
				}
				checkAndUpdateModeAdvanced(t) {
					if (t) {
						if (this.mode === a.Mode.DARK) return !0;
						if (!this.enabled) return this.updateMode(!0);
					} else {
						if (this.mode === a.Mode.NORMAL || this.mode === a.Mode.FILTER)
							return !0;
						if (!this.enabled) return this.updateMode(!1);
					}
					return !1;
				}
				updateMode(t) {
					var e = t ? a.Mode.DARK : a.Mode.NORMAL;
					return (
						s.send(
							s.Settings.ID,
							s.Settings.ACTION_CHANGE_MODE,
							{ url: o.URL, contentType: document.contentType, mode: e },
							() => {}
						),
						!0
					);
				}
				updateWebsiteBuiltInTheme_Youtube() {
					for (
						var t = this.getCookie("PREF").split("&"),
							e = "",
							r = !1,
							n = !1,
							o = 0;
						o < t.length;
						++o
					) {
						var i = t[o].split("="),
							s = i[0],
							c = i[1];
						if ("f6" === s) {
							if (
								((n = "400" === c), this.mode === a.Mode.DARK && this.enabled)
							) {
								if ("400" === c) break;
								i = "f6=400";
							} else i = "f6=80000";
							r = !0;
						} else i = t[o];
						o > 0 && (e += "&"), (e += i);
					}
					if (!this.checkAndUpdateModeSimple(n)) {
						if (!r) {
							if (this.mode !== a.Mode.DARK) return;
							e += "&f6=400";
						}
						var l = new Date();
						l.setTime(l.getTime() + 31536e6);
						var u = "; expires=" + l.toGMTString();
						document.cookie = "PREF=" + e + u + ";domain=.youtube.com;path=/";
					}
				}
				updateWebsiteBuiltInTheme_Reddit() {
					var t = this.getCookie("USER");
					null === t &&
						(t =
							"eyJwcmVmcyI6eyJnbG9iYWxUaGVtZSI6IlJFRERJVCIsImZlYXR1cmVzVmlld2VkSGlzdG9yeSI6eyJjb21tZW50Rm9ybSI6eyJtYXJrZG93bk1vZGVOb3RpZmljYXRpb24iOmZhbHNlfX0sImNvbGxhcHNlZFRyYXlTZWN0aW9ucyI6eyJmYXZvcml0ZXMiOmZhbHNlLCJtdWx0aXMiOmZhbHNlLCJtb2RlcmF0aW5nIjpmYWxzZSwic3Vic2NyaXB0aW9ucyI6ZmFsc2UsInByb2ZpbGVzIjpmYWxzZX0sIm5pZ2h0bW9kZSI6ZmFsc2UsInRvcENvbnRlbnREaXNtaXNzYWxUaW1lIjowLCJ0b3BDb250ZW50VGltZXNEaXNtaXNzZWQiOjB9LCJsYW5ndWFnZSI6ImVuIn0=");
					var e = JSON.parse(atob(t)),
						r = !!e.prefs.nightmode;
					if (!this.checkAndUpdateModeSimple(r)) {
						this.mode === a.Mode.DARK && this.enabled
							? (e.prefs.nightmode = !0)
							: (e.prefs.nightmode = !1);
						var n = btoa(JSON.stringify(e)),
							o = new Date();
						o.setTime(o.getTime() + 31536e6);
						var i = "; expires=" + o.toGMTString();
						(document.cookie = "USER=" + n + i + ";domain=.reddit.com;path=/"),
							window.location.reload();
					}
				}
				updateWebsiteBuiltInTheme_Twitter() {
					var t = "1" === this.getCookie("night_mode");
					if (!this.checkAndUpdateModeSimple(t)) {
						var e = "0",
							r = new Date();
						this.mode === a.Mode.DARK && this.enabled && (e = "1"),
							r.setTime(r.getTime() + 31536e6);
						var n = "; expires=" + r.toGMTString();
						document.cookie =
							"night_mode=" + e + n + ";domain=.twitter.com;path=/";
					}
				}
				updateWebsiteBuiltInTheme_DuckDuckGo() {
					var t = "d" === this.getCookie("ae");
					if (!this.checkAndUpdateModeSimple(t)) {
						var e = "",
							r = new Date();
						(e = this.mode === a.Mode.DARK && this.enabled ? "d" : "a"),
							r.setTime(r.getTime() + 31536e6);
						var n = "; expires=" + r.toGMTString();
						(document.cookie = "ae=" + e + n + ";domain=duckduckgo.com;path=/"),
							(document.cookie =
								"ae=" + e + n + ";domain=.duckduckgo.com;path=/");
					}
				}
				updateWebsiteBuiltInTheme_CoinMarketCap() {
					var t = "night" === this.getCookie("theme");
					if (!this.checkAndUpdateModeSimple(t)) {
						var e = "";
						e = this.mode === a.Mode.DARK && this.enabled ? "night" : "day";
						var r = new Date();
						r.setTime(r.getTime() + 31536e6);
						var n = "; expires=" + r.toGMTString();
						document.cookie = "theme=" + e + n + ";path=/";
					}
				}
				updateWebsiteBuiltInTheme_Twitch() {
					var t = "twilight.theme",
						e = "1" == localStorage.getItem(t);
					this.checkAndUpdateModeSimple(e) ||
						(this.mode === a.Mode.DARK && this.enabled
							? (localStorage.setItem(t, 1),
							  localStorage.setItem("bttv_darkenedMode", "true"))
							: (localStorage.setItem(t, 0),
							  localStorage.setItem("bttv_darkenedMode", "false")));
				}
				updateWebsiteBuiltInTheme_9gag() {
					var t,
						e = localStorage.getItem("appState"),
						r = !!(t = null == e ? { app: { darkMode: !1 } } : JSON.parse(e))
							.app.darkMode;
					this.checkAndUpdateModeSimple(r) ||
						(this.mode === a.Mode.DARK && this.enabled
							? (t.app.darkMode = !0)
							: (t.app.darkMode = !1),
						localStorage.setItem("appState", JSON.stringify(t)));
				}
				updateWebsiteBuiltInTheme_DocsMicrosoft() {
					var t = "dark" === localStorage.getItem("theme");
					this.checkAndUpdateModeSimple(t) ||
						(this.mode === a.Mode.DARK && this.enabled
							? localStorage.setItem("theme", "dark")
							: localStorage.setItem("theme", "light"));
				}
				updateWebsiteBuiltInTheme_ArsTechnica() {
					var t = "dark" === this.getCookie("theme");
					if (!this.checkAndUpdateModeSimple(t)) {
						var e = "";
						e = this.mode === a.Mode.DARK && this.enabled ? "dark" : "light";
						var r = new Date();
						r.setTime(r.getTime() + 31536e6);
						var n = "; expires=" + r.toGMTString();
						(document.cookie =
							"theme=" + e + n + ";domain=.arstechnica.com;path=/"),
							window.location.reload();
					}
				}
				updateWebsiteBuiltInTheme_MaterialUI() {
					var t = "dark" === this.getCookie("paletteType");
					if (!this.checkAndUpdateModeSimple(t)) {
						var e = "";
						e = this.mode === a.Mode.DARK && this.enabled ? "dark" : "light";
						var r = new Date();
						r.setTime(r.getTime() + 31536e6);
						var n = "; expires=" + r.toGMTString();
						document.cookie =
							"paletteType=" + e + n + ";domain=material-ui.com;path=/";
					}
				}
				getCookie(t) {
					for (
						var e = document.cookie.split(";"), r = e.length - 1;
						r >= 0;
						--r
					) {
						var n = e[r],
							o = n.indexOf("=");
						if (o > -1)
							if (n.substring(0, o).trim() === t) return n.substring(o + 1);
					}
					return null;
				}
				isBraveBrowser() {
					var t = navigator.userAgent.toLowerCase();
					return (
						/chrome|crios/.test(t) &&
						!/edge|opr\//.test(t) &&
						0 === window.navigator.plugins.length &&
						0 === window.navigator.mimeTypes.length
					);
				}
			},
			$ = r(145),
			tt = r.n($);
		new (class {
			constructor() {
				(this.core = null),
					(this.local_settings = {}),
					(this.initOnBackgroundResponse = this.initOnBackgroundResponse.bind(
						this
					));
			}
			init() {
				if (
					((o.PAGE_URL = document.location.href.substring(
						0,
						document.location.href.lastIndexOf("/") + 1
					)),
					(o.PAGE_PROTOCOL = document.location.protocol),
					(o.PAGE_HOSTNAME = document.location.hostname),
					(o.PAGE_PORT = document.location.port),
					(o.IFRAME = this.isInIframe()),
					(o.URL = i.parseURL(document.location.href)),
					"" === o.URL && (o.URL = i.parseURL(document.referrer)),
					"" !== o.URL)
				) {
					var t = n.EMPTY;
					if (!0 === o.IFRAME) {
						if ("accounts.google.com" === o.URL || "ogs.google.com" === o.URL)
							return;
						(t = i.parseURL(document.referrer)) !== o.URL &&
							(o.URL = t + "-" + o.URL);
					}
					this.checkLicenseOnVivaldiBrowser(),
						s.send(
							s.Settings.ID,
							s.Settings.ACTION_ON_START_CONVERTER,
							{ url: o.URL, contentType: document.contentType },
							this.initOnBackgroundResponse
						);
				}
			}
			initOnBackgroundResponse(t) {
				var e = c.fromState(b, t);
				if (
					((this.local_settings = e.local_settings),
					(this.local_settings.mode = e.mode),
					(o.IMAGE_PROCESSING_ENABLED = this.local_settings.images.global_v),
					(o.BUILT_IN_DARK_THEME_MODE = parseInt(
						this.local_settings.builtInDarkTheme.global_v
					)),
					(o.TURBO_CACHE_ENABLED = this.local_settings.turboCache.global_v),
					"www.facebook.com" !== o.PAGE_HOSTNAME ||
						this.local_settings.images.user_changed ||
						(o.IMAGE_PROCESSING_ENABLED = !1),
					window.self === window.top &&
						"msn.com" !== o.URL &&
						(localStorage.setItem(i.LOCAL_STORAGE.KEYS.STATE, e.local_state),
						localStorage.setItem(i.LOCAL_STORAGE.KEYS.MODE, e.local_mode),
						localStorage.setItem(
							i.LOCAL_STORAGE.KEYS.SUPPORTED,
							e.local_supported
						)),
					this.initConnection(e.start),
					(this.core = new Q(
						e.enabled,
						this.local_settings,
						e.isChangingScrollsEnabled
					)),
					!1 !== this.checkModeOfIframes())
				) {
					if (!1 === e.start) {
						var r = e.isLiteVersion && !e.is_trial_expired;
						return (
							o.BUILT_IN_DARK_THEME_MODE !==
								i.BuiltInBehaviourMode.INTEGRATED ||
								(e.isLiteVersion && !r) ||
								this.core.checkAndApplyWebsiteBuiltInTheme(),
							this.core.state.onReady(!1),
							e.is_trial_expired &&
								this.isExpiredAlertNotShownToday(e.lastExtensionWindowOpenTS) &&
								!e.isLiteVersion &&
								!this.isInIframe() &&
								("complete" === document.readyState
									? this.showTrialExpiredAlert()
									: window.addEventListener(
											"load",
											this.showTrialExpiredAlert.bind(this)
									  )),
							void (
								parseInt(this.local_settings.mode) !== i.Mode.DARK && X.erase()
							)
						);
					}
					this.core.start(e.isLiteVersion);
				}
			}
			initConnection(t) {
				!0 !== o.IFRAME &&
					(window.navigator.userAgent.indexOf("Edge") > -1
						? o.browser.runtime.onMessage.addListener((e) => {
								var r = {};
								(r.postMessage = o.browser.runtime.sendMessage),
									this.processData(e, r, t);
						  })
						: o.browser.runtime.onConnect.addListener((e) => {
								e.onMessage.addListener((r) => {
									this.processData(r, e, t);
								}),
									e.onDisconnect.addListener(() => {
										if (!1 !== t) {
											var e = {
												brightness: this.local_settings.brightness.global_v,
												contrast: this.local_settings.contrast.global_v,
												saturation: this.local_settings.saturation.global_v,
												temperature: this.local_settings.temperature.global_v,
											};
											this.updateHTMLStyles(e);
										}
									});
						  }));
			}
			processData(t, e, r) {
				switch (t.property) {
					case "brightness":
					case "contrast":
					case "saturation":
					case "temperature":
					case "dim":
						if (!1 === r) return;
						var n = {
							brightness: this.local_settings.brightness.global_v,
							contrast: this.local_settings.contrast.global_v,
							saturation: this.local_settings.saturation.global_v,
							temperature: this.local_settings.temperature.global_v,
							dim: this.local_settings.dim.global_v,
						};
						(n[t.property] = t.value), this.updateHTMLStyles(n);
						break;
					case "open_extension":
						e.postMessage({ property: t.property, data: this.local_settings });
						break;
					case "load_page_hsl_colors":
						if (!1 === r) return;
						e.postMessage({ property: t.property, data: N.colors });
						break;
					case "change_mode_bg":
						localStorage.setItem(i.LOCAL_STORAGE.KEYS.MODE, t.value),
							e.postMessage({});
						break;
					case "change_state_bg":
						localStorage.setItem(i.LOCAL_STORAGE.KEYS.STATE, t.value),
							e.postMessage({});
				}
			}
			updateHTMLStyles(t) {
				(t.brightness *= 2), (t.contrast *= 2), (t.saturation *= 2);
				var e = t.temperature < 50 ? 1 - 0.02 * t.temperature : 0,
					r = t.temperature > 50 ? 0.02 * (t.temperature - 50) : 0,
					n = 0.01 * t.dim;
				document.documentElement.style.filter =
					"brightness(" +
					t.brightness +
					"%) contrast(" +
					t.contrast +
					"%) saturate(" +
					t.saturation +
					"%)";
				var o = document.querySelector(".NIGHTEYE_YellowFilter");
				(null != o && null != o) || this.core.addColorFilters(),
					(document.querySelector(".NIGHTEYE_YellowFilter").style.opacity = r),
					(document.querySelector(".NIGHTEYE_BlueFilter").style.opacity = e),
					(document.querySelector(".NIGHTEYE_DimFilter").style.opacity = n);
			}
			isExpiredAlertNotShownToday(t) {
				var e = new Date(parseInt(t)).setHours(0, 0, 0, 0);
				return new Date().setHours(0, 0, 0, 0) !== e;
			}
			showTrialExpiredAlert() {
				var t = document.createElement("div");
				(t.className = "NightEye_TrialNotification"),
					(t.style.height = "48px"),
					(t.style.display = "flex"),
					(t.style.alignItems = "center"),
					(t.style.boxSizing = "border-box"),
					(t.style.position = "fixed"),
					(t.style.top = 0),
					(t.style.right = "16px"),
					(t.style.color = "#78787a"),
					(t.style.borderBottomLeftRadius = "4px"),
					(t.style.borderBottomRightRadius = "4px"),
					(t.style.padding = "4px 8px"),
					(t.style.zIndex = 2e9),
					(t.style.background = "#29374b"),
					(t.style.transform = "translateY(-100%)"),
					(t.style.transitionProperty = "transform"),
					(t.style.transitionDuration = ".5s"),
					(t.innerHTML =
						'<img src="' +
						tt.a +
						'" style="width: 20px; margin-right:8px;" /> The Night Eye extension is disabled. Please click the extension icon above to enable dark mode again.'),
					document.body.appendChild(t),
					requestAnimationFrame(() => {
						t.style.transform = "translateY(0)";
					});
			}
			isAllowShowingMessage() {
				for (var t = ["accounts.google.com"], e = 0; e < t.length; ++e)
					if (-1 !== window.location.href.indexOf(t[0])) return !1;
				return !0;
			}
			checkModeOfIframes() {
				if ("msn.com" !== o.URL) {
					if (window.self === window.top)
						window.onmessage = function (t) {
							if (
								void 0 !== t.data &&
								void 0 !== t.data.action &&
								"getNightEyeMode" === t.data.action
							) {
								var e = localStorage.getItem(i.LOCAL_STORAGE.KEYS.STATE),
									r = localStorage.getItem(i.LOCAL_STORAGE.KEYS.MODE);
								if (null === t.source || void 0 === t.source) return;
								t.source.postMessage(
									{ action: "nightEyeModeResponse", mode: r, state: e },
									"*"
								);
							}
						};
					else if (
						(window.top.postMessage({ action: "getNightEyeMode" }, "*"),
						(window.onmessage = function (t) {
							var e = o.URL + "-";
							if (
								void 0 !== t.data &&
								void 0 !== t.data.action &&
								"nightEyeModeResponse" === t.data.action
							) {
								var r = localStorage.getItem(e + i.LOCAL_STORAGE.KEYS.MODE);
								if (t.data.mode === r) return;
								localStorage.setItem(
									e + i.LOCAL_STORAGE.KEYS.MODE,
									t.data.mode
								);
								var n = {};
								(n.url = o.URL),
									(n.mode = t.data.mode),
									(n.refreshPage = !1),
									o.browser.runtime.sendMessage(
										{
											key: s.Settings.ID,
											action: s.Settings.ACTION_CHANGE_MODE,
											data: n,
										},
										(t) => {
											null !== r && window.location.reload();
										}
									);
							}
						}),
						this.isForbiddenIframe())
					)
						return this.checkAndInvertWebsite(), !1;
					return !0;
				}
			}
			checkLicenseOnVivaldiBrowser() {
				if (
					navigator.userAgent.lastIndexOf("Vivaldi") > -1 &&
					document.location.href === o.url_thankyou_page
				) {
					var t = localStorage.getItem("nighteye-ins");
					null !== t &&
						o.browser.runtime.sendMessage(
							{
								key: s.Settings.ID,
								action: s.Settings.ACTION_UPDATE_INSTANCE_AND_DEVICE_IDS,
								data: { instanceID: n.EMPTY, deviceID: t },
							},
							(t) => {}
						);
				}
			}
			checkAndInvertWebsite() {
				"w.soundcloud.com" === o.URL &&
					this.core.mode === i.Mode.DARK &&
					document.documentElement.classList.add("NightEyeInvert");
			}
			isForbiddenIframe() {
				return (
					(window.location.href.indexOf("ext-twitch.tv") > -1 ||
						window.location.href.indexOf("imasdk.googleapis.com") > -1 ||
						window.location.href.indexOf("w.soundcloud.com") > -1 ||
						window.location.href.indexOf("mixerusercontent.com") > -1) &&
					(document.documentElement.setAttribute("nighteye", "disabled"), !0)
				);
			}
			isInIframe() {
				try {
					return window.self !== window.top;
				} catch (t) {
					return !0;
				}
			}
		})().init();
	},
]);
