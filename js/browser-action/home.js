!(function (t) {
	var e = {};
	function n(i) {
		if (e[i]) return e[i].exports;
		var r = (e[i] = { i: i, l: !1, exports: {} });
		return t[i].call(r.exports, r, r.exports, n), (r.l = !0), r.exports;
	}
	(n.m = t),
		(n.c = e),
		(n.d = function (t, e, i) {
			n.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: i });
		}),
		(n.r = function (t) {
			"undefined" != typeof Symbol &&
				Symbol.toStringTag &&
				Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }),
				Object.defineProperty(t, "__esModule", { value: !0 });
		}),
		(n.t = function (t, e) {
			if ((1 & e && (t = n(t)), 8 & e)) return t;
			if (4 & e && "object" == typeof t && t && t.__esModule) return t;
			var i = Object.create(null);
			if (
				(n.r(i),
				Object.defineProperty(i, "default", { enumerable: !0, value: t }),
				2 & e && "string" != typeof t)
			)
				for (var r in t)
					n.d(
						i,
						r,
						function (e) {
							return t[e];
						}.bind(null, r)
					);
			return i;
		}),
		(n.n = function (t) {
			var e =
				t && t.__esModule
					? function () {
							return t.default;
					  }
					: function () {
							return t;
					  };
			return n.d(e, "a", e), e;
		}),
		(n.o = function (t, e) {
			return Object.prototype.hasOwnProperty.call(t, e);
		}),
		(n.p = ""),
		n((n.s = 142));
})([
	function (t, e, n) {
		var i = n(2),
			r = n(15).f,
			s = n(14),
			o = n(16),
			a = n(85),
			l = n(107),
			c = n(58);
		t.exports = function (t, e) {
			var n,
				u,
				h,
				d,
				f,
				p = t.target,
				g = t.global,
				v = t.stat;
			if ((n = g ? i : v ? i[p] || a(p, {}) : (i[p] || {}).prototype))
				for (u in e) {
					if (
						((d = e[u]),
						(h = t.noTargetGet ? (f = r(n, u)) && f.value : n[u]),
						!c(g ? u : p + (v ? "." : "#") + u, t.forced) && void 0 !== h)
					) {
						if (typeof d == typeof h) continue;
						l(d, h);
					}
					(t.sham || (h && h.sham)) && s(d, "sham", !0), o(n, u, d, t);
				}
		};
	},
	function (t, e) {
		t.exports = function (t) {
			try {
				return !!t();
			} catch (t) {
				return !0;
			}
		};
	},
	function (t, e, n) {
		(function (e) {
			var n = "object",
				i = function (t) {
					return t && t.Math == Math && t;
				};
			t.exports =
				i(typeof globalThis == n && globalThis) ||
				i(typeof window == n && window) ||
				i(typeof self == n && self) ||
				i(typeof e == n && e) ||
				Function("return this")();
		}.call(this, n(145)));
	},
	function (t, e) {
		t.exports = function (t) {
			return "object" == typeof t ? null !== t : "function" == typeof t;
		};
	},
	function (t, e, n) {
		var i = n(3);
		t.exports = function (t) {
			if (!i(t)) throw TypeError(String(t) + " is not an object");
			return t;
		};
	},
	function (t, e, n) {
		"use strict";
		var i,
			r = n(6),
			s = n(2),
			o = n(3),
			a = n(12),
			l = n(63),
			c = n(14),
			u = n(16),
			h = n(9).f,
			d = n(30),
			f = n(48),
			p = n(7),
			g = n(55),
			v = s.DataView,
			_ = v && v.prototype,
			m = s.Int8Array,
			y = m && m.prototype,
			b = s.Uint8ClampedArray,
			w = b && b.prototype,
			S = m && d(m),
			E = y && d(y),
			T = Object.prototype,
			L = T.isPrototypeOf,
			A = p("toStringTag"),
			I = g("TYPED_ARRAY_TAG"),
			C = !(!s.ArrayBuffer || !s.DataView),
			M = C && !!f,
			x = !1,
			D = {
				Int8Array: 1,
				Uint8Array: 1,
				Uint8ClampedArray: 1,
				Int16Array: 2,
				Uint16Array: 2,
				Int32Array: 4,
				Uint32Array: 4,
				Float32Array: 4,
				Float64Array: 8,
			},
			O = function (t) {
				return o(t) && a(D, l(t));
			};
		for (i in D) s[i] || (M = !1);
		if (
			(!M || "function" != typeof S || S === Function.prototype) &&
			((S = function () {
				throw TypeError("Incorrect invocation");
			}),
			M)
		)
			for (i in D) s[i] && f(s[i], S);
		if ((!M || !E || E === T) && ((E = S.prototype), M))
			for (i in D) s[i] && f(s[i].prototype, E);
		if ((M && d(w) !== E && f(w, E), r && !a(E, A)))
			for (i in ((x = !0),
			h(E, A, {
				get: function () {
					return o(this) ? this[I] : void 0;
				},
			}),
			D))
				s[i] && c(s[i], I, i);
		C && f && d(_) !== T && f(_, T),
			(t.exports = {
				NATIVE_ARRAY_BUFFER: C,
				NATIVE_ARRAY_BUFFER_VIEWS: M,
				TYPED_ARRAY_TAG: x && I,
				aTypedArray: function (t) {
					if (O(t)) return t;
					throw TypeError("Target is not a typed array");
				},
				aTypedArrayConstructor: function (t) {
					if (f) {
						if (L.call(S, t)) return t;
					} else
						for (var e in D)
							if (a(D, i)) {
								var n = s[e];
								if (n && (t === n || L.call(n, t))) return t;
							}
					throw TypeError("Target is not a typed array constructor");
				},
				exportProto: function (t, e, n) {
					if (r) {
						if (n)
							for (var i in D) {
								var o = s[i];
								o && a(o.prototype, t) && delete o.prototype[t];
							}
						(E[t] && !n) || u(E, t, n ? e : (M && y[t]) || e);
					}
				},
				exportStatic: function (t, e, n) {
					var i, o;
					if (r) {
						if (f) {
							if (n) for (i in D) (o = s[i]) && a(o, t) && delete o[t];
							if (S[t] && !n) return;
							try {
								return u(S, t, n ? e : (M && m[t]) || e);
							} catch (t) {}
						}
						for (i in D) !(o = s[i]) || (o[t] && !n) || u(o, t, e);
					}
				},
				isView: function (t) {
					var e = l(t);
					return "DataView" === e || a(D, e);
				},
				isTypedArray: O,
				TypedArray: S,
				TypedArrayPrototype: E,
			});
	},
	function (t, e, n) {
		var i = n(1);
		t.exports = !i(function () {
			return (
				7 !=
				Object.defineProperty({}, "a", {
					get: function () {
						return 7;
					},
				}).a
			);
		});
	},
	function (t, e, n) {
		var i = n(2),
			r = n(54),
			s = n(55),
			o = n(109),
			a = i.Symbol,
			l = r("wks");
		t.exports = function (t) {
			return l[t] || (l[t] = (o && a[t]) || (o ? a : s)("Symbol." + t));
		};
	},
	function (t, e, n) {
		var i = n(24),
			r = Math.min;
		t.exports = function (t) {
			return t > 0 ? r(i(t), 9007199254740991) : 0;
		};
	},
	function (t, e, n) {
		var i = n(6),
			r = n(104),
			s = n(4),
			o = n(27),
			a = Object.defineProperty;
		e.f = i
			? a
			: function (t, e, n) {
					if ((s(t), (e = o(e, !0)), s(n), r))
						try {
							return a(t, e, n);
						} catch (t) {}
					if ("get" in n || "set" in n)
						throw TypeError("Accessors not supported");
					return "value" in n && (t[e] = n.value), t;
			  };
	},
	function (t, e, n) {
		var i = n(19);
		t.exports = function (t) {
			return Object(i(t));
		};
	},
	function (t, e, n) {
		"use strict";
		n(25), n(46), n(40);
		class i {
			constructor(t) {
				(this.communicatorID = t), (this.promiseID = ""), (this.info = {});
			}
		}
		class r {
			static shared() {
				return (
					(void 0 !== r.instance && null !== r.instance) ||
						(r.instance = new r()),
					r.instance
				);
			}
			constructor() {
				(this.communicatorID = r.generateUUID()),
					(this.promises = new Map()),
					(this.onConnectListeners = { tabs: [], runtime: [] }),
					(this.onPortMessageListeners = []),
					(this.messageRuntimeListeners = []),
					(this.tabActionListeners = []),
					void 0 !== window.safari &&
						window.safari.self.addEventListener(
							"message",
							this.processResponse.bind(this)
						);
			}
			getFile(t) {
				return new Promise((e, n) => {
					var s = r.generateUUID();
					this.promises.set(s, { resolve: e, reject: n });
					var o = new i(this.communicatorID);
					(o.promiseID = s), (o.info.filename = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.files.postMessage(o)
							: window.safari.extension.dispatchMessage("files", o);
					} catch (t) {
						console.error(t);
					}
				});
			}
			downlaod(t) {
				return new Promise((e, n) => {
					var s = r.generateUUID();
					this.promises.set(s, { resolve: e, reject: n });
					var o = new i(this.communicatorID);
					(o.promiseID = s), (o.info.url = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.downlaod.postMessage(o)
							: window.safari.extension.dispatchMessage("download", o);
					} catch (t) {
						console.error(t);
					}
				});
			}
			tabs(t, e) {
				return new Promise((n, s) => {
					var o = r.generateUUID();
					this.promises.set(o, { resolve: n, reject: s });
					var a = new i(this.communicatorID);
					(a.promiseID = o), (a.info.action = e), (a.info.queryInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.tabs.postMessage(a)
							: window.safari.extension.dispatchMessage("tabs", a);
					} catch (t) {
						console.error(t);
					}
				});
			}
			sendMessage(t) {
				return new Promise((e, n) => {
					var s = r.generateUUID();
					this.promises.set(s, { resolve: e, reject: n });
					var o = new i(this.communicatorID);
					(o.promiseID = s), (o.info.userInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.runtime.postMessage(o)
							: window.safari.extension.dispatchMessage("runtime", o);
					} catch (t) {
						console.error(t);
					}
				});
			}
			browserAction(t) {
				return new Promise((e, n) => {
					var s = r.generateUUID();
					this.promises.set(s, { resolve: e, reject: n });
					var o = new i(this.communicatorID);
					(o.promiseID = s), (o.info.userInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.browser_action.postMessage(o)
							: window.safari.extension.dispatchMessage("browser_action", o);
					} catch (t) {
						console.error(t);
					}
				});
			}
			port(t) {
				return new Promise((e, n) => {
					var s = r.generateUUID();
					this.promises.set(s, { resolve: e, reject: n });
					var o = new i(this.communicatorID);
					(o.promiseID = s), (o.info.portInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.port.postMessage(o)
							: window.safari.extension.dispatchMessage("port", o);
					} catch (t) {
						console.error(t);
					}
				});
			}
			static generateUUID() {
				var t = new Date().getTime();
				return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
					/[xy]/g,
					function (e) {
						var n = (t + 16 * Math.random()) % 16 | 0;
						return (
							(t = Math.floor(t / 16)),
							("x" == e ? n : (3 & n) | 8).toString(16)
						);
					}
				);
			}
			processResponse(t) {
				let e = t.message.data;
				if (
					-1 !==
					[
						"files_resolve",
						"tabs_resolve",
						"runtime_resolve",
						"port_resolve",
					].indexOf(t.name)
				) {
					if (t.message.communicatorID !== this.communicatorID) return;
					let n = t.message.promiseID;
					!1 === this.resolvePromise(n, e, null) &&
						console.warn(
							`Response with promiseID: ${n} cannot not found in map`
						);
				} else
					"runtime" === t.name
						? this.onMessageRuntimeRecived(e)
						: "onConnectTab" === t.name
						? this.onConnectTabRecieved(e)
						: "onConnectRuntime" === t.name
						? this.onConnectRuntimeRecieved(e)
						: "portMessage" === t.name
						? this.onPortMessageListenersRecieved(e)
						: "tabActions" === t.name
						? this.onTabActionRecieved(e)
						: console.warn("Recieved message with name:", t.name);
			}
			resolvePromise(t, e, n) {
				var i = this.promises.get(t);
				return (
					void 0 !== i &&
					(n ? i.reject(e) : i.resolve(e), this.promises.delete(t), !0)
				);
			}
			addListenerOnConnectTab(t) {
				this.onConnectListeners.tabs.push(t);
			}
			addListenerOnConnectRuntime(t) {
				this.onConnectListeners.runtime.push(t);
			}
			onConnectTabRecieved(t) {
				for (const e of this.onConnectListeners.tabs) e(t);
			}
			onConnectRuntimeRecieved(t) {
				for (const e of this.onConnectListeners.runtime) e(t);
			}
			addListenerOnPortMessageListener(t) {
				this.onPortMessageListeners.push(t);
			}
			onPortMessageListenersRecieved(t) {
				for (const e of this.onPortMessageListeners) e(t);
			}
			addListenerOnMessageRuntime(t) {
				this.messageRuntimeListeners.push(t);
			}
			onMessageRuntimeRecived(t) {
				for (const e of this.messageRuntimeListeners) e(t);
			}
			onTabActionRecieved(t) {
				for (const e of this.tabActionListeners) e(t);
			}
			addListenerTabAction(t) {
				this.tabActionListeners.push(t);
			}
		}
		(r.instance = null), (window.communicator = r.shared());
		e.a = r;
	},
	function (t, e) {
		var n = {}.hasOwnProperty;
		t.exports = function (t, e) {
			return n.call(t, e);
		};
	},
	function (t, e, n) {
		var i = n(37),
			r = n(53),
			s = n(10),
			o = n(8),
			a = n(64);
		t.exports = function (t, e) {
			var n = 1 == t,
				l = 2 == t,
				c = 3 == t,
				u = 4 == t,
				h = 6 == t,
				d = 5 == t || h,
				f = e || a;
			return function (e, a, p) {
				for (
					var g,
						v,
						_ = s(e),
						m = r(_),
						y = i(a, p, 3),
						b = o(m.length),
						w = 0,
						S = n ? f(e, b) : l ? f(e, 0) : void 0;
					b > w;
					w++
				)
					if ((d || w in m) && ((v = y((g = m[w]), w, _)), t))
						if (n) S[w] = v;
						else if (v)
							switch (t) {
								case 3:
									return !0;
								case 5:
									return g;
								case 6:
									return w;
								case 2:
									S.push(g);
							}
						else if (u) return !1;
				return h ? -1 : c || u ? u : S;
			};
		};
	},
	function (t, e, n) {
		var i = n(6),
			r = n(9),
			s = n(41);
		t.exports = i
			? function (t, e, n) {
					return r.f(t, e, s(1, n));
			  }
			: function (t, e, n) {
					return (t[e] = n), t;
			  };
	},
	function (t, e, n) {
		var i = n(6),
			r = n(52),
			s = n(41),
			o = n(18),
			a = n(27),
			l = n(12),
			c = n(104),
			u = Object.getOwnPropertyDescriptor;
		e.f = i
			? u
			: function (t, e) {
					if (((t = o(t)), (e = a(e, !0)), c))
						try {
							return u(t, e);
						} catch (t) {}
					if (l(t, e)) return s(!r.f.call(t, e), t[e]);
			  };
	},
	function (t, e, n) {
		var i = n(2),
			r = n(54),
			s = n(14),
			o = n(12),
			a = n(85),
			l = n(105),
			c = n(20),
			u = c.get,
			h = c.enforce,
			d = String(l).split("toString");
		r("inspectSource", function (t) {
			return l.call(t);
		}),
			(t.exports = function (t, e, n, r) {
				var l = !!r && !!r.unsafe,
					c = !!r && !!r.enumerable,
					u = !!r && !!r.noTargetGet;
				"function" == typeof n &&
					("string" != typeof e || o(n, "name") || s(n, "name", e),
					(h(n).source = d.join("string" == typeof e ? e : ""))),
					t !== i
						? (l ? !u && t[e] && (c = !0) : delete t[e],
						  c ? (t[e] = n) : s(t, e, n))
						: c
						? (t[e] = n)
						: a(e, n);
			})(Function.prototype, "toString", function () {
				return ("function" == typeof this && u(this).source) || l.call(this);
			});
	},
	function (t, e, n) {
		var i = n(70),
			r = n(12),
			s = n(110),
			o = n(9).f;
		t.exports = function (t) {
			var e = i.Symbol || (i.Symbol = {});
			r(e, t) || o(e, t, { value: s.f(t) });
		};
	},
	function (t, e, n) {
		var i = n(53),
			r = n(19);
		t.exports = function (t) {
			return i(r(t));
		};
	},
	function (t, e) {
		t.exports = function (t) {
			if (null == t) throw TypeError("Can't call method on " + t);
			return t;
		};
	},
	function (t, e, n) {
		var i,
			r,
			s,
			o = n(106),
			a = n(2),
			l = n(3),
			c = n(14),
			u = n(12),
			h = n(68),
			d = n(56),
			f = a.WeakMap;
		if (o) {
			var p = new f(),
				g = p.get,
				v = p.has,
				_ = p.set;
			(i = function (t, e) {
				return _.call(p, t, e), e;
			}),
				(r = function (t) {
					return g.call(p, t) || {};
				}),
				(s = function (t) {
					return v.call(p, t);
				});
		} else {
			var m = h("state");
			(d[m] = !0),
				(i = function (t, e) {
					return c(t, m, e), e;
				}),
				(r = function (t) {
					return u(t, m) ? t[m] : {};
				}),
				(s = function (t) {
					return u(t, m);
				});
		}
		t.exports = {
			set: i,
			get: r,
			has: s,
			enforce: function (t) {
				return s(t) ? r(t) : i(t, {});
			},
			getterFor: function (t) {
				return function (e) {
					var n;
					if (!l(e) || (n = r(e)).type !== t)
						throw TypeError("Incompatible receiver, " + t + " required");
					return n;
				};
			},
		};
	},
	function (t, e) {
		t.exports = function (t) {
			if ("function" != typeof t)
				throw TypeError(String(t) + " is not a function");
			return t;
		};
	},
	function (t, e, n) {
		var i = n(19),
			r = /"/g;
		t.exports = function (t, e, n, s) {
			var o = String(i(t)),
				a = "<" + e;
			return (
				"" !== n &&
					(a += " " + n + '="' + String(s).replace(r, "&quot;") + '"'),
				a + ">" + o + "</" + e + ">"
			);
		};
	},
	function (t, e, n) {
		var i = n(1);
		t.exports = function (t) {
			return i(function () {
				var e = ""[t]('"');
				return e !== e.toLowerCase() || e.split('"').length > 3;
			});
		};
	},
	function (t, e) {
		var n = Math.ceil,
			i = Math.floor;
		t.exports = function (t) {
			return isNaN((t = +t)) ? 0 : (t > 0 ? i : n)(t);
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(18),
			r = n(38),
			s = n(61),
			o = n(20),
			a = n(93),
			l = o.set,
			c = o.getterFor("Array Iterator");
		(t.exports = a(
			Array,
			"Array",
			function (t, e) {
				l(this, { type: "Array Iterator", target: i(t), index: 0, kind: e });
			},
			function () {
				var t = c(this),
					e = t.target,
					n = t.kind,
					i = t.index++;
				return !e || i >= e.length
					? ((t.target = void 0), { value: void 0, done: !0 })
					: "keys" == n
					? { value: i, done: !1 }
					: "values" == n
					? { value: e[i], done: !1 }
					: { value: [i, e[i]], done: !1 };
			},
			"values"
		)),
			(s.Arguments = s.Array),
			r("keys"),
			r("values"),
			r("entries");
	},
	function (t, e, n) {
		"use strict";
		n.d(e, "a", function () {
			return s;
		});
		var i = n(11),
			r = n(67);
		class s {
			constructor(t, e) {
				(this.uniqueID = void 0 === e ? "" : e),
					(this.type = t),
					(this.uuid = i.a.generateUUID()),
					(this.name = ""),
					(this.sender = null),
					(this.onMessage = new r.a()),
					(this.onDisconnect = new r.a()),
					(this.ready = !1),
					(this.postMessageQueue = []);
			}
			executeWaitingMessages() {
				var t = this.postMessageQueue.pop();
				this.sendMessage(t),
					0 !== this.postMessageQueue.length && this.executeWaitingMessages();
			}
			postMessage(t) {
				this.postMessageQueue.unshift(t),
					this.ready && this.executeWaitingMessages();
			}
			sendMessage(t) {
				var e = {
					action: s.ACTION.SEND,
					portUUID: this.uuid,
					type: this.type,
					uniqueID: this.uniqueID,
					data: t,
				};
				i.a
					.shared()
					.port(e)
					.then((t) => {})
					.catch((t) => {
						console.error(`Error runtime ${t}`);
					});
			}
			disconnect() {}
		}
		(s.TYPES = { TAB: 1, RUNTIME: 2 }),
			(s.ACTION = { CREATE: 1, SEND: 2, DISCONECT: 3 }),
			(s.RECIEVER = { CONTENT: 1, POPUP: 2 });
	},
	function (t, e, n) {
		var i = n(3);
		t.exports = function (t, e) {
			if (!i(t)) return t;
			var n, r;
			if (e && "function" == typeof (n = t.toString) && !i((r = n.call(t))))
				return r;
			if ("function" == typeof (n = t.valueOf) && !i((r = n.call(t)))) return r;
			if (!e && "function" == typeof (n = t.toString) && !i((r = n.call(t))))
				return r;
			throw TypeError("Can't convert object to primitive value");
		};
	},
	function (t, e) {
		var n = {}.toString;
		t.exports = function (t) {
			return n.call(t).slice(8, -1);
		};
	},
	function (t, e, n) {
		var i = n(9).f,
			r = n(12),
			s = n(7)("toStringTag");
		t.exports = function (t, e, n) {
			t &&
				!r((t = n ? t : t.prototype), s) &&
				i(t, s, { configurable: !0, value: e });
		};
	},
	function (t, e, n) {
		var i = n(12),
			r = n(10),
			s = n(68),
			o = n(90),
			a = s("IE_PROTO"),
			l = Object.prototype;
		t.exports = o
			? Object.getPrototypeOf
			: function (t) {
					return (
						(t = r(t)),
						i(t, a)
							? t[a]
							: "function" == typeof t.constructor && t instanceof t.constructor
							? t.constructor.prototype
							: t instanceof Object
							? l
							: null
					);
			  };
	},
	function (t, e, n) {
		"use strict";
		var i = n(1);
		t.exports = function (t, e) {
			var n = [][t];
			return (
				!n ||
				!i(function () {
					n.call(
						null,
						e ||
							function () {
								throw 1;
							},
						1
					);
				})
			);
		};
	},
	function (t, e, n) {
		var i = n(4),
			r = n(21),
			s = n(7)("species");
		t.exports = function (t, e) {
			var n,
				o = i(t).constructor;
			return void 0 === o || null == (n = i(o)[s]) ? e : r(n);
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(2),
			s = n(6),
			o = n(103),
			a = n(5),
			l = n(82),
			c = n(39),
			u = n(41),
			h = n(14),
			d = n(8),
			f = n(137),
			p = n(138),
			g = n(27),
			v = n(12),
			_ = n(63),
			m = n(3),
			y = n(36),
			b = n(48),
			w = n(42).f,
			S = n(139),
			E = n(13),
			T = n(49),
			L = n(9),
			A = n(15),
			I = n(20),
			C = I.get,
			M = I.set,
			x = L.f,
			D = A.f,
			O = E(0),
			R = r.RangeError,
			N = l.ArrayBuffer,
			P = l.DataView,
			k = a.NATIVE_ARRAY_BUFFER_VIEWS,
			V = a.TYPED_ARRAY_TAG,
			B = a.TypedArray,
			U = a.TypedArrayPrototype,
			F = a.aTypedArrayConstructor,
			q = a.isTypedArray,
			H = function (t, e) {
				for (var n = 0, i = e.length, r = new (F(t))(i); i > n; ) r[n] = e[n++];
				return r;
			},
			j = function (t, e) {
				x(t, e, {
					get: function () {
						return C(this)[e];
					},
				});
			},
			W = function (t) {
				var e;
				return (
					t instanceof N ||
					"ArrayBuffer" == (e = _(t)) ||
					"SharedArrayBuffer" == e
				);
			},
			G = function (t, e) {
				return (
					q(t) && "symbol" != typeof e && e in t && String(+e) == String(e)
				);
			},
			Y = function (t, e) {
				return G(t, (e = g(e, !0))) ? u(2, t[e]) : D(t, e);
			},
			K = function (t, e, n) {
				return !(G(t, (e = g(e, !0))) && m(n) && v(n, "value")) ||
					v(n, "get") ||
					v(n, "set") ||
					n.configurable ||
					(v(n, "writable") && !n.writable) ||
					(v(n, "enumerable") && !n.enumerable)
					? x(t, e, n)
					: ((t[e] = n.value), t);
			};
		s
			? (k ||
					((A.f = Y),
					(L.f = K),
					j(U, "buffer"),
					j(U, "byteOffset"),
					j(U, "byteLength"),
					j(U, "length")),
			  i(
					{ target: "Object", stat: !0, forced: !k },
					{ getOwnPropertyDescriptor: Y, defineProperty: K }
			  ),
			  (t.exports = function (t, e, n, s) {
					var a = t + (s ? "Clamped" : "") + "Array",
						l = "get" + t,
						u = "set" + t,
						g = r[a],
						v = g,
						_ = v && v.prototype,
						E = {},
						L = function (t, n) {
							x(t, n, {
								get: function () {
									return (function (t, n) {
										var i = C(t);
										return i.view[l](n * e + i.byteOffset, !0);
									})(this, n);
								},
								set: function (t) {
									return (function (t, n, i) {
										var r = C(t);
										s &&
											(i =
												(i = Math.round(i)) < 0 ? 0 : i > 255 ? 255 : 255 & i),
											r.view[u](n * e + r.byteOffset, i, !0);
									})(this, n, t);
								},
								enumerable: !0,
							});
						};
					k
						? o &&
						  ((v = n(function (t, n, i, r) {
								return (
									c(t, v, a),
									m(n)
										? W(n)
											? void 0 !== r
												? new g(n, p(i, e), r)
												: void 0 !== i
												? new g(n, p(i, e))
												: new g(n)
											: q(n)
											? H(v, n)
											: S.call(v, n)
										: new g(f(n))
								);
						  })),
						  b && b(v, B),
						  O(w(g), function (t) {
								t in v || h(v, t, g[t]);
						  }),
						  (v.prototype = _))
						: ((v = n(function (t, n, i, r) {
								c(t, v, a);
								var s,
									o,
									l,
									u = 0,
									h = 0;
								if (m(n)) {
									if (!W(n)) return q(n) ? H(v, n) : S.call(v, n);
									(s = n), (h = p(i, e));
									var g = n.byteLength;
									if (void 0 === r) {
										if (g % e) throw R("Wrong length");
										if ((o = g - h) < 0) throw R("Wrong length");
									} else if ((o = d(r) * e) + h > g) throw R("Wrong length");
									l = o / e;
								} else (l = f(n)), (s = new N((o = l * e)));
								for (
									M(t, {
										buffer: s,
										byteOffset: h,
										byteLength: o,
										length: l,
										view: new P(s),
									});
									u < l;

								)
									L(t, u++);
						  })),
						  b && b(v, B),
						  (_ = v.prototype = y(U))),
						_.constructor !== v && h(_, "constructor", v),
						V && h(_, V, a),
						(E[a] = v),
						i({ global: !0, forced: v != g, sham: !k }, E),
						"BYTES_PER_ELEMENT" in v || h(v, "BYTES_PER_ELEMENT", e),
						"BYTES_PER_ELEMENT" in _ || h(_, "BYTES_PER_ELEMENT", e),
						T(a);
			  }))
			: (t.exports = function () {});
	},
	function (t, e) {
		t.exports = !1;
	},
	function (t, e, n) {
		var i = n(24),
			r = Math.max,
			s = Math.min;
		t.exports = function (t, e) {
			var n = i(t);
			return n < 0 ? r(n + e, 0) : s(n, e);
		};
	},
	function (t, e, n) {
		var i = n(4),
			r = n(88),
			s = n(87),
			o = n(56),
			a = n(111),
			l = n(84),
			c = n(68)("IE_PROTO"),
			u = function () {},
			h = function () {
				var t,
					e = l("iframe"),
					n = s.length;
				for (
					e.style.display = "none",
						a.appendChild(e),
						e.src = String("javascript:"),
						(t = e.contentWindow.document).open(),
						t.write("<script>document.F=Object</script>"),
						t.close(),
						h = t.F;
					n--;

				)
					delete h.prototype[s[n]];
				return h();
			};
		(t.exports =
			Object.create ||
			function (t, e) {
				var n;
				return (
					null !== t
						? ((u.prototype = i(t)),
						  (n = new u()),
						  (u.prototype = null),
						  (n[c] = t))
						: (n = h()),
					void 0 === e ? n : r(n, e)
				);
			}),
			(o[c] = !0);
	},
	function (t, e, n) {
		var i = n(21);
		t.exports = function (t, e, n) {
			if ((i(t), void 0 === e)) return t;
			switch (n) {
				case 0:
					return function () {
						return t.call(e);
					};
				case 1:
					return function (n) {
						return t.call(e, n);
					};
				case 2:
					return function (n, i) {
						return t.call(e, n, i);
					};
				case 3:
					return function (n, i, r) {
						return t.call(e, n, i, r);
					};
			}
			return function () {
				return t.apply(e, arguments);
			};
		};
	},
	function (t, e, n) {
		var i = n(7),
			r = n(36),
			s = n(14),
			o = i("unscopables"),
			a = Array.prototype;
		null == a[o] && s(a, o, r(null)),
			(t.exports = function (t) {
				a[o][t] = !0;
			});
	},
	function (t, e) {
		t.exports = function (t, e, n) {
			if (!(t instanceof e))
				throw TypeError("Incorrect " + (n ? n + " " : "") + "invocation");
			return t;
		};
	},
	function (t, e, n) {
		var i = n(2),
			r = n(355),
			s = n(25),
			o = n(14),
			a = n(7),
			l = a("iterator"),
			c = a("toStringTag"),
			u = s.values;
		for (var h in r) {
			var d = i[h],
				f = d && d.prototype;
			if (f) {
				if (f[l] !== u)
					try {
						o(f, l, u);
					} catch (t) {
						f[l] = u;
					}
				if ((f[c] || o(f, c, h), r[h]))
					for (var p in s)
						if (f[p] !== s[p])
							try {
								o(f, p, s[p]);
							} catch (t) {
								f[p] = s[p];
							}
			}
		}
	},
	function (t, e) {
		t.exports = function (t, e) {
			return {
				enumerable: !(1 & t),
				configurable: !(2 & t),
				writable: !(4 & t),
				value: e,
			};
		};
	},
	function (t, e, n) {
		var i = n(108),
			r = n(87).concat("length", "prototype");
		e.f =
			Object.getOwnPropertyNames ||
			function (t) {
				return i(t, r);
			};
	},
	function (t, e, n) {
		var i = n(28);
		t.exports =
			Array.isArray ||
			function (t) {
				return "Array" == i(t);
			};
	},
	function (t, e, n) {
		var i = n(56),
			r = n(3),
			s = n(12),
			o = n(9).f,
			a = n(55),
			l = n(59),
			c = a("meta"),
			u = 0,
			h =
				Object.isExtensible ||
				function () {
					return !0;
				},
			d = function (t) {
				o(t, c, { value: { objectID: "O" + ++u, weakData: {} } });
			},
			f = (t.exports = {
				REQUIRED: !1,
				fastKey: function (t, e) {
					if (!r(t))
						return "symbol" == typeof t
							? t
							: ("string" == typeof t ? "S" : "P") + t;
					if (!s(t, c)) {
						if (!h(t)) return "F";
						if (!e) return "E";
						d(t);
					}
					return t[c].objectID;
				},
				getWeakData: function (t, e) {
					if (!s(t, c)) {
						if (!h(t)) return !0;
						if (!e) return !1;
						d(t);
					}
					return t[c].weakData;
				},
				onFreeze: function (t) {
					return l && f.REQUIRED && h(t) && !s(t, c) && d(t), t;
				},
			});
		i[c] = !0;
	},
	function (t, e, n) {
		"use strict";
		var i = n(27),
			r = n(9),
			s = n(41);
		t.exports = function (t, e, n) {
			var o = i(e);
			o in t ? r.f(t, o, s(0, n)) : (t[o] = n);
		};
	},
	function (t, e, n) {
		"use strict";
		var i,
			r,
			s,
			o = n(0),
			a = n(34),
			l = n(2),
			c = n(70),
			u = n(51),
			h = n(29),
			d = n(49),
			f = n(3),
			p = n(21),
			g = n(39),
			v = n(28),
			_ = n(60),
			m = n(72),
			y = n(32),
			b = n(132).set,
			w = n(295),
			S = n(133),
			E = n(296),
			T = n(134),
			L = n(297),
			A = n(99),
			I = n(20),
			C = n(58),
			M = n(7)("species"),
			x = "Promise",
			D = I.get,
			O = I.set,
			R = I.getterFor(x),
			N = l.Promise,
			P = l.TypeError,
			k = l.document,
			V = l.process,
			B = l.fetch,
			U = V && V.versions,
			F = (U && U.v8) || "",
			q = T.f,
			H = q,
			j = "process" == v(V),
			W = !!(k && k.createEvent && l.dispatchEvent),
			G = C(x, function () {
				var t = N.resolve(1),
					e = function () {},
					n = ((t.constructor = {})[M] = function (t) {
						t(e, e);
					});
				return !(
					(j || "function" == typeof PromiseRejectionEvent) &&
					(!a || t.finally) &&
					t.then(e) instanceof n &&
					0 !== F.indexOf("6.6") &&
					-1 === A.indexOf("Chrome/66")
				);
			}),
			Y =
				G ||
				!m(function (t) {
					N.all(t).catch(function () {});
				}),
			K = function (t) {
				var e;
				return !(!f(t) || "function" != typeof (e = t.then)) && e;
			},
			z = function (t, e, n) {
				if (!e.notified) {
					e.notified = !0;
					var i = e.reactions;
					w(function () {
						for (
							var r = e.value,
								s = 1 == e.state,
								o = 0,
								a = function (n) {
									var i,
										o,
										a,
										l = s ? n.ok : n.fail,
										c = n.resolve,
										u = n.reject,
										h = n.domain;
									try {
										l
											? (s || (2 === e.rejection && Z(t, e), (e.rejection = 1)),
											  !0 === l
													? (i = r)
													: (h && h.enter(),
													  (i = l(r)),
													  h && (h.exit(), (a = !0))),
											  i === n.promise
													? u(P("Promise-chain cycle"))
													: (o = K(i))
													? o.call(i, c, u)
													: c(i))
											: u(r);
									} catch (t) {
										h && !a && h.exit(), u(t);
									}
								};
							i.length > o;

						)
							a(i[o++]);
						(e.reactions = []), (e.notified = !1), n && !e.rejection && $(t, e);
					});
				}
			},
			X = function (t, e, n) {
				var i, r;
				W
					? (((i = k.createEvent("Event")).promise = e),
					  (i.reason = n),
					  i.initEvent(t, !1, !0),
					  l.dispatchEvent(i))
					: (i = { promise: e, reason: n }),
					(r = l["on" + t])
						? r(i)
						: "unhandledrejection" === t && E("Unhandled promise rejection", n);
			},
			$ = function (t, e) {
				b.call(l, function () {
					var n,
						i = e.value;
					if (
						J(e) &&
						((n = L(function () {
							j
								? V.emit("unhandledRejection", i, t)
								: X("unhandledrejection", t, i);
						})),
						(e.rejection = j || J(e) ? 2 : 1),
						n.error)
					)
						throw n.value;
				});
			},
			J = function (t) {
				return 1 !== t.rejection && !t.parent;
			},
			Z = function (t, e) {
				b.call(l, function () {
					j ? V.emit("rejectionHandled", t) : X("rejectionhandled", t, e.value);
				});
			},
			Q = function (t, e, n, i) {
				return function (r) {
					t(e, n, r, i);
				};
			},
			tt = function (t, e, n, i) {
				e.done ||
					((e.done = !0),
					i && (e = i),
					(e.value = n),
					(e.state = 2),
					z(t, e, !0));
			},
			et = function (t, e, n, i) {
				if (!e.done) {
					(e.done = !0), i && (e = i);
					try {
						if (t === n) throw P("Promise can't be resolved itself");
						var r = K(n);
						r
							? w(function () {
									var i = { done: !1 };
									try {
										r.call(n, Q(et, t, i, e), Q(tt, t, i, e));
									} catch (n) {
										tt(t, i, n, e);
									}
							  })
							: ((e.value = n), (e.state = 1), z(t, e, !1));
					} catch (n) {
						tt(t, { done: !1 }, n, e);
					}
				}
			};
		G &&
			((N = function (t) {
				g(this, N, x), p(t), i.call(this);
				var e = D(this);
				try {
					t(Q(et, this, e), Q(tt, this, e));
				} catch (t) {
					tt(this, e, t);
				}
			}),
			((i = function (t) {
				O(this, {
					type: x,
					done: !1,
					notified: !1,
					parent: !1,
					reactions: [],
					rejection: !1,
					state: 0,
					value: void 0,
				});
			}).prototype = u(N.prototype, {
				then: function (t, e) {
					var n = R(this),
						i = q(y(this, N));
					return (
						(i.ok = "function" != typeof t || t),
						(i.fail = "function" == typeof e && e),
						(i.domain = j ? V.domain : void 0),
						(n.parent = !0),
						n.reactions.push(i),
						0 != n.state && z(this, n, !1),
						i.promise
					);
				},
				catch: function (t) {
					return this.then(void 0, t);
				},
			})),
			(r = function () {
				var t = new i(),
					e = D(t);
				(this.promise = t),
					(this.resolve = Q(et, t, e)),
					(this.reject = Q(tt, t, e));
			}),
			(T.f = q = function (t) {
				return t === N || t === s ? new r(t) : H(t);
			}),
			a ||
				"function" != typeof B ||
				o(
					{ global: !0, enumerable: !0, forced: !0 },
					{
						fetch: function (t) {
							return S(N, B.apply(l, arguments));
						},
					}
				)),
			o({ global: !0, wrap: !0, forced: G }, { Promise: N }),
			h(N, x, !1, !0),
			d(x),
			(s = c.Promise),
			o(
				{ target: x, stat: !0, forced: G },
				{
					reject: function (t) {
						var e = q(this);
						return e.reject.call(void 0, t), e.promise;
					},
				}
			),
			o(
				{ target: x, stat: !0, forced: a || G },
				{
					resolve: function (t) {
						return S(a && this === s ? N : this, t);
					},
				}
			),
			o(
				{ target: x, stat: !0, forced: Y },
				{
					all: function (t) {
						var e = this,
							n = q(e),
							i = n.resolve,
							r = n.reject,
							s = L(function () {
								var n = p(e.resolve),
									s = [],
									o = 0,
									a = 1;
								_(t, function (t) {
									var l = o++,
										c = !1;
									s.push(void 0),
										a++,
										n.call(e, t).then(function (t) {
											c || ((c = !0), (s[l] = t), --a || i(s));
										}, r);
								}),
									--a || i(s);
							});
						return s.error && r(s.value), n.promise;
					},
					race: function (t) {
						var e = this,
							n = q(e),
							i = n.reject,
							r = L(function () {
								var r = p(e.resolve);
								_(t, function (t) {
									r.call(e, t).then(n.resolve, i);
								});
							});
						return r.error && i(r.value), n.promise;
					},
				}
			);
	},
	function (t, e, n) {
		var i = n(108),
			r = n(87);
		t.exports =
			Object.keys ||
			function (t) {
				return i(t, r);
			};
	},
	function (t, e, n) {
		var i = n(117);
		t.exports =
			Object.setPrototypeOf ||
			("__proto__" in {}
				? (function () {
						var t,
							e = !1,
							n = {};
						try {
							(t = Object.getOwnPropertyDescriptor(
								Object.prototype,
								"__proto__"
							).set).call(n, []),
								(e = n instanceof Array);
						} catch (t) {}
						return function (n, r) {
							return i(n, r), e ? t.call(n, r) : (n.__proto__ = r), n;
						};
				  })()
				: void 0);
	},
	function (t, e, n) {
		"use strict";
		var i = n(92),
			r = n(9),
			s = n(7),
			o = n(6),
			a = s("species");
		t.exports = function (t) {
			var e = i(t),
				n = r.f;
			o &&
				e &&
				!e[a] &&
				n(e, a, {
					configurable: !0,
					get: function () {
						return this;
					},
				});
		};
	},
	function (t, e, n) {
		var i = n(19),
			r = "[" + n(79) + "]",
			s = RegExp("^" + r + r + "*"),
			o = RegExp(r + r + "*$");
		t.exports = function (t, e) {
			return (
				(t = String(i(t))),
				1 & e && (t = t.replace(s, "")),
				2 & e && (t = t.replace(o, "")),
				t
			);
		};
	},
	function (t, e, n) {
		var i = n(16);
		t.exports = function (t, e, n) {
			for (var r in e) i(t, r, e[r], n);
			return t;
		};
	},
	function (t, e, n) {
		"use strict";
		var i = {}.propertyIsEnumerable,
			r = Object.getOwnPropertyDescriptor,
			s = r && !i.call({ 1: 2 }, 1);
		e.f = s
			? function (t) {
					var e = r(this, t);
					return !!e && e.enumerable;
			  }
			: i;
	},
	function (t, e, n) {
		var i = n(1),
			r = n(28),
			s = "".split;
		t.exports = i(function () {
			return !Object("z").propertyIsEnumerable(0);
		})
			? function (t) {
					return "String" == r(t) ? s.call(t, "") : Object(t);
			  }
			: Object;
	},
	function (t, e, n) {
		var i = n(2),
			r = n(85),
			s = n(34),
			o = i["__core-js_shared__"] || r("__core-js_shared__", {});
		(t.exports = function (t, e) {
			return o[t] || (o[t] = void 0 !== e ? e : {});
		})("versions", []).push({
			version: "3.1.3",
			mode: s ? "pure" : "global",
			copyright: "© 2019 Denis Pushkarev (zloirock.ru)",
		});
	},
	function (t, e) {
		var n = 0,
			i = Math.random();
		t.exports = function (t) {
			return "Symbol(".concat(
				void 0 === t ? "" : t,
				")_",
				(++n + i).toString(36)
			);
		};
	},
	function (t, e) {
		t.exports = {};
	},
	function (t, e, n) {
		var i = n(18),
			r = n(8),
			s = n(35);
		t.exports = function (t) {
			return function (e, n, o) {
				var a,
					l = i(e),
					c = r(l.length),
					u = s(o, c);
				if (t && n != n) {
					for (; c > u; ) if ((a = l[u++]) != a) return !0;
				} else
					for (; c > u; u++)
						if ((t || u in l) && l[u] === n) return t || u || 0;
				return !t && -1;
			};
		};
	},
	function (t, e, n) {
		var i = n(1),
			r = /#|\.prototype\./,
			s = function (t, e) {
				var n = a[o(t)];
				return n == c || (n != l && ("function" == typeof e ? i(e) : !!e));
			},
			o = (s.normalize = function (t) {
				return String(t).replace(r, ".").toLowerCase();
			}),
			a = (s.data = {}),
			l = (s.NATIVE = "N"),
			c = (s.POLYFILL = "P");
		t.exports = s;
	},
	function (t, e, n) {
		var i = n(1);
		t.exports = !i(function () {
			return Object.isExtensible(Object.preventExtensions({}));
		});
	},
	function (t, e, n) {
		var i = n(4),
			r = n(89),
			s = n(8),
			o = n(37),
			a = n(62),
			l = n(115),
			c = {};
		(t.exports = function (t, e, n, u, h) {
			var d,
				f,
				p,
				g,
				v,
				_ = o(e, n, u ? 2 : 1);
			if (h) d = t;
			else {
				if ("function" != typeof (f = a(t)))
					throw TypeError("Target is not iterable");
				if (r(f)) {
					for (p = 0, g = s(t.length); g > p; p++)
						if ((u ? _(i((v = t[p]))[0], v[1]) : _(t[p])) === c) return c;
					return;
				}
				d = f.call(t);
			}
			for (; !(v = d.next()).done; ) if (l(d, _, v.value, u) === c) return c;
		}).BREAK = c;
	},
	function (t, e) {
		t.exports = {};
	},
	function (t, e, n) {
		var i = n(63),
			r = n(61),
			s = n(7)("iterator");
		t.exports = function (t) {
			if (null != t) return t[s] || t["@@iterator"] || r[i(t)];
		};
	},
	function (t, e, n) {
		var i = n(28),
			r = n(7)("toStringTag"),
			s =
				"Arguments" ==
				i(
					(function () {
						return arguments;
					})()
				);
		t.exports = function (t) {
			var e, n, o;
			return void 0 === t
				? "Undefined"
				: null === t
				? "Null"
				: "string" ==
				  typeof (n = (function (t, e) {
						try {
							return t[e];
						} catch (t) {}
				  })((e = Object(t)), r))
				? n
				: s
				? i(e)
				: "Object" == (o = i(e)) && "function" == typeof e.callee
				? "Arguments"
				: o;
		};
	},
	function (t, e, n) {
		var i = n(3),
			r = n(43),
			s = n(7)("species");
		t.exports = function (t, e) {
			var n;
			return (
				r(t) &&
					("function" != typeof (n = t.constructor) ||
					(n !== Array && !r(n.prototype))
						? i(n) && null === (n = n[s]) && (n = void 0)
						: (n = void 0)),
				new (void 0 === n ? Array : n)(0 === e ? 0 : e)
			);
		};
	},
	function (t, e, n) {
		var i = n(1),
			r = n(7)("species");
		t.exports = function (t) {
			return !i(function () {
				var e = [];
				return (
					((e.constructor = {})[r] = function () {
						return { foo: 1 };
					}),
					1 !== e[t](Boolean).foo
				);
			});
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(4);
		t.exports = function () {
			var t = i(this),
				e = "";
			return (
				t.global && (e += "g"),
				t.ignoreCase && (e += "i"),
				t.multiline && (e += "m"),
				t.unicode && (e += "u"),
				t.sticky && (e += "y"),
				e
			);
		};
	},
	function (t, e, n) {
		"use strict";
		n.d(e, "a", function () {
			return i;
		});
		n(25), n(40);
		class i {
			constructor() {
				this.listeners = [];
			}
			addListener(t) {
				this.listeners.push(t);
			}
			removeListener(t) {
				var e = this.listeners.indexOf(t);
				e > -1 && this.listeners.splice(e, 1);
			}
			hasListener(t) {
				return this.listeners.indexOf(t) > -1;
			}
			executeListeners(t) {
				for (const e of this.listeners) e(t);
			}
		}
	},
	function (t, e, n) {
		var i = n(54),
			r = n(55),
			s = i("keys");
		t.exports = function (t) {
			return s[t] || (s[t] = r(t));
		};
	},
	function (t, e) {
		e.f = Object.getOwnPropertySymbols;
	},
	function (t, e, n) {
		t.exports = n(2);
	},
	function (t, e, n) {
		"use strict";
		var i = n(34),
			r = n(2),
			s = n(1);
		t.exports =
			i ||
			!s(function () {
				var t = Math.random();
				__defineSetter__.call(null, t, function () {}), delete r[t];
			});
	},
	function (t, e, n) {
		var i = n(7)("iterator"),
			r = !1;
		try {
			var s = 0,
				o = {
					next: function () {
						return { done: !!s++ };
					},
					return: function () {
						r = !0;
					},
				};
			(o[i] = function () {
				return this;
			}),
				Array.from(o, function () {
					throw 2;
				});
		} catch (t) {}
		t.exports = function (t, e) {
			if (!e && !r) return !1;
			var n = !1;
			try {
				var s = {};
				(s[i] = function () {
					return {
						next: function () {
							return { done: (n = !0) };
						},
					};
				}),
					t(s);
			} catch (t) {}
			return n;
		};
	},
	function (t, e, n) {
		var i = n(21),
			r = n(10),
			s = n(53),
			o = n(8);
		t.exports = function (t, e, n, a, l) {
			i(e);
			var c = r(t),
				u = s(c),
				h = o(c.length),
				d = l ? h - 1 : 0,
				f = l ? -1 : 1;
			if (n < 2)
				for (;;) {
					if (d in u) {
						(a = u[d]), (d += f);
						break;
					}
					if (((d += f), l ? d < 0 : h <= d))
						throw TypeError("Reduce of empty array with no initial value");
				}
			for (; l ? d >= 0 : h > d; d += f) d in u && (a = e(a, u[d], d, c));
			return a;
		};
	},
	function (t, e, n) {
		var i = n(24),
			r = n(19);
		t.exports = function (t, e, n) {
			var s,
				o,
				a = String(r(t)),
				l = i(e),
				c = a.length;
			return l < 0 || l >= c
				? n
					? ""
					: void 0
				: (s = a.charCodeAt(l)) < 55296 ||
				  s > 56319 ||
				  l + 1 === c ||
				  (o = a.charCodeAt(l + 1)) < 56320 ||
				  o > 57343
				? n
					? a.charAt(l)
					: s
				: n
				? a.slice(l, l + 2)
				: o - 56320 + ((s - 55296) << 10) + 65536;
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(14),
			r = n(16),
			s = n(1),
			o = n(7),
			a = n(76),
			l = o("species"),
			c = !s(function () {
				var t = /./;
				return (
					(t.exec = function () {
						var t = [];
						return (t.groups = { a: "7" }), t;
					}),
					"7" !== "".replace(t, "$<a>")
				);
			}),
			u = !s(function () {
				var t = /(?:)/,
					e = t.exec;
				t.exec = function () {
					return e.apply(this, arguments);
				};
				var n = "ab".split(t);
				return 2 !== n.length || "a" !== n[0] || "b" !== n[1];
			});
		t.exports = function (t, e, n, h) {
			var d = o(t),
				f = !s(function () {
					var e = {};
					return (
						(e[d] = function () {
							return 7;
						}),
						7 != ""[t](e)
					);
				}),
				p =
					f &&
					!s(function () {
						var e = !1,
							n = /a/;
						return (
							(n.exec = function () {
								return (e = !0), null;
							}),
							"split" === t &&
								((n.constructor = {}),
								(n.constructor[l] = function () {
									return n;
								})),
							n[d](""),
							!e
						);
					});
			if (!f || !p || ("replace" === t && !c) || ("split" === t && !u)) {
				var g = /./[d],
					v = n(d, ""[t], function (t, e, n, i, r) {
						return e.exec === a
							? f && !r
								? { done: !0, value: g.call(e, n, i) }
								: { done: !0, value: t.call(n, e, i) }
							: { done: !1 };
					}),
					_ = v[0],
					m = v[1];
				r(String.prototype, t, _),
					r(
						RegExp.prototype,
						d,
						2 == e
							? function (t, e) {
									return m.call(t, this, e);
							  }
							: function (t) {
									return m.call(t, this);
							  }
					),
					h && i(RegExp.prototype[d], "sham", !0);
			}
		};
	},
	function (t, e, n) {
		"use strict";
		var i,
			r,
			s = n(66),
			o = RegExp.prototype.exec,
			a = String.prototype.replace,
			l = o,
			c =
				((i = /a/),
				(r = /b*/g),
				o.call(i, "a"),
				o.call(r, "a"),
				0 !== i.lastIndex || 0 !== r.lastIndex),
			u = void 0 !== /()??/.exec("")[1];
		(c || u) &&
			(l = function (t) {
				var e,
					n,
					i,
					r,
					l = this;
				return (
					u && (n = new RegExp("^" + l.source + "$(?!\\s)", s.call(l))),
					c && (e = l.lastIndex),
					(i = o.call(l, t)),
					c && i && (l.lastIndex = l.global ? i.index + i[0].length : e),
					u &&
						i &&
						i.length > 1 &&
						a.call(i[0], n, function () {
							for (r = 1; r < arguments.length - 2; r++)
								void 0 === arguments[r] && (i[r] = void 0);
						}),
					i
				);
			}),
			(t.exports = l);
	},
	function (t, e, n) {
		"use strict";
		var i = n(74);
		t.exports = function (t, e, n) {
			return e + (n ? i(t, e, !0).length : 1);
		};
	},
	function (t, e, n) {
		var i = n(28),
			r = n(76);
		t.exports = function (t, e) {
			var n = t.exec;
			if ("function" == typeof n) {
				var s = n.call(t, e);
				if ("object" != typeof s)
					throw TypeError(
						"RegExp exec method returned something other than an Object or null"
					);
				return s;
			}
			if ("RegExp" !== i(t))
				throw TypeError("RegExp#exec called on incompatible receiver");
			return r.call(t, e);
		};
	},
	function (t, e) {
		t.exports = "\t\n\v\f\r                　\u2028\u2029\ufeff";
	},
	function (t, e) {
		var n = Math.expm1;
		t.exports =
			!n ||
			n(10) > 22025.465794806718 ||
			n(10) < 22025.465794806718 ||
			-2e-17 != n(-2e-17)
				? function (t) {
						return 0 == (t = +t)
							? t
							: t > -1e-6 && t < 1e-6
							? t + (t * t) / 2
							: Math.exp(t) - 1;
				  }
				: n;
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(2),
			s = n(58),
			o = n(16),
			a = n(44),
			l = n(60),
			c = n(39),
			u = n(3),
			h = n(1),
			d = n(72),
			f = n(29),
			p = n(101);
		t.exports = function (t, e, n, g, v) {
			var _ = r[t],
				m = _ && _.prototype,
				y = _,
				b = g ? "set" : "add",
				w = {},
				S = function (t) {
					var e = m[t];
					o(
						m,
						t,
						"add" == t
							? function (t) {
									return e.call(this, 0 === t ? 0 : t), this;
							  }
							: "delete" == t
							? function (t) {
									return !(v && !u(t)) && e.call(this, 0 === t ? 0 : t);
							  }
							: "get" == t
							? function (t) {
									return v && !u(t) ? void 0 : e.call(this, 0 === t ? 0 : t);
							  }
							: "has" == t
							? function (t) {
									return !(v && !u(t)) && e.call(this, 0 === t ? 0 : t);
							  }
							: function (t, n) {
									return e.call(this, 0 === t ? 0 : t, n), this;
							  }
					);
				};
			if (
				s(
					t,
					"function" != typeof _ ||
						!(
							v ||
							(m.forEach &&
								!h(function () {
									new _().entries().next();
								}))
						)
				)
			)
				(y = n.getConstructor(e, t, g, b)), (a.REQUIRED = !0);
			else if (s(t, !0)) {
				var E = new y(),
					T = E[b](v ? {} : -0, 1) != E,
					L = h(function () {
						E.has(1);
					}),
					A = d(function (t) {
						new _(t);
					}),
					I =
						!v &&
						h(function () {
							for (var t = new _(), e = 5; e--; ) t[b](e, e);
							return !t.has(-0);
						});
				A ||
					(((y = e(function (e, n) {
						c(e, y, t);
						var i = p(new _(), e, y);
						return null != n && l(n, i[b], i, g), i;
					})).prototype = m),
					(m.constructor = y)),
					(L || I) && (S("delete"), S("has"), g && S("get")),
					(I || T) && S(b),
					v && m.clear && delete m.clear;
			}
			return (
				(w[t] = y),
				i({ global: !0, forced: y != _ }, w),
				f(y, t),
				v || n.setStrong(y, t, g),
				y
			);
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(2),
			r = n(6),
			s = n(5).NATIVE_ARRAY_BUFFER,
			o = n(14),
			a = n(51),
			l = n(1),
			c = n(39),
			u = n(24),
			h = n(8),
			d = n(137),
			f = n(42).f,
			p = n(9).f,
			g = n(91),
			v = n(29),
			_ = n(20),
			m = _.get,
			y = _.set,
			b = i.ArrayBuffer,
			w = b,
			S = i.DataView,
			E = i.Math,
			T = i.RangeError,
			L = E.abs,
			A = E.pow,
			I = E.floor,
			C = E.log,
			M = E.LN2,
			x = function (t, e, n) {
				var i,
					r,
					s,
					o = new Array(n),
					a = 8 * n - e - 1,
					l = (1 << a) - 1,
					c = l >> 1,
					u = 23 === e ? A(2, -24) - A(2, -77) : 0,
					h = t < 0 || (0 === t && 1 / t < 0) ? 1 : 0,
					d = 0;
				for (
					(t = L(t)) != t || t === 1 / 0
						? ((r = t != t ? 1 : 0), (i = l))
						: ((i = I(C(t) / M)),
						  t * (s = A(2, -i)) < 1 && (i--, (s *= 2)),
						  (t += i + c >= 1 ? u / s : u * A(2, 1 - c)) * s >= 2 &&
								(i++, (s /= 2)),
						  i + c >= l
								? ((r = 0), (i = l))
								: i + c >= 1
								? ((r = (t * s - 1) * A(2, e)), (i += c))
								: ((r = t * A(2, c - 1) * A(2, e)), (i = 0)));
					e >= 8;
					o[d++] = 255 & r, r /= 256, e -= 8
				);
				for (
					i = (i << e) | r, a += e;
					a > 0;
					o[d++] = 255 & i, i /= 256, a -= 8
				);
				return (o[--d] |= 128 * h), o;
			},
			D = function (t, e) {
				var n,
					i = t.length,
					r = 8 * i - e - 1,
					s = (1 << r) - 1,
					o = s >> 1,
					a = r - 7,
					l = i - 1,
					c = t[l--],
					u = 127 & c;
				for (c >>= 7; a > 0; u = 256 * u + t[l], l--, a -= 8);
				for (
					n = u & ((1 << -a) - 1), u >>= -a, a += e;
					a > 0;
					n = 256 * n + t[l], l--, a -= 8
				);
				if (0 === u) u = 1 - o;
				else {
					if (u === s) return n ? NaN : c ? -1 / 0 : 1 / 0;
					(n += A(2, e)), (u -= o);
				}
				return (c ? -1 : 1) * n * A(2, u - e);
			},
			O = function (t) {
				return (t[3] << 24) | (t[2] << 16) | (t[1] << 8) | t[0];
			},
			R = function (t) {
				return [255 & t];
			},
			N = function (t) {
				return [255 & t, (t >> 8) & 255];
			},
			P = function (t) {
				return [255 & t, (t >> 8) & 255, (t >> 16) & 255, (t >> 24) & 255];
			},
			k = function (t) {
				return x(t, 23, 4);
			},
			V = function (t) {
				return x(t, 52, 8);
			},
			B = function (t, e) {
				p(t.prototype, e, {
					get: function () {
						return m(this)[e];
					},
				});
			},
			U = function (t, e, n, i) {
				var r = d(+n),
					s = m(t);
				if (r + e > s.byteLength) throw T("Wrong index");
				var o = m(s.buffer).bytes,
					a = r + s.byteOffset,
					l = o.slice(a, a + e);
				return i ? l : l.reverse();
			},
			F = function (t, e, n, i, r, s) {
				var o = d(+n),
					a = m(t);
				if (o + e > a.byteLength) throw T("Wrong index");
				for (
					var l = m(a.buffer).bytes, c = o + a.byteOffset, u = i(+r), h = 0;
					h < e;
					h++
				)
					l[c + h] = u[s ? h : e - h - 1];
			};
		if (s) {
			if (
				!l(function () {
					b(1);
				}) ||
				!l(function () {
					new b(-1);
				}) ||
				l(function () {
					return new b(), new b(1.5), new b(NaN), "ArrayBuffer" != b.name;
				})
			) {
				for (
					var q,
						H = ((w = function (t) {
							return c(this, w), new b(d(t));
						}).prototype = b.prototype),
						j = f(b),
						W = 0;
					j.length > W;

				)
					(q = j[W++]) in w || o(w, q, b[q]);
				H.constructor = w;
			}
			var G = new S(new w(2)),
				Y = S.prototype.setInt8;
			G.setInt8(0, 2147483648),
				G.setInt8(1, 2147483649),
				(!G.getInt8(0) && G.getInt8(1)) ||
					a(
						S.prototype,
						{
							setInt8: function (t, e) {
								Y.call(this, t, (e << 24) >> 24);
							},
							setUint8: function (t, e) {
								Y.call(this, t, (e << 24) >> 24);
							},
						},
						{ unsafe: !0 }
					);
		} else
			(w = function (t) {
				c(this, w, "ArrayBuffer");
				var e = d(t);
				y(this, { bytes: g.call(new Array(e), 0), byteLength: e }),
					r || (this.byteLength = e);
			}),
				(S = function (t, e, n) {
					c(this, S, "DataView"), c(t, w, "DataView");
					var i = m(t).byteLength,
						s = u(e);
					if (s < 0 || s > i) throw T("Wrong offset");
					if (s + (n = void 0 === n ? i - s : h(n)) > i)
						throw T("Wrong length");
					y(this, { buffer: t, byteLength: n, byteOffset: s }),
						r ||
							((this.buffer = t), (this.byteLength = n), (this.byteOffset = s));
				}),
				r &&
					(B(w, "byteLength"),
					B(S, "buffer"),
					B(S, "byteLength"),
					B(S, "byteOffset")),
				a(S.prototype, {
					getInt8: function (t) {
						return (U(this, 1, t)[0] << 24) >> 24;
					},
					getUint8: function (t) {
						return U(this, 1, t)[0];
					},
					getInt16: function (t) {
						var e = U(this, 2, t, arguments[1]);
						return (((e[1] << 8) | e[0]) << 16) >> 16;
					},
					getUint16: function (t) {
						var e = U(this, 2, t, arguments[1]);
						return (e[1] << 8) | e[0];
					},
					getInt32: function (t) {
						return O(U(this, 4, t, arguments[1]));
					},
					getUint32: function (t) {
						return O(U(this, 4, t, arguments[1])) >>> 0;
					},
					getFloat32: function (t) {
						return D(U(this, 4, t, arguments[1]), 23);
					},
					getFloat64: function (t) {
						return D(U(this, 8, t, arguments[1]), 52);
					},
					setInt8: function (t, e) {
						F(this, 1, t, R, e);
					},
					setUint8: function (t, e) {
						F(this, 1, t, R, e);
					},
					setInt16: function (t, e) {
						F(this, 2, t, N, e, arguments[2]);
					},
					setUint16: function (t, e) {
						F(this, 2, t, N, e, arguments[2]);
					},
					setInt32: function (t, e) {
						F(this, 4, t, P, e, arguments[2]);
					},
					setUint32: function (t, e) {
						F(this, 4, t, P, e, arguments[2]);
					},
					setFloat32: function (t, e) {
						F(this, 4, t, k, e, arguments[2]);
					},
					setFloat64: function (t, e) {
						F(this, 8, t, V, e, arguments[2]);
					},
				});
		v(w, "ArrayBuffer"),
			v(S, "DataView"),
			(e.ArrayBuffer = w),
			(e.DataView = S);
	},
	function (t, e, n) {
		"use strict";
		n.d(e, "a", function () {
			return i;
		});
		n(25), n(40);
		class i {
			constructor() {
				this.listeners = [];
			}
			addListener(t) {
				this.listeners.push(t);
			}
			removeListener(t) {
				var e = this.listeners.indexOf(t);
				e > -1 && this.listeners.splice(e, 1);
			}
			hasListener(t) {
				return this.listeners.indexOf(t) > -1;
			}
			executeListeners(t) {
				for (const e of this.listeners) e(t);
			}
		}
	},
	function (t, e, n) {
		var i = n(2),
			r = n(3),
			s = i.document,
			o = r(s) && r(s.createElement);
		t.exports = function (t) {
			return o ? s.createElement(t) : {};
		};
	},
	function (t, e, n) {
		var i = n(2),
			r = n(14);
		t.exports = function (t, e) {
			try {
				r(i, t, e);
			} catch (n) {
				i[t] = e;
			}
			return e;
		};
	},
	function (t, e, n) {
		var i = n(2),
			r = n(42),
			s = n(69),
			o = n(4),
			a = i.Reflect;
		t.exports =
			(a && a.ownKeys) ||
			function (t) {
				var e = r.f(o(t)),
					n = s.f;
				return n ? e.concat(n(t)) : e;
			};
	},
	function (t, e) {
		t.exports = [
			"constructor",
			"hasOwnProperty",
			"isPrototypeOf",
			"propertyIsEnumerable",
			"toLocaleString",
			"toString",
			"valueOf",
		];
	},
	function (t, e, n) {
		var i = n(6),
			r = n(9),
			s = n(4),
			o = n(47);
		t.exports = i
			? Object.defineProperties
			: function (t, e) {
					s(t);
					for (var n, i = o(e), a = i.length, l = 0; a > l; )
						r.f(t, (n = i[l++]), e[n]);
					return t;
			  };
	},
	function (t, e, n) {
		var i = n(7),
			r = n(61),
			s = i("iterator"),
			o = Array.prototype;
		t.exports = function (t) {
			return void 0 !== t && (r.Array === t || o[s] === t);
		};
	},
	function (t, e, n) {
		var i = n(1);
		t.exports = !i(function () {
			function t() {}
			return (
				(t.prototype.constructor = null),
				Object.getPrototypeOf(new t()) !== t.prototype
			);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(10),
			r = n(35),
			s = n(8);
		t.exports = function (t) {
			for (
				var e = i(this),
					n = s(e.length),
					o = arguments.length,
					a = r(o > 1 ? arguments[1] : void 0, n),
					l = o > 2 ? arguments[2] : void 0,
					c = void 0 === l ? n : r(l, n);
				c > a;

			)
				e[a++] = t;
			return e;
		};
	},
	function (t, e, n) {
		var i = n(70),
			r = n(2),
			s = function (t) {
				return "function" == typeof t ? t : void 0;
			};
		t.exports = function (t, e) {
			return arguments.length < 2
				? s(i[t]) || s(r[t])
				: (i[t] && i[t][e]) || (r[t] && r[t][e]);
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(94),
			s = n(30),
			o = n(48),
			a = n(29),
			l = n(14),
			c = n(16),
			u = n(7),
			h = n(34),
			d = n(61),
			f = n(123),
			p = f.IteratorPrototype,
			g = f.BUGGY_SAFARI_ITERATORS,
			v = u("iterator"),
			_ = function () {
				return this;
			};
		t.exports = function (t, e, n, u, f, m, y) {
			r(n, e, u);
			var b,
				w,
				S,
				E = function (t) {
					if (t === f && C) return C;
					if (!g && t in A) return A[t];
					switch (t) {
						case "keys":
						case "values":
						case "entries":
							return function () {
								return new n(this, t);
							};
					}
					return function () {
						return new n(this);
					};
				},
				T = e + " Iterator",
				L = !1,
				A = t.prototype,
				I = A[v] || A["@@iterator"] || (f && A[f]),
				C = (!g && I) || E(f),
				M = ("Array" == e && A.entries) || I;
			if (
				(M &&
					((b = s(M.call(new t()))),
					p !== Object.prototype &&
						b.next &&
						(h ||
							s(b) === p ||
							(o ? o(b, p) : "function" != typeof b[v] && l(b, v, _)),
						a(b, T, !0, !0),
						h && (d[T] = _))),
				"values" == f &&
					I &&
					"values" !== I.name &&
					((L = !0),
					(C = function () {
						return I.call(this);
					})),
				(h && !y) || A[v] === C || l(A, v, C),
				(d[e] = C),
				f)
			)
				if (
					((w = {
						values: E("values"),
						keys: m ? C : E("keys"),
						entries: E("entries"),
					}),
					y)
				)
					for (S in w) (!g && !L && S in A) || c(A, S, w[S]);
				else i({ target: e, proto: !0, forced: g || L }, w);
			return w;
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(123).IteratorPrototype,
			r = n(36),
			s = n(41),
			o = n(29),
			a = n(61),
			l = function () {
				return this;
			};
		t.exports = function (t, e, n) {
			var c = e + " Iterator";
			return (
				(t.prototype = r(i, { next: s(1, n) })), o(t, c, !1, !0), (a[c] = l), t
			);
		};
	},
	function (t, e, n) {
		var i = n(96),
			r = n(19);
		t.exports = function (t, e, n) {
			if (i(e))
				throw TypeError("String.prototype." + n + " doesn't accept regex");
			return String(r(t));
		};
	},
	function (t, e, n) {
		var i = n(3),
			r = n(28),
			s = n(7)("match");
		t.exports = function (t) {
			var e;
			return i(t) && (void 0 !== (e = t[s]) ? !!e : "RegExp" == r(t));
		};
	},
	function (t, e, n) {
		var i = n(7)("match");
		t.exports = function (t) {
			var e = /./;
			try {
				"/./"[t](e);
			} catch (n) {
				try {
					return (e[i] = !1), "/./"[t](e);
				} catch (t) {}
			}
			return !1;
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(24),
			r = n(19);
		t.exports =
			"".repeat ||
			function (t) {
				var e = String(r(this)),
					n = "",
					s = i(t);
				if (s < 0 || s == 1 / 0)
					throw RangeError("Wrong number of repetitions");
				for (; s > 0; (s >>>= 1) && (e += e)) 1 & s && (n += e);
				return n;
			};
	},
	function (t, e, n) {
		var i = n(2).navigator;
		t.exports = (i && i.userAgent) || "";
	},
	function (t, e, n) {
		var i = n(1),
			r = n(79);
		t.exports = function (t) {
			return i(function () {
				return !!r[t]() || "​᠎" != "​᠎"[t]() || r[t].name !== t;
			});
		};
	},
	function (t, e, n) {
		var i = n(3),
			r = n(48);
		t.exports = function (t, e, n) {
			var s,
				o = e.constructor;
			return (
				o !== n &&
					"function" == typeof o &&
					(s = o.prototype) !== n.prototype &&
					i(s) &&
					r &&
					r(t, s),
				t
			);
		};
	},
	function (t, e) {
		t.exports =
			Math.sign ||
			function (t) {
				return 0 == (t = +t) || t != t ? t : t < 0 ? -1 : 1;
			};
	},
	function (t, e, n) {
		var i = n(2),
			r = n(1),
			s = n(72),
			o = n(5).NATIVE_ARRAY_BUFFER_VIEWS,
			a = i.ArrayBuffer,
			l = i.Int8Array;
		t.exports =
			!o ||
			!r(function () {
				l(1);
			}) ||
			!r(function () {
				new l(-1);
			}) ||
			!s(function (t) {
				new l(), new l(null), new l(1.5), new l(t);
			}, !0) ||
			r(function () {
				return 1 !== new l(new a(2), 1, void 0).length;
			});
	},
	function (t, e, n) {
		var i = n(6),
			r = n(1),
			s = n(84);
		t.exports =
			!i &&
			!r(function () {
				return (
					7 !=
					Object.defineProperty(s("div"), "a", {
						get: function () {
							return 7;
						},
					}).a
				);
			});
	},
	function (t, e, n) {
		var i = n(54);
		t.exports = i("native-function-to-string", Function.toString);
	},
	function (t, e, n) {
		var i = n(2),
			r = n(105),
			s = i.WeakMap;
		t.exports = "function" == typeof s && /native code/.test(r.call(s));
	},
	function (t, e, n) {
		var i = n(12),
			r = n(86),
			s = n(15),
			o = n(9);
		t.exports = function (t, e) {
			for (var n = r(e), a = o.f, l = s.f, c = 0; c < n.length; c++) {
				var u = n[c];
				i(t, u) || a(t, u, l(e, u));
			}
		};
	},
	function (t, e, n) {
		var i = n(12),
			r = n(18),
			s = n(57),
			o = n(56),
			a = s(!1);
		t.exports = function (t, e) {
			var n,
				s = r(t),
				l = 0,
				c = [];
			for (n in s) !i(o, n) && i(s, n) && c.push(n);
			for (; e.length > l; ) i(s, (n = e[l++])) && (~a(c, n) || c.push(n));
			return c;
		};
	},
	function (t, e, n) {
		var i = n(1);
		t.exports =
			!!Object.getOwnPropertySymbols &&
			!i(function () {
				return !String(Symbol());
			});
	},
	function (t, e, n) {
		e.f = n(7);
	},
	function (t, e, n) {
		var i = n(2).document;
		t.exports = i && i.documentElement;
	},
	function (t, e, n) {
		var i = n(18),
			r = n(42).f,
			s = {}.toString,
			o =
				"object" == typeof window && window && Object.getOwnPropertyNames
					? Object.getOwnPropertyNames(window)
					: [];
		t.exports.f = function (t) {
			return o && "[object Window]" == s.call(t)
				? (function (t) {
						try {
							return r(t);
						} catch (t) {
							return o.slice();
						}
				  })(t)
				: r(i(t));
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(6),
			r = n(1),
			s = n(47),
			o = n(69),
			a = n(52),
			l = n(10),
			c = n(53),
			u = Object.assign;
		t.exports =
			!u ||
			r(function () {
				var t = {},
					e = {},
					n = Symbol();
				return (
					(t[n] = 7),
					"abcdefghijklmnopqrst".split("").forEach(function (t) {
						e[t] = t;
					}),
					7 != u({}, t)[n] || "abcdefghijklmnopqrst" != s(u({}, e)).join("")
				);
			})
				? function (t, e) {
						for (
							var n = l(t), r = arguments.length, u = 1, h = o.f, d = a.f;
							r > u;

						)
							for (
								var f,
									p = c(arguments[u++]),
									g = h ? s(p).concat(h(p)) : s(p),
									v = g.length,
									_ = 0;
								v > _;

							)
								(f = g[_++]), (i && !d.call(p, f)) || (n[f] = p[f]);
						return n;
				  }
				: u;
	},
	function (t, e, n) {
		var i = n(6),
			r = n(47),
			s = n(18),
			o = n(52).f;
		t.exports = function (t, e) {
			for (var n, a = s(t), l = r(a), c = l.length, u = 0, h = []; c > u; )
				(n = l[u++]), (i && !o.call(a, n)) || h.push(e ? [n, a[n]] : a[n]);
			return h;
		};
	},
	function (t, e, n) {
		var i = n(4);
		t.exports = function (t, e, n, r) {
			try {
				return r ? e(i(n)[0], n[1]) : e(n);
			} catch (e) {
				var s = t.return;
				throw (void 0 !== s && i(s.call(t)), e);
			}
		};
	},
	function (t, e) {
		t.exports =
			Object.is ||
			function (t, e) {
				return t === e ? 0 !== t || 1 / t == 1 / e : t != t && e != e;
			};
	},
	function (t, e, n) {
		var i = n(3),
			r = n(4);
		t.exports = function (t, e) {
			if ((r(t), !i(e) && null !== e))
				throw TypeError("Can't set " + String(e) + " as a prototype");
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(21),
			r = n(3),
			s = [].slice,
			o = {},
			a = function (t, e, n) {
				if (!(e in o)) {
					for (var i = [], r = 0; r < e; r++) i[r] = "a[" + r + "]";
					o[e] = Function("C,a", "return new C(" + i.join(",") + ")");
				}
				return o[e](t, n);
			};
		t.exports =
			Function.bind ||
			function (t) {
				var e = i(this),
					n = s.call(arguments, 1),
					o = function () {
						var i = n.concat(s.call(arguments));
						return this instanceof o ? a(e, i.length, i) : e.apply(t, i);
					};
				return r(e.prototype) && (o.prototype = e.prototype), o;
			};
	},
	function (t, e, n) {
		"use strict";
		var i = n(37),
			r = n(10),
			s = n(115),
			o = n(89),
			a = n(8),
			l = n(45),
			c = n(62);
		t.exports = function (t) {
			var e,
				n,
				u,
				h,
				d = r(t),
				f = "function" == typeof this ? this : Array,
				p = arguments.length,
				g = p > 1 ? arguments[1] : void 0,
				v = void 0 !== g,
				_ = 0,
				m = c(d);
			if (
				(v && (g = i(g, p > 2 ? arguments[2] : void 0, 2)),
				null == m || (f == Array && o(m)))
			)
				for (n = new f((e = a(d.length))); e > _; _++)
					l(n, _, v ? g(d[_], _) : d[_]);
			else
				for (h = m.call(d), n = new f(); !(u = h.next()).done; _++)
					l(n, _, v ? s(h, g, [u.value, _], !0) : u.value);
			return (n.length = _), n;
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(10),
			r = n(35),
			s = n(8);
		t.exports =
			[].copyWithin ||
			function (t, e) {
				var n = i(this),
					o = s(n.length),
					a = r(t, o),
					l = r(e, o),
					c = arguments.length > 2 ? arguments[2] : void 0,
					u = Math.min((void 0 === c ? o : r(c, o)) - l, o - a),
					h = 1;
				for (
					l < a && a < l + u && ((h = -1), (l += u - 1), (a += u - 1));
					u-- > 0;

				)
					l in n ? (n[a] = n[l]) : delete n[a], (a += h), (l += h);
				return n;
			};
	},
	function (t, e, n) {
		"use strict";
		var i = n(43),
			r = n(8),
			s = n(37),
			o = function (t, e, n, a, l, c, u, h) {
				for (var d, f = l, p = 0, g = !!u && s(u, h, 3); p < a; ) {
					if (p in n) {
						if (((d = g ? g(n[p], p, e) : n[p]), c > 0 && i(d)))
							f = o(t, e, d, r(d.length), f, c - 1) - 1;
						else {
							if (f >= 9007199254740991)
								throw TypeError("Exceed the acceptable array length");
							t[f] = d;
						}
						f++;
					}
					p++;
				}
				return f;
			};
		t.exports = o;
	},
	function (t, e, n) {
		"use strict";
		var i = n(18),
			r = n(24),
			s = n(8),
			o = n(31),
			a = [].lastIndexOf,
			l = !!a && 1 / [1].lastIndexOf(1, -0) < 0,
			c = o("lastIndexOf");
		t.exports =
			l || c
				? function (t) {
						if (l) return a.apply(this, arguments) || 0;
						var e = i(this),
							n = s(e.length),
							o = n - 1;
						for (
							arguments.length > 1 && (o = Math.min(o, r(arguments[1]))),
								o < 0 && (o = n + o);
							o >= 0;
							o--
						)
							if (o in e && e[o] === t) return o || 0;
						return -1;
				  }
				: a;
	},
	function (t, e, n) {
		"use strict";
		var i,
			r,
			s,
			o = n(30),
			a = n(14),
			l = n(12),
			c = n(7),
			u = n(34),
			h = c("iterator"),
			d = !1;
		[].keys &&
			("next" in (s = [].keys())
				? (r = o(o(s))) !== Object.prototype && (i = r)
				: (d = !0)),
			null == i && (i = {}),
			u ||
				l(i, h) ||
				a(i, h, function () {
					return this;
				}),
			(t.exports = { IteratorPrototype: i, BUGGY_SAFARI_ITERATORS: d });
	},
	function (t, e, n) {
		var i = n(8),
			r = n(98),
			s = n(19);
		t.exports = function (t, e, n, o) {
			var a,
				l,
				c = String(s(t)),
				u = c.length,
				h = void 0 === n ? " " : String(n),
				d = i(e);
			return d <= u || "" == h
				? c
				: ((a = d - u),
				  (l = r.call(h, Math.ceil(a / h.length))).length > a &&
						(l = l.slice(0, a)),
				  o ? l + c : c + l);
		};
	},
	function (t, e, n) {
		var i = n(99);
		t.exports = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(i);
	},
	function (t, e, n) {
		"use strict";
		var i = n(74),
			r = n(20),
			s = n(93),
			o = r.set,
			a = r.getterFor("String Iterator");
		s(
			String,
			"String",
			function (t) {
				o(this, { type: "String Iterator", string: String(t), index: 0 });
			},
			function () {
				var t,
					e = a(this),
					n = e.string,
					r = e.index;
				return r >= n.length
					? { value: void 0, done: !0 }
					: ((t = i(n, r, !0)), (e.index += t.length), { value: t, done: !1 });
			}
		);
	},
	function (t, e, n) {
		var i = n(2),
			r = n(50),
			s = n(79),
			o = i.parseInt,
			a = /^[+-]?0[Xx]/,
			l = 8 !== o(s + "08") || 22 !== o(s + "0x16");
		t.exports = l
			? function (t, e) {
					var n = r(String(t), 3);
					return o(n, e >>> 0 || (a.test(n) ? 16 : 10));
			  }
			: o;
	},
	function (t, e, n) {
		var i = n(2),
			r = n(50),
			s = n(79),
			o = i.parseFloat,
			a = 1 / o(s + "-0") != -1 / 0;
		t.exports = a
			? function (t) {
					var e = r(String(t), 3),
						n = o(e);
					return 0 === n && "-" == e.charAt(0) ? -0 : n;
			  }
			: o;
	},
	function (t, e, n) {
		var i = n(3),
			r = Math.floor;
		t.exports = function (t) {
			return !i(t) && isFinite(t) && r(t) === t;
		};
	},
	function (t, e, n) {
		var i = n(28);
		t.exports = function (t) {
			if ("number" != typeof t && "Number" != i(t))
				throw TypeError("Incorrect invocation");
			return +t;
		};
	},
	function (t, e) {
		t.exports =
			Math.log1p ||
			function (t) {
				return (t = +t) > -1e-8 && t < 1e-8 ? t - (t * t) / 2 : Math.log(1 + t);
			};
	},
	function (t, e, n) {
		var i,
			r,
			s,
			o = n(2),
			a = n(1),
			l = n(28),
			c = n(37),
			u = n(111),
			h = n(84),
			d = o.location,
			f = o.setImmediate,
			p = o.clearImmediate,
			g = o.process,
			v = o.MessageChannel,
			_ = o.Dispatch,
			m = 0,
			y = {},
			b = function (t) {
				if (y.hasOwnProperty(t)) {
					var e = y[t];
					delete y[t], e();
				}
			},
			w = function (t) {
				return function () {
					b(t);
				};
			},
			S = function (t) {
				b(t.data);
			},
			E = function (t) {
				o.postMessage(t + "", d.protocol + "//" + d.host);
			};
		(f && p) ||
			((f = function (t) {
				for (var e = [], n = 1; arguments.length > n; ) e.push(arguments[n++]);
				return (
					(y[++m] = function () {
						("function" == typeof t ? t : Function(t)).apply(void 0, e);
					}),
					i(m),
					m
				);
			}),
			(p = function (t) {
				delete y[t];
			}),
			"process" == l(g)
				? (i = function (t) {
						g.nextTick(w(t));
				  })
				: _ && _.now
				? (i = function (t) {
						_.now(w(t));
				  })
				: v
				? ((s = (r = new v()).port2),
				  (r.port1.onmessage = S),
				  (i = c(s.postMessage, s, 1)))
				: !o.addEventListener ||
				  "function" != typeof postMessage ||
				  o.importScripts ||
				  a(E)
				? (i =
						"onreadystatechange" in h("script")
							? function (t) {
									u.appendChild(h("script")).onreadystatechange = function () {
										u.removeChild(this), b(t);
									};
							  }
							: function (t) {
									setTimeout(w(t), 0);
							  })
				: ((i = E), o.addEventListener("message", S, !1))),
			(t.exports = { set: f, clear: p });
	},
	function (t, e, n) {
		var i = n(4),
			r = n(3),
			s = n(134);
		t.exports = function (t, e) {
			if ((i(t), r(e) && e.constructor === t)) return e;
			var n = s.f(t);
			return (0, n.resolve)(e), n.promise;
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(21),
			r = function (t) {
				var e, n;
				(this.promise = new t(function (t, i) {
					if (void 0 !== e || void 0 !== n)
						throw TypeError("Bad Promise constructor");
					(e = t), (n = i);
				})),
					(this.resolve = i(e)),
					(this.reject = i(n));
			};
		t.exports.f = function (t) {
			return new r(t);
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(9).f,
			r = n(36),
			s = n(51),
			o = n(37),
			a = n(39),
			l = n(60),
			c = n(93),
			u = n(49),
			h = n(6),
			d = n(44).fastKey,
			f = n(20),
			p = f.set,
			g = f.getterFor;
		t.exports = {
			getConstructor: function (t, e, n, c) {
				var u = t(function (t, i) {
						a(t, u, e),
							p(t, {
								type: e,
								index: r(null),
								first: void 0,
								last: void 0,
								size: 0,
							}),
							h || (t.size = 0),
							null != i && l(i, t[c], t, n);
					}),
					f = g(e),
					v = function (t, e, n) {
						var i,
							r,
							s = f(t),
							o = _(t, e);
						return (
							o
								? (o.value = n)
								: ((s.last = o = {
										index: (r = d(e, !0)),
										key: e,
										value: n,
										previous: (i = s.last),
										next: void 0,
										removed: !1,
								  }),
								  s.first || (s.first = o),
								  i && (i.next = o),
								  h ? s.size++ : t.size++,
								  "F" !== r && (s.index[r] = o)),
							t
						);
					},
					_ = function (t, e) {
						var n,
							i = f(t),
							r = d(e);
						if ("F" !== r) return i.index[r];
						for (n = i.first; n; n = n.next) if (n.key == e) return n;
					};
				return (
					s(u.prototype, {
						clear: function () {
							for (var t = f(this), e = t.index, n = t.first; n; )
								(n.removed = !0),
									n.previous && (n.previous = n.previous.next = void 0),
									delete e[n.index],
									(n = n.next);
							(t.first = t.last = void 0), h ? (t.size = 0) : (this.size = 0);
						},
						delete: function (t) {
							var e = f(this),
								n = _(this, t);
							if (n) {
								var i = n.next,
									r = n.previous;
								delete e.index[n.index],
									(n.removed = !0),
									r && (r.next = i),
									i && (i.previous = r),
									e.first == n && (e.first = i),
									e.last == n && (e.last = r),
									h ? e.size-- : this.size--;
							}
							return !!n;
						},
						forEach: function (t) {
							for (
								var e,
									n = f(this),
									i = o(t, arguments.length > 1 ? arguments[1] : void 0, 3);
								(e = e ? e.next : n.first);

							)
								for (i(e.value, e.key, this); e && e.removed; ) e = e.previous;
						},
						has: function (t) {
							return !!_(this, t);
						},
					}),
					s(
						u.prototype,
						n
							? {
									get: function (t) {
										var e = _(this, t);
										return e && e.value;
									},
									set: function (t, e) {
										return v(this, 0 === t ? 0 : t, e);
									},
							  }
							: {
									add: function (t) {
										return v(this, (t = 0 === t ? 0 : t), t);
									},
							  }
					),
					h &&
						i(u.prototype, "size", {
							get: function () {
								return f(this).size;
							},
						}),
					u
				);
			},
			setStrong: function (t, e, n) {
				var i = e + " Iterator",
					r = g(e),
					s = g(i);
				c(
					t,
					e,
					function (t, e) {
						p(this, { type: i, target: t, state: r(t), kind: e, last: void 0 });
					},
					function () {
						for (var t = s(this), e = t.kind, n = t.last; n && n.removed; )
							n = n.previous;
						return t.target && (t.last = n = n ? n.next : t.state.first)
							? "keys" == e
								? { value: n.key, done: !1 }
								: "values" == e
								? { value: n.value, done: !1 }
								: { value: [n.key, n.value], done: !1 }
							: ((t.target = void 0), { value: void 0, done: !0 });
					},
					n ? "entries" : "values",
					!n,
					!0
				),
					u(e);
			},
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(51),
			r = n(44).getWeakData,
			s = n(4),
			o = n(3),
			a = n(39),
			l = n(60),
			c = n(13),
			u = n(12),
			h = n(20),
			d = h.set,
			f = h.getterFor,
			p = c(5),
			g = c(6),
			v = 0,
			_ = function (t) {
				return t.frozen || (t.frozen = new m());
			},
			m = function () {
				this.entries = [];
			},
			y = function (t, e) {
				return p(t.entries, function (t) {
					return t[0] === e;
				});
			};
		(m.prototype = {
			get: function (t) {
				var e = y(this, t);
				if (e) return e[1];
			},
			has: function (t) {
				return !!y(this, t);
			},
			set: function (t, e) {
				var n = y(this, t);
				n ? (n[1] = e) : this.entries.push([t, e]);
			},
			delete: function (t) {
				var e = g(this.entries, function (e) {
					return e[0] === t;
				});
				return ~e && this.entries.splice(e, 1), !!~e;
			},
		}),
			(t.exports = {
				getConstructor: function (t, e, n, c) {
					var h = t(function (t, i) {
							a(t, h, e),
								d(t, { type: e, id: v++, frozen: void 0 }),
								null != i && l(i, t[c], t, n);
						}),
						p = f(e),
						g = function (t, e, n) {
							var i = p(t),
								o = r(s(e), !0);
							return !0 === o ? _(i).set(e, n) : (o[i.id] = n), t;
						};
					return (
						i(h.prototype, {
							delete: function (t) {
								var e = p(this);
								if (!o(t)) return !1;
								var n = r(t);
								return !0 === n
									? _(e).delete(t)
									: n && u(n, e.id) && delete n[e.id];
							},
							has: function (t) {
								var e = p(this);
								if (!o(t)) return !1;
								var n = r(t);
								return !0 === n ? _(e).has(t) : n && u(n, e.id);
							},
						}),
						i(
							h.prototype,
							n
								? {
										get: function (t) {
											var e = p(this);
											if (o(t)) {
												var n = r(t);
												return !0 === n ? _(e).get(t) : n ? n[e.id] : void 0;
											}
										},
										set: function (t, e) {
											return g(this, t, e);
										},
								  }
								: {
										add: function (t) {
											return g(this, t, !0);
										},
								  }
						),
						h
					);
				},
			});
	},
	function (t, e, n) {
		var i = n(24),
			r = n(8);
		t.exports = function (t) {
			if (void 0 === t) return 0;
			var e = i(t),
				n = r(e);
			if (e !== n) throw RangeError("Wrong length or index");
			return n;
		};
	},
	function (t, e, n) {
		var i = n(24);
		t.exports = function (t, e) {
			var n = i(t);
			if (n < 0 || n % e) throw RangeError("Wrong offset");
			return n;
		};
	},
	function (t, e, n) {
		var i = n(10),
			r = n(8),
			s = n(62),
			o = n(89),
			a = n(37),
			l = n(5).aTypedArrayConstructor;
		t.exports = function (t) {
			var e,
				n,
				c,
				u,
				h,
				d = i(t),
				f = arguments.length,
				p = f > 1 ? arguments[1] : void 0,
				g = void 0 !== p,
				v = s(d);
			if (null != v && !o(v))
				for (h = v.call(d), d = []; !(u = h.next()).done; ) d.push(u.value);
			for (
				g && f > 2 && (p = a(p, arguments[2], 2)),
					n = r(d.length),
					c = new (l(this))(n),
					e = 0;
				n > e;
				e++
			)
				c[e] = g ? p(d[e], e) : d[e];
			return c;
		};
	},
	function (t, e, n) {
		var i = n(1),
			r = n(7),
			s = n(34),
			o = r("iterator");
		t.exports = !i(function () {
			var t = new URL("b?e=1", "http://a"),
				e = t.searchParams;
			return (
				(t.pathname = "c%20d"),
				(s && !t.toJSON) ||
					!e.sort ||
					"http://a/c%20d?e=1" !== t.href ||
					"1" !== e.get("e") ||
					"a=1" !== String(new URLSearchParams("?a=1")) ||
					!e[o] ||
					"a" !== new URL("https://a@b").username ||
					"b" !== new URLSearchParams(new URLSearchParams("a=b")).get("a") ||
					"xn--e1aybc" !== new URL("http://тест").host ||
					"#%D0%B1" !== new URL("http://a#б").hash
			);
		});
	},
	function (module, __webpack_exports__, __webpack_require__) {
		"use strict";
		__webpack_require__.d(__webpack_exports__, "a", function () {
			return TabsPolyfill;
		});
		var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
				25
			),
			core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(
				core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__
			),
			core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
				40
			),
			core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1___default = __webpack_require__.n(
				core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__
			),
			_communicator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(11),
			_listener_on_connect_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
				83
			),
			_port_polyfill__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(26);
		class TabsPolyfill {
			constructor() {
				(this.portsMap = new Map()),
					(this.onConnect = new _listener_on_connect_helper__WEBPACK_IMPORTED_MODULE_3__.a()),
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.addListenerOnPortMessageListener(
							this.onPortMessageListener.bind(this)
						),
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.addListenerOnConnectTab(this.onConnectTabListener.bind(this)),
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.addListenerTabAction(this.onTabActionListener.bind(this));
			}
			get(t, e) {
				console.warn("Not implemented `get` in TabsPolyfill");
			}
			getCurrent(t) {
				console.warn("Not implemented `getCurrent` in TabsPolyfill");
			}
			getSelected(t) {
				console.warn("Not implemented `getCurrent` in TabsPolyfill");
			}
			connect(t, e) {
				var n = new _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a(
					_port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a.TYPES.TAB,
					t
				);
				this.portsMap.set(n.uuid, n);
				var i = {
					action: _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a.ACTION.CREATE,
					portUUID: n.uuid,
					type: _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a.TYPES.TAB,
					uniqueID: n.uniqueID,
				};
				return (
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.port(i)
						.then((t) => {
							(n.ready = !0), n.executeWaitingMessages();
						})
						.catch((t) => {
							console.error(`Error port ${t}`);
						}),
					n
				);
			}
			sendRequest(t, e, n) {
				console.warn("Not implemented `sendRequest` in TabsPolyfill");
			}
			sendMessage(t, e, n, i) {
				console.warn("Not implemented `sendMessage` in TabsPolyfill");
			}
			getAllInWindow(t, e) {
				console.warn("Not implemented `getAllInWindow` in TabsPolyfill");
			}
			create(t, e) {
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs(t, TabsPolyfill.tabsActions.create)
					.then((t) => {
						e(t);
					})
					.catch((t) => {
						console.error(`Error in getiing tab ${t}`);
					});
			}
			duplicate(t, e) {
				console.warn("Not implemented `duplicate` in TabsPolyfill");
			}
			query(t, e) {
				return void 0 === window.safari
					? this.queryFromBrowserAction(t, e)
					: this.queryFromContent(t, e);
			}
			queryFromBrowserAction(t, e) {
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs(t, TabsPolyfill.tabsActions.getTabWith)
					.then((t) => {
						e(t);
					})
					.catch((t) => {
						console.error(`Error in getiing tab ${t}`);
					});
			}
			queryFromContent(t, e) {
				console.warn(`Enters in quering extension with: ${t}`);
			}
			highlight(t, e) {
				console.warn("Not implemented `highlightz` in TabsPolyfill");
			}
			update(t, e, n) {
				console.warn("Not implemented UPDATE funciton in TabsPolyfill");
			}
			move(t, e, n) {
				console.warn("Not implemented MOVE function in TabsPolyfill");
			}
			reload(t, e, n) {
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs({ tabID: t }, TabsPolyfill.tabsActions.reload)
					.then((t) => {
						null != n && n(t);
					})
					.catch((t) => {
						console.error(`Error reload tab ${t}`);
					});
			}
			detectLanguage(t, e) {
				console.warn("Not implemented `detectLanguage` in TabsPolyfill");
			}
			captureVisibleTab(t, e, n) {
				console.warn("Not implemented `captureVisibleTab` in TabsPolyfill");
			}
			executeScript(t, e, n) {
				void 0 === window.safari
					? this.executeScriptFromBrowserAction(t, e, n)
					: console.warn("Not implemented `executeScript` in TabsPolyfill");
			}
			executeScriptFromBrowserAction(t, e, n) {
				const i = { tabID: t, details: e };
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs(i, TabsPolyfill.tabsActions.executeScript)
					.then((t) => {
						n([t]);
					})
					.catch((t) => {
						console.error(`Error in executeScriptFromBrowserAction ${t}`);
					});
			}
			insertCSS(t, e, n) {
				console.warn("Not implemented `insertCSS` in TabsPolyfill");
			}
			setZoom(t, e, n) {
				console.warn("Not implemented `setZoom` in TabsPolyfill");
			}
			setZoomSettings(t, e, n) {
				console.warn("Not implemented `setZoomSettings` in TabsPolyfill");
			}
			discard(t, e) {
				console.warn("Not implemented `discard` in TabsPolyfill");
			}
			goForward(t, e) {
				console.warn("Not implemented `tabID` in TabsPolyfill");
			}
			goBack(t, e) {
				console.warn("Not implemented `goBack` in TabsPolyfill");
			}
			onPortMessageListener(t) {
				let e = t.portUUID,
					n = t.data;
				if (this.portsMap.has(e)) {
					let t = this.portsMap.get(e);
					void 0 !== t && t.onMessage.executeListeners(n);
				}
			}
			onConnectTabListener(t) {
				var e = t.portUUID,
					n = t.type,
					i = t.uniqueID,
					r = new _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a(n, i);
				(r.uuid = e),
					(r.ready = !0),
					this.portsMap.set(r.uuid, r),
					this.onConnect.executeListeners(r);
			}
			onTabActionListener(data) {
				console.log("Enters in onTabActionListener", data);
				let action = data.action;
				switch (action) {
					case "executeScript": {
						let callbackID = data.callbackID,
							code = data.data,
							returnValue = eval(code),
							queryInfo = { callbackID: callbackID, value: returnValue };
						_communicator__WEBPACK_IMPORTED_MODULE_2__.a
							.shared()
							.tabs(queryInfo, TabsPolyfill.tabsActions.executeScript)
							.then((t) => {
								console.log("result: ", t);
							})
							.catch((t) => {
								console.error(`Error in getiing tab ${t}`);
							});
					}
				}
			}
		}
		(TabsPolyfill.tabsActions = {
			getTabWith: 1,
			create: 2,
			reload: 3,
			executeScript: 4,
		}),
			(TabsPolyfill.onUpdated = {
				addListener: function (t) {
					window.safari.application.addEventListener(
						"beforeNavigate",
						function (e) {
							t(
								e.target.id,
								{ status: "loading", url: e.target.url, pinned: null },
								{
									id: e.target.id,
									index: null,
									windowId: null,
									openerTabId: null,
									highlighted: null,
									active: e.target.id == e.target.browserWindow.activeTab.id,
									pinned: null,
									url: e.target.url,
									title: e.target.title,
									favIconUrl: null,
									status: "loading",
									incognito: null,
								}
							);
						},
						!0
					);
				},
			});
	},
	function (t, e, n) {
		n(143), n(354), (t.exports = n(382));
	},
	function (t, e, n) {
		n(144),
			n(147),
			n(148),
			n(149),
			n(150),
			n(151),
			n(152),
			n(153),
			n(154),
			n(155),
			n(156),
			n(157),
			n(158),
			n(159),
			n(160),
			n(161),
			n(162),
			n(163),
			n(164),
			n(165),
			n(166),
			n(167),
			n(168),
			n(169),
			n(170),
			n(171),
			n(172),
			n(173),
			n(174),
			n(175),
			n(176),
			n(177),
			n(178),
			n(179),
			n(180),
			n(181),
			n(183),
			n(184),
			n(185),
			n(186),
			n(187),
			n(188),
			n(189),
			n(190),
			n(191),
			n(192),
			n(193),
			n(194),
			n(195),
			n(196),
			n(197),
			n(198),
			n(199),
			n(200),
			n(201),
			n(202),
			n(204),
			n(205),
			n(206),
			n(207),
			n(208),
			n(209),
			n(210),
			n(211),
			n(212),
			n(213),
			n(214),
			n(215),
			n(216),
			n(217),
			n(218),
			n(25),
			n(219),
			n(220),
			n(221),
			n(222),
			n(223),
			n(224),
			n(225),
			n(226),
			n(227),
			n(228),
			n(229),
			n(230),
			n(231),
			n(232),
			n(233),
			n(234),
			n(235),
			n(126),
			n(236),
			n(237),
			n(238),
			n(239),
			n(240),
			n(241),
			n(242),
			n(243),
			n(244),
			n(245),
			n(246),
			n(247),
			n(248),
			n(249),
			n(250),
			n(251),
			n(252),
			n(253),
			n(254),
			n(255),
			n(256),
			n(257),
			n(259),
			n(260),
			n(261),
			n(262),
			n(263),
			n(264),
			n(265),
			n(266),
			n(267),
			n(268),
			n(269),
			n(270),
			n(271),
			n(272),
			n(273),
			n(274),
			n(275),
			n(277),
			n(278),
			n(279),
			n(280),
			n(281),
			n(282),
			n(283),
			n(284),
			n(285),
			n(286),
			n(287),
			n(288),
			n(289),
			n(291),
			n(292),
			n(294),
			n(46),
			n(298),
			n(299),
			n(300),
			n(301),
			n(302),
			n(303),
			n(304),
			n(305),
			n(306),
			n(307),
			n(308),
			n(309),
			n(310),
			n(311),
			n(312),
			n(313),
			n(314),
			n(315),
			n(316),
			n(317),
			n(318),
			n(319),
			n(320),
			n(321),
			n(322),
			n(323),
			n(324),
			n(325),
			n(326),
			n(327),
			n(328),
			n(329),
			n(330),
			n(331),
			n(332),
			n(333),
			n(334),
			n(335),
			n(336),
			n(337),
			n(338),
			n(339),
			n(340),
			n(341),
			n(342),
			n(343),
			n(344),
			n(345),
			n(346),
			n(347),
			n(348),
			n(349),
			n(350),
			n(351),
			n(352),
			n(353),
			(t.exports = n(70));
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(2),
			s = n(12),
			o = n(109),
			a = n(6),
			l = n(34),
			c = n(16),
			u = n(56),
			h = n(1),
			d = n(54),
			f = n(29),
			p = n(55),
			g = n(7),
			v = n(110),
			_ = n(17),
			m = n(146),
			y = n(43),
			b = n(4),
			w = n(3),
			S = n(10),
			E = n(18),
			T = n(27),
			L = n(41),
			A = n(36),
			I = n(42),
			C = n(112),
			M = n(15),
			x = n(9),
			D = n(52),
			O = n(14),
			R = n(47),
			N = n(69),
			P = n(68),
			k = n(20),
			V = P("hidden"),
			B = k.set,
			U = k.getterFor("Symbol"),
			F = M.f,
			q = x.f,
			H = C.f,
			j = r.Symbol,
			W = r.JSON,
			G = W && W.stringify,
			Y = g("toPrimitive"),
			K = D.f,
			z = d("symbol-registry"),
			X = d("symbols"),
			$ = d("op-symbols"),
			J = d("wks"),
			Z = Object.prototype,
			Q = r.QObject,
			tt = !Q || !Q.prototype || !Q.prototype.findChild,
			et =
				a &&
				h(function () {
					return (
						7 !=
						A(
							q({}, "a", {
								get: function () {
									return q(this, "a", { value: 7 }).a;
								},
							})
						).a
					);
				})
					? function (t, e, n) {
							var i = F(Z, e);
							i && delete Z[e], q(t, e, n), i && t !== Z && q(Z, e, i);
					  }
					: q,
			nt = function (t, e) {
				var n = (X[t] = A(j.prototype));
				return (
					B(n, { type: "Symbol", tag: t, description: e }),
					a || (n.description = e),
					n
				);
			},
			it =
				o && "symbol" == typeof j.iterator
					? function (t) {
							return "symbol" == typeof t;
					  }
					: function (t) {
							return Object(t) instanceof j;
					  },
			rt = function (t, e, n) {
				return (
					t === Z && rt($, e, n),
					b(t),
					(e = T(e, !0)),
					b(n),
					s(X, e)
						? (n.enumerable
								? (s(t, V) && t[V][e] && (t[V][e] = !1),
								  (n = A(n, { enumerable: L(0, !1) })))
								: (s(t, V) || q(t, V, L(1, {})), (t[V][e] = !0)),
						  et(t, e, n))
						: q(t, e, n)
				);
			},
			st = function (t, e) {
				b(t);
				for (var n, i = m((e = E(e))), r = 0, s = i.length; s > r; )
					rt(t, (n = i[r++]), e[n]);
				return t;
			},
			ot = function (t) {
				var e = K.call(this, (t = T(t, !0)));
				return (
					!(this === Z && s(X, t) && !s($, t)) &&
					(!(e || !s(this, t) || !s(X, t) || (s(this, V) && this[V][t])) || e)
				);
			},
			at = function (t, e) {
				if (((t = E(t)), (e = T(e, !0)), t !== Z || !s(X, e) || s($, e))) {
					var n = F(t, e);
					return (
						!n || !s(X, e) || (s(t, V) && t[V][e]) || (n.enumerable = !0), n
					);
				}
			},
			lt = function (t) {
				for (var e, n = H(E(t)), i = [], r = 0; n.length > r; )
					s(X, (e = n[r++])) || s(u, e) || i.push(e);
				return i;
			},
			ct = function (t) {
				for (
					var e, n = t === Z, i = H(n ? $ : E(t)), r = [], o = 0;
					i.length > o;

				)
					!s(X, (e = i[o++])) || (n && !s(Z, e)) || r.push(X[e]);
				return r;
			};
		o ||
			(c(
				(j = function () {
					if (this instanceof j) throw TypeError("Symbol is not a constructor");
					var t = void 0 === arguments[0] ? void 0 : String(arguments[0]),
						e = p(t),
						n = function (t) {
							this === Z && n.call($, t),
								s(this, V) && s(this[V], e) && (this[V][e] = !1),
								et(this, e, L(1, t));
						};
					return a && tt && et(Z, e, { configurable: !0, set: n }), nt(e, t);
				}).prototype,
				"toString",
				function () {
					return U(this).tag;
				}
			),
			(D.f = ot),
			(x.f = rt),
			(M.f = at),
			(I.f = C.f = lt),
			(N.f = ct),
			a &&
				(q(j.prototype, "description", {
					configurable: !0,
					get: function () {
						return U(this).description;
					},
				}),
				l || c(Z, "propertyIsEnumerable", ot, { unsafe: !0 })),
			(v.f = function (t) {
				return nt(g(t), t);
			})),
			i({ global: !0, wrap: !0, forced: !o, sham: !o }, { Symbol: j });
		for (var ut = R(J), ht = 0; ut.length > ht; ) _(ut[ht++]);
		i(
			{ target: "Symbol", stat: !0, forced: !o },
			{
				for: function (t) {
					return s(z, (t += "")) ? z[t] : (z[t] = j(t));
				},
				keyFor: function (t) {
					if (!it(t)) throw TypeError(t + " is not a symbol");
					for (var e in z) if (z[e] === t) return e;
				},
				useSetter: function () {
					tt = !0;
				},
				useSimple: function () {
					tt = !1;
				},
			}
		),
			i(
				{ target: "Object", stat: !0, forced: !o, sham: !a },
				{
					create: function (t, e) {
						return void 0 === e ? A(t) : st(A(t), e);
					},
					defineProperty: rt,
					defineProperties: st,
					getOwnPropertyDescriptor: at,
				}
			),
			i(
				{ target: "Object", stat: !0, forced: !o },
				{ getOwnPropertyNames: lt, getOwnPropertySymbols: ct }
			),
			i(
				{
					target: "Object",
					stat: !0,
					forced: h(function () {
						N.f(1);
					}),
				},
				{
					getOwnPropertySymbols: function (t) {
						return N.f(S(t));
					},
				}
			),
			W &&
				i(
					{
						target: "JSON",
						stat: !0,
						forced:
							!o ||
							h(function () {
								var t = j();
								return (
									"[null]" != G([t]) ||
									"{}" != G({ a: t }) ||
									"{}" != G(Object(t))
								);
							}),
					},
					{
						stringify: function (t) {
							for (var e, n, i = [t], r = 1; arguments.length > r; )
								i.push(arguments[r++]);
							if (((n = e = i[1]), (w(e) || void 0 !== t) && !it(t)))
								return (
									y(e) ||
										(e = function (t, e) {
											if (
												("function" == typeof n && (e = n.call(this, t, e)),
												!it(e))
											)
												return e;
										}),
									(i[1] = e),
									G.apply(W, i)
								);
						},
					}
				),
			j.prototype[Y] || O(j.prototype, Y, j.prototype.valueOf),
			f(j, "Symbol"),
			(u[V] = !0);
	},
	function (t, e) {
		var n;
		n = (function () {
			return this;
		})();
		try {
			n = n || new Function("return this")();
		} catch (t) {
			"object" == typeof window && (n = window);
		}
		t.exports = n;
	},
	function (t, e, n) {
		var i = n(47),
			r = n(69),
			s = n(52);
		t.exports = function (t) {
			var e = i(t),
				n = r.f;
			if (n)
				for (var o, a = n(t), l = s.f, c = 0; a.length > c; )
					l.call(t, (o = a[c++])) && e.push(o);
			return e;
		};
	},
	function (t, e, n) {
		n(17)("asyncIterator");
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(6),
			s = n(2),
			o = n(12),
			a = n(3),
			l = n(9).f,
			c = n(107),
			u = s.Symbol;
		if (
			r &&
			"function" == typeof u &&
			(!("description" in u.prototype) || void 0 !== u().description)
		) {
			var h = {},
				d = function () {
					var t =
							arguments.length < 1 || void 0 === arguments[0]
								? void 0
								: String(arguments[0]),
						e = this instanceof d ? new u(t) : void 0 === t ? u() : u(t);
					return "" === t && (h[e] = !0), e;
				};
			c(d, u);
			var f = (d.prototype = u.prototype);
			f.constructor = d;
			var p = f.toString,
				g = "Symbol(test)" == String(u("test")),
				v = /^Symbol\((.*)\)[^)]+$/;
			l(f, "description", {
				configurable: !0,
				get: function () {
					var t = a(this) ? this.valueOf() : this,
						e = p.call(t);
					if (o(h, t)) return "";
					var n = g ? e.slice(7, -1) : e.replace(v, "$1");
					return "" === n ? void 0 : n;
				},
			}),
				i({ global: !0, forced: !0 }, { Symbol: d });
		}
	},
	function (t, e, n) {
		n(17)("hasInstance");
	},
	function (t, e, n) {
		n(17)("isConcatSpreadable");
	},
	function (t, e, n) {
		n(17)("iterator");
	},
	function (t, e, n) {
		n(17)("match");
	},
	function (t, e, n) {
		n(17)("matchAll");
	},
	function (t, e, n) {
		n(17)("replace");
	},
	function (t, e, n) {
		n(17)("search");
	},
	function (t, e, n) {
		n(17)("species");
	},
	function (t, e, n) {
		n(17)("split");
	},
	function (t, e, n) {
		n(17)("toPrimitive");
	},
	function (t, e, n) {
		n(17)("toStringTag");
	},
	function (t, e, n) {
		n(17)("unscopables");
	},
	function (t, e, n) {
		var i = n(0),
			r = n(113);
		i(
			{ target: "Object", stat: !0, forced: Object.assign !== r },
			{ assign: r }
		);
	},
	function (t, e, n) {
		n(0)({ target: "Object", stat: !0, sham: !n(6) }, { create: n(36) });
	},
	function (t, e, n) {
		var i = n(0),
			r = n(6);
		i(
			{ target: "Object", stat: !0, forced: !r, sham: !r },
			{ defineProperty: n(9).f }
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(6);
		i(
			{ target: "Object", stat: !0, forced: !r, sham: !r },
			{ defineProperties: n(88) }
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(114);
		i(
			{ target: "Object", stat: !0 },
			{
				entries: function (t) {
					return r(t, !0);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(59),
			s = n(1),
			o = n(3),
			a = n(44).onFreeze,
			l = Object.freeze;
		i(
			{
				target: "Object",
				stat: !0,
				forced: s(function () {
					l(1);
				}),
				sham: !r,
			},
			{
				freeze: function (t) {
					return l && o(t) ? l(a(t)) : t;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(60),
			s = n(45);
		i(
			{ target: "Object", stat: !0 },
			{
				fromEntries: function (t) {
					var e = {};
					return (
						r(
							t,
							function (t, n) {
								s(e, t, n);
							},
							void 0,
							!0
						),
						e
					);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(1),
			s = n(18),
			o = n(15).f,
			a = n(6),
			l = r(function () {
				o(1);
			});
		i(
			{ target: "Object", stat: !0, forced: !a || l, sham: !a },
			{
				getOwnPropertyDescriptor: function (t, e) {
					return o(s(t), e);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(6),
			s = n(86),
			o = n(18),
			a = n(15),
			l = n(45);
		i(
			{ target: "Object", stat: !0, sham: !r },
			{
				getOwnPropertyDescriptors: function (t) {
					for (
						var e, n, i = o(t), r = a.f, c = s(i), u = {}, h = 0;
						c.length > h;

					)
						void 0 !== (n = r(i, (e = c[h++]))) && l(u, e, n);
					return u;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(1),
			s = n(112).f;
		i(
			{
				target: "Object",
				stat: !0,
				forced: r(function () {
					return !Object.getOwnPropertyNames(1);
				}),
			},
			{ getOwnPropertyNames: s }
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(1),
			s = n(10),
			o = n(30),
			a = n(90);
		i(
			{
				target: "Object",
				stat: !0,
				forced: r(function () {
					o(1);
				}),
				sham: !a,
			},
			{
				getPrototypeOf: function (t) {
					return o(s(t));
				},
			}
		);
	},
	function (t, e, n) {
		n(0)({ target: "Object", stat: !0 }, { is: n(116) });
	},
	function (t, e, n) {
		var i = n(0),
			r = n(1),
			s = n(3),
			o = Object.isExtensible;
		i(
			{
				target: "Object",
				stat: !0,
				forced: r(function () {
					o(1);
				}),
			},
			{
				isExtensible: function (t) {
					return !!s(t) && (!o || o(t));
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(1),
			s = n(3),
			o = Object.isFrozen;
		i(
			{
				target: "Object",
				stat: !0,
				forced: r(function () {
					o(1);
				}),
			},
			{
				isFrozen: function (t) {
					return !s(t) || (!!o && o(t));
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(1),
			s = n(3),
			o = Object.isSealed;
		i(
			{
				target: "Object",
				stat: !0,
				forced: r(function () {
					o(1);
				}),
			},
			{
				isSealed: function (t) {
					return !s(t) || (!!o && o(t));
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(10),
			s = n(47);
		i(
			{
				target: "Object",
				stat: !0,
				forced: n(1)(function () {
					s(1);
				}),
			},
			{
				keys: function (t) {
					return s(r(t));
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(3),
			s = n(44).onFreeze,
			o = n(59),
			a = n(1),
			l = Object.preventExtensions;
		i(
			{
				target: "Object",
				stat: !0,
				forced: a(function () {
					l(1);
				}),
				sham: !o,
			},
			{
				preventExtensions: function (t) {
					return l && r(t) ? l(s(t)) : t;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(3),
			s = n(44).onFreeze,
			o = n(59),
			a = n(1),
			l = Object.seal;
		i(
			{
				target: "Object",
				stat: !0,
				forced: a(function () {
					l(1);
				}),
				sham: !o,
			},
			{
				seal: function (t) {
					return l && r(t) ? l(s(t)) : t;
				},
			}
		);
	},
	function (t, e, n) {
		n(0)({ target: "Object", stat: !0 }, { setPrototypeOf: n(48) });
	},
	function (t, e, n) {
		var i = n(0),
			r = n(114);
		i(
			{ target: "Object", stat: !0 },
			{
				values: function (t) {
					return r(t);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(16),
			r = n(182),
			s = Object.prototype;
		r !== s.toString && i(s, "toString", r, { unsafe: !0 });
	},
	function (t, e, n) {
		"use strict";
		var i = n(63),
			r = {};
		(r[n(7)("toStringTag")] = "z"),
			(t.exports =
				"[object z]" !== String(r)
					? function () {
							return "[object " + i(this) + "]";
					  }
					: r.toString);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(6),
			s = n(71),
			o = n(10),
			a = n(21),
			l = n(9);
		r &&
			i(
				{ target: "Object", proto: !0, forced: s },
				{
					__defineGetter__: function (t, e) {
						l.f(o(this), t, { get: a(e), enumerable: !0, configurable: !0 });
					},
				}
			);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(6),
			s = n(71),
			o = n(10),
			a = n(21),
			l = n(9);
		r &&
			i(
				{ target: "Object", proto: !0, forced: s },
				{
					__defineSetter__: function (t, e) {
						l.f(o(this), t, { set: a(e), enumerable: !0, configurable: !0 });
					},
				}
			);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(6),
			s = n(71),
			o = n(10),
			a = n(27),
			l = n(30),
			c = n(15).f;
		r &&
			i(
				{ target: "Object", proto: !0, forced: s },
				{
					__lookupGetter__: function (t) {
						var e,
							n = o(this),
							i = a(t, !0);
						do {
							if ((e = c(n, i))) return e.get;
						} while ((n = l(n)));
					},
				}
			);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(6),
			s = n(71),
			o = n(10),
			a = n(27),
			l = n(30),
			c = n(15).f;
		r &&
			i(
				{ target: "Object", proto: !0, forced: s },
				{
					__lookupSetter__: function (t) {
						var e,
							n = o(this),
							i = a(t, !0);
						do {
							if ((e = c(n, i))) return e.set;
						} while ((n = l(n)));
					},
				}
			);
	},
	function (t, e, n) {
		n(0)({ target: "Function", proto: !0 }, { bind: n(118) });
	},
	function (t, e, n) {
		var i = n(6),
			r = n(9).f,
			s = Function.prototype,
			o = s.toString,
			a = /^\s*function ([^ (]*)/;
		!i ||
			"name" in s ||
			r(s, "name", {
				configurable: !0,
				get: function () {
					try {
						return o.call(this).match(a)[1];
					} catch (t) {
						return "";
					}
				},
			});
	},
	function (t, e, n) {
		"use strict";
		var i = n(3),
			r = n(9),
			s = n(30),
			o = n(7)("hasInstance"),
			a = Function.prototype;
		o in a ||
			r.f(a, o, {
				value: function (t) {
					if ("function" != typeof this || !i(t)) return !1;
					if (!i(this.prototype)) return t instanceof this;
					for (; (t = s(t)); ) if (this.prototype === t) return !0;
					return !1;
				},
			});
	},
	function (t, e, n) {
		var i = n(0),
			r = n(119);
		i(
			{
				target: "Array",
				stat: !0,
				forced: !n(72)(function (t) {
					Array.from(t);
				}),
			},
			{ from: r }
		);
	},
	function (t, e, n) {
		n(0)({ target: "Array", stat: !0 }, { isArray: n(43) });
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(1),
			s = n(45);
		i(
			{
				target: "Array",
				stat: !0,
				forced: r(function () {
					function t() {}
					return !(Array.of.call(t) instanceof t);
				}),
			},
			{
				of: function () {
					for (
						var t = 0,
							e = arguments.length,
							n = new ("function" == typeof this ? this : Array)(e);
						e > t;

					)
						s(n, t, arguments[t++]);
					return (n.length = e), n;
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(1),
			s = n(43),
			o = n(3),
			a = n(10),
			l = n(8),
			c = n(45),
			u = n(64),
			h = n(65),
			d = n(7)("isConcatSpreadable"),
			f = !r(function () {
				var t = [];
				return (t[d] = !1), t.concat()[0] !== t;
			}),
			p = h("concat"),
			g = function (t) {
				if (!o(t)) return !1;
				var e = t[d];
				return void 0 !== e ? !!e : s(t);
			};
		i(
			{ target: "Array", proto: !0, forced: !f || !p },
			{
				concat: function (t) {
					var e,
						n,
						i,
						r,
						s,
						o = a(this),
						h = u(o, 0),
						d = 0;
					for (e = -1, i = arguments.length; e < i; e++)
						if (((s = -1 === e ? o : arguments[e]), g(s))) {
							if (d + (r = l(s.length)) > 9007199254740991)
								throw TypeError("Maximum allowed index exceeded");
							for (n = 0; n < r; n++, d++) n in s && c(h, d, s[n]);
						} else {
							if (d >= 9007199254740991)
								throw TypeError("Maximum allowed index exceeded");
							c(h, d++, s);
						}
					return (h.length = d), h;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(120),
			s = n(38);
		i({ target: "Array", proto: !0 }, { copyWithin: r }), s("copyWithin");
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(13),
			s = n(31),
			o = r(4);
		i(
			{ target: "Array", proto: !0, forced: s("every") },
			{
				every: function (t) {
					return o(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(91),
			s = n(38);
		i({ target: "Array", proto: !0 }, { fill: r }), s("fill");
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(13),
			s = n(65),
			o = r(2);
		i(
			{ target: "Array", proto: !0, forced: !s("filter") },
			{
				filter: function (t) {
					return o(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(13),
			s = n(38),
			o = r(5),
			a = !0;
		"find" in [] &&
			Array(1).find(function () {
				a = !1;
			}),
			i(
				{ target: "Array", proto: !0, forced: a },
				{
					find: function (t) {
						return o(this, t, arguments.length > 1 ? arguments[1] : void 0);
					},
				}
			),
			s("find");
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(13),
			s = n(38),
			o = r(6),
			a = !0;
		"findIndex" in [] &&
			Array(1).findIndex(function () {
				a = !1;
			}),
			i(
				{ target: "Array", proto: !0, forced: a },
				{
					findIndex: function (t) {
						return o(this, t, arguments.length > 1 ? arguments[1] : void 0);
					},
				}
			),
			s("findIndex");
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(121),
			s = n(10),
			o = n(8),
			a = n(24),
			l = n(64);
		i(
			{ target: "Array", proto: !0 },
			{
				flat: function () {
					var t = arguments[0],
						e = s(this),
						n = o(e.length),
						i = l(e, 0);
					return (i.length = r(i, e, e, n, 0, void 0 === t ? 1 : a(t))), i;
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(121),
			s = n(10),
			o = n(8),
			a = n(21),
			l = n(64);
		i(
			{ target: "Array", proto: !0 },
			{
				flatMap: function (t) {
					var e,
						n = s(this),
						i = o(n.length);
					return (
						a(t),
						((e = l(n, 0)).length = r(e, n, n, i, 0, 1, t, arguments[1])),
						e
					);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(203);
		i({ target: "Array", proto: !0, forced: [].forEach != r }, { forEach: r });
	},
	function (t, e, n) {
		"use strict";
		var i = n(13),
			r = n(31),
			s = i(0),
			o = r("forEach");
		t.exports = o
			? function (t) {
					return s(this, t, arguments[1]);
			  }
			: [].forEach;
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(57),
			s = n(38),
			o = r(!0);
		i(
			{ target: "Array", proto: !0 },
			{
				includes: function (t) {
					return o(this, t, arguments.length > 1 ? arguments[1] : void 0);
				},
			}
		),
			s("includes");
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(31),
			s = n(57)(!1),
			o = [].indexOf,
			a = !!o && 1 / [1].indexOf(1, -0) < 0,
			l = r("indexOf");
		i(
			{ target: "Array", proto: !0, forced: a || l },
			{
				indexOf: function (t) {
					return a ? o.apply(this, arguments) || 0 : s(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(53),
			s = n(18),
			o = n(31),
			a = [].join,
			l = r != Object,
			c = o("join", ",");
		i(
			{ target: "Array", proto: !0, forced: l || c },
			{
				join: function (t) {
					return a.call(s(this), void 0 === t ? "," : t);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(122);
		i(
			{ target: "Array", proto: !0, forced: r !== [].lastIndexOf },
			{ lastIndexOf: r }
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(13),
			s = n(65),
			o = r(1);
		i(
			{ target: "Array", proto: !0, forced: !s("map") },
			{
				map: function (t) {
					return o(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(73);
		i(
			{ target: "Array", proto: !0, forced: n(31)("reduce") },
			{
				reduce: function (t) {
					return r(this, t, arguments.length, arguments[1], !1);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(73);
		i(
			{ target: "Array", proto: !0, forced: n(31)("reduceRight") },
			{
				reduceRight: function (t) {
					return r(this, t, arguments.length, arguments[1], !0);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(43),
			s = [].reverse,
			o = [1, 2];
		i(
			{ target: "Array", proto: !0, forced: String(o) === String(o.reverse()) },
			{
				reverse: function () {
					return r(this) && (this.length = this.length), s.call(this);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(3),
			s = n(43),
			o = n(35),
			a = n(8),
			l = n(18),
			c = n(45),
			u = n(65),
			h = n(7)("species"),
			d = [].slice,
			f = Math.max;
		i(
			{ target: "Array", proto: !0, forced: !u("slice") },
			{
				slice: function (t, e) {
					var n,
						i,
						u,
						p = l(this),
						g = a(p.length),
						v = o(t, g),
						_ = o(void 0 === e ? g : e, g);
					if (
						s(p) &&
						("function" != typeof (n = p.constructor) ||
						(n !== Array && !s(n.prototype))
							? r(n) && null === (n = n[h]) && (n = void 0)
							: (n = void 0),
						n === Array || void 0 === n)
					)
						return d.call(p, v, _);
					for (
						i = new (void 0 === n ? Array : n)(f(_ - v, 0)), u = 0;
						v < _;
						v++, u++
					)
						v in p && c(i, u, p[v]);
					return (i.length = u), i;
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(13),
			s = n(31),
			o = r(3);
		i(
			{ target: "Array", proto: !0, forced: s("some") },
			{
				some: function (t) {
					return o(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(21),
			s = n(10),
			o = n(1),
			a = n(31),
			l = [].sort,
			c = [1, 2, 3],
			u = o(function () {
				c.sort(void 0);
			}),
			h = o(function () {
				c.sort(null);
			}),
			d = a("sort");
		i(
			{ target: "Array", proto: !0, forced: u || !h || d },
			{
				sort: function (t) {
					return void 0 === t ? l.call(s(this)) : l.call(s(this), r(t));
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(35),
			s = n(24),
			o = n(8),
			a = n(10),
			l = n(64),
			c = n(45),
			u = n(65),
			h = Math.max,
			d = Math.min;
		i(
			{ target: "Array", proto: !0, forced: !u("splice") },
			{
				splice: function (t, e) {
					var n,
						i,
						u,
						f,
						p,
						g,
						v = a(this),
						_ = o(v.length),
						m = r(t, _),
						y = arguments.length;
					if (
						(0 === y
							? (n = i = 0)
							: 1 === y
							? ((n = 0), (i = _ - m))
							: ((n = y - 2), (i = d(h(s(e), 0), _ - m))),
						_ + n - i > 9007199254740991)
					)
						throw TypeError("Maximum allowed length exceeded");
					for (u = l(v, i), f = 0; f < i; f++)
						(p = m + f) in v && c(u, f, v[p]);
					if (((u.length = i), n < i)) {
						for (f = m; f < _ - i; f++)
							(g = f + n), (p = f + i) in v ? (v[g] = v[p]) : delete v[g];
						for (f = _; f > _ - i + n; f--) delete v[f - 1];
					} else if (n > i)
						for (f = _ - i; f > m; f--)
							(g = f + n - 1),
								(p = f + i - 1) in v ? (v[g] = v[p]) : delete v[g];
					for (f = 0; f < n; f++) v[f + m] = arguments[f + 2];
					return (v.length = _ - i + n), u;
				},
			}
		);
	},
	function (t, e, n) {
		n(49)("Array");
	},
	function (t, e, n) {
		n(38)("flat");
	},
	function (t, e, n) {
		n(38)("flatMap");
	},
	function (t, e, n) {
		var i = n(0),
			r = n(35),
			s = String.fromCharCode,
			o = String.fromCodePoint;
		i(
			{ target: "String", stat: !0, forced: !!o && 1 != o.length },
			{
				fromCodePoint: function (t) {
					for (var e, n = [], i = arguments.length, o = 0; i > o; ) {
						if (((e = +arguments[o++]), r(e, 1114111) !== e))
							throw RangeError(e + " is not a valid code point");
						n.push(
							e < 65536
								? s(e)
								: s(55296 + ((e -= 65536) >> 10), (e % 1024) + 56320)
						);
					}
					return n.join("");
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(18),
			s = n(8);
		i(
			{ target: "String", stat: !0 },
			{
				raw: function (t) {
					for (
						var e = r(t.raw),
							n = s(e.length),
							i = arguments.length,
							o = [],
							a = 0;
						n > a;

					)
						o.push(String(e[a++])), a < i && o.push(String(arguments[a]));
					return o.join("");
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(74);
		i(
			{ target: "String", proto: !0 },
			{
				codePointAt: function (t) {
					return r(this, t);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(8),
			s = n(95),
			o = n(97),
			a = "".endsWith,
			l = Math.min;
		i(
			{ target: "String", proto: !0, forced: !o("endsWith") },
			{
				endsWith: function (t) {
					var e = s(this, t, "endsWith"),
						n = arguments.length > 1 ? arguments[1] : void 0,
						i = r(e.length),
						o = void 0 === n ? i : l(r(n), i),
						c = String(t);
					return a ? a.call(e, c, o) : e.slice(o - c.length, o) === c;
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(95);
		i(
			{ target: "String", proto: !0, forced: !n(97)("includes") },
			{
				includes: function (t) {
					return !!~r(this, t, "includes").indexOf(
						t,
						arguments.length > 1 ? arguments[1] : void 0
					);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(75),
			r = n(4),
			s = n(8),
			o = n(19),
			a = n(77),
			l = n(78);
		i("match", 1, function (t, e, n) {
			return [
				function (e) {
					var n = o(this),
						i = null == e ? void 0 : e[t];
					return void 0 !== i ? i.call(e, n) : new RegExp(e)[t](String(n));
				},
				function (t) {
					var i = n(e, t, this);
					if (i.done) return i.value;
					var o = r(t),
						c = String(this);
					if (!o.global) return l(o, c);
					var u = o.unicode;
					o.lastIndex = 0;
					for (var h, d = [], f = 0; null !== (h = l(o, c)); ) {
						var p = String(h[0]);
						(d[f] = p),
							"" === p && (o.lastIndex = a(c, s(o.lastIndex), u)),
							f++;
					}
					return 0 === f ? null : d;
				},
			];
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(94),
			s = n(19),
			o = n(8),
			a = n(21),
			l = n(4),
			c = n(63),
			u = n(66),
			h = n(14),
			d = n(7),
			f = n(32),
			p = n(77),
			g = n(20),
			v = n(34),
			_ = d("matchAll"),
			m = g.set,
			y = g.getterFor("RegExp String Iterator"),
			b = RegExp.prototype,
			w = b.exec,
			S = r(
				function (t, e, n, i) {
					m(this, {
						type: "RegExp String Iterator",
						regexp: t,
						string: e,
						global: n,
						unicode: i,
						done: !1,
					});
				},
				"RegExp String",
				function () {
					var t = y(this);
					if (t.done) return { value: void 0, done: !0 };
					var e = t.regexp,
						n = t.string,
						i = (function (t, e) {
							var n,
								i = t.exec;
							if ("function" == typeof i) {
								if ("object" != typeof (n = i.call(t, e)))
									throw TypeError("Incorrect exec result");
								return n;
							}
							return w.call(t, e);
						})(e, n);
					return null === i
						? { value: void 0, done: (t.done = !0) }
						: t.global
						? ("" == String(i[0]) &&
								(e.lastIndex = p(n, o(e.lastIndex), t.unicode)),
						  { value: i, done: !1 })
						: ((t.done = !0), { value: i, done: !1 });
				}
			),
			E = function (t) {
				var e,
					n,
					i,
					r,
					s,
					a,
					c = l(this),
					h = String(t);
				return (
					(e = f(c, RegExp)),
					void 0 === (n = c.flags) &&
						c instanceof RegExp &&
						!("flags" in b) &&
						(n = u.call(c)),
					(i = void 0 === n ? "" : String(n)),
					(r = new e(e === RegExp ? c.source : c, i)),
					(s = !!~i.indexOf("g")),
					(a = !!~i.indexOf("u")),
					(r.lastIndex = o(c.lastIndex)),
					new S(r, h, s, a)
				);
			};
		i(
			{ target: "String", proto: !0 },
			{
				matchAll: function (t) {
					var e,
						n,
						i,
						r = s(this);
					return null != t &&
						(void 0 === (n = t[_]) && v && "RegExp" == c(t) && (n = E),
						null != n)
						? a(n).call(t, r)
						: ((e = String(r)),
						  (i = new RegExp(t, "g")),
						  v ? E.call(i, e) : i[_](e));
				},
			}
		),
			v || _ in b || h(b, _, E);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(124);
		i(
			{ target: "String", proto: !0, forced: n(125) },
			{
				padEnd: function (t) {
					return r(this, t, arguments.length > 1 ? arguments[1] : void 0, !1);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(124);
		i(
			{ target: "String", proto: !0, forced: n(125) },
			{
				padStart: function (t) {
					return r(this, t, arguments.length > 1 ? arguments[1] : void 0, !0);
				},
			}
		);
	},
	function (t, e, n) {
		n(0)({ target: "String", proto: !0 }, { repeat: n(98) });
	},
	function (t, e, n) {
		"use strict";
		var i = n(75),
			r = n(4),
			s = n(10),
			o = n(8),
			a = n(24),
			l = n(19),
			c = n(77),
			u = n(78),
			h = Math.max,
			d = Math.min,
			f = Math.floor,
			p = /\$([$&'`]|\d\d?|<[^>]*>)/g,
			g = /\$([$&'`]|\d\d?)/g;
		i("replace", 2, function (t, e, n) {
			return [
				function (n, i) {
					var r = l(this),
						s = null == n ? void 0 : n[t];
					return void 0 !== s ? s.call(n, r, i) : e.call(String(r), n, i);
				},
				function (t, s) {
					var l = n(e, t, this, s);
					if (l.done) return l.value;
					var f = r(t),
						p = String(this),
						g = "function" == typeof s;
					g || (s = String(s));
					var v = f.global;
					if (v) {
						var _ = f.unicode;
						f.lastIndex = 0;
					}
					for (var m = []; ; ) {
						var y = u(f, p);
						if (null === y) break;
						if ((m.push(y), !v)) break;
						"" === String(y[0]) && (f.lastIndex = c(p, o(f.lastIndex), _));
					}
					for (var b, w = "", S = 0, E = 0; E < m.length; E++) {
						y = m[E];
						for (
							var T = String(y[0]),
								L = h(d(a(y.index), p.length), 0),
								A = [],
								I = 1;
							I < y.length;
							I++
						)
							A.push(void 0 === (b = y[I]) ? b : String(b));
						var C = y.groups;
						if (g) {
							var M = [T].concat(A, L, p);
							void 0 !== C && M.push(C);
							var x = String(s.apply(void 0, M));
						} else x = i(T, p, L, A, C, s);
						L >= S && ((w += p.slice(S, L) + x), (S = L + T.length));
					}
					return w + p.slice(S);
				},
			];
			function i(t, n, i, r, o, a) {
				var l = i + t.length,
					c = r.length,
					u = g;
				return (
					void 0 !== o && ((o = s(o)), (u = p)),
					e.call(a, u, function (e, s) {
						var a;
						switch (s.charAt(0)) {
							case "$":
								return "$";
							case "&":
								return t;
							case "`":
								return n.slice(0, i);
							case "'":
								return n.slice(l);
							case "<":
								a = o[s.slice(1, -1)];
								break;
							default:
								var u = +s;
								if (0 === u) return e;
								if (u > c) {
									var h = f(u / 10);
									return 0 === h
										? e
										: h <= c
										? void 0 === r[h - 1]
											? s.charAt(1)
											: r[h - 1] + s.charAt(1)
										: e;
								}
								a = r[u - 1];
						}
						return void 0 === a ? "" : a;
					})
				);
			}
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(75),
			r = n(4),
			s = n(19),
			o = n(116),
			a = n(78);
		i("search", 1, function (t, e, n) {
			return [
				function (e) {
					var n = s(this),
						i = null == e ? void 0 : e[t];
					return void 0 !== i ? i.call(e, n) : new RegExp(e)[t](String(n));
				},
				function (t) {
					var i = n(e, t, this);
					if (i.done) return i.value;
					var s = r(t),
						l = String(this),
						c = s.lastIndex;
					o(c, 0) || (s.lastIndex = 0);
					var u = a(s, l);
					return (
						o(s.lastIndex, c) || (s.lastIndex = c), null === u ? -1 : u.index
					);
				},
			];
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(75),
			r = n(96),
			s = n(4),
			o = n(19),
			a = n(32),
			l = n(77),
			c = n(8),
			u = n(78),
			h = n(76),
			d = n(1),
			f = [].push,
			p = Math.min,
			g = !d(function () {
				return !RegExp(4294967295, "y");
			});
		i(
			"split",
			2,
			function (t, e, n) {
				var i;
				return (
					(i =
						"c" == "abbc".split(/(b)*/)[1] ||
						4 != "test".split(/(?:)/, -1).length ||
						2 != "ab".split(/(?:ab)*/).length ||
						4 != ".".split(/(.?)(.?)/).length ||
						".".split(/()()/).length > 1 ||
						"".split(/.?/).length
							? function (t, n) {
									var i = String(o(this)),
										s = void 0 === n ? 4294967295 : n >>> 0;
									if (0 === s) return [];
									if (void 0 === t) return [i];
									if (!r(t)) return e.call(i, t, s);
									for (
										var a,
											l,
											c,
											u = [],
											d =
												(t.ignoreCase ? "i" : "") +
												(t.multiline ? "m" : "") +
												(t.unicode ? "u" : "") +
												(t.sticky ? "y" : ""),
											p = 0,
											g = new RegExp(t.source, d + "g");
										(a = h.call(g, i)) &&
										!(
											(l = g.lastIndex) > p &&
											(u.push(i.slice(p, a.index)),
											a.length > 1 &&
												a.index < i.length &&
												f.apply(u, a.slice(1)),
											(c = a[0].length),
											(p = l),
											u.length >= s)
										);

									)
										g.lastIndex === a.index && g.lastIndex++;
									return (
										p === i.length
											? (!c && g.test("")) || u.push("")
											: u.push(i.slice(p)),
										u.length > s ? u.slice(0, s) : u
									);
							  }
							: "0".split(void 0, 0).length
							? function (t, n) {
									return void 0 === t && 0 === n ? [] : e.call(this, t, n);
							  }
							: e),
					[
						function (e, n) {
							var r = o(this),
								s = null == e ? void 0 : e[t];
							return void 0 !== s ? s.call(e, r, n) : i.call(String(r), e, n);
						},
						function (t, r) {
							var o = n(i, t, this, r, i !== e);
							if (o.done) return o.value;
							var h = s(t),
								d = String(this),
								f = a(h, RegExp),
								v = h.unicode,
								_ =
									(h.ignoreCase ? "i" : "") +
									(h.multiline ? "m" : "") +
									(h.unicode ? "u" : "") +
									(g ? "y" : "g"),
								m = new f(g ? h : "^(?:" + h.source + ")", _),
								y = void 0 === r ? 4294967295 : r >>> 0;
							if (0 === y) return [];
							if (0 === d.length) return null === u(m, d) ? [d] : [];
							for (var b = 0, w = 0, S = []; w < d.length; ) {
								m.lastIndex = g ? w : 0;
								var E,
									T = u(m, g ? d : d.slice(w));
								if (
									null === T ||
									(E = p(c(m.lastIndex + (g ? 0 : w)), d.length)) === b
								)
									w = l(d, w, v);
								else {
									if ((S.push(d.slice(b, w)), S.length === y)) return S;
									for (var L = 1; L <= T.length - 1; L++)
										if ((S.push(T[L]), S.length === y)) return S;
									w = b = E;
								}
							}
							return S.push(d.slice(b)), S;
						},
					]
				);
			},
			!g
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(8),
			s = n(95),
			o = n(97),
			a = "".startsWith;
		i(
			{ target: "String", proto: !0, forced: !o("startsWith") },
			{
				startsWith: function (t) {
					var e = s(this, t, "startsWith"),
						n = r(
							Math.min(arguments.length > 1 ? arguments[1] : void 0, e.length)
						),
						i = String(t);
					return a ? a.call(e, i, n) : e.slice(n, n + i.length) === i;
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(50);
		i(
			{ target: "String", proto: !0, forced: n(100)("trim") },
			{
				trim: function () {
					return r(this, 3);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(50),
			s = n(100)("trimStart"),
			o = s
				? function () {
						return r(this, 1);
				  }
				: "".trimStart;
		i(
			{ target: "String", proto: !0, forced: s },
			{ trimStart: o, trimLeft: o }
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(50),
			s = n(100)("trimEnd"),
			o = s
				? function () {
						return r(this, 2);
				  }
				: "".trimEnd;
		i({ target: "String", proto: !0, forced: s }, { trimEnd: o, trimRight: o });
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("anchor") },
			{
				anchor: function (t) {
					return r(this, "a", "name", t);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("big") },
			{
				big: function () {
					return r(this, "big", "", "");
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("blink") },
			{
				blink: function () {
					return r(this, "blink", "", "");
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("bold") },
			{
				bold: function () {
					return r(this, "b", "", "");
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("fixed") },
			{
				fixed: function () {
					return r(this, "tt", "", "");
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("fontcolor") },
			{
				fontcolor: function (t) {
					return r(this, "font", "color", t);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("fontsize") },
			{
				fontsize: function (t) {
					return r(this, "font", "size", t);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("italics") },
			{
				italics: function () {
					return r(this, "i", "", "");
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("link") },
			{
				link: function (t) {
					return r(this, "a", "href", t);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("small") },
			{
				small: function () {
					return r(this, "small", "", "");
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("strike") },
			{
				strike: function () {
					return r(this, "strike", "", "");
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("sub") },
			{
				sub: function () {
					return r(this, "sub", "", "");
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(22);
		i(
			{ target: "String", proto: !0, forced: n(23)("sup") },
			{
				sup: function () {
					return r(this, "sup", "", "");
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(6),
			r = n(2),
			s = n(58),
			o = n(101),
			a = n(9).f,
			l = n(42).f,
			c = n(96),
			u = n(66),
			h = n(16),
			d = n(1),
			f = n(49),
			p = n(7)("match"),
			g = r.RegExp,
			v = g.prototype,
			_ = /a/g,
			m = /a/g,
			y = new g(_) !== _;
		if (
			s(
				"RegExp",
				i &&
					(!y ||
						d(function () {
							return (m[p] = !1), g(_) != _ || g(m) == m || "/a/i" != g(_, "i");
						}))
			)
		) {
			for (
				var b = function (t, e) {
						var n = this instanceof b,
							i = c(t),
							r = void 0 === e;
						return !n && i && t.constructor === b && r
							? t
							: o(
									y
										? new g(i && !r ? t.source : t, e)
										: g(
												(i = t instanceof b) ? t.source : t,
												i && r ? u.call(t) : e
										  ),
									n ? this : v,
									b
							  );
					},
					w = function (t) {
						(t in b) ||
							a(b, t, {
								configurable: !0,
								get: function () {
									return g[t];
								},
								set: function (e) {
									g[t] = e;
								},
							});
					},
					S = l(g),
					E = 0;
				E < S.length;

			)
				w(S[E++]);
			(v.constructor = b), (b.prototype = v), h(r, "RegExp", b);
		}
		f("RegExp");
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(76);
		i({ target: "RegExp", proto: !0, forced: /./.exec !== r }, { exec: r });
	},
	function (t, e, n) {
		var i = n(6),
			r = n(9),
			s = n(66);
		i &&
			"g" != /./g.flags &&
			r.f(RegExp.prototype, "flags", { configurable: !0, get: s });
	},
	function (t, e, n) {
		"use strict";
		var i = n(16),
			r = n(4),
			s = n(1),
			o = n(66),
			a = /./.toString,
			l = RegExp.prototype,
			c = s(function () {
				return "/a/b" != a.call({ source: "a", flags: "b" });
			}),
			u = "toString" != a.name;
		(c || u) &&
			i(
				RegExp.prototype,
				"toString",
				function () {
					var t = r(this),
						e = String(t.source),
						n = t.flags;
					return (
						"/" +
						e +
						"/" +
						String(
							void 0 === n && t instanceof RegExp && !("flags" in l)
								? o.call(t)
								: n
						)
					);
				},
				{ unsafe: !0 }
			);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(127);
		i({ global: !0, forced: parseInt != r }, { parseInt: r });
	},
	function (t, e, n) {
		var i = n(0),
			r = n(128);
		i({ global: !0, forced: parseFloat != r }, { parseFloat: r });
	},
	function (t, e, n) {
		"use strict";
		var i = n(6),
			r = n(2),
			s = n(58),
			o = n(16),
			a = n(12),
			l = n(28),
			c = n(101),
			u = n(27),
			h = n(1),
			d = n(36),
			f = n(42).f,
			p = n(15).f,
			g = n(9).f,
			v = n(50),
			_ = r.Number,
			m = _.prototype,
			y = "Number" == l(d(m)),
			b = "trim" in String.prototype,
			w = function (t) {
				var e,
					n,
					i,
					r,
					s,
					o,
					a,
					l,
					c = u(t, !1);
				if ("string" == typeof c && c.length > 2)
					if (
						43 === (e = (c = b ? c.trim() : v(c, 3)).charCodeAt(0)) ||
						45 === e
					) {
						if (88 === (n = c.charCodeAt(2)) || 120 === n) return NaN;
					} else if (48 === e) {
						switch (c.charCodeAt(1)) {
							case 66:
							case 98:
								(i = 2), (r = 49);
								break;
							case 79:
							case 111:
								(i = 8), (r = 55);
								break;
							default:
								return +c;
						}
						for (o = (s = c.slice(2)).length, a = 0; a < o; a++)
							if ((l = s.charCodeAt(a)) < 48 || l > r) return NaN;
						return parseInt(s, i);
					}
				return +c;
			};
		if (s("Number", !_(" 0o1") || !_("0b1") || _("+0x1"))) {
			for (
				var S,
					E = function (t) {
						var e = arguments.length < 1 ? 0 : t,
							n = this;
						return n instanceof E &&
							(y
								? h(function () {
										m.valueOf.call(n);
								  })
								: "Number" != l(n))
							? c(new _(w(e)), n, E)
							: w(e);
					},
					T = i
						? f(_)
						: "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(
								","
						  ),
					L = 0;
				T.length > L;
				L++
			)
				a(_, (S = T[L])) && !a(E, S) && g(E, S, p(_, S));
			(E.prototype = m), (m.constructor = E), o(r, "Number", E);
		}
	},
	function (t, e, n) {
		n(0)({ target: "Number", stat: !0 }, { EPSILON: Math.pow(2, -52) });
	},
	function (t, e, n) {
		n(0)({ target: "Number", stat: !0 }, { isFinite: n(258) });
	},
	function (t, e, n) {
		var i = n(2).isFinite;
		t.exports =
			Number.isFinite ||
			function (t) {
				return "number" == typeof t && i(t);
			};
	},
	function (t, e, n) {
		n(0)({ target: "Number", stat: !0 }, { isInteger: n(129) });
	},
	function (t, e, n) {
		n(0)(
			{ target: "Number", stat: !0 },
			{
				isNaN: function (t) {
					return t != t;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(129),
			s = Math.abs;
		i(
			{ target: "Number", stat: !0 },
			{
				isSafeInteger: function (t) {
					return r(t) && s(t) <= 9007199254740991;
				},
			}
		);
	},
	function (t, e, n) {
		n(0)(
			{ target: "Number", stat: !0 },
			{ MAX_SAFE_INTEGER: 9007199254740991 }
		);
	},
	function (t, e, n) {
		n(0)(
			{ target: "Number", stat: !0 },
			{ MIN_SAFE_INTEGER: -9007199254740991 }
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(128);
		i(
			{ target: "Number", stat: !0, forced: Number.parseFloat != r },
			{ parseFloat: r }
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(127);
		i(
			{ target: "Number", stat: !0, forced: Number.parseInt != r },
			{ parseInt: r }
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(24),
			s = n(130),
			o = n(98),
			a = n(1),
			l = (1).toFixed,
			c = Math.floor,
			u = [0, 0, 0, 0, 0, 0],
			h = function (t, e) {
				for (var n = -1, i = e; ++n < 6; )
					(i += t * u[n]), (u[n] = i % 1e7), (i = c(i / 1e7));
			},
			d = function (t) {
				for (var e = 6, n = 0; --e >= 0; )
					(n += u[e]), (u[e] = c(n / t)), (n = (n % t) * 1e7);
			},
			f = function () {
				for (var t = 6, e = ""; --t >= 0; )
					if ("" !== e || 0 === t || 0 !== u[t]) {
						var n = String(u[t]);
						e = "" === e ? n : e + o.call("0", 7 - n.length) + n;
					}
				return e;
			},
			p = function (t, e, n) {
				return 0 === e
					? n
					: e % 2 == 1
					? p(t, e - 1, n * t)
					: p(t * t, e / 2, n);
			};
		i(
			{
				target: "Number",
				proto: !0,
				forced:
					(l &&
						("0.000" !== (8e-5).toFixed(3) ||
							"1" !== (0.9).toFixed(0) ||
							"1.25" !== (1.255).toFixed(2) ||
							"1000000000000000128" !== (0xde0b6b3a7640080).toFixed(0))) ||
					!a(function () {
						l.call({});
					}),
			},
			{
				toFixed: function (t) {
					var e,
						n,
						i,
						a,
						l = s(this),
						c = r(t),
						u = "",
						g = "0";
					if (c < 0 || c > 20) throw RangeError("Incorrect fraction digits");
					if (l != l) return "NaN";
					if (l <= -1e21 || l >= 1e21) return String(l);
					if ((l < 0 && ((u = "-"), (l = -l)), l > 1e-21))
						if (
							((n =
								(e =
									(function (t) {
										for (var e = 0, n = t; n >= 4096; ) (e += 12), (n /= 4096);
										for (; n >= 2; ) (e += 1), (n /= 2);
										return e;
									})(l * p(2, 69, 1)) - 69) < 0
									? l * p(2, -e, 1)
									: l / p(2, e, 1)),
							(n *= 4503599627370496),
							(e = 52 - e) > 0)
						) {
							for (h(0, n), i = c; i >= 7; ) h(1e7, 0), (i -= 7);
							for (h(p(10, i, 1), 0), i = e - 1; i >= 23; )
								d(1 << 23), (i -= 23);
							d(1 << i), h(1, 1), d(2), (g = f());
						} else h(0, n), h(1 << -e, 0), (g = f() + o.call("0", c));
					return (g =
						c > 0
							? u +
							  ((a = g.length) <= c
									? "0." + o.call("0", c - a) + g
									: g.slice(0, a - c) + "." + g.slice(a - c))
							: u + g);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(1),
			s = n(130),
			o = (1).toPrecision;
		i(
			{
				target: "Number",
				proto: !0,
				forced:
					r(function () {
						return "1" !== o.call(1, void 0);
					}) ||
					!r(function () {
						o.call({});
					}),
			},
			{
				toPrecision: function (t) {
					return void 0 === t ? o.call(s(this)) : o.call(s(this), t);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(131),
			s = Math.acosh,
			o = Math.log,
			a = Math.sqrt,
			l = Math.LN2;
		i(
			{
				target: "Math",
				stat: !0,
				forced:
					!s || 710 != Math.floor(s(Number.MAX_VALUE)) || s(1 / 0) != 1 / 0,
			},
			{
				acosh: function (t) {
					return (t = +t) < 1
						? NaN
						: t > 94906265.62425156
						? o(t) + l
						: r(t - 1 + a(t - 1) * a(t + 1));
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = Math.asinh,
			s = Math.log,
			o = Math.sqrt;
		i(
			{ target: "Math", stat: !0, forced: !(r && 1 / r(0) > 0) },
			{
				asinh: function t(e) {
					return isFinite((e = +e)) && 0 != e
						? e < 0
							? -t(-e)
							: s(e + o(e * e + 1))
						: e;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = Math.atanh,
			s = Math.log;
		i(
			{ target: "Math", stat: !0, forced: !(r && 1 / r(-0) < 0) },
			{
				atanh: function (t) {
					return 0 == (t = +t) ? t : s((1 + t) / (1 - t)) / 2;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(102),
			s = Math.abs,
			o = Math.pow;
		i(
			{ target: "Math", stat: !0 },
			{
				cbrt: function (t) {
					return r((t = +t)) * o(s(t), 1 / 3);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = Math.floor,
			s = Math.log,
			o = Math.LOG2E;
		i(
			{ target: "Math", stat: !0 },
			{
				clz32: function (t) {
					return (t >>>= 0) ? 31 - r(s(t + 0.5) * o) : 32;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(80),
			s = Math.cosh,
			o = Math.abs,
			a = Math.E;
		i(
			{ target: "Math", stat: !0, forced: !s || s(710) === 1 / 0 },
			{
				cosh: function (t) {
					var e = r(o(t) - 1) + 1;
					return (e + 1 / (e * a * a)) * (a / 2);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(80);
		i({ target: "Math", stat: !0, forced: r != Math.expm1 }, { expm1: r });
	},
	function (t, e, n) {
		n(0)({ target: "Math", stat: !0 }, { fround: n(276) });
	},
	function (t, e, n) {
		var i = n(102),
			r = Math.pow,
			s = r(2, -52),
			o = r(2, -23),
			a = r(2, 127) * (2 - o),
			l = r(2, -126);
		t.exports =
			Math.fround ||
			function (t) {
				var e,
					n,
					r = Math.abs(t),
					c = i(t);
				return r < l
					? c * (r / l / o + 1 / s - 1 / s) * l * o
					: (n = (e = (1 + o / s) * r) - (e - r)) > a || n != n
					? c * (1 / 0)
					: c * n;
			};
	},
	function (t, e, n) {
		var i = n(0),
			r = Math.abs,
			s = Math.sqrt;
		i(
			{ target: "Math", stat: !0 },
			{
				hypot: function (t, e) {
					for (var n, i, o = 0, a = 0, l = arguments.length, c = 0; a < l; )
						c < (n = r(arguments[a++]))
							? ((o = o * (i = c / n) * i + 1), (c = n))
							: (o += n > 0 ? (i = n / c) * i : n);
					return c === 1 / 0 ? 1 / 0 : c * s(o);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(1),
			s = Math.imul;
		i(
			{
				target: "Math",
				stat: !0,
				forced: r(function () {
					return -5 != s(4294967295, 5) || 2 != s.length;
				}),
			},
			{
				imul: function (t, e) {
					var n = +t,
						i = +e,
						r = 65535 & n,
						s = 65535 & i;
					return (
						0 |
						(r * s +
							((((65535 & (n >>> 16)) * s + r * (65535 & (i >>> 16))) << 16) >>>
								0))
					);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = Math.log,
			s = Math.LOG10E;
		i(
			{ target: "Math", stat: !0 },
			{
				log10: function (t) {
					return r(t) * s;
				},
			}
		);
	},
	function (t, e, n) {
		n(0)({ target: "Math", stat: !0 }, { log1p: n(131) });
	},
	function (t, e, n) {
		var i = n(0),
			r = Math.log,
			s = Math.LN2;
		i(
			{ target: "Math", stat: !0 },
			{
				log2: function (t) {
					return r(t) / s;
				},
			}
		);
	},
	function (t, e, n) {
		n(0)({ target: "Math", stat: !0 }, { sign: n(102) });
	},
	function (t, e, n) {
		var i = n(0),
			r = n(1),
			s = n(80),
			o = Math.abs,
			a = Math.exp,
			l = Math.E;
		i(
			{
				target: "Math",
				stat: !0,
				forced: r(function () {
					return -2e-17 != Math.sinh(-2e-17);
				}),
			},
			{
				sinh: function (t) {
					return o((t = +t)) < 1
						? (s(t) - s(-t)) / 2
						: (a(t - 1) - a(-t - 1)) * (l / 2);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(80),
			s = Math.exp;
		i(
			{ target: "Math", stat: !0 },
			{
				tanh: function (t) {
					var e = r((t = +t)),
						n = r(-t);
					return e == 1 / 0 ? 1 : n == 1 / 0 ? -1 : (e - n) / (s(t) + s(-t));
				},
			}
		);
	},
	function (t, e, n) {
		n(29)(Math, "Math", !0);
	},
	function (t, e, n) {
		var i = n(0),
			r = Math.ceil,
			s = Math.floor;
		i(
			{ target: "Math", stat: !0 },
			{
				trunc: function (t) {
					return (t > 0 ? s : r)(t);
				},
			}
		);
	},
	function (t, e, n) {
		n(0)(
			{ target: "Date", stat: !0 },
			{
				now: function () {
					return new Date().getTime();
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(1),
			s = n(10),
			o = n(27);
		i(
			{
				target: "Date",
				proto: !0,
				forced: r(function () {
					return (
						null !== new Date(NaN).toJSON() ||
						1 !==
							Date.prototype.toJSON.call({
								toISOString: function () {
									return 1;
								},
							})
					);
				}),
			},
			{
				toJSON: function (t) {
					var e = s(this),
						n = o(e);
					return "number" != typeof n || isFinite(n) ? e.toISOString() : null;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(290);
		i(
			{ target: "Date", proto: !0, forced: Date.prototype.toISOString !== r },
			{ toISOString: r }
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(1),
			r = Date.prototype,
			s = r.getTime,
			o = r.toISOString,
			a = function (t) {
				return t > 9 ? t : "0" + t;
			};
		t.exports =
			i(function () {
				return "0385-07-25T07:06:39.999Z" != o.call(new Date(-5e13 - 1));
			}) ||
			!i(function () {
				o.call(new Date(NaN));
			})
				? function () {
						if (!isFinite(s.call(this))) throw RangeError("Invalid time value");
						var t = this.getUTCFullYear(),
							e = this.getUTCMilliseconds(),
							n = t < 0 ? "-" : t > 9999 ? "+" : "";
						return (
							n +
							("00000" + Math.abs(t)).slice(n ? -6 : -4) +
							"-" +
							a(this.getUTCMonth() + 1) +
							"-" +
							a(this.getUTCDate()) +
							"T" +
							a(this.getUTCHours()) +
							":" +
							a(this.getUTCMinutes()) +
							":" +
							a(this.getUTCSeconds()) +
							"." +
							(e > 99 ? e : "0" + a(e)) +
							"Z"
						);
				  }
				: o;
	},
	function (t, e, n) {
		var i = n(16),
			r = Date.prototype,
			s = r.toString,
			o = r.getTime;
		new Date(NaN) + "" != "Invalid Date" &&
			i(r, "toString", function () {
				var t = o.call(this);
				return t == t ? s.call(this) : "Invalid Date";
			});
	},
	function (t, e, n) {
		var i = n(14),
			r = n(293),
			s = n(7)("toPrimitive"),
			o = Date.prototype;
		s in o || i(o, s, r);
	},
	function (t, e, n) {
		"use strict";
		var i = n(4),
			r = n(27);
		t.exports = function (t) {
			if ("string" !== t && "number" !== t && "default" !== t)
				throw TypeError("Incorrect hint");
			return r(i(this), "number" !== t);
		};
	},
	function (t, e, n) {
		var i = n(2);
		n(29)(i.JSON, "JSON", !0);
	},
	function (t, e, n) {
		var i,
			r,
			s,
			o,
			a,
			l,
			c,
			u = n(2),
			h = n(15).f,
			d = n(28),
			f = n(132).set,
			p = n(99),
			g = u.MutationObserver || u.WebKitMutationObserver,
			v = u.process,
			_ = u.Promise,
			m = "process" == d(v),
			y = h(u, "queueMicrotask"),
			b = y && y.value;
		b ||
			((i = function () {
				var t, e;
				for (m && (t = v.domain) && t.exit(); r; ) {
					(e = r.fn), (r = r.next);
					try {
						e();
					} catch (t) {
						throw (r ? o() : (s = void 0), t);
					}
				}
				(s = void 0), t && t.enter();
			}),
			m
				? (o = function () {
						v.nextTick(i);
				  })
				: g && !/(iphone|ipod|ipad).*applewebkit/i.test(p)
				? ((a = !0),
				  (l = document.createTextNode("")),
				  new g(i).observe(l, { characterData: !0 }),
				  (o = function () {
						l.data = a = !a;
				  }))
				: _ && _.resolve
				? ((c = _.resolve(void 0)),
				  (o = function () {
						c.then(i);
				  }))
				: (o = function () {
						f.call(u, i);
				  })),
			(t.exports =
				b ||
				function (t) {
					var e = { fn: t, next: void 0 };
					s && (s.next = e), r || ((r = e), o()), (s = e);
				});
	},
	function (t, e, n) {
		var i = n(2);
		t.exports = function (t, e) {
			var n = i.console;
			n && n.error && (1 === arguments.length ? n.error(t) : n.error(t, e));
		};
	},
	function (t, e) {
		t.exports = function (t) {
			try {
				return { error: !1, value: t() };
			} catch (t) {
				return { error: !0, value: t };
			}
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(92),
			s = n(32),
			o = n(133);
		i(
			{ target: "Promise", proto: !0, real: !0 },
			{
				finally: function (t) {
					var e = s(this, r("Promise")),
						n = "function" == typeof t;
					return this.then(
						n
							? function (n) {
									return o(e, t()).then(function () {
										return n;
									});
							  }
							: t,
						n
							? function (n) {
									return o(e, t()).then(function () {
										throw n;
									});
							  }
							: t
					);
				},
			}
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(81),
			r = n(135);
		t.exports = i(
			"Map",
			function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			r,
			!0
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(81),
			r = n(135);
		t.exports = i(
			"Set",
			function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			r
		);
	},
	function (t, e, n) {
		"use strict";
		var i,
			r = n(2),
			s = n(51),
			o = n(44),
			a = n(81),
			l = n(136),
			c = n(3),
			u = n(20).enforce,
			h = n(106),
			d = !r.ActiveXObject && "ActiveXObject" in r,
			f = Object.isExtensible,
			p = function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			g = (t.exports = a("WeakMap", p, l, !0, !0));
		if (h && d) {
			(i = l.getConstructor(p, "WeakMap", !0)), (o.REQUIRED = !0);
			var v = g.prototype,
				_ = v.delete,
				m = v.has,
				y = v.get,
				b = v.set;
			s(v, {
				delete: function (t) {
					if (c(t) && !f(t)) {
						var e = u(this);
						return (
							e.frozen || (e.frozen = new i()),
							_.call(this, t) || e.frozen.delete(t)
						);
					}
					return _.call(this, t);
				},
				has: function (t) {
					if (c(t) && !f(t)) {
						var e = u(this);
						return (
							e.frozen || (e.frozen = new i()),
							m.call(this, t) || e.frozen.has(t)
						);
					}
					return m.call(this, t);
				},
				get: function (t) {
					if (c(t) && !f(t)) {
						var e = u(this);
						return (
							e.frozen || (e.frozen = new i()),
							m.call(this, t) ? y.call(this, t) : e.frozen.get(t)
						);
					}
					return y.call(this, t);
				},
				set: function (t, e) {
					if (c(t) && !f(t)) {
						var n = u(this);
						n.frozen || (n.frozen = new i()),
							m.call(this, t) ? b.call(this, t, e) : n.frozen.set(t, e);
					} else b.call(this, t, e);
					return this;
				},
			});
		}
	},
	function (t, e, n) {
		"use strict";
		n(81)(
			"WeakSet",
			function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			n(136),
			!1,
			!0
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(2),
			s = n(82),
			o = n(49),
			a = s.ArrayBuffer;
		i({ global: !0, forced: r.ArrayBuffer !== a }, { ArrayBuffer: a }),
			o("ArrayBuffer");
	},
	function (t, e, n) {
		var i = n(0),
			r = n(5);
		i(
			{ target: "ArrayBuffer", stat: !0, forced: !r.NATIVE_ARRAY_BUFFER_VIEWS },
			{ isView: r.isView }
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(0),
			r = n(1),
			s = n(82),
			o = n(4),
			a = n(35),
			l = n(8),
			c = n(32),
			u = s.ArrayBuffer,
			h = s.DataView,
			d = u.prototype.slice;
		i(
			{
				target: "ArrayBuffer",
				proto: !0,
				unsafe: !0,
				forced: r(function () {
					return !new u(2).slice(1, void 0).byteLength;
				}),
			},
			{
				slice: function (t, e) {
					if (void 0 !== d && void 0 === e) return d.call(o(this), t);
					for (
						var n = o(this).byteLength,
							i = a(t, n),
							r = a(void 0 === e ? n : e, n),
							s = new (c(this, u))(l(r - i)),
							f = new h(this),
							p = new h(s),
							g = 0;
						i < r;

					)
						p.setUint8(g++, f.getUint8(i++));
					return s;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(82);
		i(
			{ global: !0, forced: !n(5).NATIVE_ARRAY_BUFFER },
			{ DataView: r.DataView }
		);
	},
	function (t, e, n) {
		n(33)("Int8", 1, function (t) {
			return function (e, n, i) {
				return t(this, e, n, i);
			};
		});
	},
	function (t, e, n) {
		n(33)("Uint8", 1, function (t) {
			return function (e, n, i) {
				return t(this, e, n, i);
			};
		});
	},
	function (t, e, n) {
		n(33)(
			"Uint8",
			1,
			function (t) {
				return function (e, n, i) {
					return t(this, e, n, i);
				};
			},
			!0
		);
	},
	function (t, e, n) {
		n(33)("Int16", 2, function (t) {
			return function (e, n, i) {
				return t(this, e, n, i);
			};
		});
	},
	function (t, e, n) {
		n(33)("Uint16", 2, function (t) {
			return function (e, n, i) {
				return t(this, e, n, i);
			};
		});
	},
	function (t, e, n) {
		n(33)("Int32", 4, function (t) {
			return function (e, n, i) {
				return t(this, e, n, i);
			};
		});
	},
	function (t, e, n) {
		n(33)("Uint32", 4, function (t) {
			return function (e, n, i) {
				return t(this, e, n, i);
			};
		});
	},
	function (t, e, n) {
		n(33)("Float32", 4, function (t) {
			return function (e, n, i) {
				return t(this, e, n, i);
			};
		});
	},
	function (t, e, n) {
		n(33)("Float64", 8, function (t) {
			return function (e, n, i) {
				return t(this, e, n, i);
			};
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(103),
			r = n(5),
			s = n(139);
		r.exportStatic("from", s, i);
	},
	function (t, e, n) {
		"use strict";
		var i = n(103),
			r = n(5),
			s = r.aTypedArrayConstructor;
		r.exportStatic(
			"of",
			function () {
				for (var t = 0, e = arguments.length, n = new (s(this))(e); e > t; )
					n[t] = arguments[t++];
				return n;
			},
			i
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(120),
			r = n(5),
			s = r.aTypedArray;
		r.exportProto("copyWithin", function (t, e) {
			return i.call(
				s(this),
				t,
				e,
				arguments.length > 2 ? arguments[2] : void 0
			);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(13),
			r = n(5),
			s = i(4),
			o = r.aTypedArray;
		r.exportProto("every", function (t) {
			return s(o(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(5),
			r = n(91),
			s = i.aTypedArray;
		i.exportProto("fill", function (t) {
			return r.apply(s(this), arguments);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(32),
			r = n(5),
			s = n(13)(2),
			o = r.aTypedArray,
			a = r.aTypedArrayConstructor;
		r.exportProto("filter", function (t) {
			for (
				var e = s(o(this), t, arguments.length > 1 ? arguments[1] : void 0),
					n = i(this, this.constructor),
					r = 0,
					l = e.length,
					c = new (a(n))(l);
				l > r;

			)
				c[r] = e[r++];
			return c;
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(5),
			r = n(13)(5),
			s = i.aTypedArray;
		i.exportProto("find", function (t) {
			return r(s(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(5),
			r = n(13)(6),
			s = i.aTypedArray;
		i.exportProto("findIndex", function (t) {
			return r(s(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(5),
			r = n(13)(0),
			s = i.aTypedArray;
		i.exportProto("forEach", function (t) {
			r(s(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(57),
			r = n(5),
			s = r.aTypedArray,
			o = i(!0);
		r.exportProto("includes", function (t) {
			return o(s(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(57),
			r = n(5),
			s = r.aTypedArray,
			o = i(!1);
		r.exportProto("indexOf", function (t) {
			return o(s(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(2),
			r = n(25),
			s = n(5),
			o = n(7)("iterator"),
			a = i.Uint8Array,
			l = r.values,
			c = r.keys,
			u = r.entries,
			h = s.aTypedArray,
			d = s.exportProto,
			f = a && a.prototype[o],
			p = !!f && ("values" == f.name || null == f.name),
			g = function () {
				return l.call(h(this));
			};
		d("entries", function () {
			return u.call(h(this));
		}),
			d("keys", function () {
				return c.call(h(this));
			}),
			d("values", g, !p),
			d(o, g, !p);
	},
	function (t, e, n) {
		"use strict";
		var i = n(5),
			r = i.aTypedArray,
			s = [].join;
		i.exportProto("join", function (t) {
			return s.apply(r(this), arguments);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(122),
			r = n(5),
			s = r.aTypedArray;
		r.exportProto("lastIndexOf", function (t) {
			return i.apply(s(this), arguments);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(32),
			r = n(5),
			s = n(13),
			o = r.aTypedArray,
			a = r.aTypedArrayConstructor,
			l = s(1, function (t, e) {
				return new (a(i(t, t.constructor)))(e);
			});
		r.exportProto("map", function (t) {
			return l(o(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(5),
			r = n(73),
			s = i.aTypedArray;
		i.exportProto("reduce", function (t) {
			return r(s(this), t, arguments.length, arguments[1], !1);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(5),
			r = n(73),
			s = i.aTypedArray;
		i.exportProto("reduceRight", function (t) {
			return r(s(this), t, arguments.length, arguments[1], !0);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(5),
			r = i.aTypedArray;
		i.exportProto("reverse", function () {
			for (var t, e = r(this).length, n = Math.floor(e / 2), i = 0; i < n; )
				(t = this[i]), (this[i++] = this[--e]), (this[e] = t);
			return this;
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(8),
			r = n(138),
			s = n(10),
			o = n(5),
			a = n(1),
			l = o.aTypedArray,
			c = a(function () {
				new Int8Array(1).set({});
			});
		o.exportProto(
			"set",
			function (t) {
				l(this);
				var e = r(arguments[1], 1),
					n = this.length,
					o = s(t),
					a = i(o.length),
					c = 0;
				if (a + e > n) throw RangeError("Wrong length");
				for (; c < a; ) this[e + c] = o[c++];
			},
			c
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(32),
			r = n(5),
			s = n(1),
			o = r.aTypedArray,
			a = r.aTypedArrayConstructor,
			l = [].slice,
			c = s(function () {
				new Int8Array(1).slice();
			});
		r.exportProto(
			"slice",
			function (t, e) {
				for (
					var n = l.call(o(this), t, e),
						r = i(this, this.constructor),
						s = 0,
						c = n.length,
						u = new (a(r))(c);
					c > s;

				)
					u[s] = n[s++];
				return u;
			},
			c
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(5),
			r = n(13)(3),
			s = i.aTypedArray;
		i.exportProto("some", function (t) {
			return r(s(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(5),
			r = i.aTypedArray,
			s = [].sort;
		i.exportProto("sort", function (t) {
			return s.call(r(this), t);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(8),
			r = n(35),
			s = n(32),
			o = n(5),
			a = o.aTypedArray;
		o.exportProto("subarray", function (t, e) {
			var n = a(this),
				o = n.length,
				l = r(t, o);
			return new (s(n, n.constructor))(
				n.buffer,
				n.byteOffset + l * n.BYTES_PER_ELEMENT,
				i((void 0 === e ? o : r(e, o)) - l)
			);
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(2),
			r = n(1),
			s = n(5),
			o = i.Int8Array,
			a = s.aTypedArray,
			l = [].toLocaleString,
			c = [].slice,
			u =
				!!o &&
				r(function () {
					l.call(new o(1));
				}),
			h =
				r(function () {
					return [1, 2].toLocaleString() != new o([1, 2]).toLocaleString();
				}) ||
				!r(function () {
					o.prototype.toLocaleString.call([1, 2]);
				});
		s.exportProto(
			"toLocaleString",
			function () {
				return l.apply(u ? c.call(a(this)) : a(this), arguments);
			},
			h
		);
	},
	function (t, e, n) {
		"use strict";
		var i = n(2),
			r = n(5),
			s = n(1),
			o = i.Uint8Array,
			a = o && o.prototype,
			l = [].toString,
			c = [].join;
		s(function () {
			l.call({});
		}) &&
			(l = function () {
				return c.call(this);
			}),
			r.exportProto("toString", l, (a || {}).toString != l);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(2),
			s = n(21),
			o = n(4),
			a = n(1),
			l = (r.Reflect || {}).apply,
			c = Function.apply;
		i(
			{
				target: "Reflect",
				stat: !0,
				forced: !a(function () {
					l(function () {});
				}),
			},
			{
				apply: function (t, e, n) {
					return s(t), o(n), l ? l(t, e, n) : c.call(t, e, n);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(2),
			s = n(36),
			o = n(21),
			a = n(4),
			l = n(3),
			c = n(1),
			u = n(118),
			h = (r.Reflect || {}).construct,
			d = c(function () {
				function t() {}
				return !(h(function () {}, [], t) instanceof t);
			}),
			f = !c(function () {
				h(function () {});
			}),
			p = d || f;
		i(
			{ target: "Reflect", stat: !0, forced: p, sham: p },
			{
				construct: function (t, e) {
					o(t), a(e);
					var n = arguments.length < 3 ? t : o(arguments[2]);
					if (f && !d) return h(t, e, n);
					if (t == n) {
						switch (e.length) {
							case 0:
								return new t();
							case 1:
								return new t(e[0]);
							case 2:
								return new t(e[0], e[1]);
							case 3:
								return new t(e[0], e[1], e[2]);
							case 4:
								return new t(e[0], e[1], e[2], e[3]);
						}
						var i = [null];
						return i.push.apply(i, e), new (u.apply(t, i))();
					}
					var r = n.prototype,
						c = s(l(r) ? r : Object.prototype),
						p = Function.apply.call(t, c, e);
					return l(p) ? p : c;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(1),
			s = n(9),
			o = n(4),
			a = n(27),
			l = n(6);
		i(
			{
				target: "Reflect",
				stat: !0,
				forced: r(function () {
					Reflect.defineProperty(s.f({}, 1, { value: 1 }), 1, { value: 2 });
				}),
				sham: !l,
			},
			{
				defineProperty: function (t, e, n) {
					o(t), (e = a(e, !0)), o(n);
					try {
						return s.f(t, e, n), !0;
					} catch (t) {
						return !1;
					}
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(15).f,
			s = n(4);
		i(
			{ target: "Reflect", stat: !0 },
			{
				deleteProperty: function (t, e) {
					var n = r(s(t), e);
					return !(n && !n.configurable) && delete t[e];
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(15),
			s = n(30),
			o = n(12),
			a = n(3),
			l = n(4);
		i(
			{ target: "Reflect", stat: !0 },
			{
				get: function t(e, n) {
					var i,
						c,
						u = arguments.length < 3 ? e : arguments[2];
					return l(e) === u
						? e[n]
						: (i = r.f(e, n))
						? o(i, "value")
							? i.value
							: void 0 === i.get
							? void 0
							: i.get.call(u)
						: a((c = s(e)))
						? t(c, n, u)
						: void 0;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(15),
			s = n(4);
		i(
			{ target: "Reflect", stat: !0, sham: !n(6) },
			{
				getOwnPropertyDescriptor: function (t, e) {
					return r.f(s(t), e);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(30),
			s = n(4);
		i(
			{ target: "Reflect", stat: !0, sham: !n(90) },
			{
				getPrototypeOf: function (t) {
					return r(s(t));
				},
			}
		);
	},
	function (t, e, n) {
		n(0)(
			{ target: "Reflect", stat: !0 },
			{
				has: function (t, e) {
					return e in t;
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(4),
			s = Object.isExtensible;
		i(
			{ target: "Reflect", stat: !0 },
			{
				isExtensible: function (t) {
					return r(t), !s || s(t);
				},
			}
		);
	},
	function (t, e, n) {
		n(0)({ target: "Reflect", stat: !0 }, { ownKeys: n(86) });
	},
	function (t, e, n) {
		var i = n(0),
			r = n(92),
			s = n(4);
		i(
			{ target: "Reflect", stat: !0, sham: !n(59) },
			{
				preventExtensions: function (t) {
					s(t);
					try {
						var e = r("Object", "preventExtensions");
						return e && e(t), !0;
					} catch (t) {
						return !1;
					}
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(9),
			s = n(15),
			o = n(30),
			a = n(12),
			l = n(41),
			c = n(4),
			u = n(3);
		i(
			{ target: "Reflect", stat: !0 },
			{
				set: function t(e, n, i) {
					var h,
						d,
						f = arguments.length < 4 ? e : arguments[3],
						p = s.f(c(e), n);
					if (!p) {
						if (u((d = o(e)))) return t(d, n, i, f);
						p = l(0);
					}
					if (a(p, "value")) {
						if (!1 === p.writable || !u(f)) return !1;
						if ((h = s.f(f, n))) {
							if (h.get || h.set || !1 === h.writable) return !1;
							(h.value = i), r.f(f, n, h);
						} else r.f(f, n, l(0, i));
						return !0;
					}
					return void 0 !== p.set && (p.set.call(f, i), !0);
				},
			}
		);
	},
	function (t, e, n) {
		var i = n(0),
			r = n(48),
			s = n(117);
		r &&
			i(
				{ target: "Reflect", stat: !0 },
				{
					setPrototypeOf: function (t, e) {
						s(t, e);
						try {
							return r(t, e), !0;
						} catch (t) {
							return !1;
						}
					},
				}
			);
	},
	function (t, e, n) {
		var i = (function (t) {
			"use strict";
			var e,
				n = Object.prototype,
				i = n.hasOwnProperty,
				r = "function" == typeof Symbol ? Symbol : {},
				s = r.iterator || "@@iterator",
				o = r.asyncIterator || "@@asyncIterator",
				a = r.toStringTag || "@@toStringTag";
			function l(t, e, n, i) {
				var r = e && e.prototype instanceof g ? e : g,
					s = Object.create(r.prototype),
					o = new I(i || []);
				return (
					(s._invoke = (function (t, e, n) {
						var i = u;
						return function (r, s) {
							if (i === d) throw new Error("Generator is already running");
							if (i === f) {
								if ("throw" === r) throw s;
								return M();
							}
							for (n.method = r, n.arg = s; ; ) {
								var o = n.delegate;
								if (o) {
									var a = T(o, n);
									if (a) {
										if (a === p) continue;
										return a;
									}
								}
								if ("next" === n.method) n.sent = n._sent = n.arg;
								else if ("throw" === n.method) {
									if (i === u) throw ((i = f), n.arg);
									n.dispatchException(n.arg);
								} else "return" === n.method && n.abrupt("return", n.arg);
								i = d;
								var l = c(t, e, n);
								if ("normal" === l.type) {
									if (((i = n.done ? f : h), l.arg === p)) continue;
									return { value: l.arg, done: n.done };
								}
								"throw" === l.type &&
									((i = f), (n.method = "throw"), (n.arg = l.arg));
							}
						};
					})(t, n, o)),
					s
				);
			}
			function c(t, e, n) {
				try {
					return { type: "normal", arg: t.call(e, n) };
				} catch (t) {
					return { type: "throw", arg: t };
				}
			}
			t.wrap = l;
			var u = "suspendedStart",
				h = "suspendedYield",
				d = "executing",
				f = "completed",
				p = {};
			function g() {}
			function v() {}
			function _() {}
			var m = {};
			m[s] = function () {
				return this;
			};
			var y = Object.getPrototypeOf,
				b = y && y(y(C([])));
			b && b !== n && i.call(b, s) && (m = b);
			var w = (_.prototype = g.prototype = Object.create(m));
			function S(t) {
				["next", "throw", "return"].forEach(function (e) {
					t[e] = function (t) {
						return this._invoke(e, t);
					};
				});
			}
			function E(t) {
				var e;
				this._invoke = function (n, r) {
					function s() {
						return new Promise(function (e, s) {
							!(function e(n, r, s, o) {
								var a = c(t[n], t, r);
								if ("throw" !== a.type) {
									var l = a.arg,
										u = l.value;
									return u && "object" == typeof u && i.call(u, "__await")
										? Promise.resolve(u.__await).then(
												function (t) {
													e("next", t, s, o);
												},
												function (t) {
													e("throw", t, s, o);
												}
										  )
										: Promise.resolve(u).then(
												function (t) {
													(l.value = t), s(l);
												},
												function (t) {
													return e("throw", t, s, o);
												}
										  );
								}
								o(a.arg);
							})(n, r, e, s);
						});
					}
					return (e = e ? e.then(s, s) : s());
				};
			}
			function T(t, n) {
				var i = t.iterator[n.method];
				if (i === e) {
					if (((n.delegate = null), "throw" === n.method)) {
						if (
							t.iterator.return &&
							((n.method = "return"),
							(n.arg = e),
							T(t, n),
							"throw" === n.method)
						)
							return p;
						(n.method = "throw"),
							(n.arg = new TypeError(
								"The iterator does not provide a 'throw' method"
							));
					}
					return p;
				}
				var r = c(i, t.iterator, n.arg);
				if ("throw" === r.type)
					return (n.method = "throw"), (n.arg = r.arg), (n.delegate = null), p;
				var s = r.arg;
				return s
					? s.done
						? ((n[t.resultName] = s.value),
						  (n.next = t.nextLoc),
						  "return" !== n.method && ((n.method = "next"), (n.arg = e)),
						  (n.delegate = null),
						  p)
						: s
					: ((n.method = "throw"),
					  (n.arg = new TypeError("iterator result is not an object")),
					  (n.delegate = null),
					  p);
			}
			function L(t) {
				var e = { tryLoc: t[0] };
				1 in t && (e.catchLoc = t[1]),
					2 in t && ((e.finallyLoc = t[2]), (e.afterLoc = t[3])),
					this.tryEntries.push(e);
			}
			function A(t) {
				var e = t.completion || {};
				(e.type = "normal"), delete e.arg, (t.completion = e);
			}
			function I(t) {
				(this.tryEntries = [{ tryLoc: "root" }]),
					t.forEach(L, this),
					this.reset(!0);
			}
			function C(t) {
				if (t) {
					var n = t[s];
					if (n) return n.call(t);
					if ("function" == typeof t.next) return t;
					if (!isNaN(t.length)) {
						var r = -1,
							o = function n() {
								for (; ++r < t.length; )
									if (i.call(t, r)) return (n.value = t[r]), (n.done = !1), n;
								return (n.value = e), (n.done = !0), n;
							};
						return (o.next = o);
					}
				}
				return { next: M };
			}
			function M() {
				return { value: e, done: !0 };
			}
			return (
				(v.prototype = w.constructor = _),
				(_.constructor = v),
				(_[a] = v.displayName = "GeneratorFunction"),
				(t.isGeneratorFunction = function (t) {
					var e = "function" == typeof t && t.constructor;
					return (
						!!e &&
						(e === v || "GeneratorFunction" === (e.displayName || e.name))
					);
				}),
				(t.mark = function (t) {
					return (
						Object.setPrototypeOf
							? Object.setPrototypeOf(t, _)
							: ((t.__proto__ = _), a in t || (t[a] = "GeneratorFunction")),
						(t.prototype = Object.create(w)),
						t
					);
				}),
				(t.awrap = function (t) {
					return { __await: t };
				}),
				S(E.prototype),
				(E.prototype[o] = function () {
					return this;
				}),
				(t.AsyncIterator = E),
				(t.async = function (e, n, i, r) {
					var s = new E(l(e, n, i, r));
					return t.isGeneratorFunction(n)
						? s
						: s.next().then(function (t) {
								return t.done ? t.value : s.next();
						  });
				}),
				S(w),
				(w[a] = "Generator"),
				(w[s] = function () {
					return this;
				}),
				(w.toString = function () {
					return "[object Generator]";
				}),
				(t.keys = function (t) {
					var e = [];
					for (var n in t) e.push(n);
					return (
						e.reverse(),
						function n() {
							for (; e.length; ) {
								var i = e.pop();
								if (i in t) return (n.value = i), (n.done = !1), n;
							}
							return (n.done = !0), n;
						}
					);
				}),
				(t.values = C),
				(I.prototype = {
					constructor: I,
					reset: function (t) {
						if (
							((this.prev = 0),
							(this.next = 0),
							(this.sent = this._sent = e),
							(this.done = !1),
							(this.delegate = null),
							(this.method = "next"),
							(this.arg = e),
							this.tryEntries.forEach(A),
							!t)
						)
							for (var n in this)
								"t" === n.charAt(0) &&
									i.call(this, n) &&
									!isNaN(+n.slice(1)) &&
									(this[n] = e);
					},
					stop: function () {
						this.done = !0;
						var t = this.tryEntries[0].completion;
						if ("throw" === t.type) throw t.arg;
						return this.rval;
					},
					dispatchException: function (t) {
						if (this.done) throw t;
						var n = this;
						function r(i, r) {
							return (
								(a.type = "throw"),
								(a.arg = t),
								(n.next = i),
								r && ((n.method = "next"), (n.arg = e)),
								!!r
							);
						}
						for (var s = this.tryEntries.length - 1; s >= 0; --s) {
							var o = this.tryEntries[s],
								a = o.completion;
							if ("root" === o.tryLoc) return r("end");
							if (o.tryLoc <= this.prev) {
								var l = i.call(o, "catchLoc"),
									c = i.call(o, "finallyLoc");
								if (l && c) {
									if (this.prev < o.catchLoc) return r(o.catchLoc, !0);
									if (this.prev < o.finallyLoc) return r(o.finallyLoc);
								} else if (l) {
									if (this.prev < o.catchLoc) return r(o.catchLoc, !0);
								} else {
									if (!c)
										throw new Error("try statement without catch or finally");
									if (this.prev < o.finallyLoc) return r(o.finallyLoc);
								}
							}
						}
					},
					abrupt: function (t, e) {
						for (var n = this.tryEntries.length - 1; n >= 0; --n) {
							var r = this.tryEntries[n];
							if (
								r.tryLoc <= this.prev &&
								i.call(r, "finallyLoc") &&
								this.prev < r.finallyLoc
							) {
								var s = r;
								break;
							}
						}
						s &&
							("break" === t || "continue" === t) &&
							s.tryLoc <= e &&
							e <= s.finallyLoc &&
							(s = null);
						var o = s ? s.completion : {};
						return (
							(o.type = t),
							(o.arg = e),
							s
								? ((this.method = "next"), (this.next = s.finallyLoc), p)
								: this.complete(o)
						);
					},
					complete: function (t, e) {
						if ("throw" === t.type) throw t.arg;
						return (
							"break" === t.type || "continue" === t.type
								? (this.next = t.arg)
								: "return" === t.type
								? ((this.rval = this.arg = t.arg),
								  (this.method = "return"),
								  (this.next = "end"))
								: "normal" === t.type && e && (this.next = e),
							p
						);
					},
					finish: function (t) {
						for (var e = this.tryEntries.length - 1; e >= 0; --e) {
							var n = this.tryEntries[e];
							if (n.finallyLoc === t)
								return this.complete(n.completion, n.afterLoc), A(n), p;
						}
					},
					catch: function (t) {
						for (var e = this.tryEntries.length - 1; e >= 0; --e) {
							var n = this.tryEntries[e];
							if (n.tryLoc === t) {
								var i = n.completion;
								if ("throw" === i.type) {
									var r = i.arg;
									A(n);
								}
								return r;
							}
						}
						throw new Error("illegal catch attempt");
					},
					delegateYield: function (t, n, i) {
						return (
							(this.delegate = { iterator: C(t), resultName: n, nextLoc: i }),
							"next" === this.method && (this.arg = e),
							p
						);
					},
				}),
				t
			);
		})(t.exports);
		try {
			regeneratorRuntime = i;
		} catch (t) {
			Function("r", "regeneratorRuntime = r")(i);
		}
	},
	function (t, e) {
		t.exports = {
			CSSRuleList: 0,
			CSSStyleDeclaration: 0,
			CSSValueList: 0,
			ClientRectList: 0,
			DOMRectList: 0,
			DOMStringList: 0,
			DOMTokenList: 1,
			DataTransferItemList: 0,
			FileList: 0,
			HTMLAllCollection: 0,
			HTMLCollection: 0,
			HTMLFormElement: 0,
			HTMLSelectElement: 0,
			MediaList: 0,
			MimeTypeArray: 0,
			NamedNodeMap: 0,
			NodeList: 1,
			PaintRequestList: 0,
			Plugin: 0,
			PluginArray: 0,
			SVGLengthList: 0,
			SVGNumberList: 0,
			SVGPathSegList: 0,
			SVGPointList: 0,
			SVGStringList: 0,
			SVGTransformList: 0,
			SourceBufferList: 0,
			StyleSheetList: 0,
			TextTrackCueList: 0,
			TextTrackList: 0,
			TouchList: 0,
		};
	},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {
		"use strict";
		n(126);
		var i,
			r = n(0),
			s = n(6),
			o = n(140),
			a = n(2),
			l = n(88),
			c = n(16),
			u = n(39),
			h = n(12),
			d = n(113),
			f = n(119),
			p = n(74),
			g = n(367),
			v = n(29),
			_ = n(368),
			m = n(20),
			y = a.URL,
			b = _.URLSearchParams,
			w = _.getState,
			S = m.set,
			E = m.getterFor("URL"),
			T = Math.pow,
			L = /[A-Za-z]/,
			A = /[\d+\-.A-Za-z]/,
			I = /\d/,
			C = /^(0x|0X)/,
			M = /^[0-7]+$/,
			x = /^\d+$/,
			D = /^[\dA-Fa-f]+$/,
			O = /[\u0000\u0009\u000A\u000D #%/:?@[\\]]/,
			R = /[\u0000\u0009\u000A\u000D #/:?@[\\]]/,
			N = /^[\u0000-\u001F ]+|[\u0000-\u001F ]+$/g,
			P = /[\u0009\u000A\u000D]/g,
			k = function (t, e) {
				var n, i, r;
				if ("[" == e.charAt(0)) {
					if ("]" != e.charAt(e.length - 1)) return "Invalid host";
					if (!(n = B(e.slice(1, -1)))) return "Invalid host";
					t.host = n;
				} else if (Y(t)) {
					if (((e = g(e)), O.test(e))) return "Invalid host";
					if (null === (n = V(e))) return "Invalid host";
					t.host = n;
				} else {
					if (R.test(e)) return "Invalid host";
					for (n = "", i = f(e), r = 0; r < i.length; r++) n += W(i[r], F);
					t.host = n;
				}
			},
			V = function (t) {
				var e,
					n,
					i,
					r,
					s,
					o,
					a,
					l = t.split(".");
				if (("" == l[l.length - 1] && l.length && l.pop(), (e = l.length) > 4))
					return t;
				for (n = [], i = 0; i < e; i++) {
					if ("" == (r = l[i])) return t;
					if (
						((s = 10),
						r.length > 1 &&
							"0" == r.charAt(0) &&
							((s = C.test(r) ? 16 : 8), (r = r.slice(8 == s ? 1 : 2))),
						"" === r)
					)
						o = 0;
					else {
						if (!(10 == s ? x : 8 == s ? M : D).test(r)) return t;
						o = parseInt(r, s);
					}
					n.push(o);
				}
				for (i = 0; i < e; i++)
					if (((o = n[i]), i == e - 1)) {
						if (o >= T(256, 5 - e)) return null;
					} else if (o > 255) return null;
				for (a = n.pop(), i = 0; i < n.length; i++) a += n[i] * T(256, 3 - i);
				return a;
			},
			B = function (t) {
				var e,
					n,
					i,
					r,
					s,
					o,
					a,
					l = [0, 0, 0, 0, 0, 0, 0, 0],
					c = 0,
					u = null,
					h = 0,
					d = function () {
						return t.charAt(h);
					};
				if (":" == d()) {
					if (":" != t.charAt(1)) return;
					(h += 2), (u = ++c);
				}
				for (; d(); ) {
					if (8 == c) return;
					if (":" != d()) {
						for (e = n = 0; n < 4 && D.test(d()); )
							(e = 16 * e + parseInt(d(), 16)), h++, n++;
						if ("." == d()) {
							if (0 == n) return;
							if (((h -= n), c > 6)) return;
							for (i = 0; d(); ) {
								if (((r = null), i > 0)) {
									if (!("." == d() && i < 4)) return;
									h++;
								}
								if (!I.test(d())) return;
								for (; I.test(d()); ) {
									if (((s = parseInt(d(), 10)), null === r)) r = s;
									else {
										if (0 == r) return;
										r = 10 * r + s;
									}
									if (r > 255) return;
									h++;
								}
								(l[c] = 256 * l[c] + r), (2 != ++i && 4 != i) || c++;
							}
							if (4 != i) return;
							break;
						}
						if (":" == d()) {
							if ((h++, !d())) return;
						} else if (d()) return;
						l[c++] = e;
					} else {
						if (null !== u) return;
						h++, (u = ++c);
					}
				}
				if (null !== u)
					for (o = c - u, c = 7; 0 != c && o > 0; )
						(a = l[c]), (l[c--] = l[u + o - 1]), (l[u + --o] = a);
				else if (8 != c) return;
				return l;
			},
			U = function (t) {
				var e, n, i, r;
				if ("number" == typeof t) {
					for (e = [], n = 0; n < 4; n++)
						e.unshift(t % 256), (t = Math.floor(t / 256));
					return e.join(".");
				}
				if ("object" == typeof t) {
					for (
						e = "",
							i = (function (t) {
								for (var e = null, n = 1, i = null, r = 0, s = 0; s < 8; s++)
									0 !== t[s]
										? (r > n && ((e = i), (n = r)), (i = null), (r = 0))
										: (null === i && (i = s), ++r);
								return r > n && ((e = i), (n = r)), e;
							})(t),
							n = 0;
						n < 8;
						n++
					)
						(r && 0 === t[n]) ||
							(r && (r = !1),
							i === n
								? ((e += n ? ":" : "::"), (r = !0))
								: ((e += t[n].toString(16)), n < 7 && (e += ":")));
					return "[" + e + "]";
				}
				return t;
			},
			F = {},
			q = d({}, F, { " ": 1, '"': 1, "<": 1, ">": 1, "`": 1 }),
			H = d({}, q, { "#": 1, "?": 1, "{": 1, "}": 1 }),
			j = d({}, H, {
				"/": 1,
				":": 1,
				";": 1,
				"=": 1,
				"@": 1,
				"[": 1,
				"\\": 1,
				"]": 1,
				"^": 1,
				"|": 1,
			}),
			W = function (t, e) {
				var n = p(t, 0);
				return n > 32 && n < 127 && !h(e, t) ? t : encodeURIComponent(t);
			},
			G = {
				ftp: 21,
				file: null,
				gopher: 70,
				http: 80,
				https: 443,
				ws: 80,
				wss: 443,
			},
			Y = function (t) {
				return h(G, t.scheme);
			},
			K = function (t) {
				return "" != t.username || "" != t.password;
			},
			z = function (t) {
				return !t.host || t.cannotBeABaseURL || "file" == t.scheme;
			},
			X = function (t, e) {
				var n;
				return (
					2 == t.length &&
					L.test(t.charAt(0)) &&
					(":" == (n = t.charAt(1)) || (!e && "|" == n))
				);
			},
			$ = function (t) {
				var e;
				return (
					t.length > 1 &&
					X(t.slice(0, 2)) &&
					(2 == t.length ||
						"/" === (e = t.charAt(2)) ||
						"\\" === e ||
						"?" === e ||
						"#" === e)
				);
			},
			J = function (t) {
				var e = t.path,
					n = e.length;
				!n || ("file" == t.scheme && 1 == n && X(e[0], !0)) || e.pop();
			},
			Z = function (t) {
				return "." === t || "%2e" === t.toLowerCase();
			},
			Q = {},
			tt = {},
			et = {},
			nt = {},
			it = {},
			rt = {},
			st = {},
			ot = {},
			at = {},
			lt = {},
			ct = {},
			ut = {},
			ht = {},
			dt = {},
			ft = {},
			pt = {},
			gt = {},
			vt = {},
			_t = {},
			mt = {},
			yt = {},
			bt = function (t, e, n, r) {
				var s,
					o,
					a,
					l,
					c,
					u = n || Q,
					d = 0,
					p = "",
					g = !1,
					v = !1,
					_ = !1;
				for (
					n ||
						((t.scheme = ""),
						(t.username = ""),
						(t.password = ""),
						(t.host = null),
						(t.port = null),
						(t.path = []),
						(t.query = null),
						(t.fragment = null),
						(t.cannotBeABaseURL = !1),
						(e = e.replace(N, ""))),
						e = e.replace(P, ""),
						s = f(e);
					d <= s.length;

				) {
					switch (((o = s[d]), u)) {
						case Q:
							if (!o || !L.test(o)) {
								if (n) return "Invalid scheme";
								u = et;
								continue;
							}
							(p += o.toLowerCase()), (u = tt);
							break;
						case tt:
							if (o && (A.test(o) || "+" == o || "-" == o || "." == o))
								p += o.toLowerCase();
							else {
								if (":" != o) {
									if (n) return "Invalid scheme";
									(p = ""), (u = et), (d = 0);
									continue;
								}
								if (
									n &&
									(Y(t) != h(G, p) ||
										("file" == p && (K(t) || null !== t.port)) ||
										("file" == t.scheme && !t.host))
								)
									return;
								if (((t.scheme = p), n))
									return void (
										Y(t) &&
										G[t.scheme] == t.port &&
										(t.port = null)
									);
								(p = ""),
									"file" == t.scheme
										? (u = dt)
										: Y(t) && r && r.scheme == t.scheme
										? (u = nt)
										: Y(t)
										? (u = ot)
										: "/" == s[d + 1]
										? ((u = it), d++)
										: ((t.cannotBeABaseURL = !0), t.path.push(""), (u = _t));
							}
							break;
						case et:
							if (!r || (r.cannotBeABaseURL && "#" != o))
								return "Invalid scheme";
							if (r.cannotBeABaseURL && "#" == o) {
								(t.scheme = r.scheme),
									(t.path = r.path.slice()),
									(t.query = r.query),
									(t.fragment = ""),
									(t.cannotBeABaseURL = !0),
									(u = yt);
								break;
							}
							u = "file" == r.scheme ? dt : rt;
							continue;
						case nt:
							if ("/" != o || "/" != s[d + 1]) {
								u = rt;
								continue;
							}
							(u = at), d++;
							break;
						case it:
							if ("/" == o) {
								u = lt;
								break;
							}
							u = vt;
							continue;
						case rt:
							if (((t.scheme = r.scheme), o == i))
								(t.username = r.username),
									(t.password = r.password),
									(t.host = r.host),
									(t.port = r.port),
									(t.path = r.path.slice()),
									(t.query = r.query);
							else if ("/" == o || ("\\" == o && Y(t))) u = st;
							else if ("?" == o)
								(t.username = r.username),
									(t.password = r.password),
									(t.host = r.host),
									(t.port = r.port),
									(t.path = r.path.slice()),
									(t.query = ""),
									(u = mt);
							else {
								if ("#" != o) {
									(t.username = r.username),
										(t.password = r.password),
										(t.host = r.host),
										(t.port = r.port),
										(t.path = r.path.slice()),
										t.path.pop(),
										(u = vt);
									continue;
								}
								(t.username = r.username),
									(t.password = r.password),
									(t.host = r.host),
									(t.port = r.port),
									(t.path = r.path.slice()),
									(t.query = r.query),
									(t.fragment = ""),
									(u = yt);
							}
							break;
						case st:
							if (!Y(t) || ("/" != o && "\\" != o)) {
								if ("/" != o) {
									(t.username = r.username),
										(t.password = r.password),
										(t.host = r.host),
										(t.port = r.port),
										(u = vt);
									continue;
								}
								u = lt;
							} else u = at;
							break;
						case ot:
							if (((u = at), "/" != o || "/" != p.charAt(d + 1))) continue;
							d++;
							break;
						case at:
							if ("/" != o && "\\" != o) {
								u = lt;
								continue;
							}
							break;
						case lt:
							if ("@" == o) {
								g && (p = "%40" + p), (g = !0), (a = f(p));
								for (var m = 0; m < a.length; m++) {
									var y = a[m];
									if (":" != y || _) {
										var b = W(y, j);
										_ ? (t.password += b) : (t.username += b);
									} else _ = !0;
								}
								p = "";
							} else if (
								o == i ||
								"/" == o ||
								"?" == o ||
								"#" == o ||
								("\\" == o && Y(t))
							) {
								if (g && "" == p) return "Invalid authority";
								(d -= f(p).length + 1), (p = ""), (u = ct);
							} else p += o;
							break;
						case ct:
						case ut:
							if (n && "file" == t.scheme) {
								u = pt;
								continue;
							}
							if (":" != o || v) {
								if (
									o == i ||
									"/" == o ||
									"?" == o ||
									"#" == o ||
									("\\" == o && Y(t))
								) {
									if (Y(t) && "" == p) return "Invalid host";
									if (n && "" == p && (K(t) || null !== t.port)) return;
									if ((l = k(t, p))) return l;
									if (((p = ""), (u = gt), n)) return;
									continue;
								}
								"[" == o ? (v = !0) : "]" == o && (v = !1), (p += o);
							} else {
								if ("" == p) return "Invalid host";
								if ((l = k(t, p))) return l;
								if (((p = ""), (u = ht), n == ut)) return;
							}
							break;
						case ht:
							if (!I.test(o)) {
								if (
									o == i ||
									"/" == o ||
									"?" == o ||
									"#" == o ||
									("\\" == o && Y(t)) ||
									n
								) {
									if ("" != p) {
										var w = parseInt(p, 10);
										if (w > 65535) return "Invalid port";
										(t.port = Y(t) && w === G[t.scheme] ? null : w), (p = "");
									}
									if (n) return;
									u = gt;
									continue;
								}
								return "Invalid port";
							}
							p += o;
							break;
						case dt:
							if (((t.scheme = "file"), "/" == o || "\\" == o)) u = ft;
							else {
								if (!r || "file" != r.scheme) {
									u = vt;
									continue;
								}
								if (o == i)
									(t.host = r.host),
										(t.path = r.path.slice()),
										(t.query = r.query);
								else if ("?" == o)
									(t.host = r.host),
										(t.path = r.path.slice()),
										(t.query = ""),
										(u = mt);
								else {
									if ("#" != o) {
										$(s.slice(d).join("")) ||
											((t.host = r.host), (t.path = r.path.slice()), J(t)),
											(u = vt);
										continue;
									}
									(t.host = r.host),
										(t.path = r.path.slice()),
										(t.query = r.query),
										(t.fragment = ""),
										(u = yt);
								}
							}
							break;
						case ft:
							if ("/" == o || "\\" == o) {
								u = pt;
								break;
							}
							r &&
								"file" == r.scheme &&
								!$(s.slice(d).join("")) &&
								(X(r.path[0], !0) ? t.path.push(r.path[0]) : (t.host = r.host)),
								(u = vt);
							continue;
						case pt:
							if (o == i || "/" == o || "\\" == o || "?" == o || "#" == o) {
								if (!n && X(p)) u = vt;
								else if ("" == p) {
									if (((t.host = ""), n)) return;
									u = gt;
								} else {
									if ((l = k(t, p))) return l;
									if (("localhost" == t.host && (t.host = ""), n)) return;
									(p = ""), (u = gt);
								}
								continue;
							}
							p += o;
							break;
						case gt:
							if (Y(t)) {
								if (((u = vt), "/" != o && "\\" != o)) continue;
							} else if (n || "?" != o)
								if (n || "#" != o) {
									if (o != i && ((u = vt), "/" != o)) continue;
								} else (t.fragment = ""), (u = yt);
							else (t.query = ""), (u = mt);
							break;
						case vt:
							if (
								o == i ||
								"/" == o ||
								("\\" == o && Y(t)) ||
								(!n && ("?" == o || "#" == o))
							) {
								if (
									(".." === (c = (c = p).toLowerCase()) ||
									"%2e." === c ||
									".%2e" === c ||
									"%2e%2e" === c
										? (J(t), "/" == o || ("\\" == o && Y(t)) || t.path.push(""))
										: Z(p)
										? "/" == o || ("\\" == o && Y(t)) || t.path.push("")
										: ("file" == t.scheme &&
												!t.path.length &&
												X(p) &&
												(t.host && (t.host = ""), (p = p.charAt(0) + ":")),
										  t.path.push(p)),
									(p = ""),
									"file" == t.scheme && (o == i || "?" == o || "#" == o))
								)
									for (; t.path.length > 1 && "" === t.path[0]; )
										t.path.shift();
								"?" == o
									? ((t.query = ""), (u = mt))
									: "#" == o && ((t.fragment = ""), (u = yt));
							} else p += W(o, H);
							break;
						case _t:
							"?" == o
								? ((t.query = ""), (u = mt))
								: "#" == o
								? ((t.fragment = ""), (u = yt))
								: o != i && (t.path[0] += W(o, F));
							break;
						case mt:
							n || "#" != o
								? o != i &&
								  ("'" == o && Y(t)
										? (t.query += "%27")
										: (t.query += "#" == o ? "%23" : W(o, F)))
								: ((t.fragment = ""), (u = yt));
							break;
						case yt:
							o != i && (t.fragment += W(o, q));
					}
					d++;
				}
			},
			wt = function (t) {
				var e,
					n,
					i = u(this, wt, "URL"),
					r = arguments.length > 1 ? arguments[1] : void 0,
					o = String(t),
					a = S(i, { type: "URL" });
				if (void 0 !== r)
					if (r instanceof wt) e = E(r);
					else if ((n = bt((e = {}), String(r)))) throw TypeError(n);
				if ((n = bt(a, o, null, e))) throw TypeError(n);
				var l = (a.searchParams = new b()),
					c = w(l);
				c.updateSearchParams(a.query),
					(c.updateURL = function () {
						a.query = String(l) || null;
					}),
					s ||
						((i.href = Et.call(i)),
						(i.origin = Tt.call(i)),
						(i.protocol = Lt.call(i)),
						(i.username = At.call(i)),
						(i.password = It.call(i)),
						(i.host = Ct.call(i)),
						(i.hostname = Mt.call(i)),
						(i.port = xt.call(i)),
						(i.pathname = Dt.call(i)),
						(i.search = Ot.call(i)),
						(i.searchParams = Rt.call(i)),
						(i.hash = Nt.call(i)));
			},
			St = wt.prototype,
			Et = function () {
				var t = E(this),
					e = t.scheme,
					n = t.username,
					i = t.password,
					r = t.host,
					s = t.port,
					o = t.path,
					a = t.query,
					l = t.fragment,
					c = e + ":";
				return (
					null !== r
						? ((c += "//"),
						  K(t) && (c += n + (i ? ":" + i : "") + "@"),
						  (c += U(r)),
						  null !== s && (c += ":" + s))
						: "file" == e && (c += "//"),
					(c += t.cannotBeABaseURL ? o[0] : o.length ? "/" + o.join("/") : ""),
					null !== a && (c += "?" + a),
					null !== l && (c += "#" + l),
					c
				);
			},
			Tt = function () {
				var t = E(this),
					e = t.scheme,
					n = t.port;
				if ("blob" == e)
					try {
						return new URL(e.path[0]).origin;
					} catch (t) {
						return "null";
					}
				return "file" != e && Y(t)
					? e + "://" + U(t.host) + (null !== n ? ":" + n : "")
					: "null";
			},
			Lt = function () {
				return E(this).scheme + ":";
			},
			At = function () {
				return E(this).username;
			},
			It = function () {
				return E(this).password;
			},
			Ct = function () {
				var t = E(this),
					e = t.host,
					n = t.port;
				return null === e ? "" : null === n ? U(e) : U(e) + ":" + n;
			},
			Mt = function () {
				var t = E(this).host;
				return null === t ? "" : U(t);
			},
			xt = function () {
				var t = E(this).port;
				return null === t ? "" : String(t);
			},
			Dt = function () {
				var t = E(this),
					e = t.path;
				return t.cannotBeABaseURL ? e[0] : e.length ? "/" + e.join("/") : "";
			},
			Ot = function () {
				var t = E(this).query;
				return t ? "?" + t : "";
			},
			Rt = function () {
				return E(this).searchParams;
			},
			Nt = function () {
				var t = E(this).fragment;
				return t ? "#" + t : "";
			},
			Pt = function (t, e) {
				return { get: t, set: e, configurable: !0, enumerable: !0 };
			};
		if (
			(s &&
				l(St, {
					href: Pt(Et, function (t) {
						var e = E(this),
							n = String(t),
							i = bt(e, n);
						if (i) throw TypeError(i);
						w(e.searchParams).updateSearchParams(e.query);
					}),
					origin: Pt(Tt),
					protocol: Pt(Lt, function (t) {
						var e = E(this);
						bt(e, String(t) + ":", Q);
					}),
					username: Pt(At, function (t) {
						var e = E(this),
							n = f(String(t));
						if (!z(e)) {
							e.username = "";
							for (var i = 0; i < n.length; i++) e.username += W(n[i], j);
						}
					}),
					password: Pt(It, function (t) {
						var e = E(this),
							n = f(String(t));
						if (!z(e)) {
							e.password = "";
							for (var i = 0; i < n.length; i++) e.password += W(n[i], j);
						}
					}),
					host: Pt(Ct, function (t) {
						var e = E(this);
						e.cannotBeABaseURL || bt(e, String(t), ct);
					}),
					hostname: Pt(Mt, function (t) {
						var e = E(this);
						e.cannotBeABaseURL || bt(e, String(t), ut);
					}),
					port: Pt(xt, function (t) {
						var e = E(this);
						z(e) || ("" == (t = String(t)) ? (e.port = null) : bt(e, t, ht));
					}),
					pathname: Pt(Dt, function (t) {
						var e = E(this);
						e.cannotBeABaseURL || ((e.path = []), bt(e, t + "", gt));
					}),
					search: Pt(Ot, function (t) {
						var e = E(this);
						"" == (t = String(t))
							? (e.query = null)
							: ("?" == t.charAt(0) && (t = t.slice(1)),
							  (e.query = ""),
							  bt(e, t, mt)),
							w(e.searchParams).updateSearchParams(e.query);
					}),
					searchParams: Pt(Rt),
					hash: Pt(Nt, function (t) {
						var e = E(this);
						"" != (t = String(t))
							? ("#" == t.charAt(0) && (t = t.slice(1)),
							  (e.fragment = ""),
							  bt(e, t, yt))
							: (e.fragment = null);
					}),
				}),
			c(
				St,
				"toJSON",
				function () {
					return Et.call(this);
				},
				{ enumerable: !0 }
			),
			c(
				St,
				"toString",
				function () {
					return Et.call(this);
				},
				{ enumerable: !0 }
			),
			y)
		) {
			var kt = y.createObjectURL,
				Vt = y.revokeObjectURL;
			kt &&
				c(wt, "createObjectURL", function (t) {
					return kt.apply(y, arguments);
				}),
				Vt &&
					c(wt, "revokeObjectURL", function (t) {
						return Vt.apply(y, arguments);
					});
		}
		v(wt, "URL"), r({ global: !0, forced: !o, sham: !s }, { URL: wt });
	},
	function (t, e, n) {
		"use strict";
		var i = /[^\0-\u007E]/,
			r = /[.\u3002\uFF0E\uFF61]/g,
			s = "Overflow: input needs wider integers to process",
			o = Math.floor,
			a = String.fromCharCode,
			l = function (t) {
				return t + 22 + 75 * (t < 26);
			},
			c = function (t, e, n) {
				var i = 0;
				for (t = n ? o(t / 700) : t >> 1, t += o(t / e); t > 455; i += 36)
					t = o(t / 35);
				return o(i + (36 * t) / (t + 38));
			},
			u = function (t) {
				var e,
					n,
					i = [],
					r = (t = (function (t) {
						for (var e = [], n = 0, i = t.length; n < i; ) {
							var r = t.charCodeAt(n++);
							if (r >= 55296 && r <= 56319 && n < i) {
								var s = t.charCodeAt(n++);
								56320 == (64512 & s)
									? e.push(((1023 & r) << 10) + (1023 & s) + 65536)
									: (e.push(r), n--);
							} else e.push(r);
						}
						return e;
					})(t)).length,
					u = 128,
					h = 0,
					d = 72;
				for (e = 0; e < t.length; e++) (n = t[e]) < 128 && i.push(a(n));
				var f = i.length,
					p = f;
				for (f && i.push("-"); p < r; ) {
					var g = 2147483647;
					for (e = 0; e < t.length; e++) (n = t[e]) >= u && n < g && (g = n);
					var v = p + 1;
					if (g - u > o((2147483647 - h) / v)) throw RangeError(s);
					for (h += (g - u) * v, u = g, e = 0; e < t.length; e++) {
						if ((n = t[e]) < u && ++h > 2147483647) throw RangeError(s);
						if (n == u) {
							for (var _ = h, m = 36; ; m += 36) {
								var y = m <= d ? 1 : m >= d + 26 ? 26 : m - d;
								if (_ < y) break;
								var b = _ - y,
									w = 36 - y;
								i.push(a(l(y + (b % w)))), (_ = o(b / w));
							}
							i.push(a(l(_))), (d = c(h, v, p == f)), (h = 0), ++p;
						}
					}
					++h, ++u;
				}
				return i.join("");
			};
		t.exports = function (t) {
			var e,
				n,
				s = [],
				o = t.toLowerCase().replace(r, ".").split(".");
			for (e = 0; e < o.length; e++)
				(n = o[e]), s.push(i.test(n) ? "xn--" + u(n) : n);
			return s.join(".");
		};
	},
	function (t, e, n) {
		"use strict";
		n(25);
		var i = n(0),
			r = n(140),
			s = n(16),
			o = n(51),
			a = n(29),
			l = n(94),
			c = n(20),
			u = n(39),
			h = n(12),
			d = n(37),
			f = n(4),
			p = n(3),
			g = n(369),
			v = n(62),
			_ = n(7)("iterator"),
			m = c.set,
			y = c.getterFor("URLSearchParams"),
			b = c.getterFor("URLSearchParamsIterator"),
			w = /\+/g,
			S = Array(4),
			E = function (t) {
				return (
					S[t - 1] || (S[t - 1] = RegExp("((?:%[\\da-f]{2}){" + t + "})", "gi"))
				);
			},
			T = function (t) {
				try {
					return decodeURIComponent(t);
				} catch (e) {
					return t;
				}
			},
			L = function (t) {
				var e = t.replace(w, " "),
					n = 4;
				try {
					return decodeURIComponent(e);
				} catch (t) {
					for (; n; ) e = e.replace(E(n--), T);
					return e;
				}
			},
			A = /[!'()~]|%20/g,
			I = {
				"!": "%21",
				"'": "%27",
				"(": "%28",
				")": "%29",
				"~": "%7E",
				"%20": "+",
			},
			C = function (t) {
				return I[t];
			},
			M = function (t) {
				return encodeURIComponent(t).replace(A, C);
			},
			x = function (t, e) {
				if (e)
					for (var n, i, r = e.split("&"), s = 0; s < r.length; )
						(n = r[s++]).length &&
							((i = n.split("=")),
							t.push({ key: L(i.shift()), value: L(i.join("=")) }));
				return t;
			},
			D = function (t) {
				(this.entries.length = 0), x(this.entries, t);
			},
			O = function (t, e) {
				if (t < e) throw TypeError("Not enough arguments");
			},
			R = l(
				function (t, e) {
					m(this, {
						type: "URLSearchParamsIterator",
						iterator: g(y(t).entries),
						kind: e,
					});
				},
				"Iterator",
				function () {
					var t = b(this),
						e = t.kind,
						n = t.iterator.next(),
						i = n.value;
					return (
						n.done ||
							(n.value =
								"keys" === e
									? i.key
									: "values" === e
									? i.value
									: [i.key, i.value]),
						n
					);
				}
			),
			N = function () {
				u(this, N, "URLSearchParams");
				var t,
					e,
					n,
					i,
					r,
					s,
					o,
					a = arguments.length > 0 ? arguments[0] : void 0,
					l = this,
					c = [];
				if (
					(m(l, {
						type: "URLSearchParams",
						entries: c,
						updateURL: null,
						updateSearchParams: D,
					}),
					void 0 !== a)
				)
					if (p(a))
						if ("function" == typeof (t = v(a)))
							for (e = t.call(a); !(n = e.next()).done; ) {
								if (
									(r = (i = g(f(n.value))).next()).done ||
									(s = i.next()).done ||
									!i.next().done
								)
									throw TypeError("Expected sequence with length 2");
								c.push({ key: r.value + "", value: s.value + "" });
							}
						else for (o in a) h(a, o) && c.push({ key: o, value: a[o] + "" });
					else
						x(
							c,
							"string" == typeof a
								? "?" === a.charAt(0)
									? a.slice(1)
									: a
								: a + ""
						);
			},
			P = N.prototype;
		o(
			P,
			{
				append: function (t, e) {
					O(arguments.length, 2);
					var n = y(this);
					n.entries.push({ key: t + "", value: e + "" }),
						n.updateURL && n.updateURL();
				},
				delete: function (t) {
					O(arguments.length, 1);
					for (
						var e = y(this), n = e.entries, i = t + "", r = 0;
						r < n.length;

					)
						n[r].key === i ? n.splice(r, 1) : r++;
					e.updateURL && e.updateURL();
				},
				get: function (t) {
					O(arguments.length, 1);
					for (var e = y(this).entries, n = t + "", i = 0; i < e.length; i++)
						if (e[i].key === n) return e[i].value;
					return null;
				},
				getAll: function (t) {
					O(arguments.length, 1);
					for (
						var e = y(this).entries, n = t + "", i = [], r = 0;
						r < e.length;
						r++
					)
						e[r].key === n && i.push(e[r].value);
					return i;
				},
				has: function (t) {
					O(arguments.length, 1);
					for (var e = y(this).entries, n = t + "", i = 0; i < e.length; )
						if (e[i++].key === n) return !0;
					return !1;
				},
				set: function (t, e) {
					O(arguments.length, 1);
					for (
						var n,
							i = y(this),
							r = i.entries,
							s = !1,
							o = t + "",
							a = e + "",
							l = 0;
						l < r.length;
						l++
					)
						(n = r[l]).key === o &&
							(s ? r.splice(l--, 1) : ((s = !0), (n.value = a)));
					s || r.push({ key: o, value: a }), i.updateURL && i.updateURL();
				},
				sort: function () {
					var t,
						e,
						n,
						i = y(this),
						r = i.entries,
						s = r.slice();
					for (r.length = 0, e = 0; e < s.length; e++) {
						for (t = s[e], n = 0; n < e; n++)
							if (r[n].key > t.key) {
								r.splice(n, 0, t);
								break;
							}
						n === e && r.push(t);
					}
					i.updateURL && i.updateURL();
				},
				forEach: function (t) {
					for (
						var e,
							n = y(this).entries,
							i = d(t, arguments.length > 1 ? arguments[1] : void 0, 3),
							r = 0;
						r < n.length;

					)
						i((e = n[r++]).value, e.key, this);
				},
				keys: function () {
					return new R(this, "keys");
				},
				values: function () {
					return new R(this, "values");
				},
				entries: function () {
					return new R(this, "entries");
				},
			},
			{ enumerable: !0 }
		),
			s(P, _, P.entries),
			s(
				P,
				"toString",
				function () {
					for (var t, e = y(this).entries, n = [], i = 0; i < e.length; )
						(t = e[i++]), n.push(M(t.key) + "=" + M(t.value));
					return n.join("&");
				},
				{ enumerable: !0 }
			),
			a(N, "URLSearchParams"),
			i({ global: !0, forced: !r }, { URLSearchParams: N }),
			(t.exports = { URLSearchParams: N, getState: y });
	},
	function (t, e, n) {
		var i = n(4),
			r = n(62);
		t.exports = function (t) {
			var e = r(t);
			if ("function" != typeof e)
				throw TypeError(String(t) + " is not iterable");
			return i(e.call(t));
		};
	},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {},
	function (t, e, n) {
		"use strict";
		n.r(e);
		n(25), n(46), n(40);
		class i {
			static fromState(t, e) {
				if ("string" == typeof e)
					try {
						e = JSON.parse(e);
					} catch (t) {}
				var n = new t();
				return i.fillModel(n, e), n;
			}
			static fillModel(t, e) {
				for (const r in t)
					if (
						t.hasOwnProperty(r) &&
						null !== e &&
						"object" == typeof e &&
						r in e
					) {
						var n = t[r];
						"object" == typeof n && null !== n
							? i.fillModel(t[r], e[r])
							: (t[r] = e[r]);
					}
			}
			static fromJSON() {
				throw Error("Function not implemented");
			}
			static toState(t) {
				return JSON.stringify(t);
			}
		}
		n(141);
		var r = n(11);
		n(67), n(83), n(26);
		const s = {
				EMPTY: "",
				TRUE: "t",
				FALSE: "f",
				OK: "0",
				ERROR: "1",
				NOT_EXISTS: "-2147483648",
				UNDEF: "undefined",
			},
			o = {
				EDGE_LIMITATION_DATE: new Date(2019, 9, 16),
				NOT_EXISTS: -2147483648,
				EXISTS: -2147483647,
				page: null,
				page_cpanel: null,
				mobile: !1,
				browser: (function () {
					var t = window.msBrowser || window.browser || window.chrome;
					if (void 0 === t)
						try {
							t = chrome || browser;
						} catch (t) {
							console.error("ERRR: ", t);
						}
					return t;
				})(),
				PAGE_URL: "",
				PAGE_PROTOCOL: "",
				PAGE_HOSTNAME: "",
				IFRAME: "",
				IMAGE_PROCESSING_ENABLED: !1,
				BUILT_IN_DARK_THEME_MODE: 3,
				TURBO_CACHE_ENABLED: !1,
				URL: "",
				isInitialConvertedCounter: 0,
				IMPORT_CSS_INDEX_LAST_POSITION: 1e3,
				url_thankyou_page: "https://nighteye.app/thank-you/",
			};
		class a {
			static makeURL(t, e, n, i, r) {
				void 0 === e &&
					((e = o.PAGE_PROTOCOL),
					(n = o.PAGE_HOSTNAME),
					(i = o.PAGE_PORT),
					(r = o.PAGE_URL)),
					"" !== i && (i = ":" + i);
				var s = t.lastIndexOf("/%20/");
				return s > -1
					? e + "//" + n + i + t.substring(s + 4)
					: "//" === t.slice(0, 2)
					? e + t
					: "/" === t[0]
					? e + "//" + n + i + t
					: -1 !== t.slice(0, 8).lastIndexOf("://")
					? t
					: r + t;
			}
			static parseURL(t) {
				var e = (t = t.replace("www.", s.EMPTY)).indexOf("://"),
					n = 0;
				return (
					-1 !== e &&
						-1 !== (n = (t = t.substring(e + 3)).indexOf("/")) &&
						(t = t.substring(0, n)),
					-1 !== (n = t.indexOf(":", e)) && (t = t.substring(0, n)),
					t
				);
			}
			static isOSColorSchemeDark() {
				const t = window.matchMedia("(prefers-color-scheme: dark)").matches;
				window.matchMedia("(prefers-color-scheme: light)").matches,
					window.matchMedia("(prefers-color-scheme: no-preference)").matches;
				return t;
			}
			static convertBytesToHumanReadable(t, e) {
				var n = e ? 1e3 : 1024;
				if (Math.abs(t) < n) return t + " B";
				var i = e
						? ["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
						: ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"],
					r = -1;
				do {
					(t /= n), ++r;
				} while (Math.abs(t) >= n && r < i.length - 1);
				return t.toFixed(1) + " " + i[r];
			}
			static showInfoDialog(t, e) {
				null === t && (t = "Explanation");
				var n = document.createElement("div");
				n.className = "DialogDim";
				var i = document.createElement("div");
				i.className = "DialogWrapper";
				var r = document.createElement("div");
				i.appendChild(r), (r.className = "Dialog");
				var s = document.createElement("div");
				r.appendChild(s), (s.className = "Title"), (s.innerHTML = t);
				var o = document.createElement("div");
				r.appendChild(o), (o.className = "Content"), (o.innerHTML = e);
				var a = document.createElement("div");
				r.appendChild(a), (a.className = "ButtonWrapper");
				var l = document.createElement("a");
				a.appendChild(l),
					(l.innerHTML = "OK"),
					l.addEventListener("click", () => {
						document.body.removeChild(n), document.body.removeChild(i);
					}),
					document.body.appendChild(n),
					document.body.appendChild(i);
			}
			static isBuiltInDarkThemeExists(t) {
				var e = a.BuiltInWebsites;
				for (var n in e) {
					if (e[n] === t) return !0;
				}
				return !1;
			}
		}
		(a.Mode = { DARK: 1, FILTERED: 2, NORMAL: 4 }),
			(a.BuiltInBehaviourMode = { DISABLED: 1, CONVERT: 2, INTEGRATED: 3 }),
			(a.BuiltInWebsites = {
				YOUTUBE: "youtube.com",
				REDDIT: "reddit.com",
				TWITTER: "twitter.com",
				DUCK_DUCK_GO: "duckduckgo.com",
				COIN_MARKET_CAP: "coinmarketcap.com",
				TWITCH: "twitch.tv",
				NINE_GAG: "9gag.com",
				DOCS_MICROSOFT: "docs.microsoft.com",
				ARSTECHNICA: "arstechnica.com",
				MATERIAL_UI: "material-ui.com",
				INDIEHACKERS: "indiehackers.com",
			}),
			(a.unsupported = {
				newtab: null,
				extensions: null,
				"chrome.google.com": null,
				"accounts.google.com": null,
				"nighteye.app": null,
				"billing.nighteye.app": null,
			}),
			(a.LOCAL_STORAGE = {
				KEYS: {
					MODE: "darkyMode",
					STATE: "darkyState",
					SUPPORTED: "darkySupported",
				},
			});
		class l {
			static sendPromise(t, e, n) {
				return new Promise(function (i, r) {
					try {
						o.browser.runtime.sendMessage(
							{ key: t, action: e, data: n },
							(t) => {
								i(t);
							}
						);
					} catch (t) {
						r(t);
					}
				});
			}
			static send(t, e, n, i) {
				o.browser.runtime.sendMessage({ key: t, action: e, data: n }, i);
			}
		}
		l.Settings = {
			ID: 1,
			ACTION_CHANGE_MODE: 1,
			ACTION_DISABLE: 2,
			ACTION_ENABLE: 3,
			ACTION_ON_START_BROWSER_ACTION: 4,
			ACTION_ON_START_CONVERTER: 5,
			ACTION_UPDATE_IMAGES: 6,
			ACTION_UPDATE_BRIGHTNESS: 7,
			ACTION_UPDATE_CONTRAST: 8,
			ACTION_UPDATE_SATURATION: 9,
			ACTION_UPDATE_TEMPERATURE: 10,
			ACTION_UPDATE_COLOR: 11,
			ACTION_RESTORE_COLOR_DEFAULTS: 12,
			ACTION_UPDATE_DEFAULT_MODE: 13,
			ACTION_UPDATE_DIM: 14,
			ACTION_ON_CHAT_INFO: 15,
			ACTION_UPDATE_CHAT_INFO: 16,
			ACTION_AFTER_FIRST_RUN: 17,
			ACTION_UPDATE_DAY_NIGHT_TIMERS: 18,
			ACTION_UPDATE_TRIAL_EXPIRED: 19,
			ACTION_COLLECT_DATA_FOR_EXPORT: 20,
			ACTION_ACTIVATE_COLOR_SELECTOR: 21,
			ACTION_TOGGLE_MODE: 22,
			ACTION_CHANGE_LANG: 23,
			ACTION_GET_LANG: 24,
			ACTION_GET_CLIENT_INFO: 25,
			ACTION_OPEN_PRICING: 26,
			ACTION_UPDATE_INSTANCE_AND_DEVICE_IDS: 27,
			ACTION_ACTIVATE_EXT: 28,
			ACTION_DEACTIVATE_EXT: 29,
			ACTION_UPDATE_USER_AGENT: 30,
			ACTION_GET_ACCOUNT_MODEL: 31,
			ACTION_GET_TRIAL_EXPIRED_SCREEN_SHOWN_TS: 32,
			ACTION_UPDATE_TRIAL_EXPIRED_SCREEN_SHOWN_TS: 33,
			ACTION_TOGGLE_POWER: 34,
			ACTION_UPDATE_PROPERTY: 35,
			ACTION_GET_PROPERTY: 36,
			ACTION_CHECK_LICENSE: 37,
			ACTION_START_LICENCE_CHECKER: 38,
			ACTION_OPEN_SAFARI_PREFERENCES: 39,
			ACTION_UPDATE_ALLOWED_WEBSITES: 40,
			ACTION_GET_ALLOWED_WEBSITES: 41,
			ACTION_UPDATE_IS_LITE_VERSION: 42,
			ACTION_UPDATE_CHANGING_SCROLLS_ENABLED: 43,
			ACTION_UPDATE_BUILT_IN_THEME_BEHAVIOUR_MODE: 44,
			ACTION_UPDATE_OS_COLOR_SCHEME_CHECKER_ENABLED: 45,
			ACTION_UPDATE_TURBO_CACHE: 46,
		};
		var c = a;
		class u {
			constructor() {
				(this.default_v = !1), (this.global_v = !1), (this.user_changed = !1);
			}
		}
		class h {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class d {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class f {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class p {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class g {
			constructor() {
				(this.default_v = 0), (this.global_v = 0), (this.user_changed = !1);
			}
		}
		class v {
			constructor() {
				(this.default_v = !1), (this.global_v = !1), (this.user_changed = !1);
			}
		}
		class _ {
			constructor() {
				(this.default_v = 3), (this.global_v = 3), (this.user_changed = !1);
			}
		}
		var m = {
			IMAGES: "images",
			BUILT_IN_DARK_THEME: "builtInDarkTheme",
			TURBO_CACHE: "turboCache",
			BRIGHTNESS: "brightness",
			CONTRAST: "contrast",
			SATURATION: "saturation",
			TEMPERATURE: "temperature",
			DIM: "dim",
		};
		class y {
			constructor() {
				(this.images = new u()),
					(this.builtInDarkTheme = new _()),
					(this.turboCache = new v()),
					(this.brightness = new h()),
					(this.contrast = new d()),
					(this.saturation = new f()),
					(this.temperature = new p()),
					(this.dim = new g()),
					(this.palette = {});
			}
			parseAndFill(t) {
				if ("string" == typeof t)
					try {
						t = JSON.parse(t);
					} catch (t) {}
				(this.images = m.IMAGES in t ? t[m.IMAGES] : new u()),
					(this.builtInDarkTheme =
						m.BUILT_IN_DARK_THEME in t ? t[m.BUILT_IN_DARK_THEME] : new _()),
					(this.turboCache = m.TURBO_CACHE in t ? t[m.TURBO_CACHE] : new v()),
					(this.brightness = m.BRIGHTNESS in t ? t[m.BRIGHTNESS] : new h()),
					(this.contrast = m.CONTRAST in t ? t[m.CONTRAST] : new d()),
					(this.saturation = m.SATURATION in t ? t[m.SATURATION] : new f()),
					(this.temperature = m.TEMPERATURE in t ? t[m.TEMPERATURE] : new p()),
					(this.dim = m.DIM in t ? t[m.DIM] : new g());
			}
			static getInitialModel(t) {
				switch (t) {
					case m.IMAGES:
						return new u();
					case m.BUILT_IN_DARK_THEME:
						return new _();
					case m.TURBO_CACHE:
						return new v();
					case m.BRIGHTNESS:
						return new h();
					case m.CONTRAST:
						return new d();
					case m.SATURATION:
						return new f();
					case m.TEMPERATURE:
						return new p();
					case m.DIM:
						return new g();
				}
			}
		}
		class b {
			constructor() {
				(this.name = ""), (this.email = ""), (this.id = "");
			}
		}
		class w {
			constructor() {
				(this.status = !1),
					(this.startTime = "20:00"),
					(this.endTime = "08:00");
			}
		}
		class S {
			constructor() {
				(this.is_trial_expired = !1),
					(this.isLiteVersion = !1),
					(this.first_run = !0),
					(this.enabled = !1),
					(this.supported_url = !0),
					(this.mode = c.Mode.NORMAL),
					(this.global_settings = new y()),
					(this.local_settings = new y()),
					(this.default_mode = 0),
					(this.chat_info = new b()),
					(this.day_night_timers = new w()),
					(this.contentType = ""),
					(this.isChangingScrollsEnabled = !0),
					(this.isOSColorSchemeCheckerEnabled = !1);
			}
		}
		class E {
			static addClass(t, e) {
				!1 === E.hasClass(t, e) && (t.className += " " + e);
			}
			static removeClass(t, e) {
				if (typeof t.className !== s.UNDEF) {
					var n = 0,
						i = t.className.split(" "),
						r = e.split(" ");
					for (let t = i.length; t-- > 0; )
						for (let e = r.length; e-- > 0; )
							if (i[t] === r[e]) {
								++n, (i[t] = s.EMPTY);
								break;
							}
					if (0 !== n) {
						var o = new Array(i.length - n);
						n = 0;
						for (let t = 0; t < i.length; ++t)
							i[t] !== s.EMPTY && (o[n++] = i[t]);
						t.className = o.join(" ");
					}
				}
			}
			static hasClass(t, e) {
				if (typeof t.className === s.UNDEF) return !1;
				for (var n = t.className.split(" "), i = n.length; i-- > 0; )
					if (n[i] === e) return !0;
				return !1;
			}
			static toggleClass(t, e) {
				!0 === E.hasClass(t, e) ? E.removeClass(t, e) : E.addClass(t, e);
			}
		}
		var T = E;
		var L = {
			enabled: "enabled",
			isTrialExpired: "is_trial_expired",
			isLiteVersion: "is_lite",
			firstRun: "first_run",
			defaultMode: "default_mode",
			chatInfo: "chat_info",
			dayNightTimers: "day_night_timers",
			isActivatedByEmail: "is_activated_by_email",
			urlModes: "url_modes",
			urlSettings: "url_settings",
			globalSettings: "global_settings",
			changeModeBG: "change_mode_bg",
			changeStateBG: "change_state_bg",
			version: "version",
			ver: "ver",
			installTimestamp: "install_timestamp",
			instanceID: "instance_id",
			deviceID: "device_id",
			expiredDate: "expiredDate",
			trialExpiredScreenShownTS: "trialExpiredScreenShownTS",
			licenceType: "licenceType",
			defaultLang: "defaultLang",
			allowedWebsites: "allowedWebsites",
			isChangingScrollsEnabled: "isChangingScrollsEnabled",
			isOSColorSchemeCheckerEnabled: "isOSColorSchemeCheckerEnabled",
			lastExtensionWindowOpenTS: "lastExtensionWindowOpenTS",
			discountCode: "discountCode",
		};
		(String.prototype.replaceAllChar = function (t, e) {
			for (var n = -1, i = "", r = this.length, s = 0; s < r; ++s)
				this[s] === t && ((i += this.substring(n + 1, s) + e), (n = s));
			return (i += this.substring(n + 1));
		}),
			(String.prototype.replaceAll = function (t, e) {
				for (var n, i = "", r = 0, s = t.length; ; ) {
					if (-1 === (n = this.indexOf(t, r))) {
						i += this.substring(r);
						break;
					}
					(i += this.substring(r, n) + e), (r = n + s);
				}
				return i;
			}),
			(String.prototype.replaceAt = function (t, e) {
				return this.substring(0, t) + e + this.substring(t + e.length);
			}),
			typeof String.prototype.repeat === s.UNDEF &&
				(String.prototype.repeat = function (t) {
					var e = "" + this;
					if (((t = +t) != t && (t = 0), t < 0))
						throw new RangeError("repeat count must be non-negative");
					if (t == 1 / 0)
						throw new RangeError("repeat count must be less than infinity");
					if (((t = Math.floor(t)), 0 == e.length || 0 == t)) return "";
					if (e.length * t >= 1 << 28)
						throw new RangeError(
							"repeat count must not overflow maximum string size"
						);
					for (var n = "", i = 0; i < t; i++) n += e;
					return n;
				}),
			(Number.prototype.formatSpace = function (t) {
				for (var e = "", n = t; n-- > 0; ) e += "0";
				return (e + this.toString()).substr(-t);
			}),
			(Date.prototype.formatCalendarDate = function () {
				return (
					this.getDate().formatSpace(2) +
					"." +
					(this.getMonth() + 1).formatSpace(2) +
					"." +
					this.getFullYear()
				);
			}),
			(Date.prototype.formatCalendarTime = function () {
				return (
					this.getHours().formatSpace(2) +
					":" +
					this.getMinutes().formatSpace(2)
				);
			}),
			(Date.prototype.equalsCalendarDate = function (t) {
				return (
					(t = "number" == typeof t ? new Date(t) : t),
					this.getDate() === t.getDate() &&
						this.getMonth() === t.getMonth() &&
						this.getFullYear() === t.getFullYear()
				);
			}),
			(Date.prototype.clearTime = function () {
				this.setHours(0),
					this.setMinutes(0),
					this.setSeconds(0),
					this.setMilliseconds(0);
			}),
			(Date.prototype.clear = function () {
				this.clearTime(), this.setYear(1970), this.setMonth(0), this.setDate(1);
			}),
			(Array.prototype.removeElement = function (t) {
				for (var e = this.length; e-- > 0; )
					if (this[e] === t) {
						this.splice(e, 1);
						break;
					}
			}),
			(Array.prototype.pushIfNotExist = function (t, e) {
				for (var n = !0, i = this.length; i-- > 0; )
					if (!0 === e(this[i], t)) {
						n = !1;
						break;
					}
				return !0 === n && this.push(t), n;
			}),
			(Array.prototype.last = function () {
				return 0 === this.length ? null : this[this.length - 1];
			});
		var A = class {
			constructor(t) {
				this.document = t;
			}
			getElementById(t) {
				var e = this.document.getElementById(t);
				return e.removeAttribute("id"), e;
			}
			getElementByIdSafe(t) {
				var e = this.document.getElementById(t);
				return null !== e && e.removeAttribute("id"), e;
			}
			isCurrentPage(t) {
				var e = this.document.URL,
					n = this.document.URL.indexOf("#");
				return (
					-1 !== n && (e = e.substr(0, n)),
					(e = "/" !== e[e.length - 1] ? e + "/" : e),
					"/" !== t[t.length - 1] && (t += "/"),
					-1 !== e.indexOf(t)
				);
			}
			static getX(t) {
				for (var e = 0; t; ) (e += t.offsetLeft), (t = t.offsetParent);
				return e;
			}
			static getY(t) {
				for (var e = 0; t; ) (e += t.offsetTop), (t = t.offsetParent);
				return e;
			}
			static getScreenX(t) {
				return t.getBoundingClientRect().left;
			}
			static getScreenY(t) {
				return t.getBoundingClientRect().top;
			}
			static isMapEmpty(t) {
				for (var e in t) return !1;
				return !0;
			}
			getChildrenHeight(t) {
				for (var e, n = t.childNodes, i = 0, r = n.length; r-- > 0; )
					typeof n[r].tagName !== s.UNDEF &&
						(e = o.getScreenY(n[r]) + n[r].offsetHeight) > i &&
						(i = e);
				return i - o.getScreenY(t);
			}
			static stopPropagation(t) {
				t.stopPropagation();
			}
			static preventDefault(t) {
				t.preventDefault();
			}
		};
		n(356),
			n(357),
			n(358),
			n(359),
			n(360),
			n(361),
			n(362),
			n(363),
			n(364),
			n(365);
		class I {
			constructor() {
				this.initFields(), this.addListeners();
			}
			initFields() {
				(this.power_control_n = document.querySelector(".PowerControl")),
					(this.additional_controls_n = document.querySelector(
						".AdditionalControls"
					)),
					(this.cancel_n = this.additional_controls_n.querySelector(".Cancel")),
					(this.defaults_n = this.additional_controls_n.querySelector(
						".Defaults"
					)),
					(this.apply_n = this.additional_controls_n.querySelector(".Apply")),
					(this.apply_to_controls_n = document.querySelector(
						".ApplyToControls"
					)),
					(this.apply_to_global_n = this.apply_to_controls_n.querySelector(
						".Global"
					)),
					(this.apply_to_local_n = this.apply_to_controls_n.querySelector(
						".Local"
					)),
					(this.defaults_controls_n = document.querySelector(
						".DefaultsControls"
					)),
					(this.defaults_global_n = this.defaults_controls_n.querySelector(
						".Global"
					)),
					(this.defaults_local_n = this.defaults_controls_n.querySelector(
						".Local"
					));
			}
			addListeners() {
				(this.powerListener = null),
					(this.cancelListener = null),
					(this.defaultsListener = null),
					(this.applyListener = null),
					(this.applyToGlobalListener = null),
					(this.applyToLocalListener = null),
					(this.defaultsGlobalListener = null),
					(this.defaultsLocalListener = null),
					this.power_control_n.addEventListener("click", () => {
						this.setPowerState(!this.power_state),
							null !== this.powerListener &&
								this.powerListener(this.power_state);
					}),
					this.cancel_n.addEventListener("click", () => {
						null !== this.cancelListener && this.cancelListener();
					}),
					this.defaults_n.addEventListener("click", () => {
						this.setDefaultsControlsVisibility(!0);
					}),
					this.apply_n.addEventListener("click", () => {
						this.setApplyToControlsVisibility(!0);
					}),
					this.apply_to_global_n.addEventListener("click", () => {
						this.setApplyToControlsVisibility(!1),
							null !== this.applyToGlobalListener &&
								this.applyToGlobalListener();
					}),
					this.apply_to_local_n.addEventListener("click", () => {
						this.setApplyToControlsVisibility(!1),
							null !== this.applyToLocalListener && this.applyToLocalListener();
					}),
					this.defaults_global_n.addEventListener("click", () => {
						this.setDefaultsControlsVisibility(!1),
							null !== this.defaultsGlobalListener &&
								this.defaultsGlobalListener();
					}),
					this.defaults_local_n.addEventListener("click", () => {
						this.setDefaultsControlsVisibility(!1),
							null !== this.defaultsLocalListener &&
								this.defaultsLocalListener();
					});
			}
			setPowerState(t) {
				(this.power_state = t),
					!0 === this.power_state
						? (T.removeClass(this.power_control_n, "Disabled"),
						  T.addClass(this.power_control_n, "Enabled"))
						: (T.addClass(this.power_control_n, "Disabled"),
						  T.removeClass(this.power_control_n, "Enabled"));
			}
			setSlidingControlsVisibility(t) {
				this.setAdditionalControlsVisibility(t),
					this.setApplyToControlsVisibility(t),
					this.setDefaultsControlsVisibility(t);
			}
			setApplyControlVisibility(t) {
				this.apply_n.style.display = !0 === t ? "block" : "none";
			}
			setAdditionalControlsVisibility(t) {
				this.additional_controls_n.style.transform =
					"translateY(" + (!0 === t ? "" : "10") + "0%)";
			}
			setApplyToControlsVisibility(t) {
				this.apply_to_controls_n.style.transform =
					"translateY(" + (!0 === t ? "" : "10") + "0%)";
			}
			setDefaultsControlsVisibility(t) {
				this.defaults_controls_n.style.transform =
					"translateY(" + (!0 === t ? "" : "10") + "0%)";
			}
		}
		n(366);
		class C {
			constructor() {
				this.data = "";
			}
		}
		class M {
			static createLinkAncActivate(t) {
				var e = i.fromState(C, t).data,
					n = "NightEye-export-" + M.getCurrentDateTime() + ".json",
					r = document.createElement("a"),
					s = new Blob([e], { type: "application/force-download" });
				(r.href = window.URL.createObjectURL(s)),
					(r.download = n),
					document.body.appendChild(r),
					r.click();
			}
			static getCurrentDateTime() {
				var t = new Date(),
					e = "" + (t.getMonth() + 1),
					n = "" + t.getDate(),
					i = t.getFullYear(),
					r = t.getHours(),
					s = t.getMinutes(),
					o = t.getSeconds();
				return (
					e < 10 && (e = "0" + e),
					n < 10 && (n = "0" + n),
					r < 10 && (r = "0" + r),
					s < 10 && (s = "0" + s),
					o < 10 && (o = "0" + o),
					n + "." + e + "." + i + "-" + r + "_" + s + "_" + o
				);
			}
		}
		var x = M;
		class D {
			constructor() {
				(this.chosenLanguage = "en"),
					(this.isReady = !1),
					(this.strings = {
						extName: "Night Eye - Dark mode on any website",
						extDescription:
							"Night Eye enables night mode on any website using new algorithm that analyses and converts all colours instead of inverting them.",
						extToggleButton: "Toggle dark and normal mode for the tab",
						okButton: "OK",
						labelPleaseWait: "Please wait",
						labelWebsiteStatusDark: "Dark version",
						labelWebsiteStatusFiltered: "Filtered version",
						labelWebsiteStatusNormal: "Normal version",
						labelWebsiteStatusNotSupported: "Not Supported",
						messageWebsiteNotSupported: "This website is not supported",
						labelContinue: "Continue",
						filterModeNameDark: "Dark",
						filterModeNameFiltered: "Filtered",
						filterModeNameNormal: "Normal",
						labelCurrent: "Current",
						labelWebsite: "website",
						labelColors: "colors",
						labelImages: "images",
						labelBrightness: "brightness",
						labelContrast: "contrast",
						labelSaturation: "saturation",
						labelBlueLight: "blue light",
						labelDim: "dim",
						extTurnOnButton: "Turn on",
						extDisabledStatus: "Night Eye is disabled",
						labelExperimentalFeature: "Experimental feature",
						colorViewTitle: "Color picker",
						colorViewDescription:
							"Pick a color from the website and change it to whatever you wish.",
						colorViewReplacedColor: "Replaced",
						colorViewOriginalColor: "Original",
						imageViewTitle: "Images",
						imageViewDescription:
							"Night Eye processes images making them darker. This is feature requires more computational resources and can slow your computer.",
						switchLabelOn: "On",
						switchLabelOff: "Off",
						brightnessViewTitle: "Brightness",
						brightnessViewDescription:
							"Adjust the brightness by dragging the slider below.",
						contrastViewTitle: "Contrast",
						contrastViewDescription:
							"Adjust the contrast by dragging the slider below.",
						saturationViewTitle: "Saturation",
						saturationViewDescription:
							"Adjust the saturation by dragging the slider below.",
						temperatureViewTitle: "Blue light",
						temperatureViewDescription:
							"Adjust the blue light by dragging the slider below.",
						temperatureViewOption1: "With blue",
						temperatureViewOption2: "Without blue",
						dimViewTitle: "Dim",
						dimViewDescription: "Adjust the dimming level of the websites.",
						refreshViewTitle: "Please reload the page",
						refreshViewDescription:
							"You have to refresh the current page in order to apply latest settings",
						refreshButton: "Refresh",
						supportViewInfo1:
							"We are here to help if you have any questions or problems with Night Eye.",
						supportViewInfo2:
							"To use our support system, please provide correct contact information.",
						supportViewInfo3: "Message us only in English.",
						supportViewInputName: "Name",
						supportViewInputEmail: "Email",
						helpViewHelpButton: "Help",
						helpViewShortcutTitle: "On/Off Shortcut",
						helpViewShortcutDescription:
							"Find out how to assign your own shortcut",
						helpViewUninstallTitle: "Uninstall",
						helpViewUninstallDescription:
							"Find Night Eye in the extensions list and click remove",
						helpViewDefaultModeTitle: "Default mode",
						helpViewDefaultModeDescription:
							"Choose the default mode to be executed when opening new websites",
						firstRunViewTitle: "Quick Start",
						firstRunViewFeature1: "Affects loading speed on some pages",
						firstRunViewFeature2:
							"Enjoy 3 Months for Free<br>After that - just $$9/year",
						firstRunViewFeature3: "No browsing information is collected",
						firstRunViewFeature4: "Contact our support using the built-in chat",
						controlsPanelButtonCancel: "Cancel",
						controlsPanelButtonDefault: "Reset",
						controlsPanelButtonApply: "Apply",
						controlsPanelButtonApplyTo: "Apply to",
						controlsPanelAllWebsites: "all websites",
						controlsPanelCurrentWebsite: "current website",
						controlsPanelButtonRestoreTo: "Restore defaults",
						controlsPanelForAllWebsites: "for all websites",
						controlsPanelForCurrentWebsite: "for current website",
						tooltipDark:
							"Using this mode, Night Eye will change colors based on selected settings",
						tooltipFiltered:
							"Using this mode, the website will be by default plus effect of several filters - brightness, contrast, saturation, warm/cold, dim",
						tooltipNormal:
							"Using this mode, the website will be by default, Night Eye will not change any color",
					});
			}
			setLanguage(t) {
				localStorage.setItem("defaultLang", t);
			}
			getLanguage() {
				var t = localStorage.getItem("defaultLang");
				return null != t ? t : "en";
			}
			getMessage(t, e) {
				"string" == typeof e ? (e = [e]) : e || (e = []);
				for (
					var n = this.strings[t].replace(/\$\$/g, "@@@@"), i = 0;
					i < e.length;
					i++
				) {
					var r = new RegExp("(?!\\$\\$)\\$" + (i + 1), "g");
					n = n.replace(r, e[i]);
				}
				return n.replace(/\@\@\@\@/g, "$$");
			}
			fetchAndParse() {
				this.fetchAndParseInternal();
			}
			fetchAndParseInternal() {
				var t = "_locales/" + this.getLanguage() + "/messages.json";
				this.fetchFile(t);
			}
			fetchFileMacOS(t) {
				r.a
					.shared()
					.getFile(t)
					.then(
						(t) => {
							this.parseJSON(t);
						},
						(t) => {
							console.error(`Error fetchInWebView ${t}`);
						}
					);
			}
			fetchFile(t) {
				var e = o.browser.extension.getURL(t),
					n = new XMLHttpRequest();
				n.open("GET", e, !0),
					(n.onreadystatechange = () => {
						4 === n.readyState &&
							n.responseText &&
							this.parseJSON(n.responseText);
					});
				try {
					n.send();
				} catch (t) {
					console.error(t);
				}
			}
			parseJSON(t) {
				var e,
					n = JSON.parse(t);
				for (e in n) {
					var i,
						r = n[e].message,
						s = n[e].placeholders;
					if (s)
						for (i in s) {
							var o = new RegExp("\\$" + i + "\\$");
							r = r.replace(o, s[i].content);
						}
					this.strings[e] = r;
				}
				this.isReady = !0;
			}
			static shared() {
				return (
					null === D.instance &&
						((D.instance = new D()), D.instance.fetchAndParse()),
					D.instance
				);
			}
		}
		D.instance = null;
		var O = D.shared();
		class R {
			constructor() {
				this.isLiteEnabled = !1;
			}
		}
		class N {
			constructor() {
				(this.id = ""),
					(this.email = ""),
					(this.newDeviceId = ""),
					(this.expiredDate = 0),
					(this.isFreeTrial = !1),
					(this.type = 0);
			}
		}
		n(370);
		class P {
			constructor() {
				this.initFields(), this.addListeners();
			}
			initFields() {
				(this.mainView = document.querySelector(".TrialExpired")),
					(this.liteVersionView = document.querySelector(
						".TrialExpired.LiteVersionView"
					)),
					(this.proVersionView = document.querySelector(
						".TrialExpired.ProVersionView"
					)),
					(this.activationView = document.querySelector(
						".TrialExpired.ActivationView"
					)),
					(this.mainAlreadyActivatedView = document.querySelector(
						".TrialExpired.AlreadyActivatedView"
					)),
					(this.successActivationView = document.querySelector(
						".TrialExpired.SuccessActivationView"
					)),
					(this.logoutView = document.querySelector(
						".TrialExpired.LogoutView"
					)),
					(this.emailInput = document.getElementById("actionvationEmailInput")),
					(this.errorLabel = this.activationView.querySelector(".ErrorLabel")),
					(this.email = "");
			}
			async initEmail() {
				this.email = localStorage.getItem(L.isActivatedByEmail);
			}
			addListeners() {
				this.mainView
					.querySelector(".LiteVersionButton")
					.addEventListener("click", () => {
						this.showLiteVersionView();
					}),
					this.mainView
						.querySelector(".ProVersionButton")
						.addEventListener("click", () => {
							null === this.email
								? (T.removeClass(this.mainView, "Active"),
								  T.addClass(this.proVersionView, "Active"))
								: this.showMainAlreadyActivatedView();
						}),
					this.proVersionView
						.querySelector(".ActivationViewButton")
						.addEventListener("click", () => {
							T.removeClass(this.proVersionView, "Active"),
								T.addClass(this.activationView, "Active");
						}),
					this.proVersionView
						.querySelector(".PricingButton")
						.addEventListener("click", () => {
							o.browser.tabs.create({
								url: "https://billing.nighteye.app/client/login",
							});
						}),
					this.proVersionView
						.querySelector(".BackButtonPanel")
						.addEventListener("click", () => {
							window.location.reload();
						}),
					this.mainAlreadyActivatedView
						.querySelector(".BackButtonPanel")
						.addEventListener("click", () => {
							window.location.reload();
						}),
					this.mainAlreadyActivatedView
						.querySelector(".PricingButton")
						.addEventListener("click", () => {
							o.browser.tabs.create({
								url: "https://billing.nighteye.app/client/login",
							});
						}),
					this.liteVersionView
						.querySelector(".UpdateAllowedWebsitesButton")
						.addEventListener("click", () => {
							this.updateAllowedWebsites(), o.page.reload(), window.close();
						}),
					this.liteVersionView
						.querySelector(".ProVersionButton")
						.addEventListener("click", () => {
							null === this.email
								? (T.removeClass(this.liteVersionView, "Active"),
								  T.addClass(this.proVersionView, "Active"))
								: this.showMainAlreadyActivatedView();
						}),
					this.liteVersionView
						.querySelector(".BackButtonPanel")
						.addEventListener("click", () => {
							window.location.reload();
						}),
					this.liteVersionView
						.querySelector("#readAboutLiteBtnID")
						.addEventListener("click", () => {
							o.browser.tabs.create({
								url: "https://nighteye.app/lite-free-dark-mode-extension/",
							});
						}),
					this.liteVersionView
						.querySelectorAll(".RemoveButton")
						.forEach((t) => {
							t.addEventListener("click", () => {
								var e = t.parentNode.querySelector("input");
								(e.selectionStart = e.selectionEnd = e.value.length), e.focus();
							});
						}),
					this.activationView
						.querySelector(".BackButton")
						.addEventListener("click", () => {
							T.removeClass(this.activationView, "Active"),
								this.isActivationViewIsMain ||
									T.addClass(this.mainView, "Active");
						}),
					this.logoutView
						.querySelector(".BackButton")
						.addEventListener("click", () => {
							T.removeClass(this.logoutView, "Active");
						}),
					this.logoutView
						.querySelector("#controlPanel")
						.addEventListener("click", () => {
							o.browser.tabs.create({
								url: "https://billing.nighteye.app/client/login",
							});
						}),
					this.successActivationView
						.querySelector(".ActivateButton")
						.addEventListener("click", () => {
							T.removeClass(this.successActivationView, "Active");
						}),
					this.successActivationView
						.querySelector(".ActivateButton")
						.addEventListener("click", () => {
							T.removeClass(this.successActivationView, "Active"),
								window.close();
						}),
					this.activationView
						.querySelector(".ActivateButton")
						.addEventListener("click", () => {
							var t = this.emailInput.value.trim();
							this.activate(t);
						}),
					this.logoutView
						.querySelector(".DeactivateButton")
						.addEventListener("click", () => {
							this.deactivate();
						}),
					this.emailInput.addEventListener("focus", () => {
						T.removeClass(this.errorLabel, "Active");
					}),
					this.emailInput.addEventListener("keyup", (t) => {
						if (13 === t.keyCode) {
							var e = this.emailInput.value.trim();
							this.emailInput.blur(), this.activate(e);
						}
					});
			}
			initAllowedWebsites(t) {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_GET_ALLOWED_WEBSITES,
					null,
					(e) => {
						for (
							var n = this.liteVersionView.querySelectorAll("input"), i = 0;
							i < e.length;
							++i
						)
							if (n.length > i) {
								var r = n[i];
								(r.value = e[i]),
									(function (t) {
										t.parentNode.parentNode
											.querySelector(".EditButton")
											.addEventListener("click", function () {
												t.focus(), t.select();
											});
									})(r);
							}
						t();
					}
				);
			}
			updateAllowedWebsites() {
				for (
					var t = [], e = this.liteVersionView.querySelectorAll("input"), n = 0;
					n < e.length;
					++n
				) {
					var i = e[n].value.trim().toLowerCase();
					t.push(a.parseURL(i));
				}
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_UPDATE_ALLOWED_WEBSITES,
					t,
					() => {}
				);
			}
			showMainView() {
				if (
					((this.isActivationViewIsMain = !1),
					T.addClass(this.mainView, "Active"),
					window.navigator.userAgent.indexOf("Edge") > -1)
				) {
					var t = this.mainView.querySelector(".PricingButton");
					(t.style.opacity = 0),
						t.removeAttribute("href"),
						(t.querySelector(".ControlButton").style.cursor = "default"),
						(this.mainView.querySelector(".FirstRow").innerHTML =
							'We hope that Night Eye proved its usefulness for the past months!<br><div class="TextSeparator"></div>If you want to continue using it, you need to have an account.');
				}
			}
			showLiteVersionView() {
				this.initAllowedWebsites(() => {
					T.removeClass(this.mainView, "Active"),
						T.addClass(this.liteVersionView, "Active");
				});
				var t = new R();
				(t.isLiteEnabled = s.TRUE),
					l.send(
						l.Settings.ID,
						l.Settings.ACTION_UPDATE_IS_LITE_VERSION,
						t,
						() => {}
					);
			}
			showActivationSuccessView(t) {
				(this.successActivationView.querySelector(
					".SuccessTextLabel"
				).innerHTML = t),
					T.removeClass(this.mainView, "Active"),
					T.removeClass(this.activationView, "Active"),
					T.addClass(this.successActivationView, "Active");
			}
			async showMainAlreadyActivatedView() {
				var t;
				if (
					((this.isActivationViewIsMain = !1),
					(t = localStorage.getItem(L.isActivatedByEmail)),
					(this.mainAlreadyActivatedView.querySelector(
						".AccountEmail"
					).innerHTML = t),
					T.addClass(this.mainAlreadyActivatedView, "Active"),
					window.navigator.userAgent.indexOf("Edge") > -1)
				) {
					var e = this.mainAlreadyActivatedView.querySelector(".PricingButton");
					(e.style.opacity = 0),
						(e.style.cursor = "auto"),
						(e.style.pointerEvents = "none"),
						(e.querySelector(".ControlButton").style.cursor = "default"),
						(this.mainAlreadyActivatedView.querySelector(
							".FirstRow"
						).innerHTML =
							'We hope that Night Eye proved its usefulness for the past months!<br><div class="TextSeparator"></div>If you want to continue using it,<br> you have to pay.');
				}
			}
			showActivationView(t) {
				(this.isActivationViewIsMain = t),
					t &&
						(this.activationView.querySelector(
							".ClientAreaWrapper"
						).style.opacity = "1"),
					T.removeClass(this.mainView, "Active"),
					T.addClass(this.activationView, "Active");
			}
			showLogoutView(t) {
				(this.logoutView.querySelector(".ActivatedEmail").innerHTML = t),
					T.addClass(this.logoutView, "Active");
			}
			activate(t) {
				if (this.isEmailInvalid(t))
					return (
						(this.errorLabel.innerHTML =
							"It is not valid email. Please try again!"),
						void T.addClass(this.errorLabel, "Active")
					);
				(t = encodeURIComponent(t)), o.page.showLoading();
				var e = localStorage.getItem(L.instanceID),
					n = localStorage.getItem(L.deviceID);
				this.activateNetworkRequest(e, n, t);
			}
			activateNetworkRequest(t, e, n) {
				var i =
						"https://billing.nighteye.app/exc/InstanceController?ac=b&id=" +
						t +
						"&devid=" +
						e +
						"&eml=" +
						n,
					r = new XMLHttpRequest();
				(r.onreadystatechange = async () => {
					if (r.readyState === XMLHttpRequest.DONE && 200 === r.status) {
						// var t = JSON.parse(r.responseText);
						await this.activateProcessResponse(
							{
								status: true,
								data: {
									expiredDate: "27 May 2999",
									id: "1234",
									newDeviceId: "1322",
								},
							},
							n
						);
					}
				}),
					r.open("POST", i, !0),
					r.send(i);
			}
			async activateProcessResponse(t, e) {
				if (t.status) {
					// var n = t.data;
					// await l.sendPromise(
					// 	l.Settings.ID,
					// 	l.Settings.ACTION_UPDATE_TRIAL_EXPIRED,
					// 	{ email: e, value: s.FALSE, expiredDate: n.expiredDate }
					// ),
					// 	"" !== n.id &&
					// 		(await l.sendPromise(
					// 			l.Settings.ID,
					// 			l.Settings.ACTION_UPDATE_INSTANCE_AND_DEVICE_IDS,
					// 			{ instanceID: n.id, deviceID: n.newDeviceId }
					// 		)),
					this.activationSuccess(n);
				} else this.activationError(t.data);
			}
			activationError(t) {
				var e = "";
				switch (parseInt(t)) {
					case 1:
						e = "This is not a valid e-mail. Please try again!";
						break;
					case 2:
						e =
							"The email address you've entered doesn't appear to exist.<br> Please check your entry or visit</a>";
						break;
					case 3:
						e = "Your subscription has expired. <br>To continue visit";
						break;
					case 4:
						e =
							"You've reached the maximum allowed active browsers for this account.<br> To manage them, please visit";
						break;
					case 5:
						e = "Your trial version has expired. Please visit ";
						break;
					case 6:
						e =
							"You have modified extension`s ID. You need from active paid account to continue using it!";
				}
				(this.errorLabel.innerHTML = e),
					T.addClass(this.errorLabel, "Active"),
					o.page.hideLoading();
			}
			activationSuccess(t) {
				var e = 3;
				t.expiredDate.indexOf("never") > -1 && (e = 10);
				var n = "You can enjoy Night Eye on up to " + e + " browsers at once.";
				t.isFreeTrial &&
					(n =
						"You can enjoy Night Eye on up to 3 browsers at once until your <br> Free Trial expires on " +
						t.expiredDate),
					this.showActivationSuccessView(n),
					o.page.hideLoading();
			}
			deactivate() {
				o.page.showLoading(), this.deactivateNetworkRequest();
			}
			deactivateNetworkRequest() {
				var t = new XMLHttpRequest(),
					e =
						"https://billing.nighteye.app/exc/InstanceController?ac=c&id=" +
						localStorage.getItem(L.instanceID);
				(t.onreadystatechange = () => {
					if (t.readyState === XMLHttpRequest.DONE && 200 === t.status)
						try {
							var e = JSON.parse(t.responseText);
							this.deactivateProcessResponse(e);
						} catch (t) {
							console.error("Cannot parse respnse.");
						}
				}),
					t.open("POST", e, !0),
					t.send(e);
			}
			deactivateProcessResponse(t) {
				t.status &&
					(l.send(l.Settings.ID, l.Settings.ACTION_CHECK_LICENSE, {}, () => {
						window.close();
					}),
					window.navigator.userAgent.indexOf("Edge") > -1 &&
						setTimeout(function () {
							window.close();
						}, 1e3));
			}
			isEmailInvalid(t) {
				return (
					null ===
					(t = t.trim()).match(
						"[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})"
					)
				);
			}
			getFormattedTS(t) {
				var e = new Date(t),
					n = "" + e.getDate(),
					i = e.getFullYear();
				return (
					n +
					" " +
					[
						"Jan",
						"Feb",
						"Mar",
						"Apr",
						"May",
						"Jun",
						"Jul",
						"Aug",
						"Sep",
						"Oct",
						"Nov",
						"Dec",
					][e.getMonth()] +
					" " +
					i
				);
			}
		}
		class k {
			constructor(t, e, n, i, r, s, o) {
				(this.options_n = null),
					(this.is_trial_expired = t),
					(this.support_c = s),
					(this.isLiteVersion = o),
					this.initFields(e, n, i, r);
			}
			initFields(t, e, n, i) {
				(this.options_n = document.querySelector(".Options")),
					this.initSupport(),
					this.initClientArea(),
					this.initLiteVersion(),
					this.initHelp(),
					this.initWhiteList(),
					this.initShortcut(),
					this.initDayNightTimers(e),
					this.initUninstall(),
					this.initDefaultMode(t),
					this.initOSColorSchemeChecker(i),
					this.initIsChangingScrolls(n),
					this.initExportImport(),
					this.initDefaultLanguage(),
					this.options_n
						.querySelector(".Popup")
						.addEventListener("click", A.stopPropagation),
					this.options_n.addEventListener("click", (t) => {
						T.removeClass(this.options_n, "Active"), t.stopPropagation();
					}),
					document
						.querySelector(".OptionsTrigger")
						.addEventListener("click", () => {
							T.addClass(this.options_n, "Active");
						});
			}
			initSupport() {
				document
					.querySelector(".SupportTrigger")
					.addEventListener("click", () => {
						T.removeClass(this.options_n, "Active"), this.support_c.show();
					});
			}
			initClientArea() {
				var t = this.options_n.querySelector(".ClientAreaActivateButton"),
					e = this.options_n.querySelector(".InfoEmail");
				((n, i) => {
					null === n
						? ((t.innerHTML = "Activate now"), (e.innerHTML = "Expires: " + i))
						: ((t.innerHTML = "Activated"),
						  (e.innerHTML =
								"by " + decodeURIComponent(n) + "<br>expires: " + i)),
						e.addEventListener("click", () => {
							t.click();
						}),
						t.addEventListener("click", () => {
							T.removeClass(this.options_n, "Active"),
								void 0 === o.page.trial_expired_c &&
									(o.page.trial_expired_c = new P());
							((t) => {
								null === t || -1 === t.indexOf("@")
									? o.page.trial_expired_c.showActivationView(!0)
									: o.page.trial_expired_c.showLogoutView(t);
							})(localStorage.getItem(L.isActivatedByEmail));
						});
				})(
					localStorage.getItem(L.isActivatedByEmail),
					localStorage.getItem(L.expiredDate)
				),
					this.options_n
						.querySelector(".ClientAreaButton")
						.addEventListener("click", function () {
							o.browser.tabs.create({
								url: "https://billing.nighteye.app/client/login",
							});
						});
			}
			initLiteVersion() {
				var t = this.options_n.querySelector(".LiteVersion");
				t.addEventListener("click", () => {
					T.removeClass(this.options_n, "Active"),
						o.page.trial_expired_c.showLiteVersionView();
				});
				var e = this.options_n.querySelector(".GoProLink");
				e.addEventListener("click", () => {
					o.browser.tabs.create({ url: "https://billing.nighteye.app" });
				}),
					this.isLiteVersion ||
						((t.style = "display: none;"), (e.style = "display: none;"));
			}
			initHelp() {
				this.options_n.querySelector(".Help").addEventListener("click", () => {
					o.browser.tabs.create({ url: "https://nighteye.app/help" });
				}),
					this.options_n
						.querySelector("#helpOSIntegrationLink")
						.addEventListener("click", () => {
							o.browser.tabs.create({
								url: "http://nighteye.app/help#os-integration",
							});
						});
			}
			initWhiteList() {
				this.options_n
					.querySelector(".WhiteList")
					.addEventListener("click", () => {
						o.browser.tabs.create({
							url: "https://nighteye.app/whitelist-website-disable-dark-mode",
						});
					});
			}
			initShortcut() {
				this.options_n
					.querySelector(".Shortcut")
					.addEventListener("click", () => {
						o.browser.tabs.create({
							url:
								"https://nighteye.app/how-to-create-custom-keyboard-shortcuts-for-you-google-chrome-extensions/",
						});
					});
			}
			initDayNightTimers(t) {
				this.options_n
					.querySelector("#dayNightTimerInstructions")
					.addEventListener("click", () => {
						o.browser.tabs.create({
							url: "https://nighteye.app/schedule-dark-mode/",
						});
					});
				for (
					var e = this.options_n.querySelector(".PowerTimerSelect"),
						n = this.options_n.querySelector(".StartTimerSelect"),
						i = this.options_n.querySelector(".EndTimerSelect"),
						r = "",
						s = "",
						a = 0;
					a < 24;
					++a
				)
					for (var c = 0, u = 0; u < 4; ++u) {
						var h = (a < 9 ? "0" + a : a) + ":" + (0 === c ? "0" + c : c),
							d = `<option value='${h}'>${h}</option>`;
						(r += d), (s += d), (c += 15);
					}
				(n.innerHTML = r),
					(i.innerHTML = s),
					(e.value = t.status ? "1" : "2"),
					(n.value = t.startTime),
					(i.value = t.endTime);
				var f = (t) => {
					var r = "1" === e.value,
						s = n.value,
						a = i.value;
					l.send(
						l.Settings.ID,
						l.Settings.ACTION_UPDATE_DAY_NIGHT_TIMERS,
						{ status: r, startTime: s, endTime: a },
						() => {
							t && (o.page.reload(), window.location.reload());
						}
					);
				};
				e.addEventListener("change", () => {
					f(!0);
				}),
					n.addEventListener("change", () => {
						"1" === e.value ? f(!0) : f(!1),
							this.showInfoMessage(
								this.options_n.querySelector(".DayNightTimers"),
								"Close the extension window to activate"
							);
					}),
					i.addEventListener("change", () => {
						"1" === e.value ? f(!0) : f(!1),
							this.showInfoMessage(
								this.options_n.querySelector(".DayNightTimers"),
								"Close the extension window to activate"
							);
					});
			}
			initUninstall() {
				this.options_n
					.querySelector(".Uninstall")
					.addEventListener("click", () => {
						window.navigator.userAgent.indexOf("Edge") > -1
							? o.browser.tabs.create({
									url: "https://nighteye.app/help#uninstall-edge",
							  })
							: window.navigator.userAgent.indexOf("Firefox") > -1
							? o.browser.tabs.create({ url: "about:addons" })
							: window.navigator.userAgent.indexOf("Vivaldi") > -1
							? o.browser.tabs.create({ url: "vivaldi://extensions" })
							: window.navigator.userAgent.indexOf("Yandex") > -1
							? o.browser.tabs.create({
									url: "browser://tune/?id=" + o.browser.runtime.id,
							  })
							: o.browser.tabs.create({
									url: "chrome://extensions/?id=" + o.browser.runtime.id,
							  });
					});
			}
			initDefaultMode(t) {
				var e = this.options_n.querySelector(".DefaultModeSelect");
				switch (
					((e.innerHTML =
						'<option value="' +
						a.Mode.DARK +
						'">' +
						O.getMessage("filterModeNameDark") +
						'</option><option value="' +
						a.Mode.FILTERED +
						'">' +
						O.getMessage("filterModeNameFiltered") +
						'</option><option value="' +
						a.Mode.NORMAL +
						'">' +
						O.getMessage("filterModeNameNormal") +
						"</option>"),
					parseInt(t))
				) {
					case a.Mode.DARK:
						e.selectedIndex = 0;
						break;
					case a.Mode.FILTERED:
						e.selectedIndex = 1;
						break;
					case a.Mode.NORMAL:
						e.selectedIndex = 2;
						break;
					default:
						console.error(`Not implemeted mode with number: ${t}`);
				}
				e.addEventListener("change", (t) => {
					l.send(l.Settings.ID, l.Settings.ACTION_UPDATE_DEFAULT_MODE, {
						mode: parseInt(t.srcElement.value),
					});
				});
			}
			initOSColorSchemeChecker(t) {
				var e = this.options_n.querySelector(".OSColorSchemeCheckerSelect");
				(e.innerHTML =
					'<option value="' +
					s.TRUE +
					'">Enabled</option>        <option value="' +
					s.FALSE +
					'">Disabled</option>'),
					(e.selectedIndex = t ? 0 : 1),
					e.addEventListener("change", (t) => {
						l.send(
							l.Settings.ID,
							l.Settings.ACTION_UPDATE_OS_COLOR_SCHEME_CHECKER_ENABLED,
							{ isEnabled: t.srcElement.value }
						);
					});
			}
			initIsChangingScrolls(t) {
				var e = this.options_n.querySelector(".ChangingScrollsSelect");
				(e.innerHTML =
					'            <option value="' +
					s.TRUE +
					'">Enabled</option>            <option value="' +
					s.FALSE +
					'">Disabled</option>'),
					(e.selectedIndex = t ? 0 : 1),
					e.addEventListener("change", (t) => {
						l.send(
							l.Settings.ID,
							l.Settings.ACTION_UPDATE_CHANGING_SCROLLS_ENABLED,
							{ isEnabled: t.srcElement.value }
						);
					});
			}
			initExportImport() {
				this.options_n
					.querySelector(".ExportImport .Export")
					.addEventListener("click", () => {
						this.exportSettings();
					}),
					this.options_n
						.querySelector(".ExportImport .Import")
						.addEventListener("click", () => {
							this.importSettings();
						});
			}
			initDefaultLanguage() {
				var t = this.options_n.querySelector(".DefaultLanguageSelect");
				k.initDefaultLanguageSelect(t, !0);
			}
			static initDefaultLanguageSelect(t, e) {
				var n = O.getLanguage();
				(t.innerHTML =
					'            <option value="en" ' +
					("en" === n ? "selected" : "") +
					'>English</option>\n            <option value="es" ' +
					("es" === n ? "selected" : "") +
					'>Spanish</option>\n            <option value="pt" ' +
					("pt" === n ? "selected" : "") +
					'>Portuguese</option>\n            <option value="fr" ' +
					("fr" === n ? "selected" : "") +
					'>French</option>\n            <option value="de" ' +
					("de" === n ? "selected" : "") +
					'>German</option>\n            <option value="ru" ' +
					("ru" === n ? "selected" : "") +
					'>Russian</option>\n            <option value="tr" ' +
					("tr" === n ? "selected" : "") +
					'>Turkish</option>\n            <option value="jp" ' +
					("jp" === n ? "selected" : "") +
					'>Japanese</option>\n            <option value="zh_CN" ' +
					("zh_CN" === n ? "selected" : "") +
					'>Chinese</option>\n            <option value="zh_TW" ' +
					("zh_TW" === n ? "selected" : "") +
					'>Taiwanese</option>\n            <option value="bg" ' +
					("bg" === n ? "selected" : "") +
					">Bulgarian</option>"),
					t.addEventListener("change", () => {
						O.setLanguage(t.value), e && window.location.reload();
					});
			}
			exportSettings() {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_COLLECT_DATA_FOR_EXPORT,
					{},
					(t) => {
						x.createLinkAncActivate(t);
					}
				);
			}
			importSettings() {
				var t = this.options_n.querySelector(".ExportImport"),
					e = t.querySelector("input");
				e.addEventListener("change", (e) => {
					if (
						(this.showInfoMessage(t, "Loading"),
						"function" != typeof window.FileReader)
					)
						throw "The file API isn't supported on this browser.";
					let n = e.target;
					if (!n)
						throw "The browser does not properly implement the event object";
					if (!n.files)
						throw "This browser does not support the `files` property of the file input.";
					if (!n.files[0]) return;
					let i = n.files[0],
						r = new FileReader();
					(r.onload = (e) => {
						var n = "error";
						try {
							var i = JSON.parse(atob(e.target.result));
							i.length > 0 && (n = "success");
							for (var r = 0; r < i.length; ++r) {
								var s = i[r];
								localStorage.setItem(s.key, s.value);
							}
						} catch (t) {
							console.error(t);
						}
						this.showInfoMessage(t, n);
					}),
						(r.onerror = (t) => {
							console.log(t);
						}),
						r.readAsText(i);
				}),
					e.click();
			}
			showInfoMessage(t, e) {
				var n = t.querySelector(".InfoMessage");
				(n.innerHTML = e), T.addClass(n, "Active");
			}
			getCurrentDateTime() {
				var t = new Date(),
					e = "" + (t.getMonth() + 1),
					n = "" + t.getDate(),
					i = t.getFullYear(),
					r = t.getHours(),
					s = t.getMinutes(),
					o = t.getSeconds();
				return (
					e < 10 && (e = "0" + e),
					n < 10 && (n = "0" + n),
					r < 10 && (r = "0" + r),
					s < 10 && (s = "0" + s),
					o < 10 && (o = "0" + o),
					n + "." + e + "." + i + "-" + r + "_" + s + "_" + o
				);
			}
		}
		class V {
			constructor(t) {
				(this.contentType = t), this.initFields(), this.addListeners();
			}
			initFields() {
				var t = document.querySelector(".Options > .Popup > .Row > .Left");
				(this.settings_n = document.querySelector(".Settings")),
					(this.colors_n = this.settings_n.querySelector(".Colors")),
					(this.images_n = this.settings_n.querySelector(".Images")),
					(this.builtInDarkTheme_n = this.settings_n.querySelector(
						".BuiltInDarkTheme"
					)),
					(this.turboCache_n = this.settings_n.querySelector(".TurboCache")),
					(this.brightness_n = this.settings_n.querySelector(".Brightness")),
					(this.contrast_n = this.settings_n.querySelector(".Contrast")),
					(this.saturation_n = this.settings_n.querySelector(".Saturation")),
					(this.temperature_n = this.settings_n.querySelector(".Temperature")),
					(this.dim_n = this.settings_n.querySelector(".Dim")),
					o.browser.runtime.getPlatformInfo((e) => {
						switch (e.os) {
							case "mac":
								t.innerHTML = "⌘+1, ⌘+2";
								break;
							default:
								t.innerHTML = "Alt+Shift+2, Alt+Shift+3";
						}
					});
			}
			addListeners() {
				(this.colorsListener = null),
					(this.imagesListener = null),
					(this.builtInDarkThemeListener = null),
					(this.turboCacheListener = null),
					(this.brightnessListener = null),
					(this.contrastListener = null),
					(this.saturationListener = null),
					(this.temperatureListener = null),
					(this.dimListener = null),
					this.colors_n.addEventListener("click", () => {
						null !== this.colorsListener && this.colorsListener();
					}),
					this.images_n.addEventListener("click", () => {
						null !== this.imagesListener && this.imagesListener();
					}),
					this.builtInDarkTheme_n.addEventListener("click", () => {
						null !== this.builtInDarkThemeListener &&
							this.builtInDarkThemeListener();
					}),
					this.turboCache_n.addEventListener("click", () => {
						null !== this.turboCacheListener && this.turboCacheListener();
					}),
					this.brightness_n.addEventListener("click", () => {
						null !== this.brightnessListener && this.brightnessListener();
					}),
					this.contrast_n.addEventListener("click", () => {
						null !== this.contrastListener && this.contrastListener();
					}),
					this.saturation_n.addEventListener("click", () => {
						null !== this.saturationListener && this.saturationListener();
					}),
					this.temperature_n.addEventListener("click", () => {
						null !== this.temperatureListener && this.temperatureListener();
					}),
					this.dim_n.addEventListener("click", () => {
						null !== this.dimListener && this.dimListener();
					});
			}
			setColorsState(t) {
				this.setButtonState(this.colors_n, t);
			}
			setImagesState(t) {
				var e = "ImageStatus Disabled",
					n = "disabled",
					i = this.images_n.querySelector(".CPUStatus");
				t &&
					((e = "ImageStatus Enabled"),
					(n = "enabled"),
					(i.style.display = "block"));
				for (
					var r = this.images_n.querySelectorAll(".ImageStatus"), s = 0;
					s < r.length;
					++s
				) {
					var o = r[s];
					(o.className = e), (o.innerHTML = n);
				}
				this.setButtonState(this.images_n, t);
			}
			setBuiltInDarkThemeState(t) {
				(t = parseInt(t)),
					a.isBuiltInDarkThemeExists(o.URL) &&
						(t === a.BuiltInBehaviourMode.INTEGRATED
							? this.setButtonState(this.builtInDarkTheme_n, !0)
							: (T.removeClass(this.builtInDarkTheme_n, "Active"),
							  T.addClass(this.builtInDarkTheme_n, "Usable")));
			}
			setTurboCacheState(t) {
				this.setButtonState(this.turboCache_n, t);
			}
			setBrightnessState(t) {
				this.setButtonState(this.brightness_n, t);
			}
			setContrastState(t) {
				this.setButtonState(this.contrast_n, t);
			}
			setSaturationState(t) {
				this.setButtonState(this.saturation_n, t);
			}
			setTemperatureState(t) {
				this.setButtonState(this.temperature_n, t);
			}
			setDimState(t) {
				this.setButtonState(this.dim_n, t);
			}
			setButtonState(t, e) {
				e ? T.addClass(t, "Active") : T.removeClass(t, "Active");
			}
			setMode(t) {
				const e = a.isBuiltInDarkThemeExists(o.URL);
				switch (t) {
					case a.Mode.DARK:
						T.removeClass(this.colors_n, "Disabled"),
							T.removeClass(this.images_n, "Disabled"),
							T.removeClass(this.builtInDarkTheme_n, "Disabled"),
							e
								? T.addClass(this.turboCache_n, "Disabled")
								: T.removeClass(this.turboCache_n, "Disabled"),
							T.removeClass(this.brightness_n, "Disabled"),
							T.removeClass(this.contrast_n, "Disabled"),
							T.removeClass(this.saturation_n, "Disabled"),
							T.removeClass(this.temperature_n, "Disabled"),
							T.removeClass(this.dim_n, "Disabled");
						break;
					case a.Mode.FILTERED:
						T.addClass(this.colors_n, "Disabled"),
							T.addClass(this.images_n, "Disabled"),
							T.addClass(this.builtInDarkTheme_n, "Disabled"),
							T.addClass(this.turboCache_n, "Disabled"),
							T.removeClass(this.brightness_n, "Disabled"),
							T.removeClass(this.contrast_n, "Disabled"),
							T.removeClass(this.saturation_n, "Disabled"),
							T.removeClass(this.temperature_n, "Disabled"),
							T.removeClass(this.dim_n, "Disabled");
						break;
					case a.Mode.NORMAL:
						T.addClass(this.colors_n, "Disabled"),
							T.addClass(this.images_n, "Disabled"),
							T.addClass(this.builtInDarkTheme_n, "Disabled"),
							T.addClass(this.turboCache_n, "Disabled"),
							T.addClass(this.brightness_n, "Disabled"),
							T.addClass(this.contrast_n, "Disabled"),
							T.addClass(this.saturation_n, "Disabled"),
							T.addClass(this.temperature_n, "Disabled"),
							T.addClass(this.dim_n, "Disabled");
				}
				"application/pdf" === this.contentType &&
					(T.addClass(this.colors_n, "Disabled"),
					T.addClass(this.images_n, "Disabled"),
					T.removeClass(this.builtInDarkTheme_n, "Disabled"),
					T.removeClass(this.turboCache_n, "Disabled"));
			}
		}
		n(371);
		class B {
			constructor(t, e, n, i) {
				(this.min_value = void 0 === n ? 0 : n),
					(this.max_value = void 0 === i ? 100 : i),
					(this.range = this.max_value - this.min_value),
					(this.inverse_range = 1 / this.range),
					(this.container_n = t),
					(this.move_area_n = e),
					(this.dot_n = this.container_n.querySelector(".DARKY_Dot")),
					(this.progress_n = this.container_n.querySelector(".DARKY_Progress")),
					(this.value_n = this.container_n.querySelector(".DARKY_Center span")),
					(this.value = 0),
					(this.translate_x = 0),
					(this.min_translate_x = 0),
					(this.max_translate_x =
						this.container_n.querySelector(".DARKY_Bar").offsetWidth -
						this.dot_n.offsetWidth),
					(this.action = !1),
					this.addListeners();
			}
			addListeners() {
				var t = o.NOT_EXISTS;
				(this.onChangeListener = null),
					(this.onMouseMove = (e) => {
						if (!1 !== this.action) {
							(this.translate_x += e.clientX - t), (t = e.clientX);
							var n = B.clamp(
									this.min_translate_x,
									this.translate_x,
									this.max_translate_x
								),
								i = ((n / this.max_translate_x) * this.range) | 0;
							(this.progress_n.style.transform =
								"scaleX(" + i * this.inverse_range + ")"),
								(this.dot_n.style.transform = "translateX(" + n + "px)"),
								this.setValue(i, !1),
								null !== this.onChangeListener && this.onChangeListener(i);
						}
					}),
					(this.onMouseLeave = () => {
						this.action = !1;
					}),
					(this.onMouseUp = () => {
						this.action = !1;
					}),
					(this.onMouseDown = (e) => {
						(this.action = !0),
							(this.translate_x = B.clamp(
								this.min_translate_x,
								this.translate_x,
								this.max_translate_x
							)),
							(t = e.clientX);
					}),
					(this.onDotClick = (t) => {
						t.stopPropagation();
					}),
					(this.onContainerClick = (t) => {
						var e =
							(((t.offsetX - 0.5 * this.dot_n.offsetWidth) /
								this.max_translate_x) *
								this.range) |
							0;
						(e = B.clamp(0, e, this.range)),
							this.setValue(e),
							null !== this.onChangeListener && this.onChangeListener(e);
					}),
					this.move_area_n.addEventListener("mousemove", this.onMouseMove),
					this.move_area_n.addEventListener("mouseleave", this.onMouseLeave),
					this.move_area_n.addEventListener("mouseup", this.onMouseUp),
					this.dot_n.addEventListener("mousedown", this.onMouseDown),
					this.dot_n.addEventListener("click", this.onDotClick),
					this.container_n
						.querySelector(".DARKY_Bar")
						.addEventListener("click", this.onContainerClick);
			}
			removeListeners() {
				this.move_area_n.removeEventListener("mousemove", this.onMouseMove),
					this.move_area_n.removeEventListener("mouseleave", this.onMouseLeave),
					this.move_area_n.removeEventListener("mouseup", this.onMouseUp),
					this.dot_n.removeEventListener("mousedown", this.onMouseDown),
					this.dot_n.removeEventListener("click", this.onDotClick),
					this.container_n
						.querySelector(".DARKY_Bar")
						.removeEventListener("click", this.onContainerClick);
			}
			setValue(t, e) {
				(this.value = t),
					null !== this.value_n && (this.value_n.innerHTML = this.value),
					!1 !== e &&
						((this.translate_x =
							this.value * this.inverse_range * this.max_translate_x),
						(this.progress_n.style.transform =
							"scaleX(" + this.value * this.inverse_range + ")"),
						(this.dot_n.style.transform =
							"translateX(" + this.translate_x + "px)"));
			}
			static clamp(t, e, n) {
				return Math.max(t, Math.min(n, e));
			}
		}
		var U = B;
		class F {
			constructor(t, e) {
				(this.url = t),
					(this.contentType = e),
					(this.port = null),
					this.initFields();
			}
			initFields() {
				(this.container_n = document.querySelector(".SettingsBrightnessView")),
					(this.slider = new U(
						this.container_n.querySelector(".DARKY_Slider"),
						this.container_n
					)),
					(this.slider.onChangeListener = (t) => {
						this.sendMessage(t);
					});
			}
			sendMessage(t) {
				this.port.postMessage({ property: "brightness", value: t });
			}
			setValues(t, e, n) {
				(this.default_v = t),
					(this.local_v = e),
					(this.global_v = n),
					this.slider.setValue(this.local_v);
			}
			cancelListener(t) {
				this.slider.setValue(this.local_v), this.sendMessage(this.local_v), t();
			}
			applyToLocalListener(t, e) {
				(this.local_v = this.slider.value),
					t.setBrightnessState(this.default_v !== this.local_v),
					this.save(this.local_v, null, e);
			}
			applyToGlobalListener(t, e) {
				(this.local_v = this.global_v = this.slider.value),
					t.setBrightnessState(this.default_v !== this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsGlobalListener(t, e) {
				(this.local_v = this.global_v = this.default_v),
					t.setBrightnessState(this.default_v !== this.local_v),
					this.slider.setValue(this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsLocalListener(t, e) {
				(this.local_v = this.default_v),
					t.setBrightnessState(this.default_v !== this.local_v),
					this.slider.setValue(this.local_v),
					this.save(this.local_v, null, e);
			}
			save(t, e, n) {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_UPDATE_BRIGHTNESS,
					{
						url: this.url,
						contentType: this.contentType,
						local_v: t,
						global_v: e,
					},
					n
				);
			}
			setVisibility(t) {
				t
					? T.addClass(this.container_n, "Visible")
					: T.removeClass(this.container_n, "Visible");
			}
		}
		class q {
			static makeHSLColorString(t) {
				return "hsl(" + t[0] + "," + t[1] + "%," + t[2] + "%)";
			}
			static makeColorKeyFromArray(t) {
				return (t[0] << 16) | (t[1] << 8) | t[2];
			}
			static makeHSLColorArrayFromKey(t) {
				return [(t >> 16) & 255, (t >> 8) & 255, 255 & t];
			}
			static forceRGBGray(t) {
				const e = Math.min(t[0], Math.min(t[1], t[2])),
					n = Math.max(t[0], Math.max(t[1], t[2]));
				if (n - e > Math.round(8 + (4 * (255 - e)) / 255)) return;
				const i = Math.round(0.5 * (n - e));
				t[0] = t[1] = t[2] = e + i;
			}
			static RGBtoHSL(t) {
				q.forceRGBGray(t);
				var e,
					n,
					i,
					r = t[0] / 255,
					s = t[1] / 255,
					o = t[2] / 255,
					a = Math.max(r, Math.max(s, o)),
					l = Math.min(r, Math.min(s, o)),
					c = a - l;
				(i = 0.5 * (a + l)),
					0 === c
						? ((e = 0), (n = 0))
						: ((e =
								a === r
									? 60 * q.modFloat((s - o) / c, 6)
									: a === s
									? 60 * ((o - r) / c + 2)
									: 60 * ((r - s) / c + 4)),
						  (n = c / (1 - Math.abs(2 * i - 1)))),
					(t[0] = Math.round(e)),
					(t[1] = Math.round(100 * n)),
					(t[2] = Math.round(100 * i));
			}
			static HSLtoRGB(t) {
				t[0] %= 360;
				var e = 0.01 * t[1],
					n = 0.01 * t[2],
					i = (1 - Math.abs(2 * n - 1)) * e,
					r = i * (1 - Math.abs(q.modFloat(t[0] / 60, 2) - 1)),
					s = n - 0.5 * i;
				t[0] < 180
					? t[0] < 60
						? ((t[0] = i), (t[1] = r), (t[2] = 0))
						: t[0] < 120
						? ((t[0] = r), (t[1] = i), (t[2] = 0))
						: ((t[0] = 0), (t[1] = i), (t[2] = r))
					: t[0] < 240
					? ((t[0] = 0), (t[1] = r), (t[2] = i))
					: t[0] < 300
					? ((t[0] = r), (t[1] = 0), (t[2] = i))
					: ((t[0] = i), (t[1] = 0), (t[2] = r)),
					(t[0] = Math.round(255 * (t[0] + s))),
					(t[1] = Math.round(255 * (t[1] + s))),
					(t[2] = Math.round(255 * (t[2] + s)));
			}
			static modFloat(t, e) {
				var n = t / e;
				return (n -= Math.floor(n)) * e;
			}
		}
		var H = q;
		n(372);
		class j {
			constructor(t, e, n, i, r) {
				(this.url = t),
					(this.contentType = e),
					(this.container_n = n),
					(this.colors_original = i),
					(this.colors_replaced = r),
					(this.invoke_node = null),
					this.initFields(),
					this.initPreview(),
					this.initColors();
			}
			initFields() {
				(this.preview_replaced_n = this.container_n.querySelector(
					".DARKY_Preview .DARKY_Box.DARKY_Replaced"
				)),
					(this.preview_original_n = this.container_n.querySelector(
						".DARKY_Preview .DARKY_Box.DARKY_Original"
					)),
					(this.slider_hue = new U(
						this.container_n.querySelector(".DARKY_Slider.DARKY_Hue"),
						this.container_n,
						0,
						360
					)),
					(this.slider_hue.onChangeListener = (t) => {
						this.updateColors();
					}),
					(this.slider_saturation = new U(
						this.container_n.querySelector(".DARKY_Slider.DARKY_Saturation"),
						this.container_n
					)),
					(this.slider_saturation.onChangeListener = (t) => {
						this.updateColors();
					}),
					(this.slider_lightness = new U(
						this.container_n.querySelector(".DARKY_Slider.DARKY_Lightness"),
						this.container_n
					)),
					(this.slider_lightness.onChangeListener = (t) => {
						this.updateColors();
					});
			}
			removeSlidersListeners() {
				this.slider_hue.removeListeners(),
					this.slider_saturation.removeListeners(),
					this.slider_lightness.removeListeners();
			}
			initPreview() {
				(this.preview_replaced_n.style.backgroundColor = H.makeHSLColorString(
					this.colors_replaced
				)),
					(this.preview_original_n.style.backgroundColor = H.makeHSLColorString(
						this.colors_original
					));
			}
			initColors() {
				this.slider_hue.setValue(this.colors_replaced[0]),
					this.slider_saturation.setValue(this.colors_replaced[1]),
					this.slider_lightness.setValue(this.colors_replaced[2]),
					this.updateColorLayout();
			}
			updateColors() {
				this.updateColorData(), this.updateColorLayout();
			}
			updateColorData() {
				var t = [
					this.slider_hue.value,
					this.slider_saturation.value,
					this.slider_lightness.value,
				];
				H.HSLtoRGB(t),
					(this.colors_replaced[0] = this.slider_hue.value),
					(this.colors_replaced[1] = this.slider_saturation.value),
					(this.colors_replaced[2] = this.slider_lightness.value);
			}
			updateColorLayout() {
				var t = this.slider_saturation.container_n.querySelector(
						".DARKY_Bar > .DARKY_Before"
					),
					e = this.slider_lightness.container_n.querySelector(
						".DARKY_Bar > .DARKY_Before"
					);
				(t.style.background =
					"linear-gradient(to right, hsl(" +
					this.colors_replaced[0] +
					", 0%, " +
					this.colors_replaced[2] +
					"%), hsl(" +
					this.colors_replaced[0] +
					", 100%, " +
					this.colors_replaced[2] +
					"%))"),
					(e.style.background =
						"linear-gradient(to right, hsl(" +
						this.colors_replaced[0] +
						", " +
						this.colors_replaced[1] +
						"%, 0%), hsl(" +
						this.colors_replaced[0] +
						", " +
						this.colors_replaced[1] +
						"%, 50%), hsl(" +
						this.colors_replaced[0] +
						", " +
						this.colors_replaced[1] +
						"%, 100%))"),
					(this.preview_replaced_n.style.backgroundColor =
						"hsl(" +
						this.colors_replaced[0] +
						", " +
						this.colors_replaced[1] +
						"%, " +
						this.colors_replaced[2] +
						"%)");
			}
			cancelListener(t) {
				this.removeSlidersListeners();
			}
			applyToLocalListener(t) {
				this.save(!1, !1, t);
			}
			applyToGlobalListener(t) {
				this.save(!0, !1, t);
			}
			defaultsGlobalListener(t) {
				this.save(!0, !0, t);
			}
			defaultsLocalListener(t) {
				this.save(!1, !0, t);
			}
			save(t, e, n) {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_UPDATE_COLOR,
					{
						url: this.url,
						global: t,
						defaults: e,
						color: this.colors_replaced,
						key: H.makeColorKeyFromArray(this.colors_original),
					},
					() => {
						!0 === e && (this.colors_replaced = null),
							this.removeSlidersListeners(),
							n();
					}
				);
			}
			setVisibility(t) {
				t
					? CSS.addClass(this.container_n, "Visible")
					: CSS.removeClass(this.container_n, "Visible");
			}
		}
		n(373);
		class W {
			constructor(t) {
				(this.page = t), this.initFields();
			}
			initFields() {
				(this.container_n = document.querySelector(".SettingsColorsView")),
					(this.palette_n = this.container_n.querySelector(".Palette")),
					(this.color_picker = null),
					(this.color_picker_n = document.querySelector(
						".DARKY_SettingsColorPickerView"
					)),
					(this.hsl_color_map = null),
					(this.local_hsl_color_map = null),
					(this.page_hsl_color_map = null),
					(this.elementPickerButton = this.container_n.querySelector(
						"#elementPickerButton"
					)),
					(this.elementPickerButton.onclick = () => {
						l.send(
							l.Settings.ID,
							l.Settings.ACTION_ACTIVATE_COLOR_SELECTOR,
							{},
							() => {
								window.close();
							}
						);
					});
			}
			sendMessage() {
				this.port.postMessage({ property: "load_page_hsl_colors" });
			}
			setValues(t) {
				(this.local_hsl_color_map = t), this.mergeAndInvalidatePalettes();
			}
			mergeAndInvalidatePalettes() {
				if (
					null !== this.local_hsl_color_map &&
					null !== this.page_hsl_color_map
				) {
					for (var t in ((this.hsl_color_map = {}), this.page_hsl_color_map)) {
						var e = this.local_hsl_color_map[t];
						(e = void 0 === e ? H.makeHSLColorArrayFromKey(t) : e),
							(this.hsl_color_map[t] = e);
					}
					this.makePalette();
				}
			}
			makePalette() {
				var t = (t) => () => {
					var e = H.makeHSLColorArrayFromKey(t),
						n = JSON.parse(JSON.stringify(this.hsl_color_map[t]));
					this.setVisibility(!1),
						(this.color_picker = new j(
							this.page.url,
							this.color_picker_n,
							e,
							n
						)),
						this.color_picker.setVisibility(!0),
						(this.color_picker.invoke_node = this);
				};
				for (var e in ((this.palette_n.innerHTML = s.EMPTY),
				this.hsl_color_map)) {
					var n = H.makeHSLColorArrayFromKey(e),
						i = this.hsl_color_map[e];
					if (this.isColorChanged(n, i)) {
						var r = document.createElement("div");
						this.setNodeColors(r, n, i),
							r.addEventListener("click", t(e)),
							this.palette_n.appendChild(r);
					}
				}
			}
			setNodeColors(t, e, n) {
				null !== n
					? ((t.style.backgroundColor = H.makeHSLColorString(n)),
					  (t.innerHTML =
							'<span style="background-color:' +
							H.makeHSLColorString(e) +
							'"></span>'))
					: t.parentNode.removeChild(t);
			}
			isColorChanged(t, e) {
				for (var n = 0; n < t.length; ++n) if (t[n] !== e[n]) return !0;
				return !1;
			}
			updatePalette() {
				var t = H.makeColorKeyFromArray(this.color_picker.colors_original);
				null === this.color_picker.colors_replaced
					? ((this.hsl_color_map[t] = this.color_picker.colors_original),
					  delete this.local_hsl_color_map[t])
					: ((this.hsl_color_map[t] = this.color_picker.colors_replaced),
					  (this.local_hsl_color_map[t] = this.color_picker.colors_replaced)),
					this.setNodeColors(
						this.color_picker.invoke_node,
						this.color_picker.colors_original,
						this.color_picker.colors_replaced
					);
			}
			clearLocalPalette() {
				for (var t in ((this.local_hsl_color_map = {}),
				(this.hsl_color_map = {}),
				this.page_hsl_color_map))
					this.hsl_color_map[t] = H.makeHSLColorArrayFromKey(t);
			}
			closePicker() {
				this.color_picker.setVisibility(!1),
					(this.color_picker = null),
					this.setVisibility(!0);
			}
			cancelListener(t) {
				if (null !== this.color_picker)
					return this.color_picker.cancelListener(), void this.closePicker();
				t();
			}
			applyToLocalListener(t, e) {
				null !== this.color_picker
					? this.color_picker.applyToLocalListener((n) => {
							this.updatePalette(),
								this.closePicker(),
								t.setColorsState(!0 !== A.isMapEmpty(this.local_hsl_color_map)),
								e();
					  })
					: e();
			}
			applyToGlobalListener(t, e) {
				null !== this.color_picker
					? this.color_picker.applyToGlobalListener((n) => {
							this.updatePalette(),
								this.closePicker(),
								t.setColorsState(!0 !== A.isMapEmpty(this.local_hsl_color_map)),
								e();
					  })
					: e();
			}
			defaultsGlobalListener(t, e) {
				null !== this.color_picker
					? this.color_picker.defaultsGlobalListener((n) => {
							this.updatePalette(),
								this.closePicker(),
								t.setColorsState(!0 !== A.isMapEmpty(this.local_hsl_color_map)),
								e();
					  })
					: this.save(!0, () => {
							this.clearLocalPalette(), t.setColorsState(!1), e();
					  });
			}
			defaultsLocalListener(t, e) {
				null !== this.color_picker
					? this.color_picker.defaultsLocalListener((n) => {
							this.updatePalette(),
								this.closePicker(),
								t.setColorsState(!0 !== A.isMapEmpty(this.local_hsl_color_map)),
								e();
					  })
					: this.save(!1, () => {
							this.clearLocalPalette(), t.setColorsState(!1), e();
					  });
			}
			save(t, e) {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_RESTORE_COLOR_DEFAULTS,
					{ url: this.page.url, global: t },
					() => {
						(this.palette_n.innerHTML = s.EMPTY), e();
					}
				);
			}
			setVisibility(t) {
				t
					? (T.addClass(this.container_n, "Visible"),
					  this.page.controls_c.setApplyControlVisibility(!1))
					: (T.removeClass(this.container_n, "Visible"),
					  this.page.controls_c.setApplyControlVisibility(!0));
			}
			loadPageColors() {
				this.sendMessage();
			}
			onLoadPageColors(t) {
				(this.page_hsl_color_map = t), this.mergeAndInvalidatePalettes();
			}
		}
		class G {
			constructor(t, e) {
				(this.url = t),
					(this.contentType = e),
					(this.port = null),
					this.initFields();
			}
			initFields() {
				(this.container_n = document.querySelector(".SettingsContrastView")),
					(this.slider = new U(
						this.container_n.querySelector(".DARKY_Slider"),
						this.container_n
					)),
					(this.slider.onChangeListener = (t) => {
						this.sendMessage(t);
					});
			}
			sendMessage(t) {
				this.port.postMessage({ property: "contrast", value: t });
			}
			setValues(t, e, n) {
				(this.default_v = t),
					(this.local_v = e),
					(this.global_v = n),
					this.slider.setValue(this.local_v);
			}
			cancelListener(t) {
				this.slider.setValue(this.local_v), this.sendMessage(this.local_v), t();
			}
			applyToLocalListener(t, e) {
				(this.local_v = this.slider.value),
					t.setContrastState(this.default_v !== this.local_v),
					this.save(this.local_v, null, e);
			}
			applyToGlobalListener(t, e) {
				(this.local_v = this.global_v = this.slider.value),
					t.setContrastState(this.default_v !== this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsGlobalListener(t, e) {
				(this.local_v = this.global_v = this.default_v),
					t.setContrastState(this.default_v !== this.local_v),
					this.slider.setValue(this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsLocalListener(t, e) {
				(this.local_v = this.default_v),
					t.setContrastState(this.default_v !== this.local_v),
					this.slider.setValue(this.local_v),
					this.save(this.local_v, null, e);
			}
			save(t, e, n) {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_UPDATE_CONTRAST,
					{
						url: this.url,
						contentType: this.contentType,
						local_v: t,
						global_v: e,
					},
					n
				);
			}
			setVisibility(t) {
				t
					? T.addClass(this.container_n, "Visible")
					: T.removeClass(this.container_n, "Visible");
			}
		}
		class Y {
			constructor(t, e) {
				(this.url = t),
					(this.contentType = e),
					(this.port = null),
					this.initFields();
			}
			initFields() {
				(this.container_n = document.querySelector(".SettingsDimView")),
					(this.slider = new U(
						this.container_n.querySelector(".DARKY_Slider"),
						this.container_n
					)),
					(this.slider.onChangeListener = (t) => {
						this.sendMessage(t);
					});
			}
			sendMessage(t) {
				this.port.postMessage({ property: "dim", value: t });
			}
			setValues(t, e, n) {
				(this.default_v = t),
					(this.local_v = e),
					(this.global_v = n),
					this.slider.setValue(this.local_v);
			}
			cancelListener(t) {
				this.slider.setValue(this.local_v), this.sendMessage(this.local_v), t();
			}
			applyToLocalListener(t, e) {
				(this.local_v = this.slider.value),
					t.setDimState(this.default_v !== this.local_v),
					this.save(this.local_v, null, e);
			}
			applyToGlobalListener(t, e) {
				(this.local_v = this.global_v = this.slider.value),
					t.setDimState(this.default_v !== this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsGlobalListener(t, e) {
				(this.local_v = this.global_v = this.default_v),
					t.setDimState(this.default_v !== this.local_v),
					this.slider.setValue(this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsLocalListener(t, e) {
				(this.local_v = this.default_v),
					t.setDimState(this.default_v !== this.local_v),
					this.slider.setValue(this.local_v),
					this.save(this.local_v, null, e);
			}
			save(t, e, n) {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_UPDATE_DIM,
					{
						url: this.url,
						contentType: this.contentType,
						local_v: t,
						global_v: e,
					},
					n
				);
			}
			setVisibility(t) {
				t
					? T.addClass(this.container_n, "Visible")
					: T.removeClass(this.container_n, "Visible");
			}
		}
		n(374);
		class K {
			constructor(t) {
				(this.container_n = t),
					(this.enabled = !1),
					this.container_n.addEventListener("click", () => {
						this.setState(!this.enabled);
					});
			}
			setState(t) {
				(this.enabled = t),
					t
						? T.addClass(this.container_n, "On")
						: T.removeClass(this.container_n, "On");
			}
		}
		class z {
			constructor(t, e) {
				(this.url = t), (this.contentType = e), this.initFields();
			}
			initFields() {
				(this.container_n = document.querySelector(".SettingsImagesView")),
					(this.switch = new K(this.container_n.querySelector(".Switch")));
			}
			setValues(t, e, n) {
				(this.default_v = t),
					(this.local_v = e),
					(this.global_v = n),
					this.switch.setState(this.local_v);
			}
			cancelListener(t) {
				this.switch.setState(this.local_v), t();
			}
			applyToLocalListener(t, e) {
				(this.local_v = this.switch.enabled),
					t.setImagesState(this.local_v),
					this.save(this.local_v, null, e);
			}
			applyToGlobalListener(t, e) {
				(this.local_v = this.global_v = this.switch.enabled),
					t.setImagesState(this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsGlobalListener(t, e) {
				(this.local_v = this.global_v = this.default_v),
					t.setImagesState(this.local_v),
					this.switch.setState(this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsLocalListener(t, e) {
				(this.local_v = this.default_v),
					t.setImagesState(this.local_v),
					this.switch.setState(this.local_v),
					this.save(this.local_v, null, e);
			}
			save(t, e, n) {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_UPDATE_IMAGES,
					{
						url: this.url,
						contentType: this.contentType,
						local_v: t,
						global_v: e,
					},
					n
				);
			}
			setVisibility(t) {
				t
					? T.addClass(this.container_n, "Visible")
					: T.removeClass(this.container_n, "Visible");
			}
		}
		class X {
			constructor(t, e) {
				(this.url = t),
					(this.contentType = e),
					(this.isAllow = c.isBuiltInDarkThemeExists(o.URL)),
					this.initFields();
			}
			initFields() {
				(this.container_n = document.querySelector(
					".SettingsBuiltInDarkThemeView"
				)),
					(this.select = this.container_n.querySelector(".BuiltInModeSelect")),
					(this.websiteListPanel = this.container_n.querySelector(
						".WebsitesListPanel"
					)),
					this.fillWebsites();
			}
			setValues(t, e, n) {
				(this.default_v = t),
					(this.local_v = e),
					(this.global_v = n),
					this.setState(this.local_v);
			}
			cancelListener(t) {
				this.setState(this.local_v), t();
			}
			applyToLocalListener(t, e) {
				(this.local_v = this.select.value),
					t.setBuiltInDarkThemeState(this.local_v),
					this.save(this.local_v, null, e);
			}
			applyToGlobalListener(t, e) {
				(this.local_v = this.global_v = this.select.value),
					t.setBuiltInDarkThemeState(this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsGlobalListener(t, e) {
				(this.local_v = this.global_v = this.default_v),
					t.setBuiltInDarkThemeState(this.local_v),
					this.switch.setState(this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsLocalListener(t, e) {
				(this.local_v = this.default_v),
					t.setBuiltInDarkThemeState(this.local_v),
					this.switch.setState(this.local_v),
					this.save(this.local_v, null, e);
			}
			save(t, e, n) {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_UPDATE_BUILT_IN_THEME_BEHAVIOUR_MODE,
					{
						url: this.url,
						contentType: this.contentType,
						local_v: t,
						global_v: e,
					},
					n
				);
			}
			setState(t) {
				switch (
					((this.select.innerHTML =
						'        <option value="' +
						c.BuiltInBehaviourMode.DISABLED +
						'">Disabled</option>        <option value="' +
						c.BuiltInBehaviourMode.CONVERT +
						'">Night Eye dark</option>        <option value="' +
						c.BuiltInBehaviourMode.INTEGRATED +
						'">Deep integration</option>'),
					parseInt(t))
				) {
					case c.BuiltInBehaviourMode.DISABLED:
						this.select.selectedIndex = 0;
						break;
					case c.BuiltInBehaviourMode.CONVERT:
						this.select.selectedIndex = 1;
						break;
					case c.BuiltInBehaviourMode.INTEGRATED:
						this.select.selectedIndex = 2;
						break;
					default:
						console.error(`Not implemeted mode with number: ${t}`);
				}
			}
			setVisibility(t) {
				t
					? (T.addClass(this.container_n, "Visible"),
					  this.isAllow || this.hideButtons())
					: (T.removeClass(this.container_n, "Visible"), this.showButtons());
			}
			hideButtons() {
				T.addClass(this.select, "Hidden"),
					T.addClass(document.querySelector("footer"), "HideControls");
			}
			showButtons() {
				T.removeClass(document.querySelector("footer"), "HideControls");
			}
			fillWebsites() {
				var t = "",
					e = c.BuiltInWebsites;
				for (var n in e) {
					t += "<div>" + e[n] + "</div>";
				}
				this.websiteListPanel.innerHTML = t;
			}
		}
		class $ {
			constructor(t, e) {
				(this.url = t), (this.contentType = e), this.initFields();
			}
			initFields() {
				(this.container_n = document.querySelector(".SettingsTurboCacheView")),
					(this.switch = new K(this.container_n.querySelector(".Switch")));
			}
			setValues(t, e, n) {
				(this.default_v = t),
					(this.local_v = e),
					(this.global_v = n),
					this.switch.setState(this.local_v);
			}
			cancelListener(t) {
				this.switch.setState(this.local_v), t();
			}
			applyToLocalListener(t, e) {
				(this.local_v = this.switch.enabled),
					t.setTurboCacheState(this.local_v),
					this.save(this.local_v, null, e);
			}
			applyToGlobalListener(t, e) {
				(this.local_v = this.global_v = this.switch.enabled),
					t.setTurboCacheState(this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsGlobalListener(t, e) {
				(this.local_v = this.global_v = this.default_v),
					t.setTurboCacheState(this.local_v),
					this.switch.setState(this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsLocalListener(t, e) {
				(this.local_v = this.default_v),
					t.setTurboCacheState(this.local_v),
					this.switch.setState(this.local_v),
					this.save(this.local_v, null, e);
			}
			save(t, e, n) {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_UPDATE_TURBO_CACHE,
					{
						url: this.url,
						contentType: this.contentType,
						local_v: t,
						global_v: e,
					},
					n
				);
			}
			setVisibility(t) {
				t
					? T.addClass(this.container_n, "Visible")
					: T.removeClass(this.container_n, "Visible");
			}
		}
		class J {
			constructor(t, e) {
				(this.url = t),
					(this.contentType = e),
					(this.port = null),
					this.initFields();
			}
			initFields() {
				(this.container_n = document.querySelector(".SettingsSaturationView")),
					(this.slider = new U(
						this.container_n.querySelector(".DARKY_Slider"),
						this.container_n
					)),
					(this.slider.onChangeListener = (t) => {
						this.sendMessage(t);
					});
			}
			sendMessage(t) {
				this.port.postMessage({ property: "saturation", value: t });
			}
			setValues(t, e, n) {
				(this.default_v = t),
					(this.local_v = e),
					(this.global_v = n),
					this.slider.setValue(this.local_v);
			}
			cancelListener(t) {
				this.slider.setValue(this.local_v), this.sendMessage(this.local_v), t();
			}
			applyToLocalListener(t, e) {
				(this.local_v = this.slider.value),
					t.setSaturationState(this.default_v !== this.local_v),
					this.save(this.local_v, null, e);
			}
			applyToGlobalListener(t, e) {
				(this.local_v = this.global_v = this.slider.value),
					t.setSaturationState(this.default_v !== this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsGlobalListener(t, e) {
				(this.local_v = this.global_v = this.default_v),
					t.setSaturationState(this.default_v !== this.local_v),
					this.slider.setValue(this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsLocalListener(t, e) {
				(this.local_v = this.default_v),
					t.setSaturationState(this.default_v !== this.local_v),
					this.slider.setValue(this.local_v),
					this.save(this.local_v, null, e);
			}
			save(t, e, n) {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_UPDATE_SATURATION,
					{
						url: this.url,
						contentType: this.contentType,
						local_v: t,
						global_v: e,
					},
					n
				);
			}
			setVisibility(t) {
				t
					? T.addClass(this.container_n, "Visible")
					: T.removeClass(this.container_n, "Visible");
			}
		}
		class Z {
			constructor(t, e) {
				(this.url = t),
					(this.contentType = e),
					(this.port = null),
					this.initFields();
			}
			initFields() {
				(this.container_n = document.querySelector(".SettingsTemperatureView")),
					(this.slider = new U(
						this.container_n.querySelector(".DARKY_Slider"),
						this.container_n
					)),
					(this.slider.onChangeListener = (t) => {
						this.sendMessage(t);
					});
			}
			sendMessage(t) {
				this.port.postMessage({ property: "temperature", value: t });
			}
			setValues(t, e, n) {
				(this.default_v = t),
					(this.local_v = e),
					(this.global_v = n),
					this.slider.setValue(this.local_v);
			}
			cancelListener(t) {
				this.slider.setValue(this.local_v), this.sendMessage(this.local_v), t();
			}
			applyToLocalListener(t, e) {
				(this.local_v = this.slider.value),
					t.setTemperatureState(this.default_v !== this.local_v),
					this.save(this.local_v, null, e);
			}
			applyToGlobalListener(t, e) {
				(this.local_v = this.global_v = this.slider.value),
					t.setTemperatureState(this.default_v !== this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsGlobalListener(t, e) {
				(this.local_v = this.global_v = this.default_v),
					t.setTemperatureState(this.default_v !== this.local_v),
					this.slider.setValue(this.local_v),
					this.save(null, this.global_v, e);
			}
			defaultsLocalListener(t, e) {
				(this.local_v = this.default_v),
					t.setTemperatureState(this.default_v !== this.local_v),
					this.slider.setValue(this.local_v),
					this.save(this.local_v, null, e);
			}
			save(t, e, n) {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_UPDATE_TEMPERATURE,
					{
						url: this.url,
						contentType: this.contentType,
						local_v: t,
						global_v: e,
					},
					n
				);
			}
			setVisibility(t) {
				t
					? T.addClass(this.container_n, "Visible")
					: T.removeClass(this.container_n, "Visible");
			}
		}
		class Q {
			static checkForCorrectField(t, e, n) {
				var i = t.value;
				return (
					!0 !== n && (i = i.trim()),
					(t.oninput = function () {
						T.removeClass(t, "Invalid");
					}),
					i === s.EMPTY
						? (T.addClass(t, "Invalid"), null)
						: (T.removeClass(t, "Invalid"), i)
				);
			}
			static checkForCorrectEmail(t, e) {
				var n, i;
				return (
					(t.oninput = function () {
						T.removeClass(t, "Invalid");
					}),
					(i = (n = t.value.trim()).match(
						"[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})"
					)),
					n === s.EMPTY || null === i
						? (T.addClass(t, "Invalid"), null)
						: (T.removeClass(t, "Invalid"), n)
				);
			}
			static checkForCorrectPass(t, e, n) {
				var i;
				return (i = Q.checkForCorrectField(t, n, !0)) !==
					Q.checkForCorrectField(e, n, !0) || null === i
					? (T.addClass(t, "Invalid"), T.addClass(e, "Invalid"), null)
					: (T.removeClass(t, "Invalid"), T.removeClass(e, "Invalid"), i);
			}
			static filterPhone(t) {
				if (Q.isSystemKey(t)) return !0;
				var e = String.fromCharCode(t.which);
				return "+" === e
					? this.value === s.EMPTY
					: (e >= "0" && e <= "9") || " " === e;
			}
			static filterNumber(t) {
				if (Q.isSystemKey(t)) return !0;
				var e = String.fromCharCode(t.which);
				return "+" === e || "-" === e
					? this.value === s.EMPTY
					: e >= "0" && e <= "9";
			}
			static filterFloat(t) {
				if (Q.isSystemKey(t)) return !0;
				var e = String.fromCharCode(t.which);
				return "+" === e || "-" === e
					? this.value === s.EMPTY
					: (e >= "0" && e <= "9") ||
							("." === e && -1 === this.value.indexOf("."));
			}
			static filterNumberUnsigned(t) {
				var e = String.fromCharCode(t.which);
				return !!Q.isSystemKey(t) || (e >= "0" && e <= "9");
			}
			static isSystemKey(t) {
				return (
					8 === t.keyCode ||
					46 === t.keyCode ||
					(t.keyCode >= 35 && t.keyCode <= 40) ||
					0 === t.which
				);
			}
			static setSelectValue(t, e) {
				for (var n = t.options.length - 1; n >= 0; --n)
					if (t.options[n].value == e) return void (t.selectedIndex = n);
				t.selectedIndex = -1;
			}
			static checkSelectValueSelected(t, e) {
				if (
					(typeof e === s.UNDEF && (e = !1),
					(t.oninput = function () {
						T.removeClass(t, "Invalid");
					}),
					-1 === t.selectedIndex)
				)
					return e || T.addClass(t, "Invalid"), null;
				var n = t.options[t.selectedIndex].value;
				return T.removeClass(t, "Invalid"), n;
			}
		}
		var tt = Q;
		n(375);
		class et {
			static init() {
				for (
					var t = document.getElementsByClassName("Input"), e = t.length;
					e-- > 0;

				)
					et.initInput(t[e]);
			}
			static onFieldsAdded() {
				for (
					var t = document.getElementsByClassName("Input"), e = t.length;
					e-- > 0;

				)
					null === t[e].getAttribute("init") &&
						(et.initInput(t[e]), et.invalidate(t[e]));
			}
			static signal() {
				for (
					var t = document.getElementsByClassName("Input"), e = t.length;
					e-- > 0;

				)
					et.invalidate(t[e]);
			}
			static invalidate(t) {
				t.value !== s.EMPTY
					? T.addClass(t, "Filled")
					: T.removeClass(t, "Filled");
			}
			static initInput(t) {
				t.addEventListener(
					"SELECT" === t.tagName ? "change" : "input",
					et.onInput
				),
					t.setAttribute("init", s.EMPTY),
					et.invalidate(t);
			}
			static onInput() {
				et.invalidate(this);
			}
		}
		var nt = et;
		class it {
			constructor(t, e, n, i, r, s, o, a) {
				(this.projectId = t),
					(this.customUserId = e),
					(this.email = n),
					(this.name = i),
					(this.imageURL = r),
					(this.customData = s),
					(this.container = o),
					(this.readyCallback = a),
					(this.messageTextWrapper = null),
					(this.previousSenderType = 0),
					(this.previousCreationTS = 0),
					(this.currentDate = 0),
					(this.currentMonth = 0),
					(this.currentYear = 0),
					this.initElementStructure(),
					this.initMessages();
			}
			static hasUnreadMessages(t, e, n) {
				var i = [];
				i.push(it.Params.ACTION + "=" + it.ActionsMessage.HAS_UNREAD_MESSAGES),
					i.push(it.Params.PROJECT_ID + "=" + t),
					i.push(it.Params.ID + "=" + e),
					rt.send(it.Controllers.MESSAGE, i, (t) => {
						var e = JSON.parse(t);
						if (e.status) {
							var i = "1" === e.data;
							n(i);
						}
					});
			}
			sendMessage(t) {
				this.showSendLoading();
				var e = this.createMessageText(null, t);
				this.scrollToBottom();
				var n = [];
				n.push(it.Params.ACTION + "=" + it.ActionsMessage.SEND),
					n.push(it.Params.PROJECT_ID + "=" + this.projectId),
					n.push(it.Params.ID + "=" + this.customUserId),
					n.push(it.Params.EMAIL + "=" + this.email),
					n.push(it.Params.NAME + "=" + this.name),
					n.push(it.Params.IMAGE_URL + "=" + this.imageURL),
					n.push(it.Params.DATA + "=" + this.customData),
					n.push(it.Params.VALUE + "=" + t),
					rt.send(it.Controllers.MESSAGE, n, (t) => {
						var n = JSON.parse(t);
						n.status &&
							(this.UI_remove(e),
							this.hideSendLoading(),
							this.checkAndAddMessage(n.data),
							this.scrollToBottom());
					});
			}
			initMessages() {
				var t = [];
				t.push(it.Params.ACTION + "=" + it.ActionsMessage.GET_ALL),
					t.push(it.Params.PROJECT_ID + "=" + this.projectId),
					t.push(it.Params.ID + "=" + this.customUserId),
					rt.send(it.Controllers.MESSAGE, t, (t) => {
						var e = JSON.parse(t);
						e.status && this.fillMessages(e.data);
					});
			}
			initElementStructure() {
				(this.messageListBox = document.createElement("div")),
					this.container.appendChild(this.messageListBox),
					(this.messageListBox.className = "MessageListBox");
				var t = document.createElement("div");
				this.container.appendChild(t), (t.className = "Footer");
				var e = (t) => {
						var e = t.value.trim();
						(t.value = ""),
							"" !== e &&
								(this.sendMessage(e),
								this.showSendLoading(),
								this.scrollToBottom());
					},
					n = document.createElement("input");
				t.appendChild(n),
					(n.type = "text"),
					n.setAttribute("placeholder", "Type a message"),
					n.focus(),
					n.addEventListener("keyup", (t) => {
						switch (t.keyCode) {
							case 13:
								e(n);
						}
						t.preventDefault();
					});
				var i = document.createElement("i");
				t.appendChild(i),
					(i.className = "material-icons"),
					(i.innerHTML =
						'<svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M2.01 21L23 12 2.01 3 2 10l15 2-15 2z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'),
					i.addEventListener("click", () => {
						e(n);
					});
			}
			fillMessages(t) {
				if ((this.UI_removeChildren(this.messageListBox), 0 === t.length)) {
					let t = {
						operatorName: "Support Team",
						text:
							'When reporting a bug, please send us a screenshot of the issue using services like <a href="https://pasteboard.co" target="_blank">https://pasteboard.co</a> and the URL (if such) of the problematic page. To be able to fix a bug, we need to inspect and reproduce it.',
					};
					var e = this.createMessageItem(t);
					if (
						(this.createMessageText(e, t.text),
						window.navigator.userAgent.indexOf("Edge") > -1)
					) {
						let t = {
								operatorName: "Support Team",
								text:
									'For payment and activation related questions, check: <a href="https://billing.nighteye.app/client/login" target="_blank">https://billing.nighteye.app</a>',
							},
							e = this.createMessageItem(t);
						this.createMessageText(e, t.text);
					}
				} else
					for (var n = 0; n < t.length; ++n) {
						var i = t[n];
						this.checkAndAddMessage(i);
					}
				this.scrollToBottom(), this.readyCallback();
			}
			checkAndAddMessage(t) {
				var e = 1e3 * parseInt(t.creationTS),
					n = new Date(e);
				if (
					this.currentDate !== n.getDate() ||
					this.currentMonth !== n.getMonth() ||
					this.currentYear !== n.getYear()
				) {
					(this.currentDate = n.getDate()),
						(this.currentMonth = n.getMonth()),
						(this.currentYear = n.getYear());
					var i =
							it.DAYS[n.getDay()] +
							", " +
							(n.getDate() < 10 ? "0" : "") +
							n.getDate() +
							"." +
							(n.getMonth() + 1 < 10 ? "0" : "") +
							(n.getMonth() + 1) +
							"." +
							n.getFullYear(),
						r = document.createElement("div");
					this.messageListBox.appendChild(r),
						(r.className = "DateTime"),
						(r.innerHTML = "<span>" + i + "</span>");
				}
				(parseInt(this.previousSenderType) !== parseInt(t.senderType) ||
					e - this.previousCreationTS > 36e4) &&
					((this.previousCreationTS = e),
					(this.previousSenderType = t.senderType),
					(this.messageTextWrapper = this.createMessageItem(t))),
					this.createMessageText(this.messageTextWrapper, t.text);
			}
			createMessageItem(t) {
				var e = !1,
					n = "Left";
				parseInt(t.senderType) === it.UserType.EXTERNAL_USER &&
					((n = "Right"), (e = !0));
				var i = "";
				if (void 0 !== t.creationTS) {
					var r = new Date(1e3 * parseInt(t.creationTS));
					i =
						", " +
						(r.getHours() < 10 ? "0" : "") +
						r.getHours() +
						":" +
						(r.getMinutes() < 10 ? "0" : "") +
						r.getMinutes();
				}
				var s = document.createElement("div");
				this.messageListBox.appendChild(s),
					(s.className = "MessageItem row " + n);
				var o = document.createElement("div");
				if (
					(s.appendChild(o),
					(o.className = "col"),
					!e && void 0 !== t.operatorImageURL)
				) {
					var a = document.createElement("img");
					o.appendChild(a), (a.src = t.operatorImageURL);
				}
				var l = document.createElement("div");
				s.appendChild(l), (l.className = "col Message");
				var c = document.createElement("div");
				return (
					l.appendChild(c),
					(c.className = "Author"),
					(c.innerHTML = t.operatorName + i),
					l
				);
			}
			createMessageText(t, e) {
				if (((e = decodeURIComponent(e)), null === t)) {
					var n = document.createElement("div");
					this.messageListBox.appendChild(n),
						(n.className = "MessageItem row Right");
					var i = document.createElement("div");
					n.appendChild(i), (i.className = "col");
					var r = document.createElement("div");
					n.appendChild(r), (r.className = "col Message"), (t = r);
				}
				var s = document.createElement("div");
				t.appendChild(s), (s.className = "row");
				var o = document.createElement("div");
				return (
					s.appendChild(o), (o.className = "Text col"), (o.innerHTML = e), t
				);
			}
			scrollToBottom() {
				this.messageListBox.scrollTop = this.messageListBox.scrollHeight;
			}
			showSendLoading() {
				var t = document.createElement("div");
				this.messageListBox.appendChild(t),
					(t.className = "Sending"),
					(t.innerHTML = "sending");
			}
			hideSendLoading() {
				for (
					var t = this.messageListBox.querySelectorAll(".Sending"), e = 0;
					e < t.length;
					++e
				)
					this.UI_remove(t[e]);
			}
			UI_remove(t) {
				t.parentNode.removeChild(t);
			}
			UI_removeChildren(t) {
				if (void 0 !== t)
					for (var e = t.firstChild; e; ) t.removeChild(e), (e = t.firstChild);
			}
		}
		class rt {
			static send(t, e, n) {
				var i = new XMLHttpRequest(),
					r = e.join("&");
				(i.onreadystatechange = () => {
					i.readyState == XMLHttpRequest.DONE &&
						200 == i.status &&
						n(i.responseText);
				}),
					i.open("POST", t, !0),
					i.setRequestHeader(
						"Content-type",
						"application/x-www-form-urlencoded"
					),
					i.send(r);
			}
		}
		(it.Controllers = {
			MESSAGE: "https://smsystem.razorlabs.com/exc/MessageController",
		}),
			(it.Params = {
				ACTION: "ac",
				PROJECT_ID: "pid",
				ID: "id",
				EMAIL: "eml",
				NAME: "nm",
				IMAGE_URL: "imgurl",
				DATA: "dta",
				VALUE: "val",
			}),
			(it.ActionsMessage = {
				SEND: "b",
				HAS_UNREAD_MESSAGES: "d",
				GET_ALL: "f",
			}),
			(it.UserType = { OPERATOR: 1, EXTERNAL_USER: 2 }),
			(it.DAYS = [
				"Sunday",
				"Monday",
				"Tuesday",
				"Wednesday",
				"Thursday",
				"Friday",
				"Saturday",
			]);
		var st = it;
		n(376), n(377);
		class ot {
			constructor() {
				this.initFields(), (this.supportSystem = null);
			}
			initFields() {
				(this.support_trigger_circle_n = document.querySelector(
					".SupportTrigger circle"
				)),
					(this.root_n = document.querySelector(".SupportComponent")),
					(this.chat_n = this.root_n.querySelector(".SupportComponent .Chat")),
					(this.info_n = this.root_n.querySelector(".SupportComponent .Info")),
					(this.name_n = this.info_n.querySelector(
						".SupportComponent #support_name"
					)),
					(this.email_n = this.info_n.querySelector(
						".SupportComponent #support_email"
					)),
					(this.continue_n = this.info_n.querySelector(".Continue")),
					this.continue_n.addEventListener("click", () => {
						var t = tt.checkForCorrectField(this.name_n);
						if (null !== t) {
							var e = tt.checkForCorrectEmail(this.email_n);
							null !== e &&
								((e = encodeURIComponent(e)),
								l.send(
									l.Settings.ID,
									l.Settings.ACTION_UPDATE_CHAT_INFO,
									{ name: t, email: e },
									() => {
										this.openChatWindow();
									}
								));
						}
					});
			}
			show() {
				(this.chat_n.innerHTML = s.EMPTY),
					l.send(l.Settings.ID, l.Settings.ACTION_ON_CHAT_INFO, null, (t) => {
						(this.name_n.value = t.name),
							(this.email_n.value = t.email),
							(this.chat_id = t.id),
							(this.continue_n.innerHTML = "Continue"),
							T.removeClass(this.info_n, "PleaseWait"),
							nt.signal(),
							T.addClass(this.info_n, "Active"),
							T.addClass(this.root_n, "Active"),
							this.name_n.value !== s.EMPTY && this.openChatWindow();
					});
			}
			hide() {
				T.removeClass(this.root_n, "Active");
			}
			async openChatWindow() {
				var t;
				T.addClass(this.info_n, "PleaseWait"),
					(this.continue_n.innerHTML = "Please wait..."),
					(t = localStorage.getItem("instance_id")),
					(this.supportSystem = new st(
						"CU_Ivze_DH9u7QnUOivoMA",
						this.chat_id,
						this.email_n.value,
						this.name_n.value,
						s.EMPTY,
						t,
						this.chat_n,
						() => {
							T.removeClass(this.info_n, "Active"),
								this.invalidateMessagesIcons(!1);
						}
					));
			}
			checkForUnreadMessages(t) {
				st.hasUnreadMessages("CU_Ivze_DH9u7QnUOivoMA", t.id, (t) => {
					this.invalidateMessagesIcons(t);
				});
			}
			invalidateMessagesIcons(t) {
				!1 === t
					? (o.browser.browserAction.setIcon({
							path: o.browser.extension.getURL("res/icon/48.png"),
					  }),
					  (this.support_trigger_circle_n.style.opacity = "0"))
					: (o.browser.browserAction.setIcon({
							path: o.browser.extension.getURL("res/icon/48-msg.png"),
					  }),
					  (this.support_trigger_circle_n.style.opacity = "1"));
			}
		}
		class at {
			static init() {
				var t,
					e = "",
					n = navigator.userAgent;
				if (n.indexOf("Edge") >= 0) t = at.EDGE;
				else if (n.indexOf("Firefox") >= 0) t = at.FIREFOX;
				else if (n.indexOf("ivaldi") >= 0) t = at.VIVALDI;
				else if (n.indexOf("YaBrowser") >= 0) t = at.YANDEX;
				else if (n.indexOf("OPR") >= 0) t = at.OPERA;
				else if (n.indexOf("hrome") >= 0) t = at.CHROME;
				else if (n.indexOf("afari") >= 0) t = at.SAFARI;
				else {
					var i = o.NOT_EXISTS;
					if (
						(n.indexOf("MSIE") >= 0
							? (i = n.indexOf("MSIE") + 4)
							: n.indexOf(".NET") >= 0 && (i = n.indexOf("rv:") + 3),
						i !== o.NOT_EXISTS)
					) {
						for (t = at.IE; " " === n.charAt(i); ) ++i;
						do {
							e += n.charAt(i++);
						} while (n.charAt(i) >= "0" && n.charAt(i) <= "9");
					} else t = at.UNKNOWN;
				}
				(at.nameIdentity = t), (at.version = e);
			}
			static legacyCheck() {
				null === at.nameIdentity && at.init(),
					at.nameIdentity === at.IE && at.version < 11 && at.showAlert();
			}
			static showAlert() {
				alert("OLD");
			}
		}
		(at.FIREFOX = "Firefox"),
			(at.OPERA = "Opera"),
			(at.CHROME = "Chrome"),
			(at.SAFARI = "Safari"),
			(at.VIVALDI = "Vivaldi"),
			(at.YANDEX = "Yandex"),
			(at.IE = "IE"),
			(at.EDGE = "EDGE"),
			(at.UNKNOWN = "un"),
			(at.nameIdentity = null),
			(at.version = null);
		var lt = at;
		n(378);
		class ct {
			static init() {
				for (
					var t = document.getElementsByClassName("Ripple"), e = t.length - 1;
					e >= 0;
					--e
				)
					ct.invalidate(t[e]);
			}
			static invalidate(t) {
				null === t.getAttribute("init_r") &&
					(t.setAttribute("init_r", s.TRUE), ct.add(t));
			}
			static add(t) {
				var e;
				(t.addEventListener("mousedown", ct.createOnMouseDown(t)),
				lt.nameIdentity === lt.FIREFOX || lt.nameIdentity === lt.SAFARI) &&
					(t.addEventListener("mousedown", function () {
						e = -1;
					}),
					t.addEventListener("mouseup", function (n) {
						(e = 1),
							requestAnimationFrame(function () {
								requestAnimationFrame(function () {
									1 === e && t.dispatchEvent(new MouseEvent("click", n));
								});
							});
					}),
					t.addEventListener("click", function () {
						e = 0;
					}));
			}
			static createOnMouseDown(t) {
				return function (e) {
					if (0 === e.button) {
						var n,
							i = e.clientX,
							r = e.clientY,
							s = { valid: !0 },
							o =
								t.offsetWidth > t.offsetHeight
									? t.offsetWidth << 1
									: t.offsetHeight << 1;
						(s.node = document.createElement("div")),
							(s.node.className = "RippleNode"),
							null !== t.getAttribute("color") &&
								(s.node.style.backgroundColor = t.getAttribute("color")),
							(s.node.style.left = i - A.getScreenX(t) - (o >> 1) + "px"),
							(s.node.style.top = r - A.getScreenY(t) - (o >> 1) + "px"),
							(s.node.style.width = o + "px"),
							(s.node.style.height = o + "px"),
							t.insertBefore(s.node, t.firstChild),
							requestAnimationFrame(function () {
								s.node.className += " SlowTransform";
							}),
							(n = ct.createOnMouseUp(t, s)),
							t.addEventListener("mouseup", n),
							t.addEventListener("mouseout", n);
					}
				};
			}
			static createOnMouseUp(t, e) {
				var n = function (i) {
					!1 !== e.value &&
						((e.value = !1),
						(e.node.className += " FastTransform"),
						setTimeout(function () {
							requestAnimationFrame(function () {
								(e.node.style.opacity = 0),
									setTimeout(function () {
										e.node.parentNode === t && t.removeChild(e.node);
									}, 250);
							});
						}, 250),
						t.removeEventListener("mouseup", n),
						t.removeEventListener("mouseout", n));
				};
				return n;
			}
		}
		var ut = ct;
		class ht {
			constructor() {
				(this.isShowingCustomInfoLable = !1), this.initFields();
			}
			initFields() {
				var t = document.querySelector(".EnabledView");
				(this.label_n = t.querySelector(".URLLabel")),
					(this.url_n = t.querySelector(".URL")),
					(this.url_status_n = t.querySelector(".URLStatus")),
					(this.dark_control_n = t.querySelector(
						".URLControl > div:nth-child(1)"
					)),
					(this.filtered_control_n = t.querySelector(
						".URLControl > div:nth-child(2)"
					)),
					(this.normal_control_n = t.querySelector(
						".URLControl > div:nth-child(3)"
					)),
					(this.url_control_n = this.dark_control_n.parentNode),
					(this.url = ""),
					(this.darkListener = null),
					(this.filteredListener = null),
					(this.normalListener = null),
					(this.isOSColorSchemeCheckerEnabled = !1);
			}
			addListeners() {
				this.dark_control_n.addEventListener("click", () => {
					this.isOSColorSchemeCheckerEnabled
						? this.showInfoDialogForOScolorIntegration()
						: (this.darkListener(), this.setDarkMode(c.Mode.DARK));
				}),
					this.filtered_control_n.addEventListener("click", () => {
						this.isOSColorSchemeCheckerEnabled
							? this.showInfoDialogForOScolorIntegration()
							: (this.filteredListener(), this.setDarkMode(c.Mode.FILTERED));
					}),
					this.normal_control_n.addEventListener("click", () => {
						this.isOSColorSchemeCheckerEnabled
							? this.showInfoDialogForOScolorIntegration()
							: (this.normalListener(), this.setDarkMode(c.Mode.NORMAL));
					});
			}
			setURL(t) {
				(this.url = t),
					(this.url_n.innerHTML = '<div class="Dots">' + t + "</div>");
			}
			setDarkMode(t) {
				switch (t) {
					case c.Mode.DARK:
						T.addClass(this.dark_control_n, "Active"),
							T.removeClass(this.filtered_control_n, "Active"),
							T.removeClass(this.normal_control_n, "Active");
						break;
					case c.Mode.FILTERED:
						T.removeClass(this.dark_control_n, "Active"),
							T.addClass(this.filtered_control_n, "Active"),
							T.removeClass(this.normal_control_n, "Active");
						break;
					case c.Mode.NORMAL:
						T.removeClass(this.dark_control_n, "Active"),
							T.removeClass(this.filtered_control_n, "Active"),
							T.addClass(this.normal_control_n, "Active");
				}
				this.isShowingCustomInfoLable ||
					this.url_status_n.classList.add("Hidden");
			}
			init(t, e, n, i) {
				if (
					((this.isOSColorSchemeCheckerEnabled = i),
					this.isOSColorSchemeCheckerEnabled)
				) {
					this.isShowingCustomInfoLable = !0;
					var r = document.createElement("div");
					(r.className = "InfoLabel Orange"),
						(r.innerHTML =
							'OS/Browser Color Scheme integration is activated. <a id="browserIntegrationLink" target="_blank"><u>Details</u></a></div>'),
						this.url_status_n.appendChild(r);
					let t = document.getElementById("browserIntegrationLink");
					void 0 !== t &&
						t.addEventListener("click", () => {
							o.browser.tabs.create({
								url: "http://nighteye.app/help#os-integration",
							});
						});
				}
				if (!0 === t)
					this.setDarkMode(e),
						this.addListeners(),
						T.addClass(this.dark_control_n, "Ripple Cursor"),
						T.addClass(this.filtered_control_n, "Ripple Cursor"),
						T.addClass(this.normal_control_n, "Ripple Cursor"),
						ut.invalidate(this.dark_control_n),
						ut.invalidate(this.filtered_control_n),
						ut.invalidate(this.normal_control_n);
				else if ((T.addClass(this.url_status_n, "Orange"), n)) {
					var s = document.createElement("div");
					(s.innerHTML =
						"This website is not added in the dark mode list.<br> <b><u>Manage the list</u></b></b>"),
						this.url_status_n.appendChild(s),
						s.addEventListener("click", function () {
							o.page.trial_expired_c.showLiteVersionView();
						});
				} else
					this.url_status_n.innerHTML = O.getMessage(
						"labelWebsiteStatusNotSupported"
					);
			}
			showInfoDialogForOScolorIntegration() {
				c.showInfoDialog(
					null,
					"OS/Browser Color scheme is active.             You need to disable it in order to manually change the modes.<br><br>             You can disable the integration from Settings -> OS Color Scheme integration."
				);
			}
		}
		var dt = {
				onBeforeLoad: function () {
					dt.prepareCustomEvent();
				},
				onResizeW: function () {},
				onLoad: function () {
					dt.initHover();
				},
				pushEmptyHistoryState: function () {
					window.history.pushState(
						{ empty: null },
						document.title,
						document.URL
					);
				},
				prepareCustomEvent: function () {
					function t(t, e) {
						typeof e === s.UNDEF &&
							(e = { bubbles: !1, cancelable: !1, detail: void 0 }),
							typeof e.bubbles === s.UNDEF && (e.bubbles = !1),
							typeof e.cancelable === s.UNDEF && (e.cancelable = !1);
						var n = document.createEvent("CustomEvent");
						return n.initCustomEvent(t, e.bubbles, e.cancelable, e.detail), n;
					}
					"function" != typeof window.CustomEvent &&
						((t.prototype = window.Event.prototype), (window.CustomEvent = t));
				},
				initHover: function () {
					if (
						0 !== navigator.maxTouchPoints &&
						0 !== navigator.msMaxTouchPoints
					) {
						var t = !1,
							e = function () {
								t = !1;
							};
						document.documentElement.addEventListener(
							"mousemove",
							function (e) {
								!0 !== t &&
									T.removeClass(document.documentElement, "Touchable");
							},
							!0
						),
							document.documentElement.addEventListener(
								"touchstart",
								function (n) {
									(t = !0),
										T.addClass(document.documentElement, "Touchable"),
										setTimeout(e, 200);
								},
								!0
							);
					}
				},
				xorActiveClass: function (t, e) {
					return (
						(e = typeof e === s.UNDEF ? this : e),
						!1 === T.hasClass(e, "Active")
							? (T.addClass(e, "Active"), !0)
							: (T.removeClass(e, "Active"), !1)
					);
				},
				isActive: function (t) {
					return T.hasClass(t, "Active");
				},
			},
			ft = dt;
		n(379), n(380);
		var pt = class {
				constructor() {
					this.screenMetrics = { width: 0, height: 0 };
				}
				onBeforeLoad() {
					lt.init(),
						(this.screenMetrics.width = document.body.offsetWidth),
						(this.screenMetrics.height = document.body.offsetHeight),
						typeof ut !== s.UNDEF && ut.init(),
						typeof nt !== s.UNDEF && nt.init(),
						void 0 !== ft.onBeforeLoad && ft.onBeforeLoad(),
						null !== o.page &&
							void 0 !== o.page.onBeforeLoad &&
							o.page.onBeforeLoad(),
						null !== o.page_cpanel &&
							void 0 !== o.page_cpanel.onBeforeLoad &&
							o.page_cpanel.onBeforeLoad();
				}
				onLoad() {
					void 0 !== ft.onLoad && ft.onLoad(),
						null !== o.page && void 0 !== o.page.onLoad && o.page.onLoad(),
						null !== o.page_cpanel &&
							void 0 !== o.page_cpanel.onLoad &&
							o.page_cpanel.onLoad();
				}
				onResize() {
					var t, e, n, i;
					(o.mobile
						? ((t = window.innerWidth), (e = window.innerHeight))
						: ((t = document.documentElement.clientWidth),
						  (e = document.documentElement.clientHeight)),
					this.screenMetrics.width !== t || this.screenMetrics.height !== e) &&
						((n = this.screenMetrics.width),
						(i = this.screenMetrics.height),
						(this.screenMetrics.width = t),
						(this.screenMetrics.height = e),
						n !== this.screenMetrics.width &&
							(void 0 !== ft.onResizeW && ft.onResizeW(),
							null !== o.page &&
								void 0 !== o.page.onResizeW &&
								o.page.onResizeW(),
							null !== o.page_cpanel &&
								void 0 !== o.page_cpanel.onResizeW &&
								o.page_cpanel.onResizeW()),
						i !== this.screenMetrics.height &&
							(void 0 !== ft.onResizeH && ft.onResizeH(),
							null !== o.page &&
								void 0 !== o.page.onResizeH &&
								o.page.onResizeH(),
							null !== o.page_cpanel &&
								void 0 !== o.page_cpanel.onResizeH &&
								o.page_cpanel.onResizeH()),
						void 0 !== ft.onResize && ft.onResize(),
						null !== o.page && void 0 !== o.page.onResize && o.page.onResize(),
						null !== o.page_cpanel &&
							void 0 !== o.page_cpanel.onResize &&
							o.page_cpanel.onResize(),
						requestAnimationFrame(() => {
							this.onResize();
						}));
				}
				onScroll() {
					void 0 !== ft.onScroll && ft.onScroll(),
						null !== o.page && void 0 !== o.page.onScroll && o.page.onScroll(),
						null !== o.page_cpanel &&
							void 0 !== o.page_cpanel.onScroll &&
							o.page_cpanel.onScroll();
				}
				onPopState(t) {
					(null !== o.page_cpanel &&
						void 0 !== o.page_cpanel.onPopState &&
						o.page_cpanel.onPopState(t)) ||
						(null !== o.page &&
							void 0 !== o.page.onPopState &&
							o.page.onPopState(t)) ||
						(void 0 !== ft.onPopState && ft.onPopState(t)) ||
						(-1 === window.location.href.indexOf("#") && window.history.back());
				}
			},
			gt = new pt();
		(window.onload = function () {
			gt.onLoad();
		}),
			(window.onresize = function () {
				gt.onResize();
			}),
			(window.onscroll = function () {
				gt.onScroll();
			}),
			(window.onpopstate = gt.onPopState),
			gt.onBeforeLoad();
		class vt {
			constructor() {
				(this.data = new Map()),
					(this.behaviour = new pt(this)),
					(this.onDOMContentLoaded = this.onDOMContentLoaded.bind(this)),
					"loading" === document.readyState
						? document.addEventListener(
								"DOMContentLoaded",
								this.onDOMContentLoaded
						  )
						: this.onDOMContentLoaded();
			}
			onDOMContentLoaded() {
				(window.onload = () => {
					this.behaviour.onLoad();
				}),
					(window.onresize = () => {
						this.behaviour.onResize();
					}),
					(window.onscroll = () => {
						this.behaviour.onScroll();
					}),
					(window.onpopstate = () => {
						this.behaviour.onPopState();
					}),
					this.behaviour.onBeforeLoad();
			}
		}
		n(381);
		class _t {
			constructor(t) {
				(this.discountCode = t),
					this.initFields(),
					this.addListeners(),
					this.fillData();
			}
			initFields() {
				this.mainView = document.querySelector(".PaymentReminderView");
			}
			addListeners() {
				this.mainView
					.querySelector(".ProVersionButton")
					.addEventListener("click", () => {
						o.browser.tabs.create({
							url: "https://billing.nighteye.app/client/login",
						});
					}),
					this.mainView
						.querySelector(".CloseButton")
						.addEventListener("click", () => {
							this.updateDiscountCodeSeen(), this.hide();
						});
			}
			fillData() {
				this.mainView.querySelector(
					".DiscountCode"
				).textContent = this.discountCode;
			}
			show() {
				T.addClass(this.mainView, "Active");
			}
			hide() {
				T.removeClass(this.mainView, "Active"),
					o.browser.browserAction.setIcon({
						path: o.browser.extension.getURL("res/icon/48.png"),
					});
			}
			updateDiscountCodeSeen() {
				var t = localStorage.getItem(L.instanceID);
				this.updateDiscountCodeSeenNetworkRequest(t, this.discountCode);
			}
			updateDiscountCodeSeenNetworkRequest(t, e) {
				var n =
						"https://billing.nighteye.app/exc/InstanceController?ac=h&id=" +
						t +
						"&dscode=" +
						e,
					i = new XMLHttpRequest();
				(i.onreadystatechange = async () => {
					i.readyState === XMLHttpRequest.DONE &&
						200 === i.status &&
						JSON.parse(i.responseText).status &&
						l.send(
							l.Settings.ID,
							l.Settings.ACTION_CHECK_LICENSE,
							{},
							() => {}
						);
				}),
					i.open("POST", n, !0),
					i.send(n);
			}
		}
		o.page = new (class extends vt {
			onBeforeLoad() {
				var t = setInterval(() => {
					O.isReady && (clearInterval(t), this.initTranslations());
				}, 100);
				this.initFields(),
					this.initHeaderListeners(),
					this.initBrowserBasedCSS(),
					this.disableRightClick(),
					this.getTabInfo();
			}
			getTabInfo() {
				o.browser.tabs.query({ active: !0, currentWindow: !0 }, (t) => {
					var e = t[0],
						n = e.url;
					void 0 === n && (n = "New Tab"),
						(this.url = a.parseURL(n)),
						(o.URL = this.url),
						null !== a.unsupported[this.url]
							? o.browser.tabs.executeScript(
									e.id,
									{ code: "document.contentType" },
									([t]) => {
										this.init(e, t);
									}
							  )
							: this.init(e, null);
				});
			}
			init(t, e) {
				this.contentType = e;
				var n = "application/pdf" === this.contentType ? "PDF" : this.url;
				this.initComponents(),
					this.url_state_c.setURL(n),
					l.send(
						l.Settings.ID,
						l.Settings.ACTION_ON_START_BROWSER_ACTION,
						{ url: this.url, contentType: this.contentType },
						async (e) => {
							(this.appInfoModel = i.fromState(S, e)),
								!0 === this.appInfoModel.supported_url &&
									((this.connect_attempt = 0), this.connectToActiveTab(t.id)),
								await this.initPaymentReminder(),
								await this.initTrialExpired(
									this.appInfoModel.is_trial_expired,
									this.appInfoModel.isLiteVersion
								),
								console.log(
									"this.appInfoModel.local_settings: ",
									this.appInfoModel.local_settings
								),
								this.initFirstRun(this.appInfoModel.first_run),
								this.initURLState(
									this.appInfoModel.supported_url,
									this.appInfoModel.mode,
									this.appInfoModel.isLiteVersion,
									this.appInfoModel.isOSColorSchemeCheckerEnabled
								),
								this.initPowerState(
									this.appInfoModel.enabled,
									this.appInfoModel.day_night_timers
								),
								this.initVersionLabel(this.appInfoModel.isLiteVersion),
								this.initSettingsColors(
									this.appInfoModel.local_settings.palette
								),
								this.initSettingsImagesState(
									this.appInfoModel.global_settings.images.default_v,
									this.appInfoModel.local_settings.images.global_v,
									this.appInfoModel.global_settings.images.global_v
								),
								this.initSettingsBuiltInDarkThemeState(
									this.appInfoModel.global_settings.builtInDarkTheme.default_v,
									this.appInfoModel.local_settings.builtInDarkTheme.global_v,
									this.appInfoModel.global_settings.builtInDarkTheme.global_v
								),
								this.initSettingsTurboCacheState(
									this.appInfoModel.global_settings.turboCache.default_v,
									this.appInfoModel.local_settings.turboCache.global_v,
									this.appInfoModel.global_settings.turboCache.global_v
								),
								this.initSettingsBrightness(
									this.appInfoModel.global_settings.brightness.default_v,
									this.appInfoModel.local_settings.brightness.global_v,
									this.appInfoModel.global_settings.brightness.global_v
								),
								this.initSettingsContrast(
									this.appInfoModel.global_settings.contrast.default_v,
									this.appInfoModel.local_settings.contrast.global_v,
									this.appInfoModel.global_settings.contrast.global_v
								),
								this.initSettingsSaturation(
									this.appInfoModel.global_settings.saturation.default_v,
									this.appInfoModel.local_settings.saturation.global_v,
									this.appInfoModel.global_settings.saturation.global_v
								),
								this.initSettingsTemperature(
									this.appInfoModel.global_settings.temperature.default_v,
									this.appInfoModel.local_settings.temperature.global_v,
									this.appInfoModel.global_settings.temperature.global_v
								),
								this.initSettingsDim(
									this.appInfoModel.global_settings.dim.default_v,
									this.appInfoModel.local_settings.dim.global_v,
									this.appInfoModel.global_settings.dim.global_v
								),
								this.initSupport(),
								this.initOptions(
									this.appInfoModel.is_trial_expired,
									this.appInfoModel.default_mode,
									this.appInfoModel.day_night_timers,
									this.appInfoModel.isChangingScrollsEnabled,
									this.appInfoModel.isOSColorSchemeCheckerEnabled,
									this.appInfoModel.isLiteVersion
								),
								this.onInitFinish();
						}
					);
			}
			connectToActiveTab(t) {
				!0 === this.appInfoModel.enabled &&
					!0 === this.appInfoModel.supported_url &&
					null === this.port &&
					(window.navigator.userAgent.indexOf("Edge") > -1
						? ((this.port = {}),
						  (this.port = o.browser.runtime),
						  (this.port.postMessage = (e) => {
								o.browser.tabs.sendMessage(t, e);
						  }))
						: ((this.port = o.browser.tabs.connect(t)),
						  this.port.onDisconnect.addListener((e, n, i) => {
								(this.port = null),
									this.connect_attempt++ > 100 ||
										setTimeout(() => {
											this.connectToActiveTab(t);
										}, 16);
						  })),
					this.port.postMessage({ property: "open_extension" }),
					this.port.onMessage.addListener((t) => {
						switch (t.property) {
							case "open_extension":
								this.connect_attempt = 0;
								break;
							case "load_page_hsl_colors":
								this.settings_colors_c.onLoadPageColors(t.data);
						}
					}),
					(this.settings_colors_c.port = this.port),
					(this.settings_brightness_c.port = this.port),
					(this.settings_contrast_c.port = this.port),
					(this.settings_saturation_c.port = this.port),
					(this.settings_temperature_c.port = this.port),
					(this.settings_dim_c.port = this.port));
			}
			reload(t) {
				o.browser.tabs.query({ active: !0, currentWindow: !0 }, (e) => {
					o.browser.tabs.executeScript(e[0].id, {
						code: "window.location.reload();",
					}),
						null !== t && typeof t !== s.UNDEF && t();
				});
			}
			initFields() {
				(this.body = document.querySelector("section.Body")),
					(this.enabled_n = document.body.querySelector(".EnabledView")),
					(this.active_c = null),
					(this.port = null);
			}
			initHeaderListeners() {
				var t = () => {
					this.restoreDefaultView();
				};
				document.querySelector(".Logo").addEventListener("click", t),
					document.querySelector("header label").addEventListener("click", t);
			}
			initBrowserBasedCSS() {
				lt.nameIdentity === lt.CHROME && T.addClass(document.body, "Chrome");
			}
			disableRightClick() {
				document.addEventListener("contextmenu", (t) => t.preventDefault());
			}
			initComponents() {
				this.initURLStateComponent(),
					this.initControlsComponent(),
					this.initSettingsComponent();
			}
			initURLStateComponent() {
				(this.url_state_c = new ht()),
					(this.url_state_c.darkListener = () => {
						this.setDarkMode(a.Mode.DARK),
							l.send(
								l.Settings.ID,
								l.Settings.ACTION_CHANGE_MODE,
								{
									url: this.url,
									contentType: this.contentType,
									mode: a.Mode.DARK,
								},
								() => {
									this.reload(() => {
										this.reloadData();
									});
								}
							);
					}),
					(this.url_state_c.filteredListener = () => {
						this.setDarkMode(a.Mode.FILTERED),
							l.send(
								l.Settings.ID,
								l.Settings.ACTION_CHANGE_MODE,
								{
									url: this.url,
									contentType: this.contentType,
									mode: a.Mode.FILTERED,
								},
								() => {
									this.reload(() => {
										this.reloadData();
									});
								}
							);
					}),
					(this.url_state_c.normalListener = () => {
						this.setDarkMode(a.Mode.NORMAL),
							l.send(
								l.Settings.ID,
								l.Settings.ACTION_CHANGE_MODE,
								{
									url: this.url,
									contentType: this.contentType,
									mode: a.Mode.NORMAL,
								},
								() => {
									this.reload(() => {
										this.reloadData();
									});
								}
							);
					});
			}
			initControlsComponent() {
				(this.controls_c = new I()),
					(this.controls_c.powerListener = (t) => {
						this.setPowerState(t),
							!0 === t
								? l.send(l.Settings.ID, l.Settings.ACTION_ENABLE, null, () => {
										this.reload(() => {
											this.reloadData();
										});
								  })
								: l.send(l.Settings.ID, l.Settings.ACTION_DISABLE, null, () => {
										this.reload(() => {
											this.reloadData();
										});
								  });
					}),
					(this.controls_c.cancelListener = () => {
						this.active_c.cancelListener(() => {
							this.restoreDefaultView();
						});
					}),
					(this.controls_c.applyToGlobalListener = () => {
						this.active_c.applyToGlobalListener(this.settings_c, () => {
							this.restoreDefaultView(),
								this.reload(() => {
									this.reloadData();
								});
						});
					}),
					(this.controls_c.applyToLocalListener = () => {
						this.active_c.applyToLocalListener(this.settings_c, () => {
							this.restoreDefaultView(),
								this.reload(() => {
									this.reloadData();
								});
						});
					}),
					(this.controls_c.defaultsGlobalListener = () => {
						this.active_c.defaultsGlobalListener(this.settings_c, () => {
							this.restoreDefaultView(),
								this.reload(() => {
									this.reloadData();
								});
						});
					}),
					(this.controls_c.defaultsLocalListener = () => {
						this.active_c.defaultsLocalListener(this.settings_c, () => {
							this.restoreDefaultView(),
								this.reload(() => {
									this.reloadData();
								});
						});
					});
			}
			initSettingsComponent() {
				var t = () => {
					this.controls_c.setAdditionalControlsVisibility(!0),
						T.addClass(this.enabled_n, "Disabled");
				};
				(this.settings_colors_c = new W(this)),
					(this.settings_images_c = new z(this.url, this.contentType)),
					(this.settings_builtInDarkTheme_c = new X(
						this.url,
						this.contentType
					)),
					(this.settings_turboCache_c = new $(this.url, this.contentType)),
					(this.settings_brightness_c = new F(this.url, this.contentType)),
					(this.settings_contrast_c = new G(this.url, this.contentType)),
					(this.settings_saturation_c = new J(this.url, this.contentType)),
					(this.settings_temperature_c = new Z(this.url, this.contentType)),
					(this.settings_dim_c = new Y(this.url, this.contentType)),
					(this.settings_c = new V(this.contentType)),
					(this.settings_c.colorsListener = () => {
						t(),
							this.settings_colors_c.loadPageColors(),
							this.settings_colors_c.setVisibility(!0),
							(this.active_c = this.settings_colors_c);
					}),
					(this.settings_c.imagesListener = () => {
						t(),
							this.settings_images_c.setVisibility(!0),
							(this.active_c = this.settings_images_c);
					}),
					(this.settings_c.builtInDarkThemeListener = () => {
						t(),
							this.settings_builtInDarkTheme_c.setVisibility(!0),
							(this.active_c = this.settings_builtInDarkTheme_c);
					}),
					(this.settings_c.turboCacheListener = () => {
						t(),
							this.settings_turboCache_c.setVisibility(!0),
							(this.active_c = this.settings_turboCache_c);
					}),
					(this.settings_c.brightnessListener = () => {
						t(),
							this.settings_brightness_c.setVisibility(!0),
							(this.active_c = this.settings_brightness_c);
					}),
					(this.settings_c.contrastListener = () => {
						t(),
							this.settings_contrast_c.setVisibility(!0),
							(this.active_c = this.settings_contrast_c);
					}),
					(this.settings_c.saturationListener = () => {
						t(),
							this.settings_saturation_c.setVisibility(!0),
							(this.active_c = this.settings_saturation_c);
					}),
					(this.settings_c.temperatureListener = () => {
						t(),
							this.settings_temperature_c.setVisibility(!0),
							(this.active_c = this.settings_temperature_c);
					}),
					(this.settings_c.dimListener = () => {
						t(),
							this.settings_dim_c.setVisibility(!0),
							(this.active_c = this.settings_dim_c);
					});
			}
			initFirstRun(t) {
				if (!1 !== t) {
					var e = document.querySelector(".FirstRun");
					T.addClass(e, "Active"),
						e.querySelector(".Control").addEventListener("click", () => {
							T.removeClass(e, "Active"), window.location.reload();
						}),
						l.send(
							l.Settings.ID,
							l.Settings.ACTION_AFTER_FIRST_RUN,
							null,
							() => {}
						);
				}
			}
			async initPaymentReminder() {
				var t = localStorage.getItem(L.discountCode);
				null == t ||
					t === s.EMPTY ||
					t.length < 3 ||
					((this.payment_reminder = new _t(t)), this.payment_reminder.show());
			}
			async initTrialExpired(t, e) {
				this.checkLicense(t),
					(this.trial_expired_c = new P()),
					await this.trial_expired_c.initEmail(),
					!1 === t ||
						e ||
						(this.trial_expired_c.showMainView(),
						this.setTrialExpiredScreenTimestamps());
			}
			async setTrialExpiredScreenTimestamps() {
				var t = new Date().getTime();
				null === localStorage.getItem(L.trialExpiredScreenShownTS) &&
					localStorage.setItem(L.trialExpiredScreenShownTS, t);
			}
			checkLicense(t) {
				l.send(l.Settings.ID, l.Settings.ACTION_CHECK_LICENSE, {}, (e) => {
					var n = e === s.TRUE;
					t !== n && window.location.reload();
				});
			}
			initURLState(t, e, n, i) {
				this.url_state_c.init(t, e, n, i),
					!0 === t
						? this.setDarkMode(e)
						: (T.addClass(this.body, "Red"),
						  this.settings_c.setMode(a.Mode.NORMAL));
			}
			initPowerState(t, e) {
				this.controls_c.setPowerState(t),
					this.setPowerState(t),
					e.status &&
						(this.body.querySelector(".DisabledView .ScheduleLabel").innerHTML =
							"                <div>Night Eye is scheduled to work <br> from " +
							e.startTime +
							" - " +
							e.endTime +
							'</div>\n                <div class="ActionLabel">Turning it on now will deactivate the schedule.</div>'),
					this.body
						.querySelector(".DisabledView")
						.addEventListener("click", () => {
							this.controls_c.setPowerState(!0),
								this.setPowerState(!0),
								l.send(l.Settings.ID, l.Settings.ACTION_ENABLE, null, () => {
									this.reload(() => {
										this.reloadData();
									});
								});
						});
			}
			initVersionLabel(t) {
				t &&
					(document.querySelector("header .AppName").innerHTML =
						"Night Eye <i>Lite</i>");
			}
			initSettingsColors(t) {
				this.settings_c.setColorsState(!1 === A.isMapEmpty(t)),
					this.settings_colors_c.setValues(t);
			}
			initSettingsImagesState(t, e, n) {
				this.settings_c.setImagesState(e),
					this.settings_images_c.setValues(t, e, n);
			}
			initSettingsBuiltInDarkThemeState(t, e, n) {
				this.settings_c.setBuiltInDarkThemeState(e),
					this.settings_builtInDarkTheme_c.setValues(t, e, n);
			}
			initSettingsTurboCacheState(t, e, n) {
				this.settings_c.setTurboCacheState(e),
					this.settings_turboCache_c.setValues(t, e, n);
			}
			initSettingsBrightness(t, e, n) {
				this.settings_c.setBrightnessState(t !== e),
					this.settings_brightness_c.setValues(t, e, n);
			}
			initSettingsContrast(t, e, n) {
				this.settings_c.setContrastState(t !== e),
					this.settings_contrast_c.setValues(t, e, n);
			}
			initSettingsSaturation(t, e, n) {
				this.settings_c.setSaturationState(t !== e),
					this.settings_saturation_c.setValues(t, e, n);
			}
			initSettingsTemperature(t, e, n) {
				this.settings_c.setTemperatureState(t !== e),
					this.settings_temperature_c.setValues(t, e, n);
			}
			initSettingsDim(t, e, n) {
				this.settings_c.setDimState(t !== e),
					this.settings_dim_c.setValues(t, e, n);
			}
			initSupport() {
				(this.support_c = new ot()),
					this.support_c.checkForUnreadMessages(this.appInfoModel.chat_info);
			}
			initOptions(t, e, n, i, r, s) {
				if (n.status) {
					var o = document.querySelector(".DayNightTimersActiveLabel");
					(o.innerHTML =
						"Schedule is ON /" + n.startTime + " - " + n.endTime + "/"),
						T.addClass(o, "Active");
				}
				this.options_c = new k(t, e, n, i, r, this.support_c, s);
			}
			onInitFinish() {
				T.addClass(document.querySelector(".Loading"), "Done");
				for (
					var t = document.querySelectorAll(".Initable"), e = t.length;
					e-- > 0;

				)
					T.addClass(t[e], "Done");
			}
			setDarkMode(t) {
				t !== a.Mode.NORMAL
					? T.removeClass(this.body, "Light")
					: T.addClass(this.body, "Light"),
					this.settings_c.setMode(t);
			}
			setPowerState(t) {
				t
					? (T.addClass(this.body, "Enabled"),
					  T.removeClass(this.body, "Disabled"),
					  T.removeClass(this.body, "DarkRed"))
					: (T.addClass(this.body, "Disabled"),
					  T.removeClass(this.body, "Enabled"),
					  T.addClass(this.body, "DarkRed"));
			}
			restoreDefaultView() {
				this.support_c.hide(),
					this.controls_c.setSlidingControlsVisibility(!1),
					this.settings_colors_c.setVisibility(!1),
					this.settings_images_c.setVisibility(!1),
					this.settings_builtInDarkTheme_c.setVisibility(!1),
					this.settings_turboCache_c.setVisibility(!1),
					this.settings_brightness_c.setVisibility(!1),
					this.settings_contrast_c.setVisibility(!1),
					this.settings_saturation_c.setVisibility(!1),
					this.settings_temperature_c.setVisibility(!1),
					this.settings_dim_c.setVisibility(!1),
					T.removeClass(this.enabled_n, "Disabled"),
					(this.active_c = null);
			}
			reloadData() {
				l.send(
					l.Settings.ID,
					l.Settings.ACTION_ON_START_BROWSER_ACTION,
					{ url: this.url, contentType: this.contentType },
					(t) => {
						(this.appInfoModel = i.fromState(S, t)),
							o.browser.tabs.query({ active: !0, currentWindow: !0 }, (t) => {
								this.connectToActiveTab(t[0].id);
							});
					}
				);
			}
			showLoading() {
				T.addClass(document.querySelector(".ContentLoading"), "Active");
			}
			hideLoading() {
				T.removeClass(document.querySelector(".ContentLoading"), "Active");
			}
			initTranslations() {
				var t = [],
					e = 0;
				for (
					document.getElementById("modeButtonDark").innerHTML = O.getMessage(
						"filterModeNameDark"
					),
						document
							.getElementById("modeButtonDark")
							.setAttribute("title", O.getMessage("tooltipDark")),
						document.getElementById(
							"modeButtonFiltered"
						).innerHTML = O.getMessage("filterModeNameFiltered"),
						document
							.getElementById("modeButtonFiltered")
							.setAttribute("title", O.getMessage("tooltipFiltered")),
						document.getElementById(
							"modeButtonNormal"
						).innerHTML = O.getMessage("filterModeNameNormal"),
						document
							.getElementById("modeButtonNormal")
							.setAttribute("title", O.getMessage("tooltipNormal")),
						document.getElementById("labelColors").innerHTML = O.getMessage(
							"labelColors"
						),
						document.getElementById("labelBrightness").innerHTML = O.getMessage(
							"labelBrightness"
						),
						document.getElementById("labelContrast").innerHTML = O.getMessage(
							"labelContrast"
						),
						document.getElementById("labelSaturation").innerHTML = O.getMessage(
							"labelSaturation"
						),
						document.getElementById("labelBlueLight").innerHTML = O.getMessage(
							"labelBlueLight"
						),
						document.getElementById("labelDim").innerHTML = O.getMessage(
							"labelDim"
						),
						document.getElementById("extTurnOnButton").innerHTML = O.getMessage(
							"extTurnOnButton"
						),
						document.getElementById(
							"extDisabledStatus"
						).innerHTML = O.getMessage("extDisabledStatus"),
						document.getElementById("colorViewTitle").innerHTML =
							O.getMessage("colorViewTitle") +
							"<br><b>" +
							O.getMessage("labelExperimentalFeature") +
							"</b>",
						document.getElementById(
							"colorViewDescription"
						).innerHTML = O.getMessage("colorViewDescription"),
						document.getElementById(
							"colorViewTitle2"
						).innerHTML = document.getElementById("colorViewTitle").innerHTML,
						document.getElementById(
							"colorViewDescription2"
						).innerHTML = document.getElementById(
							"colorViewDescription"
						).innerHTML,
						document.getElementById(
							"colorViewReplacedColor"
						).innerHTML = O.getMessage("colorViewReplacedColor"),
						document.getElementById(
							"colorViewOriginalColor"
						).innerHTML = O.getMessage("colorViewOriginalColor"),
						document.getElementById("imageViewTitle").innerHTML = O.getMessage(
							"imageViewTitle"
						),
						t = document.querySelectorAll(".SwitchLabelOn"),
						e = 0;
					e < t.length;
					++e
				)
					t[e].innerHTML = O.getMessage("switchLabelOn");
				for (
					t = document.querySelectorAll(".SwitchLabelOff"), e = 0;
					e < t.length;
					++e
				)
					t[e].innerHTML = O.getMessage("switchLabelOff");
				(document.getElementById(
					"brightnessViewTitle"
				).innerHTML = O.getMessage("brightnessViewTitle")),
					(document.getElementById(
						"brightnessViewDescription"
					).innerHTML = O.getMessage("brightnessViewDescription")),
					(document.getElementById(
						"brightnessViewCounterName"
					).innerHTML = document.getElementById(
						"brightnessViewTitle"
					).innerHTML),
					(document.getElementById(
						"contrastViewTitle"
					).innerHTML = O.getMessage("contrastViewTitle")),
					(document.getElementById(
						"contrastViewDescription"
					).innerHTML = O.getMessage("contrastViewDescription")),
					(document.getElementById(
						"contrastViewCounterName"
					).innerHTML = document.getElementById("contrastViewTitle").innerHTML),
					(document.getElementById(
						"saturationViewTitle"
					).innerHTML = O.getMessage("saturationViewTitle")),
					(document.getElementById(
						"saturationViewDescription"
					).innerHTML = O.getMessage("saturationViewDescription")),
					(document.getElementById(
						"saturationViewCounterName"
					).innerHTML = document.getElementById(
						"saturationViewTitle"
					).innerHTML),
					(document.getElementById(
						"temperatureViewTitle"
					).innerHTML = O.getMessage("temperatureViewTitle")),
					(document.getElementById(
						"temperatureViewDescription"
					).innerHTML = O.getMessage("temperatureViewDescription")),
					(document.getElementById(
						"temperatureViewOption1"
					).innerHTML = O.getMessage("temperatureViewOption1")),
					(document.getElementById(
						"temperatureViewOption2"
					).innerHTML = O.getMessage("temperatureViewOption2")),
					(document.getElementById("dimViewTitle").innerHTML = O.getMessage(
						"dimViewTitle"
					)),
					(document.getElementById(
						"dimViewDescription"
					).innerHTML = O.getMessage("dimViewDescription")),
					(document.getElementById(
						"dimViewCounterName"
					).innerHTML = document.getElementById("dimViewTitle").innerHTML),
					(document.getElementById("refreshViewTitle").innerHTML = O.getMessage(
						"refreshViewTitle"
					)),
					(document.getElementById(
						"refreshViewDescription"
					).innerHTML = O.getMessage("refreshViewDescription")),
					(document.getElementById("refreshButton").innerHTML = O.getMessage(
						"refreshButton"
					)),
					(document.getElementById("supportViewInfo").innerHTML =
						O.getMessage("supportViewInfo1") +
						"<br>" +
						O.getMessage("supportViewInfo2") +
						"<br>" +
						O.getMessage("supportViewInfo3")),
					(document.getElementById("supportViewInputName").innerHTML =
						O.getMessage("supportViewInputName") + ":"),
					(document.getElementById("supportViewInputEmail").innerHTML =
						O.getMessage("supportViewInputEmail") + ":"),
					(document.getElementById(
						"helpViewHelpButton"
					).innerHTML = O.getMessage("helpViewHelpButton")),
					(document.getElementById(
						"helpViewShortcutTitle"
					).innerHTML = O.getMessage("helpViewShortcutTitle")),
					(document.getElementById(
						"helpViewShortcutDescription"
					).innerHTML = O.getMessage("helpViewShortcutDescription")),
					(document.getElementById(
						"helpViewUninstallTitle"
					).innerHTML = O.getMessage("helpViewUninstallTitle")),
					(document.getElementById(
						"helpViewUninstallDescription"
					).innerHTML = O.getMessage("helpViewUninstallDescription")),
					(document.getElementById(
						"helpViewDefaultModeTitle"
					).innerHTML = O.getMessage("helpViewDefaultModeTitle")),
					(document.getElementById(
						"helpViewDefaultModeDescription"
					).innerHTML = O.getMessage("helpViewDefaultModeDescription")),
					(document.getElementById(
						"firstRunViewTitle"
					).innerHTML = O.getMessage("firstRunViewTitle"));
				var n = o.EDGE_LIMITATION_DATE.getTime(),
					i = new Date().getTime();
				if (window.navigator.userAgent.indexOf("Edge") > -1 && i < n) {
					var r = document.getElementById("firstRunViewFeature2").parentNode;
					r.parentNode.removeChild(r);
					var s = document.querySelector(".Options .ClientArea");
					s.parentNode.removeChild(s),
						(s = document.querySelector(
							".Options .GoProLink"
						)).parentNode.removeChild(s);
				}
				switch (
					((document.getElementById(
						"controlsPanelButtonCancel"
					).innerHTML = O.getMessage("controlsPanelButtonCancel")),
					(document.getElementById(
						"controlsPanelButtonDefault"
					).innerHTML = O.getMessage("controlsPanelButtonDefault")),
					(document.getElementById(
						"controlsPanelButtonApply"
					).innerHTML = O.getMessage("controlsPanelButtonApply")),
					(document.getElementById(
						"controlsPanelButtonApplyToAllWebsites"
					).innerHTML =
						O.getMessage("controlsPanelButtonApplyTo") +
						"<br>" +
						O.getMessage("controlsPanelAllWebsites")),
					(document.getElementById(
						"controlsPanelButtonApplyToCurrentWebsite"
					).innerHTML =
						O.getMessage("controlsPanelButtonApplyTo") +
						"<br>" +
						O.getMessage("controlsPanelCurrentWebsite")),
					(document.getElementById(
						"controlsPanelButtonRestoreToAllWebsites"
					).innerHTML =
						O.getMessage("controlsPanelButtonRestoreTo") +
						"<br>" +
						O.getMessage("controlsPanelForAllWebsites")),
					(document.getElementById(
						"controlsPanelButtonRestoreToCurrentWebsite"
					).innerHTML =
						O.getMessage("controlsPanelButtonRestoreTo") +
						"<br>" +
						O.getMessage("controlsPanelForCurrentWebsite")),
					O.getLanguage())
				) {
					case "ru":
						for (
							t = document.body.querySelectorAll(".Settings .Control span"),
								e = 0;
							e < t.length;
							++e
						)
							t[e].style.fontSize = "8px";
						for (
							t = document.body.querySelectorAll(
								"footer .DefaultsControls > div"
							),
								e = 0;
							e < t.length;
							++e
						)
							t[e].style.fontSize = "11px";
						for (
							t = document.body.querySelectorAll(".PageHome .URLControl > div"),
								e = 0;
							e < t.length;
							++e
						)
							(t[e].style.fontSize = "10px"),
								1 === e &&
									((t[e].style.paddingLeft = "15px"),
									(t[e].style.paddingRight = "15px"));
						break;
					case "bg":
						for (
							t = document.body.querySelectorAll(".PageHome .URLControl > div"),
								e = 0;
							e < t.length;
							++e
						)
							1 === e &&
								((t[e].style.paddingLeft = "15px"),
								(t[e].style.paddingRight = "15px"));
				}
			}
		})();
	},
]);
