!(function (t) {
	var e = {};
	function r(n) {
		if (e[n]) return e[n].exports;
		var o = (e[n] = { i: n, l: !1, exports: {} });
		return t[n].call(o.exports, o, o.exports, r), (o.l = !0), o.exports;
	}
	(r.m = t),
		(r.c = e),
		(r.d = function (t, e, n) {
			r.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n });
		}),
		(r.r = function (t) {
			"undefined" != typeof Symbol &&
				Symbol.toStringTag &&
				Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }),
				Object.defineProperty(t, "__esModule", { value: !0 });
		}),
		(r.t = function (t, e) {
			if ((1 & e && (t = r(t)), 8 & e)) return t;
			if (4 & e && "object" == typeof t && t && t.__esModule) return t;
			var n = Object.create(null);
			if (
				(r.r(n),
				Object.defineProperty(n, "default", { enumerable: !0, value: t }),
				2 & e && "string" != typeof t)
			)
				for (var o in t)
					r.d(
						n,
						o,
						function (e) {
							return t[e];
						}.bind(null, o)
					);
			return n;
		}),
		(r.n = function (t) {
			var e =
				t && t.__esModule
					? function () {
							return t.default;
					  }
					: function () {
							return t;
					  };
			return r.d(e, "a", e), e;
		}),
		(r.o = function (t, e) {
			return Object.prototype.hasOwnProperty.call(t, e);
		}),
		(r.p = ""),
		r((r.s = 138));
})([
	function (t, e, r) {
		var n = r(2),
			o = r(15).f,
			i = r(14),
			a = r(22),
			s = r(82),
			c = r(105),
			u = r(56);
		t.exports = function (t, e) {
			var r,
				l,
				f,
				h,
				d,
				p = t.target,
				g = t.global,
				v = t.stat;
			if ((r = g ? n : v ? n[p] || s(p, {}) : (n[p] || {}).prototype))
				for (l in e) {
					if (
						((h = e[l]),
						(f = t.noTargetGet ? (d = o(r, l)) && d.value : r[l]),
						!u(g ? l : p + (v ? "." : "#") + l, t.forced) && void 0 !== f)
					) {
						if (typeof h == typeof f) continue;
						c(h, f);
					}
					(t.sham || (f && f.sham)) && i(h, "sham", !0), a(r, l, h, t);
				}
		};
	},
	function (t, e) {
		t.exports = function (t) {
			try {
				return !!t();
			} catch (t) {
				return !0;
			}
		};
	},
	function (t, e, r) {
		(function (e) {
			var r = "object",
				n = function (t) {
					return t && t.Math == Math && t;
				};
			t.exports =
				n(typeof globalThis == r && globalThis) ||
				n(typeof window == r && window) ||
				n(typeof self == r && self) ||
				n(typeof e == r && e) ||
				Function("return this")();
		}.call(this, r(141)));
	},
	function (t, e) {
		t.exports = function (t) {
			return "object" == typeof t ? null !== t : "function" == typeof t;
		};
	},
	function (t, e, r) {
		"use strict";
		var n,
			o = r(6),
			i = r(2),
			a = r(3),
			s = r(12),
			c = r(60),
			u = r(14),
			l = r(22),
			f = r(9).f,
			h = r(28),
			d = r(45),
			p = r(8),
			g = r(53),
			v = i.DataView,
			m = v && v.prototype,
			y = i.Int8Array,
			_ = y && y.prototype,
			E = i.Uint8ClampedArray,
			S = E && E.prototype,
			T = y && h(y),
			b = _ && h(_),
			I = Object.prototype,
			A = I.isPrototypeOf,
			w = p("toStringTag"),
			x = g("TYPED_ARRAY_TAG"),
			O = !(!i.ArrayBuffer || !i.DataView),
			M = O && !!d,
			C = !1,
			R = {
				Int8Array: 1,
				Uint8Array: 1,
				Uint8ClampedArray: 1,
				Int16Array: 2,
				Uint16Array: 2,
				Int32Array: 4,
				Uint32Array: 4,
				Float32Array: 4,
				Float64Array: 8,
			},
			N = function (t) {
				return a(t) && s(R, c(t));
			};
		for (n in R) i[n] || (M = !1);
		if (
			(!M || "function" != typeof T || T === Function.prototype) &&
			((T = function () {
				throw TypeError("Incorrect invocation");
			}),
			M)
		)
			for (n in R) i[n] && d(i[n], T);
		if ((!M || !b || b === I) && ((b = T.prototype), M))
			for (n in R) i[n] && d(i[n].prototype, b);
		if ((M && h(S) !== b && d(S, b), o && !s(b, w)))
			for (n in ((C = !0),
			f(b, w, {
				get: function () {
					return a(this) ? this[x] : void 0;
				},
			}),
			R))
				i[n] && u(i[n], x, n);
		O && d && h(m) !== I && d(m, I),
			(t.exports = {
				NATIVE_ARRAY_BUFFER: O,
				NATIVE_ARRAY_BUFFER_VIEWS: M,
				TYPED_ARRAY_TAG: C && x,
				aTypedArray: function (t) {
					if (N(t)) return t;
					throw TypeError("Target is not a typed array");
				},
				aTypedArrayConstructor: function (t) {
					if (d) {
						if (A.call(T, t)) return t;
					} else
						for (var e in R)
							if (s(R, n)) {
								var r = i[e];
								if (r && (t === r || A.call(r, t))) return t;
							}
					throw TypeError("Target is not a typed array constructor");
				},
				exportProto: function (t, e, r) {
					if (o) {
						if (r)
							for (var n in R) {
								var a = i[n];
								a && s(a.prototype, t) && delete a.prototype[t];
							}
						(b[t] && !r) || l(b, t, r ? e : (M && _[t]) || e);
					}
				},
				exportStatic: function (t, e, r) {
					var n, a;
					if (o) {
						if (d) {
							if (r) for (n in R) (a = i[n]) && s(a, t) && delete a[t];
							if (T[t] && !r) return;
							try {
								return l(T, t, r ? e : (M && y[t]) || e);
							} catch (t) {}
						}
						for (n in R) !(a = i[n]) || (a[t] && !r) || l(a, t, e);
					}
				},
				isView: function (t) {
					var e = c(t);
					return "DataView" === e || s(R, e);
				},
				isTypedArray: N,
				TypedArray: T,
				TypedArrayPrototype: b,
			});
	},
	function (t, e, r) {
		var n = r(3);
		t.exports = function (t) {
			if (!n(t)) throw TypeError(String(t) + " is not an object");
			return t;
		};
	},
	function (t, e, r) {
		var n = r(1);
		t.exports = !n(function () {
			return (
				7 !=
				Object.defineProperty({}, "a", {
					get: function () {
						return 7;
					},
				}).a
			);
		});
	},
	function (t, e, r) {
		var n = r(23),
			o = Math.min;
		t.exports = function (t) {
			return t > 0 ? o(n(t), 9007199254740991) : 0;
		};
	},
	function (t, e, r) {
		var n = r(2),
			o = r(52),
			i = r(53),
			a = r(107),
			s = n.Symbol,
			c = o("wks");
		t.exports = function (t) {
			return c[t] || (c[t] = (a && s[t]) || (a ? s : i)("Symbol." + t));
		};
	},
	function (t, e, r) {
		var n = r(6),
			o = r(102),
			i = r(5),
			a = r(25),
			s = Object.defineProperty;
		e.f = n
			? s
			: function (t, e, r) {
					if ((i(t), (e = a(e, !0)), i(r), o))
						try {
							return s(t, e, r);
						} catch (t) {}
					if ("get" in r || "set" in r)
						throw TypeError("Accessors not supported");
					return "value" in r && (t[e] = r.value), t;
			  };
	},
	function (t, e, r) {
		var n = r(18);
		t.exports = function (t) {
			return Object(n(t));
		};
	},
	function (t, e, r) {
		"use strict";
		r(30), r(100), r(49);
		class n {
			constructor(t) {
				(this.communicatorID = t), (this.promiseID = ""), (this.info = {});
			}
		}
		class o {
			static shared() {
				return (
					(void 0 !== o.instance && null !== o.instance) ||
						(o.instance = new o()),
					o.instance
				);
			}
			constructor() {
				(this.communicatorID = o.generateUUID()),
					(this.promises = new Map()),
					(this.onConnectListeners = { tabs: [], runtime: [] }),
					(this.onPortMessageListeners = []),
					(this.messageRuntimeListeners = []),
					(this.tabActionListeners = []),
					void 0 !== window.safari &&
						window.safari.self.addEventListener(
							"message",
							this.processResponse.bind(this)
						);
			}
			getFile(t) {
				return new Promise((e, r) => {
					var i = o.generateUUID();
					this.promises.set(i, { resolve: e, reject: r });
					var a = new n(this.communicatorID);
					(a.promiseID = i), (a.info.filename = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.files.postMessage(a)
							: window.safari.extension.dispatchMessage("files", a);
					} catch (t) {
						console.error(t);
					}
				});
			}
			downlaod(t) {
				return new Promise((e, r) => {
					var i = o.generateUUID();
					this.promises.set(i, { resolve: e, reject: r });
					var a = new n(this.communicatorID);
					(a.promiseID = i), (a.info.url = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.downlaod.postMessage(a)
							: window.safari.extension.dispatchMessage("download", a);
					} catch (t) {
						console.error(t);
					}
				});
			}
			tabs(t, e) {
				return new Promise((r, i) => {
					var a = o.generateUUID();
					this.promises.set(a, { resolve: r, reject: i });
					var s = new n(this.communicatorID);
					(s.promiseID = a), (s.info.action = e), (s.info.queryInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.tabs.postMessage(s)
							: window.safari.extension.dispatchMessage("tabs", s);
					} catch (t) {
						console.error(t);
					}
				});
			}
			sendMessage(t) {
				return new Promise((e, r) => {
					var i = o.generateUUID();
					this.promises.set(i, { resolve: e, reject: r });
					var a = new n(this.communicatorID);
					(a.promiseID = i), (a.info.userInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.runtime.postMessage(a)
							: window.safari.extension.dispatchMessage("runtime", a);
					} catch (t) {
						console.error(t);
					}
				});
			}
			browserAction(t) {
				return new Promise((e, r) => {
					var i = o.generateUUID();
					this.promises.set(i, { resolve: e, reject: r });
					var a = new n(this.communicatorID);
					(a.promiseID = i), (a.info.userInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.browser_action.postMessage(a)
							: window.safari.extension.dispatchMessage("browser_action", a);
					} catch (t) {
						console.error(t);
					}
				});
			}
			port(t) {
				return new Promise((e, r) => {
					var i = o.generateUUID();
					this.promises.set(i, { resolve: e, reject: r });
					var a = new n(this.communicatorID);
					(a.promiseID = i), (a.info.portInfo = t);
					try {
						void 0 === window.safari
							? window.webkit.messageHandlers.port.postMessage(a)
							: window.safari.extension.dispatchMessage("port", a);
					} catch (t) {
						console.error(t);
					}
				});
			}
			static generateUUID() {
				var t = new Date().getTime();
				return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
					/[xy]/g,
					function (e) {
						var r = (t + 16 * Math.random()) % 16 | 0;
						return (
							(t = Math.floor(t / 16)),
							("x" == e ? r : (3 & r) | 8).toString(16)
						);
					}
				);
			}
			processResponse(t) {
				let e = t.message.data;
				if (
					-1 !==
					[
						"files_resolve",
						"tabs_resolve",
						"runtime_resolve",
						"port_resolve",
					].indexOf(t.name)
				) {
					if (t.message.communicatorID !== this.communicatorID) return;
					let r = t.message.promiseID;
					!1 === this.resolvePromise(r, e, null) &&
						console.warn(
							`Response with promiseID: ${r} cannot not found in map`
						);
				} else
					"runtime" === t.name
						? this.onMessageRuntimeRecived(e)
						: "onConnectTab" === t.name
						? this.onConnectTabRecieved(e)
						: "onConnectRuntime" === t.name
						? this.onConnectRuntimeRecieved(e)
						: "portMessage" === t.name
						? this.onPortMessageListenersRecieved(e)
						: "tabActions" === t.name
						? this.onTabActionRecieved(e)
						: console.warn("Recieved message with name:", t.name);
			}
			resolvePromise(t, e, r) {
				var n = this.promises.get(t);
				return (
					void 0 !== n &&
					(r ? n.reject(e) : n.resolve(e), this.promises.delete(t), !0)
				);
			}
			addListenerOnConnectTab(t) {
				this.onConnectListeners.tabs.push(t);
			}
			addListenerOnConnectRuntime(t) {
				this.onConnectListeners.runtime.push(t);
			}
			onConnectTabRecieved(t) {
				for (const e of this.onConnectListeners.tabs) e(t);
			}
			onConnectRuntimeRecieved(t) {
				for (const e of this.onConnectListeners.runtime) e(t);
			}
			addListenerOnPortMessageListener(t) {
				this.onPortMessageListeners.push(t);
			}
			onPortMessageListenersRecieved(t) {
				for (const e of this.onPortMessageListeners) e(t);
			}
			addListenerOnMessageRuntime(t) {
				this.messageRuntimeListeners.push(t);
			}
			onMessageRuntimeRecived(t) {
				for (const e of this.messageRuntimeListeners) e(t);
			}
			onTabActionRecieved(t) {
				for (const e of this.tabActionListeners) e(t);
			}
			addListenerTabAction(t) {
				this.tabActionListeners.push(t);
			}
		}
		(o.instance = null), (window.communicator = o.shared());
		e.a = o;
	},
	function (t, e) {
		var r = {}.hasOwnProperty;
		t.exports = function (t, e) {
			return r.call(t, e);
		};
	},
	function (t, e, r) {
		var n = r(42),
			o = r(51),
			i = r(10),
			a = r(7),
			s = r(61);
		t.exports = function (t, e) {
			var r = 1 == t,
				c = 2 == t,
				u = 3 == t,
				l = 4 == t,
				f = 6 == t,
				h = 5 == t || f,
				d = e || s;
			return function (e, s, p) {
				for (
					var g,
						v,
						m = i(e),
						y = o(m),
						_ = n(s, p, 3),
						E = a(y.length),
						S = 0,
						T = r ? d(e, E) : c ? d(e, 0) : void 0;
					E > S;
					S++
				)
					if ((h || S in y) && ((v = _((g = y[S]), S, m)), t))
						if (r) T[S] = v;
						else if (v)
							switch (t) {
								case 3:
									return !0;
								case 5:
									return g;
								case 6:
									return S;
								case 2:
									T.push(g);
							}
						else if (l) return !1;
				return f ? -1 : u || l ? l : T;
			};
		};
	},
	function (t, e, r) {
		var n = r(6),
			o = r(9),
			i = r(37);
		t.exports = n
			? function (t, e, r) {
					return o.f(t, e, i(1, r));
			  }
			: function (t, e, r) {
					return (t[e] = r), t;
			  };
	},
	function (t, e, r) {
		var n = r(6),
			o = r(50),
			i = r(37),
			a = r(17),
			s = r(25),
			c = r(12),
			u = r(102),
			l = Object.getOwnPropertyDescriptor;
		e.f = n
			? l
			: function (t, e) {
					if (((t = a(t)), (e = s(e, !0)), u))
						try {
							return l(t, e);
						} catch (t) {}
					if (c(t, e)) return i(!o.f.call(t, e), t[e]);
			  };
	},
	function (t, e, r) {
		var n = r(68),
			o = r(12),
			i = r(108),
			a = r(9).f;
		t.exports = function (t) {
			var e = n.Symbol || (n.Symbol = {});
			o(e, t) || a(e, t, { value: i.f(t) });
		};
	},
	function (t, e, r) {
		var n = r(51),
			o = r(18);
		t.exports = function (t) {
			return n(o(t));
		};
	},
	function (t, e) {
		t.exports = function (t) {
			if (null == t) throw TypeError("Can't call method on " + t);
			return t;
		};
	},
	function (t, e) {
		t.exports = function (t) {
			if ("function" != typeof t)
				throw TypeError(String(t) + " is not a function");
			return t;
		};
	},
	function (t, e, r) {
		var n = r(18),
			o = /"/g;
		t.exports = function (t, e, r, i) {
			var a = String(n(t)),
				s = "<" + e;
			return (
				"" !== r &&
					(s += " " + r + '="' + String(i).replace(o, "&quot;") + '"'),
				s + ">" + a + "</" + e + ">"
			);
		};
	},
	function (t, e, r) {
		var n = r(1);
		t.exports = function (t) {
			return n(function () {
				var e = ""[t]('"');
				return e !== e.toLowerCase() || e.split('"').length > 3;
			});
		};
	},
	function (t, e, r) {
		var n = r(2),
			o = r(52),
			i = r(14),
			a = r(12),
			s = r(82),
			c = r(103),
			u = r(26),
			l = u.get,
			f = u.enforce,
			h = String(c).split("toString");
		o("inspectSource", function (t) {
			return c.call(t);
		}),
			(t.exports = function (t, e, r, o) {
				var c = !!o && !!o.unsafe,
					u = !!o && !!o.enumerable,
					l = !!o && !!o.noTargetGet;
				"function" == typeof r &&
					("string" != typeof e || a(r, "name") || i(r, "name", e),
					(f(r).source = h.join("string" == typeof e ? e : ""))),
					t !== n
						? (c ? !l && t[e] && (u = !0) : delete t[e],
						  u ? (t[e] = r) : i(t, e, r))
						: u
						? (t[e] = r)
						: s(e, r);
			})(Function.prototype, "toString", function () {
				return ("function" == typeof this && l(this).source) || c.call(this);
			});
	},
	function (t, e) {
		var r = Math.ceil,
			n = Math.floor;
		t.exports = function (t) {
			return isNaN((t = +t)) ? 0 : (t > 0 ? n : r)(t);
		};
	},
	function (t, e, r) {
		"use strict";
		r.d(e, "a", function () {
			return i;
		});
		var n = r(11),
			o = r(65);
		class i {
			constructor(t, e) {
				(this.uniqueID = void 0 === e ? "" : e),
					(this.type = t),
					(this.uuid = n.a.generateUUID()),
					(this.name = ""),
					(this.sender = null),
					(this.onMessage = new o.a()),
					(this.onDisconnect = new o.a()),
					(this.ready = !1),
					(this.postMessageQueue = []);
			}
			executeWaitingMessages() {
				var t = this.postMessageQueue.pop();
				this.sendMessage(t),
					0 !== this.postMessageQueue.length && this.executeWaitingMessages();
			}
			postMessage(t) {
				this.postMessageQueue.unshift(t),
					this.ready && this.executeWaitingMessages();
			}
			sendMessage(t) {
				var e = {
					action: i.ACTION.SEND,
					portUUID: this.uuid,
					type: this.type,
					uniqueID: this.uniqueID,
					data: t,
				};
				n.a
					.shared()
					.port(e)
					.then((t) => {})
					.catch((t) => {
						console.error(`Error runtime ${t}`);
					});
			}
			disconnect() {}
		}
		(i.TYPES = { TAB: 1, RUNTIME: 2 }),
			(i.ACTION = { CREATE: 1, SEND: 2, DISCONECT: 3 }),
			(i.RECIEVER = { CONTENT: 1, POPUP: 2 });
	},
	function (t, e, r) {
		var n = r(3);
		t.exports = function (t, e) {
			if (!n(t)) return t;
			var r, o;
			if (e && "function" == typeof (r = t.toString) && !n((o = r.call(t))))
				return o;
			if ("function" == typeof (r = t.valueOf) && !n((o = r.call(t)))) return o;
			if (!e && "function" == typeof (r = t.toString) && !n((o = r.call(t))))
				return o;
			throw TypeError("Can't convert object to primitive value");
		};
	},
	function (t, e, r) {
		var n,
			o,
			i,
			a = r(104),
			s = r(2),
			c = r(3),
			u = r(14),
			l = r(12),
			f = r(66),
			h = r(54),
			d = s.WeakMap;
		if (a) {
			var p = new d(),
				g = p.get,
				v = p.has,
				m = p.set;
			(n = function (t, e) {
				return m.call(p, t, e), e;
			}),
				(o = function (t) {
					return g.call(p, t) || {};
				}),
				(i = function (t) {
					return v.call(p, t);
				});
		} else {
			var y = f("state");
			(h[y] = !0),
				(n = function (t, e) {
					return u(t, y, e), e;
				}),
				(o = function (t) {
					return l(t, y) ? t[y] : {};
				}),
				(i = function (t) {
					return l(t, y);
				});
		}
		t.exports = {
			set: n,
			get: o,
			has: i,
			enforce: function (t) {
				return i(t) ? o(t) : n(t, {});
			},
			getterFor: function (t) {
				return function (e) {
					var r;
					if (!c(e) || (r = o(e)).type !== t)
						throw TypeError("Incompatible receiver, " + t + " required");
					return r;
				};
			},
		};
	},
	function (t, e) {
		var r = {}.toString;
		t.exports = function (t) {
			return r.call(t).slice(8, -1);
		};
	},
	function (t, e, r) {
		var n = r(12),
			o = r(10),
			i = r(66),
			a = r(87),
			s = i("IE_PROTO"),
			c = Object.prototype;
		t.exports = a
			? Object.getPrototypeOf
			: function (t) {
					return (
						(t = o(t)),
						n(t, s)
							? t[s]
							: "function" == typeof t.constructor && t instanceof t.constructor
							? t.constructor.prototype
							: t instanceof Object
							? c
							: null
					);
			  };
	},
	function (t, e, r) {
		"use strict";
		var n = r(1);
		t.exports = function (t, e) {
			var r = [][t];
			return (
				!r ||
				!n(function () {
					r.call(
						null,
						e ||
							function () {
								throw 1;
							},
						1
					);
				})
			);
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(17),
			o = r(36),
			i = r(59),
			a = r(26),
			s = r(90),
			c = a.set,
			u = a.getterFor("Array Iterator");
		(t.exports = s(
			Array,
			"Array",
			function (t, e) {
				c(this, { type: "Array Iterator", target: n(t), index: 0, kind: e });
			},
			function () {
				var t = u(this),
					e = t.target,
					r = t.kind,
					n = t.index++;
				return !e || n >= e.length
					? ((t.target = void 0), { value: void 0, done: !0 })
					: "keys" == r
					? { value: n, done: !1 }
					: "values" == r
					? { value: e[n], done: !1 }
					: { value: [n, e[n]], done: !1 };
			},
			"values"
		)),
			(i.Arguments = i.Array),
			o("keys"),
			o("values"),
			o("entries");
	},
	function (t, e, r) {
		var n = r(5),
			o = r(19),
			i = r(8)("species");
		t.exports = function (t, e) {
			var r,
				a = n(t).constructor;
			return void 0 === a || null == (r = n(a)[i]) ? e : o(r);
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(2),
			i = r(6),
			a = r(101),
			s = r(4),
			c = r(79),
			u = r(48),
			l = r(37),
			f = r(14),
			h = r(7),
			d = r(134),
			p = r(135),
			g = r(25),
			v = r(12),
			m = r(60),
			y = r(3),
			_ = r(35),
			E = r(45),
			S = r(39).f,
			T = r(136),
			b = r(13),
			I = r(46),
			A = r(9),
			w = r(15),
			x = r(26),
			O = x.get,
			M = x.set,
			C = A.f,
			R = w.f,
			N = b(0),
			L = o.RangeError,
			D = c.ArrayBuffer,
			P = c.DataView,
			U = s.NATIVE_ARRAY_BUFFER_VIEWS,
			k = s.TYPED_ARRAY_TAG,
			B = s.TypedArray,
			j = s.TypedArrayPrototype,
			F = s.aTypedArrayConstructor,
			G = s.isTypedArray,
			W = function (t, e) {
				for (var r = 0, n = e.length, o = new (F(t))(n); n > r; ) o[r] = e[r++];
				return o;
			},
			H = function (t, e) {
				C(t, e, {
					get: function () {
						return O(this)[e];
					},
				});
			},
			V = function (t) {
				var e;
				return (
					t instanceof D ||
					"ArrayBuffer" == (e = m(t)) ||
					"SharedArrayBuffer" == e
				);
			},
			K = function (t, e) {
				return (
					G(t) && "symbol" != typeof e && e in t && String(+e) == String(e)
				);
			},
			q = function (t, e) {
				return K(t, (e = g(e, !0))) ? l(2, t[e]) : R(t, e);
			},
			Y = function (t, e, r) {
				return !(K(t, (e = g(e, !0))) && y(r) && v(r, "value")) ||
					v(r, "get") ||
					v(r, "set") ||
					r.configurable ||
					(v(r, "writable") && !r.writable) ||
					(v(r, "enumerable") && !r.enumerable)
					? C(t, e, r)
					: ((t[e] = r.value), t);
			};
		i
			? (U ||
					((w.f = q),
					(A.f = Y),
					H(j, "buffer"),
					H(j, "byteOffset"),
					H(j, "byteLength"),
					H(j, "length")),
			  n(
					{ target: "Object", stat: !0, forced: !U },
					{ getOwnPropertyDescriptor: q, defineProperty: Y }
			  ),
			  (t.exports = function (t, e, r, i) {
					var s = t + (i ? "Clamped" : "") + "Array",
						c = "get" + t,
						l = "set" + t,
						g = o[s],
						v = g,
						m = v && v.prototype,
						b = {},
						A = function (t, r) {
							C(t, r, {
								get: function () {
									return (function (t, r) {
										var n = O(t);
										return n.view[c](r * e + n.byteOffset, !0);
									})(this, r);
								},
								set: function (t) {
									return (function (t, r, n) {
										var o = O(t);
										i &&
											(n =
												(n = Math.round(n)) < 0 ? 0 : n > 255 ? 255 : 255 & n),
											o.view[l](r * e + o.byteOffset, n, !0);
									})(this, r, t);
								},
								enumerable: !0,
							});
						};
					U
						? a &&
						  ((v = r(function (t, r, n, o) {
								return (
									u(t, v, s),
									y(r)
										? V(r)
											? void 0 !== o
												? new g(r, p(n, e), o)
												: void 0 !== n
												? new g(r, p(n, e))
												: new g(r)
											: G(r)
											? W(v, r)
											: T.call(v, r)
										: new g(d(r))
								);
						  })),
						  E && E(v, B),
						  N(S(g), function (t) {
								t in v || f(v, t, g[t]);
						  }),
						  (v.prototype = m))
						: ((v = r(function (t, r, n, o) {
								u(t, v, s);
								var i,
									a,
									c,
									l = 0,
									f = 0;
								if (y(r)) {
									if (!V(r)) return G(r) ? W(v, r) : T.call(v, r);
									(i = r), (f = p(n, e));
									var g = r.byteLength;
									if (void 0 === o) {
										if (g % e) throw L("Wrong length");
										if ((a = g - f) < 0) throw L("Wrong length");
									} else if ((a = h(o) * e) + f > g) throw L("Wrong length");
									c = a / e;
								} else (c = d(r)), (i = new D((a = c * e)));
								for (
									M(t, {
										buffer: i,
										byteOffset: f,
										byteLength: a,
										length: c,
										view: new P(i),
									});
									l < c;

								)
									A(t, l++);
						  })),
						  E && E(v, B),
						  (m = v.prototype = _(j))),
						m.constructor !== v && f(m, "constructor", v),
						k && f(m, k, s),
						(b[s] = v),
						n({ global: !0, forced: v != g, sham: !U }, b),
						"BYTES_PER_ELEMENT" in v || f(v, "BYTES_PER_ELEMENT", e),
						"BYTES_PER_ELEMENT" in m || f(m, "BYTES_PER_ELEMENT", e),
						I(s);
			  }))
			: (t.exports = function () {});
	},
	function (t, e, r) {
		var n = r(23),
			o = Math.max,
			i = Math.min;
		t.exports = function (t, e) {
			var r = n(t);
			return r < 0 ? o(r + e, 0) : i(r, e);
		};
	},
	function (t, e, r) {
		var n = r(9).f,
			o = r(12),
			i = r(8)("toStringTag");
		t.exports = function (t, e, r) {
			t &&
				!o((t = r ? t : t.prototype), i) &&
				n(t, i, { configurable: !0, value: e });
		};
	},
	function (t, e, r) {
		var n = r(5),
			o = r(109),
			i = r(84),
			a = r(54),
			s = r(110),
			c = r(81),
			u = r(66)("IE_PROTO"),
			l = function () {},
			f = function () {
				var t,
					e = c("iframe"),
					r = i.length;
				for (
					e.style.display = "none",
						s.appendChild(e),
						e.src = String("javascript:"),
						(t = e.contentWindow.document).open(),
						t.write("<script>document.F=Object</script>"),
						t.close(),
						f = t.F;
					r--;

				)
					delete f.prototype[i[r]];
				return f();
			};
		(t.exports =
			Object.create ||
			function (t, e) {
				var r;
				return (
					null !== t
						? ((l.prototype = n(t)),
						  (r = new l()),
						  (l.prototype = null),
						  (r[u] = t))
						: (r = f()),
					void 0 === e ? r : o(r, e)
				);
			}),
			(a[u] = !0);
	},
	function (t, e, r) {
		var n = r(8),
			o = r(35),
			i = r(14),
			a = n("unscopables"),
			s = Array.prototype;
		null == s[a] && i(s, a, o(null)),
			(t.exports = function (t) {
				s[a][t] = !0;
			});
	},
	function (t, e) {
		t.exports = function (t, e) {
			return {
				enumerable: !(1 & t),
				configurable: !(2 & t),
				writable: !(4 & t),
				value: e,
			};
		};
	},
	function (t, e) {
		t.exports = !1;
	},
	function (t, e, r) {
		var n = r(106),
			o = r(84).concat("length", "prototype");
		e.f =
			Object.getOwnPropertyNames ||
			function (t) {
				return n(t, o);
			};
	},
	function (t, e, r) {
		var n = r(27);
		t.exports =
			Array.isArray ||
			function (t) {
				return "Array" == n(t);
			};
	},
	function (t, e, r) {
		var n = r(54),
			o = r(3),
			i = r(12),
			a = r(9).f,
			s = r(53),
			c = r(57),
			u = s("meta"),
			l = 0,
			f =
				Object.isExtensible ||
				function () {
					return !0;
				},
			h = function (t) {
				a(t, u, { value: { objectID: "O" + ++l, weakData: {} } });
			},
			d = (t.exports = {
				REQUIRED: !1,
				fastKey: function (t, e) {
					if (!o(t))
						return "symbol" == typeof t
							? t
							: ("string" == typeof t ? "S" : "P") + t;
					if (!i(t, u)) {
						if (!f(t)) return "F";
						if (!e) return "E";
						h(t);
					}
					return t[u].objectID;
				},
				getWeakData: function (t, e) {
					if (!i(t, u)) {
						if (!f(t)) return !0;
						if (!e) return !1;
						h(t);
					}
					return t[u].weakData;
				},
				onFreeze: function (t) {
					return c && d.REQUIRED && f(t) && !i(t, u) && h(t), t;
				},
			});
		n[u] = !0;
	},
	function (t, e, r) {
		var n = r(19);
		t.exports = function (t, e, r) {
			if ((n(t), void 0 === e)) return t;
			switch (r) {
				case 0:
					return function () {
						return t.call(e);
					};
				case 1:
					return function (r) {
						return t.call(e, r);
					};
				case 2:
					return function (r, n) {
						return t.call(e, r, n);
					};
				case 3:
					return function (r, n, o) {
						return t.call(e, r, n, o);
					};
			}
			return function () {
				return t.apply(e, arguments);
			};
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(25),
			o = r(9),
			i = r(37);
		t.exports = function (t, e, r) {
			var a = n(e);
			a in t ? o.f(t, a, i(0, r)) : (t[a] = r);
		};
	},
	function (t, e, r) {
		var n = r(106),
			o = r(84);
		t.exports =
			Object.keys ||
			function (t) {
				return n(t, o);
			};
	},
	function (t, e, r) {
		var n = r(115);
		t.exports =
			Object.setPrototypeOf ||
			("__proto__" in {}
				? (function () {
						var t,
							e = !1,
							r = {};
						try {
							(t = Object.getOwnPropertyDescriptor(
								Object.prototype,
								"__proto__"
							).set).call(r, []),
								(e = r instanceof Array);
						} catch (t) {}
						return function (r, o) {
							return n(r, o), e ? t.call(r, o) : (r.__proto__ = o), r;
						};
				  })()
				: void 0);
	},
	function (t, e, r) {
		"use strict";
		var n = r(89),
			o = r(9),
			i = r(8),
			a = r(6),
			s = i("species");
		t.exports = function (t) {
			var e = n(t),
				r = o.f;
			a &&
				e &&
				!e[s] &&
				r(e, s, {
					configurable: !0,
					get: function () {
						return this;
					},
				});
		};
	},
	function (t, e, r) {
		var n = r(18),
			o = "[" + r(76) + "]",
			i = RegExp("^" + o + o + "*"),
			a = RegExp(o + o + "*$");
		t.exports = function (t, e) {
			return (
				(t = String(n(t))),
				1 & e && (t = t.replace(i, "")),
				2 & e && (t = t.replace(a, "")),
				t
			);
		};
	},
	function (t, e) {
		t.exports = function (t, e, r) {
			if (!(t instanceof e))
				throw TypeError("Incorrect " + (r ? r + " " : "") + "invocation");
			return t;
		};
	},
	function (t, e, r) {
		var n = r(2),
			o = r(354),
			i = r(30),
			a = r(14),
			s = r(8),
			c = s("iterator"),
			u = s("toStringTag"),
			l = i.values;
		for (var f in o) {
			var h = n[f],
				d = h && h.prototype;
			if (d) {
				if (d[c] !== l)
					try {
						a(d, c, l);
					} catch (t) {
						d[c] = l;
					}
				if ((d[u] || a(d, u, f), o[f]))
					for (var p in i)
						if (d[p] !== i[p])
							try {
								a(d, p, i[p]);
							} catch (t) {
								d[p] = i[p];
							}
			}
		}
	},
	function (t, e, r) {
		"use strict";
		var n = {}.propertyIsEnumerable,
			o = Object.getOwnPropertyDescriptor,
			i = o && !n.call({ 1: 2 }, 1);
		e.f = i
			? function (t) {
					var e = o(this, t);
					return !!e && e.enumerable;
			  }
			: n;
	},
	function (t, e, r) {
		var n = r(1),
			o = r(27),
			i = "".split;
		t.exports = n(function () {
			return !Object("z").propertyIsEnumerable(0);
		})
			? function (t) {
					return "String" == o(t) ? i.call(t, "") : Object(t);
			  }
			: Object;
	},
	function (t, e, r) {
		var n = r(2),
			o = r(82),
			i = r(38),
			a = n["__core-js_shared__"] || o("__core-js_shared__", {});
		(t.exports = function (t, e) {
			return a[t] || (a[t] = void 0 !== e ? e : {});
		})("versions", []).push({
			version: "3.1.3",
			mode: i ? "pure" : "global",
			copyright: "© 2019 Denis Pushkarev (zloirock.ru)",
		});
	},
	function (t, e) {
		var r = 0,
			n = Math.random();
		t.exports = function (t) {
			return "Symbol(".concat(
				void 0 === t ? "" : t,
				")_",
				(++r + n).toString(36)
			);
		};
	},
	function (t, e) {
		t.exports = {};
	},
	function (t, e, r) {
		var n = r(17),
			o = r(7),
			i = r(33);
		t.exports = function (t) {
			return function (e, r, a) {
				var s,
					c = n(e),
					u = o(c.length),
					l = i(a, u);
				if (t && r != r) {
					for (; u > l; ) if ((s = c[l++]) != s) return !0;
				} else
					for (; u > l; l++)
						if ((t || l in c) && c[l] === r) return t || l || 0;
				return !t && -1;
			};
		};
	},
	function (t, e, r) {
		var n = r(1),
			o = /#|\.prototype\./,
			i = function (t, e) {
				var r = s[a(t)];
				return r == u || (r != c && ("function" == typeof e ? n(e) : !!e));
			},
			a = (i.normalize = function (t) {
				return String(t).replace(o, ".").toLowerCase();
			}),
			s = (i.data = {}),
			c = (i.NATIVE = "N"),
			u = (i.POLYFILL = "P");
		t.exports = i;
	},
	function (t, e, r) {
		var n = r(1);
		t.exports = !n(function () {
			return Object.isExtensible(Object.preventExtensions({}));
		});
	},
	function (t, e, r) {
		var n = r(5),
			o = r(85),
			i = r(7),
			a = r(42),
			s = r(86),
			c = r(113),
			u = {};
		(t.exports = function (t, e, r, l, f) {
			var h,
				d,
				p,
				g,
				v,
				m = a(e, r, l ? 2 : 1);
			if (f) h = t;
			else {
				if ("function" != typeof (d = s(t)))
					throw TypeError("Target is not iterable");
				if (o(d)) {
					for (p = 0, g = i(t.length); g > p; p++)
						if ((l ? m(n((v = t[p]))[0], v[1]) : m(t[p])) === u) return u;
					return;
				}
				h = d.call(t);
			}
			for (; !(v = h.next()).done; ) if (c(h, m, v.value, l) === u) return u;
		}).BREAK = u;
	},
	function (t, e) {
		t.exports = {};
	},
	function (t, e, r) {
		var n = r(27),
			o = r(8)("toStringTag"),
			i =
				"Arguments" ==
				n(
					(function () {
						return arguments;
					})()
				);
		t.exports = function (t) {
			var e, r, a;
			return void 0 === t
				? "Undefined"
				: null === t
				? "Null"
				: "string" ==
				  typeof (r = (function (t, e) {
						try {
							return t[e];
						} catch (t) {}
				  })((e = Object(t)), o))
				? r
				: i
				? n(e)
				: "Object" == (a = n(e)) && "function" == typeof e.callee
				? "Arguments"
				: a;
		};
	},
	function (t, e, r) {
		var n = r(3),
			o = r(40),
			i = r(8)("species");
		t.exports = function (t, e) {
			var r;
			return (
				o(t) &&
					("function" != typeof (r = t.constructor) ||
					(r !== Array && !o(r.prototype))
						? n(r) && null === (r = r[i]) && (r = void 0)
						: (r = void 0)),
				new (void 0 === r ? Array : r)(0 === e ? 0 : e)
			);
		};
	},
	function (t, e, r) {
		var n = r(1),
			o = r(8)("species");
		t.exports = function (t) {
			return !n(function () {
				var e = [];
				return (
					((e.constructor = {})[o] = function () {
						return { foo: 1 };
					}),
					1 !== e[t](Boolean).foo
				);
			});
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(5);
		t.exports = function () {
			var t = n(this),
				e = "";
			return (
				t.global && (e += "g"),
				t.ignoreCase && (e += "i"),
				t.multiline && (e += "m"),
				t.unicode && (e += "u"),
				t.sticky && (e += "y"),
				e
			);
		};
	},
	function (t, e, r) {
		var n = r(22);
		t.exports = function (t, e, r) {
			for (var o in e) n(t, o, e[o], r);
			return t;
		};
	},
	function (t, e, r) {
		"use strict";
		r.d(e, "a", function () {
			return n;
		});
		r(30), r(49);
		class n {
			constructor() {
				this.listeners = [];
			}
			addListener(t) {
				this.listeners.push(t);
			}
			removeListener(t) {
				var e = this.listeners.indexOf(t);
				e > -1 && this.listeners.splice(e, 1);
			}
			hasListener(t) {
				return this.listeners.indexOf(t) > -1;
			}
			executeListeners(t) {
				for (const e of this.listeners) e(t);
			}
		}
	},
	function (t, e, r) {
		var n = r(52),
			o = r(53),
			i = n("keys");
		t.exports = function (t) {
			return i[t] || (i[t] = o(t));
		};
	},
	function (t, e) {
		e.f = Object.getOwnPropertySymbols;
	},
	function (t, e, r) {
		t.exports = r(2);
	},
	function (t, e, r) {
		"use strict";
		var n = r(38),
			o = r(2),
			i = r(1);
		t.exports =
			n ||
			!i(function () {
				var t = Math.random();
				__defineSetter__.call(null, t, function () {}), delete o[t];
			});
	},
	function (t, e, r) {
		var n = r(8)("iterator"),
			o = !1;
		try {
			var i = 0,
				a = {
					next: function () {
						return { done: !!i++ };
					},
					return: function () {
						o = !0;
					},
				};
			(a[n] = function () {
				return this;
			}),
				Array.from(a, function () {
					throw 2;
				});
		} catch (t) {}
		t.exports = function (t, e) {
			if (!e && !o) return !1;
			var r = !1;
			try {
				var i = {};
				(i[n] = function () {
					return {
						next: function () {
							return { done: (r = !0) };
						},
					};
				}),
					t(i);
			} catch (t) {}
			return r;
		};
	},
	function (t, e, r) {
		var n = r(19),
			o = r(10),
			i = r(51),
			a = r(7);
		t.exports = function (t, e, r, s, c) {
			n(e);
			var u = o(t),
				l = i(u),
				f = a(u.length),
				h = c ? f - 1 : 0,
				d = c ? -1 : 1;
			if (r < 2)
				for (;;) {
					if (h in l) {
						(s = l[h]), (h += d);
						break;
					}
					if (((h += d), c ? h < 0 : f <= h))
						throw TypeError("Reduce of empty array with no initial value");
				}
			for (; c ? h >= 0 : f > h; h += d) h in l && (s = e(s, l[h], h, u));
			return s;
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(14),
			o = r(22),
			i = r(1),
			a = r(8),
			s = r(73),
			c = a("species"),
			u = !i(function () {
				var t = /./;
				return (
					(t.exec = function () {
						var t = [];
						return (t.groups = { a: "7" }), t;
					}),
					"7" !== "".replace(t, "$<a>")
				);
			}),
			l = !i(function () {
				var t = /(?:)/,
					e = t.exec;
				t.exec = function () {
					return e.apply(this, arguments);
				};
				var r = "ab".split(t);
				return 2 !== r.length || "a" !== r[0] || "b" !== r[1];
			});
		t.exports = function (t, e, r, f) {
			var h = a(t),
				d = !i(function () {
					var e = {};
					return (
						(e[h] = function () {
							return 7;
						}),
						7 != ""[t](e)
					);
				}),
				p =
					d &&
					!i(function () {
						var e = !1,
							r = /a/;
						return (
							(r.exec = function () {
								return (e = !0), null;
							}),
							"split" === t &&
								((r.constructor = {}),
								(r.constructor[c] = function () {
									return r;
								})),
							r[h](""),
							!e
						);
					});
			if (!d || !p || ("replace" === t && !u) || ("split" === t && !l)) {
				var g = /./[h],
					v = r(h, ""[t], function (t, e, r, n, o) {
						return e.exec === s
							? d && !o
								? { done: !0, value: g.call(e, r, n) }
								: { done: !0, value: t.call(r, e, n) }
							: { done: !1 };
					}),
					m = v[0],
					y = v[1];
				o(String.prototype, t, m),
					o(
						RegExp.prototype,
						h,
						2 == e
							? function (t, e) {
									return y.call(t, this, e);
							  }
							: function (t) {
									return y.call(t, this);
							  }
					),
					f && n(RegExp.prototype[h], "sham", !0);
			}
		};
	},
	function (t, e, r) {
		"use strict";
		var n,
			o,
			i = r(63),
			a = RegExp.prototype.exec,
			s = String.prototype.replace,
			c = a,
			u =
				((n = /a/),
				(o = /b*/g),
				a.call(n, "a"),
				a.call(o, "a"),
				0 !== n.lastIndex || 0 !== o.lastIndex),
			l = void 0 !== /()??/.exec("")[1];
		(u || l) &&
			(c = function (t) {
				var e,
					r,
					n,
					o,
					c = this;
				return (
					l && (r = new RegExp("^" + c.source + "$(?!\\s)", i.call(c))),
					u && (e = c.lastIndex),
					(n = a.call(c, t)),
					u && n && (c.lastIndex = c.global ? n.index + n[0].length : e),
					l &&
						n &&
						n.length > 1 &&
						s.call(n[0], r, function () {
							for (o = 1; o < arguments.length - 2; o++)
								void 0 === arguments[o] && (n[o] = void 0);
						}),
					n
				);
			}),
			(t.exports = c);
	},
	function (t, e, r) {
		"use strict";
		var n = r(91);
		t.exports = function (t, e, r) {
			return e + (r ? n(t, e, !0).length : 1);
		};
	},
	function (t, e, r) {
		var n = r(27),
			o = r(73);
		t.exports = function (t, e) {
			var r = t.exec;
			if ("function" == typeof r) {
				var i = r.call(t, e);
				if ("object" != typeof i)
					throw TypeError(
						"RegExp exec method returned something other than an Object or null"
					);
				return i;
			}
			if ("RegExp" !== n(t))
				throw TypeError("RegExp#exec called on incompatible receiver");
			return o.call(t, e);
		};
	},
	function (t, e) {
		t.exports = "\t\n\v\f\r                　\u2028\u2029\ufeff";
	},
	function (t, e) {
		var r = Math.expm1;
		t.exports =
			!r ||
			r(10) > 22025.465794806718 ||
			r(10) < 22025.465794806718 ||
			-2e-17 != r(-2e-17)
				? function (t) {
						return 0 == (t = +t)
							? t
							: t > -1e-6 && t < 1e-6
							? t + (t * t) / 2
							: Math.exp(t) - 1;
				  }
				: r;
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(2),
			i = r(56),
			a = r(22),
			s = r(41),
			c = r(58),
			u = r(48),
			l = r(3),
			f = r(1),
			h = r(70),
			d = r(34),
			p = r(98);
		t.exports = function (t, e, r, g, v) {
			var m = o[t],
				y = m && m.prototype,
				_ = m,
				E = g ? "set" : "add",
				S = {},
				T = function (t) {
					var e = y[t];
					a(
						y,
						t,
						"add" == t
							? function (t) {
									return e.call(this, 0 === t ? 0 : t), this;
							  }
							: "delete" == t
							? function (t) {
									return !(v && !l(t)) && e.call(this, 0 === t ? 0 : t);
							  }
							: "get" == t
							? function (t) {
									return v && !l(t) ? void 0 : e.call(this, 0 === t ? 0 : t);
							  }
							: "has" == t
							? function (t) {
									return !(v && !l(t)) && e.call(this, 0 === t ? 0 : t);
							  }
							: function (t, r) {
									return e.call(this, 0 === t ? 0 : t, r), this;
							  }
					);
				};
			if (
				i(
					t,
					"function" != typeof m ||
						!(
							v ||
							(y.forEach &&
								!f(function () {
									new m().entries().next();
								}))
						)
				)
			)
				(_ = r.getConstructor(e, t, g, E)), (s.REQUIRED = !0);
			else if (i(t, !0)) {
				var b = new _(),
					I = b[E](v ? {} : -0, 1) != b,
					A = f(function () {
						b.has(1);
					}),
					w = h(function (t) {
						new m(t);
					}),
					x =
						!v &&
						f(function () {
							for (var t = new m(), e = 5; e--; ) t[E](e, e);
							return !t.has(-0);
						});
				w ||
					(((_ = e(function (e, r) {
						u(e, _, t);
						var n = p(new m(), e, _);
						return null != r && c(r, n[E], n, g), n;
					})).prototype = y),
					(y.constructor = _)),
					(A || x) && (T("delete"), T("has"), g && T("get")),
					(x || I) && T(E),
					v && y.clear && delete y.clear;
			}
			return (
				(S[t] = _),
				n({ global: !0, forced: _ != m }, S),
				d(_, t),
				v || r.setStrong(_, t, g),
				_
			);
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(2),
			o = r(6),
			i = r(4).NATIVE_ARRAY_BUFFER,
			a = r(14),
			s = r(64),
			c = r(1),
			u = r(48),
			l = r(23),
			f = r(7),
			h = r(134),
			d = r(39).f,
			p = r(9).f,
			g = r(88),
			v = r(34),
			m = r(26),
			y = m.get,
			_ = m.set,
			E = n.ArrayBuffer,
			S = E,
			T = n.DataView,
			b = n.Math,
			I = n.RangeError,
			A = b.abs,
			w = b.pow,
			x = b.floor,
			O = b.log,
			M = b.LN2,
			C = function (t, e, r) {
				var n,
					o,
					i,
					a = new Array(r),
					s = 8 * r - e - 1,
					c = (1 << s) - 1,
					u = c >> 1,
					l = 23 === e ? w(2, -24) - w(2, -77) : 0,
					f = t < 0 || (0 === t && 1 / t < 0) ? 1 : 0,
					h = 0;
				for (
					(t = A(t)) != t || t === 1 / 0
						? ((o = t != t ? 1 : 0), (n = c))
						: ((n = x(O(t) / M)),
						  t * (i = w(2, -n)) < 1 && (n--, (i *= 2)),
						  (t += n + u >= 1 ? l / i : l * w(2, 1 - u)) * i >= 2 &&
								(n++, (i /= 2)),
						  n + u >= c
								? ((o = 0), (n = c))
								: n + u >= 1
								? ((o = (t * i - 1) * w(2, e)), (n += u))
								: ((o = t * w(2, u - 1) * w(2, e)), (n = 0)));
					e >= 8;
					a[h++] = 255 & o, o /= 256, e -= 8
				);
				for (
					n = (n << e) | o, s += e;
					s > 0;
					a[h++] = 255 & n, n /= 256, s -= 8
				);
				return (a[--h] |= 128 * f), a;
			},
			R = function (t, e) {
				var r,
					n = t.length,
					o = 8 * n - e - 1,
					i = (1 << o) - 1,
					a = i >> 1,
					s = o - 7,
					c = n - 1,
					u = t[c--],
					l = 127 & u;
				for (u >>= 7; s > 0; l = 256 * l + t[c], c--, s -= 8);
				for (
					r = l & ((1 << -s) - 1), l >>= -s, s += e;
					s > 0;
					r = 256 * r + t[c], c--, s -= 8
				);
				if (0 === l) l = 1 - a;
				else {
					if (l === i) return r ? NaN : u ? -1 / 0 : 1 / 0;
					(r += w(2, e)), (l -= a);
				}
				return (u ? -1 : 1) * r * w(2, l - e);
			},
			N = function (t) {
				return (t[3] << 24) | (t[2] << 16) | (t[1] << 8) | t[0];
			},
			L = function (t) {
				return [255 & t];
			},
			D = function (t) {
				return [255 & t, (t >> 8) & 255];
			},
			P = function (t) {
				return [255 & t, (t >> 8) & 255, (t >> 16) & 255, (t >> 24) & 255];
			},
			U = function (t) {
				return C(t, 23, 4);
			},
			k = function (t) {
				return C(t, 52, 8);
			},
			B = function (t, e) {
				p(t.prototype, e, {
					get: function () {
						return y(this)[e];
					},
				});
			},
			j = function (t, e, r, n) {
				var o = h(+r),
					i = y(t);
				if (o + e > i.byteLength) throw I("Wrong index");
				var a = y(i.buffer).bytes,
					s = o + i.byteOffset,
					c = a.slice(s, s + e);
				return n ? c : c.reverse();
			},
			F = function (t, e, r, n, o, i) {
				var a = h(+r),
					s = y(t);
				if (a + e > s.byteLength) throw I("Wrong index");
				for (
					var c = y(s.buffer).bytes, u = a + s.byteOffset, l = n(+o), f = 0;
					f < e;
					f++
				)
					c[u + f] = l[i ? f : e - f - 1];
			};
		if (i) {
			if (
				!c(function () {
					E(1);
				}) ||
				!c(function () {
					new E(-1);
				}) ||
				c(function () {
					return new E(), new E(1.5), new E(NaN), "ArrayBuffer" != E.name;
				})
			) {
				for (
					var G,
						W = ((S = function (t) {
							return u(this, S), new E(h(t));
						}).prototype = E.prototype),
						H = d(E),
						V = 0;
					H.length > V;

				)
					(G = H[V++]) in S || a(S, G, E[G]);
				W.constructor = S;
			}
			var K = new T(new S(2)),
				q = T.prototype.setInt8;
			K.setInt8(0, 2147483648),
				K.setInt8(1, 2147483649),
				(!K.getInt8(0) && K.getInt8(1)) ||
					s(
						T.prototype,
						{
							setInt8: function (t, e) {
								q.call(this, t, (e << 24) >> 24);
							},
							setUint8: function (t, e) {
								q.call(this, t, (e << 24) >> 24);
							},
						},
						{ unsafe: !0 }
					);
		} else
			(S = function (t) {
				u(this, S, "ArrayBuffer");
				var e = h(t);
				_(this, { bytes: g.call(new Array(e), 0), byteLength: e }),
					o || (this.byteLength = e);
			}),
				(T = function (t, e, r) {
					u(this, T, "DataView"), u(t, S, "DataView");
					var n = y(t).byteLength,
						i = l(e);
					if (i < 0 || i > n) throw I("Wrong offset");
					if (i + (r = void 0 === r ? n - i : f(r)) > n)
						throw I("Wrong length");
					_(this, { buffer: t, byteLength: r, byteOffset: i }),
						o ||
							((this.buffer = t), (this.byteLength = r), (this.byteOffset = i));
				}),
				o &&
					(B(S, "byteLength"),
					B(T, "buffer"),
					B(T, "byteLength"),
					B(T, "byteOffset")),
				s(T.prototype, {
					getInt8: function (t) {
						return (j(this, 1, t)[0] << 24) >> 24;
					},
					getUint8: function (t) {
						return j(this, 1, t)[0];
					},
					getInt16: function (t) {
						var e = j(this, 2, t, arguments[1]);
						return (((e[1] << 8) | e[0]) << 16) >> 16;
					},
					getUint16: function (t) {
						var e = j(this, 2, t, arguments[1]);
						return (e[1] << 8) | e[0];
					},
					getInt32: function (t) {
						return N(j(this, 4, t, arguments[1]));
					},
					getUint32: function (t) {
						return N(j(this, 4, t, arguments[1])) >>> 0;
					},
					getFloat32: function (t) {
						return R(j(this, 4, t, arguments[1]), 23);
					},
					getFloat64: function (t) {
						return R(j(this, 8, t, arguments[1]), 52);
					},
					setInt8: function (t, e) {
						F(this, 1, t, L, e);
					},
					setUint8: function (t, e) {
						F(this, 1, t, L, e);
					},
					setInt16: function (t, e) {
						F(this, 2, t, D, e, arguments[2]);
					},
					setUint16: function (t, e) {
						F(this, 2, t, D, e, arguments[2]);
					},
					setInt32: function (t, e) {
						F(this, 4, t, P, e, arguments[2]);
					},
					setUint32: function (t, e) {
						F(this, 4, t, P, e, arguments[2]);
					},
					setFloat32: function (t, e) {
						F(this, 4, t, U, e, arguments[2]);
					},
					setFloat64: function (t, e) {
						F(this, 8, t, k, e, arguments[2]);
					},
				});
		v(S, "ArrayBuffer"),
			v(T, "DataView"),
			(e.ArrayBuffer = S),
			(e.DataView = T);
	},
	function (t, e, r) {
		"use strict";
		r.d(e, "a", function () {
			return n;
		});
		r(30), r(49);
		class n {
			constructor() {
				this.listeners = [];
			}
			addListener(t) {
				this.listeners.push(t);
			}
			removeListener(t) {
				var e = this.listeners.indexOf(t);
				e > -1 && this.listeners.splice(e, 1);
			}
			hasListener(t) {
				return this.listeners.indexOf(t) > -1;
			}
			executeListeners(t) {
				for (const e of this.listeners) e(t);
			}
		}
	},
	function (t, e, r) {
		var n = r(2),
			o = r(3),
			i = n.document,
			a = o(i) && o(i.createElement);
		t.exports = function (t) {
			return a ? i.createElement(t) : {};
		};
	},
	function (t, e, r) {
		var n = r(2),
			o = r(14);
		t.exports = function (t, e) {
			try {
				o(n, t, e);
			} catch (r) {
				n[t] = e;
			}
			return e;
		};
	},
	function (t, e, r) {
		var n = r(2),
			o = r(39),
			i = r(67),
			a = r(5),
			s = n.Reflect;
		t.exports =
			(s && s.ownKeys) ||
			function (t) {
				var e = o.f(a(t)),
					r = i.f;
				return r ? e.concat(r(t)) : e;
			};
	},
	function (t, e) {
		t.exports = [
			"constructor",
			"hasOwnProperty",
			"isPrototypeOf",
			"propertyIsEnumerable",
			"toLocaleString",
			"toString",
			"valueOf",
		];
	},
	function (t, e, r) {
		var n = r(8),
			o = r(59),
			i = n("iterator"),
			a = Array.prototype;
		t.exports = function (t) {
			return void 0 !== t && (o.Array === t || a[i] === t);
		};
	},
	function (t, e, r) {
		var n = r(60),
			o = r(59),
			i = r(8)("iterator");
		t.exports = function (t) {
			if (null != t) return t[i] || t["@@iterator"] || o[n(t)];
		};
	},
	function (t, e, r) {
		var n = r(1);
		t.exports = !n(function () {
			function t() {}
			return (
				(t.prototype.constructor = null),
				Object.getPrototypeOf(new t()) !== t.prototype
			);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(10),
			o = r(33),
			i = r(7);
		t.exports = function (t) {
			for (
				var e = n(this),
					r = i(e.length),
					a = arguments.length,
					s = o(a > 1 ? arguments[1] : void 0, r),
					c = a > 2 ? arguments[2] : void 0,
					u = void 0 === c ? r : o(c, r);
				u > s;

			)
				e[s++] = t;
			return e;
		};
	},
	function (t, e, r) {
		var n = r(68),
			o = r(2),
			i = function (t) {
				return "function" == typeof t ? t : void 0;
			};
		t.exports = function (t, e) {
			return arguments.length < 2
				? i(n[t]) || i(o[t])
				: (n[t] && n[t][e]) || (o[t] && o[t][e]);
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(120),
			i = r(28),
			a = r(45),
			s = r(34),
			c = r(14),
			u = r(22),
			l = r(8),
			f = r(38),
			h = r(59),
			d = r(121),
			p = d.IteratorPrototype,
			g = d.BUGGY_SAFARI_ITERATORS,
			v = l("iterator"),
			m = function () {
				return this;
			};
		t.exports = function (t, e, r, l, d, y, _) {
			o(r, e, l);
			var E,
				S,
				T,
				b = function (t) {
					if (t === d && O) return O;
					if (!g && t in w) return w[t];
					switch (t) {
						case "keys":
						case "values":
						case "entries":
							return function () {
								return new r(this, t);
							};
					}
					return function () {
						return new r(this);
					};
				},
				I = e + " Iterator",
				A = !1,
				w = t.prototype,
				x = w[v] || w["@@iterator"] || (d && w[d]),
				O = (!g && x) || b(d),
				M = ("Array" == e && w.entries) || x;
			if (
				(M &&
					((E = i(M.call(new t()))),
					p !== Object.prototype &&
						E.next &&
						(f ||
							i(E) === p ||
							(a ? a(E, p) : "function" != typeof E[v] && c(E, v, m)),
						s(E, I, !0, !0),
						f && (h[I] = m))),
				"values" == d &&
					x &&
					"values" !== x.name &&
					((A = !0),
					(O = function () {
						return x.call(this);
					})),
				(f && !_) || w[v] === O || c(w, v, O),
				(h[e] = O),
				d)
			)
				if (
					((S = {
						values: b("values"),
						keys: y ? O : b("keys"),
						entries: b("entries"),
					}),
					_)
				)
					for (T in S) (!g && !A && T in w) || u(w, T, S[T]);
				else n({ target: e, proto: !0, forced: g || A }, S);
			return S;
		};
	},
	function (t, e, r) {
		var n = r(23),
			o = r(18);
		t.exports = function (t, e, r) {
			var i,
				a,
				s = String(o(t)),
				c = n(e),
				u = s.length;
			return c < 0 || c >= u
				? r
					? ""
					: void 0
				: (i = s.charCodeAt(c)) < 55296 ||
				  i > 56319 ||
				  c + 1 === u ||
				  (a = s.charCodeAt(c + 1)) < 56320 ||
				  a > 57343
				? r
					? s.charAt(c)
					: i
				: r
				? s.slice(c, c + 2)
				: a - 56320 + ((i - 55296) << 10) + 65536;
		};
	},
	function (t, e, r) {
		var n = r(93),
			o = r(18);
		t.exports = function (t, e, r) {
			if (n(e))
				throw TypeError("String.prototype." + r + " doesn't accept regex");
			return String(o(t));
		};
	},
	function (t, e, r) {
		var n = r(3),
			o = r(27),
			i = r(8)("match");
		t.exports = function (t) {
			var e;
			return n(t) && (void 0 !== (e = t[i]) ? !!e : "RegExp" == o(t));
		};
	},
	function (t, e, r) {
		var n = r(8)("match");
		t.exports = function (t) {
			var e = /./;
			try {
				"/./"[t](e);
			} catch (r) {
				try {
					return (e[n] = !1), "/./"[t](e);
				} catch (t) {}
			}
			return !1;
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(23),
			o = r(18);
		t.exports =
			"".repeat ||
			function (t) {
				var e = String(o(this)),
					r = "",
					i = n(t);
				if (i < 0 || i == 1 / 0)
					throw RangeError("Wrong number of repetitions");
				for (; i > 0; (i >>>= 1) && (e += e)) 1 & i && (r += e);
				return r;
			};
	},
	function (t, e, r) {
		var n = r(2).navigator;
		t.exports = (n && n.userAgent) || "";
	},
	function (t, e, r) {
		var n = r(1),
			o = r(76);
		t.exports = function (t) {
			return n(function () {
				return !!o[t]() || "​᠎" != "​᠎"[t]() || o[t].name !== t;
			});
		};
	},
	function (t, e, r) {
		var n = r(3),
			o = r(45);
		t.exports = function (t, e, r) {
			var i,
				a = e.constructor;
			return (
				a !== r &&
					"function" == typeof a &&
					(i = a.prototype) !== r.prototype &&
					n(i) &&
					o &&
					o(t, i),
				t
			);
		};
	},
	function (t, e) {
		t.exports =
			Math.sign ||
			function (t) {
				return 0 == (t = +t) || t != t ? t : t < 0 ? -1 : 1;
			};
	},
	function (t, e, r) {
		"use strict";
		var n,
			o,
			i,
			a = r(0),
			s = r(38),
			c = r(2),
			u = r(68),
			l = r(64),
			f = r(34),
			h = r(46),
			d = r(3),
			p = r(19),
			g = r(48),
			v = r(27),
			m = r(58),
			y = r(70),
			_ = r(31),
			E = r(129).set,
			S = r(294),
			T = r(130),
			b = r(295),
			I = r(131),
			A = r(296),
			w = r(96),
			x = r(26),
			O = r(56),
			M = r(8)("species"),
			C = "Promise",
			R = x.get,
			N = x.set,
			L = x.getterFor(C),
			D = c.Promise,
			P = c.TypeError,
			U = c.document,
			k = c.process,
			B = c.fetch,
			j = k && k.versions,
			F = (j && j.v8) || "",
			G = I.f,
			W = G,
			H = "process" == v(k),
			V = !!(U && U.createEvent && c.dispatchEvent),
			K = O(C, function () {
				var t = D.resolve(1),
					e = function () {},
					r = ((t.constructor = {})[M] = function (t) {
						t(e, e);
					});
				return !(
					(H || "function" == typeof PromiseRejectionEvent) &&
					(!s || t.finally) &&
					t.then(e) instanceof r &&
					0 !== F.indexOf("6.6") &&
					-1 === w.indexOf("Chrome/66")
				);
			}),
			q =
				K ||
				!y(function (t) {
					D.all(t).catch(function () {});
				}),
			Y = function (t) {
				var e;
				return !(!d(t) || "function" != typeof (e = t.then)) && e;
			},
			z = function (t, e, r) {
				if (!e.notified) {
					e.notified = !0;
					var n = e.reactions;
					S(function () {
						for (
							var o = e.value,
								i = 1 == e.state,
								a = 0,
								s = function (r) {
									var n,
										a,
										s,
										c = i ? r.ok : r.fail,
										u = r.resolve,
										l = r.reject,
										f = r.domain;
									try {
										c
											? (i || (2 === e.rejection && Q(t, e), (e.rejection = 1)),
											  !0 === c
													? (n = o)
													: (f && f.enter(),
													  (n = c(o)),
													  f && (f.exit(), (s = !0))),
											  n === r.promise
													? l(P("Promise-chain cycle"))
													: (a = Y(n))
													? a.call(n, u, l)
													: u(n))
											: l(o);
									} catch (t) {
										f && !s && f.exit(), l(t);
									}
								};
							n.length > a;

						)
							s(n[a++]);
						(e.reactions = []), (e.notified = !1), r && !e.rejection && X(t, e);
					});
				}
			},
			J = function (t, e, r) {
				var n, o;
				V
					? (((n = U.createEvent("Event")).promise = e),
					  (n.reason = r),
					  n.initEvent(t, !1, !0),
					  c.dispatchEvent(n))
					: (n = { promise: e, reason: r }),
					(o = c["on" + t])
						? o(n)
						: "unhandledrejection" === t && b("Unhandled promise rejection", r);
			},
			X = function (t, e) {
				E.call(c, function () {
					var r,
						n = e.value;
					if (
						$(e) &&
						((r = A(function () {
							H
								? k.emit("unhandledRejection", n, t)
								: J("unhandledrejection", t, n);
						})),
						(e.rejection = H || $(e) ? 2 : 1),
						r.error)
					)
						throw r.value;
				});
			},
			$ = function (t) {
				return 1 !== t.rejection && !t.parent;
			},
			Q = function (t, e) {
				E.call(c, function () {
					H ? k.emit("rejectionHandled", t) : J("rejectionhandled", t, e.value);
				});
			},
			Z = function (t, e, r, n) {
				return function (o) {
					t(e, r, o, n);
				};
			},
			tt = function (t, e, r, n) {
				e.done ||
					((e.done = !0),
					n && (e = n),
					(e.value = r),
					(e.state = 2),
					z(t, e, !0));
			},
			et = function (t, e, r, n) {
				if (!e.done) {
					(e.done = !0), n && (e = n);
					try {
						if (t === r) throw P("Promise can't be resolved itself");
						var o = Y(r);
						o
							? S(function () {
									var n = { done: !1 };
									try {
										o.call(r, Z(et, t, n, e), Z(tt, t, n, e));
									} catch (r) {
										tt(t, n, r, e);
									}
							  })
							: ((e.value = r), (e.state = 1), z(t, e, !1));
					} catch (r) {
						tt(t, { done: !1 }, r, e);
					}
				}
			};
		K &&
			((D = function (t) {
				g(this, D, C), p(t), n.call(this);
				var e = R(this);
				try {
					t(Z(et, this, e), Z(tt, this, e));
				} catch (t) {
					tt(this, e, t);
				}
			}),
			((n = function (t) {
				N(this, {
					type: C,
					done: !1,
					notified: !1,
					parent: !1,
					reactions: [],
					rejection: !1,
					state: 0,
					value: void 0,
				});
			}).prototype = l(D.prototype, {
				then: function (t, e) {
					var r = L(this),
						n = G(_(this, D));
					return (
						(n.ok = "function" != typeof t || t),
						(n.fail = "function" == typeof e && e),
						(n.domain = H ? k.domain : void 0),
						(r.parent = !0),
						r.reactions.push(n),
						0 != r.state && z(this, r, !1),
						n.promise
					);
				},
				catch: function (t) {
					return this.then(void 0, t);
				},
			})),
			(o = function () {
				var t = new n(),
					e = R(t);
				(this.promise = t),
					(this.resolve = Z(et, t, e)),
					(this.reject = Z(tt, t, e));
			}),
			(I.f = G = function (t) {
				return t === D || t === i ? new o(t) : W(t);
			}),
			s ||
				"function" != typeof B ||
				a(
					{ global: !0, enumerable: !0, forced: !0 },
					{
						fetch: function (t) {
							return T(D, B.apply(c, arguments));
						},
					}
				)),
			a({ global: !0, wrap: !0, forced: K }, { Promise: D }),
			f(D, C, !1, !0),
			h(C),
			(i = u.Promise),
			a(
				{ target: C, stat: !0, forced: K },
				{
					reject: function (t) {
						var e = G(this);
						return e.reject.call(void 0, t), e.promise;
					},
				}
			),
			a(
				{ target: C, stat: !0, forced: s || K },
				{
					resolve: function (t) {
						return T(s && this === i ? D : this, t);
					},
				}
			),
			a(
				{ target: C, stat: !0, forced: q },
				{
					all: function (t) {
						var e = this,
							r = G(e),
							n = r.resolve,
							o = r.reject,
							i = A(function () {
								var r = p(e.resolve),
									i = [],
									a = 0,
									s = 1;
								m(t, function (t) {
									var c = a++,
										u = !1;
									i.push(void 0),
										s++,
										r.call(e, t).then(function (t) {
											u || ((u = !0), (i[c] = t), --s || n(i));
										}, o);
								}),
									--s || n(i);
							});
						return i.error && o(i.value), r.promise;
					},
					race: function (t) {
						var e = this,
							r = G(e),
							n = r.reject,
							o = A(function () {
								var o = p(e.resolve);
								m(t, function (t) {
									o.call(e, t).then(r.resolve, n);
								});
							});
						return o.error && n(o.value), r.promise;
					},
				}
			);
	},
	function (t, e, r) {
		var n = r(2),
			o = r(1),
			i = r(70),
			a = r(4).NATIVE_ARRAY_BUFFER_VIEWS,
			s = n.ArrayBuffer,
			c = n.Int8Array;
		t.exports =
			!a ||
			!o(function () {
				c(1);
			}) ||
			!o(function () {
				new c(-1);
			}) ||
			!i(function (t) {
				new c(), new c(null), new c(1.5), new c(t);
			}, !0) ||
			o(function () {
				return 1 !== new c(new s(2), 1, void 0).length;
			});
	},
	function (t, e, r) {
		var n = r(6),
			o = r(1),
			i = r(81);
		t.exports =
			!n &&
			!o(function () {
				return (
					7 !=
					Object.defineProperty(i("div"), "a", {
						get: function () {
							return 7;
						},
					}).a
				);
			});
	},
	function (t, e, r) {
		var n = r(52);
		t.exports = n("native-function-to-string", Function.toString);
	},
	function (t, e, r) {
		var n = r(2),
			o = r(103),
			i = n.WeakMap;
		t.exports = "function" == typeof i && /native code/.test(o.call(i));
	},
	function (t, e, r) {
		var n = r(12),
			o = r(83),
			i = r(15),
			a = r(9);
		t.exports = function (t, e) {
			for (var r = o(e), s = a.f, c = i.f, u = 0; u < r.length; u++) {
				var l = r[u];
				n(t, l) || s(t, l, c(e, l));
			}
		};
	},
	function (t, e, r) {
		var n = r(12),
			o = r(17),
			i = r(55),
			a = r(54),
			s = i(!1);
		t.exports = function (t, e) {
			var r,
				i = o(t),
				c = 0,
				u = [];
			for (r in i) !n(a, r) && n(i, r) && u.push(r);
			for (; e.length > c; ) n(i, (r = e[c++])) && (~s(u, r) || u.push(r));
			return u;
		};
	},
	function (t, e, r) {
		var n = r(1);
		t.exports =
			!!Object.getOwnPropertySymbols &&
			!n(function () {
				return !String(Symbol());
			});
	},
	function (t, e, r) {
		e.f = r(8);
	},
	function (t, e, r) {
		var n = r(6),
			o = r(9),
			i = r(5),
			a = r(44);
		t.exports = n
			? Object.defineProperties
			: function (t, e) {
					i(t);
					for (var r, n = a(e), s = n.length, c = 0; s > c; )
						o.f(t, (r = n[c++]), e[r]);
					return t;
			  };
	},
	function (t, e, r) {
		var n = r(2).document;
		t.exports = n && n.documentElement;
	},
	function (t, e, r) {
		var n = r(17),
			o = r(39).f,
			i = {}.toString,
			a =
				"object" == typeof window && window && Object.getOwnPropertyNames
					? Object.getOwnPropertyNames(window)
					: [];
		t.exports.f = function (t) {
			return a && "[object Window]" == i.call(t)
				? (function (t) {
						try {
							return o(t);
						} catch (t) {
							return a.slice();
						}
				  })(t)
				: o(n(t));
		};
	},
	function (t, e, r) {
		var n = r(6),
			o = r(44),
			i = r(17),
			a = r(50).f;
		t.exports = function (t, e) {
			for (var r, s = i(t), c = o(s), u = c.length, l = 0, f = []; u > l; )
				(r = c[l++]), (n && !a.call(s, r)) || f.push(e ? [r, s[r]] : s[r]);
			return f;
		};
	},
	function (t, e, r) {
		var n = r(5);
		t.exports = function (t, e, r, o) {
			try {
				return o ? e(n(r)[0], r[1]) : e(r);
			} catch (e) {
				var i = t.return;
				throw (void 0 !== i && n(i.call(t)), e);
			}
		};
	},
	function (t, e) {
		t.exports =
			Object.is ||
			function (t, e) {
				return t === e ? 0 !== t || 1 / t == 1 / e : t != t && e != e;
			};
	},
	function (t, e, r) {
		var n = r(3),
			o = r(5);
		t.exports = function (t, e) {
			if ((o(t), !n(e) && null !== e))
				throw TypeError("Can't set " + String(e) + " as a prototype");
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(19),
			o = r(3),
			i = [].slice,
			a = {},
			s = function (t, e, r) {
				if (!(e in a)) {
					for (var n = [], o = 0; o < e; o++) n[o] = "a[" + o + "]";
					a[e] = Function("C,a", "return new C(" + n.join(",") + ")");
				}
				return a[e](t, r);
			};
		t.exports =
			Function.bind ||
			function (t) {
				var e = n(this),
					r = i.call(arguments, 1),
					a = function () {
						var n = r.concat(i.call(arguments));
						return this instanceof a ? s(e, n.length, n) : e.apply(t, n);
					};
				return o(e.prototype) && (a.prototype = e.prototype), a;
			};
	},
	function (t, e, r) {
		"use strict";
		var n = r(10),
			o = r(33),
			i = r(7);
		t.exports =
			[].copyWithin ||
			function (t, e) {
				var r = n(this),
					a = i(r.length),
					s = o(t, a),
					c = o(e, a),
					u = arguments.length > 2 ? arguments[2] : void 0,
					l = Math.min((void 0 === u ? a : o(u, a)) - c, a - s),
					f = 1;
				for (
					c < s && s < c + l && ((f = -1), (c += l - 1), (s += l - 1));
					l-- > 0;

				)
					c in r ? (r[s] = r[c]) : delete r[s], (s += f), (c += f);
				return r;
			};
	},
	function (t, e, r) {
		"use strict";
		var n = r(40),
			o = r(7),
			i = r(42),
			a = function (t, e, r, s, c, u, l, f) {
				for (var h, d = c, p = 0, g = !!l && i(l, f, 3); p < s; ) {
					if (p in r) {
						if (((h = g ? g(r[p], p, e) : r[p]), u > 0 && n(h)))
							d = a(t, e, h, o(h.length), d, u - 1) - 1;
						else {
							if (d >= 9007199254740991)
								throw TypeError("Exceed the acceptable array length");
							t[d] = h;
						}
						d++;
					}
					p++;
				}
				return d;
			};
		t.exports = a;
	},
	function (t, e, r) {
		"use strict";
		var n = r(17),
			o = r(23),
			i = r(7),
			a = r(29),
			s = [].lastIndexOf,
			c = !!s && 1 / [1].lastIndexOf(1, -0) < 0,
			u = a("lastIndexOf");
		t.exports =
			c || u
				? function (t) {
						if (c) return s.apply(this, arguments) || 0;
						var e = n(this),
							r = i(e.length),
							a = r - 1;
						for (
							arguments.length > 1 && (a = Math.min(a, o(arguments[1]))),
								a < 0 && (a = r + a);
							a >= 0;
							a--
						)
							if (a in e && e[a] === t) return a || 0;
						return -1;
				  }
				: s;
	},
	function (t, e, r) {
		"use strict";
		var n = r(121).IteratorPrototype,
			o = r(35),
			i = r(37),
			a = r(34),
			s = r(59),
			c = function () {
				return this;
			};
		t.exports = function (t, e, r) {
			var u = e + " Iterator";
			return (
				(t.prototype = o(n, { next: i(1, r) })), a(t, u, !1, !0), (s[u] = c), t
			);
		};
	},
	function (t, e, r) {
		"use strict";
		var n,
			o,
			i,
			a = r(28),
			s = r(14),
			c = r(12),
			u = r(8),
			l = r(38),
			f = u("iterator"),
			h = !1;
		[].keys &&
			("next" in (i = [].keys())
				? (o = a(a(i))) !== Object.prototype && (n = o)
				: (h = !0)),
			null == n && (n = {}),
			l ||
				c(n, f) ||
				s(n, f, function () {
					return this;
				}),
			(t.exports = { IteratorPrototype: n, BUGGY_SAFARI_ITERATORS: h });
	},
	function (t, e, r) {
		var n = r(7),
			o = r(95),
			i = r(18);
		t.exports = function (t, e, r, a) {
			var s,
				c,
				u = String(i(t)),
				l = u.length,
				f = void 0 === r ? " " : String(r),
				h = n(e);
			return h <= l || "" == f
				? u
				: ((s = h - l),
				  (c = o.call(f, Math.ceil(s / f.length))).length > s &&
						(c = c.slice(0, s)),
				  a ? c + u : u + c);
		};
	},
	function (t, e, r) {
		var n = r(96);
		t.exports = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(n);
	},
	function (t, e, r) {
		var n = r(2),
			o = r(47),
			i = r(76),
			a = n.parseInt,
			s = /^[+-]?0[Xx]/,
			c = 8 !== a(i + "08") || 22 !== a(i + "0x16");
		t.exports = c
			? function (t, e) {
					var r = o(String(t), 3);
					return a(r, e >>> 0 || (s.test(r) ? 16 : 10));
			  }
			: a;
	},
	function (t, e, r) {
		var n = r(2),
			o = r(47),
			i = r(76),
			a = n.parseFloat,
			s = 1 / a(i + "-0") != -1 / 0;
		t.exports = s
			? function (t) {
					var e = o(String(t), 3),
						r = a(e);
					return 0 === r && "-" == e.charAt(0) ? -0 : r;
			  }
			: a;
	},
	function (t, e, r) {
		var n = r(3),
			o = Math.floor;
		t.exports = function (t) {
			return !n(t) && isFinite(t) && o(t) === t;
		};
	},
	function (t, e, r) {
		var n = r(27);
		t.exports = function (t) {
			if ("number" != typeof t && "Number" != n(t))
				throw TypeError("Incorrect invocation");
			return +t;
		};
	},
	function (t, e) {
		t.exports =
			Math.log1p ||
			function (t) {
				return (t = +t) > -1e-8 && t < 1e-8 ? t - (t * t) / 2 : Math.log(1 + t);
			};
	},
	function (t, e, r) {
		var n,
			o,
			i,
			a = r(2),
			s = r(1),
			c = r(27),
			u = r(42),
			l = r(110),
			f = r(81),
			h = a.location,
			d = a.setImmediate,
			p = a.clearImmediate,
			g = a.process,
			v = a.MessageChannel,
			m = a.Dispatch,
			y = 0,
			_ = {},
			E = function (t) {
				if (_.hasOwnProperty(t)) {
					var e = _[t];
					delete _[t], e();
				}
			},
			S = function (t) {
				return function () {
					E(t);
				};
			},
			T = function (t) {
				E(t.data);
			},
			b = function (t) {
				a.postMessage(t + "", h.protocol + "//" + h.host);
			};
		(d && p) ||
			((d = function (t) {
				for (var e = [], r = 1; arguments.length > r; ) e.push(arguments[r++]);
				return (
					(_[++y] = function () {
						("function" == typeof t ? t : Function(t)).apply(void 0, e);
					}),
					n(y),
					y
				);
			}),
			(p = function (t) {
				delete _[t];
			}),
			"process" == c(g)
				? (n = function (t) {
						g.nextTick(S(t));
				  })
				: m && m.now
				? (n = function (t) {
						m.now(S(t));
				  })
				: v
				? ((i = (o = new v()).port2),
				  (o.port1.onmessage = T),
				  (n = u(i.postMessage, i, 1)))
				: !a.addEventListener ||
				  "function" != typeof postMessage ||
				  a.importScripts ||
				  s(b)
				? (n =
						"onreadystatechange" in f("script")
							? function (t) {
									l.appendChild(f("script")).onreadystatechange = function () {
										l.removeChild(this), E(t);
									};
							  }
							: function (t) {
									setTimeout(S(t), 0);
							  })
				: ((n = b), a.addEventListener("message", T, !1))),
			(t.exports = { set: d, clear: p });
	},
	function (t, e, r) {
		var n = r(5),
			o = r(3),
			i = r(131);
		t.exports = function (t, e) {
			if ((n(t), o(e) && e.constructor === t)) return e;
			var r = i.f(t);
			return (0, r.resolve)(e), r.promise;
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(19),
			o = function (t) {
				var e, r;
				(this.promise = new t(function (t, n) {
					if (void 0 !== e || void 0 !== r)
						throw TypeError("Bad Promise constructor");
					(e = t), (r = n);
				})),
					(this.resolve = n(e)),
					(this.reject = n(r));
			};
		t.exports.f = function (t) {
			return new o(t);
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(9).f,
			o = r(35),
			i = r(64),
			a = r(42),
			s = r(48),
			c = r(58),
			u = r(90),
			l = r(46),
			f = r(6),
			h = r(41).fastKey,
			d = r(26),
			p = d.set,
			g = d.getterFor;
		t.exports = {
			getConstructor: function (t, e, r, u) {
				var l = t(function (t, n) {
						s(t, l, e),
							p(t, {
								type: e,
								index: o(null),
								first: void 0,
								last: void 0,
								size: 0,
							}),
							f || (t.size = 0),
							null != n && c(n, t[u], t, r);
					}),
					d = g(e),
					v = function (t, e, r) {
						var n,
							o,
							i = d(t),
							a = m(t, e);
						return (
							a
								? (a.value = r)
								: ((i.last = a = {
										index: (o = h(e, !0)),
										key: e,
										value: r,
										previous: (n = i.last),
										next: void 0,
										removed: !1,
								  }),
								  i.first || (i.first = a),
								  n && (n.next = a),
								  f ? i.size++ : t.size++,
								  "F" !== o && (i.index[o] = a)),
							t
						);
					},
					m = function (t, e) {
						var r,
							n = d(t),
							o = h(e);
						if ("F" !== o) return n.index[o];
						for (r = n.first; r; r = r.next) if (r.key == e) return r;
					};
				return (
					i(l.prototype, {
						clear: function () {
							for (var t = d(this), e = t.index, r = t.first; r; )
								(r.removed = !0),
									r.previous && (r.previous = r.previous.next = void 0),
									delete e[r.index],
									(r = r.next);
							(t.first = t.last = void 0), f ? (t.size = 0) : (this.size = 0);
						},
						delete: function (t) {
							var e = d(this),
								r = m(this, t);
							if (r) {
								var n = r.next,
									o = r.previous;
								delete e.index[r.index],
									(r.removed = !0),
									o && (o.next = n),
									n && (n.previous = o),
									e.first == r && (e.first = n),
									e.last == r && (e.last = o),
									f ? e.size-- : this.size--;
							}
							return !!r;
						},
						forEach: function (t) {
							for (
								var e,
									r = d(this),
									n = a(t, arguments.length > 1 ? arguments[1] : void 0, 3);
								(e = e ? e.next : r.first);

							)
								for (n(e.value, e.key, this); e && e.removed; ) e = e.previous;
						},
						has: function (t) {
							return !!m(this, t);
						},
					}),
					i(
						l.prototype,
						r
							? {
									get: function (t) {
										var e = m(this, t);
										return e && e.value;
									},
									set: function (t, e) {
										return v(this, 0 === t ? 0 : t, e);
									},
							  }
							: {
									add: function (t) {
										return v(this, (t = 0 === t ? 0 : t), t);
									},
							  }
					),
					f &&
						n(l.prototype, "size", {
							get: function () {
								return d(this).size;
							},
						}),
					l
				);
			},
			setStrong: function (t, e, r) {
				var n = e + " Iterator",
					o = g(e),
					i = g(n);
				u(
					t,
					e,
					function (t, e) {
						p(this, { type: n, target: t, state: o(t), kind: e, last: void 0 });
					},
					function () {
						for (var t = i(this), e = t.kind, r = t.last; r && r.removed; )
							r = r.previous;
						return t.target && (t.last = r = r ? r.next : t.state.first)
							? "keys" == e
								? { value: r.key, done: !1 }
								: "values" == e
								? { value: r.value, done: !1 }
								: { value: [r.key, r.value], done: !1 }
							: ((t.target = void 0), { value: void 0, done: !0 });
					},
					r ? "entries" : "values",
					!r,
					!0
				),
					l(e);
			},
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(64),
			o = r(41).getWeakData,
			i = r(5),
			a = r(3),
			s = r(48),
			c = r(58),
			u = r(13),
			l = r(12),
			f = r(26),
			h = f.set,
			d = f.getterFor,
			p = u(5),
			g = u(6),
			v = 0,
			m = function (t) {
				return t.frozen || (t.frozen = new y());
			},
			y = function () {
				this.entries = [];
			},
			_ = function (t, e) {
				return p(t.entries, function (t) {
					return t[0] === e;
				});
			};
		(y.prototype = {
			get: function (t) {
				var e = _(this, t);
				if (e) return e[1];
			},
			has: function (t) {
				return !!_(this, t);
			},
			set: function (t, e) {
				var r = _(this, t);
				r ? (r[1] = e) : this.entries.push([t, e]);
			},
			delete: function (t) {
				var e = g(this.entries, function (e) {
					return e[0] === t;
				});
				return ~e && this.entries.splice(e, 1), !!~e;
			},
		}),
			(t.exports = {
				getConstructor: function (t, e, r, u) {
					var f = t(function (t, n) {
							s(t, f, e),
								h(t, { type: e, id: v++, frozen: void 0 }),
								null != n && c(n, t[u], t, r);
						}),
						p = d(e),
						g = function (t, e, r) {
							var n = p(t),
								a = o(i(e), !0);
							return !0 === a ? m(n).set(e, r) : (a[n.id] = r), t;
						};
					return (
						n(f.prototype, {
							delete: function (t) {
								var e = p(this);
								if (!a(t)) return !1;
								var r = o(t);
								return !0 === r
									? m(e).delete(t)
									: r && l(r, e.id) && delete r[e.id];
							},
							has: function (t) {
								var e = p(this);
								if (!a(t)) return !1;
								var r = o(t);
								return !0 === r ? m(e).has(t) : r && l(r, e.id);
							},
						}),
						n(
							f.prototype,
							r
								? {
										get: function (t) {
											var e = p(this);
											if (a(t)) {
												var r = o(t);
												return !0 === r ? m(e).get(t) : r ? r[e.id] : void 0;
											}
										},
										set: function (t, e) {
											return g(this, t, e);
										},
								  }
								: {
										add: function (t) {
											return g(this, t, !0);
										},
								  }
						),
						f
					);
				},
			});
	},
	function (t, e, r) {
		var n = r(23),
			o = r(7);
		t.exports = function (t) {
			if (void 0 === t) return 0;
			var e = n(t),
				r = o(e);
			if (e !== r) throw RangeError("Wrong length or index");
			return r;
		};
	},
	function (t, e, r) {
		var n = r(23);
		t.exports = function (t, e) {
			var r = n(t);
			if (r < 0 || r % e) throw RangeError("Wrong offset");
			return r;
		};
	},
	function (t, e, r) {
		var n = r(10),
			o = r(7),
			i = r(86),
			a = r(85),
			s = r(42),
			c = r(4).aTypedArrayConstructor;
		t.exports = function (t) {
			var e,
				r,
				u,
				l,
				f,
				h = n(t),
				d = arguments.length,
				p = d > 1 ? arguments[1] : void 0,
				g = void 0 !== p,
				v = i(h);
			if (null != v && !a(v))
				for (f = v.call(h), h = []; !(l = f.next()).done; ) h.push(l.value);
			for (
				g && d > 2 && (p = s(p, arguments[2], 2)),
					r = o(h.length),
					u = new (c(this))(r),
					e = 0;
				r > e;
				e++
			)
				u[e] = g ? p(h[e], e) : h[e];
			return u;
		};
	},
	function (module, __webpack_exports__, __webpack_require__) {
		"use strict";
		__webpack_require__.d(__webpack_exports__, "a", function () {
			return TabsPolyfill;
		});
		var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
				30
			),
			core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(
				core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__
			),
			core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
				49
			),
			core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1___default = __webpack_require__.n(
				core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__
			),
			_communicator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(11),
			_listener_on_connect_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
				80
			),
			_port_polyfill__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(24);
		class TabsPolyfill {
			constructor() {
				(this.portsMap = new Map()),
					(this.onConnect = new _listener_on_connect_helper__WEBPACK_IMPORTED_MODULE_3__.a()),
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.addListenerOnPortMessageListener(
							this.onPortMessageListener.bind(this)
						),
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.addListenerOnConnectTab(this.onConnectTabListener.bind(this)),
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.addListenerTabAction(this.onTabActionListener.bind(this));
			}
			get(t, e) {
				console.warn("Not implemented `get` in TabsPolyfill");
			}
			getCurrent(t) {
				console.warn("Not implemented `getCurrent` in TabsPolyfill");
			}
			getSelected(t) {
				console.warn("Not implemented `getCurrent` in TabsPolyfill");
			}
			connect(t, e) {
				var r = new _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a(
					_port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a.TYPES.TAB,
					t
				);
				this.portsMap.set(r.uuid, r);
				var n = {
					action: _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a.ACTION.CREATE,
					portUUID: r.uuid,
					type: _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a.TYPES.TAB,
					uniqueID: r.uniqueID,
				};
				return (
					_communicator__WEBPACK_IMPORTED_MODULE_2__.a
						.shared()
						.port(n)
						.then((t) => {
							(r.ready = !0), r.executeWaitingMessages();
						})
						.catch((t) => {
							console.error(`Error port ${t}`);
						}),
					r
				);
			}
			sendRequest(t, e, r) {
				console.warn("Not implemented `sendRequest` in TabsPolyfill");
			}
			sendMessage(t, e, r, n) {
				console.warn("Not implemented `sendMessage` in TabsPolyfill");
			}
			getAllInWindow(t, e) {
				console.warn("Not implemented `getAllInWindow` in TabsPolyfill");
			}
			create(t, e) {
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs(t, TabsPolyfill.tabsActions.create)
					.then((t) => {
						e(t);
					})
					.catch((t) => {
						console.error(`Error in getiing tab ${t}`);
					});
			}
			duplicate(t, e) {
				console.warn("Not implemented `duplicate` in TabsPolyfill");
			}
			query(t, e) {
				return void 0 === window.safari
					? this.queryFromBrowserAction(t, e)
					: this.queryFromContent(t, e);
			}
			queryFromBrowserAction(t, e) {
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs(t, TabsPolyfill.tabsActions.getTabWith)
					.then((t) => {
						e(t);
					})
					.catch((t) => {
						console.error(`Error in getiing tab ${t}`);
					});
			}
			queryFromContent(t, e) {
				console.warn(`Enters in quering extension with: ${t}`);
			}
			highlight(t, e) {
				console.warn("Not implemented `highlightz` in TabsPolyfill");
			}
			update(t, e, r) {
				console.warn("Not implemented UPDATE funciton in TabsPolyfill");
			}
			move(t, e, r) {
				console.warn("Not implemented MOVE function in TabsPolyfill");
			}
			reload(t, e, r) {
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs({ tabID: t }, TabsPolyfill.tabsActions.reload)
					.then((t) => {
						null != r && r(t);
					})
					.catch((t) => {
						console.error(`Error reload tab ${t}`);
					});
			}
			detectLanguage(t, e) {
				console.warn("Not implemented `detectLanguage` in TabsPolyfill");
			}
			captureVisibleTab(t, e, r) {
				console.warn("Not implemented `captureVisibleTab` in TabsPolyfill");
			}
			executeScript(t, e, r) {
				void 0 === window.safari
					? this.executeScriptFromBrowserAction(t, e, r)
					: console.warn("Not implemented `executeScript` in TabsPolyfill");
			}
			executeScriptFromBrowserAction(t, e, r) {
				const n = { tabID: t, details: e };
				_communicator__WEBPACK_IMPORTED_MODULE_2__.a
					.shared()
					.tabs(n, TabsPolyfill.tabsActions.executeScript)
					.then((t) => {
						r([t]);
					})
					.catch((t) => {
						console.error(`Error in executeScriptFromBrowserAction ${t}`);
					});
			}
			insertCSS(t, e, r) {
				console.warn("Not implemented `insertCSS` in TabsPolyfill");
			}
			setZoom(t, e, r) {
				console.warn("Not implemented `setZoom` in TabsPolyfill");
			}
			setZoomSettings(t, e, r) {
				console.warn("Not implemented `setZoomSettings` in TabsPolyfill");
			}
			discard(t, e) {
				console.warn("Not implemented `discard` in TabsPolyfill");
			}
			goForward(t, e) {
				console.warn("Not implemented `tabID` in TabsPolyfill");
			}
			goBack(t, e) {
				console.warn("Not implemented `goBack` in TabsPolyfill");
			}
			onPortMessageListener(t) {
				let e = t.portUUID,
					r = t.data;
				if (this.portsMap.has(e)) {
					let t = this.portsMap.get(e);
					void 0 !== t && t.onMessage.executeListeners(r);
				}
			}
			onConnectTabListener(t) {
				var e = t.portUUID,
					r = t.type,
					n = t.uniqueID,
					o = new _port_polyfill__WEBPACK_IMPORTED_MODULE_4__.a(r, n);
				(o.uuid = e),
					(o.ready = !0),
					this.portsMap.set(o.uuid, o),
					this.onConnect.executeListeners(o);
			}
			onTabActionListener(data) {
				console.log("Enters in onTabActionListener", data);
				let action = data.action;
				switch (action) {
					case "executeScript": {
						let callbackID = data.callbackID,
							code = data.data,
							returnValue = eval(code),
							queryInfo = { callbackID: callbackID, value: returnValue };
						_communicator__WEBPACK_IMPORTED_MODULE_2__.a
							.shared()
							.tabs(queryInfo, TabsPolyfill.tabsActions.executeScript)
							.then((t) => {
								console.log("result: ", t);
							})
							.catch((t) => {
								console.error(`Error in getiing tab ${t}`);
							});
					}
				}
			}
		}
		(TabsPolyfill.tabsActions = {
			getTabWith: 1,
			create: 2,
			reload: 3,
			executeScript: 4,
		}),
			(TabsPolyfill.onUpdated = {
				addListener: function (t) {
					window.safari.application.addEventListener(
						"beforeNavigate",
						function (e) {
							t(
								e.target.id,
								{ status: "loading", url: e.target.url, pinned: null },
								{
									id: e.target.id,
									index: null,
									windowId: null,
									openerTabId: null,
									highlighted: null,
									active: e.target.id == e.target.browserWindow.activeTab.id,
									pinned: null,
									url: e.target.url,
									title: e.target.title,
									favIconUrl: null,
									status: "loading",
									incognito: null,
								}
							);
						},
						!0
					);
				},
			});
	},
	function (t, e, r) {
		r(139), r(353), (t.exports = r(355));
	},
	function (t, e, r) {
		r(140),
			r(143),
			r(144),
			r(145),
			r(146),
			r(147),
			r(148),
			r(149),
			r(150),
			r(151),
			r(152),
			r(153),
			r(154),
			r(155),
			r(156),
			r(157),
			r(159),
			r(160),
			r(161),
			r(162),
			r(163),
			r(164),
			r(165),
			r(166),
			r(167),
			r(168),
			r(169),
			r(170),
			r(171),
			r(172),
			r(173),
			r(174),
			r(175),
			r(176),
			r(177),
			r(178),
			r(180),
			r(181),
			r(182),
			r(183),
			r(184),
			r(185),
			r(186),
			r(187),
			r(189),
			r(190),
			r(191),
			r(192),
			r(193),
			r(194),
			r(195),
			r(196),
			r(197),
			r(198),
			r(199),
			r(200),
			r(202),
			r(203),
			r(204),
			r(205),
			r(206),
			r(207),
			r(208),
			r(209),
			r(210),
			r(211),
			r(212),
			r(213),
			r(214),
			r(215),
			r(216),
			r(30),
			r(217),
			r(218),
			r(219),
			r(220),
			r(221),
			r(222),
			r(223),
			r(224),
			r(225),
			r(226),
			r(227),
			r(228),
			r(229),
			r(230),
			r(231),
			r(232),
			r(233),
			r(234),
			r(235),
			r(236),
			r(237),
			r(238),
			r(239),
			r(240),
			r(241),
			r(242),
			r(243),
			r(244),
			r(245),
			r(246),
			r(247),
			r(248),
			r(249),
			r(250),
			r(251),
			r(252),
			r(253),
			r(254),
			r(255),
			r(256),
			r(258),
			r(259),
			r(260),
			r(261),
			r(262),
			r(263),
			r(264),
			r(265),
			r(266),
			r(267),
			r(268),
			r(269),
			r(270),
			r(271),
			r(272),
			r(273),
			r(274),
			r(276),
			r(277),
			r(278),
			r(279),
			r(280),
			r(281),
			r(282),
			r(283),
			r(284),
			r(285),
			r(286),
			r(287),
			r(288),
			r(290),
			r(291),
			r(293),
			r(100),
			r(297),
			r(298),
			r(299),
			r(300),
			r(301),
			r(302),
			r(303),
			r(304),
			r(305),
			r(306),
			r(307),
			r(308),
			r(309),
			r(310),
			r(311),
			r(312),
			r(313),
			r(314),
			r(315),
			r(316),
			r(317),
			r(318),
			r(319),
			r(320),
			r(321),
			r(322),
			r(323),
			r(324),
			r(325),
			r(326),
			r(327),
			r(328),
			r(329),
			r(330),
			r(331),
			r(332),
			r(333),
			r(334),
			r(335),
			r(336),
			r(337),
			r(338),
			r(339),
			r(340),
			r(341),
			r(342),
			r(343),
			r(344),
			r(345),
			r(346),
			r(347),
			r(348),
			r(349),
			r(350),
			r(351),
			r(352),
			(t.exports = r(68));
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(2),
			i = r(12),
			a = r(107),
			s = r(6),
			c = r(38),
			u = r(22),
			l = r(54),
			f = r(1),
			h = r(52),
			d = r(34),
			p = r(53),
			g = r(8),
			v = r(108),
			m = r(16),
			y = r(142),
			_ = r(40),
			E = r(5),
			S = r(3),
			T = r(10),
			b = r(17),
			I = r(25),
			A = r(37),
			w = r(35),
			x = r(39),
			O = r(111),
			M = r(15),
			C = r(9),
			R = r(50),
			N = r(14),
			L = r(44),
			D = r(67),
			P = r(66),
			U = r(26),
			k = P("hidden"),
			B = U.set,
			j = U.getterFor("Symbol"),
			F = M.f,
			G = C.f,
			W = O.f,
			H = o.Symbol,
			V = o.JSON,
			K = V && V.stringify,
			q = g("toPrimitive"),
			Y = R.f,
			z = h("symbol-registry"),
			J = h("symbols"),
			X = h("op-symbols"),
			$ = h("wks"),
			Q = Object.prototype,
			Z = o.QObject,
			tt = !Z || !Z.prototype || !Z.prototype.findChild,
			et =
				s &&
				f(function () {
					return (
						7 !=
						w(
							G({}, "a", {
								get: function () {
									return G(this, "a", { value: 7 }).a;
								},
							})
						).a
					);
				})
					? function (t, e, r) {
							var n = F(Q, e);
							n && delete Q[e], G(t, e, r), n && t !== Q && G(Q, e, n);
					  }
					: G,
			rt = function (t, e) {
				var r = (J[t] = w(H.prototype));
				return (
					B(r, { type: "Symbol", tag: t, description: e }),
					s || (r.description = e),
					r
				);
			},
			nt =
				a && "symbol" == typeof H.iterator
					? function (t) {
							return "symbol" == typeof t;
					  }
					: function (t) {
							return Object(t) instanceof H;
					  },
			ot = function (t, e, r) {
				return (
					t === Q && ot(X, e, r),
					E(t),
					(e = I(e, !0)),
					E(r),
					i(J, e)
						? (r.enumerable
								? (i(t, k) && t[k][e] && (t[k][e] = !1),
								  (r = w(r, { enumerable: A(0, !1) })))
								: (i(t, k) || G(t, k, A(1, {})), (t[k][e] = !0)),
						  et(t, e, r))
						: G(t, e, r)
				);
			},
			it = function (t, e) {
				E(t);
				for (var r, n = y((e = b(e))), o = 0, i = n.length; i > o; )
					ot(t, (r = n[o++]), e[r]);
				return t;
			},
			at = function (t) {
				var e = Y.call(this, (t = I(t, !0)));
				return (
					!(this === Q && i(J, t) && !i(X, t)) &&
					(!(e || !i(this, t) || !i(J, t) || (i(this, k) && this[k][t])) || e)
				);
			},
			st = function (t, e) {
				if (((t = b(t)), (e = I(e, !0)), t !== Q || !i(J, e) || i(X, e))) {
					var r = F(t, e);
					return (
						!r || !i(J, e) || (i(t, k) && t[k][e]) || (r.enumerable = !0), r
					);
				}
			},
			ct = function (t) {
				for (var e, r = W(b(t)), n = [], o = 0; r.length > o; )
					i(J, (e = r[o++])) || i(l, e) || n.push(e);
				return n;
			},
			ut = function (t) {
				for (
					var e, r = t === Q, n = W(r ? X : b(t)), o = [], a = 0;
					n.length > a;

				)
					!i(J, (e = n[a++])) || (r && !i(Q, e)) || o.push(J[e]);
				return o;
			};
		a ||
			(u(
				(H = function () {
					if (this instanceof H) throw TypeError("Symbol is not a constructor");
					var t = void 0 === arguments[0] ? void 0 : String(arguments[0]),
						e = p(t),
						r = function (t) {
							this === Q && r.call(X, t),
								i(this, k) && i(this[k], e) && (this[k][e] = !1),
								et(this, e, A(1, t));
						};
					return s && tt && et(Q, e, { configurable: !0, set: r }), rt(e, t);
				}).prototype,
				"toString",
				function () {
					return j(this).tag;
				}
			),
			(R.f = at),
			(C.f = ot),
			(M.f = st),
			(x.f = O.f = ct),
			(D.f = ut),
			s &&
				(G(H.prototype, "description", {
					configurable: !0,
					get: function () {
						return j(this).description;
					},
				}),
				c || u(Q, "propertyIsEnumerable", at, { unsafe: !0 })),
			(v.f = function (t) {
				return rt(g(t), t);
			})),
			n({ global: !0, wrap: !0, forced: !a, sham: !a }, { Symbol: H });
		for (var lt = L($), ft = 0; lt.length > ft; ) m(lt[ft++]);
		n(
			{ target: "Symbol", stat: !0, forced: !a },
			{
				for: function (t) {
					return i(z, (t += "")) ? z[t] : (z[t] = H(t));
				},
				keyFor: function (t) {
					if (!nt(t)) throw TypeError(t + " is not a symbol");
					for (var e in z) if (z[e] === t) return e;
				},
				useSetter: function () {
					tt = !0;
				},
				useSimple: function () {
					tt = !1;
				},
			}
		),
			n(
				{ target: "Object", stat: !0, forced: !a, sham: !s },
				{
					create: function (t, e) {
						return void 0 === e ? w(t) : it(w(t), e);
					},
					defineProperty: ot,
					defineProperties: it,
					getOwnPropertyDescriptor: st,
				}
			),
			n(
				{ target: "Object", stat: !0, forced: !a },
				{ getOwnPropertyNames: ct, getOwnPropertySymbols: ut }
			),
			n(
				{
					target: "Object",
					stat: !0,
					forced: f(function () {
						D.f(1);
					}),
				},
				{
					getOwnPropertySymbols: function (t) {
						return D.f(T(t));
					},
				}
			),
			V &&
				n(
					{
						target: "JSON",
						stat: !0,
						forced:
							!a ||
							f(function () {
								var t = H();
								return (
									"[null]" != K([t]) ||
									"{}" != K({ a: t }) ||
									"{}" != K(Object(t))
								);
							}),
					},
					{
						stringify: function (t) {
							for (var e, r, n = [t], o = 1; arguments.length > o; )
								n.push(arguments[o++]);
							if (((r = e = n[1]), (S(e) || void 0 !== t) && !nt(t)))
								return (
									_(e) ||
										(e = function (t, e) {
											if (
												("function" == typeof r && (e = r.call(this, t, e)),
												!nt(e))
											)
												return e;
										}),
									(n[1] = e),
									K.apply(V, n)
								);
						},
					}
				),
			H.prototype[q] || N(H.prototype, q, H.prototype.valueOf),
			d(H, "Symbol"),
			(l[k] = !0);
	},
	function (t, e) {
		var r;
		r = (function () {
			return this;
		})();
		try {
			r = r || new Function("return this")();
		} catch (t) {
			"object" == typeof window && (r = window);
		}
		t.exports = r;
	},
	function (t, e, r) {
		var n = r(44),
			o = r(67),
			i = r(50);
		t.exports = function (t) {
			var e = n(t),
				r = o.f;
			if (r)
				for (var a, s = r(t), c = i.f, u = 0; s.length > u; )
					c.call(t, (a = s[u++])) && e.push(a);
			return e;
		};
	},
	function (t, e, r) {
		r(16)("asyncIterator");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(6),
			i = r(2),
			a = r(12),
			s = r(3),
			c = r(9).f,
			u = r(105),
			l = i.Symbol;
		if (
			o &&
			"function" == typeof l &&
			(!("description" in l.prototype) || void 0 !== l().description)
		) {
			var f = {},
				h = function () {
					var t =
							arguments.length < 1 || void 0 === arguments[0]
								? void 0
								: String(arguments[0]),
						e = this instanceof h ? new l(t) : void 0 === t ? l() : l(t);
					return "" === t && (f[e] = !0), e;
				};
			u(h, l);
			var d = (h.prototype = l.prototype);
			d.constructor = h;
			var p = d.toString,
				g = "Symbol(test)" == String(l("test")),
				v = /^Symbol\((.*)\)[^)]+$/;
			c(d, "description", {
				configurable: !0,
				get: function () {
					var t = s(this) ? this.valueOf() : this,
						e = p.call(t);
					if (a(f, t)) return "";
					var r = g ? e.slice(7, -1) : e.replace(v, "$1");
					return "" === r ? void 0 : r;
				},
			}),
				n({ global: !0, forced: !0 }, { Symbol: h });
		}
	},
	function (t, e, r) {
		r(16)("hasInstance");
	},
	function (t, e, r) {
		r(16)("isConcatSpreadable");
	},
	function (t, e, r) {
		r(16)("iterator");
	},
	function (t, e, r) {
		r(16)("match");
	},
	function (t, e, r) {
		r(16)("matchAll");
	},
	function (t, e, r) {
		r(16)("replace");
	},
	function (t, e, r) {
		r(16)("search");
	},
	function (t, e, r) {
		r(16)("species");
	},
	function (t, e, r) {
		r(16)("split");
	},
	function (t, e, r) {
		r(16)("toPrimitive");
	},
	function (t, e, r) {
		r(16)("toStringTag");
	},
	function (t, e, r) {
		r(16)("unscopables");
	},
	function (t, e, r) {
		var n = r(0),
			o = r(158);
		n(
			{ target: "Object", stat: !0, forced: Object.assign !== o },
			{ assign: o }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(6),
			o = r(1),
			i = r(44),
			a = r(67),
			s = r(50),
			c = r(10),
			u = r(51),
			l = Object.assign;
		t.exports =
			!l ||
			o(function () {
				var t = {},
					e = {},
					r = Symbol();
				return (
					(t[r] = 7),
					"abcdefghijklmnopqrst".split("").forEach(function (t) {
						e[t] = t;
					}),
					7 != l({}, t)[r] || "abcdefghijklmnopqrst" != i(l({}, e)).join("")
				);
			})
				? function (t, e) {
						for (
							var r = c(t), o = arguments.length, l = 1, f = a.f, h = s.f;
							o > l;

						)
							for (
								var d,
									p = u(arguments[l++]),
									g = f ? i(p).concat(f(p)) : i(p),
									v = g.length,
									m = 0;
								v > m;

							)
								(d = g[m++]), (n && !h.call(p, d)) || (r[d] = p[d]);
						return r;
				  }
				: l;
	},
	function (t, e, r) {
		r(0)({ target: "Object", stat: !0, sham: !r(6) }, { create: r(35) });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(6);
		n(
			{ target: "Object", stat: !0, forced: !o, sham: !o },
			{ defineProperty: r(9).f }
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(6);
		n(
			{ target: "Object", stat: !0, forced: !o, sham: !o },
			{ defineProperties: r(109) }
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(112);
		n(
			{ target: "Object", stat: !0 },
			{
				entries: function (t) {
					return o(t, !0);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(57),
			i = r(1),
			a = r(3),
			s = r(41).onFreeze,
			c = Object.freeze;
		n(
			{
				target: "Object",
				stat: !0,
				forced: i(function () {
					c(1);
				}),
				sham: !o,
			},
			{
				freeze: function (t) {
					return c && a(t) ? c(s(t)) : t;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(58),
			i = r(43);
		n(
			{ target: "Object", stat: !0 },
			{
				fromEntries: function (t) {
					var e = {};
					return (
						o(
							t,
							function (t, r) {
								i(e, t, r);
							},
							void 0,
							!0
						),
						e
					);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(17),
			a = r(15).f,
			s = r(6),
			c = o(function () {
				a(1);
			});
		n(
			{ target: "Object", stat: !0, forced: !s || c, sham: !s },
			{
				getOwnPropertyDescriptor: function (t, e) {
					return a(i(t), e);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(6),
			i = r(83),
			a = r(17),
			s = r(15),
			c = r(43);
		n(
			{ target: "Object", stat: !0, sham: !o },
			{
				getOwnPropertyDescriptors: function (t) {
					for (
						var e, r, n = a(t), o = s.f, u = i(n), l = {}, f = 0;
						u.length > f;

					)
						void 0 !== (r = o(n, (e = u[f++]))) && c(l, e, r);
					return l;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(111).f;
		n(
			{
				target: "Object",
				stat: !0,
				forced: o(function () {
					return !Object.getOwnPropertyNames(1);
				}),
			},
			{ getOwnPropertyNames: i }
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(10),
			a = r(28),
			s = r(87);
		n(
			{
				target: "Object",
				stat: !0,
				forced: o(function () {
					a(1);
				}),
				sham: !s,
			},
			{
				getPrototypeOf: function (t) {
					return a(i(t));
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "Object", stat: !0 }, { is: r(114) });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(3),
			a = Object.isExtensible;
		n(
			{
				target: "Object",
				stat: !0,
				forced: o(function () {
					a(1);
				}),
			},
			{
				isExtensible: function (t) {
					return !!i(t) && (!a || a(t));
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(3),
			a = Object.isFrozen;
		n(
			{
				target: "Object",
				stat: !0,
				forced: o(function () {
					a(1);
				}),
			},
			{
				isFrozen: function (t) {
					return !i(t) || (!!a && a(t));
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(3),
			a = Object.isSealed;
		n(
			{
				target: "Object",
				stat: !0,
				forced: o(function () {
					a(1);
				}),
			},
			{
				isSealed: function (t) {
					return !i(t) || (!!a && a(t));
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(10),
			i = r(44);
		n(
			{
				target: "Object",
				stat: !0,
				forced: r(1)(function () {
					i(1);
				}),
			},
			{
				keys: function (t) {
					return i(o(t));
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(3),
			i = r(41).onFreeze,
			a = r(57),
			s = r(1),
			c = Object.preventExtensions;
		n(
			{
				target: "Object",
				stat: !0,
				forced: s(function () {
					c(1);
				}),
				sham: !a,
			},
			{
				preventExtensions: function (t) {
					return c && o(t) ? c(i(t)) : t;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(3),
			i = r(41).onFreeze,
			a = r(57),
			s = r(1),
			c = Object.seal;
		n(
			{
				target: "Object",
				stat: !0,
				forced: s(function () {
					c(1);
				}),
				sham: !a,
			},
			{
				seal: function (t) {
					return c && o(t) ? c(i(t)) : t;
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "Object", stat: !0 }, { setPrototypeOf: r(45) });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(112);
		n(
			{ target: "Object", stat: !0 },
			{
				values: function (t) {
					return o(t);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(22),
			o = r(179),
			i = Object.prototype;
		o !== i.toString && n(i, "toString", o, { unsafe: !0 });
	},
	function (t, e, r) {
		"use strict";
		var n = r(60),
			o = {};
		(o[r(8)("toStringTag")] = "z"),
			(t.exports =
				"[object z]" !== String(o)
					? function () {
							return "[object " + n(this) + "]";
					  }
					: o.toString);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(6),
			i = r(69),
			a = r(10),
			s = r(19),
			c = r(9);
		o &&
			n(
				{ target: "Object", proto: !0, forced: i },
				{
					__defineGetter__: function (t, e) {
						c.f(a(this), t, { get: s(e), enumerable: !0, configurable: !0 });
					},
				}
			);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(6),
			i = r(69),
			a = r(10),
			s = r(19),
			c = r(9);
		o &&
			n(
				{ target: "Object", proto: !0, forced: i },
				{
					__defineSetter__: function (t, e) {
						c.f(a(this), t, { set: s(e), enumerable: !0, configurable: !0 });
					},
				}
			);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(6),
			i = r(69),
			a = r(10),
			s = r(25),
			c = r(28),
			u = r(15).f;
		o &&
			n(
				{ target: "Object", proto: !0, forced: i },
				{
					__lookupGetter__: function (t) {
						var e,
							r = a(this),
							n = s(t, !0);
						do {
							if ((e = u(r, n))) return e.get;
						} while ((r = c(r)));
					},
				}
			);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(6),
			i = r(69),
			a = r(10),
			s = r(25),
			c = r(28),
			u = r(15).f;
		o &&
			n(
				{ target: "Object", proto: !0, forced: i },
				{
					__lookupSetter__: function (t) {
						var e,
							r = a(this),
							n = s(t, !0);
						do {
							if ((e = u(r, n))) return e.set;
						} while ((r = c(r)));
					},
				}
			);
	},
	function (t, e, r) {
		r(0)({ target: "Function", proto: !0 }, { bind: r(116) });
	},
	function (t, e, r) {
		var n = r(6),
			o = r(9).f,
			i = Function.prototype,
			a = i.toString,
			s = /^\s*function ([^ (]*)/;
		!n ||
			"name" in i ||
			o(i, "name", {
				configurable: !0,
				get: function () {
					try {
						return a.call(this).match(s)[1];
					} catch (t) {
						return "";
					}
				},
			});
	},
	function (t, e, r) {
		"use strict";
		var n = r(3),
			o = r(9),
			i = r(28),
			a = r(8)("hasInstance"),
			s = Function.prototype;
		a in s ||
			o.f(s, a, {
				value: function (t) {
					if ("function" != typeof this || !n(t)) return !1;
					if (!n(this.prototype)) return t instanceof this;
					for (; (t = i(t)); ) if (this.prototype === t) return !0;
					return !1;
				},
			});
	},
	function (t, e, r) {
		var n = r(0),
			o = r(188);
		n(
			{
				target: "Array",
				stat: !0,
				forced: !r(70)(function (t) {
					Array.from(t);
				}),
			},
			{ from: o }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(42),
			o = r(10),
			i = r(113),
			a = r(85),
			s = r(7),
			c = r(43),
			u = r(86);
		t.exports = function (t) {
			var e,
				r,
				l,
				f,
				h = o(t),
				d = "function" == typeof this ? this : Array,
				p = arguments.length,
				g = p > 1 ? arguments[1] : void 0,
				v = void 0 !== g,
				m = 0,
				y = u(h);
			if (
				(v && (g = n(g, p > 2 ? arguments[2] : void 0, 2)),
				null == y || (d == Array && a(y)))
			)
				for (r = new d((e = s(h.length))); e > m; m++)
					c(r, m, v ? g(h[m], m) : h[m]);
			else
				for (f = y.call(h), r = new d(); !(l = f.next()).done; m++)
					c(r, m, v ? i(f, g, [l.value, m], !0) : l.value);
			return (r.length = m), r;
		};
	},
	function (t, e, r) {
		r(0)({ target: "Array", stat: !0 }, { isArray: r(40) });
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(1),
			i = r(43);
		n(
			{
				target: "Array",
				stat: !0,
				forced: o(function () {
					function t() {}
					return !(Array.of.call(t) instanceof t);
				}),
			},
			{
				of: function () {
					for (
						var t = 0,
							e = arguments.length,
							r = new ("function" == typeof this ? this : Array)(e);
						e > t;

					)
						i(r, t, arguments[t++]);
					return (r.length = e), r;
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(1),
			i = r(40),
			a = r(3),
			s = r(10),
			c = r(7),
			u = r(43),
			l = r(61),
			f = r(62),
			h = r(8)("isConcatSpreadable"),
			d = !o(function () {
				var t = [];
				return (t[h] = !1), t.concat()[0] !== t;
			}),
			p = f("concat"),
			g = function (t) {
				if (!a(t)) return !1;
				var e = t[h];
				return void 0 !== e ? !!e : i(t);
			};
		n(
			{ target: "Array", proto: !0, forced: !d || !p },
			{
				concat: function (t) {
					var e,
						r,
						n,
						o,
						i,
						a = s(this),
						f = l(a, 0),
						h = 0;
					for (e = -1, n = arguments.length; e < n; e++)
						if (((i = -1 === e ? a : arguments[e]), g(i))) {
							if (h + (o = c(i.length)) > 9007199254740991)
								throw TypeError("Maximum allowed index exceeded");
							for (r = 0; r < o; r++, h++) r in i && u(f, h, i[r]);
						} else {
							if (h >= 9007199254740991)
								throw TypeError("Maximum allowed index exceeded");
							u(f, h++, i);
						}
					return (f.length = h), f;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(117),
			i = r(36);
		n({ target: "Array", proto: !0 }, { copyWithin: o }), i("copyWithin");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(29),
			a = o(4);
		n(
			{ target: "Array", proto: !0, forced: i("every") },
			{
				every: function (t) {
					return a(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(88),
			i = r(36);
		n({ target: "Array", proto: !0 }, { fill: o }), i("fill");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(62),
			a = o(2);
		n(
			{ target: "Array", proto: !0, forced: !i("filter") },
			{
				filter: function (t) {
					return a(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(36),
			a = o(5),
			s = !0;
		"find" in [] &&
			Array(1).find(function () {
				s = !1;
			}),
			n(
				{ target: "Array", proto: !0, forced: s },
				{
					find: function (t) {
						return a(this, t, arguments.length > 1 ? arguments[1] : void 0);
					},
				}
			),
			i("find");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(36),
			a = o(6),
			s = !0;
		"findIndex" in [] &&
			Array(1).findIndex(function () {
				s = !1;
			}),
			n(
				{ target: "Array", proto: !0, forced: s },
				{
					findIndex: function (t) {
						return a(this, t, arguments.length > 1 ? arguments[1] : void 0);
					},
				}
			),
			i("findIndex");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(118),
			i = r(10),
			a = r(7),
			s = r(23),
			c = r(61);
		n(
			{ target: "Array", proto: !0 },
			{
				flat: function () {
					var t = arguments[0],
						e = i(this),
						r = a(e.length),
						n = c(e, 0);
					return (n.length = o(n, e, e, r, 0, void 0 === t ? 1 : s(t))), n;
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(118),
			i = r(10),
			a = r(7),
			s = r(19),
			c = r(61);
		n(
			{ target: "Array", proto: !0 },
			{
				flatMap: function (t) {
					var e,
						r = i(this),
						n = a(r.length);
					return (
						s(t),
						((e = c(r, 0)).length = o(e, r, r, n, 0, 1, t, arguments[1])),
						e
					);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(201);
		n({ target: "Array", proto: !0, forced: [].forEach != o }, { forEach: o });
	},
	function (t, e, r) {
		"use strict";
		var n = r(13),
			o = r(29),
			i = n(0),
			a = o("forEach");
		t.exports = a
			? function (t) {
					return i(this, t, arguments[1]);
			  }
			: [].forEach;
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(55),
			i = r(36),
			a = o(!0);
		n(
			{ target: "Array", proto: !0 },
			{
				includes: function (t) {
					return a(this, t, arguments.length > 1 ? arguments[1] : void 0);
				},
			}
		),
			i("includes");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(29),
			i = r(55)(!1),
			a = [].indexOf,
			s = !!a && 1 / [1].indexOf(1, -0) < 0,
			c = o("indexOf");
		n(
			{ target: "Array", proto: !0, forced: s || c },
			{
				indexOf: function (t) {
					return s ? a.apply(this, arguments) || 0 : i(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(51),
			i = r(17),
			a = r(29),
			s = [].join,
			c = o != Object,
			u = a("join", ",");
		n(
			{ target: "Array", proto: !0, forced: c || u },
			{
				join: function (t) {
					return s.call(i(this), void 0 === t ? "," : t);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(119);
		n(
			{ target: "Array", proto: !0, forced: o !== [].lastIndexOf },
			{ lastIndexOf: o }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(62),
			a = o(1);
		n(
			{ target: "Array", proto: !0, forced: !i("map") },
			{
				map: function (t) {
					return a(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(71);
		n(
			{ target: "Array", proto: !0, forced: r(29)("reduce") },
			{
				reduce: function (t) {
					return o(this, t, arguments.length, arguments[1], !1);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(71);
		n(
			{ target: "Array", proto: !0, forced: r(29)("reduceRight") },
			{
				reduceRight: function (t) {
					return o(this, t, arguments.length, arguments[1], !0);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(40),
			i = [].reverse,
			a = [1, 2];
		n(
			{ target: "Array", proto: !0, forced: String(a) === String(a.reverse()) },
			{
				reverse: function () {
					return o(this) && (this.length = this.length), i.call(this);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(3),
			i = r(40),
			a = r(33),
			s = r(7),
			c = r(17),
			u = r(43),
			l = r(62),
			f = r(8)("species"),
			h = [].slice,
			d = Math.max;
		n(
			{ target: "Array", proto: !0, forced: !l("slice") },
			{
				slice: function (t, e) {
					var r,
						n,
						l,
						p = c(this),
						g = s(p.length),
						v = a(t, g),
						m = a(void 0 === e ? g : e, g);
					if (
						i(p) &&
						("function" != typeof (r = p.constructor) ||
						(r !== Array && !i(r.prototype))
							? o(r) && null === (r = r[f]) && (r = void 0)
							: (r = void 0),
						r === Array || void 0 === r)
					)
						return h.call(p, v, m);
					for (
						n = new (void 0 === r ? Array : r)(d(m - v, 0)), l = 0;
						v < m;
						v++, l++
					)
						v in p && u(n, l, p[v]);
					return (n.length = l), n;
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(13),
			i = r(29),
			a = o(3);
		n(
			{ target: "Array", proto: !0, forced: i("some") },
			{
				some: function (t) {
					return a(this, t, arguments[1]);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(19),
			i = r(10),
			a = r(1),
			s = r(29),
			c = [].sort,
			u = [1, 2, 3],
			l = a(function () {
				u.sort(void 0);
			}),
			f = a(function () {
				u.sort(null);
			}),
			h = s("sort");
		n(
			{ target: "Array", proto: !0, forced: l || !f || h },
			{
				sort: function (t) {
					return void 0 === t ? c.call(i(this)) : c.call(i(this), o(t));
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(33),
			i = r(23),
			a = r(7),
			s = r(10),
			c = r(61),
			u = r(43),
			l = r(62),
			f = Math.max,
			h = Math.min;
		n(
			{ target: "Array", proto: !0, forced: !l("splice") },
			{
				splice: function (t, e) {
					var r,
						n,
						l,
						d,
						p,
						g,
						v = s(this),
						m = a(v.length),
						y = o(t, m),
						_ = arguments.length;
					if (
						(0 === _
							? (r = n = 0)
							: 1 === _
							? ((r = 0), (n = m - y))
							: ((r = _ - 2), (n = h(f(i(e), 0), m - y))),
						m + r - n > 9007199254740991)
					)
						throw TypeError("Maximum allowed length exceeded");
					for (l = c(v, n), d = 0; d < n; d++)
						(p = y + d) in v && u(l, d, v[p]);
					if (((l.length = n), r < n)) {
						for (d = y; d < m - n; d++)
							(g = d + r), (p = d + n) in v ? (v[g] = v[p]) : delete v[g];
						for (d = m; d > m - n + r; d--) delete v[d - 1];
					} else if (r > n)
						for (d = m - n; d > y; d--)
							(g = d + r - 1),
								(p = d + n - 1) in v ? (v[g] = v[p]) : delete v[g];
					for (d = 0; d < r; d++) v[d + y] = arguments[d + 2];
					return (v.length = m - n + r), l;
				},
			}
		);
	},
	function (t, e, r) {
		r(46)("Array");
	},
	function (t, e, r) {
		r(36)("flat");
	},
	function (t, e, r) {
		r(36)("flatMap");
	},
	function (t, e, r) {
		var n = r(0),
			o = r(33),
			i = String.fromCharCode,
			a = String.fromCodePoint;
		n(
			{ target: "String", stat: !0, forced: !!a && 1 != a.length },
			{
				fromCodePoint: function (t) {
					for (var e, r = [], n = arguments.length, a = 0; n > a; ) {
						if (((e = +arguments[a++]), o(e, 1114111) !== e))
							throw RangeError(e + " is not a valid code point");
						r.push(
							e < 65536
								? i(e)
								: i(55296 + ((e -= 65536) >> 10), (e % 1024) + 56320)
						);
					}
					return r.join("");
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(17),
			i = r(7);
		n(
			{ target: "String", stat: !0 },
			{
				raw: function (t) {
					for (
						var e = o(t.raw),
							r = i(e.length),
							n = arguments.length,
							a = [],
							s = 0;
						r > s;

					)
						a.push(String(e[s++])), s < n && a.push(String(arguments[s]));
					return a.join("");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(91);
		n(
			{ target: "String", proto: !0 },
			{
				codePointAt: function (t) {
					return o(this, t);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(7),
			i = r(92),
			a = r(94),
			s = "".endsWith,
			c = Math.min;
		n(
			{ target: "String", proto: !0, forced: !a("endsWith") },
			{
				endsWith: function (t) {
					var e = i(this, t, "endsWith"),
						r = arguments.length > 1 ? arguments[1] : void 0,
						n = o(e.length),
						a = void 0 === r ? n : c(o(r), n),
						u = String(t);
					return s ? s.call(e, u, a) : e.slice(a - u.length, a) === u;
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(92);
		n(
			{ target: "String", proto: !0, forced: !r(94)("includes") },
			{
				includes: function (t) {
					return !!~o(this, t, "includes").indexOf(
						t,
						arguments.length > 1 ? arguments[1] : void 0
					);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(72),
			o = r(5),
			i = r(7),
			a = r(18),
			s = r(74),
			c = r(75);
		n("match", 1, function (t, e, r) {
			return [
				function (e) {
					var r = a(this),
						n = null == e ? void 0 : e[t];
					return void 0 !== n ? n.call(e, r) : new RegExp(e)[t](String(r));
				},
				function (t) {
					var n = r(e, t, this);
					if (n.done) return n.value;
					var a = o(t),
						u = String(this);
					if (!a.global) return c(a, u);
					var l = a.unicode;
					a.lastIndex = 0;
					for (var f, h = [], d = 0; null !== (f = c(a, u)); ) {
						var p = String(f[0]);
						(h[d] = p),
							"" === p && (a.lastIndex = s(u, i(a.lastIndex), l)),
							d++;
					}
					return 0 === d ? null : h;
				},
			];
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(120),
			i = r(18),
			a = r(7),
			s = r(19),
			c = r(5),
			u = r(60),
			l = r(63),
			f = r(14),
			h = r(8),
			d = r(31),
			p = r(74),
			g = r(26),
			v = r(38),
			m = h("matchAll"),
			y = g.set,
			_ = g.getterFor("RegExp String Iterator"),
			E = RegExp.prototype,
			S = E.exec,
			T = o(
				function (t, e, r, n) {
					y(this, {
						type: "RegExp String Iterator",
						regexp: t,
						string: e,
						global: r,
						unicode: n,
						done: !1,
					});
				},
				"RegExp String",
				function () {
					var t = _(this);
					if (t.done) return { value: void 0, done: !0 };
					var e = t.regexp,
						r = t.string,
						n = (function (t, e) {
							var r,
								n = t.exec;
							if ("function" == typeof n) {
								if ("object" != typeof (r = n.call(t, e)))
									throw TypeError("Incorrect exec result");
								return r;
							}
							return S.call(t, e);
						})(e, r);
					return null === n
						? { value: void 0, done: (t.done = !0) }
						: t.global
						? ("" == String(n[0]) &&
								(e.lastIndex = p(r, a(e.lastIndex), t.unicode)),
						  { value: n, done: !1 })
						: ((t.done = !0), { value: n, done: !1 });
				}
			),
			b = function (t) {
				var e,
					r,
					n,
					o,
					i,
					s,
					u = c(this),
					f = String(t);
				return (
					(e = d(u, RegExp)),
					void 0 === (r = u.flags) &&
						u instanceof RegExp &&
						!("flags" in E) &&
						(r = l.call(u)),
					(n = void 0 === r ? "" : String(r)),
					(o = new e(e === RegExp ? u.source : u, n)),
					(i = !!~n.indexOf("g")),
					(s = !!~n.indexOf("u")),
					(o.lastIndex = a(u.lastIndex)),
					new T(o, f, i, s)
				);
			};
		n(
			{ target: "String", proto: !0 },
			{
				matchAll: function (t) {
					var e,
						r,
						n,
						o = i(this);
					return null != t &&
						(void 0 === (r = t[m]) && v && "RegExp" == u(t) && (r = b),
						null != r)
						? s(r).call(t, o)
						: ((e = String(o)),
						  (n = new RegExp(t, "g")),
						  v ? b.call(n, e) : n[m](e));
				},
			}
		),
			v || m in E || f(E, m, b);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(122);
		n(
			{ target: "String", proto: !0, forced: r(123) },
			{
				padEnd: function (t) {
					return o(this, t, arguments.length > 1 ? arguments[1] : void 0, !1);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(122);
		n(
			{ target: "String", proto: !0, forced: r(123) },
			{
				padStart: function (t) {
					return o(this, t, arguments.length > 1 ? arguments[1] : void 0, !0);
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "String", proto: !0 }, { repeat: r(95) });
	},
	function (t, e, r) {
		"use strict";
		var n = r(72),
			o = r(5),
			i = r(10),
			a = r(7),
			s = r(23),
			c = r(18),
			u = r(74),
			l = r(75),
			f = Math.max,
			h = Math.min,
			d = Math.floor,
			p = /\$([$&'`]|\d\d?|<[^>]*>)/g,
			g = /\$([$&'`]|\d\d?)/g;
		n("replace", 2, function (t, e, r) {
			return [
				function (r, n) {
					var o = c(this),
						i = null == r ? void 0 : r[t];
					return void 0 !== i ? i.call(r, o, n) : e.call(String(o), r, n);
				},
				function (t, i) {
					var c = r(e, t, this, i);
					if (c.done) return c.value;
					var d = o(t),
						p = String(this),
						g = "function" == typeof i;
					g || (i = String(i));
					var v = d.global;
					if (v) {
						var m = d.unicode;
						d.lastIndex = 0;
					}
					for (var y = []; ; ) {
						var _ = l(d, p);
						if (null === _) break;
						if ((y.push(_), !v)) break;
						"" === String(_[0]) && (d.lastIndex = u(p, a(d.lastIndex), m));
					}
					for (var E, S = "", T = 0, b = 0; b < y.length; b++) {
						_ = y[b];
						for (
							var I = String(_[0]),
								A = f(h(s(_.index), p.length), 0),
								w = [],
								x = 1;
							x < _.length;
							x++
						)
							w.push(void 0 === (E = _[x]) ? E : String(E));
						var O = _.groups;
						if (g) {
							var M = [I].concat(w, A, p);
							void 0 !== O && M.push(O);
							var C = String(i.apply(void 0, M));
						} else C = n(I, p, A, w, O, i);
						A >= T && ((S += p.slice(T, A) + C), (T = A + I.length));
					}
					return S + p.slice(T);
				},
			];
			function n(t, r, n, o, a, s) {
				var c = n + t.length,
					u = o.length,
					l = g;
				return (
					void 0 !== a && ((a = i(a)), (l = p)),
					e.call(s, l, function (e, i) {
						var s;
						switch (i.charAt(0)) {
							case "$":
								return "$";
							case "&":
								return t;
							case "`":
								return r.slice(0, n);
							case "'":
								return r.slice(c);
							case "<":
								s = a[i.slice(1, -1)];
								break;
							default:
								var l = +i;
								if (0 === l) return e;
								if (l > u) {
									var f = d(l / 10);
									return 0 === f
										? e
										: f <= u
										? void 0 === o[f - 1]
											? i.charAt(1)
											: o[f - 1] + i.charAt(1)
										: e;
								}
								s = o[l - 1];
						}
						return void 0 === s ? "" : s;
					})
				);
			}
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(72),
			o = r(5),
			i = r(18),
			a = r(114),
			s = r(75);
		n("search", 1, function (t, e, r) {
			return [
				function (e) {
					var r = i(this),
						n = null == e ? void 0 : e[t];
					return void 0 !== n ? n.call(e, r) : new RegExp(e)[t](String(r));
				},
				function (t) {
					var n = r(e, t, this);
					if (n.done) return n.value;
					var i = o(t),
						c = String(this),
						u = i.lastIndex;
					a(u, 0) || (i.lastIndex = 0);
					var l = s(i, c);
					return (
						a(i.lastIndex, u) || (i.lastIndex = u), null === l ? -1 : l.index
					);
				},
			];
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(72),
			o = r(93),
			i = r(5),
			a = r(18),
			s = r(31),
			c = r(74),
			u = r(7),
			l = r(75),
			f = r(73),
			h = r(1),
			d = [].push,
			p = Math.min,
			g = !h(function () {
				return !RegExp(4294967295, "y");
			});
		n(
			"split",
			2,
			function (t, e, r) {
				var n;
				return (
					(n =
						"c" == "abbc".split(/(b)*/)[1] ||
						4 != "test".split(/(?:)/, -1).length ||
						2 != "ab".split(/(?:ab)*/).length ||
						4 != ".".split(/(.?)(.?)/).length ||
						".".split(/()()/).length > 1 ||
						"".split(/.?/).length
							? function (t, r) {
									var n = String(a(this)),
										i = void 0 === r ? 4294967295 : r >>> 0;
									if (0 === i) return [];
									if (void 0 === t) return [n];
									if (!o(t)) return e.call(n, t, i);
									for (
										var s,
											c,
											u,
											l = [],
											h =
												(t.ignoreCase ? "i" : "") +
												(t.multiline ? "m" : "") +
												(t.unicode ? "u" : "") +
												(t.sticky ? "y" : ""),
											p = 0,
											g = new RegExp(t.source, h + "g");
										(s = f.call(g, n)) &&
										!(
											(c = g.lastIndex) > p &&
											(l.push(n.slice(p, s.index)),
											s.length > 1 &&
												s.index < n.length &&
												d.apply(l, s.slice(1)),
											(u = s[0].length),
											(p = c),
											l.length >= i)
										);

									)
										g.lastIndex === s.index && g.lastIndex++;
									return (
										p === n.length
											? (!u && g.test("")) || l.push("")
											: l.push(n.slice(p)),
										l.length > i ? l.slice(0, i) : l
									);
							  }
							: "0".split(void 0, 0).length
							? function (t, r) {
									return void 0 === t && 0 === r ? [] : e.call(this, t, r);
							  }
							: e),
					[
						function (e, r) {
							var o = a(this),
								i = null == e ? void 0 : e[t];
							return void 0 !== i ? i.call(e, o, r) : n.call(String(o), e, r);
						},
						function (t, o) {
							var a = r(n, t, this, o, n !== e);
							if (a.done) return a.value;
							var f = i(t),
								h = String(this),
								d = s(f, RegExp),
								v = f.unicode,
								m =
									(f.ignoreCase ? "i" : "") +
									(f.multiline ? "m" : "") +
									(f.unicode ? "u" : "") +
									(g ? "y" : "g"),
								y = new d(g ? f : "^(?:" + f.source + ")", m),
								_ = void 0 === o ? 4294967295 : o >>> 0;
							if (0 === _) return [];
							if (0 === h.length) return null === l(y, h) ? [h] : [];
							for (var E = 0, S = 0, T = []; S < h.length; ) {
								y.lastIndex = g ? S : 0;
								var b,
									I = l(y, g ? h : h.slice(S));
								if (
									null === I ||
									(b = p(u(y.lastIndex + (g ? 0 : S)), h.length)) === E
								)
									S = c(h, S, v);
								else {
									if ((T.push(h.slice(E, S)), T.length === _)) return T;
									for (var A = 1; A <= I.length - 1; A++)
										if ((T.push(I[A]), T.length === _)) return T;
									S = E = b;
								}
							}
							return T.push(h.slice(E)), T;
						},
					]
				);
			},
			!g
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(7),
			i = r(92),
			a = r(94),
			s = "".startsWith;
		n(
			{ target: "String", proto: !0, forced: !a("startsWith") },
			{
				startsWith: function (t) {
					var e = i(this, t, "startsWith"),
						r = o(
							Math.min(arguments.length > 1 ? arguments[1] : void 0, e.length)
						),
						n = String(t);
					return s ? s.call(e, n, r) : e.slice(r, r + n.length) === n;
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(47);
		n(
			{ target: "String", proto: !0, forced: r(97)("trim") },
			{
				trim: function () {
					return o(this, 3);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(47),
			i = r(97)("trimStart"),
			a = i
				? function () {
						return o(this, 1);
				  }
				: "".trimStart;
		n(
			{ target: "String", proto: !0, forced: i },
			{ trimStart: a, trimLeft: a }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(47),
			i = r(97)("trimEnd"),
			a = i
				? function () {
						return o(this, 2);
				  }
				: "".trimEnd;
		n({ target: "String", proto: !0, forced: i }, { trimEnd: a, trimRight: a });
	},
	function (t, e, r) {
		"use strict";
		var n = r(91),
			o = r(26),
			i = r(90),
			a = o.set,
			s = o.getterFor("String Iterator");
		i(
			String,
			"String",
			function (t) {
				a(this, { type: "String Iterator", string: String(t), index: 0 });
			},
			function () {
				var t,
					e = s(this),
					r = e.string,
					o = e.index;
				return o >= r.length
					? { value: void 0, done: !0 }
					: ((t = n(r, o, !0)), (e.index += t.length), { value: t, done: !1 });
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("anchor") },
			{
				anchor: function (t) {
					return o(this, "a", "name", t);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("big") },
			{
				big: function () {
					return o(this, "big", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("blink") },
			{
				blink: function () {
					return o(this, "blink", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("bold") },
			{
				bold: function () {
					return o(this, "b", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("fixed") },
			{
				fixed: function () {
					return o(this, "tt", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("fontcolor") },
			{
				fontcolor: function (t) {
					return o(this, "font", "color", t);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("fontsize") },
			{
				fontsize: function (t) {
					return o(this, "font", "size", t);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("italics") },
			{
				italics: function () {
					return o(this, "i", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("link") },
			{
				link: function (t) {
					return o(this, "a", "href", t);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("small") },
			{
				small: function () {
					return o(this, "small", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("strike") },
			{
				strike: function () {
					return o(this, "strike", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("sub") },
			{
				sub: function () {
					return o(this, "sub", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(20);
		n(
			{ target: "String", proto: !0, forced: r(21)("sup") },
			{
				sup: function () {
					return o(this, "sup", "", "");
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(6),
			o = r(2),
			i = r(56),
			a = r(98),
			s = r(9).f,
			c = r(39).f,
			u = r(93),
			l = r(63),
			f = r(22),
			h = r(1),
			d = r(46),
			p = r(8)("match"),
			g = o.RegExp,
			v = g.prototype,
			m = /a/g,
			y = /a/g,
			_ = new g(m) !== m;
		if (
			i(
				"RegExp",
				n &&
					(!_ ||
						h(function () {
							return (y[p] = !1), g(m) != m || g(y) == y || "/a/i" != g(m, "i");
						}))
			)
		) {
			for (
				var E = function (t, e) {
						var r = this instanceof E,
							n = u(t),
							o = void 0 === e;
						return !r && n && t.constructor === E && o
							? t
							: a(
									_
										? new g(n && !o ? t.source : t, e)
										: g(
												(n = t instanceof E) ? t.source : t,
												n && o ? l.call(t) : e
										  ),
									r ? this : v,
									E
							  );
					},
					S = function (t) {
						(t in E) ||
							s(E, t, {
								configurable: !0,
								get: function () {
									return g[t];
								},
								set: function (e) {
									g[t] = e;
								},
							});
					},
					T = c(g),
					b = 0;
				b < T.length;

			)
				S(T[b++]);
			(v.constructor = E), (E.prototype = v), f(o, "RegExp", E);
		}
		d("RegExp");
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(73);
		n({ target: "RegExp", proto: !0, forced: /./.exec !== o }, { exec: o });
	},
	function (t, e, r) {
		var n = r(6),
			o = r(9),
			i = r(63);
		n &&
			"g" != /./g.flags &&
			o.f(RegExp.prototype, "flags", { configurable: !0, get: i });
	},
	function (t, e, r) {
		"use strict";
		var n = r(22),
			o = r(5),
			i = r(1),
			a = r(63),
			s = /./.toString,
			c = RegExp.prototype,
			u = i(function () {
				return "/a/b" != s.call({ source: "a", flags: "b" });
			}),
			l = "toString" != s.name;
		(u || l) &&
			n(
				RegExp.prototype,
				"toString",
				function () {
					var t = o(this),
						e = String(t.source),
						r = t.flags;
					return (
						"/" +
						e +
						"/" +
						String(
							void 0 === r && t instanceof RegExp && !("flags" in c)
								? a.call(t)
								: r
						)
					);
				},
				{ unsafe: !0 }
			);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(124);
		n({ global: !0, forced: parseInt != o }, { parseInt: o });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(125);
		n({ global: !0, forced: parseFloat != o }, { parseFloat: o });
	},
	function (t, e, r) {
		"use strict";
		var n = r(6),
			o = r(2),
			i = r(56),
			a = r(22),
			s = r(12),
			c = r(27),
			u = r(98),
			l = r(25),
			f = r(1),
			h = r(35),
			d = r(39).f,
			p = r(15).f,
			g = r(9).f,
			v = r(47),
			m = o.Number,
			y = m.prototype,
			_ = "Number" == c(h(y)),
			E = "trim" in String.prototype,
			S = function (t) {
				var e,
					r,
					n,
					o,
					i,
					a,
					s,
					c,
					u = l(t, !1);
				if ("string" == typeof u && u.length > 2)
					if (
						43 === (e = (u = E ? u.trim() : v(u, 3)).charCodeAt(0)) ||
						45 === e
					) {
						if (88 === (r = u.charCodeAt(2)) || 120 === r) return NaN;
					} else if (48 === e) {
						switch (u.charCodeAt(1)) {
							case 66:
							case 98:
								(n = 2), (o = 49);
								break;
							case 79:
							case 111:
								(n = 8), (o = 55);
								break;
							default:
								return +u;
						}
						for (a = (i = u.slice(2)).length, s = 0; s < a; s++)
							if ((c = i.charCodeAt(s)) < 48 || c > o) return NaN;
						return parseInt(i, n);
					}
				return +u;
			};
		if (i("Number", !m(" 0o1") || !m("0b1") || m("+0x1"))) {
			for (
				var T,
					b = function (t) {
						var e = arguments.length < 1 ? 0 : t,
							r = this;
						return r instanceof b &&
							(_
								? f(function () {
										y.valueOf.call(r);
								  })
								: "Number" != c(r))
							? u(new m(S(e)), r, b)
							: S(e);
					},
					I = n
						? d(m)
						: "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(
								","
						  ),
					A = 0;
				I.length > A;
				A++
			)
				s(m, (T = I[A])) && !s(b, T) && g(b, T, p(m, T));
			(b.prototype = y), (y.constructor = b), a(o, "Number", b);
		}
	},
	function (t, e, r) {
		r(0)({ target: "Number", stat: !0 }, { EPSILON: Math.pow(2, -52) });
	},
	function (t, e, r) {
		r(0)({ target: "Number", stat: !0 }, { isFinite: r(257) });
	},
	function (t, e, r) {
		var n = r(2).isFinite;
		t.exports =
			Number.isFinite ||
			function (t) {
				return "number" == typeof t && n(t);
			};
	},
	function (t, e, r) {
		r(0)({ target: "Number", stat: !0 }, { isInteger: r(126) });
	},
	function (t, e, r) {
		r(0)(
			{ target: "Number", stat: !0 },
			{
				isNaN: function (t) {
					return t != t;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(126),
			i = Math.abs;
		n(
			{ target: "Number", stat: !0 },
			{
				isSafeInteger: function (t) {
					return o(t) && i(t) <= 9007199254740991;
				},
			}
		);
	},
	function (t, e, r) {
		r(0)(
			{ target: "Number", stat: !0 },
			{ MAX_SAFE_INTEGER: 9007199254740991 }
		);
	},
	function (t, e, r) {
		r(0)(
			{ target: "Number", stat: !0 },
			{ MIN_SAFE_INTEGER: -9007199254740991 }
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(125);
		n(
			{ target: "Number", stat: !0, forced: Number.parseFloat != o },
			{ parseFloat: o }
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(124);
		n(
			{ target: "Number", stat: !0, forced: Number.parseInt != o },
			{ parseInt: o }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(23),
			i = r(127),
			a = r(95),
			s = r(1),
			c = (1).toFixed,
			u = Math.floor,
			l = [0, 0, 0, 0, 0, 0],
			f = function (t, e) {
				for (var r = -1, n = e; ++r < 6; )
					(n += t * l[r]), (l[r] = n % 1e7), (n = u(n / 1e7));
			},
			h = function (t) {
				for (var e = 6, r = 0; --e >= 0; )
					(r += l[e]), (l[e] = u(r / t)), (r = (r % t) * 1e7);
			},
			d = function () {
				for (var t = 6, e = ""; --t >= 0; )
					if ("" !== e || 0 === t || 0 !== l[t]) {
						var r = String(l[t]);
						e = "" === e ? r : e + a.call("0", 7 - r.length) + r;
					}
				return e;
			},
			p = function (t, e, r) {
				return 0 === e
					? r
					: e % 2 == 1
					? p(t, e - 1, r * t)
					: p(t * t, e / 2, r);
			};
		n(
			{
				target: "Number",
				proto: !0,
				forced:
					(c &&
						("0.000" !== (8e-5).toFixed(3) ||
							"1" !== (0.9).toFixed(0) ||
							"1.25" !== (1.255).toFixed(2) ||
							"1000000000000000128" !== (0xde0b6b3a7640080).toFixed(0))) ||
					!s(function () {
						c.call({});
					}),
			},
			{
				toFixed: function (t) {
					var e,
						r,
						n,
						s,
						c = i(this),
						u = o(t),
						l = "",
						g = "0";
					if (u < 0 || u > 20) throw RangeError("Incorrect fraction digits");
					if (c != c) return "NaN";
					if (c <= -1e21 || c >= 1e21) return String(c);
					if ((c < 0 && ((l = "-"), (c = -c)), c > 1e-21))
						if (
							((r =
								(e =
									(function (t) {
										for (var e = 0, r = t; r >= 4096; ) (e += 12), (r /= 4096);
										for (; r >= 2; ) (e += 1), (r /= 2);
										return e;
									})(c * p(2, 69, 1)) - 69) < 0
									? c * p(2, -e, 1)
									: c / p(2, e, 1)),
							(r *= 4503599627370496),
							(e = 52 - e) > 0)
						) {
							for (f(0, r), n = u; n >= 7; ) f(1e7, 0), (n -= 7);
							for (f(p(10, n, 1), 0), n = e - 1; n >= 23; )
								h(1 << 23), (n -= 23);
							h(1 << n), f(1, 1), h(2), (g = d());
						} else f(0, r), f(1 << -e, 0), (g = d() + a.call("0", u));
					return (g =
						u > 0
							? l +
							  ((s = g.length) <= u
									? "0." + a.call("0", u - s) + g
									: g.slice(0, s - u) + "." + g.slice(s - u))
							: l + g);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(1),
			i = r(127),
			a = (1).toPrecision;
		n(
			{
				target: "Number",
				proto: !0,
				forced:
					o(function () {
						return "1" !== a.call(1, void 0);
					}) ||
					!o(function () {
						a.call({});
					}),
			},
			{
				toPrecision: function (t) {
					return void 0 === t ? a.call(i(this)) : a.call(i(this), t);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(128),
			i = Math.acosh,
			a = Math.log,
			s = Math.sqrt,
			c = Math.LN2;
		n(
			{
				target: "Math",
				stat: !0,
				forced:
					!i || 710 != Math.floor(i(Number.MAX_VALUE)) || i(1 / 0) != 1 / 0,
			},
			{
				acosh: function (t) {
					return (t = +t) < 1
						? NaN
						: t > 94906265.62425156
						? a(t) + c
						: o(t - 1 + s(t - 1) * s(t + 1));
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.asinh,
			i = Math.log,
			a = Math.sqrt;
		n(
			{ target: "Math", stat: !0, forced: !(o && 1 / o(0) > 0) },
			{
				asinh: function t(e) {
					return isFinite((e = +e)) && 0 != e
						? e < 0
							? -t(-e)
							: i(e + a(e * e + 1))
						: e;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.atanh,
			i = Math.log;
		n(
			{ target: "Math", stat: !0, forced: !(o && 1 / o(-0) < 0) },
			{
				atanh: function (t) {
					return 0 == (t = +t) ? t : i((1 + t) / (1 - t)) / 2;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(99),
			i = Math.abs,
			a = Math.pow;
		n(
			{ target: "Math", stat: !0 },
			{
				cbrt: function (t) {
					return o((t = +t)) * a(i(t), 1 / 3);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.floor,
			i = Math.log,
			a = Math.LOG2E;
		n(
			{ target: "Math", stat: !0 },
			{
				clz32: function (t) {
					return (t >>>= 0) ? 31 - o(i(t + 0.5) * a) : 32;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(77),
			i = Math.cosh,
			a = Math.abs,
			s = Math.E;
		n(
			{ target: "Math", stat: !0, forced: !i || i(710) === 1 / 0 },
			{
				cosh: function (t) {
					var e = o(a(t) - 1) + 1;
					return (e + 1 / (e * s * s)) * (s / 2);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(77);
		n({ target: "Math", stat: !0, forced: o != Math.expm1 }, { expm1: o });
	},
	function (t, e, r) {
		r(0)({ target: "Math", stat: !0 }, { fround: r(275) });
	},
	function (t, e, r) {
		var n = r(99),
			o = Math.pow,
			i = o(2, -52),
			a = o(2, -23),
			s = o(2, 127) * (2 - a),
			c = o(2, -126);
		t.exports =
			Math.fround ||
			function (t) {
				var e,
					r,
					o = Math.abs(t),
					u = n(t);
				return o < c
					? u * (o / c / a + 1 / i - 1 / i) * c * a
					: (r = (e = (1 + a / i) * o) - (e - o)) > s || r != r
					? u * (1 / 0)
					: u * r;
			};
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.abs,
			i = Math.sqrt;
		n(
			{ target: "Math", stat: !0 },
			{
				hypot: function (t, e) {
					for (var r, n, a = 0, s = 0, c = arguments.length, u = 0; s < c; )
						u < (r = o(arguments[s++]))
							? ((a = a * (n = u / r) * n + 1), (u = r))
							: (a += r > 0 ? (n = r / u) * n : r);
					return u === 1 / 0 ? 1 / 0 : u * i(a);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = Math.imul;
		n(
			{
				target: "Math",
				stat: !0,
				forced: o(function () {
					return -5 != i(4294967295, 5) || 2 != i.length;
				}),
			},
			{
				imul: function (t, e) {
					var r = +t,
						n = +e,
						o = 65535 & r,
						i = 65535 & n;
					return (
						0 |
						(o * i +
							((((65535 & (r >>> 16)) * i + o * (65535 & (n >>> 16))) << 16) >>>
								0))
					);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.log,
			i = Math.LOG10E;
		n(
			{ target: "Math", stat: !0 },
			{
				log10: function (t) {
					return o(t) * i;
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "Math", stat: !0 }, { log1p: r(128) });
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.log,
			i = Math.LN2;
		n(
			{ target: "Math", stat: !0 },
			{
				log2: function (t) {
					return o(t) / i;
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "Math", stat: !0 }, { sign: r(99) });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(77),
			a = Math.abs,
			s = Math.exp,
			c = Math.E;
		n(
			{
				target: "Math",
				stat: !0,
				forced: o(function () {
					return -2e-17 != Math.sinh(-2e-17);
				}),
			},
			{
				sinh: function (t) {
					return a((t = +t)) < 1
						? (i(t) - i(-t)) / 2
						: (s(t - 1) - s(-t - 1)) * (c / 2);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(77),
			i = Math.exp;
		n(
			{ target: "Math", stat: !0 },
			{
				tanh: function (t) {
					var e = o((t = +t)),
						r = o(-t);
					return e == 1 / 0 ? 1 : r == 1 / 0 ? -1 : (e - r) / (i(t) + i(-t));
				},
			}
		);
	},
	function (t, e, r) {
		r(34)(Math, "Math", !0);
	},
	function (t, e, r) {
		var n = r(0),
			o = Math.ceil,
			i = Math.floor;
		n(
			{ target: "Math", stat: !0 },
			{
				trunc: function (t) {
					return (t > 0 ? i : o)(t);
				},
			}
		);
	},
	function (t, e, r) {
		r(0)(
			{ target: "Date", stat: !0 },
			{
				now: function () {
					return new Date().getTime();
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(1),
			i = r(10),
			a = r(25);
		n(
			{
				target: "Date",
				proto: !0,
				forced: o(function () {
					return (
						null !== new Date(NaN).toJSON() ||
						1 !==
							Date.prototype.toJSON.call({
								toISOString: function () {
									return 1;
								},
							})
					);
				}),
			},
			{
				toJSON: function (t) {
					var e = i(this),
						r = a(e);
					return "number" != typeof r || isFinite(r) ? e.toISOString() : null;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(289);
		n(
			{ target: "Date", proto: !0, forced: Date.prototype.toISOString !== o },
			{ toISOString: o }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(1),
			o = Date.prototype,
			i = o.getTime,
			a = o.toISOString,
			s = function (t) {
				return t > 9 ? t : "0" + t;
			};
		t.exports =
			n(function () {
				return "0385-07-25T07:06:39.999Z" != a.call(new Date(-5e13 - 1));
			}) ||
			!n(function () {
				a.call(new Date(NaN));
			})
				? function () {
						if (!isFinite(i.call(this))) throw RangeError("Invalid time value");
						var t = this.getUTCFullYear(),
							e = this.getUTCMilliseconds(),
							r = t < 0 ? "-" : t > 9999 ? "+" : "";
						return (
							r +
							("00000" + Math.abs(t)).slice(r ? -6 : -4) +
							"-" +
							s(this.getUTCMonth() + 1) +
							"-" +
							s(this.getUTCDate()) +
							"T" +
							s(this.getUTCHours()) +
							":" +
							s(this.getUTCMinutes()) +
							":" +
							s(this.getUTCSeconds()) +
							"." +
							(e > 99 ? e : "0" + s(e)) +
							"Z"
						);
				  }
				: a;
	},
	function (t, e, r) {
		var n = r(22),
			o = Date.prototype,
			i = o.toString,
			a = o.getTime;
		new Date(NaN) + "" != "Invalid Date" &&
			n(o, "toString", function () {
				var t = a.call(this);
				return t == t ? i.call(this) : "Invalid Date";
			});
	},
	function (t, e, r) {
		var n = r(14),
			o = r(292),
			i = r(8)("toPrimitive"),
			a = Date.prototype;
		i in a || n(a, i, o);
	},
	function (t, e, r) {
		"use strict";
		var n = r(5),
			o = r(25);
		t.exports = function (t) {
			if ("string" !== t && "number" !== t && "default" !== t)
				throw TypeError("Incorrect hint");
			return o(n(this), "number" !== t);
		};
	},
	function (t, e, r) {
		var n = r(2);
		r(34)(n.JSON, "JSON", !0);
	},
	function (t, e, r) {
		var n,
			o,
			i,
			a,
			s,
			c,
			u,
			l = r(2),
			f = r(15).f,
			h = r(27),
			d = r(129).set,
			p = r(96),
			g = l.MutationObserver || l.WebKitMutationObserver,
			v = l.process,
			m = l.Promise,
			y = "process" == h(v),
			_ = f(l, "queueMicrotask"),
			E = _ && _.value;
		E ||
			((n = function () {
				var t, e;
				for (y && (t = v.domain) && t.exit(); o; ) {
					(e = o.fn), (o = o.next);
					try {
						e();
					} catch (t) {
						throw (o ? a() : (i = void 0), t);
					}
				}
				(i = void 0), t && t.enter();
			}),
			y
				? (a = function () {
						v.nextTick(n);
				  })
				: g && !/(iphone|ipod|ipad).*applewebkit/i.test(p)
				? ((s = !0),
				  (c = document.createTextNode("")),
				  new g(n).observe(c, { characterData: !0 }),
				  (a = function () {
						c.data = s = !s;
				  }))
				: m && m.resolve
				? ((u = m.resolve(void 0)),
				  (a = function () {
						u.then(n);
				  }))
				: (a = function () {
						d.call(l, n);
				  })),
			(t.exports =
				E ||
				function (t) {
					var e = { fn: t, next: void 0 };
					i && (i.next = e), o || ((o = e), a()), (i = e);
				});
	},
	function (t, e, r) {
		var n = r(2);
		t.exports = function (t, e) {
			var r = n.console;
			r && r.error && (1 === arguments.length ? r.error(t) : r.error(t, e));
		};
	},
	function (t, e) {
		t.exports = function (t) {
			try {
				return { error: !1, value: t() };
			} catch (t) {
				return { error: !0, value: t };
			}
		};
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(89),
			i = r(31),
			a = r(130);
		n(
			{ target: "Promise", proto: !0, real: !0 },
			{
				finally: function (t) {
					var e = i(this, o("Promise")),
						r = "function" == typeof t;
					return this.then(
						r
							? function (r) {
									return a(e, t()).then(function () {
										return r;
									});
							  }
							: t,
						r
							? function (r) {
									return a(e, t()).then(function () {
										throw r;
									});
							  }
							: t
					);
				},
			}
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(78),
			o = r(132);
		t.exports = n(
			"Map",
			function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			o,
			!0
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(78),
			o = r(132);
		t.exports = n(
			"Set",
			function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			o
		);
	},
	function (t, e, r) {
		"use strict";
		var n,
			o = r(2),
			i = r(64),
			a = r(41),
			s = r(78),
			c = r(133),
			u = r(3),
			l = r(26).enforce,
			f = r(104),
			h = !o.ActiveXObject && "ActiveXObject" in o,
			d = Object.isExtensible,
			p = function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			g = (t.exports = s("WeakMap", p, c, !0, !0));
		if (f && h) {
			(n = c.getConstructor(p, "WeakMap", !0)), (a.REQUIRED = !0);
			var v = g.prototype,
				m = v.delete,
				y = v.has,
				_ = v.get,
				E = v.set;
			i(v, {
				delete: function (t) {
					if (u(t) && !d(t)) {
						var e = l(this);
						return (
							e.frozen || (e.frozen = new n()),
							m.call(this, t) || e.frozen.delete(t)
						);
					}
					return m.call(this, t);
				},
				has: function (t) {
					if (u(t) && !d(t)) {
						var e = l(this);
						return (
							e.frozen || (e.frozen = new n()),
							y.call(this, t) || e.frozen.has(t)
						);
					}
					return y.call(this, t);
				},
				get: function (t) {
					if (u(t) && !d(t)) {
						var e = l(this);
						return (
							e.frozen || (e.frozen = new n()),
							y.call(this, t) ? _.call(this, t) : e.frozen.get(t)
						);
					}
					return _.call(this, t);
				},
				set: function (t, e) {
					if (u(t) && !d(t)) {
						var r = l(this);
						r.frozen || (r.frozen = new n()),
							y.call(this, t) ? E.call(this, t, e) : r.frozen.set(t, e);
					} else E.call(this, t, e);
					return this;
				},
			});
		}
	},
	function (t, e, r) {
		"use strict";
		r(78)(
			"WeakSet",
			function (t) {
				return function () {
					return t(this, arguments.length > 0 ? arguments[0] : void 0);
				};
			},
			r(133),
			!1,
			!0
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(2),
			i = r(79),
			a = r(46),
			s = i.ArrayBuffer;
		n({ global: !0, forced: o.ArrayBuffer !== s }, { ArrayBuffer: s }),
			a("ArrayBuffer");
	},
	function (t, e, r) {
		var n = r(0),
			o = r(4);
		n(
			{ target: "ArrayBuffer", stat: !0, forced: !o.NATIVE_ARRAY_BUFFER_VIEWS },
			{ isView: o.isView }
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(0),
			o = r(1),
			i = r(79),
			a = r(5),
			s = r(33),
			c = r(7),
			u = r(31),
			l = i.ArrayBuffer,
			f = i.DataView,
			h = l.prototype.slice;
		n(
			{
				target: "ArrayBuffer",
				proto: !0,
				unsafe: !0,
				forced: o(function () {
					return !new l(2).slice(1, void 0).byteLength;
				}),
			},
			{
				slice: function (t, e) {
					if (void 0 !== h && void 0 === e) return h.call(a(this), t);
					for (
						var r = a(this).byteLength,
							n = s(t, r),
							o = s(void 0 === e ? r : e, r),
							i = new (u(this, l))(c(o - n)),
							d = new f(this),
							p = new f(i),
							g = 0;
						n < o;

					)
						p.setUint8(g++, d.getUint8(n++));
					return i;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(79);
		n(
			{ global: !0, forced: !r(4).NATIVE_ARRAY_BUFFER },
			{ DataView: o.DataView }
		);
	},
	function (t, e, r) {
		r(32)("Int8", 1, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(32)("Uint8", 1, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(32)(
			"Uint8",
			1,
			function (t) {
				return function (e, r, n) {
					return t(this, e, r, n);
				};
			},
			!0
		);
	},
	function (t, e, r) {
		r(32)("Int16", 2, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(32)("Uint16", 2, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(32)("Int32", 4, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(32)("Uint32", 4, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(32)("Float32", 4, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		r(32)("Float64", 8, function (t) {
			return function (e, r, n) {
				return t(this, e, r, n);
			};
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(101),
			o = r(4),
			i = r(136);
		o.exportStatic("from", i, n);
	},
	function (t, e, r) {
		"use strict";
		var n = r(101),
			o = r(4),
			i = o.aTypedArrayConstructor;
		o.exportStatic(
			"of",
			function () {
				for (var t = 0, e = arguments.length, r = new (i(this))(e); e > t; )
					r[t] = arguments[t++];
				return r;
			},
			n
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(117),
			o = r(4),
			i = o.aTypedArray;
		o.exportProto("copyWithin", function (t, e) {
			return n.call(
				i(this),
				t,
				e,
				arguments.length > 2 ? arguments[2] : void 0
			);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(13),
			o = r(4),
			i = n(4),
			a = o.aTypedArray;
		o.exportProto("every", function (t) {
			return i(a(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(4),
			o = r(88),
			i = n.aTypedArray;
		n.exportProto("fill", function (t) {
			return o.apply(i(this), arguments);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(31),
			o = r(4),
			i = r(13)(2),
			a = o.aTypedArray,
			s = o.aTypedArrayConstructor;
		o.exportProto("filter", function (t) {
			for (
				var e = i(a(this), t, arguments.length > 1 ? arguments[1] : void 0),
					r = n(this, this.constructor),
					o = 0,
					c = e.length,
					u = new (s(r))(c);
				c > o;

			)
				u[o] = e[o++];
			return u;
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(4),
			o = r(13)(5),
			i = n.aTypedArray;
		n.exportProto("find", function (t) {
			return o(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(4),
			o = r(13)(6),
			i = n.aTypedArray;
		n.exportProto("findIndex", function (t) {
			return o(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(4),
			o = r(13)(0),
			i = n.aTypedArray;
		n.exportProto("forEach", function (t) {
			o(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(55),
			o = r(4),
			i = o.aTypedArray,
			a = n(!0);
		o.exportProto("includes", function (t) {
			return a(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(55),
			o = r(4),
			i = o.aTypedArray,
			a = n(!1);
		o.exportProto("indexOf", function (t) {
			return a(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(2),
			o = r(30),
			i = r(4),
			a = r(8)("iterator"),
			s = n.Uint8Array,
			c = o.values,
			u = o.keys,
			l = o.entries,
			f = i.aTypedArray,
			h = i.exportProto,
			d = s && s.prototype[a],
			p = !!d && ("values" == d.name || null == d.name),
			g = function () {
				return c.call(f(this));
			};
		h("entries", function () {
			return l.call(f(this));
		}),
			h("keys", function () {
				return u.call(f(this));
			}),
			h("values", g, !p),
			h(a, g, !p);
	},
	function (t, e, r) {
		"use strict";
		var n = r(4),
			o = n.aTypedArray,
			i = [].join;
		n.exportProto("join", function (t) {
			return i.apply(o(this), arguments);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(119),
			o = r(4),
			i = o.aTypedArray;
		o.exportProto("lastIndexOf", function (t) {
			return n.apply(i(this), arguments);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(31),
			o = r(4),
			i = r(13),
			a = o.aTypedArray,
			s = o.aTypedArrayConstructor,
			c = i(1, function (t, e) {
				return new (s(n(t, t.constructor)))(e);
			});
		o.exportProto("map", function (t) {
			return c(a(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(4),
			o = r(71),
			i = n.aTypedArray;
		n.exportProto("reduce", function (t) {
			return o(i(this), t, arguments.length, arguments[1], !1);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(4),
			o = r(71),
			i = n.aTypedArray;
		n.exportProto("reduceRight", function (t) {
			return o(i(this), t, arguments.length, arguments[1], !0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(4),
			o = n.aTypedArray;
		n.exportProto("reverse", function () {
			for (var t, e = o(this).length, r = Math.floor(e / 2), n = 0; n < r; )
				(t = this[n]), (this[n++] = this[--e]), (this[e] = t);
			return this;
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(7),
			o = r(135),
			i = r(10),
			a = r(4),
			s = r(1),
			c = a.aTypedArray,
			u = s(function () {
				new Int8Array(1).set({});
			});
		a.exportProto(
			"set",
			function (t) {
				c(this);
				var e = o(arguments[1], 1),
					r = this.length,
					a = i(t),
					s = n(a.length),
					u = 0;
				if (s + e > r) throw RangeError("Wrong length");
				for (; u < s; ) this[e + u] = a[u++];
			},
			u
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(31),
			o = r(4),
			i = r(1),
			a = o.aTypedArray,
			s = o.aTypedArrayConstructor,
			c = [].slice,
			u = i(function () {
				new Int8Array(1).slice();
			});
		o.exportProto(
			"slice",
			function (t, e) {
				for (
					var r = c.call(a(this), t, e),
						o = n(this, this.constructor),
						i = 0,
						u = r.length,
						l = new (s(o))(u);
					u > i;

				)
					l[i] = r[i++];
				return l;
			},
			u
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(4),
			o = r(13)(3),
			i = n.aTypedArray;
		n.exportProto("some", function (t) {
			return o(i(this), t, arguments.length > 1 ? arguments[1] : void 0);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(4),
			o = n.aTypedArray,
			i = [].sort;
		n.exportProto("sort", function (t) {
			return i.call(o(this), t);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(7),
			o = r(33),
			i = r(31),
			a = r(4),
			s = a.aTypedArray;
		a.exportProto("subarray", function (t, e) {
			var r = s(this),
				a = r.length,
				c = o(t, a);
			return new (i(r, r.constructor))(
				r.buffer,
				r.byteOffset + c * r.BYTES_PER_ELEMENT,
				n((void 0 === e ? a : o(e, a)) - c)
			);
		});
	},
	function (t, e, r) {
		"use strict";
		var n = r(2),
			o = r(1),
			i = r(4),
			a = n.Int8Array,
			s = i.aTypedArray,
			c = [].toLocaleString,
			u = [].slice,
			l =
				!!a &&
				o(function () {
					c.call(new a(1));
				}),
			f =
				o(function () {
					return [1, 2].toLocaleString() != new a([1, 2]).toLocaleString();
				}) ||
				!o(function () {
					a.prototype.toLocaleString.call([1, 2]);
				});
		i.exportProto(
			"toLocaleString",
			function () {
				return c.apply(l ? u.call(s(this)) : s(this), arguments);
			},
			f
		);
	},
	function (t, e, r) {
		"use strict";
		var n = r(2),
			o = r(4),
			i = r(1),
			a = n.Uint8Array,
			s = a && a.prototype,
			c = [].toString,
			u = [].join;
		i(function () {
			c.call({});
		}) &&
			(c = function () {
				return u.call(this);
			}),
			o.exportProto("toString", c, (s || {}).toString != c);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(2),
			i = r(19),
			a = r(5),
			s = r(1),
			c = (o.Reflect || {}).apply,
			u = Function.apply;
		n(
			{
				target: "Reflect",
				stat: !0,
				forced: !s(function () {
					c(function () {});
				}),
			},
			{
				apply: function (t, e, r) {
					return i(t), a(r), c ? c(t, e, r) : u.call(t, e, r);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(2),
			i = r(35),
			a = r(19),
			s = r(5),
			c = r(3),
			u = r(1),
			l = r(116),
			f = (o.Reflect || {}).construct,
			h = u(function () {
				function t() {}
				return !(f(function () {}, [], t) instanceof t);
			}),
			d = !u(function () {
				f(function () {});
			}),
			p = h || d;
		n(
			{ target: "Reflect", stat: !0, forced: p, sham: p },
			{
				construct: function (t, e) {
					a(t), s(e);
					var r = arguments.length < 3 ? t : a(arguments[2]);
					if (d && !h) return f(t, e, r);
					if (t == r) {
						switch (e.length) {
							case 0:
								return new t();
							case 1:
								return new t(e[0]);
							case 2:
								return new t(e[0], e[1]);
							case 3:
								return new t(e[0], e[1], e[2]);
							case 4:
								return new t(e[0], e[1], e[2], e[3]);
						}
						var n = [null];
						return n.push.apply(n, e), new (l.apply(t, n))();
					}
					var o = r.prototype,
						u = i(c(o) ? o : Object.prototype),
						p = Function.apply.call(t, u, e);
					return c(p) ? p : u;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(1),
			i = r(9),
			a = r(5),
			s = r(25),
			c = r(6);
		n(
			{
				target: "Reflect",
				stat: !0,
				forced: o(function () {
					Reflect.defineProperty(i.f({}, 1, { value: 1 }), 1, { value: 2 });
				}),
				sham: !c,
			},
			{
				defineProperty: function (t, e, r) {
					a(t), (e = s(e, !0)), a(r);
					try {
						return i.f(t, e, r), !0;
					} catch (t) {
						return !1;
					}
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(15).f,
			i = r(5);
		n(
			{ target: "Reflect", stat: !0 },
			{
				deleteProperty: function (t, e) {
					var r = o(i(t), e);
					return !(r && !r.configurable) && delete t[e];
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(15),
			i = r(28),
			a = r(12),
			s = r(3),
			c = r(5);
		n(
			{ target: "Reflect", stat: !0 },
			{
				get: function t(e, r) {
					var n,
						u,
						l = arguments.length < 3 ? e : arguments[2];
					return c(e) === l
						? e[r]
						: (n = o.f(e, r))
						? a(n, "value")
							? n.value
							: void 0 === n.get
							? void 0
							: n.get.call(l)
						: s((u = i(e)))
						? t(u, r, l)
						: void 0;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(15),
			i = r(5);
		n(
			{ target: "Reflect", stat: !0, sham: !r(6) },
			{
				getOwnPropertyDescriptor: function (t, e) {
					return o.f(i(t), e);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(28),
			i = r(5);
		n(
			{ target: "Reflect", stat: !0, sham: !r(87) },
			{
				getPrototypeOf: function (t) {
					return o(i(t));
				},
			}
		);
	},
	function (t, e, r) {
		r(0)(
			{ target: "Reflect", stat: !0 },
			{
				has: function (t, e) {
					return e in t;
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(5),
			i = Object.isExtensible;
		n(
			{ target: "Reflect", stat: !0 },
			{
				isExtensible: function (t) {
					return o(t), !i || i(t);
				},
			}
		);
	},
	function (t, e, r) {
		r(0)({ target: "Reflect", stat: !0 }, { ownKeys: r(83) });
	},
	function (t, e, r) {
		var n = r(0),
			o = r(89),
			i = r(5);
		n(
			{ target: "Reflect", stat: !0, sham: !r(57) },
			{
				preventExtensions: function (t) {
					i(t);
					try {
						var e = o("Object", "preventExtensions");
						return e && e(t), !0;
					} catch (t) {
						return !1;
					}
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(9),
			i = r(15),
			a = r(28),
			s = r(12),
			c = r(37),
			u = r(5),
			l = r(3);
		n(
			{ target: "Reflect", stat: !0 },
			{
				set: function t(e, r, n) {
					var f,
						h,
						d = arguments.length < 4 ? e : arguments[3],
						p = i.f(u(e), r);
					if (!p) {
						if (l((h = a(e)))) return t(h, r, n, d);
						p = c(0);
					}
					if (s(p, "value")) {
						if (!1 === p.writable || !l(d)) return !1;
						if ((f = i.f(d, r))) {
							if (f.get || f.set || !1 === f.writable) return !1;
							(f.value = n), o.f(d, r, f);
						} else o.f(d, r, c(0, n));
						return !0;
					}
					return void 0 !== p.set && (p.set.call(d, n), !0);
				},
			}
		);
	},
	function (t, e, r) {
		var n = r(0),
			o = r(45),
			i = r(115);
		o &&
			n(
				{ target: "Reflect", stat: !0 },
				{
					setPrototypeOf: function (t, e) {
						i(t, e);
						try {
							return o(t, e), !0;
						} catch (t) {
							return !1;
						}
					},
				}
			);
	},
	function (t, e, r) {
		var n = (function (t) {
			"use strict";
			var e,
				r = Object.prototype,
				n = r.hasOwnProperty,
				o = "function" == typeof Symbol ? Symbol : {},
				i = o.iterator || "@@iterator",
				a = o.asyncIterator || "@@asyncIterator",
				s = o.toStringTag || "@@toStringTag";
			function c(t, e, r, n) {
				var o = e && e.prototype instanceof g ? e : g,
					i = Object.create(o.prototype),
					a = new x(n || []);
				return (
					(i._invoke = (function (t, e, r) {
						var n = l;
						return function (o, i) {
							if (n === h) throw new Error("Generator is already running");
							if (n === d) {
								if ("throw" === o) throw i;
								return M();
							}
							for (r.method = o, r.arg = i; ; ) {
								var a = r.delegate;
								if (a) {
									var s = I(a, r);
									if (s) {
										if (s === p) continue;
										return s;
									}
								}
								if ("next" === r.method) r.sent = r._sent = r.arg;
								else if ("throw" === r.method) {
									if (n === l) throw ((n = d), r.arg);
									r.dispatchException(r.arg);
								} else "return" === r.method && r.abrupt("return", r.arg);
								n = h;
								var c = u(t, e, r);
								if ("normal" === c.type) {
									if (((n = r.done ? d : f), c.arg === p)) continue;
									return { value: c.arg, done: r.done };
								}
								"throw" === c.type &&
									((n = d), (r.method = "throw"), (r.arg = c.arg));
							}
						};
					})(t, r, a)),
					i
				);
			}
			function u(t, e, r) {
				try {
					return { type: "normal", arg: t.call(e, r) };
				} catch (t) {
					return { type: "throw", arg: t };
				}
			}
			t.wrap = c;
			var l = "suspendedStart",
				f = "suspendedYield",
				h = "executing",
				d = "completed",
				p = {};
			function g() {}
			function v() {}
			function m() {}
			var y = {};
			y[i] = function () {
				return this;
			};
			var _ = Object.getPrototypeOf,
				E = _ && _(_(O([])));
			E && E !== r && n.call(E, i) && (y = E);
			var S = (m.prototype = g.prototype = Object.create(y));
			function T(t) {
				["next", "throw", "return"].forEach(function (e) {
					t[e] = function (t) {
						return this._invoke(e, t);
					};
				});
			}
			function b(t) {
				var e;
				this._invoke = function (r, o) {
					function i() {
						return new Promise(function (e, i) {
							!(function e(r, o, i, a) {
								var s = u(t[r], t, o);
								if ("throw" !== s.type) {
									var c = s.arg,
										l = c.value;
									return l && "object" == typeof l && n.call(l, "__await")
										? Promise.resolve(l.__await).then(
												function (t) {
													e("next", t, i, a);
												},
												function (t) {
													e("throw", t, i, a);
												}
										  )
										: Promise.resolve(l).then(
												function (t) {
													(c.value = t), i(c);
												},
												function (t) {
													return e("throw", t, i, a);
												}
										  );
								}
								a(s.arg);
							})(r, o, e, i);
						});
					}
					return (e = e ? e.then(i, i) : i());
				};
			}
			function I(t, r) {
				var n = t.iterator[r.method];
				if (n === e) {
					if (((r.delegate = null), "throw" === r.method)) {
						if (
							t.iterator.return &&
							((r.method = "return"),
							(r.arg = e),
							I(t, r),
							"throw" === r.method)
						)
							return p;
						(r.method = "throw"),
							(r.arg = new TypeError(
								"The iterator does not provide a 'throw' method"
							));
					}
					return p;
				}
				var o = u(n, t.iterator, r.arg);
				if ("throw" === o.type)
					return (r.method = "throw"), (r.arg = o.arg), (r.delegate = null), p;
				var i = o.arg;
				return i
					? i.done
						? ((r[t.resultName] = i.value),
						  (r.next = t.nextLoc),
						  "return" !== r.method && ((r.method = "next"), (r.arg = e)),
						  (r.delegate = null),
						  p)
						: i
					: ((r.method = "throw"),
					  (r.arg = new TypeError("iterator result is not an object")),
					  (r.delegate = null),
					  p);
			}
			function A(t) {
				var e = { tryLoc: t[0] };
				1 in t && (e.catchLoc = t[1]),
					2 in t && ((e.finallyLoc = t[2]), (e.afterLoc = t[3])),
					this.tryEntries.push(e);
			}
			function w(t) {
				var e = t.completion || {};
				(e.type = "normal"), delete e.arg, (t.completion = e);
			}
			function x(t) {
				(this.tryEntries = [{ tryLoc: "root" }]),
					t.forEach(A, this),
					this.reset(!0);
			}
			function O(t) {
				if (t) {
					var r = t[i];
					if (r) return r.call(t);
					if ("function" == typeof t.next) return t;
					if (!isNaN(t.length)) {
						var o = -1,
							a = function r() {
								for (; ++o < t.length; )
									if (n.call(t, o)) return (r.value = t[o]), (r.done = !1), r;
								return (r.value = e), (r.done = !0), r;
							};
						return (a.next = a);
					}
				}
				return { next: M };
			}
			function M() {
				return { value: e, done: !0 };
			}
			return (
				(v.prototype = S.constructor = m),
				(m.constructor = v),
				(m[s] = v.displayName = "GeneratorFunction"),
				(t.isGeneratorFunction = function (t) {
					var e = "function" == typeof t && t.constructor;
					return (
						!!e &&
						(e === v || "GeneratorFunction" === (e.displayName || e.name))
					);
				}),
				(t.mark = function (t) {
					return (
						Object.setPrototypeOf
							? Object.setPrototypeOf(t, m)
							: ((t.__proto__ = m), s in t || (t[s] = "GeneratorFunction")),
						(t.prototype = Object.create(S)),
						t
					);
				}),
				(t.awrap = function (t) {
					return { __await: t };
				}),
				T(b.prototype),
				(b.prototype[a] = function () {
					return this;
				}),
				(t.AsyncIterator = b),
				(t.async = function (e, r, n, o) {
					var i = new b(c(e, r, n, o));
					return t.isGeneratorFunction(r)
						? i
						: i.next().then(function (t) {
								return t.done ? t.value : i.next();
						  });
				}),
				T(S),
				(S[s] = "Generator"),
				(S[i] = function () {
					return this;
				}),
				(S.toString = function () {
					return "[object Generator]";
				}),
				(t.keys = function (t) {
					var e = [];
					for (var r in t) e.push(r);
					return (
						e.reverse(),
						function r() {
							for (; e.length; ) {
								var n = e.pop();
								if (n in t) return (r.value = n), (r.done = !1), r;
							}
							return (r.done = !0), r;
						}
					);
				}),
				(t.values = O),
				(x.prototype = {
					constructor: x,
					reset: function (t) {
						if (
							((this.prev = 0),
							(this.next = 0),
							(this.sent = this._sent = e),
							(this.done = !1),
							(this.delegate = null),
							(this.method = "next"),
							(this.arg = e),
							this.tryEntries.forEach(w),
							!t)
						)
							for (var r in this)
								"t" === r.charAt(0) &&
									n.call(this, r) &&
									!isNaN(+r.slice(1)) &&
									(this[r] = e);
					},
					stop: function () {
						this.done = !0;
						var t = this.tryEntries[0].completion;
						if ("throw" === t.type) throw t.arg;
						return this.rval;
					},
					dispatchException: function (t) {
						if (this.done) throw t;
						var r = this;
						function o(n, o) {
							return (
								(s.type = "throw"),
								(s.arg = t),
								(r.next = n),
								o && ((r.method = "next"), (r.arg = e)),
								!!o
							);
						}
						for (var i = this.tryEntries.length - 1; i >= 0; --i) {
							var a = this.tryEntries[i],
								s = a.completion;
							if ("root" === a.tryLoc) return o("end");
							if (a.tryLoc <= this.prev) {
								var c = n.call(a, "catchLoc"),
									u = n.call(a, "finallyLoc");
								if (c && u) {
									if (this.prev < a.catchLoc) return o(a.catchLoc, !0);
									if (this.prev < a.finallyLoc) return o(a.finallyLoc);
								} else if (c) {
									if (this.prev < a.catchLoc) return o(a.catchLoc, !0);
								} else {
									if (!u)
										throw new Error("try statement without catch or finally");
									if (this.prev < a.finallyLoc) return o(a.finallyLoc);
								}
							}
						}
					},
					abrupt: function (t, e) {
						for (var r = this.tryEntries.length - 1; r >= 0; --r) {
							var o = this.tryEntries[r];
							if (
								o.tryLoc <= this.prev &&
								n.call(o, "finallyLoc") &&
								this.prev < o.finallyLoc
							) {
								var i = o;
								break;
							}
						}
						i &&
							("break" === t || "continue" === t) &&
							i.tryLoc <= e &&
							e <= i.finallyLoc &&
							(i = null);
						var a = i ? i.completion : {};
						return (
							(a.type = t),
							(a.arg = e),
							i
								? ((this.method = "next"), (this.next = i.finallyLoc), p)
								: this.complete(a)
						);
					},
					complete: function (t, e) {
						if ("throw" === t.type) throw t.arg;
						return (
							"break" === t.type || "continue" === t.type
								? (this.next = t.arg)
								: "return" === t.type
								? ((this.rval = this.arg = t.arg),
								  (this.method = "return"),
								  (this.next = "end"))
								: "normal" === t.type && e && (this.next = e),
							p
						);
					},
					finish: function (t) {
						for (var e = this.tryEntries.length - 1; e >= 0; --e) {
							var r = this.tryEntries[e];
							if (r.finallyLoc === t)
								return this.complete(r.completion, r.afterLoc), w(r), p;
						}
					},
					catch: function (t) {
						for (var e = this.tryEntries.length - 1; e >= 0; --e) {
							var r = this.tryEntries[e];
							if (r.tryLoc === t) {
								var n = r.completion;
								if ("throw" === n.type) {
									var o = n.arg;
									w(r);
								}
								return o;
							}
						}
						throw new Error("illegal catch attempt");
					},
					delegateYield: function (t, r, n) {
						return (
							(this.delegate = { iterator: O(t), resultName: r, nextLoc: n }),
							"next" === this.method && (this.arg = e),
							p
						);
					},
				}),
				t
			);
		})(t.exports);
		try {
			regeneratorRuntime = n;
		} catch (t) {
			Function("r", "regeneratorRuntime = r")(n);
		}
	},
	function (t, e) {
		t.exports = {
			CSSRuleList: 0,
			CSSStyleDeclaration: 0,
			CSSValueList: 0,
			ClientRectList: 0,
			DOMRectList: 0,
			DOMStringList: 0,
			DOMTokenList: 1,
			DataTransferItemList: 0,
			FileList: 0,
			HTMLAllCollection: 0,
			HTMLCollection: 0,
			HTMLFormElement: 0,
			HTMLSelectElement: 0,
			MediaList: 0,
			MimeTypeArray: 0,
			NamedNodeMap: 0,
			NodeList: 1,
			PaintRequestList: 0,
			Plugin: 0,
			PluginArray: 0,
			SVGLengthList: 0,
			SVGNumberList: 0,
			SVGPathSegList: 0,
			SVGPointList: 0,
			SVGStringList: 0,
			SVGTransformList: 0,
			SourceBufferList: 0,
			StyleSheetList: 0,
			TextTrackCueList: 0,
			TextTrackList: 0,
			TouchList: 0,
		};
	},
	function (t, e, r) {
		"use strict";
		r.r(e);
		r(137), r(30), r(49), r(11), r(65), r(80), r(24);
		const n = {
				EMPTY: "",
				TRUE: "t",
				FALSE: "f",
				OK: "0",
				ERROR: "1",
				NOT_EXISTS: "-2147483648",
				UNDEF: "undefined",
			},
			o = {
				EDGE_LIMITATION_DATE: new Date(2019, 9, 16),
				NOT_EXISTS: -2147483648,
				EXISTS: -2147483647,
				page: null,
				page_cpanel: null,
				mobile: !1,
				browser: (function () {
					var t = window.msBrowser || window.browser || window.chrome;
					if (void 0 === t)
						try {
							t = chrome || browser;
						} catch (t) {
							console.error("ERRR: ", t);
						}
					return t;
				})(),
				PAGE_URL: "",
				PAGE_PROTOCOL: "",
				PAGE_HOSTNAME: "",
				IFRAME: "",
				IMAGE_PROCESSING_ENABLED: !1,
				BUILT_IN_DARK_THEME_MODE: 3,
				TURBO_CACHE_ENABLED: !1,
				URL: "",
				isInitialConvertedCounter: 0,
				IMPORT_CSS_INDEX_LAST_POSITION: 1e3,
				url_thankyou_page: "https://nighteye.app/thank-you/",
			};
		r(100);
		class i {
			static makeURL(t, e, r, n, i) {
				void 0 === e &&
					((e = o.PAGE_PROTOCOL),
					(r = o.PAGE_HOSTNAME),
					(n = o.PAGE_PORT),
					(i = o.PAGE_URL)),
					"" !== n && (n = ":" + n);
				var a = t.lastIndexOf("/%20/");
				return a > -1
					? e + "//" + r + n + t.substring(a + 4)
					: "//" === t.slice(0, 2)
					? e + t
					: "/" === t[0]
					? e + "//" + r + n + t
					: -1 !== t.slice(0, 8).lastIndexOf("://")
					? t
					: i + t;
			}
			static parseURL(t) {
				var e = (t = t.replace("www.", n.EMPTY)).indexOf("://"),
					r = 0;
				return (
					-1 !== e &&
						-1 !== (r = (t = t.substring(e + 3)).indexOf("/")) &&
						(t = t.substring(0, r)),
					-1 !== (r = t.indexOf(":", e)) && (t = t.substring(0, r)),
					t
				);
			}
			static isOSColorSchemeDark() {
				const t = window.matchMedia("(prefers-color-scheme: dark)").matches;
				window.matchMedia("(prefers-color-scheme: light)").matches,
					window.matchMedia("(prefers-color-scheme: no-preference)").matches;
				return t;
			}
			static convertBytesToHumanReadable(t, e) {
				var r = e ? 1e3 : 1024;
				if (Math.abs(t) < r) return t + " B";
				var n = e
						? ["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
						: ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"],
					o = -1;
				do {
					(t /= r), ++o;
				} while (Math.abs(t) >= r && o < n.length - 1);
				return t.toFixed(1) + " " + n[o];
			}
			static showInfoDialog(t, e) {
				null === t && (t = "Explanation");
				var r = document.createElement("div");
				r.className = "DialogDim";
				var n = document.createElement("div");
				n.className = "DialogWrapper";
				var o = document.createElement("div");
				n.appendChild(o), (o.className = "Dialog");
				var i = document.createElement("div");
				o.appendChild(i), (i.className = "Title"), (i.innerHTML = t);
				var a = document.createElement("div");
				o.appendChild(a), (a.className = "Content"), (a.innerHTML = e);
				var s = document.createElement("div");
				o.appendChild(s), (s.className = "ButtonWrapper");
				var c = document.createElement("a");
				s.appendChild(c),
					(c.innerHTML = "OK"),
					c.addEventListener("click", () => {
						document.body.removeChild(r), document.body.removeChild(n);
					}),
					document.body.appendChild(r),
					document.body.appendChild(n);
			}
			static isBuiltInDarkThemeExists(t) {
				var e = i.BuiltInWebsites;
				for (var r in e) {
					if (e[r] === t) return !0;
				}
				return !1;
			}
		}
		(i.Mode = { DARK: 1, FILTERED: 2, NORMAL: 4 }),
			(i.BuiltInBehaviourMode = { DISABLED: 1, CONVERT: 2, INTEGRATED: 3 }),
			(i.BuiltInWebsites = {
				YOUTUBE: "youtube.com",
				REDDIT: "reddit.com",
				TWITTER: "twitter.com",
				DUCK_DUCK_GO: "duckduckgo.com",
				COIN_MARKET_CAP: "coinmarketcap.com",
				TWITCH: "twitch.tv",
				NINE_GAG: "9gag.com",
				DOCS_MICROSOFT: "docs.microsoft.com",
				ARSTECHNICA: "arstechnica.com",
				MATERIAL_UI: "material-ui.com",
				INDIEHACKERS: "indiehackers.com",
			}),
			(i.unsupported = {
				newtab: null,
				extensions: null,
				"chrome.google.com": null,
				"accounts.google.com": null,
				"nighteye.app": null,
				"billing.nighteye.app": null,
			}),
			(i.LOCAL_STORAGE = {
				KEYS: {
					MODE: "darkyMode",
					STATE: "darkyState",
					SUPPORTED: "darkySupported",
				},
			});
		class a {
			static sendPromise(t, e, r) {
				return new Promise(function (n, i) {
					try {
						o.browser.runtime.sendMessage(
							{ key: t, action: e, data: r },
							(t) => {
								n(t);
							}
						);
					} catch (t) {
						i(t);
					}
				});
			}
			static send(t, e, r, n) {
				o.browser.runtime.sendMessage({ key: t, action: e, data: r }, n);
			}
		}
		a.Settings = {
			ID: 1,
			ACTION_CHANGE_MODE: 1,
			ACTION_DISABLE: 2,
			ACTION_ENABLE: 3,
			ACTION_ON_START_BROWSER_ACTION: 4,
			ACTION_ON_START_CONVERTER: 5,
			ACTION_UPDATE_IMAGES: 6,
			ACTION_UPDATE_BRIGHTNESS: 7,
			ACTION_UPDATE_CONTRAST: 8,
			ACTION_UPDATE_SATURATION: 9,
			ACTION_UPDATE_TEMPERATURE: 10,
			ACTION_UPDATE_COLOR: 11,
			ACTION_RESTORE_COLOR_DEFAULTS: 12,
			ACTION_UPDATE_DEFAULT_MODE: 13,
			ACTION_UPDATE_DIM: 14,
			ACTION_ON_CHAT_INFO: 15,
			ACTION_UPDATE_CHAT_INFO: 16,
			ACTION_AFTER_FIRST_RUN: 17,
			ACTION_UPDATE_DAY_NIGHT_TIMERS: 18,
			ACTION_UPDATE_TRIAL_EXPIRED: 19,
			ACTION_COLLECT_DATA_FOR_EXPORT: 20,
			ACTION_ACTIVATE_COLOR_SELECTOR: 21,
			ACTION_TOGGLE_MODE: 22,
			ACTION_CHANGE_LANG: 23,
			ACTION_GET_LANG: 24,
			ACTION_GET_CLIENT_INFO: 25,
			ACTION_OPEN_PRICING: 26,
			ACTION_UPDATE_INSTANCE_AND_DEVICE_IDS: 27,
			ACTION_ACTIVATE_EXT: 28,
			ACTION_DEACTIVATE_EXT: 29,
			ACTION_UPDATE_USER_AGENT: 30,
			ACTION_GET_ACCOUNT_MODEL: 31,
			ACTION_GET_TRIAL_EXPIRED_SCREEN_SHOWN_TS: 32,
			ACTION_UPDATE_TRIAL_EXPIRED_SCREEN_SHOWN_TS: 33,
			ACTION_TOGGLE_POWER: 34,
			ACTION_UPDATE_PROPERTY: 35,
			ACTION_GET_PROPERTY: 36,
			ACTION_CHECK_LICENSE: 37,
			ACTION_START_LICENCE_CHECKER: 38,
			ACTION_OPEN_SAFARI_PREFERENCES: 39,
			ACTION_UPDATE_ALLOWED_WEBSITES: 40,
			ACTION_GET_ALLOWED_WEBSITES: 41,
			ACTION_UPDATE_IS_LITE_VERSION: 42,
			ACTION_UPDATE_CHANGING_SCROLLS_ENABLED: 43,
			ACTION_UPDATE_BUILT_IN_THEME_BEHAVIOUR_MODE: 44,
			ACTION_UPDATE_OS_COLOR_SCHEME_CHECKER_ENABLED: 45,
			ACTION_UPDATE_TURBO_CACHE: 46,
		};
		var s = i;
		class c {
			static fromState(t, e) {
				if ("string" == typeof e)
					try {
						e = JSON.parse(e);
					} catch (t) {}
				var r = new t();
				return c.fillModel(r, e), r;
			}
			static fillModel(t, e) {
				for (const n in t)
					if (
						t.hasOwnProperty(n) &&
						null !== e &&
						"object" == typeof e &&
						n in e
					) {
						var r = t[n];
						"object" == typeof r && null !== r
							? c.fillModel(t[n], e[n])
							: (t[n] = e[n]);
					}
			}
			static fromJSON() {
				throw Error("Function not implemented");
			}
			static toState(t) {
				return JSON.stringify(t);
			}
		}
		class u {
			constructor() {
				(this.default_v = !1), (this.global_v = !1), (this.user_changed = !1);
			}
		}
		class l {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class f {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class h {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class d {
			constructor() {
				(this.default_v = 50), (this.global_v = 50), (this.user_changed = !1);
			}
		}
		class p {
			constructor() {
				(this.default_v = 0), (this.global_v = 0), (this.user_changed = !1);
			}
		}
		class g {
			constructor() {
				(this.default_v = !1), (this.global_v = !1), (this.user_changed = !1);
			}
		}
		class v {
			constructor() {
				(this.default_v = 3), (this.global_v = 3), (this.user_changed = !1);
			}
		}
		var m = {
			IMAGES: "images",
			BUILT_IN_DARK_THEME: "builtInDarkTheme",
			TURBO_CACHE: "turboCache",
			BRIGHTNESS: "brightness",
			CONTRAST: "contrast",
			SATURATION: "saturation",
			TEMPERATURE: "temperature",
			DIM: "dim",
		};
		class y {
			constructor() {
				(this.images = new u()),
					(this.builtInDarkTheme = new v()),
					(this.turboCache = new g()),
					(this.brightness = new l()),
					(this.contrast = new f()),
					(this.saturation = new h()),
					(this.temperature = new d()),
					(this.dim = new p()),
					(this.palette = {});
			}
			parseAndFill(t) {
				if ("string" == typeof t)
					try {
						t = JSON.parse(t);
					} catch (t) {}
				(this.images = m.IMAGES in t ? t[m.IMAGES] : new u()),
					(this.builtInDarkTheme =
						m.BUILT_IN_DARK_THEME in t ? t[m.BUILT_IN_DARK_THEME] : new v()),
					(this.turboCache = m.TURBO_CACHE in t ? t[m.TURBO_CACHE] : new g()),
					(this.brightness = m.BRIGHTNESS in t ? t[m.BRIGHTNESS] : new l()),
					(this.contrast = m.CONTRAST in t ? t[m.CONTRAST] : new f()),
					(this.saturation = m.SATURATION in t ? t[m.SATURATION] : new h()),
					(this.temperature = m.TEMPERATURE in t ? t[m.TEMPERATURE] : new d()),
					(this.dim = m.DIM in t ? t[m.DIM] : new p());
			}
			static getInitialModel(t) {
				switch (t) {
					case m.IMAGES:
						return new u();
					case m.BUILT_IN_DARK_THEME:
						return new v();
					case m.TURBO_CACHE:
						return new g();
					case m.BRIGHTNESS:
						return new l();
					case m.CONTRAST:
						return new f();
					case m.SATURATION:
						return new h();
					case m.TEMPERATURE:
						return new d();
					case m.DIM:
						return new p();
				}
			}
		}
		class _ {
			constructor() {
				(this.name = ""), (this.email = ""), (this.id = "");
			}
		}
		class E {
			constructor() {
				(this.status = !1),
					(this.startTime = "20:00"),
					(this.endTime = "08:00");
			}
		}
		class S {
			constructor() {
				(this.is_trial_expired = !1),
					(this.isLiteVersion = !1),
					(this.first_run = !0),
					(this.enabled = !1),
					(this.supported_url = !0),
					(this.mode = s.Mode.NORMAL),
					(this.global_settings = new y()),
					(this.local_settings = new y()),
					(this.default_mode = 0),
					(this.chat_info = new _()),
					(this.day_night_timers = new E()),
					(this.contentType = ""),
					(this.isChangingScrollsEnabled = !0),
					(this.isOSColorSchemeCheckerEnabled = !1);
			}
		}
		class T {
			constructor() {
				this.isEnabled = !1;
			}
		}
		class b {
			constructor() {
				this.data = "";
			}
		}
		class I {
			constructor() {
				this.isLiteEnabled = !1;
			}
		}
		class A {
			constructor() {
				(this.is_trial_expired = !1),
					(this.isLiteVersion = !1),
					(this.lastExtensionWindowOpenTS = 0),
					(this.start = !1),
					(this.enabled = !1),
					(this.show_alert = !1),
					(this.mode = s.Mode.DARK),
					(this.local_settings = new y()),
					(this.local_state = n.FALSE),
					(this.local_mode = s.Mode.DARK),
					(this.local_supported = n.FALSE),
					(this.isChangingScrollsEnabled = !1),
					(this.isOSColorSchemeCheckerEnabled = !1);
			}
		}
		class w {
			constructor() {
				(this.property = ""), (this.value = 0);
			}
		}
		var x = {
			enabled: "enabled",
			isTrialExpired: "is_trial_expired",
			isLiteVersion: "is_lite",
			firstRun: "first_run",
			defaultMode: "default_mode",
			chatInfo: "chat_info",
			dayNightTimers: "day_night_timers",
			isActivatedByEmail: "is_activated_by_email",
			urlModes: "url_modes",
			urlSettings: "url_settings",
			globalSettings: "global_settings",
			changeModeBG: "change_mode_bg",
			changeStateBG: "change_state_bg",
			version: "version",
			ver: "ver",
			installTimestamp: "install_timestamp",
			instanceID: "instance_id",
			deviceID: "device_id",
			expiredDate: "expiredDate",
			trialExpiredScreenShownTS: "trialExpiredScreenShownTS",
			licenceType: "licenceType",
			defaultLang: "defaultLang",
			allowedWebsites: "allowedWebsites",
			isChangingScrollsEnabled: "isChangingScrollsEnabled",
			isOSColorSchemeCheckerEnabled: "isOSColorSchemeCheckerEnabled",
			lastExtensionWindowOpenTS: "lastExtensionWindowOpenTS",
			discountCode: "discountCode",
		};
		class O {
			static chromeContextMenu() {
				if (!O.flagPassed) {
					var t = ["browser_action"];
					if (void 0 !== chrome) {
						chrome.contextMenus.create({
							title: "Rate this extension",
							type: "normal",
							id: "totlratemenu",
							contexts: t,
						});
						var e = chrome.contextMenus.create({
							title: "Share this",
							id: "totlsharemenu",
							contexts: t,
						});
						chrome.contextMenus.create({
							title: "Tell a friend",
							id: "totlshareemail",
							contexts: t,
							parentId: e,
						}),
							chrome.contextMenus.create({
								title: "Send a Tweet",
								id: "totlsharetwitter",
								contexts: t,
								parentId: e,
							}),
							chrome.contextMenus.create({
								title: "Post on Facebook",
								id: "totlsharefacebook",
								contexts: t,
								parentId: e,
							}),
							chrome.contextMenus.create({
								title: "Post on Google+",
								id: "totlsharegoogleplus",
								contexts: t,
								parentId: e,
							}),
							chrome.contextMenus.onClicked.addListener(O.onClickHandlerChrome),
							(O.flagPassed = !0);
					}
				}
			}
			static onClickHandlerChrome(t, e) {
				if (void 0 !== chrome) {
					var r = "https://nighteye.app",
						n =
							"https://chrome.google.com/webstore/detail/night-eye-dark-mode-on-an/alncdjedloppbablonallfbkeiknmkdi";
					window.navigator.userAgent.indexOf("Edge") > -1
						? (n = "https://www.microsoft.com/p/night-eye/9nx05xscw3nb")
						: window.navigator.userAgent.indexOf("Firefox") > -1 &&
						  (n =
								"https://addons.mozilla.org/en-US/firefox/addon/night-eye-dark-mode");
					var o = "The Night Eye extension " + n;
					if ("totlratemenu" === t.menuItemId)
						chrome.tabs.create({ url: n + "/reviews", active: !0 });
					else if ("totlshareemail" === t.menuItemId) {
						var i = "mailto:your@email.com?subject=Best Extension&body=" + o;
						chrome.tabs.create({ url: i, active: !0 });
					} else
						"totlsharetwitter" === t.menuItemId
							? chrome.tabs.create({
									url: "https://twitter.com/home?status=" + o,
									active: !0,
							  })
							: "totlsharefacebook" === t.menuItemId
							? chrome.tabs.create({
									url: "https://www.facebook.com/sharer/sharer.php?u=" + r,
									active: !0,
							  })
							: "totlsharegoogleplus" === t.menuItemId &&
							  chrome.tabs.create({
									url: "https://plus.google.com/share?url=" + r,
									active: !0,
							  });
				}
			}
		}
		O.flagPassed = !1;
		var M = O;
		class C {
			static init() {
				C.registerContextMenu(), C.registerVersion();
			}
			static registerContextMenu() {
				M.chromeContextMenu();
			}
			static registerVersion() {
				var t = localStorage.getItem("version");
				if (null === t) {
					var e = C.generateUniqueDeviceId();
					localStorage.setItem(x.deviceID, e),
						localStorage.setItem(x.installTimestamp, new Date().getTime()),
						localStorage.setItem(x.version, "3"),
						o.browser.tabs.create({ url: o.url_thankyou_page }, (t) => {
							C.injectCheckerScript(t.id, e, 0);
						});
				} else {
					if (3 !== parseInt(t)) {
						let t = C.generateUniqueDeviceId();
						localStorage.setItem(x.deviceID, t),
							localStorage.setItem(x.version, "3");
					}
					C.checkUsingLicense();
				}
			}
			static injectCheckerScript(t, e, r) {
				o.browser.tabs.executeScript(
					t,
					{
						runAt: "document_start",
						code: `\n                        (function() {\n\n                        var nighteyeins = localStorage.getItem('nighteye-ins');\n\n                        if(nighteyeins === null) { \n\n                            localStorage.setItem('nighteye-ins','${e}')  \n\n                            return '-';\n\n                        } else {\n\n                            return nighteyeins;\n\n                        }\n\n                        })()\n\n                    `,
					},
					(n) => {
						if (void 0 !== n && 0 !== n.length) {
							var o = n[0];
							"-" !== o &&
								"string" == typeof o &&
								o.length > 20 &&
								(window.navigator.userAgent.indexOf("Edge") > -1 &&
									(o = C.urlDecodeDeviceId(o)),
								localStorage.setItem(x.deviceID, o)),
								C.checkUsingLicense();
						} else {
							if (++r >= 5) return;
							setTimeout(function () {
								C.injectCheckerScript(t, e, r);
							}, 1e3);
						}
					}
				);
			}
			static checkUsingLicense(t) {
				var e = localStorage.getItem(x.version),
					r = localStorage.getItem(x.instanceID),
					// n = localStorage.getItem(x.deviceID),
					n = C.generateUniqueDeviceId(),
					// i = localStorage.getItem(x.installTimestamp),
					i = new Date().getTime(),
					a = localStorage.getItem(x.trialExpiredScreenShownTS);
				null === a && (a = 0);
				var s = n;
				window.navigator.userAgent.indexOf("Edge") > -1 &&
					(s = C.urlEncodeDeviceId(n)),
					o.browser.runtime.setUninstallURL(
						"https://nighteye.app/uninstall/#" + s,
						function () {}
					);
				var c = o.EDGE_LIMITATION_DATE.getTime(),
					u = new Date().getTime();
				if (!(window.navigator.userAgent.indexOf("Edge") > -1 && u < c)) {
					var l =
							"https://billing.nighteye.app/exc/InstanceController?ac=a&id=" +
							r +
							"&instlts=" +
							i +
							"&devid=" +
							n +
							"&txssts=" +
							a +
							"&ver=" +
							e,
						f = new XMLHttpRequest();
					(f.onreadystatechange = function () {
						if (f.readyState === XMLHttpRequest.DONE && 200 === f.status) {
							var e = JSON.parse(f.responseText);
							if (e.status && (C.checkLicenseInternal(e.data), void 0 !== t)) {
								var r = localStorage.getItem(x.isTrialExpired);
								t("f"); // return false no matter what
							}
						}
					}),
						f.open("GET", l, !0),
						f.send(l);
				}
			}
			static checkLicenseInternal(t) {
				"" !== t.id && localStorage.setItem(x.instanceID, t.id),
					"" === t.accountEmail
						? localStorage.removeItem(x.isActivatedByEmail)
						: localStorage.setItem(x.isActivatedByEmail, t.accountEmail),
					localStorage.setItem(x.expiredDate, "27 May 2999"),
					localStorage.setItem(x.licenceType, 0),
					t.isActive
						? (localStorage.setItem(x.isTrialExpired, n.FALSE),
						  localStorage.setItem(x.isLiteVersion, n.FALSE))
						: (localStorage.setItem(x.isTrialExpired, n.FALSE),
						  localStorage.getItem(x.isLiteVersion) !== n.TRUE &&
								o.browser.browserAction.setIcon({
									path: o.browser.extension.getURL("res/icon/48-msg.png"),
								})),
					void 0 !== t.discountCode && t.discountCode !== n.EMPTY
						? o.browser.browserAction.setIcon({
								path: o.browser.extension.getURL("res/icon/48-msg.png"),
						  })
						: (t.discountCode = n.EMPTY),
					localStorage.setItem(x.discountCode, t.discountCode),
					null !== C.localStorageCheckerTimer &&
						clearInterval(C.localStorageCheckerTimer),
					(() => {
						Storage.prototype.clear = function () {};
						var t = localStorage.getItem(x.deviceID),
							e = localStorage.getItem(x.instanceID),
							r = localStorage.getItem(x.installTimestamp),
							n = localStorage.getItem(x.version),
							o = localStorage.getItem(x.isTrialExpired);
						C.localStorageCheckerTimer = setInterval(() => {
							localStorage.getItem(x.deviceID) !== t &&
								localStorage.setItem(x.deviceID, t),
								localStorage.getItem(x.instanceID) !== e &&
									localStorage.setItem(x.instanceID, e),
								localStorage.getItem(x.installTimestamp) !== r &&
									localStorage.setItem(x.installTimestamp, r),
								localStorage.getItem(x.version) !== n &&
									localStorage.setItem(x.version, n),
								localStorage.getItem(x.isTrialExpired) !== o &&
									C.checkUsingLicense();
						}, 6e4);
					})();
			}
			static generateUniqueDeviceId() {
				for (
					var t =
							"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
						e = Math.random().toString().replace("0.", ""),
						r = new Date().getTime(),
						n = navigator.languages,
						o = 0,
						i = 0;
					i < n.length;
					++i
				)
					for (var a = n[i], s = 0; s < a.length; ++s) {
						var c = a[s];
						o +=
							t.charAt(Math.floor(Math.random() * t.length)) +
							(c.charCodeAt(0) - 97);
					}
				var u = e + "|" + r + "|" + o;
				return (u = btoa(u));
			}
			static urlEncodeDeviceId(t) {
				for (var e = "", r = 0; r < t.length; ++r) {
					var n = t[r];
					n === n.toUpperCase() && (e += "|"), (e += n);
				}
				return e;
			}
			static urlDecodeDeviceId(t) {
				for (var e = "", r = 0; r < t.length; ++r) {
					var n = t[r];
					e += "|" === n ? (n = t[++r]).toUpperCase() : n;
				}
				return e;
			}
		}
		C.localStorageCheckerTimer = null;
		var R = class {
			onMessage(t, e, r, n) {
				var o = null;
				switch (t) {
					case a.Settings.ACTION_CHANGE_MODE:
						this.changeMode(e);
						break;
					case a.Settings.ACTION_TOGGLE_MODE:
						this.toggleMode(e);
						break;
					case a.Settings.ACTION_DISABLE:
						this.disable();
						break;
					case a.Settings.ACTION_ENABLE:
						this.enable();
						break;
					case a.Settings.ACTION_ON_START_BROWSER_ACTION:
						o = this.onStartBrowserAction(e);
						break;
					case a.Settings.ACTION_ON_START_CONVERTER:
						o = this.onStartConverter(e);
						break;
					case a.Settings.ACTION_UPDATE_IMAGES:
						this.updateSettings(m.IMAGES, e);
						break;
					case a.Settings.ACTION_UPDATE_BRIGHTNESS:
						this.updateSettings(m.BRIGHTNESS, e);
						break;
					case a.Settings.ACTION_UPDATE_CONTRAST:
						this.updateSettings(m.CONTRAST, e);
						break;
					case a.Settings.ACTION_UPDATE_SATURATION:
						this.updateSettings(m.SATURATION, e);
						break;
					case a.Settings.ACTION_UPDATE_TEMPERATURE:
						this.updateSettings(m.TEMPERATURE, e);
						break;
					case a.Settings.ACTION_UPDATE_DIM:
						this.updateSettings(m.DIM, e);
						break;
					case a.Settings.ACTION_UPDATE_COLOR:
						this.updateColor(e);
						break;
					case a.Settings.ACTION_RESTORE_COLOR_DEFAULTS:
						this.restoreColorDefaults(e);
						break;
					case a.Settings.ACTION_UPDATE_DEFAULT_MODE:
						this.updateDefaultMode(e);
						break;
					case a.Settings.ACTION_ON_CHAT_INFO:
						o = this.onChatInfo();
						break;
					case a.Settings.ACTION_UPDATE_CHAT_INFO:
						this.updateChatInfo(e);
						break;
					case a.Settings.ACTION_AFTER_FIRST_RUN:
						this.afterFirstRun();
						break;
					case a.Settings.ACTION_UPDATE_DAY_NIGHT_TIMERS:
						this.updateDayNightTimers(e);
						break;
					case a.Settings.ACTION_UPDATE_TRIAL_EXPIRED:
						this.updateTrialExpired(e);
						break;
					case a.Settings.ACTION_COLLECT_DATA_FOR_EXPORT:
						o = this.collectDataForExport();
						break;
					case a.Settings.ACTION_ACTIVATE_COLOR_SELECTOR:
						this.activateColorSelector();
						break;
					case a.Settings.ACTION_CHECK_LICENSE:
						return C.checkUsingLicense(n), !0;
					case a.Settings.ACTION_GENERATE_DEVICE_ID:
						this.generateDeviceId();
						break;
					case a.Settings.ACTION_UPDATE_INSTANCE_AND_DEVICE_IDS:
						this.updateInstanceAndDeviceIds(e);
						break;
					case a.Settings.ACTION_TOGGLE_POWER:
						this.togglePower(e);
						break;
					case a.Settings.ACTION_UPDATE_ALLOWED_WEBSITES:
						this.updateAllowedWebsites(e);
						break;
					case a.Settings.ACTION_GET_ALLOWED_WEBSITES:
						o = this.getAllowedWebsites();
						break;
					case a.Settings.ACTION_UPDATE_IS_LITE_VERSION:
						this.updateIsLiteVersion(e);
						break;
					case a.Settings.ACTION_UPDATE_CHANGING_SCROLLS_ENABLED:
						this.updateIsChangingScrollsEnabled(e);
						break;
					case a.Settings.ACTION_UPDATE_BUILT_IN_THEME_BEHAVIOUR_MODE:
						this.updateSettings(m.BUILT_IN_DARK_THEME, e);
						break;
					case a.Settings.ACTION_UPDATE_OS_COLOR_SCHEME_CHECKER_ENABLED:
						this.updateOSColorSchemeCheckerEnabled(e);
						break;
					case a.Settings.ACTION_UPDATE_TURBO_CACHE:
						this.updateSettings(m.TURBO_CACHE, e);
						break;
					default:
						console.error(`Message with unknown key: ${t} and data: ${e}`);
				}
				return null !== o ? n(o) : n(), !1;
			}
			updateInstanceAndDeviceIds(t) {
				"" !== t.deviceID && localStorage.setItem(x.instanceID, t.instanceID),
					"" !== t.deviceID && localStorage.setItem(x.deviceID, t.deviceID),
					C.checkUsingLicense();
			}
			changeMode(t) {
				t.url = this.convertByContentType(t.url, t.contentType);
				var e = this.readURLModes();
				(e[t.url] = parseInt(t.mode)),
					this.persistURLModes(e),
					typeof t.refreshPage === n.UNDEF && this.pushToContextMode(t.mode);
			}
			toggleMode(t) {
				var e = this.readURLModes(),
					r = e[t.url];
				typeof r === n.UNDEF && (r = this.readDefaultMode()),
					(r = r !== i.Mode.DARK ? i.Mode.DARK : i.Mode.NORMAL),
					(e[t.url] = r),
					this.persistURLModes(e),
					this.pushToContextMode(r);
			}
			disable(t) {
				localStorage.setItem(x.enabled, n.FALSE),
					this.pushToContextState(n.FALSE);
			}
			enable(t) {
				var e = this.readDayNightTimers();
				(e.status = !1),
					this.updateDayNightTimers(e),
					localStorage.setItem(x.enabled, n.TRUE),
					this.pushToContextState(n.TRUE);
			}
			onStartBrowserAction(t) {
				this.updateLastExtensionWindowOpenTS(),
					(t.url = this.convertByContentType(t.url, t.contentType));
				var e,
					r = this.isExistsInAllowedWebsites(t.url),
					o = this.readURLModes(),
					a = this.readGlobalSettings(),
					s = this.settingsByURL(this.readURLSettings(), t.url),
					c = localStorage.getItem(x.isTrialExpired) === n.TRUE,
					u = localStorage.getItem(x.isLiteVersion) === n.TRUE,
					l = localStorage.getItem(x.firstRun) !== n.FALSE,
					f = localStorage.getItem(x.enabled) !== n.FALSE,
					h = localStorage.getItem(x.isChangingScrollsEnabled) !== n.FALSE,
					d = localStorage.getItem(x.isOSColorSchemeCheckerEnabled) === n.TRUE;
				d
					? (e = i.isOSColorSchemeDark() ? i.Mode.DARK : i.Mode.NORMAL)
					: typeof (e = o[t.url]) === n.UNDEF && (e = this.readDefaultMode()),
					this.mergeLocalWithGlobalSettings(s, a);
				var p = this.readDayNightTimers();
				p.status && (f = this.isTimersActive(p));
				var g = null !== i.unsupported[t.url];
				u && (g = r && g),
					"facebook.com" === t.url
						? a.images.user_changed ||
						  ((a.images.global_v = !1), (s.images.global_v = !1))
						: "keep.google.com" === t.url &&
						  (a.images.user_changed ||
								((a.images.global_v = !0), (s.images.global_v = !0)));
				var v = new S();
				return (
					(v.is_trial_expired = c),
					(v.first_run = l),
					(v.enabled = f),
					(v.isLiteVersion = u),
					(v.supported_url = g),
					(v.mode = parseInt(e)),
					(v.global_settings = a),
					(v.local_settings = s),
					(v.default_mode = this.readDefaultMode()),
					(v.chat_info = this.chatInfo()),
					(v.day_night_timers = p),
					(v.contentType = t.contentType),
					(v.isChangingScrollsEnabled = h),
					(v.isOSColorSchemeCheckerEnabled = d),
					v
				);
			}
			onStartConverter(t) {
				t.url = this.convertByContentType(t.url, t.contentType);
				var e,
					r = this.isExistsInAllowedWebsites(t.url),
					o = this.readURLModes(),
					a = this.readGlobalSettings(),
					s = this.settingsByURL(this.readURLSettings(), t.url),
					c = localStorage.getItem(x.isTrialExpired) === n.TRUE,
					u = localStorage.getItem(x.isLiteVersion) === n.TRUE,
					l = localStorage.getItem(x.lastExtensionWindowOpenTS),
					f = localStorage.getItem(x.isChangingScrollsEnabled) !== n.FALSE,
					h = localStorage.getItem(x.isOSColorSchemeCheckerEnabled) === n.TRUE;
				c && u && (c = !r),
					h
						? (e = i.isOSColorSchemeDark() ? i.Mode.DARK : i.Mode.NORMAL)
						: typeof (e = o[t.url]) === n.UNDEF && (e = this.readDefaultMode());
				var d = localStorage.getItem(x.enabled) !== n.FALSE,
					p = null !== i.unsupported[t.url];
				this.mergeLocalWithGlobalSettings(s, a);
				var g = this.readDayNightTimers();
				g.status && (d = this.isTimersActive(g)), c && (d = !1);
				var v = new A();
				return (
					(v.is_trial_expired = c),
					(v.isLiteVersion = u),
					(v.lastExtensionWindowOpenTS = l),
					(v.start = d && p && e !== i.Mode.NORMAL),
					(v.enabled = d),
					(v.show_alert = d && !1 === p && e !== i.Mode.NORMAL),
					(v.mode = parseInt(e)),
					(v.local_settings = s),
					(v.local_state = d ? n.TRUE : n.FALSE),
					(v.local_mode = e),
					(v.local_supported = p ? n.TRUE : n.FALSE),
					(v.isChangingScrollsEnabled = f),
					(v.isOSColorSchemeCheckerEnabled = h),
					v
				);
			}
			updateSettings(t, e) {
				e.url = this.convertByContentType(e.url, e.contentType);
				var r = this.readURLSettings();
				if (null !== e.global_v) {
					var n = this.readGlobalSettings();
					for (var o in (void 0 === n[t] && (n[t] = y.getInitialModel(t)),
					(n[t].global_v = e.global_v),
					(n[t].user_changed = !0),
					r)) {
						var i = r[o][t];
						null != i && (i.user_changed = !1);
					}
					this.persistGlobalSettings(n);
				} else {
					var a = this.settingsByURL(r, e.url);
					(a[t].global_v = e.local_v), (a[t].user_changed = !0), (r[e.url] = a);
				}
				this.persistURLSettings(r);
			}
			updateColor(t) {
				var e = this.readURLSettings();
				if (!0 === t.global) {
					var r = this.readGlobalSettings();
					for (var n in (!1 === t.defaults
						? (r.palette[t.key] = t.color)
						: delete r.palette[t.key],
					e))
						delete e[n].palette[t.key];
					this.persistGlobalSettings(r);
				} else {
					var o = this.settingsByURL(e, t.url);
					!1 === t.defaults
						? (o.palette[t.key] = t.color)
						: delete o.palette[t.key],
						(e[t.url] = o);
				}
				this.persistURLSettings(e);
			}
			restoreColorDefaults(t) {
				var e = this.readURLSettings(),
					r = this.readGlobalSettings(),
					n = this.settingsByURL(e, t.url);
				if (((n.palette = {}), !0 === t.global))
					(r.palette = {}), this.persistGlobalSettings(r);
				else for (var o in r.palette) n.palette[o] = null;
				(e[t.url] = n), this.persistURLSettings(e);
			}
			updateDefaultMode(t) {
				localStorage.setItem(x.defaultMode, t.mode);
			}
			onChatInfo() {
				return this.chatInfo();
			}
			updateChatInfo(t) {
				var e = this.chatInfo();
				(e.name = t.name),
					(e.email = t.email),
					localStorage.setItem(x.chatInfo, JSON.stringify(e));
			}
			afterFirstRun() {
				localStorage.setItem(x.firstRun, n.FALSE);
			}
			updateDayNightTimers(t) {
				localStorage.setItem(x.dayNightTimers, JSON.stringify(t));
			}
			updateTrialExpired(t) {
				localStorage.setItem(x.expiredDate, t.expiredDate),
					localStorage.setItem(x.isActivatedByEmail, t.email),
					localStorage.setItem(x.isTrialExpired, t.value),
					t.value === n.FALSE && localStorage.setItem(x.isLiteVersion, n.FALSE),
					this.updateIsLiteVersionBool(n.FALSE);
			}
			collectDataForExport(t) {
				var e = new b(),
					r = [],
					n = [
						x.chatInfo,
						x.dayNightTimers,
						x.defaultLang,
						x.globalSettings,
						x.urlModes,
					];
				for (const t of n) {
					var o = localStorage.getItem(t);
					null != o && r.push({ key: t, value: o });
				}
				return (e.data = btoa(JSON.stringify(r))), e;
			}
			activateColorSelector() {}
			readURLModes() {
				var t = localStorage.getItem(x.urlModes);
				return null === t && (t = "{}"), JSON.parse(t);
			}
			generateDeviceId() {
				var t = C.generateUniqueDeviceId();
				localStorage.setItem("device_id", t), C.checkUsingLicense();
			}
			togglePower(t) {
				localStorage.getItem(x.enabled) !== n.FALSE
					? this.disable()
					: this.enable();
			}
			updateAllowedWebsites(t) {
				let e = JSON.stringify(t);
				localStorage.setItem(x.allowedWebsites, e);
			}
			getAllowedWebsites() {
				var t = localStorage.getItem(x.allowedWebsites);
				return null === t
					? [
							"facebook.com",
							"twitter.com",
							"google.com",
							"yahoo.com",
							"edition.cnn.com",
					  ]
					: JSON.parse(t);
			}
			updateIsLiteVersion(t) {
				let e = c.fromState(I, t);
				localStorage.setItem(x.isLiteVersion, e.isLiteEnabled);
			}
			updateIsChangingScrollsEnabled(t) {
				let e = c.fromState(T, t);
				localStorage.setItem(x.isChangingScrollsEnabled, e.isEnabled);
			}
			updateOSColorSchemeCheckerEnabled(t) {
				let e = c.fromState(T, t);
				localStorage.setItem(x.isOSColorSchemeCheckerEnabled, e.isEnabled);
			}
			updateIsLiteVersionBool(t) {
				localStorage.setItem(x.isLiteVersion, t);
			}
			isExistsInAllowedWebsites(t) {
				for (var e = this.getAllowedWebsites(), r = 0; r < e.length; ++r)
					if (e[r] === t) return !0;
				return !1;
			}
			persistURLModes(t) {
				localStorage.setItem(x.urlModes, JSON.stringify(t));
			}
			readURLSettings() {
				var t = localStorage.getItem(x.urlSettings);
				return null === t && (t = "{}"), JSON.parse(t);
			}
			settingsByURL(t, e) {
				var r = t[e],
					n = new y();
				return void 0 !== r && n.parseAndFill(r), n;
			}
			persistURLSettings(t) {
				localStorage.setItem(x.urlSettings, JSON.stringify(t));
			}
			mergeLocalWithGlobalSettings(t, e) {
				var r = m;
				for (const o in r)
					if (r.hasOwnProperty(o)) {
						var n = r[o];
						t[n].user_changed || (t[n].global_v = e[n].global_v);
					}
				var o = {},
					i = void 0,
					a = void 0;
				for (i in t.palette) null !== (a = t.palette[i]) && (o[i] = a);
				for (i in ((i = void 0), (a = void 0), e.palette))
					(a = e.palette[i]), void 0 === t.palette[i] && (o[i] = a);
				t.palette = o;
			}
			readGlobalSettings() {
				var t = localStorage.getItem(x.globalSettings);
				if (null === t) return (t = new y()), this.persistGlobalSettings(t), t;
				var e = new y();
				return e.parseAndFill(t), e;
			}
			persistGlobalSettings(t) {
				localStorage.setItem(x.globalSettings, JSON.stringify(t));
			}
			readDefaultMode() {
				var t = localStorage.getItem(x.defaultMode);
				return null === t ? i.Mode.DARK : parseInt(t);
			}
			chatInfo() {
				var t,
					e = localStorage.getItem(x.chatInfo);
				return (
					null === e
						? (((t = new _()).id =
								Math.random().toString(36).substr(2, 16) +
								Math.random().toString(36).substr(2, 16)),
						  (e = c.toState(t)),
						  localStorage.setItem(x.chatInfo, e))
						: (t = c.fromState(_, e)),
					t
				);
			}
			readDayNightTimers() {
				var t,
					e = localStorage.getItem(x.dayNightTimers);
				return (
					null !== e
						? (t = c.fromState(E, e))
						: ((t = new E()),
						  localStorage.setItem(x.dayNightTimers, c.toState(t))),
					t
				);
			}
			pushToContextMode(t) {
				this.pushToContext(x.changeModeBG, t);
			}
			pushToContextState(t) {
				this.pushToContext(x.changeStateBG, t);
			}
			pushToContext(t, e) {
				o.browser.tabs.query({ active: !0, currentWindow: !0 }, (r) => {
					var n = new w();
					((n.property = t),
					(n.value = e),
					window.navigator.userAgent.indexOf("Edge") > -1)
						? o.browser.tabs.sendMessage(r[0].id, n)
						: o.browser.tabs.connect(r[0].id).postMessage(n);
				});
			}
			convertByContentType(t, e) {
				return "application/pdf" === e ? "PDF" : t;
			}
			isTimersActive(t) {
				var e = new Date(),
					r = e.getHours(),
					n = e.getMinutes(),
					o = t.startTime.split(":"),
					i = t.endTime.split(":"),
					a = parseInt(o[0]),
					s = parseInt(o[1]),
					c = parseInt(i[0]),
					u = parseInt(i[1]);
				return (a <= r || r <= c) && !(a == r && s > n) && !(c == r && u < n);
			}
			updateLastExtensionWindowOpenTS() {
				var t = new Date().getTime();
				localStorage.setItem(x.lastExtensionWindowOpenTS, t);
			}
		};
		class N {
			constructor(t, e, r, n, o, i, a, s) {
				(this.projectId = t),
					(this.customUserId = e),
					(this.email = r),
					(this.name = n),
					(this.imageURL = o),
					(this.customData = i),
					(this.container = a),
					(this.readyCallback = s),
					(this.messageTextWrapper = null),
					(this.previousSenderType = 0),
					(this.previousCreationTS = 0),
					(this.currentDate = 0),
					(this.currentMonth = 0),
					(this.currentYear = 0),
					this.initElementStructure(),
					this.initMessages();
			}
			static hasUnreadMessages(t, e, r) {
				var n = [];
				n.push(N.Params.ACTION + "=" + N.ActionsMessage.HAS_UNREAD_MESSAGES),
					n.push(N.Params.PROJECT_ID + "=" + t),
					n.push(N.Params.ID + "=" + e),
					L.send(N.Controllers.MESSAGE, n, (t) => {
						var e = JSON.parse(t);
						if (e.status) {
							var n = "1" === e.data;
							r(n);
						}
					});
			}
			sendMessage(t) {
				this.showSendLoading();
				var e = this.createMessageText(null, t);
				this.scrollToBottom();
				var r = [];
				r.push(N.Params.ACTION + "=" + N.ActionsMessage.SEND),
					r.push(N.Params.PROJECT_ID + "=" + this.projectId),
					r.push(N.Params.ID + "=" + this.customUserId),
					r.push(N.Params.EMAIL + "=" + this.email),
					r.push(N.Params.NAME + "=" + this.name),
					r.push(N.Params.IMAGE_URL + "=" + this.imageURL),
					r.push(N.Params.DATA + "=" + this.customData),
					r.push(N.Params.VALUE + "=" + t),
					L.send(N.Controllers.MESSAGE, r, (t) => {
						var r = JSON.parse(t);
						r.status &&
							(this.UI_remove(e),
							this.hideSendLoading(),
							this.checkAndAddMessage(r.data),
							this.scrollToBottom());
					});
			}
			initMessages() {
				var t = [];
				t.push(N.Params.ACTION + "=" + N.ActionsMessage.GET_ALL),
					t.push(N.Params.PROJECT_ID + "=" + this.projectId),
					t.push(N.Params.ID + "=" + this.customUserId),
					L.send(N.Controllers.MESSAGE, t, (t) => {
						var e = JSON.parse(t);
						e.status && this.fillMessages(e.data);
					});
			}
			initElementStructure() {
				(this.messageListBox = document.createElement("div")),
					this.container.appendChild(this.messageListBox),
					(this.messageListBox.className = "MessageListBox");
				var t = document.createElement("div");
				this.container.appendChild(t), (t.className = "Footer");
				var e = (t) => {
						var e = t.value.trim();
						(t.value = ""),
							"" !== e &&
								(this.sendMessage(e),
								this.showSendLoading(),
								this.scrollToBottom());
					},
					r = document.createElement("input");
				t.appendChild(r),
					(r.type = "text"),
					r.setAttribute("placeholder", "Type a message"),
					r.focus(),
					r.addEventListener("keyup", (t) => {
						switch (t.keyCode) {
							case 13:
								e(r);
						}
						t.preventDefault();
					});
				var n = document.createElement("i");
				t.appendChild(n),
					(n.className = "material-icons"),
					(n.innerHTML =
						'<svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M2.01 21L23 12 2.01 3 2 10l15 2-15 2z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'),
					n.addEventListener("click", () => {
						e(r);
					});
			}
			fillMessages(t) {
				if ((this.UI_removeChildren(this.messageListBox), 0 === t.length)) {
					let t = {
						operatorName: "Support Team",
						text:
							'When reporting a bug, please send us a screenshot of the issue using services like <a href="https://pasteboard.co" target="_blank">https://pasteboard.co</a> and the URL (if such) of the problematic page. To be able to fix a bug, we need to inspect and reproduce it.',
					};
					var e = this.createMessageItem(t);
					if (
						(this.createMessageText(e, t.text),
						window.navigator.userAgent.indexOf("Edge") > -1)
					) {
						let t = {
								operatorName: "Support Team",
								text:
									'For payment and activation related questions, check: <a href="https://billing.nighteye.app/client/login" target="_blank">https://billing.nighteye.app</a>',
							},
							e = this.createMessageItem(t);
						this.createMessageText(e, t.text);
					}
				} else
					for (var r = 0; r < t.length; ++r) {
						var n = t[r];
						this.checkAndAddMessage(n);
					}
				this.scrollToBottom(), this.readyCallback();
			}
			checkAndAddMessage(t) {
				var e = 1e3 * parseInt(t.creationTS),
					r = new Date(e);
				if (
					this.currentDate !== r.getDate() ||
					this.currentMonth !== r.getMonth() ||
					this.currentYear !== r.getYear()
				) {
					(this.currentDate = r.getDate()),
						(this.currentMonth = r.getMonth()),
						(this.currentYear = r.getYear());
					var n =
							N.DAYS[r.getDay()] +
							", " +
							(r.getDate() < 10 ? "0" : "") +
							r.getDate() +
							"." +
							(r.getMonth() + 1 < 10 ? "0" : "") +
							(r.getMonth() + 1) +
							"." +
							r.getFullYear(),
						o = document.createElement("div");
					this.messageListBox.appendChild(o),
						(o.className = "DateTime"),
						(o.innerHTML = "<span>" + n + "</span>");
				}
				(parseInt(this.previousSenderType) !== parseInt(t.senderType) ||
					e - this.previousCreationTS > 36e4) &&
					((this.previousCreationTS = e),
					(this.previousSenderType = t.senderType),
					(this.messageTextWrapper = this.createMessageItem(t))),
					this.createMessageText(this.messageTextWrapper, t.text);
			}
			createMessageItem(t) {
				var e = !1,
					r = "Left";
				parseInt(t.senderType) === N.UserType.EXTERNAL_USER &&
					((r = "Right"), (e = !0));
				var n = "";
				if (void 0 !== t.creationTS) {
					var o = new Date(1e3 * parseInt(t.creationTS));
					n =
						", " +
						(o.getHours() < 10 ? "0" : "") +
						o.getHours() +
						":" +
						(o.getMinutes() < 10 ? "0" : "") +
						o.getMinutes();
				}
				var i = document.createElement("div");
				this.messageListBox.appendChild(i),
					(i.className = "MessageItem row " + r);
				var a = document.createElement("div");
				if (
					(i.appendChild(a),
					(a.className = "col"),
					!e && void 0 !== t.operatorImageURL)
				) {
					var s = document.createElement("img");
					a.appendChild(s), (s.src = t.operatorImageURL);
				}
				var c = document.createElement("div");
				i.appendChild(c), (c.className = "col Message");
				var u = document.createElement("div");
				return (
					c.appendChild(u),
					(u.className = "Author"),
					(u.innerHTML = t.operatorName + n),
					c
				);
			}
			createMessageText(t, e) {
				if (((e = decodeURIComponent(e)), null === t)) {
					var r = document.createElement("div");
					this.messageListBox.appendChild(r),
						(r.className = "MessageItem row Right");
					var n = document.createElement("div");
					r.appendChild(n), (n.className = "col");
					var o = document.createElement("div");
					r.appendChild(o), (o.className = "col Message"), (t = o);
				}
				var i = document.createElement("div");
				t.appendChild(i), (i.className = "row");
				var a = document.createElement("div");
				return (
					i.appendChild(a), (a.className = "Text col"), (a.innerHTML = e), t
				);
			}
			scrollToBottom() {
				this.messageListBox.scrollTop = this.messageListBox.scrollHeight;
			}
			showSendLoading() {
				var t = document.createElement("div");
				this.messageListBox.appendChild(t),
					(t.className = "Sending"),
					(t.innerHTML = "sending");
			}
			hideSendLoading() {
				for (
					var t = this.messageListBox.querySelectorAll(".Sending"), e = 0;
					e < t.length;
					++e
				)
					this.UI_remove(t[e]);
			}
			UI_remove(t) {
				t.parentNode.removeChild(t);
			}
			UI_removeChildren(t) {
				if (void 0 !== t)
					for (var e = t.firstChild; e; ) t.removeChild(e), (e = t.firstChild);
			}
		}
		class L {
			static send(t, e, r) {
				var n = new XMLHttpRequest(),
					o = e.join("&");
				(n.onreadystatechange = () => {
					n.readyState == XMLHttpRequest.DONE &&
						200 == n.status &&
						r(n.responseText);
				}),
					n.open("POST", t, !0),
					n.setRequestHeader(
						"Content-type",
						"application/x-www-form-urlencoded"
					),
					n.send(o);
			}
		}
		(N.Controllers = {
			MESSAGE: "https://smsystem.razorlabs.com/exc/MessageController",
		}),
			(N.Params = {
				ACTION: "ac",
				PROJECT_ID: "pid",
				ID: "id",
				EMAIL: "eml",
				NAME: "nm",
				IMAGE_URL: "imgurl",
				DATA: "dta",
				VALUE: "val",
			}),
			(N.ActionsMessage = {
				SEND: "b",
				HAS_UNREAD_MESSAGES: "d",
				GET_ALL: "f",
			}),
			(N.UserType = { OPERATOR: 1, EXTERNAL_USER: 2 }),
			(N.DAYS = [
				"Sunday",
				"Monday",
				"Tuesday",
				"Wednesday",
				"Thursday",
				"Friday",
				"Saturday",
			]);
		var D = N;
		new (class {
			init() {
				(this.settings = new R()),
					C.init(),
					this.initListeners(),
					this.initSMSystem(),
					this.initHeaderModer();
			}
			initListeners() {
				o.browser.runtime.onMessage.addListener((t, e, r) => {
					var n = !1;
					switch (t.key) {
						case a.Settings.ID:
							n = this.settings.onMessage(t.action, t.data, e, r);
							break;
						default:
							console.warn(`Not implemeted action recieved. Key: ${t.key}`);
					}
					return n;
				}),
					window.navigator.userAgent.indexOf("Edge") > -1 ||
						o.browser.commands.onCommand.addListener((t) => {
							var e;
							"toggle-current-website" === t
								? (1 ===
										(e = o.browser.extension.getViews({ type: "popup" }))
											.length && e[0].close(),
								  o.browser.tabs.query(
										{ active: !0, currentWindow: !0 },
										(t) => {
											var e = i.parseURL(t[0].url);
											this.settings.onMessage(
												a.Settings.ACTION_TOGGLE_MODE,
												{ url: e },
												null,
												() => {
													o.browser.tabs.reload(t[0].id);
												}
											);
										}
								  ))
								: "toggle-power-extension" === t &&
								  (1 ===
										(e = o.browser.extension.getViews({ type: "popup" }))
											.length && e[0].close(),
								  o.browser.tabs.query(
										{ active: !0, currentWindow: !0 },
										(t) => {
											var e = i.parseURL(t[0].url);
											this.settings.onMessage(
												a.Settings.ACTION_TOGGLE_POWER,
												{ url: e },
												null,
												() => {
													o.browser.tabs.reload(t[0].id);
												}
											);
										}
								  ));
						});
			}
			initSMSystem() {
				var t = localStorage.getItem("chat_info");
				null !== t &&
					((t = JSON.parse(t)),
					D.hasUnreadMessages("CU_Ivze_DH9u7QnUOivoMA", t.id, (t) => {
						!1 !== t &&
							o.browser.browserAction.setIcon({ path: "res/icon/48-msg.png" });
					}));
			}
			initHeaderModer() {
				o.browser.webRequest.onHeadersReceived.addListener(
					(t) => {
						for (
							var e = [], r = !1, n = !1, o = 0;
							o < t.responseHeaders.length;
							++o
						) {
							var i = t.responseHeaders[o].name.toLowerCase(),
								a = t.responseHeaders[o].value;
							"access-control-allow-origin" !== i
								? ("access-control-allow-credentials" === i
										? (r = !0)
										: "content-security-policy" === i &&
										  (t.responseHeaders[o].value = t.responseHeaders[
												o
										  ].value.replace("img-src", "img-src data: blob:")),
								  e.push(t.responseHeaders[o]))
								: "*" === a && (n = !0);
						}
						return (
							r && !n
								? e.push({
										name: "Access-Control-Allow-Origin",
										value: t.initiator,
								  })
								: e.push({ name: "Access-Control-Allow-Origin", value: "*" }),
							{ responseHeaders: e }
						);
					},
					{ urls: ["http://*/*", "https://*/*"] },
					["blocking", "responseHeaders"]
				);
			}
		})().init();
	},
]);
